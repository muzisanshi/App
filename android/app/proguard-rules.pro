# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /usr/local/Cellar/android-sdk/24.3.3/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

-dontwarn android.net.SSLCertificateSocketFactory
-dontwarn android.text.StaticLayout

-dontwarn org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement

-dontwarn org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement

-dontwarn android.os.SystemProperties

-dontwarn com.facebook.webpsupport.WebpBitmapFactoryImpl

-dontwarn com.facebook.imagepipeline.animated.factory.AnimatedFactoryImplSupport

-dontwarn com.facebook.imagepipeline.animated.factory.AnimatedFactoryImpl

-dontwarn com.facebook.imagepipeline.nativecode.WebpTranscoderImpl

-dontwarn com.yunos.baseservice.clouduuid.CloudUUID

-dontwarn android.os.SystemProperties

-dontwarn com.android.org.conscrypt.SSLParametersImpl

-dontwarn org.apache.harmony.xnet.provider.jsse.SSLParametersImpl

-dontwarn dalvik.system.CloseGuard

-dontwarn sun.security.ssl.SSLContextImpl


-keep class com.tencent.mm.sdk.** {
   *;
}
-keep class com.tencent.mm.opensdk.** {
    *;
}

-keep class com.tencent.wxop.** {
    *;
}


