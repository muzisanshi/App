package com.xiaoyangjiang.app;

import android.os.Bundle;
import com.facebook.react.ReactActivity;
import android.provider.Settings;
import android.util.Log;
import android.content.Intent;
import android.net.Uri;

import android.content.Intent; 
import android.content.res.Configuration; 

public class MainActivity extends ReactActivity {

	public static boolean isActivityCreated;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isActivityCreated = true;
        
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isActivityCreated = false;
    }
    
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Intent intent = new Intent("onConfigurationChanged");
        intent.putExtra("newConfig", newConfig);
        this.sendBroadcast(intent);
    }

    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return "App";
    }

}
