package com.xiaoyangjiang.app;

import android.app.Application;

import com.facebook.react.ReactApplication;
// import com.github.yamill.orientation.OrientationPackage;
import com.brentvatne.react.ReactVideoPackage;
import com.BV.LinearGradient.LinearGradientPackage;

// import com.rnfs.RNFSPackage;
// import com.eko.RNBackgroundDownloaderPackage;
// import com.heyao216.react_native_installapk.InstallApkPackager;
import com.rnfs.RNFSPackage;
import com.heyao216.react_native_installapk.InstallApkPackager;
import com.eko.RNBackgroundDownloaderPackage;
import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.toast.RCTToastPackage;

// import com.rnlib.wechat.RNWechatPackage;

// import com.theweflex.react.WeChatPackage;
import com.rnlib.wechat.RNWechatPackage;
import com.burnweb.rnwebview.RNWebViewPackage;
import com.beefe.picker.PickerViewPackage;
import com.payment.alipay.AlipayPackage;
import com.RNFetchBlob.RNFetchBlobPackage; 
import com.react.rnspinkit.RNSpinkitPackage;
import com.reactnative.ivpusic.imagepicker.PickerPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;

// import com.cnull.apkinstaller.ApkInstallerPackage;



import java.util.Arrays;
import java.util.List;


public class MainApplication extends Application implements ReactApplication {
  
  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
          new MainReactPackage(),
            // new OrientationPackage(),
            new ReactVideoPackage(),
            new LinearGradientPackage(),
            // new RNFSPackage(),
            // new RNBackgroundDownloaderPackage(),
            // new InstallApkPackager(),
            new RNFSPackage(),
            new InstallApkPackager(),
            new RNBackgroundDownloaderPackage(),
            new RNDeviceInfo(),
            new RCTToastPackage(),
            new RNWechatPackage(),
            // new RNWechatPackage(),
            // new WeChatPackage(),
            new RNWebViewPackage(),
            new PickerViewPackage(),
            
            
          new AlipayPackage(),
          new RNFetchBlobPackage(),
          new RNSpinkitPackage(),
          new PickerPackage()
           

      );
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
   
    SoLoader.init(this, /* native exopackage */ false);
  } 

}