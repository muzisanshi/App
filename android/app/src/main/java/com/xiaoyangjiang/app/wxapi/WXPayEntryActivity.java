
package com.xiaoyangjiang.app.wxapi;
 
import android.app.Activity;
import android.os.Bundle;
 
// import com.theweflex.react.WeChatModule;
import com.rnlib.wechat.RNWechatModule;
 
/**
 * Created by admin on 2017/11/1.
 */
 
public class WXPayEntryActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // WeChatModule.handleIntent(getIntent());
        RNWechatModule.handleIntent(this.getIntent());
        finish();
    }
}