
/**
 * @name Cart.js
 * @auhor 李磊
 * @date 2018.8.16
 * @desc 购物车页面
 */

import React,{Component} from "react";
import {ActivityIndicator,Modal,Image,Text,View,StyleSheet,ScrollView,FlatList,Dimensions,Button,TextInput,TouchableOpacity,Alert,TouchableWithoutFeedback} from 'react-native';
// 左滑组件
import Swipeout from 'react-native-swipeout';

// 导入工具函数库
import T from "../lib/Tools";
import BaseComponent from "./BaseComponent";
// 导入公共样式
import St from "../lib/Style";
// 导入图片资源库
import Imgs from "../lib/Img";
import Querendingdan from './cart/Querendingdan';

// 支付
import Alipay from 'react-native-payment-alipay';

// 转菊花
import Spinner from 'react-native-loading-spinner-overlay';


// Tools实例
let ti = T.getInstance();
let W = BaseComponent.W;
let H = BaseComponent.H;

let style = StyleSheet.create({
  wrapper:{
    backgroundColor:"rgb(236,236,236)",
    flex:1,
    flexDirection:"column",
    alignItems:"stretch",
    justifyContent:"flex-start",
    position:"relative",
  },
  statusBar:{
    backgroundColor:"rgb(255,255,255)",
    height:ti.select({ios:16,android:22}),
    position:"relative",
    zIndex:1,
    opacity:1,
  },
  navBar:{
    backgroundColor:"rgb(255,255,255)",
    height:"7%",
    position:"relative",
    zIndex:1,
    opacity:1,
    borderBottomWidth:1,
    borderBottomColor:"rgb(241,241,241)"
  },
  Modal:{
    flex:1,
    backgroundColor:'rgba(14,14,14,0.5)',
    justifyContent:'center',alignItems:'center',
  }
});

export default class Cart extends BaseComponent {
  
  constructor(props){
    super(props);
    
    this.state={
      visible:false,
      switch:0,// 0(没有数据)、1(有数据)
      checked:false,// 是否选中了所有商品
      hasChecked:false,// 判断是否有商品被选中
      isEditMode:false,// 是否处于编辑模式
      // 判断是否可以编辑
      canEdit:false,
      isSlideDelete:false,// 是否处于侧滑删除模式
      data:{},
      //默认地址
      defaultAddress:{},

      // 空数据显示区域
      emptyH:BaseComponent.H-110,

      // 提示遮罩
      hintVisible:false,

      // 无效商品列表
      invalidGoods:[],
      isCloseSwipeout:false,
      netError:false,//true表示网络错误
    }
  }

  // 计算商品相关
  calcuTotal(){
    let thiz = this;
    let data = thiz.state.data;
    // 选中商品sku数
    let totalSelectNum = 0;
    // 总sku数
    let totalItemNum = 0;

    if(data.shoppingCarGroupItems){
      for(let i=0;i<data.shoppingCarGroupItems.length;i++){
        let item = data.shoppingCarGroupItems[i];
        // 计算单个仓库是否全选
        let selectNum = 0;
        
        for(let ii=0;ii<item.shoppingCarItems.length;ii++){

          let it = item.shoppingCarItems[ii];

          // 处理商品属性
          let attrs = "";
          for(let k=0;k<it.sku.goodsAttributeOptions.length;k++){
            let attrItem = it.sku.goodsAttributeOptions[k];
            attrs += " "+attrItem.goodsAttributeOption.value;
          }
          it.attrs = attrs;
          
          totalItemNum++;
          if(it.checked){
            selectNum++;
            totalSelectNum++;
          }
        }
        thiz.log("--------selectNum--------",selectNum);
        if(selectNum==item.shoppingCarItems.length){
          item.repertory.isAllChecked = true;
        }else{
          item.repertory.isAllChecked = false;
        }
      }

      // 判断是否全部选中了
      if(totalItemNum!=0&&totalItemNum==totalSelectNum){
        data.feeComputeResult.isAllChecked = true;
      }else{
        data.feeComputeResult.isAllChecked = false;
      }

      // 判断是否可以去结算
      if(totalSelectNum>0){
        data.feeComputeResult.canPay = true;
      }else{
        data.feeComputeResult.canPay = false;
      }
      

    }
    thiz.setState({
      data:data,
    });
  }

  // 加载购物车数据
  loadData(isflower){
    let thiz = this;
    if(isflower){
      thiz.setState({
        visible:true
      });
    }
    
    let data = {};
    if(thiz.state.defaultAddress&&thiz.state.defaultAddress.id){
      data.receiverAddressId = thiz.state.defaultAddress.id;
    }
    thiz.request("shoppingCar/getAllGoods",{
          method:"POST",
          data:data,
          success:function(ret){
                thiz.setState({
                      visible:false
                });
                if(ret.respCode=="00"){
                      // 关闭商品属性选择
                      let sw = 0;
                      let canEdit = false;
                      
                      if(ret.data.shoppingCarGroupItems&&ret.data.shoppingCarGroupItems.length>0){
                        sw = 1;
                        canEdit = true;
                      }else{
                        // 调整样式
                        thiz.ajust();
                      }
                      thiz.setState({
                            switch:sw,
                            isvisible:false,
                            data:ret.data,
                            canEdit:canEdit,
                      });

                      // 判断收货地址字段是否存在
                      if(ret.data.receiverAddresses){

                        // 判断是否已经选择过地址了
                        if(thiz.state.defaultAddress&&thiz.state.defaultAddress.id){// 已经选择过地址了
                          thiz.log("--------选择了地址--------","选择了地址");
                          let count = 0;
                          // 从返回的地址中，判断当前选择的地址的状态，是否已删除，删除了就置空，未删除就更新
                          for(let i=0;i<ret.data.receiverAddresses.length;i++){
                            if(ret.data.receiverAddresses[i].id==thiz.state.defaultAddress.id){
                              thiz.setState({
                                defaultAddress:ret.data.receiverAddresses[i]
                              });
                              break;
                            }else{
                              count++;
                            }
                          }
                          // 如果count的值和返回地址的长度一致，说明找不到当前选择的地址，它已经被干掉了
                          if(count==ret.data.receiverAddresses.length){
                            // 置空收货地址
                            thiz.setState({
                              defaultAddress:{}
                            });
                          }

                        }else{// 未选择过地址
                          thiz.log("--------未选择过地址--------","未选择过地址");
                          let dnum = 0;
                          // 直接设置地址信息
                          for(let i=0;i<ret.data.receiverAddresses.length;i++){
                            if(ret.data.receiverAddresses[i].defaultAddress){
                              thiz.setState({
                                    defaultAddress:ret.data.receiverAddresses[i]
                              });
                              dnum++;
                              break;
                            }
                          }
                          if(dnum==0&&ret.data.receiverAddresses.length>0){// 没有默认地址，取第一个地址显示
                            thiz.setState({
                              defaultAddress:ret.data.receiverAddresses[0]
                            });
                          }

                        }
                      }else{// 没有地址，默认置空
                        thiz.setState({
                          defaultAddress:{}
                        });
                      }

                      // 计算总价和仓库总价
                      thiz.calcuTotal();
                      thiz.log("--------thiz.state.data--------",thiz.state.data);
                      thiz.log("--------thiz.state.defaultAddress--------",thiz.state.defaultAddress);
                }
          },
          error:function(err){
                thiz.toast(err.msg);
                thiz.setState({
                      visible:false,
                });
                if(err.code&&err.code==200){
                  thiz.setState({netError:true});
                } 
          }
    });
  }


  
  // 渲染购物车为空的模板函数
  empty(){
    let thiz = this;
    return (
      <View ref="empty" style={{backgroundColor:"white",height:thiz.state.emptyH,
      flexDirection:"column",justifyContent:"center"}}>
        <View style={{paddingTop:40,paddingBottom:40,display:"flex"}}>  
          <Image source={require("../image/cart/empty.png")} style={{width:W*0.48,height:W*0.48,marginLeft:W*0.27}}/>
          <Text style={{textAlign:"center",fontSize:17,color:"#020202",marginTop:20,}}>购物车空空如也</Text>
          <Text style={{textAlign:"center",fontSize:14,color:"#9F9F9F",marginTop:5,}}>赶紧抢点东西慰劳自己吧</Text>
        </View>
        <View style={{display:"none",backgroundColor:"transparent",flexDirection:"row",justifyContent:"flex-start",marginTop:30,paddingBottom:30,}}>
        
          <View style={{width:W*0.42,height:W*0.42,marginLeft:W*0.06,marginRight:W*0.02,
            borderWidth:1,borderColor:"rgb(241,241,241)",justifyContent:"center",alignItems:"center"}}>
            <Text style={{fontSize:22,color:"#020202"}}>每日上新</Text>
            <Text style={{color:"#020202",marginTop:5,}}>NEW ARRIVALS</Text>
          </View>
          
          <View style={{width:W*0.42,height:W*0.42,marginLeft:W*0.02,marginRight:W*0.06,
            borderWidth:1,borderColor:"rgb(241,241,241)",justifyContent:"center",alignItems:"center"}}>
            <Text style={{fontSize:22,color:"#020202"}}>热销商品</Text>
            <Text style={{color:"#020202",marginTop:5,}}>HOT GOODS</Text>
          </View>
          
        </View>
      </View>
    );
  }

  // 发请求改变选中状态（使用）
  changeCheckState(state,ids){
    let thiz = this;
    thiz.log("--------changeCheckState--------","changeCheckState");
    if(ids.length>0){
      thiz.setState({
            visible:true
      });
      thiz.request("shoppingCar/checkOrUnCheckItem",{
            method:"POST",
            data:{
              checked:state,
              shoppingCarItemIds:ids
            },
            success:function(ret){
                  
                  if(ret.respCode=="00"){
                        // 重新加载数据
                        thiz.loadData(true);
                  }else{
                    thiz.setState({
                          visible:false
                    });
                  }
            },
            error:function(err){
                  thiz.toast(err.msg);
                  thiz.setState({
                        visible:false,
                  });
            }
      });
    }
  }

  // 发请求改变商品数量
  changeNum(num,id){
    let thiz = this;
    thiz.log("--------changeNum--------","changeNum");
    thiz.setState({
          visible:true
    });
    thiz.request("shoppingCar/updateGoodsNum",{
          method:"POST",
          data:{
            num:num,
            shoppingCarItemId:id
          },
          success:function(ret){
                if(ret.respCode=="00"){
                      // 重新加载数据
                      thiz.loadData(true);
                }else{
                  thiz.setState({
                        visible:false
                  });
                }
          },
          error:function(err){
                thiz.toast(err.msg);
                thiz.setState({
                      visible:false,
                });
          }
    });
  }

  // 删除商品
  delGoods(ids,callback){
    let thiz = this;
    thiz.log("--------delGoods--------","delGoods");
    thiz.setState({
          visible:true
    });
    thiz.request("shoppingCar/removeGoods",{
          method:"POST",
          data:{
            shoppingCarItemIds:ids
          },
          success:function(ret){
                
                if(ret.respCode=="00"){
                      // 重新加载数据
                      thiz.loadData(true);
                }else{
                  thiz.setState({
                        visible:false,
                        isCloseSwipeout:true,
                  });
                }
          },
          error:function(err){
                thiz.toast(err.msg);
                thiz.setState({
                      visible:false,
                      isCloseSwipeout:true,
                });
          }
    });
  }

  // 选中单条商品（使用）
  checkItemGoods(index,subindex){
    let thiz = this;
    let data = thiz.state.data;
    // 判断当前点击的单条商品是否处于选中状态
    let checkState = !data.shoppingCarGroupItems[index].shoppingCarItems[subindex].checked;
    let id = data.shoppingCarGroupItems[index].shoppingCarItems[subindex].id;

    // 发请求改变状态
    thiz.changeCheckState(checkState,[id]);

  }

  // 选中单个仓库的所有商品（使用）
  checkGoods(index){
    let thiz = this;
    let data = thiz.state.data;
    let ids = [];
    let checkState = !data.shoppingCarGroupItems[index].repertory.isAllChecked;

    // 获取当前仓库的所有单条商品id
    for(let i=0;i<data.shoppingCarGroupItems[index].shoppingCarItems.length;i++){
      let item = data.shoppingCarGroupItems[index].shoppingCarItems[i];
      ids.push(item.id);
    }

    // 发请求改变状态
    thiz.changeCheckState(checkState,ids);

  }

  // 选中所有商品（使用）
  checkAllGoods(){
    let thiz = this;
    thiz.log("--------checkAllGoods--------","checkAllGoods");
    let data = thiz.state.data;

    let ids = [];
    let checkState = !data.feeComputeResult.isAllChecked;

    // 获取所有单条商品id
    for(let i=0;i<data.shoppingCarGroupItems.length;i++){

      for(let ii=0;ii<data.shoppingCarGroupItems[i].shoppingCarItems.length;ii++){
        let it =data.shoppingCarGroupItems[i].shoppingCarItems[ii];
        ids.push(it.id);
      }

    }

    // 发请求改变状态
    thiz.changeCheckState(checkState,ids);

  }

  // 增加商品数量（使用）
  addNum(index,subindex){
    let thiz = this;
    let data = thiz.state.data;
    let item = data.shoppingCarGroupItems[index].shoppingCarItems[subindex];
    thiz.log("--------num&minNum--------",item.sku.num+"   "+item.sku.spu.singleOrderMinNum);
    if(item.num<item.sku.canSalesStockNum&&item.num<item.sku.spu.singleOrderMaxNum){
      // 发请求修改数量
      thiz.changeNum(item.num+1,item.id);
    }else{
      let max = item.sku.canSalesStockNum<item.sku.spu.singleOrderMaxNum?item.sku.canSalesStockNum:item.sku.spu.singleOrderMaxNum;
      thiz.toast("至多购买"+max+"件该商品");
    }
  }
  // 减少商品数量（使用）
  minusNum(index,subindex){
    let thiz = this;
    let data = thiz.state.data;
    let item = data.shoppingCarGroupItems[index].shoppingCarItems[subindex];
    thiz.log("--------num&maxNum--------",item.sku.num+"   "+item.sku.spu.singleOrderMinNum);
    if(item.num>item.sku.spu.singleOrderMinNum){
      // 发请求修改数量
      thiz.changeNum(item.num-1,item.id);
    }else{
      thiz.toast("至少购买"+item.sku.spu.singleOrderMinNum+"件该商品");
    }

  }
  // 删除商品（使用）
  deleteGoods(index,subindex){
    let thiz = this;
    let data = thiz.state.data;
    let item = data.shoppingCarGroupItems[index].shoppingCarItems[subindex];

    // 发请求删除商品
    thiz.delGoods([item.id]);
  }

  // 监听商品侧滑显示删除按钮（使用）
  onSlideOpen(index,subindex){
    this.log("-------onSlideOpen-------","onSlideOpen");
    var state = this.state;

    if(!state.isSlideDelete){
      state.isSlideDelete = true;
      this.setState(state);
    }

  }
  // 使用
  onSlideClose(index,subindex){
    this.log("-------onSlideClose-------","onSlideClose");
    var state = this.state;

    if(state.isSlideDelete){
      state.isSlideDelete = false;
      this.setState(state);
    }

  }

  // 渲染商品
  renderGoods(data){
    var thiz = this;
    // let btns = [
    //   {
    //     text: '删除',
    //     backgroundColor:"#FF407E",
    //     onPress:
    //   }
    // ];
    return (
      <View style={{paddingBottom:10,}}>
        
        {
          data.map(function(value,index){
            return (
              <View style={{backgroundColor:"white",marginTop:10,}}>
                {/*仓库*/}
                <View style={{flexDirection:"row",padding:10,borderBottomWidth:1,borderBottomColor:"rgb(241,241,241)",backgroundColor:"transparent"}}>
                  <TouchableWithoutFeedback onPress={()=>thiz.checkGoods(index)}>
                    <Image source={value.repertory.isAllChecked?require("../image/home/xuanzhong.png"):require("../image/cart/e_circle.png")} style={{width:24,height:24,lineHeight:24,}}/>
                  </TouchableWithoutFeedback>
                  <Image source={require("../image/cart/shop.png")} style={{width:20,height:20,marginLeft:8,marginTop:2,}}/>
                  <Text style={{color:"#333333",marginLeft:8,fontSize:16,lineHeight:22,}}>{value.repertory.name}</Text>
                </View>

                {/*循环商品*/}
                {
                  value.shoppingCarItems.map(function(v,i){
                    return (
                      <Swipeout 
                        right={
                          [
                            {
                              text: '删除',
                              color:"#3a3a3a",
                              backgroundColor:"#FDD17A",
                              onPress:()=>{
                                thiz.deleteGoods(index,i);
                              }
                            }
                          ]
                        } 
                        buttonWidth={W*0.17} 
                        style={{backgroundColor:"white",}}
                        onOpen={()=>thiz.onSlideOpen(index,i)}
                        onClose={()=>thiz.onSlideClose(index,i)}
                        sensitivity={0}
                        autoClose={true}>

                        <View style={{paddingLeft:10,paddingRight:10,flexDirection:"row"}}>

                          <View style={{justifyContent:"center",alignItems:"center",}}>
                            <TouchableWithoutFeedback onPress={()=>thiz.checkItemGoods(index,i)}>
                              <Image source={v.checked?require("../image/home/xuanzhong.png"):require("../image/cart/e_circle.png")} style={{width:24,height:24,}}/>
                            </TouchableWithoutFeedback>
                          </View>

                          {/*右边布局*/}
                          
                          <View style={{flexDirection:"column",flexGrow:1,marginLeft:10,borderBottomWidth:1,borderBottomColor:"rgb(241,241,241)",paddingTop:15,paddingBottom:15,}}>
                            
                            <TouchableWithoutFeedback onPress={()=>{
                              // 判断是否处于编辑模式
                              if(!thiz.state.isEditMode){
                                thiz.navigate("GoodsXiangQing",{id:v.sku.id,from:"Cart"});
                              }
                            }}>
                            <View style={{flexDirection:"row",width:"100%"}}>
                              <Image source={{uri:thiz.state.data.shoppingCarGroupItems[index].shoppingCarItems[i].sku.imageAttachment?thiz.state.data.shoppingCarGroupItems[index].shoppingCarItems[i].sku.imageAttachment.resourceFullAddress:""}} style={{width:64,height:64,}}/>
                              <View style={{position:"relative",width:"60%",paddingLeft:10,}}>

                                {/*商品名称*/}
                                <Text numberOfLines={2} style={{color:"#000000",
                                  fontSize:14,width:thiz.state.isEditMode?0:"100%",height:thiz.state.isEditMode?0:"auto"}}>{thiz.state.data.shoppingCarGroupItems[index].shoppingCarItems[i].sku.name}</Text>

                                {/*商品数量加减*/}
                                <View style={{display:thiz.state.isEditMode?"flex":"none",width:"70%",flexDirection:"row",borderWidth:thiz.state.isEditMode?1:0,borderColor:"#989898",borderRadius:3,}}>
                                  <TouchableWithoutFeedback onPress={()=>thiz.minusNum(index,i)}>
                                    <View style={{padding:3}}>
                                      <Image source={require("../image/cart/minus.png")} style={{width:thiz.state.isEditMode?26:0,height:26,}}/>
                                    </View>
                                  </TouchableWithoutFeedback>
                                  <View style={{justifyContent:"center",alignItems:"center",flexGrow:1,borderLeftWidth:1,borderColor:"#989898",borderRightWidth:1,}}>
                                    <Text style={{fontSize:20,color:"#212121"}}>{thiz.state.data.shoppingCarGroupItems[index].shoppingCarItems[i].num}</Text>
                                  </View>
                                  <TouchableWithoutFeedback onPress={()=>thiz.addNum(index,i)}>
                                    <View style={{padding:3}}>
                                      <Image source={require("../image/cart/add.png")} style={{width:thiz.state.isEditMode?26:0,height:26,}}/>
                                    </View>
                                  </TouchableWithoutFeedback>
                                </View>

                                {/*商品属性*/}
                                <Text numberOfLines={1} style={{position:"absolute",bottom:0,fontSize:12,color:"#999999",left:10,}}>{v.attrs}</Text>
                                
                              </View>

                              <View style={{flexGrow:1,flexDirection:"column",position:"relative",height:"100%"}}>
                                {/*商品价格*/}
                                <Text style={{fontSize:14,textAlign:"right",width:"100%",color:"#3a3a3a"}}>¥{thiz.state.data.shoppingCarGroupItems[index].shoppingCarItems[i].sku.realSalePrice}</Text>
                                {/*商品数量*/}
                                <Text style={{fontSize:12,textAlign:"right",width:thiz.state.isEditMode?0:"100%",color:"#3a3a3a"}}>×{thiz.state.data.shoppingCarGroupItems[index].shoppingCarItems[i].num}</Text>
                                {/*税费*/}
                                <Text style={{position:"absolute",bottom:0,fontSize:12,color:"#999999",textAlign:"right",right:0,width:thiz.state.isEditMode?0:200}}>税费：￥{(v.sku.taxPrice*v.num).toFixed(2)}</Text>

                                {/*删除图标*/}
                                <TouchableWithoutFeedback onPress={()=>thiz.deleteGoods(index,i)}>
                                  <Image source={require("../image/cart/delete.png")} style={{position:"absolute",bottom:0,right:0,width:thiz.state.isEditMode?20:0,height:20,}}/>
                                </TouchableWithoutFeedback>
                                
                              </View>
                            </View>
                            </TouchableWithoutFeedback>

                            {/*服务*/}
                            <View style={{flexDirection:"row",width:"100%",marginTop:10,display:v.specialServiceOptions&&v.specialServiceOptions.length>0?"flex":"none"}}>
                              <View>
                                <Text style={{fontSize:12,color:"#999999"}}>【服务】</Text>  
                              </View>
                              <View style={{marginLeft:10,flexGrow:1,flexDirection:"column"}}>
                                {
                                  v.specialServiceOptions?v.specialServiceOptions.map(function(sv,si){
                                    return (
                                      <View style={{flexDirection:"row",position:"relative",width:"100%",marginBottom:2,}}>
                                        <Text style={{fontSize:12,color:"#999999"}}>{sv.name}</Text>
                                        <View style={{flexDirection:"row",flexGrow:1,position:"absolute",right:0,}}>
                                          <Text style={{fontSize:12,color:"#3A3A3A",marginRight:10,}}>￥{sv.price.toFixed(2)}</Text>
                                          <Text style={{fontSize:12,color:"#3A3A3A"}}>x{v.num}</Text>
                                        </View>
                                      </View>
                                    );
                                  }):null
                                }

                              </View>
                            </View>
                          </View>
                          
                        </View>

                      </Swipeout>
                    )
                  })
                }

                {/*总计*/}
                <View style={{paddingTop:15,paddingBottom:15,paddingRight:10,}}>
                  <Text style={{fontSize:12,color:"#3A3A3A",textAlign:"right"}}>优惠：-¥{value.feeComputeResult.reduceTotalAmount.toFixed(2)}</Text>
                  <Text style={{fontSize:12,color:"#3A3A3A",textAlign:"right",marginTop:5,}}>总计：¥{value.feeComputeResult.totalAmount.toFixed(2)}</Text>
                </View>

              </View>
            );
          })
        }
      </View>
    );
    
  }

  // 编辑
  edit(){
    // 让界面处于编辑模式
    this.setState({
      isEditMode:true,
    });

  }

  // 完成编辑
  editOver(){
    // 退出编辑模式
    this.setState({
      isEditMode:false,
    });
  }

  // 去支付
  toPay(){
    let thiz = this;

    // 获取收货地址
    let addressId = thiz.state.defaultAddress.id;
    if(addressId==undefined){
      thiz.toast("您还没有邮寄地址，请添加邮寄地址");
      return;
    }
    // 判断是否有商品选中
    if(thiz.state.data.feeComputeResult.canPay){
      // 调用生成订单接口
      thiz.setState({
            visible:true
      });
      thiz.request("shoppingCar/settleConfirmV2",{
      // thiz.request("shoppingCar/settleConfirm",{
            method:"POST",
            data:{
              receiverAddressId:addressId
            },
            success:function(ret){
                  thiz.setState({
                        visible:false
                  });
                  if(ret.respCode=="00"){
                      
                        // 判断商品的库存和下架
                        let count = 0;
                        let invalidGoods = [];
                        for(let i=0;i<ret.data.shoppingCarGroupItems.length;i++){
                          for(let ii=0;ii<ret.data.shoppingCarGroupItems[i].shoppingCarItems.length;ii++){
                            let it = ret.data.shoppingCarGroupItems[i].shoppingCarItems[ii];
                            if(it.checked && (it.sku.goodsState=="NOT_ON_SALE" || it.sku.goodsState=="SOLD_OUT")){

                              // 处理商品属性
                              let attrs = "";
                              for(let k=0;k<it.sku.goodsAttributeOptions.length;k++){
                                let attrItem = it.sku.goodsAttributeOptions[k];
                                attrs += " "+attrItem.goodsAttributeOption.value;
                              }
                              it.attrs = attrs;
                              
                              invalidGoods.push(it);
                              count++;
                            }
                          }
                        }
                        if(count>0){
                          thiz.log("--------有无效商品--------","有无效商品");
                          //弹窗提示
                          thiz.setState({
                            hintVisible:true,
                            invalidGoods:invalidGoods,
                          });
                          return;
                        }else{
                          thiz.log("--------商品全部有效--------","商品全部有效");
                        }

                        // 处理默认地址
                        // for(let i=0;i<ret.data.receiverAddresses.length;i++){
                        //   if(ret.data.receiverAddresses[i].defaultAddress){
                        //     thiz.setState({
                        //           defaultAddress:ret.data.receiverAddresses[i]
                        //     });
                        //     break;
                        //   }
                        // }

                        // 计算总价和仓库总价
                        thiz.calcuTotal();

                        // 跳转到确认订单页面
                        let finalData = ret.data;
                        finalData.defaultAddress = thiz.state.defaultAddress;
                        // 处理电话号码
                        let phone = finalData.defaultAddress.receiverPhoneNo;
                        if(phone){
                          finalData.defaultAddress.receiverPhoneNo = phone.substring(0,3)+"****"+phone.substring(phone.length-4);
                        }
                        // 过滤商品图片
                        let imgs = [];
                        let goodsNum = 0;

                        for(let i=0;i<ret.data.shoppingCarGroupItemsV2.length;i++){
                          let item = ret.data.shoppingCarGroupItemsV2[i];
                          for(let ii=0;ii<item.shoppingCarItems.length;ii++){
                            // 判断商品是否处于选中状态
                            if(item.shoppingCarItems[ii].checked){
                              imgs.push(item.shoppingCarItems[ii].sku.imageAttachment.resourceFullAddress);
                              goodsNum += item.shoppingCarItems[ii].num;
                            }
                          }
                        }

                        finalData.imgs = imgs;
                        finalData.goodsNum = goodsNum;

                        thiz.log("--------finalData--------",finalData);
                        
                        thiz.goPage("Querendingdan",{title:"确认订单",data:finalData,allData:ret.data.shoppingCarGroupItemsV2,from:"Cart"});

                  }
            },
            error:function(err){
                  thiz.toast(err.msg);
                  thiz.setState({
                        visible:false,
                  });
            }
      });
    }
  }

  // 取消提交
  cancelCommit(){
    let thiz = this;
    thiz.setState({
      hintVisible:false,
    });
  }
  // 继续提交
  commit(){
    let thiz = this;
    thiz.setState({
      hintVisible:false,
    });
    thiz.log("--------无效商品列表--------",thiz.state.invalidGoods);
    // 先从购物车删除无效商品
    let ids = [];
    for(let i=0;i<thiz.state.invalidGoods.length;i++){
      ids.push(thiz.state.invalidGoods[i].id);
    }

    thiz.setState({
          visible:true
    });
    thiz.request("shoppingCar/removeGoods",{
          method:"POST",
          data:{
            shoppingCarItemIds:ids
          },
          success:function(ret){
                
                if(ret.respCode=="00"){
                      // 提交数据
                      thiz.toPay();
                }
                // else{
                //   thiz.setState({
                //         visible:false
                //   });
                // }
          },
          error:function(err){
                thiz.toast(err.msg);
                thiz.setState({
                      visible:false,
                });
          }
    });

  }

  render(){
    let thiz = this;
    return (
      <View style={style.wrapper}>
        {/*状态栏占位*/}
        <View style={{backgroundColor:"rgb(255,255,255)",
                      height:thiz.isIphoneX(35,BaseComponent.SH),
                      position:"relative",
                      zIndex:1,
                      opacity:1,}}></View>

        {/*转菊花*/}
        <Modal
          visible={this.state.visible}
          onRequestClose={()=>{this.setState({visible:false})}}
          transparent={true}
          animationType={"fade"}>
          <View style={{backgroundColor:"rgba(0,0,0,0.4)",width:"100%",width:"100%",flex:1,flexDirection:"column",alignItems:"center",justifyContent:"center",position:"relative"}}>
            <View>
                  <ActivityIndicator size="large" color='white'/>
            </View>
          </View>
        </Modal>

        {/*商品无效提示*/}
        <Modal visible={this.state.hintVisible}
             transparent={true}
                 onRequestClose={() => {this.setState({hintVisible:false})}}>
             <View style={style.Modal}>
              <View style={{width:BaseComponent.W*275/375,backgroundColor:'rgba(255, 255, 255,1)',borderRadius:BaseComponent.W*20/375}}>
              <View style={{width:'100%',padding:15,}}>
                <Text style={{fontSize:BaseComponent.W*15/375,color:'rgba(34, 34, 34, 1)',textAlign:"center"}}>您购物车中存在无效商品，请确认后提交。</Text>
              </View>

              {/*无效商品列表*/}
              <View style={{width:"100%",paddingLeft:10,paddingRight:10,maxHeight:BaseComponent.H*0.5,backgroundColor:"white",}}>
                {
                  thiz.state.invalidGoods&&thiz.state.invalidGoods.length>0?thiz.state.invalidGoods.map(function(vv,ii){
                    return (
                      <View style={{flexDirection:"column",borderBottomWidth:ii==thiz.state.invalidGoods.length-1?0:1,borderBottomColor:"rgb(241,241,241)",paddingTop:10,paddingBottom:10,width:"100%"}}>
                        <View style={{flexDirection:"row",width:"100%"}}>
                          <Image source={{uri:vv.sku.imageAttachment?vv.sku.imageAttachment.resourceFullAddress:""}} style={{width:64,height:64,}}/>
                          <View style={{position:"relative",width:"60%",paddingLeft:10,}}>

                            {/*商品名称*/}
                            <Text numberOfLines={2} style={{color:"#000000",fontSize:14,width:"100%"}}>{vv.sku.name}</Text>

                            {/*商品属性*/}
                            <Text style={{position:"absolute",bottom:0,fontSize:12,color:"#999999",left:10,}}>{vv.attrs}</Text>
                            
                          </View>
                        </View>
                      </View>
                    );
                  }):null
                }
              </View>

              <View style={{width:'100%',height:0.5,backgroundColor:'rgba(189, 189, 189, 1)'}}></View>

              <View style={{width:'100%',flexDirection:'row',}}>

              <TouchableWithoutFeedback onPress={()=>this.cancelCommit()}> 
                <View style={{width:'50%',justifyContent:'center',alignItems:'center',padding:15,}}>
                  <Text style={{fontSize:BaseComponent.W*15/375,color:'rgba(94, 94, 94, 1)'}}>返回重选</Text>
                </View>
              </TouchableWithoutFeedback> 

                <View style={{width:0.5,height:'100%',backgroundColor:'rgba(189, 189, 189, 1)'}}></View>

                <TouchableWithoutFeedback onPress={()=>{this.commit()}}> 

                <View style={{width:'50%',justifyContent:'center',alignItems:'center',padding:15,}}>
                  <Text style={{fontSize:BaseComponent.W*15/375,color:'rgba(16, 98, 255, 1)'}}>继续提交</Text>
                </View>
                </TouchableWithoutFeedback>

              </View>

              </View>
             </View>
        </Modal>

        {/*顶部导航栏*/}
        <View style={style.navBar}>
          <View style={{flex:1,flexDirection:"row",justifyContent:"flex-start"}}>

            {/*占位*/}
            <TouchableWithoutFeedback>
            <View style={{width:"25.4%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center",opacity:0,}}>
              <Image source={Imgs.back} style={{width:(BaseComponent.W*0.127*0.4),height:(BaseComponent.W*0.127*0.4),marginTop:ti.select({ios:BaseComponent.W*0.02,android:0}),marginRight:BaseComponent.W*0.04}}></Image>
            </View>
            </TouchableWithoutFeedback>

            {/*标题*/}
            <View style={{flexGrow:1,backgroundColor:"transparent",
              marginTop:ti.select({ios:(H*0.02)/2,android:(H*0.016)/2}),borderRadius:ti.select({ios:(H*0.05)/2,android:(H*0.054)/2}),borderColor:"#0A0A0A",
              borderWidth:0,}}>
              <Text style={{color:"#0A0A0A",width:"100%",textAlign:"center",marginTop:ti.select({ios:"0%",android:"0%"}),fontSize:17}}>购物车</Text>
            </View>

            {/*编辑display:!thiz.state.canEdit||thiz.state.isEditMode?"none":"flex"  this.edit()*/}
            <View style={{width:"12.7%",display:"none",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center",position:"relative",}}>
              <TouchableWithoutFeedback onPress={()=>{}}>
                <Text style={{color:"#323232",marginBottom:2,}}>编辑</Text>
              </TouchableWithoutFeedback>
            </View>
            
            {/*消息*/}
            <TouchableWithoutFeedback onPress={()=>{
              // this.navigate("Msgcenter",{title:"消息中心"});
            }}>
            <View style={{opacity:0,width:"12.7%",display:thiz.state.isEditMode?"none":"flex",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center",position:"relative",}}>
              <Image source={require('../image/cart/msg_gray.png')} style={{width:(BaseComponent.W*0.127*0.5),height:(BaseComponent.W*0.127*0.5),position:"absolute",bottom:"23%"}}></Image>
            </View>
            </TouchableWithoutFeedback>

            {/*空白占位*/}
            <View style={{width:"12.7%",display:this.state.isEditMode?"flex":"none",height:"100%",backgroundColor:"transparent",justifyContent:"center",
              alignItems:"center",position:"relative",opacity:0,}}>
              <Text>空白占位</Text>
            </View>

            {/*完成编辑display:this.state.isEditMode?"flex":"none"*/}
            <TouchableWithoutFeedback onPress={()=>{
                let mode = thiz.state.isEditMode;
                thiz.setState({
                  isEditMode:!mode
                });
              }}>
            <View style={{opacity:thiz.state.data.shoppingCarGroupItems&&thiz.state.data.shoppingCarGroupItems.length>0?1:0,width:"12.7%",
              display:"flex",height:"100%",backgroundColor:"transparent",justifyContent:"center",
              alignItems:"center",position:"relative",}}>
              
                <Text style={{color:"#3a3a3a",marginBottom:2,}}>{this.state.isEditMode?"完成":"编辑"}</Text>
              
            </View>
            </TouchableWithoutFeedback>

          </View>
        </View>

        {/*地址选择*/}
        <TouchableWithoutFeedback onPress={()=>{
          
            if(thiz.state.data.receiverAddresses&&thiz.state.data.receiverAddresses.length>0){
              
              this.goPage("shouhuoAddress",{title:"收货地址",isBack:true,from:"Cart",id:this.state.defaultAddress.id});
            }else{
             this.goPage('AddAddress',{title:'添加地址',from:"Cart"});
            }
        }
        }>
          <View style={{flexDirection:"row",justifyContent:"flex-start",display:this.state.switch==0?"none":'flex',
          paddingTop:10,paddingRight:5,paddingBottom:10,paddingLeft:5,backgroundColor:"white",borderBottomWith:1,borderColor:"rgb(241,241,241)"}}>
            <Image source={require("../image/cart/location.png")} style={{width:20,height:20,marginRight:10,}}/>
            <Text style={{fontSize:14,color:"#3A3A3A",width:BaseComponent.W-70}}>{this.state.defaultAddress.id!=undefined?this.state.defaultAddress.fullAddress:"选择邮寄地址"}</Text>
            {/*箭头*/}
            <Image source={require("../image/cart/location.png")} style={{width:20,height:20,marginLeft:10,display:"none"}}/>
          </View>
        </TouchableWithoutFeedback>

        <ScrollView ref="mainScroll" showsVerticalScrollIndicator={false} style={{flexGrow:1,flexDirection:"column"}}>
        {/*主要内容*/}
        {
          this.state.netError?( 
                    <View style={{width:BaseComponent.W,height:BaseComponent.H*0.85,justifyContent:'center',alignItems:'center'}}>
                      
                      <Image style={{width:BaseComponent.W*180/375,height:BaseComponent.W*180/375,borderRadius:BaseComponent.W*90/375}} source={require('../image/mine/neterror.png')}></Image>
                      

                      <TouchableWithoutFeedback onPress={()=>{
                        thiz.setState({netError:false});
                        
                        thiz.loadData(true);
                      }}>
                      <View style={{width:BaseComponent.W*70/375,height:BaseComponent.W*30/375,borderRadius:BaseComponent.W*15/375,borderWidth:0.5,borderColor:'#080808',
                                justifyContent:'center',alignItems:'center',marginTop:BaseComponent.W*15/375}}>
                            <Text style={{fontSize:BaseComponent.W*15/375,color:'#252525'}}>刷新</Text>          
                      </View>
                      </TouchableWithoutFeedback>
                    </View>):(this.state.switch==0?this.empty():this.renderGoods(this.state.data.shoppingCarGroupItems?this.state.data.shoppingCarGroupItems:[]))
        }
        </ScrollView>

        {/*状态栏*/}
        <View style={{flexDirection:"row",borderTopWidth:0.5,borderTopColor:"#DDDDDD",backgroundColor:"white",display:this.state.switch==0?'none':'flex'}}>

          <View style={{width:"75%",flexDirection:"row",backgroundColor:"white",padding:10,paddingTop:15,paddingBottom:15,}}>

            <View style={{width:"25%",flexDirection:"row",position:"relative",top:0,alignItems:"center"}}>

              <TouchableWithoutFeedback onPress={()=>this.checkAllGoods()}>
                <Image source={this.state.data.feeComputeResult&&this.state.data.feeComputeResult.isAllChecked?require('../image/home/xuanzhong.png'):require('../image/cart/e_circle.png')} style={{width:24,height:24,}}/>
              </TouchableWithoutFeedback>

              <Text style={{marginLeft:10,fontSize:12,color:"#999999",lineHeight:24,}}>全选</Text>
            </View>

            <View style={{width:"75%",flexDirection:"column"}}>
              <Text style={{textAlign:"right"}}>实付款：<Text style={{color:"#FF407E",fontSize:15,fontWeight:"bold"}}>￥{this.state.data.feeComputeResult?this.state.data.feeComputeResult.payTotalAmount.toFixed(2):"0.00"}</Text></Text>
              {/*<View style={{width:"100%",position:"relative",flexDirection:"row"}}>
                <Text style={{fontSize:12,color:"#999999",textAlign:"right",width:"50%"}}>商品：<Text>￥{this.state.data.feeComputeResult?this.state.data.feeComputeResult.goodsTotalAmount.toFixed(2):"0.00"}</Text></Text>
                <Text style={{fontSize:12,color:"#999999",textAlign:"right",width:"50%"}}>税费：<Text>￥{this.state.data.feeComputeResult?this.state.data.feeComputeResult.taxTotalAmount.toFixed(2):"0.00"}</Text></Text>
              </View>
              <View style={{width:"100%",position:"relative",flexDirection:"row"}}>
                <Text style={{fontSize:12,color:"#999999",textAlign:"right",width:"50%"}}>服务：<Text>￥{this.state.data.feeComputeResult?this.state.data.feeComputeResult.specialServiceTotalAmount.toFixed(2):"0.00"}</Text></Text>
                <Text style={{fontSize:12,color:"#999999",textAlign:"right",width:"50%"}}>邮费：<Text>￥{this.state.data.feeComputeResult?this.state.data.feeComputeResult.logisticTotalAmount.toFixed(2):"0.00"}</Text></Text>
              </View>*/}
            </View>

          </View>
          {/*this.navigate('Querendingdan',{title:'确认订单'});*/}
          <TouchableWithoutFeedback onPress={()=>{this.toPay()}}>
            <View style={{width:"25%",justifyContent:"center",alignItems:"center",backgroundColor:(function(){
              let color = "#999999";
              if(thiz.state.data.feeComputeResult&&thiz.state.data.feeComputeResult.canPay&&thiz.state.defaultAddress.id!=undefined){
                color = "#FDD17A";
              }
              return color;
            })()}}>
              <Text style={{fontSize:18,color:"#3A3A3A"}}>去结算</Text>
            </View>
          </TouchableWithoutFeedback>

        </View>

      </View>
    );
  }

  // 待结算按钮的颜色
  getColor(){

  }

  ajust(){
    let thiz = this;
    if(thiz.refs.mainScroll){
      // 获取scrollview的高度
      let sh = thiz.getViewInfo(thiz.refs.mainScroll,function(x,y,width,height,left,top){
        thiz.log("--------ajust--------",height);
        thiz.setState({
          emptyH:height
        });
      });
    }
  }

  componentDidMount(){
    let thiz = this;
    thiz.loadData(true);

    // 监听消息
    thiz.listen("add_cart_success",function(){
      thiz.loadData(false);
    });
    
    //监听默认地址改变
    thiz.listen("set_default_address",function(ret){
        thiz.log("--------set_default_address--------",thiz.state.defaultAddress);
        var addressData={};
        addressData.id=ret.data.id;
        addressData.fullAddress=ret.data.address;
        thiz.setState({
          defaultAddress:addressData,
        });
        
        // 刷新购物车
        thiz.loadData(false);

    });

    // 监听删除地址成功
    thiz.listen("delete_address_success",function(){
      thiz.log("--------delete_address_success--------","delete_address_success");
      thiz.loadData(false);
    });

    // 监听保存地址成功
    thiz.listen("save_receiveraddr_success",function(ret){
      thiz.log("--------Cart_save_receiveraddr_success--------",ret);

      // 地址只要编辑了，就必须在用到的地方更新，如果有就更新
      if(ret&&ret.address){
        thiz.setState({
          defaultAddress:ret.address,
        });
      }
      setTimeout(function(){
        thiz.loadData(false);
      },200);
    });

    // 监听点击购物车tab
    thiz.listen("click_cart",function(){
      thiz.loadData(true);
    });
    //监听再次购买
    thiz.listen('click_buyAgain',function(){
      thiz.loadData(false);
    });
    // 监听下单成功
    thiz.listen("submit_order_success",function(){
      thiz.loadData(false);
    });

    // 监听取消支付订单
    thiz.listen("cancel_pay",function(){
      thiz.loadData(false);
    });
    
    thiz.listen("refresh_cart",function(){
      thiz.loadData(false);
    });

    // 调整样式
    thiz.ajust();

  }

}
                