
/**
 * @name Category.js
 * @auhor 李磊
 * @date 2018.8.16
 * @desc 分类页面
 */

import React, {Component} from "react";
import {Platform, StyleSheet, View,Text,Button,Image,TouchableWithoutFeedback} from "react-native";
import {ScrollView} from "react-native";
// 导入工具函数库
import T from "../lib/Tools";
import BaseComponent from "./BaseComponent";
// 导入公共样式
import St from "../lib/Style";
// 导入图片资源库
import Imgs from "../lib/Img";

// Tools实例
let ti = T.getInstance();

let style = StyleSheet.create({
  
});


export default class Category extends BaseComponent{

  constructor(props){
    super(props);

  }

  render() {
    return (
      <View>
      		<Image style={{width:100,height:100}} source={{
			  uri: 'http://112.74.41.66:8080/LOL/Image/123.jpg',
			  cache: 'force-cache'}} />
			<Image style={{width:100,height:100}} source={{
			  uri: 'http://112.74.41.66:8080/LOL/Image/1.jpg',
			  cache: 'force-cache'}} />
			  <Image style={{width:100,height:100}} source={{
			  uri: 'http://112.74.41.66:8080/LOL/Image/12.jpg',
			  cache: 'force-cache'}} />
      </View>
    );
  }

}
