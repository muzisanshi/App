
/**
 * @name Home.js
 * @auhor 李磊
 * @date 2018.8.14
 * @desc APP首页
 */

import React,{Component} from "react";
import {ActivityIndicator,RefreshControl,Modal,Platform, StyleSheet, View,Text,Button,Image,TouchableWithoutFeedback,TouchableHighlight,Linking} from "react-native";
import {StatusBar,ScrollView} from "react-native";
import {createBottomTabNavigator} from 'react-navigation';
import Swiper from "react-native-swiper";
import Country from "./home/Country";
import DailyUpdate from "./home/DailyUpdate";
import HomeSearch from "./HomeSearch";
import Msgcenter from './mine/Msgcenter';
// 导入工具函数库
import T from "../lib/Tools";
import BaseComponent from "./BaseComponent";
// 导入公共样式
import St from "../lib/Style";
// 导入图片资源库
import Imgs from "../lib/Img";

// 转菊花
import Spinner from 'react-native-loading-spinner-overlay';
// 下拉组件
// import {PullView} from 'react-native-pull';

import MyImage from "../lib/MyImage";

import Launcher from "./common/Launcher";
import Guide from "./common/Guide";



// Tools实例
let ti = T.getInstance();

let style = StyleSheet.create({
  wrapper:{
    backgroundColor:"transparent",
    flex:1,
    flexDirection:"column",
    alignItems:"stretch",
    justifyContent:"flex-start",
    position:"relative",
    backgroundColor:"#f0f0f0",
  },
  // statusBar:{
  //   backgroundColor:"rgba(0,0,0,0.07)",
  //   height:ti.select({ios:16,android:22}),
  //   position:"relative",
  //   zIndex:1,
  //   opacity:1,
  // },
  navBar:{
    position:"relative",
    height:BaseComponent.H*0.05,
    zIndex:1,
    opacity:1,
  },
  scroll:{
    backgroundColor:"rgb(236,236,236)",
    top:0,
    left:0,
    width:"100%",
    height:BaseComponent.H - 50,
    position:"absolute",
    zIndex:0,

  },
  lunbotuimg:{
    width:"100%",
    height:"100%"
  },
  ad:{
    flex:1 ,
    backgroundColor:'#F0F0F0' ,
    paddingTop:10,
    paddingBottom:10
  },
  ad_1:{
    flexDirection:'row' ,
    justifyContent:'space-around',
    alignItems:'center' ,
  },
  img3:{
    height:80 ,
    width: BaseComponent.W*175/375,
    borderRadius:5 ,
    margin:10,
    marginRight:0,
    resizeMode:'contain',
  },
  img3_1:{
    height:80 ,
    width: BaseComponent.W*175/375,
    borderRadius:5 ,
    margin:10,
    marginLeft:7,
    resizeMode:'contain',
  },
  timeCountry:{
    flexDirection:'row' ,
    backgroundColor:"white"
    // height:164      
  },
  sort1:{
    width:BaseComponent.W/2,
    // height:164,
  },
  line1:{
    width:0.5,
    // height:"100%",
    backgroundColor:'#D9D9D9'   
  },
  line2:{
    width:BaseComponent.W,
    height:0.5,
    backgroundColor:'#D9D9D9'
    // backgroundColor:'red'
    
  },
  naifen:{
    flexDirection:'row',
    backgroundColor:'white',
  },
  sort2:{
    width:BaseComponent.W/3,

    // height:164 ,
  },
  recommendImg:{
    margin:BaseComponent.W*10/375 ,
    width:BaseComponent.W*355/375,   
    resizeMode:'contain' ,
    height:75 ,
  },
  dayNew:{
    height:54,
    backgroundColor:'white'
  },
  dailyUpdateTitle:{
    resizeMode:'contain',
    width:BaseComponent.W*69/375 ,
    marginLeft:BaseComponent.W*153/375 ,
    height:19,
    marginTop:17.5 ,
    marginBottom:17.5 ,
  },
  discountTitle:{
    resizeMode:'contain',
    width:BaseComponent.W*69/375 ,
    marginLeft:BaseComponent.W*153/375 ,
    height:19,
    marginTop:17.5 ,
    marginBottom:17.5 ,
  },
  dailyUpdateIntroduce:{
    width:'100%',
    height:200
  },
  updataShangp:{
    flex:1 ,
    backgroundColor:'white',
  },
  dailyUpdateSlideImg:{
    width:BaseComponent.W/4,
    height:100 ,
    borderWidth:0.5,
    borderColor:'#f6f6f6' ,
    marginLeft:10 ,
    marginTop:10 ,
    marginBottom:0
  },
  dayNew:{
    flex:1 ,
    backgroundColor:'white'
  },
  img7:{
    resizeMode:'contain',
    width:'20%' ,
    marginLeft:'40%' ,
    marginTop:10 ,
    marginBottom:10 ,
  },
  shangp:{
    flex:1 ,
    backgroundColor:'white',
  },
  jieshao:{
    width:BaseComponent.W*125/375,
    margin:10 ,
    marginLeft:BaseComponent.W*0/375,
    marginTop:0
  },
  goodsPicture:{
    width:BaseComponent.W*130/375,
    height:BaseComponent.W*130/375,
    borderWidth:0.5,
    borderColor:'#f6f6f6' ,
    margin:5 ,
    marginTop:0
  },
  jieshaotxt:{
    fontSize:12,
    color:'#3A3A3A',
    marginLeft:10
  },
  show:{
    flexDirection:'row',
    margin:10 ,
    marginRight: 0 ,
    marginBottom:3,
    justifyContent:'space-between'
   },
  jieshaotxt1:{
    color:'#FF407E' ,
    fontSize:12 ,
  },
  jieshaotxt2:{
    color:'#999999' ,
    fontSize: 9 ,
    marginTop:1.5 ,
    marginLeft:5,
    textDecorationLine:'line-through'
  },
  img11:{
    width:BaseComponent.W/18,
    height:BaseComponent.W/18,
    marginTop:-5
  },
  shoppingCart:{
    width:BaseComponent.W*25/375,
    height:BaseComponent.W*25/375,
    bottom:7
  },
  specialEventsTitle:{
    width:BaseComponent.W/4 ,
    // width:"100%",
    resizeMode:'contain'
  },
  rxShangp:{
    flex:1 ,        
    backgroundColor:'white' ,
    flexDirection:'row' ,
    flexWrap:'wrap' ,
    justifyContent:'space-around'
  },
  jieshao1:{
    width:BaseComponent.W/2,
    flexDirection:'column' ,        
  },
  jieshaotxt111:{
    fontSize:15 ,
    fontWeight:'bold',
    margin:7 ,
    marginLeft:0,
    color:'#FF407E',
    width:"100%",
  },
  jieshaotxt121:{
    fontSize:11 ,
    margin:7 ,
    color:'#999999' ,
    textDecorationLine: 'line-through',
    marginLeft:BaseComponent.W*3/375,
    marginTop:BaseComponent.W/40
  },
  jieshaotxt11:{
    fontSize:13 ,
    margin:7 ,
    color:'#3A3A3A'      
  },
  img101:{
    width:BaseComponent.W*170/375,
    height:170 ,
    borderWidth:0.5,
    borderColor:'#f0f0f0' ,
    margin:7       
  },
  img111:{
    width:BaseComponent.W/13,
    height:BaseComponent.W/13,
    marginLeft:BaseComponent.W*45/375,
  },
  money:{
    flexDirection:'row',
    margin:10,
    marginTop:0,
  },
});

export default class Home extends BaseComponent{

  constructor(props){
    super(props);

    let thiz = this;

    /**
     * 如果是android系统，默认先跳转到启动图页面
     */
    if(BaseComponent.OS=="android"){
      this.navigate("Launcher");
    }


    // 维护一个页面栈
    this.state = {
      pageStack:{},
      visible:false,

      banners:[],// banner图
      blocks:[],// 栏目模块
      indexBlocks:[],// 模块
      hotGoods:{},// 热销商品
      cates:{},// 分类
      hasNext:false,
      isRefreshing:false,// 是否正在刷新

      isHeadFocus:false,// 是否聚焦头部
      netError:false,//true表示网络错误
      // 是否显示启动图和引导页
      isShowLaunch:true,

      // ios引导页是否可见
      iosGuideVisible:false,

      // 是否显示app更新弹窗
      showUpdate:false,
      // 升级信息
      updateInfo:{},

      // 是否正在下载
      isDownloading:false,
      // 下载进度
      downloadProgress:0,

      // 升级状态文本
      updateStateTxt:"立即更新",

      // 具体升级信息
      infoArr:[],

      // 下载任务
      task:null,

      // 构建的图片缓存字典
      remoteImgs:{

      },
      localCaches:{

      },
    }
    this.hotCurPage = 0;// 热销商品当前页码
    this.isHotLoading = false;// 热销商品是否正在加载

    /**
     * 如果是ios系统，先判断是否第一次使用app
     */
    if(BaseComponent.OS=="ios"){
      thiz.T.load("isFirst",function(ret){
        
        if(!ret){
          thiz.log("--------ios_isFirst--------","YES");
          // thiz.state.iosGuideVisible = true;
          thiz.setState({
            iosGuideVisible:true,
          });
        }else{
          thiz.log("--------ios_isFirst--------","NO");
        }

      });
    }

    // 加载图片缓存
    thiz.T.load("localCaches",function(ret,err){
      thiz.log("--------localCaches--------",ret);
      if(ret){
        thiz.setState({
          localCaches:ret,
        });
      }

    });

  }

  // 首页图片数据缓存
  doCache(){
    let thiz = this;
    
    // 去除无效的图片（所有数据加载完成后）
    thiz.log("--------doCache_remote--------",thiz.state.remoteImgs);
    // for(let key in thiz.state.localCaches){
    //   if(!thiz.state.remoteImgs[key]){// 远程图片中不包含本地缓存的，则需要清除本地的对应图片
    //     // 删除对应图片
    //     thiz.deleteFile(thiz.state.localCaches[key].localPath,function(ret,err){
    //       if(ret){
    //         thiz.log("--------deleteFile--------"+ret.msg);
    //       }
    //     });
    //     delete thiz.state.localCaches[key];
    //   }
    // }

    // 延时缓存到本地
    setTimeout(function(){
      thiz.log("--------doCache_local--------",thiz.state.localCaches);
      thiz.T.save("localCaches",thiz.state.localCaches);
    },200);

  };

  setCache(data,interval){

    let thiz = this;

    for(let i=0;i<data.length;i++){

      let remoteImgs = JSON.parse(JSON.stringify(thiz.state.remoteImgs));
      let localCaches = JSON.parse(JSON.stringify(thiz.state.localCaches));

      let remotePath = data[i].imageAttachment.resourceFullAddress;
      let rpSplit = remotePath.split("/");
      let fileName  = rpSplit[rpSplit.length - 1];

      remoteImgs[fileName] = {
        remotePath:remotePath
      };

      if(i==data.length-1){
        thiz.setState({
          remoteImgs:remoteImgs,
        });
      }

      if(!localCaches[fileName] || !localCaches[fileName].localPath){// 本地没有该图片就缓存

        localCaches[fileName] = {
          remotePath:remotePath,
          localPath:""
        };

        if(i==data.length-1){
          thiz.setState({
            localCaches:localCaches,
          });
        }        

        thiz.log("--------fuck_no_pic--------","fuck_no_pic");
        setTimeout(function(){
          thiz.download(remotePath,function(ret,err){
            if(ret){
              let localCachesCur = JSON.parse(JSON.stringify(thiz.state.localCaches));
              // 每下完一个再设置一下
              localCachesCur[fileName] = {
                remotePath:remotePath,
                localPath:ret.saveFinalPath
              };
              thiz.setState({
                localCaches:localCachesCur,
              });
            }
          });  
        },interval*i);
      }

    }

  }


  // 加载数据
  loadData(callback){
    let thiz = this;

    // 先加载banner
    thiz.setState({
          visible:true
    });
    thiz.request("bannerGroup/getByCodes",{
          method:"POST",
          data:{
            codes:[]
          },
          success:function(ret){
                if(ret.respCode=="00"){
                      // 关闭商品属性选择
                      thiz.setState({
                            banners:ret.data,
                      });

                      thiz.setCache(ret.data[0].bannerGroupItems,5);
                      thiz.setCache(ret.data[1].bannerGroupItems,10);

                      // 延时缓存
                      // setTimeout(function(){
                      //   thiz.doCache();
                      // },5000);
                      
                }
          },
          error:function(err){
                thiz.toast(err.msg);
                thiz.setState({
                      visible:false,
                      isRefreshing:false,
                });
                if(err.code&&err.code==200){
                    thiz.setState({netError:true});
                } 
          }
    });

    // 获取分类
    thiz.request("goodsGroup/getPageToShowHomePage",{
        method:"POST",
        data:{
          page:{
            pageSize:5,
            pageNum:1
          }
        },
        success:function(ret){
              if(ret.respCode=="00"){

                thiz.setState({
                  cates:ret.data,
                });

                thiz.setCache(ret.data.records,20);

                // 延时缓存
                // setTimeout(function(){
                //   thiz.doCache();
                // },5000);

              }
        },
        error:function(err){
              thiz.toast(err.msg);
              thiz.setState({
                    visible:false,
                    isRefreshing:false,
              });
              if(err.code&&err.code==200){
                  thiz.setState({netError:true});
              } 
        }
    });

    // 获取动态模块（可能包括国家馆信息）
    thiz.request("dynamicModule/getByCodes",{
          method:"POST",
          data:{
            // 加载首页的动态模块
            codes:["INDEX"]
          },
          success:function(ret){
                if(ret.respCode=="00"){
                      thiz.setState({
                            indexBlocks:ret.data,
                      });

                      thiz.setCache(ret.data[0].dynamicModuleGroupItems,25);

                      // 延时缓存
                      // setTimeout(function(){
                      //   thiz.doCache();
                      // },5000);
                }
          },
          error:function(err){
                thiz.toast(err.msg);
                thiz.setState({
                      visible:false,
                      isRefreshing:false,
                });
                if(callback){
                  callback();
                }
          }
    });

    // 加载首页板块的数据
    // thiz.request("columnGroup/getPage",{
    thiz.request("columnGroup/getPageRefSpu",{
          method:"POST",
          data:{
            page:{}
          },
          success:function(ret){
                if(ret.respCode=="00"){
                      thiz.setState({
                            blocks:ret.data.records,
                      });

                      // 筛选出所有的图片
                      let allImgs = [];
                      for(let i=0;i<ret.data.records.length;i++){
                        allImgs.push({
                          imageAttachment:ret.data.records[i].imageAttachment,
                        });
                        for(let ii=0;ii<ret.data.records[i].columnGroupItems.length;ii++){
                          allImgs.push({
                            imageAttachment:ret.data.records[i].columnGroupItems[ii].imageAttachment,
                          });
                        }
                      }

                      thiz.setCache(allImgs,30);

                      // 延时缓存
                      setTimeout(function(){
                        thiz.doCache();
                      },8000);
                }
          },
          error:function(err){
                thiz.toast(err.msg);
                thiz.setState({
                      visible:false,
                      isRefreshing:false,
                });
                if(callback){
                  callback();
                }

                // 定时5秒后备份数据到本地
                
          }
    });

    // 加载热销商品
    thiz.isHotLoading = true;
    thiz.request("spu/getOnSalePage",{
        method:"POST",
        data:{
          hot:true,
          page:{
            pageSize:BaseComponent.PAGE_SIZE,
            pageNumber:1
          }
        },
        success:function(ret){
              if(ret.respCode=="00"){
                    thiz.setState({
                          visible:false
                    });
                    // if(ret.data.spuPage.records&&ret.data.spuPage.records.length>0){
                    if(ret.data.spuPage.records){
                      thiz.setState({
                            hotGoods:ret.data.spuPage,
                            hasNext:ret.data.spuPage.hasNext
                      });

                      thiz.hotCurPage = ret.data.spuPage.pageNumber;
                      thiz.log("--------loadData_hotCurPage--------",thiz.hotCurPage);
                    }

              }

              thiz.isHotLoading = false;

              if(callback){
                callback();
              }

        },
        error:function(err){
              thiz.toast(err.msg);
              thiz.setState({
                    visible:false,
                    isRefreshing:false,
              });

              thiz.isHotLoading = false;

              if(callback){
                callback();
              }
        }
    });

  };

  getShowPath(remotePath){
    let thiz = this;
    let showPath = remotePath;

    if(remotePath){
      // 获取图片的名称
      let rpSplit = remotePath.split("/");
      let key = rpSplit[rpSplit.length - 1];
      let isLocal = thiz.state.localCaches[key]&&thiz.state.localCaches[key].localPath?true:false;
      
      if(isLocal){
        thiz.log("--------加载本地图片--------","加载本地图片");
        if(BaseComponent.OS=="android"){
          showPath = "file://"+thiz.state.localCaches[key].localPath;
        }else{
          showPath = thiz.state.localCaches[key].localPath;
        }
      }
    }

    return showPath;

  }

  //轮播模板函数
  //  source={{uri:value.imageAttachment.resourceFullAddress}}
  getBannerTpl(data){
    let thiz = this;
    if(data&&data.length>0){

      return (

        <Swiper
            ref="swiper"
            style={{height:BaseComponent.W*376/750}}
            key={data.length}
            autoplay={true}
            autoplayTimeout={5}
            horizontal={true}
            paginationStyle={{bottom: 5}}
            showsButtons={false}
            showsPagination={true}
            dot={<View style={{width:8, height:4,borderRadius:5,backgroundColor:'#F0F0F0', marginLeft:10
              }}/>}
            activeDot={<View style={{width:8, height:4,borderRadius:5,backgroundColor:'#FDD17A', marginLeft:10
            }}/>}
        >
          {
            data.map(function(value,index){

              // 获取图片的显示路径
              let showPath = thiz.getShowPath(value.imageAttachment.resourceFullAddress);

              return (
                <TouchableWithoutFeedback onPress={()=>thiz.goPage("BannerTxtImg",{title:value.title,data:value})}>
                  <View>
                    <Image style={style.lunbotuimg} source={{uri:showPath}}/>
                  </View>
                </TouchableWithoutFeedback>
              );

            })
          }

        </Swiper>

      );

    }else{
      return (

        <Swiper
            ref="swiper"
            style={{height:BaseComponent.W*376/750}}
            key={1}
            autoplay={true}
            horizontal={true}
            paginationStyle={{bottom: 5}}
            showsButtons={false}
            showsPagination={true}
            dot={<View style={{width:8, height:4,borderRadius:5,backgroundColor:'#F0F0F0', marginLeft:10
              }}/>}
            activeDot={<View style={{width:8, height:4,borderRadius:5,backgroundColor:'#FDD17A', marginLeft:10
            }}/>}
         >
          <View>
            <Image style={style.lunbotuimg}/>
          </View>
        </Swiper>

      );
    }
  }

  // 跳转分类详情
  onclick(value){
    let thiz = this;
    if(value){
      let pageParams={
        from:"index",
        interParams:{
          goodsGroupIds:[value.id]
        }
      }
      thiz.goPage("FenleiXiangqing",pageParams);
    }
  }

  // 分类模板函数
  getCateTpl(data){
    let thiz = this;
    return (
      <View style={{display:data.length>0?"flex":"none",flex:1,flexDirection:"row",
      justifyContent:"space-around",
      alignItems:"flex-start",
      paddingTop:28,paddingBottom:18,
      backgroundColor:"white"}}>
        {
          data.map(function(value,index){

            // 获取图片的显示路径
            let showPath = thiz.getShowPath(value.imageAttachment.resourceFullAddress);

            return (
              <TouchableWithoutFeedback onPress={()=> thiz.onclick(value)}>
              <View style={{textAlign:"center"}}>               
                  <Image source={{uri:showPath}} style={{width:BaseComponent.W*0.127*0.9,height:BaseComponent.W*0.127*0.9}}/>
                  <Text style={[{textAlign:"center",color:"#636363",marginTop:16},St.fontSmall]}>{value.name}</Text>               
              </View>
              </TouchableWithoutFeedback>
            );
          })
        }
      </View>
    );
  }

  // 广告模块函数
  getAdsTpl(data){
    let thiz = this;
    thiz.log("--------getAdsTpl--------","getAdsTpl");
    if(data&&data.length>0){

      // 获取图片的显示路径
      let showPath0 = thiz.getShowPath(data[0].imageAttachment.resourceFullAddress);
      let showPath1 = data[1]&&data[1].imageAttachment&&data[1].imageAttachment.resourceFullAddress?thiz.getShowPath(data[1].imageAttachment.resourceFullAddress):null;

      return (
        <View style={style.ad}>
             <View style={style.ad_1}>

                  <MyImage onPress={()=>{thiz.goPage("BannerTxtImg",{title:data[0].title,data:data[0]})}} style={{borderRadius:8}} width={BaseComponent.W/2-12} url={showPath0}/>
                  {
                    data[1]&&data[1].imageAttachment&&data[1].imageAttachment.resourceFullAddress?(
                      <MyImage onPress={()=>{thiz.goPage("BannerTxtImg",{title:data[1].title,data:data[1]})}} style={{borderRadius:8}} width={BaseComponent.W/2-12} url={showPath1}/> 
                    ):null
                  }
                  
             </View>    
        </View>
      );
    }
  }

  // 处理点击动态模块
  onPressDynamic(id){
    let thiz = this;
    thiz.log("--------onPressDynamic--------","onPressDynamic");
    let item = thiz.state.indexBlocks[0].dynamicModuleGroupItems[id];

    // 跳转到商品分类列表
    if(item.skipType=="WINDOW"&&(item.pageType=="GoodsList"||item.pageType=="GOODS_LIST")){
      thiz.log("--------GoodsList--------","GoodsList");
      let interParams = JSON.parse(item.interfaceParams);
      let pageParams = {
        interParams:interParams,
        from:"index",
      }
      thiz.log("--------pageParams--------",pageParams);
      thiz.goPage("FenleiXiangqing",pageParams);
      return;
    }

    // 跳转到国家馆
    if(item.skipType=="WINDOW"&&(item.pageType=="CountryPavilion"||item.pageType=="COUNTRY_PAVILION")){
      thiz.log("--------CountryPavilion--------","CountryPavilion");
      thiz.goPage("Country",{title:"国家馆"});
      return;
    }

  }

  //功能模块函数
  getSortTpl(data){
    let thiz =  this;
    // thiz.log("--------动态模块--------",data);
    if(data.length>0){

      let showPath0 = thiz.getShowPath(data[0].dynamicModuleGroupItems[0].imageAttachment.resourceFullAddress);
      let showPath1 = thiz.getShowPath(data[0].dynamicModuleGroupItems[1].imageAttachment.resourceFullAddress);
      // thiz.log("--------dynamic_showPath1--------",showPath1);
      let showPath2 = thiz.getShowPath(data[0].dynamicModuleGroupItems[2].imageAttachment.resourceFullAddress);
      let showPath3 = thiz.getShowPath(data[0].dynamicModuleGroupItems[3].imageAttachment.resourceFullAddress);
      let showPath4 = thiz.getShowPath(data[0].dynamicModuleGroupItems[4].imageAttachment.resourceFullAddress);

      return (
        <View style={{backgroundColor:"white",marginBottom:10,marginTop:thiz.state.banners&&thiz.state.banners.length>0&&thiz.state.banners[1]&&thiz.state.banners[1].bannerGroupItems&&thiz.state.banners[1].bannerGroupItems.length>0?0:10,}}>

          <View style={style.timeCountry}>

            <TouchableWithoutFeedback>
              
              <MyImage onPress={()=>{
                thiz.onPressDynamic(0);
                // thiz.toast("我是提示");
              }} style={{backgroundColor:'white'}} width={BaseComponent.W/2} url={showPath0}/>
            </TouchableWithoutFeedback>

            <View style={style.line1}></View>

            <TouchableWithoutFeedback>
              
              <MyImage onPress={()=>{thiz.onPressDynamic(1)}} style={{backgroundColor:'white'}} width={BaseComponent.W/2} url={showPath1}/>
            </TouchableWithoutFeedback>    

          </View>

          
          <View style={style.line2}></View>
          <View style={style.naifen}>
            
            <TouchableWithoutFeedback>
              <MyImage onPress={()=>{thiz.onPressDynamic(2)}} width={BaseComponent.W/3} style={{backgroundColor:'white'}} url={showPath2}/>
            </TouchableWithoutFeedback>
            
            <View style={style.line1}></View>
            
            
            <TouchableWithoutFeedback>
              <MyImage onPress={()=>{thiz.onPressDynamic(3)}} width={BaseComponent.W/3} style={{backgroundColor:'white'}} url={showPath3}/>
            </TouchableWithoutFeedback>
            
            <View style={style.line1}></View>
            
            <TouchableWithoutFeedback>
              <MyImage onPress={()=>{thiz.onPressDynamic(4)}} width={BaseComponent.W/3} style={{backgroundColor:'white'}} url={showPath4}/>
            </TouchableWithoutFeedback>  

          
          </View>
          <View style={style.line2}></View>
        </View>
      );
    }else{
      return (
        <View></View>
      );
    }

  }

  //推荐模块函数
  getRecommendTpl(data){
    return (
      <View style={{backgroundColor:'#F0F0F0'}}>
        <TouchableWithoutFeedback>
          <Image style={style.recommendImg} source={Imgs.recommend}/>
        </TouchableWithoutFeedback>
      </View>

    );
  }
  //每日上新模块函数
  getDailyUpdateTpl(data){
    return (
      <View>
        <View style={style.dayNew}>           
            <Image style={style.dailyUpdateTitle} source={Imgs.dayNew}></Image>
        </View>
        <View>
          <TouchableWithoutFeedback onPress={()=> this.navigate('DailyUpdate',{title:'每日上新'})}>
            <Image style={style.dailyUpdateIntroduce} source={Imgs.dayNew1}/>
          </TouchableWithoutFeedback>
        </View>
        <View style={style.updataShangp}>           
          <ScrollView
          horizontal={true} 
          showsHorizontalScrollIndicator={false}  
          >           
            {
              data.map(function(value,index){
                return (
                  <View>
                    <Image style={style.dailyUpdateSlideImg} source={value.img}/>
                  </View>
                  );
              })
            }              
          </ScrollView>
        </View>
      </View>  
    );
  }
   //限时折扣模块函数
  getDiscountTpl(data){
    return (
      <View>
        <View style={style.dayNew}>           
          <Image style={style.discountTitle} source={Imgs.timeDiscount}></Image>  
        </View>
        <View style={style.shangp}>           
          <ScrollView
            horizontal={true} 
            showsHorizontalScrollIndicator={false}>
             {
                data.map(function(value,index){
                  return (
                    <View style={style.jieshao}>      
                      <Image style={style.goodsPicture} source={value.img}/>
                      <View>
                        <Text style={style.jieshaotxt} numberOfLines={2}>
                          {value.introduce}
                        </Text>
                      </View>
                      <View style={style.show}>
                        <View style={{flexDirection:'row'}}>
                          <Text style={style.jieshaotxt1}>
                            {value.price}
                          </Text>     
                          <Text style={style.jieshaotxt2}>
                            {value.discount1}
                          </Text>
                        </View>  
                        <Image style={style.shoppingCart} source={value.img1}/>
                      </View>
                    </View>
                    );
                })
              }                  
          </ScrollView>  
        </View>  
      </View>  
    );
  }

  //处理点击每日上新等
  // goBlock(index,subindex){
  //   let thiz = this;
  //   let item = thiz.state.blocks[index];
  //   thiz.log("--------goBlock--------","goBlock");
  //   // 每日上新
  //   if(item.title=="每日上新"){
  //     thiz.goPage("DailyUpdate",{title:"每日上新"});
  //   }
  // }

  // 处理点击每日上新等的单个商品
  goGoodsDetail(value){
    let thiz = this;
    thiz.log("--------goGoodsDetail--------",value);
    if(value){
      // 跳转到商品详情
      thiz.goPage("GoodsXiangQing",{id:value.id});
    }
  }

  // 点击每日上新等的查看更多
  showMore(value){
    let thiz = this;
    thiz.log("--------showMore--------",value);

    // 判断是否是跳转到每日上新
    if(value.type=="EVERYDAY_NEW"){
      thiz.goPage("DailyUpdate",{title:value.title,id:value.id});
      return;
    }

    // 跳转到商品分类详情
    if(value){
      let pageParams = {
        from:"index",
        interParams:{
          columnGroupItemId:value.id
        }
      }
      thiz.goPage("FenleiXiangqing",pageParams);
    }
  }

  //精选活动一
  getSpecialEventsFirstTpl(data){
    let thiz = this;
    if(data&&data.length>0){

      return data.map(function(vvalue,iindex){

        let showPath0 = thiz.getShowPath(vvalue.imageAttachment.resourceFullAddress);

        return (
          <View>
            <View style={{width:"100%"}}>
              <MyImage width={BaseComponent.W} url={showPath0}/>
            </View>
            {
              vvalue.columnGroupItems?vvalue.columnGroupItems.map(function(value,index){

                let showPath1 = thiz.getShowPath(value.imageAttachment.resourceFullAddress);

                return (
                  <View>
                    
                    <View style={St.worldBuy}>
                      <TouchableWithoutFeedback>
                        <MyImage onPress={()=>{
                        // if(vvalue.code=="EVERYDAY_NEW"){
                          // alert("EVERYDAY_NEW");
                          thiz.showMore({id:value.id,title:value.title,type:vvalue.code});
                        // }
                      }} width={BaseComponent.W} url={showPath1}/>
                      </TouchableWithoutFeedback>
                    </View>
                    
                    <View style={St.worldBuyShangp}>
                      <ScrollView
                       horizontal={true}
                       showsHorizontalScrollIndicator={false}>
                       {
                          value.spuPage.records?value.spuPage.records.map(function(v,id){
                            return (
                              <View>
                                  <TouchableWithoutFeedback onPress={()=>{thiz.goGoodsDetail(v)}}>
                                  <Image style={St.img14} source={{uri:v.attachment.resourceFullAddress}}/>
                                  </TouchableWithoutFeedback>
                              </View>
                            );
                          }):null
                        }
                        {/*更多*/}
                        <View style={{display:value.spuPage.hasNext&&value.spuPage.records&&value.spuPage.records.length>4?"flex":"none"}}>
                            <TouchableWithoutFeedback onPress={()=>{thiz.showMore({id:value.id,title:value.title,type:vvalue.code})}}>
                            <Image style={St.img14} source={require("../image/home/home_more.png")}/>
                            </TouchableWithoutFeedback>
                        </View>           
                      </ScrollView>
                    </View>

                    {/*分割线*/}
                    <View style={{width:"100%",height:10,backgroundColor:"#f0f0f0",display:index<vvalue.columnGroupItems.length-1?"flex":"none"}}></View>
                  </View>
                );
              }):null
            }
          </View>
        )

      })

    }
    
  }

  //精选活动二
  getSpecialEventsSecondTpl(data){
    return (
      <View>
        <View style={St.hang3}>
        </View>
        <View style={St.worldBuy}>
          <Image style={St.SpecialEventsImg} source={Imgs.luna}/>
        </View>
        <View style={St.worldBuyShangp}>
          <ScrollView
           horizontal={true}
           showsHorizontalScrollIndicator={false}
          >
           {
              data.map(function(value,index){
                return (
                  <View>
                    <View style={St.lunaShangp}>
                      <Image style={St.lunaImage} source={value.img}/>
                      <Text style={St.lunatxt} numberOfLines={2}>
                        {value.introduce}
                      </Text>
                      <Text style={St.lunatxt1}>
                        {value.price}
                      </Text>
                    </View>
                  </View>
                  );
              })
            }           
          </ScrollView>
        </View>  
    </View>       
    );
  }
  //精选活动三
  getSpecialEventsThirdTpl(data){
    return (
      <View>
        <View style={St.hang3}>
        </View>
        <View style={St.worldBuy}>
          <Image style={St.SpecialEventsImg} source={Imgs.aptamil}/>
        </View>
        <View style={St.worldBuyShangp}>
          <ScrollView
           horizontal={true}
           showsHorizontalScrollIndicator={false}
          >
           {
              data.map(function(value,index){
                return (
                  <View>
                    <View style={St.lunaShangp}>
                      <Image style={St.lunaImage} source={value.img}/>
                      <Text style={St.lunatxt} numberOfLines={2}>
                        {value.introduce}
                      </Text>
                      <Text style={St.lunatxt1}>
                        {value.price}
                      </Text>
                    </View>
                  </View>
                  );
              })
            }           
          </ScrollView>
        </View>  
      </View>       
    );
  }
  //精选活动四
  getSpecialEventsFourthTpl(data){
    return (
      <View>
        <View style={St.hang3}>
        </View>
        <View style={St.worldBuy}>
          <Image style={St.SpecialEventsImg} source={Imgs.swisse}/>
        </View>
        <View style={St.worldBuyShangp}>
          <ScrollView
           horizontal={true}
           showsHorizontalScrollIndicator={false}
          >
           {
              data.map(function(value,index){
                return (
                  <View>
                    <View style={St.lunaShangp}>
                      <Image style={St.lunaImage} source={value.img}/>
                      <Text style={St.lunatxt} numberOfLines={2}>
                        {value.introduce}
                      </Text>
                      <Text style={St.lunatxt1}>
                        {value.price}
                      </Text>
                    </View>
                  </View>
                  );
              })
            }           
          </ScrollView>
        </View>  
      </View>       
    );
  }

  // 加入购物车
  addCart(value){
    let thiz = this;
    thiz.log("--------addCart--------",value);
    if(value){
      // thiz.setState({
      //       visible:true
      // });
      // thiz.request("shoppingCar/addGoods",{
      //       method:"POST",
      //       data:{
      //             num:1,
      //             skuId:value.id
      //       },
      //       success:function(ret){
      //             thiz.setState({
      //                   visible:false
      //             });
      //             if(ret.respCode=="00"){
      //                   thiz.log("--------加入购物车成功--------","加入购物车成功");
      //                   thiz.toast("加入购物车成功");
      //                   // 关闭商品属性选择
      //                   thiz.setState({
      //                         visible:false,
      //                   });

      //                   // 发送添加购物车成功消息
      //                   thiz.emit("add_cart_success");
      //             }
      //       },
      //       error:function(err){
      //             thiz.toast(err.msg);
      //             thiz.setState({
      //                   visible:false,
      //             });
      //       }
      // });

      // 改为跳转商品详情
      thiz.goPage("GoodsXiangQing",{id:value.id,operation:"show_specs"});
      
    }
  }

  //热销商品
  getHotProductTpl(data){
    let thiz = this;
    if(data&&data.length>0){
      return (
        <View>
          <View style={St.hang2}>
            <Image style={style.specialEventsTitle} source={Imgs.hotProduct}/>
          </View>
          <View>

            <View style={style.rxShangp}>
              {

                data.map(function(v,id){
                  return (

                    <View>
                      <TouchableWithoutFeedback onPress={()=>{thiz.goGoodsDetail(v)}}>
                        <View style={[style.jieshao1,{position:"relative"}]}>

                          <Image style={style.img101} source={{uri:v.attachment.resourceFullAddress}}></Image>

                          <View style={{height:46}}>
                            <Text style={style.jieshaotxt11} numberOfLines={2}>
                               {v.name}
                            </Text>
                          </View>

                          <View style={[style.money,{width:"100%",position:"relative"}]}>
                            <Text style={style.jieshaotxt111}>
                              ￥{parseFloat(v.buyUnitPrice).toFixed(2)}
                            </Text>

                            {/*<Text style={style.jieshaotxt121}>
                              ￥{v.marketPrice}
                            </Text>*/}

                            {/*<TouchableWithoutFeedback onPress={()=>{thiz.addCart(v)}}>                
                            <Image style={[style.img111,{position:"absolute",right:BaseComponent.W/13*0.7,}]} source={require("../image/home/shoppingCart.png")}></Image>
                            </TouchableWithoutFeedback>*/}
                          </View>

                        </View>
                      </TouchableWithoutFeedback>

                    </View>
                  );
                })

              }

              {/*空白占位*/}
              {
                data.length%2==1?(

                    <View style={{opacity:0}}>

                        <View style={style.jieshao1}>

                          <Image style={style.img101}></Image>

                          <View>
                            <Text style={style.jieshaotxt11} numberOfLines={2}>
                               未知
                            </Text>
                          </View>

                          <View style={style.money}>
                            <Text style={style.jieshaotxt111}>
                              未知
                            </Text>     
                            <Text style={style.jieshaotxt121}>
                              未知
                            </Text>                        
                            <Image style={style.img111} source={require("../image/home/shoppingCart.png")}></Image>
                          </View>

                        </View>

                    </View>

                ):null
              }

            </View>

          </View>
        </View>
      )
    }
    
  }

  // 每一步滚动回调
  onScrollStep(e){
    let thiz = this;
    thiz.log("--------onScrollStep--------","onScrollStep");

    var offsetY = e.nativeEvent.contentOffset.y; //滑动距离
    var contentSizeHeight = e.nativeEvent.contentSize.height; //scrollView contentSize高度
    var oriageScrollHeight = e.nativeEvent.layoutMeasurement.height; //scrollView高度

    // 判断是否聚焦头部
    let headH = BaseComponent.H*0.05+ti.select({ios:16,android:22});
    thiz.log("--------offsetY--------",offsetY);
    thiz.log("--------headH--------",headH);
    if(offsetY<headH){
      thiz.setState({
        isHeadFocus:false
      });
    }else{
      thiz.setState({
        isHeadFocus:true
      });
    }

  }

  // 滚动结束回调
  onScroll(e){
    let thiz = this;
    thiz.log("--------onScroll--------",e.nativeEvent);

    var offsetY = e.nativeEvent.contentOffset.y; //滑动距离
    var contentSizeHeight = e.nativeEvent.contentSize.height; //scrollView contentSize高度
    var oriageScrollHeight = e.nativeEvent.layoutMeasurement.height; //scrollView高度

    if (Math.abs((offsetY + oriageScrollHeight) - contentSizeHeight)<5){
        thiz.log("--------onScroll--------","滚动到了底部");
        // 判断是否还有更多商品
        if(thiz.state.hotGoods&&thiz.state.hasNext){
          thiz.log("--------thiz.isHotLoading--------",thiz.isHotLoading);
          // 判断是否正在加载
          if(!thiz.isHotLoading){
            // 加载热销商品
            thiz.isHotLoading = true;
            thiz.log("--------upload_hotCurPage111111--------",thiz.hotCurPage);

            thiz.setState({
                  visible:true,
            });
            thiz.request("spu/getOnSalePage",{
                method:"POST",
                data:{
                  hot:true,
                  page:{
                    pageSize:BaseComponent.PAGE_SIZE,
                    pageNumber:thiz.hotCurPage+1
                  }
                },
                success:function(ret){
                      thiz.log("--------upload_ret--------",ret);
                      thiz.isHotLoading = false;
                      thiz.setState({
                            visible:false,
                      });
                      if(ret.respCode=="00"){
                            let hotGoods = thiz.state.hotGoods;
                            let items = hotGoods.records;
                            if(items&&ret.data.spuPage.records){
                              if(ret.data.spuPage.records.length>0){
                                for(let i=0;i<ret.data.spuPage.records.length;i++){
                                  items.push(ret.data.spuPage.records[i]);
                                }
                                hotGoods.records = items;
                                thiz.setState({
                                      hotGoods:hotGoods,
                                      hasNext:ret.data.spuPage.hasNext
                                });
                                thiz.hotCurPage = ret.data.spuPage.pageNumber;
                                thiz.log("--------upload_hotCurPage222222--------",thiz.hotCurPage);
                              }
                            }

                      }
                },
                error:function(err){
                      thiz.isHotLoading = false;
                      thiz.toast(err.msg);
                      thiz.setState({
                            visible:false
                      });
                }
            });
          }
        }else{
          // thiz.toast("没有更多商品了");
        }

    }
  }

  // 下拉刷新回调
  onRefresh(){
    let thiz = this;
    thiz.log("--------onRefresh--------","onRefresh");
    thiz.setState({
      isRefreshing:true,
    });

    thiz.hotCurPage = 0;
    thiz.loadData(function(){
      thiz.setState({
        isRefreshing:false,
      });
    })
  }

  // 主要渲染函数
  render() {
    let thiz = this;
    return (
      <View style={[style.wrapper]}>
        {/*状态栏，沉浸式*/}
        <StatusBar translucent={true} backgroundColor={'transparent'} barStyle={'dark-content'}/>

        {/*状态栏占位*/}
        <View style={{backgroundColor:!thiz.state.isHeadFocus?"white":"white",
                      height:thiz.isIphoneX(35,BaseComponent.SH),
                      position:"relative",
                      zIndex:1,
                      opacity:1,}}></View>

        {/*app升级弹窗*/}
        <Modal
          visible={this.state.showUpdate}
          transparent={true}
          animationType={"none"}>

          {/*老的弹窗界面*/}
          <View style={{flex:1,display:"none",justifyContent:"center",alignItems:"center",backgroundColor:"rgba(0,0,0,0.2)"}}>
            <View style={{width:BaseComponent.W*275/375,backgroundColor:'rgba(255, 255, 255,1)',borderRadius:BaseComponent.W*20/375}}>

              <View style={{width:'100%',padding:15,borderBottomWidth:0.5,borderBottomColor:"rgba(189, 189, 189, 1)"}}>
                <Text style={{fontSize:BaseComponent.W*15/375,color:'rgba(34, 34, 34, 1)',textAlign:"center"}}>发现新版本v{thiz.state.updateInfo&&thiz.state.updateInfo.appVersion?thiz.state.updateInfo.appVersion:""}</Text>
              </View>

              {/*升级描述*/}
              <View style={{width:"100%",paddingLeft:10,paddingRight:10,padding:10,maxHeight:BaseComponent.H*0.5,backgroundColor:"white",}}>
                <Text style={{marginBottom:0,width:"100%"}}>{thiz.state.updateInfo&&thiz.state.updateInfo.remark?thiz.state.updateInfo.remark:"暂无修复信息"}</Text>
              </View>

              <View style={{width:'100%',height:0.5,backgroundColor:'rgba(189, 189, 189, 1)'}}></View>

              <View style={{width:'100%',flexDirection:'row',}}>

                <TouchableWithoutFeedback onPress={()=>{
                  // 停止下载
                  if(thiz.state.task){
                    thiz.state.task.stop();
                  }
                  thiz.setState({
                    showUpdate:false,
                  });
                }}> 
                  <View style={{display:thiz.state.updateInfo&&thiz.state.updateInfo.forcedUpdates?"none":"flex",width:"50%",justifyContent:'center',alignItems:'center',padding:15,}}>
                    <Text style={{fontSize:BaseComponent.W*15/375,color:'rgba(94, 94, 94, 1)'}}>取消</Text>
                  </View>
                </TouchableWithoutFeedback> 

                <View style={{width:0.5,height:'100%',backgroundColor:'rgba(189, 189, 189, 1)'}}></View>

                <TouchableWithoutFeedback onPress={()=>{

                  // andorid系统直接下载
                  if(BaseComponent.OS=="android"){

                    // 隐藏显示
                    thiz.setState({
                      showUpdate:false,
                      isDownloading:true,
                    });
                    thiz.log("--------apk_name_time--------",((new Date()).getTime()/1000));
                    thiz.T.download({
                      fileUrl:thiz.state.updateInfo.downloadUrl,
                      fileName:"xyj_v"+thiz.state.updateInfo.appVersion+((new Date()).getTime()/1000)+".apk"},function(ret,err){

                        // 处理启动下载
                        if(ret&&ret.type===0){   
                          if(ret.task){
                            thiz.setState({
                              task:ret.task,
                            });
                          }
                          return;
                        }

                        // 处理下载进度
                        if(ret&&ret.type==1){
                          thiz.setState({
                            // showUpdate:false,
                            // isDownloading:true,
                            downloadProgress:ret.progress,
                          });
                          return;
                        }

                        // 下载完毕
                        if(ret&&ret.type==2){

                          thiz.setState({
                            downloadProgress:1
                          });

                          setTimeout(function(){
                            thiz.setState({
                              isDownloading:false,
                              showUpdate:false,
                            });
                            // 执行安装操作...
                            thiz.log("--------savePath--------",ret.savePath);
                            thiz.T.installApk(ret.savePath);

                          },1000);

                          setTimeout(function(){
                            thiz.setState({
                              isDownloading:false,
                              showUpdate:true,
                              downloadProgress:0
                            });
                          },1100);

                          return;
                        }

                        // 下载出错
                        if(err){
                          thiz.log("--------download_apk_error--------",err);
                          thiz.setState({
                            isDownloading:false,
                            showUpdate:true,
                            updateStateTxt:"重试"
                          });
                          thiz.toast("升级失败");
                        }
                    });

                  }else{// ios系统就跳转appStore
                    thiz.T.toAppStore(BaseComponent.APP_ID);
                  }

                }}> 

                <View style={{width:thiz.state.updateInfo&&thiz.state.updateInfo.forcedUpdates?'100%':"50%",justifyContent:'center',alignItems:'center',padding:15,}}>
                  <Text style={{fontSize:BaseComponent.W*15/375,color:'rgba(16, 98, 255, 1)'}}>{thiz.state.updateStateTxt}</Text>
                </View>
                </TouchableWithoutFeedback>

              </View>

            </View>
          </View>

          {/*新的弹窗界面*/}
          <View style={{flex:1,justifyContent:"center",alignItems:"center",backgroundColor:"rgba(0,0,0,0.5)"}}>

            <View style={{width:BaseComponent.W*255/375,backgroundColor:'transparent',borderRadius:BaseComponent.W*7/375}}>
              {/*顶部图 #2C1C5A*/}
              <View style={{width:BaseComponent.W*255/375,backgroundColor:"transparent"}}>
                <MyImage width={BaseComponent.W*255/375} style={{backgroundColor:"transparent"}} url={require('../image/mine/top.png')}/>
              </View>
              {/*升级描述*/}
              <View style={{borderBottomLeftRadius:BaseComponent.W*7/375,borderBottomRightRadius:BaseComponent.W*7/375,width:BaseComponent.W*255/375,
                paddingTop:15,paddingBottom:15,paddingLeft:40,paddingRight:40,backgroundColor:"white",flexDirection:"column",position:"relative",top:-0.2}}>
                <Text style={{width:"100%",color:"#3a3a3a",fontSize:16,fontWeight:"bold",textAlign:"center"}}>发现新版本{thiz.state.updateInfo&&thiz.state.updateInfo.appVersion?thiz.state.updateInfo.appVersion:""}</Text>

                <View style={{width:"100%",marginTop:10,marginBottom:10,}}>
                  {
                    thiz.state.infoArr?thiz.state.infoArr.map(function(value,index){

                      if(value){
                        return (
                          <View style={{width:"100%",flexDirection:"row"}}>
                            <View style={{width:3,height:3,borderRadius:1.5,backgroundColor:"#FFD074",marginTop:ti.select({android:8,ios:BaseComponent.H*3.4/375}),}}></View>
                            <Text style={{marginLeft:10,fontSize:13,}}>{value}</Text>
                          </View>
                        );
                      }
                      
                    }):null
                  }

                </View> 

                {/*按钮*/}
                <View style={{width:"100%",marginTop:15,marginBottom:10,paddingLeft:10,paddingRight:15,}}>

                  <TouchableWithoutFeedback onPress={()=>{
                    // andorid系统直接下载
                    if(BaseComponent.OS=="android"){

                      // 隐藏显示
                      thiz.setState({
                        showUpdate:false,
                        isDownloading:true,
                      });
                      thiz.log("--------apk_name_time--------",((new Date()).getTime()/1000));
                      thiz.T.download({
                        fileUrl:thiz.state.updateInfo.downloadUrl,
                        fileName:"xyj_v"+thiz.state.updateInfo.appVersion+((new Date()).getTime()/1000)+".apk"},function(ret,err){

                          // 处理启动下载
                          if(ret&&ret.type===0){   
                            if(ret.task){
                              thiz.setState({
                                task:ret.task,
                              });
                            }
                            return;
                          }

                          // 处理下载进度
                          if(ret&&ret.type==1){
                            thiz.setState({
                              // showUpdate:false,
                              // isDownloading:true,
                              downloadProgress:ret.progress,
                            });
                            return;
                          }

                          // 下载完毕
                          if(ret&&ret.type==2){

                            thiz.setState({
                              downloadProgress:1
                            });

                            setTimeout(function(){
                              thiz.setState({
                                isDownloading:false,
                                showUpdate:false,
                              });
                              // 执行安装操作...
                              thiz.log("--------savePath--------",ret.savePath);
                              thiz.T.installApk(ret.savePath);

                            },1000);

                            setTimeout(function(){
                              thiz.setState({
                                isDownloading:false,
                                showUpdate:true,
                                downloadProgress:0
                              });
                            },1100);

                            return;
                          }

                          // 下载出错
                          if(err){
                            thiz.log("--------download_apk_error--------",err);
                            thiz.setState({
                              isDownloading:false,
                              showUpdate:true,
                              updateStateTxt:"重试"
                            });
                            thiz.toast("升级失败");
                          }

                      });

                    }else{// ios系统就跳转appStore
                      thiz.T.toAppStore(BaseComponent.APP_ID);
                    }
                  }}>
                  <View style={{height:34,borderRadius:17,backgroundColor:"#FFD074",alignItems:"center",justifyContent:"center",width:"100%"}}>
                    <Text style={{fontSize:18,}}>{thiz.state.updateStateTxt}</Text>
                  </View>
                  </TouchableWithoutFeedback>

                </View>

                <Text style={{width:"100%",color:"#3a3a3a",textAlign:"center",fontSize:13,}}>建议在WIFI环境下更新</Text>
              </View>
            </View>

            {/*取消按钮*/}
            <View style={{display:thiz.state.updateInfo&&thiz.state.updateInfo.forcedUpdates?"flex":"flex",width:BaseComponent.W*255/375,alignItems:"center",justifyContent:"center",marginTop:35,}}>
              <MyImage url={require('../image/mine/cancel_two.png')} width={30} onPress={()=>{
                // 停止下载
                if(thiz.state.task){
                  thiz.state.task.stop();
                }
                thiz.setState({
                  showUpdate:false,
                });
              }}/>
            </View>

          </View>

        </Modal>

        {/*下载进度弹窗*/}
        <Modal
          visible={this.state.isDownloading}
          transparent={true}
          animationType={"none"}>
          <View style={{flex:1,justifyContent:"center",alignItems:"center",backgroundColor:"rgba(0,0,0,0.2)"}}>
            <View style={{width:BaseComponent.W*275/375,backgroundColor:'white',borderRadius:BaseComponent.W*20/375}}>

              <View style={{width:'100%',padding:15,borderBottomWidth:0,borderBottomColor:"rgba(189, 189, 189, 1)"}}>
                <Text style={{fontSize:BaseComponent.W*15/375,color:'rgba(34, 34, 34, 1)',textAlign:"center"}}>下载中：{parseInt(thiz.state.downloadProgress*100)}%</Text>
              </View>

              {/*下载进度*/}
              <View style={{borderBottomLeftRadius:BaseComponent.W*20/375,borderBottomRightRadius:BaseComponent.W*20/375,width:"100%",paddingLeft:10,paddingRight:10,paddingTop:0,paddingBottom:20,maxHeight:BaseComponent.H*0.5,backgroundColor:"white",}}>
                <View style={{width:"100%",height:14,borderWidth:1,borderColor:"#eeeeee",borderRadius:7}}>
                  {/*进度条*/}
                  <View style={{width:((BaseComponent.W*275/375 - 22)*thiz.state.downloadProgress),height:12,borderRadius:6,backgroundColor:"rgba(16, 98, 255, 1)"}}></View>
                </View>
              </View>

            </View>
          </View>
        </Modal>

        {/*Modal来实现引导页（仅仅ios使用）*/}
        <Modal
          visible={this.state.iosGuideVisible}
          transparent={false}
          animationType={"none"}>
          <Guide></Guide>
        </Modal>
        
        {/*转菊花*/}
        <Modal
          visible={this.state.visible}
          onRequestClose={()=>{this.setState({visible:false})}}
          transparent={true}
          animationType={"fade"}>
          <View style={{backgroundColor:"rgba(0,0,0,0.4)",width:"100%",width:"100%",flex:1,flexDirection:"column",alignItems:"center",justifyContent:"center",position:"relative"}}>
            <View>
                  <ActivityIndicator size="large" color='white'/>
            </View>
          </View>
        </Modal>

  
        {/*顶部导航栏*/}
        <View style={style.navBar}>
          <View style={{width:"100%",height:BaseComponent.H*0.06,
          position:"absolute",top:0,left:0,zIndex:99,
          backgroundColor:!thiz.state.isHeadFocus?"white":"white",
          opacity:1,}}></View>
          <View style={{flex:1,flexDirection:"row",position:"relative",zIndex:100,}}>

            <View style={{width:"12.7%"}}></View>

            <TouchableWithoutFeedback onPress={()=>{
              this.navigate('HomeSearch');
            }}>
            <View style={{
              flexGrow:1,
              alignItems:"center",
              justifyContent:"center",
              borderRadius:24,
              borderColor:!thiz.state.isHeadFocus?"#D4D4D4":"#D4D4D4",
              borderWidth:1,
              paddingTop:15,
              paddingBottom:15,
            }}>
              <Text style={{backgroundColor:"transparent",color:!thiz.state.isHeadFocus?"#8F8F8F":"#8F8F8F",width:"100%",textAlign:"center",fontSize:14,height:ti.select({ios:16,android:20}),}}>请输入商品名，品牌或国家</Text>
            </View>
            </TouchableWithoutFeedback>

            {/*第一期屏蔽消息*/}
            <TouchableWithoutFeedback onPress={()=>{
              // this.navigate("Msgcenter",{title:"消息中心"});
            }}>
            <View style={{
              width:"12.7%",
              alignItems:"center",opacity:0,}}>
              <Image source={!thiz.state.isHeadFocus?require('../image/home/msg.png'):require('../image/home/msg_focus.png')} style={{width:ti.select({ios:22,android:20}),height:ti.select({ios:22,android:20}),marginTop:ti.select({ios:6,android:5}),}}></Image>
            </View>
            </TouchableWithoutFeedback>
          </View>
        </View>
        
        {
          thiz.state.netError?( 
              <View style={{width:BaseComponent.W,height:BaseComponent.H*0.85,justifyContent:'center',alignItems:'center'}}>
                      
                      <Image style={{width:BaseComponent.W*180/375,height:BaseComponent.W*180/375,borderRadius:BaseComponent.W*90/375}} source={require('../image/mine/neterror.png')}></Image>
                      

                      <TouchableWithoutFeedback onPress={()=>{
                        thiz.setState({netError:false});
                        
                        thiz.loadData();
                      }}>
                      <View style={{width:BaseComponent.W*70/375,height:BaseComponent.W*30/375,borderRadius:BaseComponent.W*15/375,borderWidth:0.5,borderColor:'#080808',
                                justifyContent:'center',alignItems:'center',marginTop:BaseComponent.W*15/375}}>
                            <Text style={{fontSize:BaseComponent.W*15/375,color:'#252525'}}>刷新</Text>          
                      </View>
                      </TouchableWithoutFeedback>
                    </View>):(
             
              <View style={{ backgroundColor:"#f0f0f0",
                        position:"absolute",
                        top:BaseComponent.H*0.06+thiz.isIphoneX(35,BaseComponent.SH),
                        width:"100%",
                        height:BaseComponent.H*0.94 - (thiz.isIphoneX(35,BaseComponent.SH)) - 50,
                        
                        paddingBottom:thiz.isIphoneX(35,0)}}>

                {/*主要视图区域*/}
                <ScrollView
                  ref="sv"
                  showsVerticalScrollIndicator={false} 
                  style={{flex:1}} 

                  onMomentumScrollEnd={(e)=>{thiz.onScroll(e)}}

                  onScroll={(e)=>{thiz.onScrollStep(e)}}

                  refreshControl={
                    <RefreshControl
                      refreshing={this.state.isRefreshing}
                      onRefresh={()=>{this.onRefresh()}}
                      colors={['#ff0000', '#00ff00','#0000ff','#3ad564']}
                      progressBackgroundColor="#ffffff"
                    />
                  }>

                  <View style={{width:"100%"}}>
                    {/*轮播*/}
                    {(function(){
                      // this.getBannerTpl(this.state.banners&&this.state.banners.length>0&&this.state.banners[0]?this.state.banners[0].bannerGroupItems:[])
                      let headBanner = [];
                      if(thiz.state.banners){
                        for(let i=0;i<thiz.state.banners.length;i++){
                          if(thiz.state.banners[i].code=="INDEX_HEAD"){
                            headBanner = thiz.state.banners[i].bannerGroupItems;
                          }
                        }
                      }
                      return thiz.getBannerTpl(headBanner);
                    })()}
                  </View>

                  {/*分类*/}
                  {this.getCateTpl(this.state.cates&&this.state.cates.records&&this.state.cates.records.length>0?this.state.cates.records:[])}
                  {/*广告*/}  
                  {
                    (function(){
                      //this.getAdsTpl(this.state.banners&&this.state.banners.length>0?this.state.banners[1].bannerGroupItems:[])
                      let midBanner = [];
                      if(thiz.state.banners){
                        for(let i=0;i<thiz.state.banners.length;i++){
                          if(thiz.state.banners[i].code=="INDEX_MIDDLE"){
                            midBanner = thiz.state.banners[i].bannerGroupItems;
                          }
                        }
                      }
                      return thiz.getAdsTpl(midBanner);
                    })()
                  }
                  {/*动态板块*/}
                  {this.getSortTpl(this.state.indexBlocks&&this.state.indexBlocks.length>0?this.state.indexBlocks:[])}
                  {/*推荐this.getRecommendTpl()*/}
                  {}

                  {/*每日上新*/}
                  {}
                  {/*限时折扣*/}
                  {}
                  
                  {/*分割线*/}
                  <View style={{width:"100%",height:10,backgroundColor:"#f0f0f0",display:"none"}}></View>
                  
                  {/*每日上新、限时折扣、精选活动*/}
                  {this.getSpecialEventsFirstTpl(this.state.blocks&&this.state.blocks.length>0?this.state.blocks:[])}
                  
                  {/*热销商品*/}
                  {
                    this.getHotProductTpl(this.state.hotGoods&&this.state.hotGoods.records?this.state.hotGoods.records:[])
                  }
                  {
                    !thiz.state.hasNext?thiz.nomore({bgcolor:'#F0F0F0'}):null
                  }
                </ScrollView>
              </View>
            )
        }
        

      </View>
    );
  }

  // 界面渲染完毕
  componentDidMount(){
    let thiz = this;
    Linking.getInitialURL().then((url) => {
      if (url) {
        console.log("Initial url is:",url);
        thiz.T.save("isFromWeb",{isFromWeb:'yes'});
      }
    }).catch(err => console.error('An error occurred', err));

    // 全局代理跳转全部订单
    thiz.listen("open_all_order_list",function(){
        thiz.navigate("Wodedingdan",{title:"我的订单",page:0});
    });

    // 监听添加页面
    thiz.listen("add_page",function(ret,err){
      if(ret){
        thiz.setState({
          pageStack:{
            [ret.name]:ret.data,
          }
        });
      }
    });

    // 监听获取page的key
    thiz.listen("get_page",function(ret,err){
      if(ret){
        if(ret.name&&thiz.state.pageStack[ret.name]){
          let msg = {
            data:thiz.state.pageStack[ret.name],
          }
          msg = Object.assign(msg,ret);
          thiz.emit("return_page",msg);
        }
      }
    });

    thiz.listen("click_home",function(){
      thiz.loadData();
    });

    // 监听登录成功
    // thiz.listen("login_success",function(){
    //   thiz.loadData();
    // });

    // 监听退出登录
    // thiz.listen("quit_login",function(){
    //   thiz.loadData();
    // });

    // 监听代理跳转登录页面
    thiz.listen("go_login",function(){
      thiz.goPage("Login");
    });

    

    /**
     * 如果是android系统，需要监听启动页面结束才进行第一次加载数据，不然有转菊花，
     * ios系统，直接加载数据，因为ios用的自身默认的启动页
     */
    if(BaseComponent.OS=="android"){
      thiz.listen("launch_or_guide_over",function(ret){
        thiz.loadData();
      });
    }

    /**
     * 如果是ios系统，先判断是否第一次使用app
     */
    if(BaseComponent.OS=="ios"){
      thiz.T.load("isFirst",function(ret){
        if(ret){
          thiz.loadData();
          thiz.T.load('isFromWeb',function(ret_ret){
            if(ret_ret&&ret_ret.isFromWeb=='yes'){
                thiz.navigate('Register',{title:'注册'});
                thiz.T.remove('isFromWeb');
            }else{
                thiz.navigate("Home");
            }
          })
        }else{
          thiz.listen("launch_or_guide_over",function(){
            thiz.log("--------ios_launch_or_guide_over--------","launch_or_guide_over");
            thiz.setState({
              iosGuideVisible:false,
            });
            thiz.loadData();
            thiz.T.load('isFromWeb',function(ret_ret){
              if(ret_ret&&ret_ret.isFromWeb=='yes'){
                  thiz.navigate('Register',{title:'注册'});
                  thiz.T.remove('isFromWeb');
              }else{
                  thiz.navigate("Home");
              }
            })
          });
        }
      });


    }

    // 最后来检测版本升级，等待转菊花出现，避免出现被遮住的问题
    setTimeout(function(){
      thiz.log("--------checkUpdate_countDown--------","checkUpdate_countDown");
      thiz.checkUpdate(function(ret,err){
        if(ret&&ret.respCode=="00"){
          // 判断是否有更新
          if(ret.data&&ret.data.length>0){
            let vSplits = BaseComponent.V.split(".");
            for(let i=0;i<ret.data.length;i++){
              let item = ret.data[i];
              // 匹配当前系统类型
              thiz.log("--------checkUpdate_item--------",item);
              thiz.log("--------item.deviceType.toLowerCase()--------",item.deviceType.toLowerCase());
              thiz.log("--------BaseComponent.OS--------",BaseComponent.OS);

              if(item.deviceType.toLowerCase()==BaseComponent.OS){

                // 处理升级信息
                let infoSplits = [];
                if(item.remark){
                  infoSplits = item.remark.split(";");
                }

                // 判断是否有新版本
                let nvSplits = item.appVersion.split(".");
                let isUpdate = false;
                
                if(parseInt(vSplits[0])==parseInt(nvSplits[0])){

                  if(parseInt(vSplits[1])==parseInt(nvSplits[1])){

                    if(parseInt(vSplits[2])<parseInt(nvSplits[2])){
                      isUpdate = true;
                    }

                  }else if(parseInt(vSplits[1])<parseInt(nvSplits[1])){
                    isUpdate = true;
                  }

                }else if(parseInt(vSplits[0])<parseInt(nvSplits[0])){
                  isUpdate = true;
                }
                
                thiz.log("--------isUpdate--------",isUpdate);

                if(isUpdate){// 升级
                  // 隐藏转菊花
                  thiz.setState({
                    visible:false,
                    updateInfo:item,
                    infoArr:infoSplits,
                  });

                  // 显示升级弹窗
                  setTimeout(function(){
                    thiz.setState({
                      showUpdate:true,
                    });
                  },500);
                  
                }
                break;
              }

            }
          }
        }
      });
    },200);

  }

}
