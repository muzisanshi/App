/**
 * @name HomeSearch.js
 * @auhor 程浩
 * @date 2018.8.14
 * @desc Home主页的搜索界面，实现商品的搜索功能
 */

import React, {Component} from "react";
import {Text,View,StyleSheet,Dimensions, StatusBar,Platform,Image,TextInput,FlatList,TouchableWithoutFeedback} from "react-native";
import HomeSearchResult from './HomeSearchResult';

 // 导入工具函数库
import T from "../lib/Tools";
// 导入公共样式
import St from "../lib/Style";
// 导入图片资源库
import Imgs from "../lib/Img";
import BaseComponent from './BaseComponent';



// Tools实例
let ti = T.getInstance();

let style = StyleSheet.create({
  wrapper:{
    backgroundColor:"white",
    flex:1,
    flexDirection:"column",
    alignItems:"stretch",
    justifyContent:"flex-start",
    position:"relative",
  },
  statusBar:{
    backgroundColor:"rgb(255,255,255)",
    height:BaseComponent.SH,
    position:"relative",
    zIndex:1,
    opacity:1,
  },
  navBar:{
    backgroundColor:"rgb(255,255,255)",
    height:"7%",
    position:"relative",
    zIndex:1,
    opacity:1,
    borderBottomWidth:1,
    borderBottomColor:"rgb(241,241,241)"
  },
});

console.disableYellowBox=true;

let item=[{	key:'倩碧'},
			{key:'爱他美'},
			{key:'兰蔻'},
			{key:'雅诗兰黛雅诗兰黛雅诗兰黛雅诗兰黛'},
			{key:'喜宝'},
			{key:'A2'},
			{key:'DM'},
			{key:'3M'}];
// let item1=[	{keywords:'SKII'},
// 			{keywords:'药丸面膜药丸面膜'},
// 			{keywords:'牙膏'},
// 			{keywords:'洗脸仪洗脸仪'},
// 			{keywords:'保健品'},
// 			{keywords:'蔓越莓'},
// 			{keywords:'DW'},
// 			{keywords:'咖啡'}];			


export default class HomeSearch extends BaseComponent{
	 constructor(props){
	 	super(props);
	 	this.state={
	 		datasource:[],//最近搜索数据
	 		datasource1:[],//热门搜索数据
	 		ShowClearBtn:true,//是否显示最近搜索的清除按钮，true表示显示
	 	}
	 };
	 componentDidMount(){
	 	var thiz=this;

	 	//获取热门搜索数据
	 	thiz.request("hotKeywords/getPage",{
	 		method:"POST",
	 		data:{page:{pageNumber:1,pageSize:10}},
	 		success:function(ret){
	 			if(ret.respCode=="00")
	 			{
	 				thiz.setState({datasource1:ret.data.records});
	 			}
	 		},
	 		error:function(err){

	 		}
	 	});
	 	//监听最近搜索数据
	 	thiz.listen('recent_records',function(ret){
	 		thiz.RecentlyRecords(ret);
	 		thiz.T.load("RecentlyRecords",function(data){
	 		thiz.log("----------------------------zero--------------------------",data);
	 		if(data===null)
	 		{
	 			thiz.setState({
	 				datasource:[],
	 				ShowClearBtn:false,
	 			});
	 		}
	 		else{
				thiz.setState({
					datasource:data,
					ShowClearBtn:true,
				});
	 		}
	 		
	 	});
	 	});
	 	//加载最近搜索的搜索记录
	 	this.T.load("RecentlyRecords",function(data){
	 		thiz.log("----------------------------zero--------------------------",data);
	 		if(data===null)
	 		{
	 			thiz.setState({
	 				datasource:[],
	 				ShowClearBtn:false,
	 			});
	 		}
	 		else{
				thiz.setState({
					datasource:data,
					ShowClearBtn:true,
				});
	 		}
	 		
	 	});

	 	//监听移除最近搜索数据
	 	thiz.listen('remove_RecentlyRecords',function(ret){
	 		thiz.RecentlyRecords(ret);
	 		thiz.T.load("RecentlyRecords",function(data){
	 		thiz.log("----------------------------zero--------------------------",data);
	 		if(data===null)
	 		{
	 			thiz.setState({
	 				datasource:[],
	 				ShowClearBtn:false,
	 			});
	 		}
	 		else{
				thiz.setState({
					datasource:data,
					ShowClearBtn:true,
				});
	 		}
	 		
	 	});
	 	});
	 	
	 };
	 //保存最近搜索的记录，只保存10条
	 RecentlyRecords=(record)=>{
	 	var thiz=this;
	 	thiz.log('------------------------recent_records-------------------------',record);
	 	//先拿到保存的最近搜索数组，如果不存在，则新建一个数组
	 	thiz.T.load("RecentlyRecords",function(ret,err){
	 		var data = [];
	 		if(ret){
	 			thiz.log("-----------------------------------ret--------------------------",ret);
	 			data = ret;
	 			for(var i=0;i<data.length;i++)
	 			{
	 				if(record.recent_records==data[i])
	 				{
	 					return ;
	 				}
	 			}
	 			data.push(record.recent_records);
	 			if(data.length>10)
	 			{
	 				data=thiz.T.delArr(data,0);
	 			}
	 		}else{
	 			thiz.log("------------------first_data----------------------",data);
	 			data.push(record.recent_records);
	 		}
	 		thiz.T.save("RecentlyRecords",data);
	 		thiz.log("------------------second_data----------------------",data);
	 	});

	 	
	 };
	render(){
		let thiz=this;
		return (	
			 <View style={style.wrapper}>
		        {/*状态栏，沉浸式*/}
		        <StatusBar translucent={true} backgroundColor={'transparent'} barStyle={'dark-content'}/>

		        {/*状态栏占位*/}
		        <View style={{backgroundColor:"rgb(255,255,255)",
							    height:thiz.isIphoneX(30,BaseComponent.SH),
							    position:"relative",
							    zIndex:1,
							    opacity:1,}}></View>
		        {/*顶部导航栏*/}
		        <View style={style.navBar}>
		          <View style={{flex:1,flexDirection:"row",justifyContent:"flex-start"}}>
		            <TouchableWithoutFeedback onPress={()=>this.goBack()}>
		            <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:'center'}}>
		               <Image source={Imgs.back} style={{width:(BaseComponent.W*0.127*0.4),height:(BaseComponent.W*0.127*0.4),marginLeft:BaseComponent.W*0.02}}></Image>
		            </View>
		            </TouchableWithoutFeedback>

		           
		            <View style={{height:ti.select({ios:(BaseComponent.H*0.05),android:(BaseComponent.H*0.054)}),flexGrow:1,backgroundColor:"transparent",
		              marginTop:ti.select({ios:(BaseComponent.H*0.02)/2,android:(BaseComponent.H*0.016)/2}),borderRadius:24,borderColor:"#C7C7C7",
		              borderWidth:1,alignItems:"center",justifyContent:"center"}}>
		              <TextInput style={{width:'100%',padding:0,backgroundColor:"transparent",marginLeft:BaseComponent.W*30/375,color:"#121212"}} placeholderTextColor="#C7C7C7"
		               placeholder="请输入商品名，品牌或国家" underlineColorAndroid='transparent' maxLength={30} onSubmitEditing={(event)=>{
		               		
		               		if(event.nativeEvent.text==""||event.nativeEvent.text.length==null)
		               		{
		               			return ;
		               		}
		               		this.emit('recent_records',{recent_records:event.nativeEvent.text});
		               		this.navigate("FenleiXiangqing",{searchKeywords:event.nativeEvent.text});
		               }}>
		               </TextInput>
		            </View>
		          	
		          	<TouchableWithoutFeedback onPress={()=>{this.goBack();}}>
		            <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>   
		            	<Text style={{color:'rgba(135, 134, 135, 1)'}}>取消</Text>
		            </View>
		            </TouchableWithoutFeedback>
		          </View>
		        </View>

					

					{/*最近搜索*/}
					<View style={styles.search}>

					<View style={{flexDirection:'row',justifyContent:'space-between'}}>
					<Text style={{fontSize:15,marginLeft:Dimensions.get('window').width/29}}>最近搜索</Text>
					<TouchableWithoutFeedback onPress={()=>
						{
							this.T.remove("RecentlyRecords");
							thiz.emit("remove_RecentlyRecords");
						}
					}>
				
					<Text style={{fontSize:14,marginRight:Dimensions.get('window').width/34,color:'#AFAFAF',display:thiz.state.ShowClearBtn?'flex':'none'}}>清除</Text>
					
					</TouchableWithoutFeedback>
					</View>
		            </View>

					<View style={{flexDirection:'row',flexWrap:'wrap'}}>
						
						{

							this.state.datasource.length==0?(<View style={{width:'100%',height:50,justifyContent:'center',alignItems:'center'}}><Text>暂无搜索记录</Text></View>):(
								this.state.datasource.map((item,index)=>{
									
									return (
										<TouchableWithoutFeedback key={index} onPress={()=>this.navigate('FenleiXiangqing',{searchKeywords:item})}>
			 							<View key={index} style={{height:Dimensions.get('window').height/25,
											backgroundColor:'#EDEDED',
											justifyContent:'center',
											alignItems:'center',
											marginLeft:Dimensions.get('window').width/29,
											marginTop:20,
											borderRadius:5,
											width:item.length>3?Dimensions.get('window').width/6+(item.length-3)*Dimensions.get('window').width/25:Dimensions.get('window').width/6}}>
										
			 							<Text key={index} style={{fontSize:15}}>{item}</Text>
			 							
			 							</View>
			 							</TouchableWithoutFeedback>
				 					)
								})
							)	
						}
					</View>

				{/*热门搜索*/}
		            <View style={styles.search}>

		                    <View style={{flexDirection:'row',justifyContent:'space-between'}}>
		                        <Text style={{fontSize:15,marginLeft:Dimensions.get('window').width/29}}>热门搜索</Text>
		                        <TouchableWithoutFeedback onPress={()=>
		                        {
		                            this.setState({
		                                datasource1:[]
		                            })
		                        }
		                        }>
		                            <Text style={{fontSize:14,marginRight:Dimensions.get('window').width/34,color:'#AFAFAF',display:'none'}}>清除</Text>
		                        </TouchableWithoutFeedback>
		                    </View>
		                </View>

		            <View style={{flexDirection:'row',flexWrap:'wrap'}}>

		                    {
		                    	this.state.datasource1.length==0?(<View style={{width:'100%',height:50,justifyContent:'center',alignItems:'center'}}><Text>暂无搜索记录</Text></View>):(
		                        this.state.datasource1.map((item,index)=>{
		                        	
		                            return (
		                            	<TouchableWithoutFeedback key={index} onPress={()=>this.navigate('FenleiXiangqing',{searchKeywords:item.keywords})}>
		                                <View  key={index} style={{height:Dimensions.get('window').height/25,
		                                    backgroundColor:'#EDEDED',
		                                    justifyContent:'center',
		                                    alignItems:'center',
		                                    marginLeft:Dimensions.get('window').width/29,
		                                    marginTop:20,
		                                    borderRadius:5,
		                                   	width:item.keywords.length>3?Dimensions.get('window').width/6+(item.keywords.length-3)*Dimensions.get('window').width/25:Dimensions.get('window').width/6}}>
		                                  
		                                    <Text key={index} style={{fontSize:15}}>{item.keywords}</Text>
		                                    
		                                </View>
		                                </TouchableWithoutFeedback>
		                            )
		                        })
		                        )
		                    }
		                </View>

					</View>
		)
	}; 
};

const styles=StyleSheet.create({
	Top:{
		width:'100%',
		height:(Dimensions.get('window').height-StatusBar.currentHeight)/17,
		flexDirection:'row',
		alignItems:'center',
		marginTop:Platform.OS ==="ios"?Dimensions.get('window').height/25:0,	
	},
	Top_image:{
		width:Dimensions.get('window').width/37,
		height:'50%',
		marginLeft:Dimensions.get('window').width/29
	},
	Top_view:{
		height:'75%',
		width:'74%',
		marginLeft:Dimensions.get('window').width/15*2-Dimensions.get('window').width/37-Dimensions.get('window').width/29,
		borderRadius:Dimensions.get('window').width/20,
		borderColor:'#BBBBBB',
		borderWidth:1,
		borderStyle:'solid',
	},
	search:{
		marginTop:Dimensions.get('window').height/23,
		width:'100%',
		flexDirection:'column',
	},
	renderItem:{
		height:Dimensions.get('window').height/25,
		backgroundColor:'red',
		justifyContent:'center',
		alignItems:'center'
	}
})