/**
 * @name HomeSearch.js
 * @auhor 程浩
 * @date 2018.8.16
 * @desc 品牌展示
 */
import React, {Component} from "react";
import {Modal,Text,View,StyleSheet,Dimensions, StatusBar,Image,TextInput,FlatList,TouchableWithoutFeedback,ScrollView,ActivityIndicator,RefreshControl} from "react-native";
 // 导入工具函数库
import T from "../lib/Tools";
// 导入公共样式
import St from "../lib/Style";
// 导入图片资源库
import Imgs from "../lib/Img";
import BaseComponent from './BaseComponent';
// //引入图片阴影效果
// import {BoxShadow} from 'react-native-shadow'
// 转菊花
import Spinner from 'react-native-loading-spinner-overlay';

// Tools实例
let ti = T.getInstance();

let W=Dimensions.get('window').width;
let H=Dimensions.get('window').height;


export default class HomeSearchResult extends BaseComponent{

     constructor(props){
     	super(props);
     	this.state={
     		shitu:false,//false表示商品列表呈两列展示，true为一列
        brandIds:[],//brandId品牌ID
        goodsGroupIds:[],//goodsGroupId其余分类的ID
        pageSize:BaseComponent.PAGE_SIZE,//每次查询的条数
        pageNumber:1,//查询的页数
        totalPage:"",//数据总页数
        brandIns:{country:{name:"",imageAttachment:{resourceFullAddress:""}},name:"",imageAttachment:{resourceFullAddress:""},summary:""},//品牌简介
     	  hasNext:"",//是否有下一页数据true为有
        // 转菊花
        visible:false,
        netError:false,//true表示网络错误
        //商品数据
        data:[],
       
        isRefreshing:false,//下拉刷新,
        underLine:true,//默认是true代表综合
        SALES_NUM:0,//销量 默认是0,1代表升序,2代表降序
        PRICE:0,//价格 默认是0,1代表升序,2代表降序
        orders:{},//销量价格筛选
        params:{page:{pageNumber:1,pageSize:BaseComponent.PAGE_SIZE}},//传给后台的获取数据的参数!!!!!!,这个很重要
        MoreBrandInfo:false,//是否查看更多品牌信息
        
      }
     };

     componentDidMount(){
        var thiz=this;
        var data={};
        //从国家点击进来的话接收国家ID
        // if(thiz.params.countryPavilionId){
        //   var countryPavilionId=thiz.params.countryPavilionId;
        //   console.log("----------------countryPavilionId---------------",countryPavilionId);
        //   thiz.setState({
        //     countryPavilionId:countryPavilionId
        //   });
        //   if(!data.countryPavilionId)
        //   {
        //     data.countryPavilionIds=[];
        //     data.countryPavilionIds.push(countryPavilionId);
        //   }
        // }
        //从品牌点击进来的话接收品牌ID
        if(thiz.params.brandId){
          var brandId=thiz.params.brandId;
          console.log("----------------brandId---------------",brandId);
        
          if(!data.brandIds)
          {
            data.brandIds=[];
            data.brandIds.push(brandId);
          }
          var brandIds=thiz.state.brandIds
            brandIds.push(brandId);
            thiz.setState({
            brandIds:brandIds,

          });
          console.log("--------------------------------------",thiz.state.brandIds);
        }
        //从其他分类点击进来的话接收其他分类ID
        if(thiz.params.goodsGroupId){
          var goodsGroupId=thiz.params.goodsGroupId;
          console.log("----------------goodsGroupId---------------",goodsGroupId);
        
          if(!data.goodsGroupIds)
          {
            data.goodsGroupIds=[];
            data.goodsGroupIds.push(goodsGroupId);
          }
            thiz.setState({
            goodsGroupIds:data
          })
        }
        if(!data.page)
         {
           data.page={};
           if(!data.page.pageSize){
            data.page.pageSize=thiz.state.pageSize;
           }
           if(!data.page.pageNumber){
            data.page.pageNumber=thiz.state.pageNumber;
           }
         }
         thiz.setState({visible:true,params:data});
         thiz.log("---------------------------------------------data-------------------------",data);
          thiz.request("spu/getOnSalePage",{
            method:"POST",
            data:data,
            success:function(ret){

                var data1=[];
                //重构商品品牌内容数据结构
                var brandIns={};

                if(!ret.data.brand)
                {
                  thiz.setState({visible:false});
                  return ;
                }
                if(!brandIns.country)
                {
                  brandIns.country={};
                  if(!brandIns.country.name)
                  {
                    brandIns.country.name="";
                  }
                  if(!brandIns.country.imageAttachment)
                  {
                    brandIns.country.imageAttachment={};
                    if(!brandIns.country.imageAttachment.resourceFullAddress)
                    {
                      brandIns.country.imageAttachment.resourceFullAddress="";
                    }
                  }
                }
                brandIns.name="";
                if(!brandIns.imageAttachment)
                {
                  brandIns.imageAttachment={};
                  if(!brandIns.imageAttachment.resourceFullAddress)
                  {
                    brandIns.imageAttachment.resourceFullAddress="";
                  }
                }
                brandIns.summary="";
                brandIns.country.name=ret.data.brand.country.name;
                brandIns.country.imageAttachment.resourceFullAddress=ret.data.brand.country.imageAttachment.resourceFullAddress;
                brandIns.name=ret.data.brand.name;
                brandIns.imageAttachment.resourceFullAddress=ret.data.brand.imageAttachment.resourceFullAddress;
                brandIns.summary=ret.data.brand.summary;  
                thiz.log("------------------------brandIns------------------------------",brandIns);
                if(!ret.data.spuPage.records)
                {
                  thiz.setState({visible:false,brandIns:brandIns});
                  return ;
                }  
                if(ret.data.spuPage.records.length==0){
                  thiz.setState({visible:false,brandIns:brandIns});
                    return ;
                }
                //重构商品列表数据结构  
                for(var i=0;i<ret.data.spuPage.records.length;i++)
                {
                      var data2={};
                      var img=ret.data.spuPage.records[i].attachment.resourceFullAddress;//商品图片路径;
                      var id=ret.data.spuPage.records[i].id;//商品ID
                      var ins=ret.data.spuPage.records[i].name;//商品介绍
                      var price=ret.data.spuPage.records[i].buyUnitPrice;//商品卖出的价格
                      // var cutprice=ret.data.spuPage.records[i].marketPrice;//商品市场价
                      // var logisticPrice=ret.data.spuPage.records[i].logisticPrice;//logisticPrice
                      // var taxPrice=ret.data.spuPage.records[i].taxPrice;//taxPrice
                      // var surplusStockNum=ret.data.spuPage.records[i].surplusStockNum;//surplusStockNum
                      data2.img=img;
                      data2.id=id;
                      data2.ins=ins;
                      data2.price=price;
                      // data2.cutprice=cutprice;
                      // data2.logisticPrice=logisticPrice;
                      // data2.taxPrice=taxPrice;
                      // data2.surplusStockNum=surplusStockNum;
                      data1.push(data2);
                }

                thiz.setState({
                  data:data1,
                  brandIns:brandIns,
                  visible:false,
                  hasNext:ret.data.spuPage.hasNext,
                  params:data,
                });
            },
            error:function(err){
              thiz.setState({visible:false});
              if(err.code&&err.code==200){
                thiz.setState({netError:true});
              }  
            }
         });
        }
      

     _keyExtractor = (item,index) => index.toString();

     /*两列商品展示*/
      _renderItem=(info)=>{
        let thiz=this;
      return (
        
        <View style={{width:info.index==thiz.state.data.length-1&&thiz.state.data.length%2==1?'100%':'50%',paddingLeft:BaseComponent.W*10/375,paddingTop:BaseComponent.W*12/375,backgroundColor:'white'}}>

        <TouchableWithoutFeedback onPress={()=>this.goodsOnclick(info)}>    
        <View style={{width:BaseComponent.W*171/375,height:BaseComponent.W*172/375,borderWidth:0.5,borderColor:'rgba(240, 240, 240, 1)',justifyContent:'center',alignItems:'center'}}>
        <Image style={{width:BaseComponent.W*153/375,height:BaseComponent.W*153/375}} source={{uri:info.item.img}}></Image>
        </View>
        </TouchableWithoutFeedback>

        <View style={{width:BaseComponent.W*169/375,height:BaseComponent.W*31/375,marginTop:BaseComponent.W*0.026,backgroundColor:'#fff'}}>
        <Text style={{fontSize:BaseComponent.W*13/375,color:'rgba(58, 58, 58, 1)',lineHeight:BaseComponent.W*20/375}} numberOfLines={2}>{info.item.ins}</Text>
        </View>

        <View style={{width:BaseComponent.W*171/375,height:BaseComponent.W*25/375,marginTop:BaseComponent.W*0.026,
                    flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
            <Text style={{fontSize:BaseComponent.W*15/375,color:'#FF407E',fontWeight:"bold"}}>¥{info.item.price}&nbsp;&nbsp;&nbsp;&nbsp;   
              {/*<Text style={{textDecorationLine:'line-through',fontSize:BaseComponent.W*11/375,color:'rgba(153, 153, 153, 1)',display:info.item.price>=info.item.cutprice?'none':'flex'}}>¥{info.item.cutprice}</Text>*/}
            </Text>
            
            {/*<TouchableWithoutFeedback onPress={()=>this.addCart(info)}>
            <Image style={{width:BaseComponent.W*25/375,height:BaseComponent.W*25/375,marginRight:BaseComponent.W*0.013}} source={require('../image/home/shoppingCart.png')}></Image>
            </TouchableWithoutFeedback>*/}

        </View>

        </View>
        
      )
    };

    //一列商品展示
    renderItem=(info)=>{
      
      return (
      <View style={{backgroundColor:'white'}}>
      <View style={{width:'100%',height:BaseComponent.W*125/375,marginTop:BaseComponent.W*12/375,flexDirection:'row',backgroundColor:'white'}}>
        
        <TouchableWithoutFeedback onPress={()=>this.goodsOnclick(info)}>  
        <View style={{width:BaseComponent.W*120/375,height:BaseComponent.W*120/375,
                      borderColor:'rgba(240, 240, 240, 1)',borderWidth:0.5,marginLeft:BaseComponent.W*0.026,
                    justifyContent:'center',alignItems:'center'}}>
            <Image style={{width:BaseComponent.W*110/375,height:BaseComponent.W*110/375}} source={{uri:info.item.img}}></Image>
        </View>
        </TouchableWithoutFeedback>  

        <View style={{width:BaseComponent.W*220/375,height:BaseComponent.W*120/375,marginLeft:BaseComponent.W*0.026,justifyContent:'space-between'}}>
            <View style={{width:BaseComponent.W*220/375,height:BaseComponent.W*60/375}}>
                <Text style={{color:'rgba(58, 58, 58, 1)',fontSize:BaseComponent.W*15/375,lineHeight:BaseComponent.W*20/375}} numberOfLines={2}>{info.item.ins}</Text>
            </View>
            <View style={{width:BaseComponent.W*220/375,height:BaseComponent.W*25/375,flexDirection:'row',
                         justifyContent:'space-between'}}>
                <Text style={{color:'#FF407E',fontSize:BaseComponent.W*15/375,fontWeight:"bold"}}>¥{info.item.price}&nbsp;&nbsp;&nbsp;&nbsp;
                
                  {/*<Text style={{color:'rgba(153, 153, 153, 1)',fontSize:BaseComponent.W*11/375,textDecorationLine:'line-through',display:info.item.price>=info.item.cutprice?'none':'flex'}}>¥{info.item.cutprice}</Text>*/}
                </Text>
                
                {/*<TouchableWithoutFeedback onPress={()=>this.addCart(info)}>
                <Image style={{width:BaseComponent.W*25/375,height:BaseComponent.W*25/375,marginRight:BaseComponent.W*0.013}} source={require('../image/home/shoppingCart.png')}></Image>
                </TouchableWithoutFeedback>*/}
            </View>
        </View>
      </View>
      </View>
    )
  };

  //商品点击事件
  goodsOnclick=(info)=>{
    var thiz=this;
    var id=info.item.id.toString();
    thiz.navigate('GoodsXiangQing',{id:id});
  };

  // 加载数据
  loadData(){
    let thiz = this;
    thiz.setState({
          visible:true
    });
    thiz.request("shoppingCar/addGoods",{
          method:"POST",
          data:{
                num:thiz.state.buyNum,
                skuId:selectedSpecs.skuId,
                specialServicesSelectItems:selectedServices
          },
          success:function(ret){
                thiz.setState({
                      visible:false
                });
                if(ret.respCode=="00"){
                      thiz.log("--------加入购物车成功--------","加入购物车成功");
                      thiz.toast("加入购物车成功");
                      // 关闭商品属性选择
                      thiz.setState({
                            visible:false,
                      });

                      // 发送添加购物车成功消息
                      thiz.emit("add_cart_success");
                }
          },
          error:function(err){
                thiz.toast(err.msg);
                thiz.setState({
                      visible:false,
                });
          }
    });
  };
   // 加入购物车
  addCart(value){
    let thiz = this;
    thiz.log("--------addCart--------",value);
    if(value){
      thiz.setState({
            visible:true
      });
      thiz.request("shoppingCar/addGoods",{
            method:"POST",
            data:{
                  num:1,
                  skuId:value.item.id
            },
            success:function(ret){
                  thiz.setState({
                        visible:false
                  });
                  if(ret.respCode=="00"){
                        thiz.log("--------加入购物车成功--------","加入购物车成功");
                        thiz.toast("加入购物车成功");
                        // 关闭商品属性选择
                        thiz.setState({
                              visible:false,
                        });

                        // 发送添加购物车成功消息
                        thiz.emit("add_cart_success");
                  }
            },
            error:function(err){
                  thiz.toast(err.msg);
                  thiz.setState({
                        visible:false,
                  });
            }
      });
    }
  };
  //综合
  Comprehensive=()=>{
    var thiz=this;
    var state=thiz.state;
    var orders={};
    var brandIds=thiz.state.brandIds;
    state.orders=orders;
    state.PRICE=0;
    state.SALES_NUM=0;
    state.underLine=true;
    state.params={page:{pageNumber:1,pageSize:BaseComponent.PAGE_SIZE},brandIds:brandIds};
    thiz.setState(state);
    thiz.getScreeningData(orders);
  };
  //销量筛选数据
  Sales_num=()=>{
    var thiz=this;
    thiz.setState({underLine:false});
    var SALES_NUM=thiz.state.SALES_NUM;
    var orders={};
    var params=thiz.state.params;
    if(params.page)
    {
      if(!params.page.pageNumber)
      {
        params.page.pageNumber="";
      }
        params.page.pageNumber=1;
      
    }else{
      params.page={};
      if(!params.page.pageNumber)
      {
         params.page.pageNumber="";
      }
      params.page.pageNumber=1;
    }

    if(SALES_NUM==0)
    {
      //销量升序
      SALES_NUM++;
      // orders.SALES_NUM="ASC";
      if(thiz.state.PRICE==1)
      {
        orders.PRICE="ASC";
      }
      if(thiz.state.PRICE==2)
      {
        orders.PRICE="DESC";
      }
      orders.SALES_NUM="ASC";
      thiz.log("--------------------销量升序orders-----------------------",orders);
      thiz.setState({SALES_NUM:SALES_NUM,orders:orders,params:params,pageNumber:1});
      thiz.getScreeningData(orders);//开始筛选数据
      return ;
    };
    if(SALES_NUM==1)
    {
      //销量降序
      SALES_NUM++;
      // orders.SALES_NUM="DESC";
      if(thiz.state.PRICE==1)
      {
        orders.PRICE="ASC";
      }
      if(thiz.state.PRICE==2)
      {
        orders.PRICE="DESC";
      }
      orders.SALES_NUM="DESC";
      thiz.log("----------------------------销量降序orders-----------------------",orders);
      thiz.setState({SALES_NUM:SALES_NUM,orders:orders,params:params,pageNumber:1});
      thiz.getScreeningData(orders);//开始筛选数据
      return ;
    };
    if(SALES_NUM==2)
    {
      //销量升序
      SALES_NUM--;
      // orders.SALES_NUM="ASC";
      if(thiz.state.PRICE==1)
      {
        orders.PRICE="ASC";
      }
      if(thiz.state.PRICE==2)
      {
        orders.PRICE="DESC";
      }
      orders.SALES_NUM="ASC";
      thiz.log("----------------------------销量升序orders-----------------------",orders);
      thiz.setState({SALES_NUM:SALES_NUM,orders:orders,params:params,pageNumber:1});
      thiz.getScreeningData(orders);//开始筛选数据
      return ;
    };
  };
  //价格筛选数据
  Price=()=>{
    var thiz=this;
    thiz.setState({underLine:false});
    var PRICE=thiz.state.PRICE;
    var orders={};
    var params=thiz.state.params;
    if(params.page)
    {
      if(!params.page.pageNumber)
      {
        params.page.pageNumber="";
      }
      params.page.pageNumber=1;
    }else{
      params.page={};
      if(!params.page.pageNumber)
      {
         params.page.pageNumber="";
      }
      params.page.pageNumber=1;
    }
    if(PRICE==0)
    {
      //价格升序
      PRICE++;
      // orders.PRICE="ASC";
      if(thiz.state.SALES_NUM==1)
      {
        orders.SALES_NUM="ASC";
      }
      if(thiz.state.SALES_NUM==2)
      {
        orders.SALES_NUM="DESC";
      }
      orders.PRICE="ASC";
      thiz.log("---------------价格升序----------------",orders);
      thiz.setState({PRICE:PRICE,orders:orders,params:params,pageNumber:1});
      thiz.getScreeningData(orders);//开始筛选数据
      return ;
    };
    if(PRICE==1)
    {
      //价格降序
      PRICE++;
      // orders.PRICE="DESC";
      if(thiz.state.SALES_NUM==1)
      {
        orders.SALES_NUM="ASC";
      }
      if(thiz.state.SALES_NUM==2)
      {
        orders.SALES_NUM="DESC";
      }
      orders.PRICE="DESC";
      thiz.log("---------------价格降序----------------",orders);
      thiz.setState({PRICE:PRICE,orders:orders,params:params,pageNumber:1});
      thiz.getScreeningData(orders);//开始筛选数据
      return ;
    };
    if(PRICE==2)
    {
      //价格升序
      PRICE--;
      // orders.PRICE="ASC";
      if(thiz.state.SALES_NUM==1)
      {
        orders.SALES_NUM="ASC";
      }
      if(thiz.state.SALES_NUM==2)
      {
        orders.SALES_NUM="DESC";
      }
      orders.PRICE="ASC";
      thiz.log("---------------价格升序----------------",orders);
      thiz.setState({PRICE:PRICE,orders:orders,params:params,pageNumber:1});
      thiz.getScreeningData(orders);//开始筛选数据
      return ;
    };
  };
  //获取销量价格综合筛选数据
  getScreeningData=(orders)=>{
    var thiz=this;
    var brandIds=thiz.state.brandIds;//品牌ID
    var goodsGroupIds=thiz.state.goodsGroupIds;//其余分类ID
    var params=thiz.state.params;
    params.page.orders=orders;

    // if(brandIds.length==0)
    // {
    //     thiz.log("------------------------goodsGroupIds------------------",goodsGroupIds);
    //     params.goodsGroupIds=[];
       
    //     for(var i=0;i<goodsGroupIds.goodsGroupIds.length;i++)
    //     {
    //         var data=goodsGroupIds.goodsGroupIds[i];
    //         params.goodsGroupIds.push(data);
    //     }
    // }
  
        thiz.log("-------------------------brandIds----------------------",brandIds);
        params.brandIds=[];
        for(var i=0;i<brandIds.length;i++)
        {
            var data=brandIds[i];
            params.brandIds.push(data);
        }
    
    thiz.log("----------------------params----------------------",params);
    thiz.log("------------------------orders--------------------",orders);
    thiz.setState({visible:true})
    thiz.request("spu/getOnSalePage",{
      method:"POST",
      data:params,
      success:function(ret){
          var data=[];
           if(!ret.data.spuPage.records)
            {
              thiz.setState({visible:false});
              return ;
            }  
          //重构商品列表数据结构  
          for(var i=0;i<ret.data.spuPage.records.length;i++)
          {
                var data2={};
                var img=ret.data.spuPage.records[i].attachment.resourceFullAddress;//商品图片路径;
                var id=ret.data.spuPage.records[i].id;//商品ID
                var ins=ret.data.spuPage.records[i].name;//商品介绍
                var price=ret.data.spuPage.records[i].buyUnitPrice;//商品卖出的价格
                // var cutprice=ret.data.spuPage.records[i].marketPrice;//商品市场价
                // var logisticPrice=ret.data.spuPage.records[i].logisticPrice;//logisticPrice
                // var taxPrice=ret.data.spuPage.records[i].taxPrice;//taxPrice
                // var surplusStockNum=ret.data.spuPage.records[i].surplusStockNum;//surplusStockNum
                data2.img=img;
                data2.id=id;
                data2.ins=ins;
                data2.price=price;
                // data2.cutprice=cutprice;
                // data2.logisticPrice=logisticPrice;
                // data2.taxPrice=taxPrice;
                // data2.surplusStockNum=surplusStockNum;
                data.push(data2);
          }
          
          thiz.setState({
            data:data,
            visible:false,
            params:params,
            hasNext:ret.data.spuPage.hasNext
          });
      },
      error:function(err){
        thiz.setState({visible:false});
      }
    })
  };
  //下拉刷新
  onRefresh=()=>{
    var thiz=this;
    var state=thiz.state;
    state.data=[];
    state.pageNumber=1;
    state.isRefreshing=true;
    state.hasNext=true;
    thiz.setState(state);
    thiz.getData();
  };
  //下拉刷新，上拉加载数据
  getData=()=>{
    var thiz=this;
    var paramsStr=thiz.state.params;
    var brandIds=thiz.state.brandIds;
    var goodsGroupIds=thiz.state.goodsGroupIds;
    var pageNumber=thiz.state.pageNumber;
    var hasNext=thiz.state.hasNext;
    // if(brandIds.length==0)
    // {
    //   paramsStr.goodsGroupIds=[];
    //   // paramsStr.goodsGroupIds=goodsGroupIds;
    //   for(var i=0;i<goodsGroupIds.goodsGroupIds.length;i++)
    //   {
    //       var data=goodsGroupIds.goodsGroupIds[i];
    //       paramsStr.goodsGroupIds.push(data);
    //   }
    // }
   
      paramsStr.brandIds=[];
      // paramsStr.brandIds=brandIds;
      for(var i=0;i<brandIds.length;i++)
      {
          var data=brandIds[i];
          paramsStr.brandIds.push(data);
      }
    
    paramsStr.page.pageNumber=pageNumber;
    thiz.log("----------------------------paramsStr-----------------------------",paramsStr);
    thiz.log("----------------------------brandIds---------------------------",brandIds);
    thiz.log("----------------------------goodsGroupIds---------------------------",goodsGroupIds);
    console.log("----------------------------pageNumber---------------------------",pageNumber);
    console.log("----------------------------hasNext---------------------------",hasNext);
      if(hasNext){
      thiz.setState({visible:true});
      thiz.request("spu/getOnSalePage",{
        method:"POST",
        data:paramsStr,
        success:function(ret){
          //判断是下拉刷新还是上拉加载
           if(!ret.data.spuPage.records)
            {
              thiz.setState({visible:false,isRefreshing:false});
              return ;
            }  
          if(thiz.state.isRefreshing)
          {
            var data=[];
          }
          else
          {
            var data=thiz.state.data;
          }
          //重构商品列表数据结构  
          for(var i=0;i<ret.data.spuPage.records.length;i++)
          {
                var data2={};
                var img=ret.data.spuPage.records[i].attachment.resourceFullAddress;//商品图片路径;
                var id=ret.data.spuPage.records[i].id;//商品ID
                var ins=ret.data.spuPage.records[i].name;//商品介绍
                var price=ret.data.spuPage.records[i].buyUnitPrice;//商品卖出的价格
                // var cutprice=ret.data.spuPage.records[i].marketPrice;//商品市场价
                // var logisticPrice=ret.data.spuPage.records[i].logisticPrice;//logisticPrice
                // var taxPrice=ret.data.spuPage.records[i].taxPrice;//taxPrice
                // var surplusStockNum=ret.data.spuPage.records[i].surplusStockNum;//surplusStockNum
                data2.img=img;
                data2.id=id;
                data2.ins=ins;
                data2.price=price;
                // data2.cutprice=cutprice;
                // data2.logisticPrice=logisticPrice;
                // data2.taxPrice=taxPrice;
                // data2.surplusStockNum=surplusStockNum;
                data.push(data2);
          }
            thiz.setState({
              isRefreshing:false,
              visible:false,
              data:data,
              hasNext:ret.data.spuPage.hasNext
            });
        },
        error:function(err){
          thiz.setState({
            isRefreshing:false,
            visible:false,
          })
        }
      });
    }
    else
    {
      thiz.setState({isRefreshing:false})
    }
  };

  //搜索框搜索
  search=(event)=>{
    var thiz=this;
    var searchWords=event.nativeEvent.text;
    var brandIds=thiz.state.brandIds;
    var goodsGroupIds=thiz.state.goodsGroupIds;
    var params=thiz.state.params;
    params.searchKeywords=searchWords;
    // if(brandIds.length==0)
    // {
    //     thiz.log("------------------------goodsGroupIds------------------",goodsGroupIds);       
    //     params.goodsGroupIds=[];
    //     for(var i=0;i<goodsGroupIds.goodsGroupIds.length;i++)
    //     {
    //         var data=goodsGroupIds.goodsGroupIds[i];
    //         params.goodsGroupIds.push(data);
    //     }
    // }
    // if(goodsGroupIds.length==0)
    // {
        thiz.log("-------------------------brandIds----------------------",brandIds);
        params.brandIds=[];
        for(var i=0;i<brandIds.length;i++)
        {
            var data=brandIds[i];
            params.brandIds.push(data);
        }
    // }
    console.log("--------------------------searchWords--------------------------",searchWords);
    console.log("-----------------------------brandId--------------------",brandIds);
    console.log("---------------------------goodsGroupIds-----------------",goodsGroupIds);
    thiz.log("-----------------------params------------------",params);
    thiz.setState({visible:true,data:[]});
    thiz.request("spu/getOnSalePage",{
      method:"POST",
      data:params,
      success:function(ret){
          var data1=[];
           //重构商品品牌内容数据结构
          var brandIns={};
          if(!ret.data.brand)
          {
            thiz.setState({visible:false});
            return ;
          }
          if(!brandIns.country)
                {
                  brandIns.country={};
                  if(!brandIns.country.name)
                  {
                    brandIns.country.name="";
                  }
                  if(!brandIns.country.imageAttachment)
                  {
                    brandIns.country.imageAttachment={};
                    if(!brandIns.country.imageAttachment.resourceFullAddress)
                    {
                      brandIns.country.imageAttachment.resourceFullAddress="";
                    }
                  }
                }
                brandIns.name="";
                if(!brandIns.imageAttachment)
                {
                  brandIns.imageAttachment={};
                  if(!brandIns.imageAttachment.resourceFullAddress)
                  {
                    brandIns.imageAttachment.resourceFullAddress="";
                  }
                }
               
                brandIns.summary="";
                brandIns.country.name=ret.data.brand.country.name;
                brandIns.country.imageAttachment.resourceFullAddress=ret.data.brand.country.imageAttachment.resourceFullAddress;
                brandIns.name=ret.data.brand.name;
                brandIns.imageAttachment.resourceFullAddress=ret.data.brand.imageAttachment.resourceFullAddress;
                brandIns.summary=ret.data.brand.summary;  
                thiz.log("------------------------brandIns------------------------------",brandIns);
          if(!ret.data.spuPage.records)
          {
            thiz.setState({visible:false});
            return ;
          }  
          if(ret.data.spuPage.records.length==0){
            thiz.setState({visible:false});
              return ;
          }
          
          //重构商品列表数据结构  
          for(var i=0;i<ret.data.spuPage.records.length;i++)
          {
                var data2={};
                var img=ret.data.spuPage.records[i].attachment.resourceFullAddress;//商品图片路径;
                var id=ret.data.spuPage.records[i].id;//商品ID
                var ins=ret.data.spuPage.records[i].name;//商品介绍
                var price=ret.data.spuPage.records[i].buyUnitPrice;//商品卖出的价格
                // var cutprice=ret.data.spuPage.records[i].marketPrice;//商品市场价
                // var logisticPrice=ret.data.spuPage.records[i].logisticPrice;//logisticPrice
                // var taxPrice=ret.data.spuPage.records[i].taxPrice;//taxPrice
                // var surplusStockNum=ret.data.spuPage.records[i].surplusStockNum;//surplusStockNum
                data2.img=img;
                data2.id=id;
                data2.ins=ins;
                data2.price=price;
                // data2.cutprice=cutprice;
                // data2.logisticPrice=logisticPrice;
                // data2.taxPrice=taxPrice;
                // data2.surplusStockNum=surplusStockNum;
                data1.push(data2);
          }
          thiz.setState({data:data1,
            hasNext:ret.data.spuPage.hasNext,
            params:params,
            visible:false,
            brandIns:brandIns
          });
      },
      error:function(err){
        thiz.setState({visible:false});
      }
    })
  };
	render(){
    
    let thiz=this;
		return(
			<View style={{flex:1,backgroundColor:'#F0F0F0'}}>
        {/*状态栏，沉浸式*/}
        <StatusBar translucent={true} backgroundColor={this.state.visible?'rgba(14,14,14,0.5)':'transparent'} barStyle={'dark-content'}/>
       {/*转菊花*/}
        <Modal
          visible={this.state.visible}
          onRequestClose={()=>{this.setState({visible:false})}}
          transparent={true}
          animationType={"fade"}>
          <View style={{backgroundColor:"rgba(14,14,14,0.5)",width:"100%",width:"100%",flex:1,flexDirection:"column",alignItems:"center",justifyContent:"center",position:"relative"}}>
            <View>
                  <ActivityIndicator size="large" color='white'/>
            </View>
          </View>
        </Modal>

				{/*搜索框*/}
				<View style={{width:'100%',height:50,position:'absolute',top:0,zIndex:1000,marginTop:thiz.isIphoneX(30,BaseComponent.SH),flexDirection:'row',alignItems:"center"}}>
					<TouchableWithoutFeedback onPress={()=>thiz.goBack()}>
          <Image style={{width:(BaseComponent.W*0.127*0.4),height:(BaseComponent.W*0.127*0.4),marginLeft:BaseComponent.W*11/375}} source={require('../image/home/back1.png')}></Image>	
					</TouchableWithoutFeedback> 
          <View style={{width:BaseComponent.W*280/375,height:BaseComponent.W*35/375,borderColor:'#fff',borderWidth:1,borderRadius:BaseComponent.W*24/375,
									marginLeft:BaseComponent.W*28/375}}>
							<TextInput style={{width:'100%',height:'100%',padding:0,fontSize:BaseComponent.W*15/375,marginLeft:BaseComponent.W*15/375,color:'#121212'}}  placeholder="请在品牌内搜索商品" underlineColorAndroid='transparent' maxLength={30} placeholderTextColor="#C7C7C7"
									     	placeholderTextColor="#fff" onSubmitEditing={(event)=>{this.search(event)}}></TextInput>		
					</View>
				</View>
				<Image style={{width:'100%',height:BaseComponent.W*115/375}} source={require('../image/home/brandbackground.png')}></Image>
				
				{
          thiz.state.netError?(
                    <View style={{width:BaseComponent.W,height:BaseComponent.H*0.85,justifyContent:'center',alignItems:'center'}}>
                      
                      <Image style={{width:BaseComponent.W*180/375,height:BaseComponent.W*180/375,borderRadius:BaseComponent.W*90/375}} source={require('../image/mine/neterror.png')}></Image>
                      
                      <TouchableWithoutFeedback onPress={()=>{
                        thiz.setState({netError:false});
                        var params=thiz.state.params;
                        var brandIds=thiz.state.brandIds;
                        thiz.log("--------------------params---------------------",params);
                        thiz.log("------------------brandIds-----------------------",brandIds);
                        if(!params.brandIds)
                        {
                          params.brandIds=[];
                        }
                        for(var i=0;i<brandIds.length;i++)
                        {
                          params.brandIds.push(brandIds[i]);
                        }
                        thiz.setState({visible:true});
                        thiz.request("spu/getOnSalePage",{
                          method:"POST",
                          data:params,
                          success:function(ret){
                              var data=thiz.state.data;
                               //重构商品品牌内容数据结构
                              var brandIns={};

                              if(!ret.data.brand)
                              {
                                thiz.setState({visible:false});
                                return ;
                              }
                              if(!brandIns.country)
                              {
                                brandIns.country={};
                                if(!brandIns.country.name)
                                {
                                  brandIns.country.name="";
                                }
                                if(!brandIns.country.imageAttachment)
                                {
                                  brandIns.country.imageAttachment={};
                                  if(!brandIns.country.imageAttachment.resourceFullAddress)
                                  {
                                    brandIns.country.imageAttachment.resourceFullAddress="";
                                  }
                                }
                              }
                              brandIns.name="";
                              if(!brandIns.imageAttachment)
                              {
                                brandIns.imageAttachment={};
                                if(!brandIns.imageAttachment.resourceFullAddress)
                                {
                                  brandIns.imageAttachment.resourceFullAddress="";
                                }
                              }
                              brandIns.summary="";
                              brandIns.country.name=ret.data.brand.country.name;
                              brandIns.country.imageAttachment.resourceFullAddress=ret.data.brand.country.imageAttachment.resourceFullAddress;
                              brandIns.name=ret.data.brand.name;
                              brandIns.imageAttachment.resourceFullAddress=ret.data.brand.imageAttachment.resourceFullAddress;
                              brandIns.summary=ret.data.brand.summary;
                              thiz.setState({brandIns:brandIns});  
                             if(!ret.data.spuPage.records)
                              {
                                thiz.setState({visible:false});
                                return ;
                              }  
                            //重构商品列表数据结构  
                            for(var i=0;i<ret.data.spuPage.records.length;i++)
                            {
                                  var data2={};
                                  var img=ret.data.spuPage.records[i].attachment.resourceFullAddress;//商品图片路径;
                                  var id=ret.data.spuPage.records[i].id;//商品ID
                                  var ins=ret.data.spuPage.records[i].name;//商品介绍
                                  var price=ret.data.spuPage.records[i].buyUnitPrice;//商品卖出的价格
                                  // var cutprice=ret.data.spuPage.records[i].marketPrice;//商品市场价
                                  // var logisticPrice=ret.data.spuPage.records[i].logisticPrice;//logisticPrice
                                  // var taxPrice=ret.data.spuPage.records[i].taxPrice;//taxPrice
                                  // var surplusStockNum=ret.data.spuPage.records[i].surplusStockNum;//surplusStockNum
                                  data2.img=img;
                                  data2.id=id;
                                  data2.ins=ins;
                                  data2.price=price;
                                  // data2.cutprice=cutprice;
                                  // data2.logisticPrice=logisticPrice;
                                  // data2.taxPrice=taxPrice;
                                  // data2.surplusStockNum=surplusStockNum;
                                  data.push(data2);
                            }
                            
                            thiz.setState({
                              data:data,
                              visible:false,
                              params:params,
                              hasNext:ret.data.spuPage.hasNext
                            });
                          },
                          error:function(err){
                              thiz.setState({visible:false})
                              if(err.code&&err.code==200){
                                thiz.setState({netError:true});
                              }  
                          }
                        })
                      }}>
                      <View style={{width:BaseComponent.W*70/375,height:BaseComponent.W*30/375,borderRadius:BaseComponent.W*15/375,borderWidth:0.5,borderColor:'#080808',
                                justifyContent:'center',alignItems:'center',marginTop:BaseComponent.W*15/375}}>
                            <Text style={{fontSize:BaseComponent.W*15/375,color:'#252525'}}>刷新</Text>          
                      </View>
                      </TouchableWithoutFeedback>
                    </View>):(
                        <View style={{flex:1}}>
                        {/*品牌介绍*/}
                        <View style={{shadowOpacity:1,
                            shadowColor:"#000000",shadowOffset:{width:0,height:2},
                            width:BaseComponent.W*70/375,height:BaseComponent.W*70/375,position:'absolute',top:0,zIndex:10000,
                                marginTop:-BaseComponent.W*25/375,marginLeft:BaseComponent.W*20/375}}>
                          <Image style={{
                            width:BaseComponent.W*70/375,height:BaseComponent.W*70/375
                            }} source={{uri:this.state.brandIns.imageAttachment.resourceFullAddress}}></Image>
                        </View>
                       
                        <View style={{width:'100%',backgroundColor:'#fff'}}>
                            <View style={{width:BaseComponent.W*120/375,height:BaseComponent.W*25/375,marginLeft:BaseComponent.W*100/375,marginTop:BaseComponent.W*10/375,flexDirection:'row',alignItems:'center'}}>
                              <Text style={{fontSize:BaseComponent.W*16/375,color:'#010101'}}>{this.state.brandIns.name}</Text>
                              <View style={{width:BaseComponent.W*70/375,height:BaseComponent.W*14/375,backgroundColor:'#FDD17A',marginLeft:BaseComponent.W*10/375,borderRadius:BaseComponent.W*7/375,
                                     justifyContent:'center',alignItems:'center',display:'none'}}>
                                  <Text style={{fontSize:BaseComponent.W*9/375,color:'#818181'}}></Text>
                              </View>
                            </View>
                            <View style={{width:BaseComponent.W*90/375,height:BaseComponent.W*20/375,marginLeft:BaseComponent.W*100/375,
                                  flexDirection:'row',alignItems:'center'}}>
                                <Image style={{width:BaseComponent.W*20/375,height:BaseComponent.W*12/375}} source={{uri:this.state.brandIns.country.imageAttachment.resourceFullAddress}}></Image>
                                <Text style={{fontSize:BaseComponent.W*10/375,color:'#BEBEBE',marginLeft:BaseComponent.W*10/375}}>{this.state.brandIns.country.name}品牌</Text>
                            </View>
                            <View style={{width:BaseComponent.W*320/375,marginTop:BaseComponent.W*10/375,marginLeft:BaseComponent.W*21/375}}>
                                <Text style={{fontSize:BaseComponent.W*12/375,color:'#BEBEBE',lineHeight:BaseComponent.W*20/375,color:'black'}} numberOfLines={thiz.state.MoreBrandInfo?100:2}>{this.state.brandIns.summary}</Text>
                            </View>
                            {/*更多品牌信息*/}
                            <TouchableWithoutFeedback onPress={()=>{
                              thiz.setState({MoreBrandInfo:!thiz.state.MoreBrandInfo});
                            }}>
                            <View style={{width:BaseComponent.W*320/375,height:BaseComponent.W*31/375,marginLeft:BaseComponent.W*21/375,flexDirection:'row',alignItems:'center'}}>
                                <View style={{width:BaseComponent.W*130/375,height:0.5,backgroundColor:'#E7E7E7'}}></View>   
                                <Text style={{fontSize:BaseComponent.W*10/375,color:'#BEBEBE',marginLeft:BaseComponent.W*10/375}}>{thiz.state.MoreBrandInfo?"收起品牌信息":"更多品牌信息"}</Text>
                                <View style={{width:BaseComponent.W*100/375,height:0.5,backgroundColor:"#E7E7E7",marginLeft:BaseComponent.W*10/375}}></View>
                            </View>
                            </TouchableWithoutFeedback>
                        </View>
                         
                        <View style={{width:'100%',height:BaseComponent.W*10/375,backgroundColor:"#F0F0F0"}}></View>  
                        
                        {/*商品选择*/}
                        <View style={{flex:1,backgroundColor:'#fff'}}>
                            {/*综合销量价格*/}
                            <View style={{width:'100%',height:BaseComponent.W*35/375,flexDirection:'row',alignItems:'center'}}>
                                <TouchableWithoutFeedback onPress={()=>{
                                      thiz.Comprehensive();
                                }}>
                                <View style={{width:BaseComponent.W*40/375,height:'100%',alignItems:'center',marginLeft:BaseComponent.W*22/375,justifyContent:'center'}}>
                                      <Text style={{fontSize:BaseComponent.W*14/375,color:thiz.state.underLine?'#272727':'#888888',marginTop:ti.select({ios:5,android:0})}}>综合</Text>
                                      <View style={{width:BaseComponent.W*30/375,height:2,borderRadius:1,marginTop:BaseComponent.W*5/375,backgroundColor:thiz.state.underLine?'#272727':'white'}}></View>
                                </View>
                                </TouchableWithoutFeedback>

                                {/*销量*/}
                                <TouchableWithoutFeedback onPress={()=>{
                                  thiz.Sales_num();
                                }}>
                                <View style={{width:BaseComponent.W*50/375,height:'100%',marginLeft:BaseComponent.W*30/375,alignItems:'center'}}>
                                    <View style={{width:BaseComponent.W*50/375,height:BaseComponent.W*24/375,flexDirection:'row',alignItems:'flex-end',justifyContent:'space-between'}}>
                                        <Text style={{fontSize:BaseComponent.W*14/375,color:thiz.state.SALES_NUM==0?'#888888':'#272727'}}>销量</Text>
                                        <View style={{width:BaseComponent.W*12/375,height:"100%"}}>
                                              <Image style={{width:BaseComponent.W*10/375,height:BaseComponent.W*6/375,marginTop:BaseComponent.W*8/375}} source={thiz.state.SALES_NUM==1?require('../image/home/xuanzhongshangsanjiao.png'):require('../image/home/weixuanzhongshangsanjiao.png')} resizeMode="contain"></Image>
                                              <Image style={{width:BaseComponent.W*10/375,height:BaseComponent.W*6/375,marginTop:BaseComponent.W*2/375}} source={thiz.state.SALES_NUM==2?require('../image/home/xuanzhongxiasanjiao.png'):require('../image/home/weixuanzhongxiasanjiao.png')} resizeMode="contain"></Image>
                                        </View>
                                    </View>
                                    <View style={{width:BaseComponent.W*30/375,height:2,borderRadius:1,backgroundColor:'#272727',marginTop:BaseComponent.W*5/375,display:thiz.state.SALES_NUM==0?'none':'flex',marginRight:BaseComponent.W*20/375}}></View>
                                </View>
                                </TouchableWithoutFeedback>

                                {/*价格*/}
                                <TouchableWithoutFeedback onPress={()=>{
                                  thiz.Price();
                                }}>
                                <View style={{width:BaseComponent.W*50/375,height:'100%',marginLeft:BaseComponent.W*30/375,alignItems:'center'}}>
                                    <View style={{width:BaseComponent.W*50/375,height:BaseComponent.W*24/375,flexDirection:'row',alignItems:'flex-end',justifyContent:'space-between'}}>
                                        <Text style={{fontSize:BaseComponent.W*14/375,color:thiz.state.PRICE==0?'#888888':'#272727'}}>价格</Text>
                                        <View style={{width:BaseComponent.W*12/375,height:"100%"}}>
                                              <Image style={{width:BaseComponent.W*10/375,height:BaseComponent.W*6/375,marginTop:BaseComponent.W*8/375}} source={thiz.state.PRICE==1?require('../image/home/xuanzhongshangsanjiao.png'):require('../image/home/weixuanzhongshangsanjiao.png')} resizeMode="contain"></Image>
                                              <Image style={{width:BaseComponent.W*10/375,height:BaseComponent.W*6/375,marginTop:BaseComponent.W*2/375}} source={thiz.state.PRICE==2?require('../image/home/xuanzhongxiasanjiao.png'):require('../image/home/weixuanzhongxiasanjiao.png')} resizeMode="contain"></Image>
                                        </View>
                                    </View>
                                    <View style={{width:BaseComponent.W*30/375,height:2,borderRadius:1,backgroundColor:'#272727',marginTop:BaseComponent.W*5/375,display:thiz.state.PRICE==0?'none':'flex',marginRight:BaseComponent.W*20/375}}></View>
                                </View>
                                </TouchableWithoutFeedback>

                                {/*切换视图*/}
                                <TouchableWithoutFeedback onPress={()=>this.setState({shitu:!this.state.shitu})}>
                                <Image style={{width:BaseComponent.W*20/375,height:BaseComponent.W*20/375,marginLeft:BaseComponent.W*90/375}} source={this.state.shitu?require('../image/home/gengduo.png'):require('../image/home/sigongge.png')}></Image>
                                </TouchableWithoutFeedback>
                            </View> 
                            <View style={{width:'100%',height:0.5,backgroundColor:'#EFEFEF'}}></View>
                            {/*选择种类*/}
                            {/*<View style={{width:'100%',height:BaseComponent.W*38/375,flexDirection:'row',alignItems:'center'}}>
                                <ScrollView style={{flex:1}} 
                                            horizontal={true}
                                            showsHorizontalScrollIndicator={false}>
                                      <View style={{width:'100%',height:BaseComponent.W*38/375,flexDirection:'row',alignItems:'center'}}>
                                            {
                                              data1.map((item,index)=>{
                                                console.log("-----item-----",item)
                                                return (
                                                    <View style={{width:BaseComponent.W*70/375,height:BaseComponent.W*25/375,backgroundColor:'#F5F5F5',borderRadius:3,marginLeft:BaseComponent.W*10/375,
                                                              justifyContent:'center',alignItems:'center'}} key={index}>
                                                          <Text style={{fontSize:BaseComponent.W*14/375,color:'#888888'}}>{item.key}</Text>  
                                                    </View>
                                                )
                                              })
                                            }
                                      </View>      
                                </ScrollView>
                            </View>
                            <View style={{width:'100%',height:0.5,backgroundColor:'#EFEFEF'}}></View>*/}

                            {/*商品列表*/}

                            {
                              thiz.state.data.length==0?(
                                  <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                                    <Image style={{width:BaseComponent.W*180/375,height:BaseComponent.W*180/375,borderRadius:BaseComponent.W*90/375}} source={require('../image/mine/nogoods.png')}></Image>
                                  </View>
                                ):(<View style={{flex:1,backgroundColor:'#F0F0F0'}}>
                                     <View style={{backgroundColor:"white"}}>
                                        <FlatList
                                          style={{backgroundColor:"#F0F0F0",height:'100%'}}
                              
                                          data={this.state.data}
                                          keyExtractor={this._keyExtractor}
                                          renderItem={this.state.shitu?this.renderItem:this._renderItem}
                                          showsVerticalScrollIndicator = {false}
                                          numColumns={this.state.shitu?'1':'2'}
                                          ListFooterComponent={thiz.state.hasNext?null:thiz.nomore({bgcolor:'#F0F0F0'})}
                                          extraData={this.state}
                                          key={this.state.shitu}
                                          refreshing={this.state.isRefreshing}
                                          onMomentumScrollEnd={(e)=>{
                                              var offsetY = e.nativeEvent.contentOffset.y; //滑动距离
                                              var contentSizeHeight = e.nativeEvent.contentSize.height; //scrollView contentSize高度
                                              var oriageScrollHeight = e.nativeEvent.layoutMeasurement.height; //scrollView高度
                                              console.log("---------------------滑动距离-------------------",offsetY);
                                              console.log("---------------------scrollView contentSize高度-------------------",contentSizeHeight);
                                              console.log("---------------------scrollView高度-------------------",oriageScrollHeight);
                                              if (offsetY + oriageScrollHeight >= contentSizeHeight-5)
                                              {
                                                   console.log("------------------------onEndReached_hasNext-------------------------",thiz.state.hasNext);
                                                    if(thiz.state.hasNext){
                                                    var pageNumber=thiz.state.pageNumber;
                                                    var state=thiz.state;
                                                    
                                                    pageNumber++;
                                                    state.pageNumber=pageNumber;
                                                    thiz.setState(state);
                                                    
                                                    thiz.getData();
                                                  }
                                                  else
                                                  {
                                                    // thiz.toast("已经到底了，没有更多商品了");
                                                  }
                                              }
                                          }}
                                          refreshControl={
                                            <RefreshControl
                                              refreshing={thiz.state.isRefreshing}
                                              onRefresh={()=>{thiz.onRefresh()}}
                                              colors={['#ff0000', '#00ff00','#0000ff','#3ad564']}
                                              progressBackgroundColor="#ffffff"
                                            />}
                                        />
                                    </View>
                                    
                                  </View>)
                            }
                            
                        </View> 
                    </View>
                    )
        }
  

			</View>
		)
	}
};
	
