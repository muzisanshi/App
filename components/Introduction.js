/**
 * @name Introduction.js
 * @auhor 程浩
 * @date 2018.8.24
 * @desc 小洋匠简介
 */
import React,{Component} from 'react';
import {Text,View,StyleSheet,Image,TouchableWithoutFeedback,ScrollView,StatusBar,TextInput,FlatList,Modal,SectionList,ActivityIndicator} from 'react-native';
 // 导入工具函数库
import T from "../lib/Tools";
// 导入公共样式
import St from "../lib/Style";
// 导入图片资源库
import Imgs from "../lib/Img";
import BaseComponent from './BaseComponent';



// Tools实例
let ti = T.getInstance();

let style = StyleSheet.create({
  wrapper:{
    backgroundColor:"white",
    flex:1,
    flexDirection:"column",
    alignItems:"stretch",
    justifyContent:"flex-start",
    position:"relative",
  },
  statusBar:{
    backgroundColor:"rgb(255,255,255)",
    height:ti.select({ios:16,android:22}),
    position:"relative",
    zIndex:1,
    opacity:1,
  },
  navBar:{
    backgroundColor:"rgb(255,255,255)",
    height:"7%",
    position:"relative",
    zIndex:1,
    opacity:1,
    borderBottomWidth:1,
    borderBottomColor:"rgb(241,241,241)"
  },
});
export default class Introduction extends BaseComponent{
  render(){
    return (
      <View style={style.wrapper}>
                {/*状态栏占位*/}
                <View style={{
                  backgroundColor:"rgb(255,255,255)",
                  height:this.isIphoneX(30,BaseComponent.SH),
                  position:"relative",
                  zIndex:1,
                  opacity:1,
                }}></View>
                {/*顶部导航栏*/}
                <View style={style.navBar}>
                  <View style={{flex:1,flexDirection:"row",justifyContent:"flex-start"}}>

                    <TouchableWithoutFeedback onPress={()=>this.goBack()}>
                    <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>
                      <Image source={Imgs.back} style={{width:(BaseComponent.W*0.127*0.4),height:(BaseComponent.W*0.127*0.4),marginRight:BaseComponent.W*0.04}}></Image>
                    </View>
                    </TouchableWithoutFeedback>

                    <View style={{flexGrow:1,backgroundColor:"transparent",
                      marginTop:ti.select({ios:(BaseComponent.H*0.02)/2,android:(BaseComponent.H*0.016)/2}),borderRadius:ti.select({ios:(BaseComponent.H*0.05)/2,android:(BaseComponent.H*0.054)/2}),borderColor:"#0A0A0A",
                      borderWidth:0,}}>
                      <Text style={{color:"#0A0A0A",width:"100%",textAlign:"center",marginTop:ti.select({ios:"4.5%",android:"1.6%"}),fontSize:17}}>{this.params.title}</Text>
                    </View>
                    <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>
                      
                    </View>
                  </View>
                </View>
            {/*服务条款*/}
           
            <View style={{width:BaseComponent.W*355/375,marginLeft:BaseComponent.W*10/375,marginTop:BaseComponent.W*30/375,justifyContent:'center',alignItems:'center'}}>
              <Text style={{lineHeight:BaseComponent.W*20/375}}>&nbsp;&nbsp;&nbsp;&nbsp;小洋匠app是一款跨境电商平台，由成都俊介科技有限公司自主研发。匠心精选全球优品，平台立志以匠心精神，对每一件商品通过严格的质量把关，保障正品和品质，海外原产地直采优质商品、境外直邮等保障用户购买安心，不断提升用户的跨境电商购物体验。</Text>
              <Text style={{lineHeight:BaseComponent.W*20/375,marginTop:BaseComponent.W*20/375}}>&nbsp;&nbsp;&nbsp;&nbsp;平台坚持以“自营”为中心，通过与海外顶级供应商合作、自建海外仓、自建海外转运物流平台、自建国内清关公司等，将跨境供应链效率做到极致。</Text>
            </View>
              
            

      </View>          
    )
  }
}
