/**
 * @name RefundInstructions.js
 * @auhor 程浩
 * @date 2018.8.24
 * @desc 退款说明
 */
import React,{Component} from 'react';
import {Text,View,StyleSheet,Image,TouchableWithoutFeedback,ScrollView,StatusBar,TextInput,FlatList,Modal,SectionList,ActivityIndicator} from 'react-native';
 // 导入工具函数库
import T from "../lib/Tools";
// 导入公共样式
import St from "../lib/Style";
// 导入图片资源库
import Imgs from "../lib/Img";
import BaseComponent from './BaseComponent';



// Tools实例
let ti = T.getInstance();

let style = StyleSheet.create({
  wrapper:{
    backgroundColor:"white",
    flex:1,
    flexDirection:"column",
    alignItems:"stretch",
    justifyContent:"flex-start",
    position:"relative",
  },
  statusBar:{
    backgroundColor:"rgb(255,255,255)",
    height:ti.select({ios:16,android:22}),
    position:"relative",
    zIndex:1,
    opacity:1,
  },
  navBar:{
    backgroundColor:"rgb(255,255,255)",
    height:"7%",
    position:"relative",
    zIndex:1,
    opacity:1,
    borderBottomWidth:1,
    borderBottomColor:"rgb(241,241,241)"
  },
});
export default class RefundInstructions extends BaseComponent{
  render(){
    return (
      <View style={style.wrapper}>
                {/*状态栏占位*/}
                <View style={{
                  backgroundColor:"rgb(255,255,255)",
                  height:this.isIphoneX(30,BaseComponent.SH),
                  position:"relative",
                  zIndex:1,
                  opacity:1,
                }}></View>
                {/*顶部导航栏*/}
                <View style={style.navBar}>
                  <View style={{flex:1,flexDirection:"row",justifyContent:"flex-start"}}>

                    <TouchableWithoutFeedback onPress={()=>this.goBack()}>
                    <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>
                      <Image source={Imgs.back} style={{width:(BaseComponent.W*0.127*0.4),height:(BaseComponent.W*0.127*0.4),marginRight:BaseComponent.W*0.04}}></Image>
                    </View>
                    </TouchableWithoutFeedback>

                    <View style={{flexGrow:1,backgroundColor:"transparent",
                      marginTop:ti.select({ios:(BaseComponent.H*0.02)/2,android:(BaseComponent.H*0.016)/2}),borderRadius:ti.select({ios:(BaseComponent.H*0.05)/2,android:(BaseComponent.H*0.054)/2}),borderColor:"#0A0A0A",
                      borderWidth:0,}}>
                      <Text style={{color:"#0A0A0A",width:"100%",textAlign:"center",marginTop:ti.select({ios:"4.5%",android:"1.6%"}),fontSize:17}}>{this.params.title}</Text>
                    </View>
                    <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>
                      
                    </View>
                  </View>
                </View>
            {/*退款说明*/}
            <ScrollView style={{flex:1}}>
            <View style={{width:BaseComponent.W*355/375,marginLeft:BaseComponent.W*10/375,marginTop:BaseComponent.W*30/375}}>
              	<Text style={{lineHeight:BaseComponent.W*20/375}}>&nbsp;&nbsp;&nbsp;&nbsp;在办理退款前,请确保您的商品符合退货政策。如果售后人员同意了您的退款申请，退款将按照原路径退回。</Text>
                <Text style={{fontSize:BaseComponent.W*20/375,marginTop:BaseComponent.W*20/375}}>&nbsp;&nbsp;&nbsp;&nbsp;邮费退还说明</Text>
                <View style={{width:BaseComponent.W*355/375,marginTop:BaseComponent.W*20/375,borderWidth:0.5,borderColor:"#888888"}}>
                    <View style={{width:BaseComponent.W*355/375,flexDirection:'row'}}>
                        <View style={{width:BaseComponent.W*95/375,height:BaseComponent.W*60/375,backgroundColor:"#F0F0F0",borderBottomWidth:0.5,borderRightWidth:0.5,borderBottomColor:'#888888',borderRightColor:'#888888'}}>
                            <Text style={{marginTop:BaseComponent.W*20/375,marginLeft:BaseComponent.W*20/375,fontWeight:"bold"}}>退货原因</Text>
                        </View>

                        <View style={{width:BaseComponent.W*75/375,height:BaseComponent.W*60/375,backgroundColor:"#F0F0F0",borderBottomWidth:0.5,borderRightWidth:0.5,borderBottomColor:'#888888',borderRightColor:'#888888'}}>
                            <Text style={{marginTop:BaseComponent.W*10/375,marginLeft:BaseComponent.W*20/375,fontWeight:"bold"}}>发货</Text>
                            <Text style={{marginTop:BaseComponent.W*5/375,marginLeft:BaseComponent.W*20/375,fontWeight:"bold"}}>邮费</Text>
                        </View>
                        <View style={{width:BaseComponent.W*75/375,height:BaseComponent.W*60/375,backgroundColor:"#F0F0F0",borderBottomWidth:0.5,borderRightWidth:0.5,borderBottomColor:'#888888',borderRightColor:'#888888'}}>
                            <Text style={{marginTop:BaseComponent.W*10/375,marginLeft:BaseComponent.W*20/375,fontWeight:"bold"}}>寄回</Text>
                            <Text style={{marginTop:BaseComponent.W*5/375,marginLeft:BaseComponent.W*20/375,fontWeight:"bold"}}>邮费</Text>
                        </View>
                        <View style={{width:BaseComponent.W*110/375,height:BaseComponent.W*60/375,backgroundColor:"#F0F0F0",borderBottomWidth:0.5,borderRightWidth:0.5,borderBottomColor:'#888888',borderRightColor:'#888888'}}>
                            <Text style={{marginTop:BaseComponent.W*20/375,marginLeft:BaseComponent.W*20/375,fontWeight:"bold"}}>备注</Text>
                        </View>
                    </View>

                    <View style={{width:BaseComponent.W*355/375,flexDirection:'row'}}>
                        <View style={{width:BaseComponent.W*95/375,height:BaseComponent.W*70/375,borderBottomWidth:0.5,borderRightWidth:0.5,borderBottomColor:'#888888',borderRightColor:'#888888',justifyContent:'center'}}>
                            <Text style={{marginLeft:5}}>客户自己的原因</Text>
                        </View>

                        <View style={{width:BaseComponent.W*75/375,height:BaseComponent.W*70/375,borderBottomWidth:0.5,borderRightWidth:0.5,borderBottomColor:'#888888',borderRightColor:'#888888',justifyContent:'center'}}>
                            <Text style={{marginLeft:BaseComponent.W*20/375}}>客户</Text>
                            <Text style={{marginTop:BaseComponent.W*5/375,marginLeft:BaseComponent.W*20/375}}>承担</Text>
                        </View>
                        <View style={{width:BaseComponent.W*75/375,height:BaseComponent.W*70/375,borderBottomWidth:0.5,borderRightWidth:0.5,borderBottomColor:'#888888',borderRightColor:'#888888',justifyContent:'center'}}>
                            <Text style={{marginLeft:BaseComponent.W*20/375}}>客户</Text>
                            <Text style={{marginTop:BaseComponent.W*5/375,marginLeft:BaseComponent.W*20/375}}>承担</Text>
                        </View>
                        <View style={{backgroundColor:"white",width:BaseComponent.W*110/375,height:BaseComponent.W*70/375,borderBottomWidth:0.5,borderRightWidth:0.5,borderBottomColor:'#888888',borderRightColor:'#888888',justifyContent:'center'}}>
                            <Text style={{marginLeft:5}}>一律不接受客户的到付邮件或者平邮邮件。</Text>
                        </View>
                    </View>

                    <View style={{width:BaseComponent.W*355/375,flexDirection:'row'}}>
                        <View style={{width:BaseComponent.W*95/375,height:BaseComponent.W*100/375,borderBottomWidth:0.5,borderRightWidth:0.5,borderBottomColor:'#888888',borderRightColor:'#888888',justifyContent:'center'}}>
                            <Text style={{marginLeft:5}}>物流配送、第三方不可抗力、小洋匠自己的原因</Text>
                        </View>

                        <View style={{width:BaseComponent.W*75/375,height:BaseComponent.W*100/375,borderBottomWidth:0.5,borderRightWidth:0.5,borderBottomColor:'#888888',borderRightColor:'#888888',justifyContent:'center'}}>
                            <Text style={{marginLeft:BaseComponent.W*20/375}}>小洋匠承担</Text>
                         
                        </View>
                        <View style={{width:BaseComponent.W*75/375,height:BaseComponent.W*100/375,borderBottomWidth:0.5,borderRightWidth:0.5,borderBottomColor:'#888888',borderRightColor:'#888888',justifyContent:'center'}}>
                            <Text style={{marginLeft:BaseComponent.W*20/375}}>小洋匠承担</Text>
                            
                        </View>
                        <View style={{backgroundColor:"white",width:BaseComponent.W*110/375,height:BaseComponent.W*100/375,borderBottomWidth:0.5,borderRightWidth:0.5,borderBottomColor:'#888888',borderRightColor:'#888888',justifyContent:'center'}}>
                            <Text style={{marginLeft:5}}>寄出邮费按照商品价格比例分摊。用户退回的邮费将返还到用户的账户。</Text>
                        </View>
                    </View>

                </View>
                <Text style={{fontWeight:"bold",fontSize:15,marginTop:15}}>&nbsp;&nbsp;&nbsp;&nbsp;举例说明:</Text>
                <Text style={{marginTop:5,lineHeight:BaseComponent.W*20/375}}>&nbsp;&nbsp;&nbsp;&nbsp;如果您购买了价格100元的商品A，和价格50元的商品B，邮费为30元。如果为物流配送、第三方不可抗力、小洋匠自己的原因导致的退货。
则A商品退货时，退还的邮费=30*100/（100+50）=20元
B商品退货时，退还的邮费=30*50/（100+50）=10元
如果您将退货商品退回，商品经客服审核不影响二次使用和销售，则在客服确认收货后用户退回的邮费将返还到您的账户。</Text>

                <Text style={{fontSize:BaseComponent.W*20/375,marginTop:BaseComponent.W*20/375}}>&nbsp;&nbsp;&nbsp;&nbsp;税费退还说明</Text>
            
                <View style={{width:BaseComponent.W*355/375,marginTop:BaseComponent.W*20/375,borderWidth:0.5,borderColor:"#888888"}}>
                    <View style={{width:BaseComponent.W*355/375,height:BaseComponent.W*50/375,backgroundColor:"#F0F0F0",flexDirection:'row'}}>
                        <View style={{width:BaseComponent.W*200/375,height:BaseComponent.W*50/375,borderBottomWidth:0.5,borderRightWidth:0.5,borderRightColor:'#888888',borderBottomColor:'#888888'}}>
                            <Text style={{marginTop:BaseComponent.W*20/375,marginLeft:BaseComponent.W*20/375,fontWeight:"bold"}}>退货原因</Text>
                        </View>
                        <View style={{width:BaseComponent.W*155/375,height:BaseComponent.W*50/375,borderBottomWidth:0.5,borderRightWidth:0.5,borderRightColor:'#888888',borderBottomColor:'#888888'}}>
                            <Text style={{marginTop:BaseComponent.W*20/375,marginLeft:BaseComponent.W*20/375,fontWeight:"bold"}}>税费</Text>
                        </View>
                    </View>
                    <View style={{width:BaseComponent.W*355/375,height:BaseComponent.W*50/375,backgroundColor:"white",flexDirection:'row'}}>
                        <View style={{width:BaseComponent.W*200/375,height:BaseComponent.W*50/375,borderBottomWidth:0.5,borderRightWidth:0.5,borderRightColor:'#888888',borderBottomColor:'#888888'}}>
                            <Text style={{marginTop:BaseComponent.W*20/375,marginLeft:BaseComponent.W*20/375}}>客户自己的原因</Text>
                        </View>
                        <View style={{width:BaseComponent.W*155/375,height:BaseComponent.W*50/375,borderBottomWidth:0.5,borderRightWidth:0.5,borderRightColor:'#888888',borderBottomColor:'#888888'}}>
                            <Text style={{marginTop:BaseComponent.W*10/375,marginLeft:BaseComponent.W*20/375}}>如已缴纳税费，退还税费</Text>
                        </View>
                    </View>
                  

                     <View style={{width:BaseComponent.W*355/375,height:BaseComponent.W*50/375,backgroundColor:"white",flexDirection:'row'}}>
                        <View style={{width:BaseComponent.W*200/375,height:BaseComponent.W*50/375,borderBottomWidth:0.5,borderRightWidth:0.5,borderRightColor:'#888888',borderBottomColor:'#888888'}}>
                            <Text style={{marginTop:BaseComponent.W*10/375,marginLeft:BaseComponent.W*10/375}}>物流配送、第三方不可抗力、小洋匠自己的原因</Text>
                        </View>
                        <View style={{width:BaseComponent.W*155/375,height:BaseComponent.W*50/375,borderBottomWidth:0.5,borderRightWidth:0.5,borderRightColor:'#888888',borderBottomColor:'#888888'}}>
                            <Text style={{marginTop:BaseComponent.W*10/375,marginLeft:BaseComponent.W*20/375}}>如已缴纳税费，退还税费</Text>
                        </View>
                    </View>
                </View>
                <Text style={{fontWeight:"bold",fontSize:15,marginTop:15}}>&nbsp;&nbsp;&nbsp;&nbsp;举例说明:</Text>
                 <Text style={{marginTop:5,lineHeight:BaseComponent.W*20/375}}>&nbsp;&nbsp;&nbsp;&nbsp;如果您购买了价格100元的商品A，和价格50元的商品B，A商品税费为50元，B商品税费为25元。
则A商品退货时，退还的税费=50元
B商品退货时，退还的税费=25元</Text>
            </View>
            <View style={{width:'100%',height:BaseComponent.W*30/375,backgroundColor:'#fff'}}></View>  
            </ScrollView> 

      </View>          
    )
  }
}
