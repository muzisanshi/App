/**
 * @name SelectAddressOrDate.js
 * @auhor 程浩
 * @date 2018.8.21
 * @desc 选择售后退款原因界面
 */
import React, { Component } from 'react';
 
import {
    AppRegistry,
    Text,
    View,
    Dimensions,
    StyleSheet,
    Platform,
    BackHandler,
    Animated,
    Easing,
    TouchableWithoutFeedback
} from 'react-native';
import Picker from 'react-native-picker';
import PropTypes from 'prop-types';
import BaseComponent from './BaseComponent';

export default class SelectAddressOrDate extends BaseComponent {
 
    static defaultProps = {
        cancel:'取消',
        title:'请选择原因',
        confirm:'确定',
        pickerBg:[255,255,255,1],
        startYear:2010,
        endYear:2030,
        synchronousRefresh:false
    }
 
    constructor(props){
        super(props);
        this.state={
            xPosition: new Animated.Value(0),
            isShow:false,      //弹框是否显示
            selectedValue:[],  //选择的值  address: ['河北', '唐山', '古冶区']   date: ['2018年', '1月', '1日']
            data:[],
            // businessOrderNo:"",//取消订单的订单号
            // reason:"",//取消的原因    
            // date:[],//选择的时间
            copyData:[],
            id:"",

        }
    };
 
    componentWillMount(){
        console.log("-----------------------------------------------------");
        this._createAreaData();
        if (Platform.OS === 'android') {
            BackHandler.addEventListener('hardwareBackPress', this.onBackAndroid);
        }
    };
    componentDidMount(){
        var thiz=this;
        thiz.listen("cancel_order",function(ret){
            var businessOrderNo = ret.businessOrderNo;
            console.log('--------------------FirstbusinessOrderNo----------------------',businessOrderNo);
            thiz.setState({businessOrderNo:businessOrderNo});
        });
    }
    componentWillUnmount(){
 
        if (Platform.OS === 'android') {
            BackHandler.removeEventListener('hardwareBackPress', this.onBackAndroid);
        }
    };

    onBackAndroid = () => {
        if(this.state.isShow){
            this.hide();
            return true;
        }
        return false
    };
 
 
    /**
     *  在外部调用 显示地址
     * @param pickedValue ['河北', '唐山', '古冶区']
     */
    showAddress(pickedValue){
        if(!this.state.isShow){
            this.state.selectedValue=pickedValue
            this._showAreaPicker()
            this.showAnimal()
            this.state.isShow=true
        }
    }
 
 
    /**
     *  在外部调用 显示地址
     * @param pickedValue ['2018年', '1月', '1日']
     */
    showDate(pickedValue){
        if(!this.state.isShow){
            this.state.selectedValue=pickedValue
            this._showDatePicker()
            this.showAnimal()
            this.state.isShow=true
        }
    }
 
 
 
 
    /**
     * 隐藏地址
     */
    hide(){
        if(this.state.isShow){
            Picker.hide()
            this.hideAnimal()
            this.state.isShow=false
        }
    }
 
 
    /**
     * 地址确认值
     * @param pickedValue  ['河北', '唐山', '古冶区']
     */
    confirmAddressValue(pickedValue){
        this.props.callBackAddressValue&&this.props.callBackAddressValue(pickedValue)
    }
 
    /**
     * 时间确认值
     * @param pickedValue  ['2018年', '1月', '1日']
     */
    confirmDateValue(pickedValue){
        this.props.callBackDateValue&&this.props.callBackDateValue(pickedValue)
    }
   

    _showAreaPicker() {
        let thiz = this;
        Picker.init({
            // pickerData: this._createAreaData(),
            pickerData:this.state.data,
            // pickerData:['收货人信息有误','商品数量或款式需要调整','商品缺货','有更优惠的购买方案','商品缺货','我不想买了','其他原因'],
            selectedValue:this.state.selectedValue,
            pickerTextEllipsisLen:20,
            onPickerConfirm: pickedValue => {
                this.confirmAddressValue(pickedValue);
                console.log("----------------------------------------",pickedValue);
                var copyData=thiz.state.copyData;
                for(var i=0;i<copyData.length;i++){
                    if(pickedValue==copyData[i].value)
                    {
                        thiz.emit("selected_cancel_id",{id:copyData[i].id});
                    }
                }
                thiz.emit("hide_blank");
                setTimeout(function(){
                    thiz.hide()
                },100);               
                
            },
            onPickerCancel: pickedValue => {

                // 发送关闭消息
                thiz.emit("hide_blank");
                setTimeout(function(){
                    thiz.hide()
                },100);  
            },
            onPickerSelect: pickedValue => {
                //Picker.select(['山东', '青岛', '黄岛区'])
                if(this.props.synchronousRefresh){
                    this.confirmAddressValue(pickedValue)
                }
            },
            pickerBg:this.props.pickerBg,
            pickerCancelBtnText:this.props.cancel,
            pickerConfirmBtnText:this.props.confirm,
            pickerTitleText:this.props.title,
            ...this.props
        });

        Picker.show();
    }
 
 
    _showDatePicker() {
        Picker.init({
            pickerData: this._createDateData(),
        
            selectedValue:this.state.selectedValue,
            pickerFontColor: [0, 0 ,0, 1],
            onPickerConfirm: (pickedValue, pickedIndex) => {
                this.confirmDateValue(pickedValue);
                console.log("-----------------------------Date_pickedValue------------------",pickedValue);
                this.setState({date:pickedValue});
                this.hide()
            },
            onPickerCancel: (pickedValue, pickedIndex) => {
                this.emit("cancel_selected_check_integral_date");
                this.hide()
            },
            onPickerSelect: (pickedValue, pickedIndex) => {
                if(this.props.synchronousRefresh){
                    this.confirmDateValue(pickedValue)
                }
            },
            pickerBg:this.props.pickerBg,
            pickerCancelBtnText:this.props.cancel,
            pickerConfirmBtnText:this.props.confirm,
            // pickerTitleText:this.props.title,
            pickerTitleText:"请选择日期",
            ...this.props
        });
        Picker.show();
    }
 
 
    _createAreaData() {
        let thiz = this;
        let data=[];
        let copyData=[]
        thiz.request("refundReason/getAll",{
          method:"POST",
          data:{},
          success:function(ret){  
            console.log("---------------------------------------------------------------------aaaaaaaaaaaaaaaaaaaa-----------------ret---------------------",ret)
              if(!ret.data)
              {
                return ;
              }
              for(var i=0;i<ret.data.length;i++){
                data.push(ret.data[i].value)
              }
              for(var ii=0;ii<ret.data.length;ii++){
                copyData.push(ret.data[ii])
              }
              thiz.setState({data:data,copyData:copyData});
          },
          error:function(err){
             
          }
        })
        
        
    }
    
    _createDateData() {
        let date = [];
        var year=new Date().getFullYear();
        
        for(let i=this.props.startYear;i<year+1;i++){
            let month = [];
            for(let j = 1;j<13;j++){
                // let day = [];
                // if(j === 2){
                //     for(let k=1;k<29;k++){
                //         day.push(k+'日');
                //     }
                //     //Leap day for years that are divisible by 4, such as 2000, 2004
                //     if(i%4 === 0){
                //         day.push(29+'日');
                //     }
                // }
                // else if(j in {1:1, 3:1, 5:1, 7:1, 8:1, 10:1, 12:1}){
                //     for(let k=1;k<32;k++){
                //         day.push(k+'日');
                //     }
                // }
                // else{
                //     for(let k=1;k<31;k++){
                //         day.push(k+'日');
                //     }
                // }
                // let _month = {};
                // _month[j+'月'] = day;
                // month.push(_month);
                month.push(j);
            }
            let _date = {};
            // _date[i+'年'] = month;
            _date[i] = month;
            date.push(_date);
        }
        return date;
    }
    showAnimal(){
        Animated.timing(
            this.state.xPosition,
            {
                toValue: 1,
                easing: Easing.linear,
                duration: 300,
            }
        ).start()
    }
    hideAnimal(){
        Animated.timing(
            this.state.xPosition,
            {
                toValue: 0,
                easing: Easing.linear,
                duration: 200,
            }
        ).start()
    }
    render(){
        return (
            <Animated.View
                style={[styles.contain,{transform: [{
                    translateY: this.state.xPosition.interpolate({
                        inputRange: [0, 1],
                        outputRange: [ Platform.OS === 'ios' ? Dimensions.get('window').height : Dimensions.get('window').height - 24 , 0]
                    }),
                }]
                } ]}>
            </Animated.View>
        )
    }
}
 
const styles = StyleSheet.create({
    contain:{
        width:'100%',
        height: '100%',
        position: 'absolute',
        opacity: 0.3,
        backgroundColor: "#fff",
        left: 0,
        bottom: 0,
    },
});