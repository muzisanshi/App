/**
 * @name ServiceClause.js
 * @auhor 程浩
 * @date 2018.8.24
 * @desc 服务条款
 */
import React,{Component} from 'react';
import {Text,View,StyleSheet,Image,TouchableWithoutFeedback,ScrollView,StatusBar,TextInput,FlatList,Modal,SectionList,ActivityIndicator} from 'react-native';
 // 导入工具函数库
import T from "../lib/Tools";
// 导入公共样式
import St from "../lib/Style";
// 导入图片资源库
import Imgs from "../lib/Img";
import BaseComponent from './BaseComponent';



// Tools实例
let ti = T.getInstance();

let style = StyleSheet.create({
  wrapper:{
    backgroundColor:"white",
    flex:1,
    flexDirection:"column",
    alignItems:"stretch",
    justifyContent:"flex-start",
    position:"relative",
  },
  statusBar:{
    backgroundColor:"rgb(255,255,255)",
    height:ti.select({ios:16,android:22}),
    position:"relative",
    zIndex:1,
    opacity:1,
  },
  navBar:{
    backgroundColor:"rgb(255,255,255)",
    height:"7%",
    position:"relative",
    zIndex:1,
    opacity:1,
    borderBottomWidth:1,
    borderBottomColor:"rgb(241,241,241)"
  },
});
export default class ServiceClause extends BaseComponent{
  render(){
    return (
      <View style={style.wrapper}>
                {/*状态栏占位*/}
                <View style={{
                  backgroundColor:"rgb(255,255,255)",
                  height:this.isIphoneX(30,BaseComponent.SH),
                  position:"relative",
                  zIndex:1,
                  opacity:1,
                }}></View>
                {/*顶部导航栏*/}
                <View style={style.navBar}>
                  <View style={{flex:1,flexDirection:"row",justifyContent:"flex-start"}}>

                    <TouchableWithoutFeedback onPress={()=>this.goBack()}>
                    <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>
                      <Image source={Imgs.back} style={{width:(BaseComponent.W*0.127*0.4),height:(BaseComponent.W*0.127*0.4),marginRight:BaseComponent.W*0.04}}></Image>
                    </View>
                    </TouchableWithoutFeedback>

                    <View style={{flexGrow:1,backgroundColor:"transparent",
                      marginTop:ti.select({ios:(BaseComponent.H*0.02)/2,android:(BaseComponent.H*0.016)/2}),borderRadius:ti.select({ios:(BaseComponent.H*0.05)/2,android:(BaseComponent.H*0.054)/2}),borderColor:"#0A0A0A",
                      borderWidth:0,}}>
                      <Text style={{color:"#0A0A0A",width:"100%",textAlign:"center",marginTop:ti.select({ios:"4.5%",android:"1.6%"}),fontSize:17}}>{this.params.title}</Text>
                    </View>
                    <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>
                      
                    </View>
                  </View>
                </View>
            {/*服务条款*/}
            <ScrollView style={{flex:1}}>
            <View style={{width:BaseComponent.W*355/375,marginLeft:BaseComponent.W*10/375,marginTop:BaseComponent.W*30/375}}>
              	<Text style={{fontWeight:"bold",textAlign:'center'}}>洋匠手机帐号服务条款</Text>
              	<Text style={{marginTop:BaseComponent.W*10/375,lineHeight:BaseComponent.W*20/375}}><Text style={{fontWeight:'bold'}}>&nbsp;&nbsp;&nbsp;&nbsp;【注意】欢迎申请使用洋匠科技提供的服务。</Text>请您（下列简称为“用户”）仔细阅读以下全部内容<Text style={{fontWeight:"bold",textDecorationLine:"underline"}}>（特别是粗体下划线标注的内容）</Text>。如用户不同意本服务条款任意内容，请勿注册或使用洋匠服务。如用户通过进入注册程序并勾选“我同意洋匠手机帐号服务条款”，即表示用户与洋匠科技已达成协议，自愿接受本服务条款的所有内容。此后，用户不得以未阅读本服务条款内容作任何形式的抗辩。</Text>
            	
            	<Text style={{fontWeight:"bold",marginTop:BaseComponent.W*10/375}}>&nbsp;&nbsp;&nbsp;&nbsp;1.服务条款的确认和接纳</Text>	
            	<Text style={{marginTop:BaseComponent.W*10/375,lineHeight:BaseComponent.W*20/375}}>&nbsp;&nbsp;&nbsp;&nbsp;洋匠服务涉及到的洋匠产品的所有权以及相关软件的知识产权归洋匠科技所有。洋匠科技所提供的服务必须按照其发布的科技章程，服务条款和操作规则严格执行。本服务条款的效力范围及于将小洋匠手机帐号作为登录帐号的洋匠产品和服务（以下简称“洋匠各单项服务”），用户在享受洋匠各单项服务时，应当受本服务条款的约束。</Text>
            	<Text>&nbsp;&nbsp;&nbsp;&nbsp;<Text style={{marginTop:BaseComponent.W*10/375,fontWeight:"bold",textDecorationLine:"underline",lineHeight:BaseComponent.W*20/375}}>当用户使用洋匠各单项服务时，用户同意以单项服务要求的方式同意该单项服务的服务条款以及洋匠科技在该单项服务中发出的各类公告（下列简称为“单项条款”），在此情况下单项条款与本服务条款同时对用户产生效力。若单项条款与本服务条款存在同类条款的冲突，则在单项条款约束范围内应以单项条款为准。</Text></Text>	
            	
            	<Text style={{fontWeight:"bold",marginTop:BaseComponent.W*10/375}}>&nbsp;&nbsp;&nbsp;&nbsp;2.小洋匠手机帐号服务简介</Text>
            	<Text style={{fontWeight:"bold",marginTop:BaseComponent.W*10/375,lineHeight:BaseComponent.W*20/375}}>&nbsp;&nbsp;&nbsp;&nbsp;本服务条款所称的“小洋匠手机帐号”是指用户以手机号码作为帐号注册的合法、有效的帐号，小洋匠手机帐号注册成功后帐号即为手机号。</Text>
            
            	<Text style={{marginTop:BaseComponent.W*10/375,lineHeight:BaseComponent.W*20/375}}>&nbsp;&nbsp;&nbsp;&nbsp;洋匠科技运用自己的操作系统通过国际互联网络为用户提供各项服务。用户必须：</Text>
            	<Text style={{marginTop:BaseComponent.W*10/375}}>(1)提供设备，如个人电脑、手机或其他上网设备。</Text>
				<Text style={{marginTop:BaseComponent.W*10/375}}>(2)个人上网和支付与此服务有关的费用。</Text>	

				<Text style={{fontWeight:"bold",marginTop:BaseComponent.W*10/375}}>&nbsp;&nbsp;&nbsp;&nbsp;3.小洋匠手机帐号注册规则</Text>
           		<Text style={{marginTop:BaseComponent.W*10/375,lineHeight:BaseComponent.W*20/375}}>&nbsp;&nbsp;&nbsp;&nbsp;小洋匠手机帐号注册资料包括但不限于：用户的手机号码、密码及注册洋匠手机帐号或更新小洋匠手机帐号注册资料时输入的所有信息或更新小洋匠手机帐号时输入的所有信息以及用户使用小洋匠各单项服务时输入的名称、头像等所有信息。</Text>
            	<Text style={{marginTop:BaseComponent.W*10/375,lineHeight:BaseComponent.W*20/375}}>&nbsp;&nbsp;&nbsp;&nbsp;用户在注册小洋匠手机帐号时承诺遵守法律法规、社会主义制度、国家利益、公民合法权益、公共秩序、社会道德风尚和信息真实性等七条底线，不得在小洋匠手机帐号注册资料中出现违法和不良信息，且用户保证其在注册和使用帐号时，不得有以下情形：</Text>
            	<Text style={{marginTop:BaseComponent.W*10/375}}>(1)违反宪法或法律法规规定的;</Text>
            	<Text style={{marginTop:BaseComponent.W*10/375}}>(2)危害国家安全，泄露国家秘密，颠覆国家政权，破坏国家统一的;</Text>
            	<Text style={{marginTop:BaseComponent.W*10/375}}>(3)损害国家荣誉和利益的，损害公共利益的;</Text>
            	<Text style={{marginTop:BaseComponent.W*10/375}}>(4)煽动民族仇恨、民族歧视，破坏民族团结的;</Text>
            	<Text style={{marginTop:BaseComponent.W*10/375}}>(5)破坏国家宗教政策，宣扬邪教和封建迷信的;</Text>
            	<Text style={{marginTop:BaseComponent.W*10/375}}>(6)散布谣言，扰乱社会秩序，破坏社会稳定的;</Text>
            	<Text style={{marginTop:BaseComponent.W*10/375}}>(7)散布淫秽、色情、赌博、暴力、凶杀、恐怖或者教唆犯罪的;</Text>
            	<Text style={{marginTop:BaseComponent.W*10/375}}>(8)侮辱或者诽谤他人，侵害他人合法权益的;</Text>
            	<Text style={{marginTop:BaseComponent.W*10/375}}>(9)含有法律、行政法规禁止的其他内容的。</Text>
				
				<Text style={{marginTop:BaseComponent.W*10/375,lineHeight:BaseComponent.W*20/375}}>若用户提供给洋匠科技的帐号注册资料不准确，不真实，含有违法或不良信息的，洋匠科技有权不予注册，并保留终止用户使用洋匠各项服务的权利。若用户以虚假信息骗取帐号注册或帐号头像、个人简介等注册资料存在违法和不良信息的，洋匠科技有权采取通知限期改正、暂停使用、注销登记等措施。对于冒用关联机构或社会名人注册帐号名称的，洋匠科技有权注销该帐号，并向政府主管部门进行报告。</Text>
            	<Text style={{marginTop:BaseComponent.W*10/375,lineHeight:BaseComponent.W*20/375}}>根据相关法律、法规规定以及考虑到洋匠产品服务的重要性，用户同意：</Text>
            	
            	<Text style={{marginTop:BaseComponent.W*10/375}}>(1)在注册洋匠手机帐号时提交个人有效身份信息进行实名认证;</Text>
            	<Text style={{marginTop:BaseComponent.W*10/375}}>(2)提供及时、详尽及准确的帐户注册资料;</Text>
            	<Text style={{marginTop:BaseComponent.W*10/375,lineHeight:BaseComponent.W*20/375}}>(3)不断更新注册资料，符合及时、详尽准确的要求，对注册洋匠手机帐号时填写的身份证件信息不能更新。</Text>
            
				<Text style={{marginTop:BaseComponent.W*10/375}}>&nbsp;&nbsp;&nbsp;&nbsp;<Text style={{fontWeight:"bold",textDecorationLine:"underline",lineHeight:BaseComponent.W*20/375}}>用户同意，其提供的真实、准确、合法的洋匠手机帐号注册资料作为认定用户与其洋匠手机帐号的关联性以及用户身份的唯一证据。用户在享用洋匠各项服务的同时，同意接受洋匠提供的各类信息服务。洋匠科技提醒用户，用户注册洋匠手机帐号或更新注册信息时填写的证件号码，在注册洋匠手机帐号成功或补充填写后将无法进行修改，请用户慎重填写各类注册信息。</Text></Text>
            	
            	<Text style={{marginTop:BaseComponent.W*10/375}}>&nbsp;&nbsp;&nbsp;&nbsp;<Text style={{fontWeight:"bold",textDecorationLine:"underline",lineHeight:BaseComponent.W*20/375}}>用户同意，在用户通过洋匠科技公布的线上渠道或通过客服申诉并提交洋匠科技要求的身份认证信息材料的，则该用户原洋匠手机帐号下的所有使用记录及充值数据可以转移到该用户提供的，未注册过洋匠手机帐号的新手机号关联的新洋匠手机帐号并自行承担因此等转移可能产生的后果。</Text></Text>
				
				<Text style={{marginTop:BaseComponent.W*10/375}}>&nbsp;&nbsp;&nbsp;&nbsp;<Text style={{fontWeight:"bold",textDecorationLine:"underline",lineHeight:BaseComponent.W*20/375}}>为使用户及时、全面了解洋匠科技提供的各项服务，用户同意，洋匠科技可以多次、长期向用户发送各类商业性短信息而无需另行获得用户的同意。用户同意与注册、使用洋匠手机帐号相关的一切资料、数据和记录，包括但不限于手机帐号、注册信息、所有登录、消费记录和相关的使用统计数字等归洋匠科技所有。发生争议时，用户同意以洋匠科技的系统数据为准，洋匠科技保证该数据的真实性。</Text></Text>
				<Text style={{marginTop:BaseComponent.W*10/375}}>&nbsp;&nbsp;&nbsp;&nbsp;<Text style={{fontWeight:"bold",textDecorationLine:"underline",lineHeight:BaseComponent.W*20/375}}>尽管有前述约定，对于用户使用洋匠手机帐号享受洋匠科技旗下单项服务时产生的一切数据，包括但不限于产品登录记录、消费记录及其他产品日志、产品客户服务记录、用户在产品中创造的社会网络内容等，归具体产品运营主体所有。发生争议时，用户同意以具体产品运营主体的系统数据为准。但是如果单项条款存在与前述不同的约定，则以单项条款约定为准。</Text></Text>
            
				<Text style={{fontWeight:"bold",marginTop:BaseComponent.W*10/375}}>&nbsp;&nbsp;&nbsp;&nbsp;4.服务条款的修改</Text>
            	<Text style={{marginTop:BaseComponent.W*10/375}}>&nbsp;&nbsp;&nbsp;&nbsp;<Text style={{fontWeight:"bold",textDecorationLine:"underline",lineHeight:BaseComponent.W*20/375}}>洋匠科技有权在必要时通过在网页上发出公告等合理方式修改本服务条款以及各单项服务的相关条款。用户在享受各项服务时，应当及时查阅了解修改的内容，并自觉遵守本服务条款以及该单项服务的相关条款。用户如继续使用本服务条款涉及的服务，则视为对修改内容的同意，当发生有关争议时，以最新的服务条款为准；用户在不同意修改内容的情况下，有权停止使用本服务条款涉及的服务。</Text></Text>
				
				<Text style={{fontWeight:"bold",marginTop:BaseComponent.W*10/375}}>&nbsp;&nbsp;&nbsp;&nbsp;5.服务的变更或中止</Text>
            	<Text style={{marginTop:BaseComponent.W*10/375}}>&nbsp;&nbsp;&nbsp;&nbsp;<Text style={{fontWeight:"bold",textDecorationLine:"underline",lineHeight:BaseComponent.W*20/375}}>洋匠科技始终在不断变更和改进服务。洋匠科技可能会增加或删除功能，也可能暂停或彻底停止某项服务。用户同意洋匠科技有权行使上述权利且不需对用户或第三方承担任何责任。</Text></Text>
            
            	<Text style={{fontWeight:"bold",marginTop:BaseComponent.W*10/375}}>&nbsp;&nbsp;&nbsp;&nbsp;6.用户隐私制度</Text>
            	<Text style={{marginTop:BaseComponent.W*10/375}}>&nbsp;&nbsp;&nbsp;&nbsp;<Text style={{fontWeight:"bold",textDecorationLine:"underline",lineHeight:BaseComponent.W*20/375}}>尊重用户隐私是洋匠科技的一项基本政策。洋匠科技将按照公布的隐私政策收集、存储、使用、披露和保护您的个人信息。请您完整阅读上述隐私权政策，以帮助您更好地保护您的个人信息。</Text></Text>
            
            	<Text style={{fontWeight:"bold",marginTop:BaseComponent.W*10/375}}>&nbsp;&nbsp;&nbsp;&nbsp;7.用户的帐号、密码和安全性</Text>	
            	<Text style={{marginTop:BaseComponent.W*10/375,lineHeight:BaseComponent.W*20/375}}>&nbsp;&nbsp;&nbsp;&nbsp;用户可随时改变用户的密码和图标，也可以结束旧的帐户重开一个新帐户。用户应维持密码及洋匠手机帐号的机密安全，如果用户未保管好自己的帐号和密码而对用户、洋匠科技或第三方造成损害，用户将负全部责任。用户同意若发现任何非法使用用户帐号或安全漏洞的情况，有义务立即通告洋匠科技。</Text>
            
				<Text style={{fontWeight:"bold",marginTop:BaseComponent.W*10/375}}>&nbsp;&nbsp;&nbsp;&nbsp;8.不可抗力条款</Text>
            	<Text style={{marginTop:BaseComponent.W*10/375}}>&nbsp;&nbsp;&nbsp;&nbsp;<Text style={{fontWeight:"bold",textDecorationLine:"underline",lineHeight:BaseComponent.W*20/375}}>洋匠科技对不可抗力导致的损失不承担责任。本服务条款所指不可抗力包括：天灾、法律法规或政府指令的变更，因网络服务特性而特有的原因，例如境内外基础电信运营商的故障、计算机或互联网相关技术缺陷、互联网覆盖范围限制、计算机病毒、黑客攻击等因素，及其他合法范围内的不能预见、不能避免并不能克服的客观情况。</Text></Text>

            	<Text style={{fontWeight:"bold",marginTop:BaseComponent.W*10/375}}>&nbsp;&nbsp;&nbsp;&nbsp;9.禁止服务的商业化</Text>
            	<Text style={{marginTop:BaseComponent.W*10/375}}>&nbsp;&nbsp;&nbsp;&nbsp;<Text style={{fontWeight:"bold",textDecorationLine:"underline",lineHeight:BaseComponent.W*20/375}}>用户承诺，非经洋匠科技同意，用户不能利用洋匠各项服务进行销售或其他商业用途。如用户有需要将服务用于商业用途，应书面通知洋匠并获得洋匠的明确授权。</Text></Text>
            	
            	<Text style={{fontWeight:"bold",marginTop:BaseComponent.W*10/375}}>&nbsp;&nbsp;&nbsp;&nbsp;10.用户管理</Text>
            	<Text style={{marginTop:BaseComponent.W*10/375,lineHeight:BaseComponent.W*20/375}}>用户独立承担其发布内容的责任。用户对服务的使用必须遵守所有适用于服务的地方法律、国家法律和国际法律。用户承诺：</Text>
            	
            	<Text style={{marginTop:BaseComponent.W*10/375,lineHeight:BaseComponent.W*20/375}}>(1)用户在洋匠的网页上发布信息或者利用洋匠的服务时必须符合中国有关法规，不得在洋匠的网页上或者利用洋匠的服务制作、复制、发布、传播以下信息：</Text>
            	<Text style={{marginTop:BaseComponent.W*5/375,lineHeight:BaseComponent.W*20/375}}>(a)违反宪法确定的基本原则的;</Text>
            	<Text style={{marginTop:BaseComponent.W*5/375,lineHeight:BaseComponent.W*20/375}}>(b)危害国家安全，泄露国家秘密，颠覆国家政权，破坏国家统一的;</Text>
            	<Text style={{marginTop:BaseComponent.W*5/375,lineHeight:BaseComponent.W*20/375}}>(c)损害国家荣誉和利益的;</Text>
            	<Text style={{marginTop:BaseComponent.W*5/375,lineHeight:BaseComponent.W*20/375}}>(d)煽动民族仇恨、民族歧视，破坏民族团结的;</Text>
            	<Text style={{marginTop:BaseComponent.W*5/375,lineHeight:BaseComponent.W*20/375}}>(e)破坏国家宗教政策，宣扬邪教和封建迷信的;</Text>
            	<Text style={{marginTop:BaseComponent.W*5/375,lineHeight:BaseComponent.W*20/375}}>(f)散布谣言，扰乱社会秩序，破坏社会稳定的;</Text>
            	<Text style={{marginTop:BaseComponent.W*5/375,lineHeight:BaseComponent.W*20/375}}>(g)散布淫秽、色情、赌博、暴力、恐怖或者教唆犯罪的;</Text>
            	<Text style={{marginTop:BaseComponent.W*5/375,lineHeight:BaseComponent.W*20/375}}>(h) 侮辱或者诽谤他人，侵害他人合法权益的;</Text>
            	<Text style={{marginTop:BaseComponent.W*5/375,lineHeight:BaseComponent.W*20/375}}>(i) 煽动非法集会、结社、游行、示威、聚众扰乱社会秩序的;</Text>
            	<Text style={{marginTop:BaseComponent.W*5/375,lineHeight:BaseComponent.W*20/375}}>(j) 以非法民间组织名义活动的;</Text>
            	<Text style={{marginTop:BaseComponent.W*5/375,lineHeight:BaseComponent.W*20/375}}>(k) 含有法律、行政法规禁止的其他内容的。</Text>
            	
            	<Text style={{marginTop:BaseComponent.W*10/375,lineHeight:BaseComponent.W*20/375}}>(2)用户在洋匠的网页上发布信息或者利用洋匠的服务时还必须符合其他有关国家和地区的法律规定以及国际法的有关规定。用户需遵守法律法规的规定使用洋匠微、博客服务。</Text>
            	<Text style={{marginTop:BaseComponent.W*10/375,lineHeight:BaseComponent.W*20/375}}>(3)用户不得利用洋匠的服务从事以下活动：</Text>
            	<Text style={{marginTop:BaseComponent.W*5/375,lineHeight:BaseComponent.W*20/375}}>(a)未经允许，进入计算机信息网络或者使用计算机信息网络资源的;</Text>
            	<Text style={{marginTop:BaseComponent.W*5/375,lineHeight:BaseComponent.W*20/375}}>(b)未经允许，对计算机信息网络功能进行删除、修改或者增加的;</Text>
            	<Text style={{marginTop:BaseComponent.W*5/375,lineHeight:BaseComponent.W*20/375}}>(c)未经允许，对进入计算机信息网络中存储、处理或者传输的数据和应用程序进行删除、修改或者增加的;</Text>
            	<Text style={{marginTop:BaseComponent.W*5/375,lineHeight:BaseComponent.W*20/375}}>(d)故意制作、传播计算机病毒等破坏性程序的;</Text>
            	<Text style={{marginTop:BaseComponent.W*5/375,lineHeight:BaseComponent.W*20/375}}>(e) 其他危害计算机信息网络安全的行为。</Text>

            	<Text style={{marginTop:BaseComponent.W*10/375,lineHeight:BaseComponent.W*20/375}}>(4)用户不得以任何方式干扰洋匠的服务。</Text>
            	<Text style={{marginTop:BaseComponent.W*10/375,lineHeight:BaseComponent.W*20/375}}>(5)用户不得滥用洋匠服务，包括但不限于：利用洋匠服务进行侵害他人知识产权或者合法利益的其他行为。</Text>
				<Text style={{marginTop:BaseComponent.W*10/375,lineHeight:BaseComponent.W*20/375}}>(6)用户应遵守洋匠的所有其他规定和程序。</Text>
				<Text style={{marginTop:BaseComponent.W*10/375,lineHeight:BaseComponent.W*20/375}}>&nbsp;&nbsp;&nbsp;&nbsp;用户须对自己在使用洋匠服务过程中的行为承担法律责任。用户承担法律责任的形式包括但不限于：对受到侵害者进行赔偿，以及在洋匠科技首先承担了因用户行为导致的行政处罚或侵权损害赔偿责任后，用户应给予洋匠科技等额的赔偿。若用户违反以上规定，洋匠有权作出独立判断立即暂停或终止对用户提供部分或全部服务，包括冻结、取消用户服务帐号等措施。用户理解，如果洋匠发现其网站传输的信息明显属于上段第(1)条所列内容之一，依据中国法律，洋匠有义务立即停止传输，保存有关记录，向国家有关机关报告，并且删除含有该内容的地址、目录或关闭服务器。</Text>
				<Text style={{marginTop:BaseComponent.W*10/375,lineHeight:BaseComponent.W*20/375}}>&nbsp;&nbsp;&nbsp;&nbsp;用户使用洋匠电子公告服务，包括电子布告牌、电子白板、电子论坛、网络聊天室和留言等以交互形式为上网用户提供信息发布条件的行为，也须遵守本条的规定以及洋匠将专门发布的电子公告服务规则，上段中描述的法律后果和法律责任同样适用于电子公告服务的用户。</Text>
            
				<Text style={{fontWeight:'bold',marginTop:BaseComponent.W*10/375}}>&nbsp;&nbsp;&nbsp;&nbsp;11.保障</Text>
				<Text style={{marginTop:BaseComponent.W*10/375,lineHeight:BaseComponent.W*20/375}}>&nbsp;&nbsp;&nbsp;&nbsp;用户同意保障和维护洋匠科技全体成员的利益，承担由用户违反服务条款的损害补偿费用，其它人使用用户的电脑、手机、帐号而产生的费用。用户或者使用用户帐号的其他人在进行游戏过程中侵犯第三方知识产权及其他权利而导致被侵权人索赔的，由用户承担责任。</Text>
            	<Text style={{marginTop:BaseComponent.W*10/375}}>&nbsp;&nbsp;&nbsp;&nbsp;<Text style={{fontWeight:"bold",lineHeight:BaseComponent.W*20/375,textDecorationLine:"underline"}}>如用户或其它网络服务提供者利用洋匠科技的服务侵害他人民事权益的，应当承担侵权等法律责任。</Text></Text>
            	<Text style={{marginTop:BaseComponent.W*10/375}}>&nbsp;&nbsp;&nbsp;&nbsp;<Text style={{fontWeight:"bold",lineHeight:BaseComponent.W*20/375,textDecorationLine:"underline"}}>如用户利用洋匠科技的服务实施侵权行为的，被侵权人有权通知洋匠科技采取删除、屏蔽、断开链接等必要措施。洋匠科技接到通知后，因自身过错若未及时采取必要措施的，按法律规定承担责任。</Text></Text>
            	
            	<Text style={{marginTop:BaseComponent.W*10/375,fontWeight:"bold"}}>&nbsp;&nbsp;&nbsp;&nbsp;12.通知</Text>
            	<Text style={{marginTop:BaseComponent.W*10/375,lineHeight:BaseComponent.W*20/375}}>&nbsp;&nbsp;&nbsp;&nbsp;所有发给用户的通知都可通过电子邮件、常规的信件或在网站显著位置公告的方式进行传送。洋匠科技将通过上述方法之一将消息传递给用户，告知他们服务条款的修改、服务变更、或其它重要事情。</Text>
            
				<Text style={{marginTop:BaseComponent.W*10/375,fontWeight:"bold"}}>&nbsp;&nbsp;&nbsp;&nbsp;13.内容、商标的所有权</Text>
            	<Text style={{marginTop:BaseComponent.W*10/375,lineHeight:BaseComponent.W*20/375}}>&nbsp;&nbsp;&nbsp;&nbsp;洋匠科技提供的内容包括但不限于：非用户上传/提供的文字、软件、声音、相片、视频、图表等。所有这些内容均属于洋匠科技，并受版权、商标、专利和其它财产所有权法律的保护。所以，用户只能在洋匠科技授权下才能使用这些内容，而不能擅自复制、再造这些内容、或创造与内容有关的派生产品。</Text>
            	<Text style={{marginTop:BaseComponent.W*10/375,lineHeight:BaseComponent.W*20/375}}>&nbsp;&nbsp;&nbsp;&nbsp;未经洋匠科技事先书面同意，您不得将洋匠标识以任何方式展示或使用或作其他处理，也不得向他人表明您有权展示、使用、或其他有权处理洋匠标识的行为。</Text>
            
				<Text style={{marginTop:BaseComponent.W*10/375,fontWeight:"bold"}}>&nbsp;&nbsp;&nbsp;&nbsp;14.法律</Text>

				<Text style={{marginTop:BaseComponent.W*10/375}}>&nbsp;&nbsp;&nbsp;&nbsp;<Text style={{fontWeight:"bold",lineHeight:BaseComponent.W*20/375,textDecorationLine:"underline"}}>本条款适用中华人民共和国的法律，并且排除一切冲突法规定的适用。</Text></Text>
				<Text style={{marginTop:BaseComponent.W*10/375}}>&nbsp;&nbsp;&nbsp;&nbsp;<Text style={{fontWeight:"bold",lineHeight:BaseComponent.W*20/375,textDecorationLine:"underline"}}>如使用本条款项下服务中出现纠纷的，用户与洋匠科技应友好协商解决，若协商不成，用户同意将纠纷提交中国国际经济贸易仲裁委员会仲裁解决，并由3名仲裁员进行审理。仲裁裁决是终局的，对双方均有约束力。若单项条款与本服务条款在管辖约定内容上存在冲突，则在单项条款约束范围内应以单项条款为准。</Text></Text>

				<Text style={{marginTop:BaseComponent.W*10/375,fontWeight:"bold"}}>&nbsp;&nbsp;&nbsp;&nbsp;15.信息储存及相关知识产权</Text>
				<Text style={{marginTop:BaseComponent.W*10/375}}>&nbsp;&nbsp;&nbsp;&nbsp;<Text style={{fontWeight:"bold",lineHeight:BaseComponent.W*20/375,textDecorationLine:"underline"}}>洋匠科技对洋匠手机帐号上所有服务将尽力维护其安全性及方便性，但对服务中出现的信息（包括但不限于用户发布的信息）删除或储存失败不承担任何责任。另外洋匠科技有权判定用户的行为是否符合本服务条款的要求，如果用户违背了本服务条款的规定，洋匠科技有权中止或者终止对其洋匠手机帐号的服务。</Text></Text>
				<Text style={{marginTop:BaseComponent.W*10/375}}>&nbsp;&nbsp;&nbsp;&nbsp;<Text style={{fontWeight:"bold",lineHeight:BaseComponent.W*20/375,textDecorationLine:"underline"}}>洋匠科技尊重知识产权并注重保护用户享有的各项权利。在洋匠手机帐号所含服务中，用户可能需要通过上传、发布等各种方式向洋匠科技提供内容。在此情况下，用户仍然享有此等内容的完整知识产权。用户在提供内容时将授予洋匠科技一项全球性的免费许可，允许洋匠科技使用、传播、复制、修改、再许可、翻译、创建衍生作品、出版、表演及展示此等内容。</Text></Text>
            
				<Text style={{marginTop:BaseComponent.W*10/375,fontWeight:"bold"}}>&nbsp;&nbsp;&nbsp;&nbsp;16.青少年用户特别提示</Text>
				<Text style={{marginTop:BaseComponent.W*10/375,lineHeight:BaseComponent.W*20/375}}>&nbsp;&nbsp;&nbsp;&nbsp;青少年用户必须遵守全国青少年网络文明公约:</Text>
				<Text style={{marginTop:BaseComponent.W*10/375,lineHeight:BaseComponent.W*20/375}}>&nbsp;&nbsp;&nbsp;&nbsp;要善于网上学习，不浏览不良信息；要诚实友好交流，不侮辱欺诈他人；要增强自护意识，不随意约会网友；要维护网络安全，不破坏网络秩序；要有益身心健康，不沉溺虚拟时空。</Text>		
            	
            	<Text style={{marginTop:BaseComponent.W*10/375,fontWeight:"bold"}}>&nbsp;&nbsp;&nbsp;&nbsp;17.其他</Text>
            	<Text style={{marginTop:BaseComponent.W*10/375,lineHeight:BaseComponent.W*20/375}}>&nbsp;&nbsp;&nbsp;&nbsp;(1)洋匠科技不行使、未能及时行使或者未充分行使本条款或者按照法律规定所享有的权利，不应被视为放弃该权利，也不影响洋匠科技在将来行使该权利。</Text>
            	<Text style={{marginTop:BaseComponent.W*10/375,lineHeight:BaseComponent.W*20/375}}>&nbsp;&nbsp;&nbsp;&nbsp;(2)如本条款中的任何条款无论因何种原因完全或部分无效或不具有执行力，本服务条款的其余条款仍应有效且具有约束力，洋匠科技及用户均应尽力使该条款设定的本意得到实现。</Text>
            	<Text style={{marginTop:BaseComponent.W*10/375,lineHeight:BaseComponent.W*20/375}}>&nbsp;&nbsp;&nbsp;&nbsp;(3)本条款中的标题仅为方便而设，不作为解释本条款的依据。</Text>
            </View>
            <View style={{width:'100%',height:BaseComponent.W*30/375,backgroundColor:'#fff'}}></View>  
            </ScrollView> 

      </View>          
    )
  }
}
