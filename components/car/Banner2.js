/**
 * by zh at time 7.24
 */

import React,{Component} from "react";
import {Image,Text,View,StyleSheet,ScrollView,Button,Dimensions,ImageBackground,SectionList,Alert,TouchableWithoutFeedback} from 'react-native';
import Cart from '../Cart';
const windowWidth= Dimensions.get('window').width;
const windowHeight= Dimensions.get('window').height;
let sections = [
    {
        key: "韩国自营仓",allselected:false,
        data: [ {number: 1, img: require('../../src/x1x1.png'),jieshao:'108色号', money: '360123',isCheck:false}, 
                {number: 1,  img: require('../../src/xs2.png'), jieshao:'黑色',money: '180', isCheck:false},
                {number: 5, img: require('../../src/xs1.png'),jieshao:'马鞭草',  money: '90' , isCheck:false}],
        discounts:5,
        total:40,
    },
    {
        key: "澳洲自营仓",allselected:false,
        data: [ {number: 0, img: require('../../src/x1x1.png'), jieshao:'马鞭草',money: '260563', isCheck:false}, 
                {number: 6, img: require('../../src/x1x1.png'),jieshao:'马鞭草', money: '960', isCheck:false}, 
                {number: 1, img: require('../../src/x1x1.png'),jieshao:'马鞭草', money: '260', isCheck:false}],
        discounts:25,
        total:14,
    },
    {
        key: "德国自营仓",allselected:false,
        data: [ {number: 1, img: require('../../src/x1x1.png'), jieshao:'马鞭草',money: '4360', isCheck:false}, 
                {number: 1, img: require('../../src/xs4.png'), jieshao:'马鞭草', money: '1360', isCheck:false}],
        discounts:35,
        total:19,
    },
    {
        key: "美国自营仓",allselected:false,
        data: [{number: 1, img: require('../../src/x1x1.png'),jieshao:'马鞭草', money: '160',  isCheck:false}, 
               {number: 1, img: require('../../src/x1x1.png'),jieshao:'马鞭草', money: '3060', isCheck:false}, 
               {number: 1, img: require('../../src/x1x1.png'),jieshao:'马鞭草', money: '360',  isCheck:false}, 
               {number: 1, img: require('../../src/x1x1.png'),jieshao:'马鞭草', money: '360',  isCheck:false}, 
               {number: 1, img: require('../../src/x1x1.png'),jieshao:'马鞭草', money: '360',  isCheck:false}, 
               {number: 1, img: require('../../src/x1x1.png'),jieshao:'马鞭草', money: '360',  isCheck:false}],
        discounts:30,
        total:5,
    },
];
export default class ShopCar extends Component {
    static  navigationOptions = {
        headerTitle: '国家馆',
        header: null,
        headerTitleStyle: {
            color: 'white',
            fontSize: 18
        },
        headerStyle: {
            backgroundColor: '#ffffff'
        },
    };
    //生成下标
    _keyExtractor = (item, index) => index;
    constructor(props) {
        super(props);
        this.state = {
            refreshing: false,
            datasource:sections,
            AllChoose:false,
            Allcheck:false,
        };
    }
    // console.log();
    _zengjia = (info) => {
        info.item.number=info.item.number+1;
        // info.section.total=info.item.number*info.item.money;
        this.setState({})
    };
    _jianshao=(info)=>{
        if(info.item.number>1) {
            info.item.number = info.item.number - 1;
        }
        this.setState({})
    };
    pressItem=(info)=>{
         info.item.isCheck=!info.item.isCheck;
         //section是取一个key当中的所有isCheck,item则是去点击的某一个
            if(info.item.isCheck)
             {
                 this.setState({
                     Allcheck:true
                 })
             }else{
                 this.setState({
                     Allcheck:false
                 })
             }
        this.setState({ })
    };
    selected=(info)=>{
      if(info.section.allselected){
          for(let i=0;i<info.section.data.length;i++)
          {
              info.section.data[i].isCheck=false;
          }
          info.section.allselected=!info.section.allselected;
        }
        else
        {
            for(let i=0;i<info.section.data.length;i++)
            {
                info.section.data[i].isCheck=true;
            }
            info.section.allselected= !info.section.allselected;
        }
        for(let j=0;j<info.section.data.length;j++)
        {
            if(info.section.data[j].isCheck)
            {
                this.setState({
                    Allcheck:true
                })
            }else{
                this.setState({
                    Allcheck:false
                })
            }
        }
        this.setState({

        });
    };
    chooseAll=()=>{
        console.log(windowWidth);
        let info=this.state.datasource;
        console.log(info);
        if(!this.state.AllChoose){
            for(let i=0;i<info.length;i++){
                    info[i].allselected=true;
                 for(let j=0;j<info[i].data.length;j++)
                 {
                     info[i].data[j].isCheck=true
                 }
            }
        }else{
            for(let i=0;i<info.length;i++){
                info[i].allselected=false;
                for(let j=0;j<info[i].data.length;j++)
                {
                    info[i].data[j].isCheck=false
                  
                }
            }
        }
        for(let j=0;j<info.length;j++)
        {
            if(info[j].allselected)
            {
                this.setState({
                    Allcheck:true
                })
            }else{
                this.setState({
                    Allcheck:false
                })
            }
        }
        this.setState({
             AllChoose:!this.state.AllChoose,
        })
    };


    _renderItem = (info) => {
        return (
            <View>
                <View style={{flexDirection: 'row', margin: 10, height: 100, alignItems: 'center'}}>
                    <TouchableWithoutFeedback onPress={()=> this.pressItem(info)}>
                   <Image style={styles.img3} source={info.item.isCheck? require('../../src/t2.png') : require('../../src/t1.png')}/>
                    </TouchableWithoutFeedback>
                    <Image style={{width: windowWidth / 5, height: windowWidth / 5}} source={info.item.img}/>
                    <View style={{width: windowWidth / 2.3, margin: 5}}>
                        <View style={{flexDirection: 'row'}}>
                            <TouchableWithoutFeedback onPress={()=>this._jianshao(info)}>
                                <Image style={styles.img5} source={info.item.number>1?require('../../src/j1.png') : require('../../src/j5.png')}></Image>
                            </TouchableWithoutFeedback>
                            <ImageBackground style={styles.img51} source={require('../../src/j2.png')}>
                                <Text style={{fontSize: 17,color:'#212121'}}>
                                    {info.item.number}
                                </Text>
                            </ImageBackground>
                            <TouchableWithoutFeedback onPress={()=>this._zengjia(info)}>
                                <Image style={styles.img5} source={require('../../src/j3.png')}></Image>
                            </TouchableWithoutFeedback>
                        </View>
                        <Text style={{color: '#999999', marginTop: 10}}>{info.item.jieshao}</Text>
                    </View>
                    <View style={{marginTop: -windowHeight / 30,width:windowWidth*1/4,alignItems:'center'}}>
                        <Text style={{color:'#3A3A3A'}}>¥{info.item.money}</Text>

                        <Image style={{width: windowWidth / 14, height: windowWidth / 14}}
                               source={require('../../src/j4.png')}></Image>

                    </View>
                </View>
                <View style={{
                    width: windowWidth / 1,
                    height: 1,
                    backgroundColor: '#d6d6d6',
                    marginLeft: windowWidth / 10
                }}>
                </View>
                <View style={{}}>
                </View>

            </View>
        )
    }
    _sectionComp = (info) => {
        return (
            <View>
                
                <View style={{flexDirection: 'row', margin: 10, alignItems: 'center'}}>
                    <TouchableWithoutFeedback onPress={()=>this.selected(info)}>
                        <Image style={styles.img3} source={info.section.allselected?require('../../src/t2.png'):require('../../src/t1.png')}></Image>
                    </TouchableWithoutFeedback>
                    <Image style={styles.img3} source={require('../../src/1-1.png')}/>
                    <Text>{info.section.key}</Text>
                </View>
                <View style={{width: windowWidth / 1, height: 1, backgroundColor: '#F0F0F0'}}>
                </View>
            </View>
        )
    };

    _ListFooterComponent =(info)=>{
         
        return(
                <View>
                        <View style={{marginTop:10}}>
                              <Text style={{fontSize:12 ,color:'#3A3A3A' ,marginLeft:windowWidth/1.45}}>活动优惠： -¥{info.section.discounts}.00</Text>

                              <Text style={{fontSize:12 ,color:'#3A3A3A', marginTop:10,marginBottom:10,marginLeft:windowWidth/1.76}}>本仓总计 (含邮费) : ¥{info.section.total}.00</Text>
                          </View>
                          <View style={{height: 11, width: windowWidth / 1, backgroundColor: '#F0F0F0'}}>
                        </View>
                </View>
            )
    };
    render() {
        const {navigation} = this.props;

        return (
            <View style={{flex: 1, backgroundColor: 'white'}}>
                <View style={styles.daohanglan}>
                    <View style={styles.biaoti}>
                        <Text style={styles.ziti1}>
                            购物车
                        </Text>
                    </View>
                    <View style={styles.bianji}>
                        <Text style={styles.ziti2}
                              onPress={() => navigation.navigate('Cart')}>
                            完成
                        </Text>
                    </View>
                </View>
                <View style={styles.hang}>
                    <Image style={styles.img2} source={require('../../src/weizhi.jpg')}/>
                    <Text style={{marginLeft: 15}}>选择邮寄地址</Text>
                </View>
                 <View style={{height: 11, width: windowWidth / 1, backgroundColor: '#F0F0F0'}}>
                </View>

                <SectionList
                    renderSectionHeader={this._sectionComp}
                    renderItem={this._renderItem}
                    renderSectionFooter={this._ListFooterComponent}
                    sections={this.state.datasource}
                    extraData={this.state}
                    keyExtractor={this._keyExtractor}
                    ItemSeparatorComponent={() => <View><Text></Text></View>}
                    ListFooterComponent={() => <View style={{alignItems: 'center', height: 30}}><Text style={{fontSize: 18, color: '#d6d6d6'}}>到底啦，没有了!</Text></View>}
                />
                <View style={styles.allMoney}>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between', width: windowWidth * 4 / 5}}>
                        <View style={styles.zongji}>
                            <TouchableWithoutFeedback onPress={()=>this.chooseAll()}>
                            <Image style={styles.img111} source={this.state.AllChoose?require('../../src/t2.png'):require('../../src/t1.png')}/>
                            </TouchableWithoutFeedback>
                            <Text style={styles.ck1}>
                                全选
                            </Text>
                        </View>

                    </View>
                    <TouchableWithoutFeedback onPress={() => {
                        if (this.state.Allcheck) {
                            Alert.alert('确定清空购物车' , '', [
                                {text: '取消'},
                                {
                                    text: '确定',
                                    onPress: async () => {
                                        navigation.navigate('ShopCar')

                                    },
                                },
                            ]);
                        }
                    }}>
                   
                        <View style={[styles.jiesuan,{backgroundColor: this.state.Allcheck? '#B350FF' : '#d6d6d6'},]}>
                            <Text style={styles.ck}>
                                删除
                            </Text>
                        </View>
                    </TouchableWithoutFeedback>
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    all: {
       flex:1 ,
       backgroundColor:'white'
    },
    daohanglan:{
        height: windowWidth/6,
        width: windowWidth/1,
        backgroundColor:'#ffffff',
        alignItems:'center' ,
        justifyContent:'center'
    },
    biaoti:{
      top:windowWidth/35
    },
    bianji:{
      left:windowWidth/2.4
    },
    ziti1:{
        color:'#0A0A0A' ,
        fontSize:18 ,

    },
    ziti2:{
        color:'#B350FF' ,
        fontSize:14 ,
    },
    allMoney:{
        borderTopWidth:0.5,
        borderTopColor:'grey',
        height: windowWidth/7.5,
        width: windowWidth/1,
        backgroundColor:'white',
        flexDirection:'row' ,
        justifyContent:'space-between'
    },
    zongji:{
      flexDirection:'row' ,
      justifyContent:'space-between' ,
      alignItems:'center' ,
      marginLeft:10
    },
    jiesuan:{
         
        height:windowWidth/7.5,
        width:windowWidth*1/5 ,
        alignItems:'center' ,
        justifyContent:'center'
    },
    img111:{
      width:windowWidth/13 ,
      height:windowWidth/13
    },
    ck:{
        color:'white' ,
        fontSize:18 ,
        marginLeft:-8
    },
    ck1:{
        color:'grey' ,
        marginLeft:10
    },
    mon:{
        marginLeft:windowWidth/13,
        marginTop:windowWidth/50
    },
    t3:{
      color:'red',
      fontSize:13,
    },
    t2:{
      color:'grey',
      fontSize:13,
    },
    tx1:{
      flexDirection:'row'
    },
    tx2:{
      marginLeft:windowWidth/5.3
    },
    hang:{
      height:30 ,
      flexDirection:'row',
      alignItems:'center',
      margin:10
    },
    img2:{
      width:windowWidth/15,
      height:windowWidth/15

    },
    img3:{
        width:windowWidth/14 ,
        height:windowWidth/14,
        marginRight:10
    },
    img4:{
      width:22 ,
      height:22,
      marginLeft:windowWidth/20
    },
    img5:{
      width:windowWidth*32/375,
      height:windowWidth*32/375 ,
      justifyContent:'center',alignItems:'center',
    },
    img51:{
      width:windowWidth*46/375 ,
      height:windowWidth*32/375,
      resizeMode:'cover' ,
      justifyContent:'center',alignItems:'center',
    },
});
