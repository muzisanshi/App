/**
 * by zh at time 7.24
 */

import React,{Component} from "react";
import {Image,Text,View,StyleSheet} from 'react-native';
export default class ShopCar extends Component{
    static  navigationOptions={
        headerTitle:'购物车' ,
        headerTitleStyle:{
            color:'white' ,
            fontSize: 18 ,
        },
        headerStyle:{
            backgroundColor:'pink'

        }
    };
    render() {
        return (
                <View style={styles.all}>
                    

                    <Image style={styles.one} source={require('../../src/cart.png')}>
                    </Image>
                    <Text style={styles.two}>
                        购物车空空如也
                    </Text>
                    <Text style={styles.three}>
                          赶快抢点东西慰劳自己吧
                    </Text>
                    <View style={styles.four}> 
                        
                        <Text style={styles.new}>
                                每日上新
                        </Text>
                        < Text style={styles.new1}>
                              NEW ARRIVALS
                        </Text>
                    </View>
                    <View style={styles.five}> 
                        <Text style={styles.hot}>
                              热销商品    
                        </Text>
                        <Text style={styles.hot1}>
                            HOT GOODS
                        </Text>
                    </View>
                </View>
        );
    }
}
const styles = StyleSheet.create({
    all: {
       width:'100%' ,
       height:'100%' ,
       backgroundColor:'white'
    },
     one1: {
        borderColor:'pink',
        backgroundColor:'pink' ,
        borderWidth:2,
        width:'100%' ,
        height: 80  ,

    },
    m1: { 
        textAlign:'center',
        top:30 ,
        fontSize: 22

    },
    m2: { 
        top:25 ,
        fontSize:15 ,
        left:330,


    },
    one: {
        width:226 , 
        height:226 ,
        top: '2%',
        left:'22%'
    },
    two: {
        color:'black' , 
        fontSize:20 , 
        top:'6%' , 
        textAlign:'center'
    },
    three: {
        color:'grey' , 
        fontSize:15 , 
        top:'6.5%' , 
        textAlign:'center'
    },
    four: {    

          borderRadius: 5,
           width:150,
           height:150,
          borderWidth:1 ,
          borderColor: 'gray',
          left:20,
          top:100

    },
    five: {
          borderRadius: 5,
          width:150,
           height:150,
          borderWidth:1 ,
          borderColor: 'gray',
          left:200,
          top:-50

    },
    new:{
        fontSize:20,
        left: 35,
        top: 40,
    },
    new1:{
        fontSize:17,
        left: 20,
        top: 50,
    },
    hot:{
        fontSize:20,
        left: 35,
        top: 40,
    },
    hot1:{
        fontSize:17,
        left: 25,
        top: 50,
    },

});

