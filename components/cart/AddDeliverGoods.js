/**
 * @name AddDeliverGoods.js
 * @auhor 程浩
 * @date 2018.8.21
 * @desc 编辑发货地址
 */
import React,{Component} from 'react';
import {Text,View,StyleSheet,Image,TouchableWithoutFeedback,ScrollView,TextInput} from 'react-native';
 // 导入工具函数库
import T from "../../lib/Tools";
// 导入公共样式
import St from "../../lib/Style";
// 导入图片资源库
import Imgs from "../../lib/Img";
import BaseComponent from '../BaseComponent';

// Tools实例
let ti = T.getInstance();

let style = StyleSheet.create({
  wrapper:{
    backgroundColor:"white",
    flex:1,
    flexDirection:"column",
    alignItems:"stretch",
    justifyContent:"flex-start",
    position:"relative",
  },
  statusBar:{
    backgroundColor:"rgb(255,255,255)",
    height:ti.select({ios:16,android:22}),
    position:"relative",
    zIndex:1,
    opacity:1,
  },
  navBar:{
    backgroundColor:"rgb(255,255,255)",
    height:"7%",
    position:"relative",
    zIndex:1,
    opacity:1,
    borderBottomWidth:1,
    borderBottomColor:"rgb(241,241,241)"
  },
});

export default class AddDeliverGoods extends BaseComponent{
  constructor(props){
    super(props);
    this.state={
      id:"",//编辑发货人地址的ID
      senderName:"",//发货人姓名
      senderPhoneNo:"",//发货人手机
      senderAddress:"",//发货人地址
      repertory:null
    }
  }
 
  componentDidMount(){
    var thiz=this;
    var id=thiz.params.id;
    // console.log("--------------------id--------------------",id);
    thiz.request("senderAddress/getById",{
      method:"POST",
      data:{id:id},
      success:function(ret){
        var senderName=ret.data.senderName;
        var senderPhoneNo=ret.data.senderPhoneNo;
        var senderAddress=ret.data.senderAddress;
        thiz.setState({
          id:id,
          senderName:senderName,
          senderAddress:senderAddress,
          senderPhoneNo:senderPhoneNo,
          repertory:ret.data.repertory,
        })
      },
      error:function(err){

      }
    })
  }
  //保存点击事件
  save=()=>{
      var thiz=this;
      var id=thiz.state.id;
      var senderName=thiz.state.senderName;
      var senderPhoneNo=thiz.state.senderPhoneNo;
      var senderAddress=thiz.state.senderAddress;
      var data={};
      data.id=id;
      data.senderName=senderName;
      data.senderAddress=senderAddress;
      data.senderPhoneNo=senderPhoneNo;
      thiz.request("senderAddress/update",{
        method:"POST",
        data:data,
        success:function(ret){
          thiz.toast("修改成功");
          thiz.emit("modify_sender_info_success",data);
          thiz.goBack();
        },
        error:function(ret){
          thiz.toast("失败");
        }
      })
  }
	render(){
		return(
			<View style={style.wrapper}>
          {/*状态栏占位*/}
          <View style={{
            backgroundColor:"rgb(255,255,255)",
            height:this.isIphoneX(30,BaseComponent.SH),
            position:"relative",
            zIndex:1,
            opacity:1,
          }}></View>
          {/*顶部导航栏*/}
          <View style={style.navBar}>
          <View style={{flex:1,flexDirection:"row",justifyContent:"flex-start"}}>

          <TouchableWithoutFeedback onPress={()=>this.goBack()}>
          <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>
          <Image source={Imgs.back} style={{width:(BaseComponent.W*0.127*0.4),height:(BaseComponent.W*0.127*0.4),marginRight:BaseComponent.W*0.04}}></Image>
          </View>
          </TouchableWithoutFeedback>

          <View style={{flexGrow:1,backgroundColor:"transparent",marginTop:ti.select({ios:(BaseComponent.H*0.02)/2,android:(BaseComponent.H*0.016)/2}),
                      borderRadius:ti.select({ios:(BaseComponent.H*0.05)/2,android:(BaseComponent.H*0.054)/2}),borderColor:"#0A0A0A",
                      borderWidth:0,}}>
          <Text style={{color:"#0A0A0A",width:"100%",textAlign:"center",marginTop:ti.select({ios:"4.5%",android:"1.6%"}),fontSize:17}}>{this.params.title}</Text>
          </View>
          <TouchableWithoutFeedback onPress={()=>this.save()}>  
          <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center",marginTop:BaseComponent.W*3.5/375}}>
          <Text style={{color:'#878687',fontSize:BaseComponent.W*14/375}}>保存</Text>
          </View>
          </TouchableWithoutFeedback>

          </View>
          </View>

          {/*发货姓名*/}
          <View style={{width:BaseComponent.W*365/375,marginLeft:BaseComponent.W*10/375,height:BaseComponent.W*50/375,
                      flexDirection:'row',alignItems:'center'}}>
                <View style={{width:BaseComponent.W*88/375,height:BaseComponent.W*50/375,justifyContent:'center'}}>     
                <Text style={{fontSize:BaseComponent.W*14/375,color:'#121212'}}>发货人</Text> 
                </View>   
                <TextInput placeholder="输入姓名" style={{height:BaseComponent.W*40/375,flex:1,marginTop:BaseComponent.W*10/375,fontSize:BaseComponent.W*14/375}}
                           underlineColorAndroid='transparent' maxLength={30} defaultValue={this.state.senderName} onChangeText={(event)=>this.setState({senderName:event})}></TextInput>              
          </View>

          <View style={{width:BaseComponent.W*365/375,marginLeft:BaseComponent.W*10/375,height:0.5,backgroundColor:'#E4E4E4'}}></View>
           
          {/*手机*/}
          <View style={{width:BaseComponent.W*365/375,marginLeft:BaseComponent.W*10/375,height:BaseComponent.W*50/375,
                      flexDirection:'row',alignItems:'center'}}>
                <View style={{width:BaseComponent.W*88/375,height:BaseComponent.W*50/375,justifyContent:'center'}}>     
                <Text style={{fontSize:BaseComponent.W*14/375,color:'#121212'}}>手机</Text> 
                </View>  
                <TextInput placeholder="输入联系方式" style={{height:BaseComponent.W*40/375,flex:1,marginTop:BaseComponent.W*10/375,fontSize:BaseComponent.W*14/375}}
                           underlineColorAndroid='transparent' maxLength={11} defaultValue={this.state.senderPhoneNo} onChangeText={(event)=>this.setState({senderPhoneNo:event})}></TextInput>              
          </View>

          <View style={{width:BaseComponent.W*365/375,marginLeft:BaseComponent.W*10/375,height:0.5,backgroundColor:'#E4E4E4'}}></View>

          {/*地址*/}
          <View style={{width:BaseComponent.W*365/375,marginLeft:BaseComponent.W*10/375,height:BaseComponent.W*134/375,flexDirection:'row'}}>

                <View style={{width:BaseComponent.W*88/375,height:BaseComponent.W*134/375,marginTop:BaseComponent.W*10/375}}>     
                <Text style={{fontSize:BaseComponent.W*14/375,color:'#121212'}}>地址</Text> 
                </View>  
                <View style={{width:BaseComponent.W*260/375}}>
                <Text style={{marginTop:BaseComponent.W*10/375,fontSize:16,color:"#1a1a1a"}}>{this.state.repertory&&this.state.repertory.country?"("+this.state.repertory.country.name+")":""}</Text>
                <TextInput placeholder="输入详细地址(只支持英文)" style={{fontSize:BaseComponent.W*14/375,marginTop:BaseComponent.W*0/375}} multiline={true}
                           underlineColorAndroid='transparent' maxLength={200} defaultValue={this.state.senderAddress} onChangeText={(event)=>this.setState({senderAddress:event})}>
                </TextInput>     
                </View>         
          </View>
          <View style={{flex:1,backgroundColor:'#F0F0F0'}}></View>
      </View>
          
		)
	}
}
