/**
 * @name CartPay.js
 * @auhor 程浩
 * @date 2018.10.8
 * @desc 购物车-支付界面
 */
import React,{Component} from 'react';
import {Text,View,StyleSheet,Image,TouchableWithoutFeedback,ScrollView,
ImageBackground,TextInput,StatusBar,Modal,FlatList,Picker} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
// 导入工具函数库
import T from "../../lib/Tools";
// 导入公共样式
import St from "../../lib/Style";
// 导入图片资源库
import Imgs from "../../lib/Img";
import BaseComponent from '../BaseComponent';
import FinishPay from './FinishPay';

// Tools实例
let ti = T.getInstance();

let style = StyleSheet.create({
  wrapper:{
    backgroundColor:"rgba(14,14,14,0.3)",
    flex:1,
    flexDirection:"column",
    alignItems:"stretch",
    justifyContent:"flex-start",
    position:"relative",
  },
  statusBar:{
    backgroundColor:"rgba(14,14,14,0.1)",
    height:ti.select({ios:16,android:22}),
    position:"relative",
    zIndex:1,
    opacity:1,
  },
  navBar:{
    backgroundColor:"rgba(14,14,14,0)",
    height:"7%",
    position:"relative",
    zIndex:1,
    opacity:1,
    borderBottomWidth:1,
    borderBottomColor:"rgba(16,16,16,0.1)"
  },
});
export default class CartPay extends BaseComponent{
  constructor(props){
    super(props);
    this.state={
      WxPay:true,//true表示微信支付,false表示支付宝支付。
      isvisible:false,//true表示弹出放弃选择框.
    }
  }
  //确认支付
  ConfirmPay=()=>{
    
  }
  render(){
    return (
      <View style={style.wrapper}>   
                {/*状态栏占位*/}
                <View style={{ backgroundColor:this.state.isvisible?"rgba(14,14,14,0.5)":"rgba(14,14,14,0)",
                      height:ti.select({ios:16,android:StatusBar.currentHeight}),
                      position:"relative",
                      zIndex:1,
                      opacity:1,}}></View>
                {/*顶部导航栏*/}
              <View style={style.navBar}>
                <View style={{flex:1,flexDirection:"row",justifyContent:"flex-start"}}>
                    
                    <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>   
                    </View>
                    
                    <View style={{flexGrow:1,marginTop:ti.select({ios:(BaseComponent.H*0.02)/2,android:(BaseComponent.H*0.016)/2}),
                        borderRadius:ti.select({ios:(BaseComponent.H*0.05)/2,android:(BaseComponent.H*0.054)/2}),borderColor:"#0A0A0A",
                      borderWidth:0,}}>
                      <Text style={{color:"#0A0A0A",width:"100%",textAlign:"center",marginTop:ti.select({ios:"4%",android:"1.6%"}),fontSize:17,marginLeft:'4.3%'}}>{this.params.title}</Text>
                    </View>
                    <View style={{width:"12.7%",height:"100%",justifyContent:"center",alignItems:"center",marginRight:BaseComponent.W*5/375}}>
                        <Image source={require('../../image/cart/msg_gray.png')} style={{width:(BaseComponent.W*0.127*0.5),height:(BaseComponent.W*0.127*0.5)}}></Image>
                    </View>
                </View>
              </View>

            {/*弹出放弃支付框*/}
            <Modal visible={this.state.isvisible}
                   transparent={true}
                   onRequestClose={() => {this.setState({isvisible:false})}}>
                <View style={{flex:1,backgroundColor:'rgba(14,14,14,0.5)'}}>
                    <View style={{width:BaseComponent.W*275/375,height:BaseComponent.W*151/375,backgroundColor:'white',
                                marginTop:BaseComponent.W*80/375,marginLeft:BaseComponent.W*52/375,borderRadius:BaseComponent.W*10/375}}>
                    <View style={{width:'100%',height:BaseComponent.W*25/375,marginTop:BaseComponent.W*10/375,alignItems:'center'}}>
                        <Text style={{fontSize:BaseComponent.W*18/375,color:'#222222'}}>确认放弃支付吗？</Text>
                    </View>
                    <View style={{width:BaseComponent.W*228/375,height:BaseComponent.W*38/375,marginLeft:BaseComponent.W*24/375,marginTop:BaseComponent.W*15/375}}>
                        <Text style={{fontSize:BaseComponent.W*14/375,color:'#666666',textAlign:'center',lineHeight:BaseComponent.W*25/375}}>超过订单支付时效后,订单将被取消,请尽快完成支付。</Text>
                    </View>      
                    <View style={{width:'100%',height:0.5,backgroundColor:'#BDBDBD',marginTop:BaseComponent.W*20/375}}></View>      
                    <View style={{flex:1,flexDirection:"row"}}>
                        <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                            <Text style={{fontSize:BaseComponent.W*15/375,color:'#007AFF'}}>继续支付</Text>
                        </View>
                        <View style={{width:0.5,height:'100%',backgroundColor:'#BDBDBD'}}></View>
                        <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                            <Text style={{fontSize:BaseComponent.W*15/375,color:'#5E5E5E'}}>放弃</Text>
                        </View>
                    </View>
                    </View>

                </View>   
            </Modal>

          <View style={{width:BaseComponent.W*180/375,height:BaseComponent.W*180/375,marginLeft:BaseComponent.W*97.5/375,marginTop:BaseComponent.W*30/375}}>
              <Image source={require('../../image/cart/empty.png')} style={{width:'100%',height:'100%'}}></Image>
          </View>
        {/*选择支付方式*/}
          <View style={{flex:1,backgroundColor:'white',marginTop:BaseComponent.W*10/375}}>
              <View style={{width:'100%',height:BaseComponent.W*43/375,flexDirection:'row',alignItems:'center'}}>
                  <Text style={{fontSize:BaseComponent.W*15/375,color:'#0E0E0E',marginLeft:BaseComponent.W*139/375}}>请选择支付方式</Text>
                  <TouchableWithoutFeedback onPress={()=>this.setState({isvisible:true})}>
                  <View style={{width:BaseComponent.W*30/375,height:BaseComponent.W*43/375,marginLeft:BaseComponent.W*100/375,justifyContent:'center'}}>
                  <View style={{width:BaseComponent.W*18/375,height:BaseComponent.W*18/375,backgroundColor:'blue'}}></View>
                  </View>
                  </TouchableWithoutFeedback>
              </View>
              <View style={{width:'100%',height:0.5,backgroundColor:'#C8C7CC'}}></View>
              <View style={{width:'100%',height:BaseComponent.W*30/375,flexDirection:'row',alignItems:'flex-end'}}>
                  <Text style={{width:"100%",textAlign:"center",fontSize:BaseComponent.W*15/375,color:'#0E0E0E',marginLeft:BaseComponent.W*10/375}}>金额&nbsp;&nbsp;<Text style={{fontSize:BaseComponent.W*15/375,color:'#E31436'}}>¥279.00</Text>&nbsp;&nbsp;元</Text>
              </View>
            {/*开始选择支付方式*/}
              <View style={{width:'100%',height:BaseComponent.W*51/375,flexDirection:'row',marginTop:BaseComponent.W*10/375,alignItems:'center'}}>
                  <View style={{width:BaseComponent.W*29/375,height:BaseComponent.W*25/375,backgroundColor:'red',marginLeft:BaseComponent.W*10/375}}></View>
                  <Text style={{fontSize:BaseComponent.W*15/375,color:'#0E0E0E',marginLeft:BaseComponent.W*20/375}}>银行卡快捷支付</Text>
              </View>
              <View style={{width:'100%',height:0.5,backgroundColor:'#C8C7CC'}}></View>
            {/*支付宝支付*/}
            <TouchableWithoutFeedback onPress={()=>{this.setState({WxPay:false})}}>
            <View style={{width:'100%',height:BaseComponent.W*51/375,flexDirection:'row',alignItems:'center'}}>
                  <View style={{width:BaseComponent.W*29/375,height:BaseComponent.W*25/375,backgroundColor:'red',marginLeft:BaseComponent.W*10/375}}></View>
                  <Text style={{fontSize:BaseComponent.W*15/375,color:'#0E0E0E',marginLeft:BaseComponent.W*20/375}}>支付宝</Text>
                  <View style={{width:BaseComponent.W*21/375,height:BaseComponent.W*21/375,backgroundColor:'red',marginLeft:BaseComponent.W*237/375,display:this.state.WxPay?'none':'flex'}}></View>
            </View>
            </TouchableWithoutFeedback>
            <View style={{width:'100%',height:0.5,backgroundColor:'#C8C7CC'}}></View>
            {/*微信支付*/}
            <TouchableWithoutFeedback onPress={()=>{this.setState({WxPay:true})}}>
            <View style={{width:'100%',height:BaseComponent.W*51/375,flexDirection:'row',alignItems:'center'}}>
                  <View style={{width:BaseComponent.W*29/375,height:BaseComponent.W*25/375,backgroundColor:'red',marginLeft:BaseComponent.W*10/375}}></View>
                  <Text style={{fontSize:BaseComponent.W*15/375,color:'#0E0E0E',marginLeft:BaseComponent.W*20/375}}>微信支付</Text>
                  <View style={{width:BaseComponent.W*21/375,height:BaseComponent.W*21/375,backgroundColor:'red',marginLeft:BaseComponent.W*222/375,display:this.state.WxPay?'flex':'none'}}></View>
            </View>
            </TouchableWithoutFeedback>
            <View style={{width:'100%',height:0.5,backgroundColor:'#C8C7CC'}}></View>
            {/*确认支付*/}
            <TouchableWithoutFeedback onPress={()=>this.navigate('FinishPay',{title:"支付完成"})}>
            <View style={{width:'100%',height:BaseComponent.W*49/375,backgroundColor:'#FDD17A',position:'absolute',bottom:0,justifyContent:'center',alignItems:'center'}}>
                <Text style={{color:'#0E0E0E',fontSize:BaseComponent.W*18/375}}>确认支付</Text>
            </View>
            </TouchableWithoutFeedback>
          </View>
      </View>
    )
  }
}          