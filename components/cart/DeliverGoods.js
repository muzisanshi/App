/**
 * @name DeliverGoods.js
 * @auhor 程浩
 * @date 2018.8.21
 * @desc 发货地址
 */
import React,{Component} from 'react';
import {Text,View,StyleSheet,Image,TouchableWithoutFeedback,ScrollView} from 'react-native';
 // 导入工具函数库
import T from "../../lib/Tools";
// 导入公共样式
import St from "../../lib/Style";
// 导入图片资源库
import Imgs from "../../lib/Img";
import BaseComponent from '../BaseComponent';
import AddDeliverGoods from './AddDeliverGoods';
// let data=[{name:'[德国] 发货人：summer',address:'Starkenburgstr.11-13,60388 Frankfurt,Germany',tel:18782028081},
// 		  {name:'[美国] 发货人：Eu Express',address:'Starkenburgstr.11-13,60388 Frankfurt,Germany',tel:18782028081}];

// Tools实例
let ti = T.getInstance();

let style = StyleSheet.create({
  wrapper:{
    backgroundColor:"white",
    width:'100%',
    height:'100%',
    flexDirection:"column",
    alignItems:"stretch",
    justifyContent:"flex-start",
    position:"relative",
  },
  statusBar:{
    backgroundColor:"rgb(255,255,255)",
    height:ti.select({ios:16,android:22}),
    position:"relative",
    zIndex:1,
    opacity:1,
  },
  navBar:{
    backgroundColor:"rgb(255,255,255)",
    height:"7%",
    position:"relative",
    zIndex:1,
    opacity:1,
    borderBottomWidth:1,
    borderBottomColor:"rgb(241,241,241)"
  },
});

export default class DeliverGoods extends BaseComponent{

  constructor(props){
    super(props);
    this.state={
      senderAddress:[],//发货人地址数据
      repertoryIds:this.params.repertoryIds?this.params.repertoryIds:[],
    }
  }
  //获取发货人地址
  getData(isListen){
    let thiz=this;
    setTimeout(function(){
    var senderAddress=[];
    thiz.request("senderAddress/getPage",{
      method:"POST",
      data:{
          page:{},
          repertoryIds:thiz.state.repertoryIds,
      },
      success:function(ret){
       var senderAddressData=[];
       for(var i=0;i<ret.data.records.length;i++)
       {
        var senderData={};
        var id=ret.data.records[i].id;//编辑发货地址的ID
        var country=ret.data.records[i].repertory.country.name;//发货的国家
        var senderName=ret.data.records[i].senderName;//发货人姓名
        var senderPhoneNo=ret.data.records[i].senderPhoneNo;//发货人电话
        var senderAddress=ret.data.records[i].senderAddress;//发货人地址
        senderData.id=id;
        senderData.name="["+country+"]"+"  发货人 ："+senderName;
        senderData.address=senderAddress;
        senderData.tel=senderPhoneNo;
        senderAddressData.push(senderData);
        // console.log("-----------------senderData------------------------",senderAddressData);
       }
       thiz.setState({
        senderAddress:senderAddressData,
       })

       if(isListen){
        thiz.emit("edit_senderaddr_success",{data:ret.data.records});
       }

      },
      error:function(error){

      }
      })
    },50);
  }
  componentDidMount(){
    var thiz=this;
    thiz.getData(false);
    thiz.listen("modify_sender_info_success",function(){
      thiz.getData(true);
    })
  };
	render(){
		return(
			 <View style={style.wrapper}>
          {/*状态栏占位*/}
          <View style={{backgroundColor:"rgb(255,255,255)",
            height:this.isIphoneX(30,BaseComponent.SH),
            position:"relative",
            zIndex:1,
            opacity:1,}}></View>
          {/*顶部导航栏*/}
          <View style={style.navBar}>
          <View style={{flex:1,flexDirection:"row",justifyContent:"flex-start"}}>

          <TouchableWithoutFeedback onPress={()=>this.goBack()}>
          <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>
              <Image source={Imgs.back} style={{width:(BaseComponent.W*0.127*0.4),height:(BaseComponent.W*0.127*0.4),marginRight:BaseComponent.W*0.04}}></Image>
          </View>
          </TouchableWithoutFeedback>

          <View style={{flexGrow:1,backgroundColor:"transparent",marginTop:ti.select({ios:(BaseComponent.H*0.02)/2,android:(BaseComponent.H*0.016)/2}),
                        borderRadius:ti.select({ios:(BaseComponent.H*0.05)/2,android:(BaseComponent.H*0.054)/2}),borderColor:"#0A0A0A",
                        borderWidth:0,}}>
              <Text style={{color:"#0A0A0A",width:"100%",textAlign:"center",marginTop:ti.select({ios:"4.5%",android:"1.6%"}),fontSize:17}}>{this.params.title}</Text>
          </View>

          <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>
              <Text style={{color:'#878687'}}></Text>
          </View>

          </View>
          </View>
          <ScrollView showsVerticalScrollIndicator = {false} style={{flexGrow:0}}>  
          {/*发货人地址循环列表*/}
          {
            	this.state.senderAddress.map((item,index)=>{
                this.log("--------------------------------senderAddress-----------------------",this.state.senderAddress);
                return (
            		  <View style={{width:'100%',height:BaseComponent.W*0.395}} key={index}>

            		    <View style={{width:'100%',height:BaseComponent.W*0.027,backgroundColor:'#F0F0F0'}}></View>

            		    <View style={{flex:1,alignItems:'flex-end'}}>

        						<View style={{width:BaseComponent.W*0.97,height:BaseComponent.W*0.19,flexDirection:'column'}}>
        								<Text style={{fontSize:BaseComponent.W*0.037,color:'#121212',marginTop:BaseComponent.W*0.05}}>{item.name}</Text>
        								<Text style={{fontSize:BaseComponent.W*0.037,color:'#6B6B6B',marginTop:BaseComponent.W*0.032}}>{item.address}</Text>
        								<Text style={{position:'absolute',fontSize:BaseComponent.W*0.037,color:'#121212',
        								right:0,marginTop:BaseComponent.W*0.05,marginRight:BaseComponent.W*0.024}}>{item.tel}</Text>
        						</View>

                    <View style={{width:BaseComponent.W*0.97,height:0.5,backgroundColor:'#E4E4E4',marginLeft:BaseComponent.W*0.03,marginTop:BaseComponent.W*12/375}}></View>

        						<View style={{width:'100%',height:BaseComponent.W*0.144,flexDirection:'row',
        									justifyContent:'flex-end'}}>
      							<View style={{width:BaseComponent.W*0.23,height:BaseComponent.W*0.144,display:'none',
      								flexDirection:'row',justifyContent:'space-between',alignItems:'center',marginLeft:BaseComponent.W*0.035}}>
      								<Image style={{width:BaseComponent.W*0.053,height:BaseComponent.W*0.053}} source={item.default?require('../../image/home/xuanzhong.png'):require('../../image/home/weixuanzhong.png')}/>
      								<Text style={{fontSize:BaseComponent.W*0.037,color:'#121212'}}>默认地址</Text>
      							</View>
      							<View style={{width:BaseComponent.W*0.39,height:BaseComponent.W*0.144,flexDirection:'row',
      										alignItems:'center',justifyContent:'flex-end',marginRight:BaseComponent.W*0.024}}>
                    <TouchableWithoutFeedback onPress={()=>{
                      var id=this.state.senderAddress[index].id;
                      this.navigate('AddDeliverGoods',{title:'编辑发货地址',id:id});

                    }}>        
    								<View style={{width:BaseComponent.W*0.17,height:BaseComponent.W*0.08,borderRadius:2,
    											borderWidth:0.5,borderColor:'#7F7F7F',justifyContent:'center',alignItems:'center'}}>
    									<Text style={{fontSize:BaseComponent.W*0.037,color:'#121212'}}>编辑</Text>
    								</View>
                    </TouchableWithoutFeedback>
    								<View style={{width:BaseComponent.W*0.17,height:BaseComponent.W*0.08,display:'none',
    											borderWidth:0.5,borderColor:'#7F7F7F',justifyContent:'center',alignItems:'center'}}>
									  <Text style={{fontSize:BaseComponent.W*0.037,color:'#121212'}}>删除</Text>
								    </View>

							      </View>
						        </View>

            			  </View>
            		 </View>
            		)
            	})
          }
        </ScrollView>  
        <View style={{flexGrow:1,backgroundColor:'#F0F0F0'}}></View>  
    </View>
		)
	}
}
