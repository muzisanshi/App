/**
 * @name FinishPay.js
 * @auhor 程浩
 * @date 2018.10.8
 * @desc 完成支付
 */
import React,{Component} from 'react';
import {Text,View,StyleSheet,Image,TouchableWithoutFeedback,ScrollView,
ImageBackground,TextInput,StatusBar,Modal,FlatList,Picker} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
// 导入工具函数库
import T from "../../lib/Tools";
// 导入公共样式
import St from "../../lib/Style";
// 导入图片资源库
import Imgs from "../../lib/Img";
import BaseComponent from '../BaseComponent';


// Tools实例
let ti = T.getInstance();

let style = StyleSheet.create({
  wrapper:{
    backgroundColor:"white",
    flex:1,
    flexDirection:"column",
    alignItems:"stretch",
    justifyContent:"flex-start",
    position:"relative",
  },
  statusBar:{
    backgroundColor:"rgb(255,255,255)",
    height:ti.select({ios:16,android:22}),
    position:"relative",
    zIndex:1,
    opacity:1,
  },
  navBar:{
    backgroundColor:"rgb(255,255,255)",
    height:"7%",
    position:"relative",
    zIndex:1,
    opacity:1,
    borderBottomWidth:1,
    borderBottomColor:"rgb(241,241,241)"
  },
});
export default class FinishPay extends BaseComponent{

  render(){
    let thiz = this;
    return (
      <View style={style.wrapper}>   
                {/*状态栏占位*/}
                <View style={{ backgroundColor:"rgb(255,255,255)",
                      height:thiz.isIphoneX(30,BaseComponent.SH),
                      position:"relative",
                      zIndex:1,
                      opacity:1,}}></View>
                {/*顶部导航栏*/}
              <View style={style.navBar}>
                <View style={{flex:1,flexDirection:"row",justifyContent:"flex-start"}}>

                    <TouchableWithoutFeedback onPress={()=>{
                      if(thiz.params.from&&thiz.params.from=="Cart"){
                        thiz.navigate("Cart");
                        return;
                      }
                      if(thiz.params.from&&thiz.params.from=="GoodsXiangQing"){
                        thiz.navigate("GoodsXiangQing");
                        return;
                      }
                      if(thiz.params.from&&thiz.params.from=="DingdanXiangqingDaizhifu"){
                        thiz.navigate('Wodedingdan',{title:"我的订单",page:2});
                        return;
                      }
                    }}>
                    <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>
                      <Image source={Imgs.back} style={{width:(BaseComponent.W*0.127*0.4),height:(BaseComponent.W*0.127*0.4),marginRight:BaseComponent.W*0.04}}></Image>
                    </View>
                    </TouchableWithoutFeedback>

                    <View style={{flexGrow:1,backgroundColor:"transparent",
                      marginTop:ti.select({ios:(BaseComponent.H*0.02)/2,android:(BaseComponent.H*0.016)/2}),borderRadius:ti.select({ios:(BaseComponent.H*0.05)/2,android:(BaseComponent.H*0.054)/2}),borderColor:"#0A0A0A",
                      borderWidth:0,}}>
                      <Text style={{color:"#0A0A0A",width:"100%",textAlign:"center",marginTop:ti.select({ios:"4.5%",android:"1.6%"}),fontSize:17,marginLeft:'4.3%'}}>{this.params.title}</Text>
                    </View>
                    <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center",marginRight:BaseComponent.W*5/375}}>
                    </View>
                </View>
              </View>

        <View style={{width:'100%',height:BaseComponent.W*200/375,justifyContent:'center',alignItems:'center'}}>
            <View style={{width:BaseComponent.W*215/375,height:BaseComponent.W*150/375}}>
                <View style={{width:'100%',height:BaseComponent.W*50/375,flexDirection:"row"}}>

                    <View style={{width:BaseComponent.W*28/375,height:BaseComponent.W*28/375,marginLeft:BaseComponent.W*25/375,marginTop:ti.select({ios:0,android:BaseComponent.W*10/375})}}>
                        <Image style={{width:BaseComponent.W*28/375,height:BaseComponent.W*28/375}} source={require('../../image/home/paysuccess.png')}></Image>
                    </View>

                    <Text style={{fontSize:BaseComponent.W*30/375,color:'#333333',marginLeft:BaseComponent.W*10/375}}>支付成功</Text>
                </View>
                <Text style={{fontSize:BaseComponent.W*17/375,color:'#666666',marginLeft:BaseComponent.W*60/375,marginTop:BaseComponent.W*10/375}}>实付¥{thiz.params.totalMoney}</Text>
                <View style={{width:'100%',height:BaseComponent.W*40/375,position:'absolute',bottom:0,flexDirection:'row',justifyContent:'space-between'}}>
                    <TouchableWithoutFeedback onPress={()=>{
                      let orderNo = thiz.params.orderNo;
                      thiz.log("--------FinishPay_orderNo--------",orderNo);
                      thiz.navigate("DingdanXiangqingDaifahuo",{title:"订单详情",businessOrderNo:orderNo,from:thiz.params.from});
                    }}>
                    <View style={{width:BaseComponent.W*95/375,height:BaseComponent.W*40/375,borderWidth:1,borderRadius:BaseComponent.W*20/375,borderColor:'#D7D7D7',
                                justifyContent:'center',alignItems:'center'}}>
                          <Text style={{color:'#666666',fontSize:BaseComponent.W*15/375}}>查看订单</Text>        
                    </View>
                    </TouchableWithoutFeedback>
                    <TouchableWithoutFeedback onPress={()=>{
                      thiz.navigate("Home");
                    }}>
                    <View style={{width:BaseComponent.W*95/375,height:BaseComponent.W*40/375,borderWidth:1,borderRadius:BaseComponent.W*20/375,borderColor:'#D7D7D7',
                                justifyContent:'center',alignItems:'center'}}>
                          <Text style={{color:'#666666',fontSize:BaseComponent.W*15/375}}>返回首页</Text>        
                    </View>
                    </TouchableWithoutFeedback>
                </View>
            </View>
        </View>
        
        {/*安全提醒*/}
        <View style={{width:BaseComponent.W*355/375,height:BaseComponent.W*120/375,marginLeft:BaseComponent.W*10/375,backgroundColor:'#F2F2F2'}}>
            <View style={{width:"100%",height:BaseComponent.W*46/375,flexDirection:'row',alignItems:'center'}}>

                <Image style={{width:BaseComponent.W*18/375,height:BaseComponent.W*18/375,marginLeft:BaseComponent.W*10/375}} source={require('../../image/home/remind.png')}></Image>

                <Text style={{color:'#333333',fontSize:BaseComponent.W*15/375,marginLeft:BaseComponent.W*10/375}}>安全提醒</Text>
            </View>
            <View style={{width:BaseComponent.W*345/375,height:BaseComponent.W*40/375,marginLeft:BaseComponent.W*10/375}}>
                <Text style={{fontSize:BaseComponent.W*15/375,color:'#999999'}}>小洋匠不会通过任何非官方电话、QQ、微信与您联系，也不会要求您点击链接进行退款或取消订单！</Text>
            </View>
        </View>
      </View>
    )
  }
}      