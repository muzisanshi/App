/**
 * @name Payfailure.js
 * @auhor 程浩
 * @date 2018.10.8
 * @desc 支付失败
 */
import React,{Component} from 'react';
import {Text,View,StyleSheet,Image,TouchableWithoutFeedback,ScrollView,
ImageBackground,TextInput,StatusBar,Modal,FlatList,Picker} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
// 导入工具函数库
import T from "../../lib/Tools";
// 导入公共样式
import St from "../../lib/Style";
// 导入图片资源库
import Imgs from "../../lib/Img";
import BaseComponent from '../BaseComponent';


// Tools实例
let ti = T.getInstance();

let style = StyleSheet.create({
  wrapper:{
    backgroundColor:"white",
    flex:1,
    flexDirection:"column",
    alignItems:"stretch",
    justifyContent:"flex-start",
    position:"relative",
  },
  statusBar:{
    backgroundColor:"rgb(255,255,255)",
    height:ti.select({ios:16,android:22}),
    position:"relative",
    zIndex:1,
    opacity:1,
  },
  navBar:{
    backgroundColor:"rgb(255,255,255)",
    height:"7%",
    position:"relative",
    zIndex:1,
    opacity:1,
    borderBottomWidth:1,
    borderBottomColor:"rgb(241,241,241)"
  },
});
export default class Payfailure extends BaseComponent{
  constructor(props){
    super(props);
  }

  componentDidMount(){
    let thiz = this;
    let businessOrderNo = thiz.params.businessOrderNo;
    console.log("------------------------businessOrderNo--------------------------------",businessOrderNo);
  }

  render(){
    let thiz = this;
    return (
      <View style={style.wrapper}>   
              {/*状态栏占位*/}
              <View style={{ backgroundColor:"rgb(255,255,255)",
                    height:thiz.isIphoneX(30,BaseComponent.SH),
                    position:"relative",
                    zIndex:1,
                    opacity:1,}}></View>
              {/*顶部导航栏*/}
              <View style={style.navBar}>
                <View style={{flex:1,flexDirection:"row",justifyContent:"flex-start"}}>

                    <TouchableWithoutFeedback onPress={()=>{
                      thiz.goBack();
                    }}>
                    <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>
                      <Image source={Imgs.back} style={{width:(BaseComponent.W*0.127*0.4),height:(BaseComponent.W*0.127*0.4),marginRight:BaseComponent.W*0.04}}></Image>
                    </View>
                    </TouchableWithoutFeedback>

                    <View style={{flexGrow:1,backgroundColor:"transparent",
                      marginTop:ti.select({ios:(BaseComponent.H*0.02)/2,android:(BaseComponent.H*0.016)/2}),borderRadius:ti.select({ios:(BaseComponent.H*0.05)/2,android:(BaseComponent.H*0.054)/2}),borderColor:"#0A0A0A",
                      borderWidth:0,}}>
                      <Text style={{color:"#0A0A0A",width:"100%",textAlign:"center",marginTop:ti.select({ios:"4.5%",android:"1.6%"}),fontSize:17,marginLeft:'4.3%'}}>{this.params.title}</Text>
                    </View>
                    <View style={{width:"15%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center",marginRight:BaseComponent.W*5/375}}>
                    </View>
                </View>
              </View>

              {/*支付失败*/}
              <Text style={{fontSize:BaseComponent.W*30/375,color:'#B4282D',textAlign:'center',marginTop:BaseComponent.W*22/375}}>支付失败</Text>
              <Text style={{fontSize:BaseComponent.W*15/375,color:'#848484',textAlign:'center',marginTop:BaseComponent.W*14/375}}>{thiz.params.limitTime}完成付款</Text>
              <Text style={{fontSize:BaseComponent.W*15/375,color:'#848484',textAlign:'center',marginTop:BaseComponent.W*5/375}}>否则订单会被系统取消</Text>    
              <View style={{width:'100%',height:BaseComponent.W*40/375,flexDirection:'row',justifyContent:'center',marginTop:BaseComponent.W*17/375}}>
                    <TouchableWithoutFeedback onPress={()=>{
                        thiz.navigate('DingdanXiangqingDaizhifu',{title:'订单详情',businessOrderNo:thiz.params.businessOrderNo});
                    }}>
                    <View style={{width:BaseComponent.W*95/375,height:BaseComponent.W*40/375,borderWidth:1,borderRadius:BaseComponent.W*20/375,borderColor:'#D7D7D7',
                                justifyContent:'center',alignItems:'center'}}>
                          <Text style={{color:'#666666',fontSize:BaseComponent.W*15/375}}>查看订单</Text>        
                    </View>
                    </TouchableWithoutFeedback>

                    <TouchableWithoutFeedback onPress={()=>{
                        // if(thiz.params.from&&thiz.params.from=="Querendingdan"){
                        //   thiz.navigate('Querendingdan',{title:"确认订单",data:thiz.params.data});
                        //   return ;
                        // }
                        // if(thiz.params.from&&thiz.params.from=="DingdanXiangqingDaizhifu"){
                        //   thiz.navigate('DingdanXiangqingDaizhifu',{title:'订单详情',businessOrderNo:thiz.params.businessOrderNo});
                        //   return ;
                        // }
                        thiz.goBack();
                    }}>
                    <View style={{width:BaseComponent.W*95/375,height:BaseComponent.W*40/375,borderWidth:1,borderRadius:BaseComponent.W*20/375,borderColor:'#D7D7D7',marginLeft:BaseComponent.W*29/375,
                                justifyContent:'center',alignItems:'center'}}>
                          <Text style={{color:'#666666',fontSize:BaseComponent.W*15/375}}>重新付款</Text>        
                    </View>
                    </TouchableWithoutFeedback>
              </View>

              {/*小洋匠提醒*/}
              <View style={{width:BaseComponent.W*355/375,justifyContent:'center',alignItems:'center',marginLeft:BaseComponent.W*10/375,backgroundColor:'#F9F9F9',marginTop:BaseComponent.W*55/375,paddingTop:BaseComponent.W*17/375,paddingBottom:BaseComponent.W*20/375}}>
                  <Text style={{fontSize:BaseComponent.W*15/375,color:'#848484'}}>如已扣款成功，显示失败，请到订单详情页面刷新，</Text>
                  <Text style={{fontSize:BaseComponent.W*15/375,color:'#848484',marginTop:5}}>待订单状态更新；如有其他问题，可联系在线客服。</Text>
              </View>
      </View>
    )
  }
}      