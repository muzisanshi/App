/**
 * @name Querendingdan.js
 * @auhor 程浩
 * @date 2018.8.27
 * @desc 提交订单
 */
import React,{Component} from 'react';
import {Text,View,StyleSheet,Image,TouchableWithoutFeedback,ScrollView,Modal,TextInput,StatusBar,BackHandler,ActivityIndicator} from 'react-native';
 // 导入工具函数库
import T from "../../lib/Tools";
// 导入公共样式
import St from "../../lib/Style";
// 导入图片资源库
import Imgs from "../../lib/Img";
import BaseComponent from '../BaseComponent';
import CartPay from './CartPay';

import DeliverGoods from './DeliverGoods';
import shouhuoAddress from '../mine/shouhuoAddress';

// 支付
import Alipay from 'react-native-payment-alipay';
// import * as Wxpay from 'react-native-wechat';
import Wxpay from '@yyyyu/react-native-wechat';

// 转菊花
import Spinner from 'react-native-loading-spinner-overlay';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

// Tools实例
let ti = T.getInstance();

let style = StyleSheet.create({
  wrapper:{
    backgroundColor:"rgba(255,255,255,1)",
    flex:1,
    flexDirection:"column",
    alignItems:"stretch",
    justifyContent:"flex-start",
    position:"relative",
  },
  statusBar:{
  	backgroundColor:"rgb(255,255,255)",
    height:ti.select({ios:16,android:22}),
    position:"relative",
    zIndex:1,
    opacity:1,
  },
  navBar:{
    backgroundColor:"rgb(255,255,255)",
    height:"7%",
    position:"relative",
    zIndex:1,
    opacity:1,
    borderBottomWidth:1,
    borderBottomColor:"rgb(241,241,241)"
  },
});

export default class Querendingdan extends BaseComponent{
	constructor(props){
		super(props);
		this.state={
			number:4,
			paytype:"wxPay",//默认是ali支付
			isvisible:false,//离开对话框
			payvisible:false,//选择支付方式对话框	
			data:this.params.data,

			// 全数据
			allData:this.params.allData,

			senderAddresses:this.params.data.senderAddresses,
			
			receivAddress:this.params.data.defaultAddress,

			visible:false,

			// 订单是否下单成功
			isOrdered:false,

			// 支付限时
			limitTime:"请在0小时0分内完成支付",

			// 备注
			remark:"",

			addressHint:false,

			payMode:null,

			userInfo:null,
		}

		// 支付返回数据备份
		this.payData = undefined;

		// 解决快速点击确认支付的问题
		this.canEnsure = true;

		this.log("--------this.state.data--------",this.state.data);
	};
	
	//监听物理返回键
	cancel=()=>{
		this.setState({isvisible:true});
	};

	componentDidMount(){
		let thiz = this;
     	// BackHandler.addEventListener('hardwareBackPress', thiz.handleBackPress);
     	// thiz.listen("edit_senderaddr_success",function(ret){
     	// 	thiz.setState({
     	// 		senderAddresses:ret.data,
     	// 	})
     	// });

     	// 注册微信
     	// Wxpay.registerApp('wx928c9e134253166e');
     	// 判断微信是否已安装

     	// thiz.getAccount(function(ret){
     	// 	thiz.log("--------account--------",ret);

     	// 	if(ret&&ret.userInfo){
     	// 		thiz.setState({
     	// 			userInfo:ret.userInfo,
     	// 		});
     	// 	}
     	// });

     	// 从服务器加载用户信息
     	thiz.request("user/getInfo",{
          method:"POST",
          success:function(ret){
            thiz.setState({visible:false});
            thiz.log("--------querendingdan_user/getInfo-------",ret);
            if(ret&&ret.respCode==="00"){
                thiz.setState({
                    userInfo:ret.data,
                });
            }
          },
          error:function(err){
            thiz.toast(err.msg);
          }
        });

     	thiz.listen("set_default_address",function(ret){
     		thiz.log("------------------------------address-------------------",ret.data);
     		var addressData={};
     		if(!addressData.receiverName)
     		{
     			addressData.receiverName="";
     		}
     		if(!addressData.fullAddress)
     		{
     			addressData.fullAddress="";
     		}
     		if(!addressData.receiverPhoneNo)
     		{
     			addressData.receiverPhoneNo="";
     		}
     		if(!addressData.id)
     		{
     			addressData.id="";
     		}
     		addressData.id=ret.data.id;
     		addressData.receiverName=ret.data.name;
     		addressData.fullAddress=ret.data.address;
     		addressData.receiverPhoneNo=ret.data.tel;
     		addressData.defaultAddress = ret.data.defaultAddress;
     		thiz.setState({
     			receivAddress:addressData,
     		});
     	});

     	// 监听保存收货地址
     	thiz.listen("save_receiveraddr_success",function(ret){
     		thiz.log("--------Querendingdan_save_receiveraddr_success--------",ret);
     		if(ret&&ret.address){
     			thiz.setState({
	     			receivAddress:ret.address,
	     		});
     		}
     	});

     	// 监听修改了发货地址
     	thiz.listen("modify_sender_info_success",function(ret){
     		thiz.log("--------modify_sender_info_success--------",ret);
     		// 重新修改和遍历数据
     		if(thiz.state.allData&&ret){
     			let allDataStr = JSON.stringify(thiz.state.allData);
     			let allDataNew = JSON.parse(allDataStr);
     			for(let i=0;i<allDataNew.length;i++){
     				if(allDataNew[i].repertoryV2&&allDataNew[i].repertoryV2.senderAddress.id==ret.id){
     					allDataNew[i].repertoryV2.senderAddress.senderName = ret.senderName;
     					allDataNew[i].repertoryV2.senderAddress.senderAddress = ret.senderAddress;
     					allDataNew[i].repertoryV2.senderAddress.senderPhoneNo = ret.senderPhoneNo;
     				}
     			}
     			thiz.setState({
     				allData:allDataNew,
     			});
     		}
	    });



	}
 	handleBackPress = () => {
   	 	this.cancel();
    	return true;
  	}

	componentWillUnmount(){
  		// BackHandler.removeEventListener('hardwareBackPress',this.handleBackPress);
  	}

  	// 微信支付接口
  	wxpay(info){
  		let thiz = this;

  		thiz.setState({
    		payvisible:false,
    	});

  		thiz.log("--------wxpay_info--------",info);
  		let data = info;

  		// 判断微信是否安装
  		let callback = async function(isInstalled){
  			if(!isInstalled){
	   			thiz.toast("请先安装微信");
	   			return;
	   		}
	   		let payP = {

	   			appId:"wx928c9e134253166e",
	   			partnerId:data.clientInvokePayData.partnerid,
	   			prepayId:data.clientInvokePayData.prepayid,
	   			nonceStr:data.clientInvokePayData.noncestr,
	   			timestamp:data.clientInvokePayData.timestamp,
	   			packageSign:data.clientInvokePayData.package,
	   			sign:data.clientInvokePayData.sign

	   		};
	   		thiz.log("--------payP--------",payP);
	   		Wxpay.pay(payP).then(function(ret){
	   			thiz.log("--------wxpay_ret--------",ret);
	   			
			    if(ret&&ret.errCode==0){
			    	// 通知后端支付成功
			        thiz.setState({
				        visible:true
				    });
			    	thiz.request("order/paySuccessNotify",{
			          method:"POST",
				          data:{
				          	channelOrderNo:info.channelOrderNo
				          },
				          success:function(ret){
				          		thiz.setState({
				                      visible:false,
				                });
				                if(ret.respCode=="00"){
				                	thiz.log("--------wxpay_channelOrderNo--------",info.channelOrderNo);
				                	thiz.navigate('FinishPay',{
				                		title:'支付完成',
				                		totalMoney:thiz.state.data.feeComputeResult.payTotalAmount,
				                		orderNo:thiz.retCopy.data.orderInfo.businessOrderNo,
				                		from:thiz.params.from,
				                	});
				                	thiz.setState({
				                		payvisible:false,
				                	});

				                }
				          },
				          error:function(err){
				                thiz.toast(err.msg);
				                thiz.setState({
				                      visible:false,
				                });
				          }
				    });
			    }else{
			    	
			    	thiz.toast("微信支付失败");
			    	
			    	thiz.setState({
				        visible:true
				    });
	       			// 避免模块的异常和ios上从左上角点击返回导致的app无法判断是否支付成功
	       			thiz.request("order/queryPayResult",{
			          method:"POST",
				          data:{
				          	channelOrderNo:info.channelOrderNo
				          },
				          success:function(ret){
				          		thiz.setState({
				                      visible:false,
				                });
				          		thiz.log("--------order/queryPayResult--------",ret);
				                if(ret.respCode=="00"&&ret.data.channelOrderStatus=="SUCCESS"){

				                	// 通知后端支付完成
				                	thiz.setState({
				                		payvisible:false,
				                	});

				                	setTimeout(function(){
				                		thiz.navigate('FinishPay',{
					                		title:'支付完成',
					                		totalMoney:thiz.state.data.feeComputeResult.totalAmount,
					                		orderNo:thiz.retCopy.data.orderInfo.businessOrderNo,
					                		from:thiz.params.from,
					                	});
				                	},200);
				                	
								    return;
				                }
				                
				        //         if(ret.respCode=="00"&&ret.data.channelOrderStatus=="PROCESSING"){
				        //         	// 支付失败
				        //         	thiz.toast("支付处理中");
							    	// thiz.setState({
					       //                visible:false,
					       //          });
					       //          return;
				        //         }
				        //         if(ret.respCode=="00"&&ret.data.channelOrderStatus=="CLOSE"){
				        //         	// 支付失败
				        //         	thiz.toast("支付关闭");
							    	// thiz.setState({
					       //                visible:false,
					       //          });
					       //          return;
				        //         }
				        //         if(ret.respCode=="00"&&ret.data.channelOrderStatus=="FAIL"){
				        //         	// 支付失败
				        //         	thiz.toast("支付失败");
							    	// thiz.setState({
					       //                visible:false,
					       //          });
					       //          return;
				        //         }

				                // 其他情况
				                // thiz.toast("支付失败");
						    	// thiz.setState({
				       //                visible:false,
				       //          });

				                setTimeout(function(){
			                		thiz.navigate('Payfailure',{title:'支付结果',from:'Querendingdan',businessOrderNo:thiz.retCopy.data.orderInfo.businessOrderNo,data:thiz.params.data,limitTime:thiz.state.limitTime});
			                	},200);

				                
				          },
				          error:function(err){
				                // thiz.toast("支付失败");
				               
						    	thiz.setState({
				                      visible:false,
				                });

				                setTimeout(function(){
			                		thiz.navigate('Payfailure',{title:'支付结果',from:'Querendingdan',businessOrderNo:thiz.retCopy.data.orderInfo.businessOrderNo,data:thiz.params.data,limitTime:thiz.state.limitTime});
			                	},200);
				          }
				    });
				    
			    }

	   		}).catch(function(err){
	   			thiz.log("--------wxpay_err--------",err);

	   			if(err&&err.errCode == -2){
		            thiz.log("--------wxpay_err--------","微信支付取消");
		        }
	   			// thiz.toast("支付失败");
	   			thiz.setState({
                      visible:false,
                });
                
                setTimeout(function(){
            		thiz.navigate('Payfailure',{title:'支付结果',from:'Querendingdan',businessOrderNo:thiz.retCopy.data.orderInfo.businessOrderNo,data:thiz.params.data,limitTime:thiz.state.limitTime});
            	},200);
	   		});
	   		
  		};
  		Wxpay.isWXAppInstalled().then(callback);
  	}

  	// 测试支付接口
	alipay(info){
	    let thiz = this;
	    thiz.setState({
			payvisible:false,
		});
	    // 调支付接口
	    thiz.log("--------alipay_info--------",info);
	    Alipay.pay(info.clientInvokePayData).then(function(data){
	        thiz.log("--------pay_success--------",data);
	        
		    // 获取状态
		    let status = 0;

		    if(BaseComponent.OS=="android"){
		    	if(data && typeof(data) == "string"){
			    	let result = data.split(";")[2].split("result=")[1];
			    	// 去除前后的花括号（二逼的支付宝）
			    	result = result.substring(1,result.length-1);
			    	result = JSON.parse(result);
			    	status = result.alipay_trade_app_pay_response.code;
			    }
		    }else{
		    	if(data&&data.length>0){
		    		let result = data[0].result;
		    		result = JSON.parse(result);
		    		status = result.alipay_trade_app_pay_response.code;
		    	}
		    }
		    
		    thiz.log("--------status--------",status);
	        // 判断支付状态
	        if(status>=9000){
	        	// 通知后端支付成功
		        thiz.setState({
			        visible:true
			    });
			    thiz.request("order/paySuccessNotify",{
			          method:"POST",
			          data:{
			          	channelOrderNo:info.channelOrderNo
			          },
			          success:function(ret){
			          		thiz.setState({
			                      visible:false,
			                });
			                if(ret.respCode=="00"){
			                	thiz.navigate('FinishPay',{
			                		title:'支付完成',
			                		totalMoney:thiz.state.data.feeComputeResult.payTotalAmount,
			                		orderNo:thiz.retCopy.data.orderInfo.businessOrderNo,
			                		from:thiz.params.from,
			                	});
			                	thiz.setState({
			                		payvisible:false,
			                	});

			                	// 定时关闭自己
			                	// setTimeout(function(){
			                	// 	thiz.goBack();
			                	// },500);

			                }
			          },
			          error:function(err){
			                thiz.toast(err.msg);
			                thiz.setState({
			                      visible:false,
			                });
			          }
			    });
	        }else{
	        	// thiz.toast("支付失败");
	        	thiz.setState({
                      visible:false,
                });
                
                setTimeout(function(){
	        		thiz.navigate('Payfailure',{title:'支付结果',from:'Querendingdan',businessOrderNo:thiz.retCopy.data.orderInfo.businessOrderNo,data:thiz.params.data,limitTime:thiz.state.limitTime});
	        	},200);
	        }

	    }).catch(function (err) {
	        thiz.log("--------pay_err--------",err);
	        if(err){
	        	// thiz.toast("支付失败");

	        	thiz.setState({
                      visible:false,
                });

                
                setTimeout(function(){
            		thiz.navigate('Payfailure',{title:'支付结果',from:'Querendingdan',businessOrderNo:thiz.retCopy.data.orderInfo.businessOrderNo,data:thiz.params.data,limitTime:thiz.state.limitTime});
            	},200);
	        }
	    });
	}

  	// 提交订单
  	commit(){
  		let thiz = this;
  		if(!thiz.state.isOrdered){// 已经掉过下单接口
  			thiz.setState({
				visible:true,
			});
	  		thiz.request("order/submit",{
			          method:"POST",
			          data:{
			          	receiverAddressId:thiz.state.receivAddress.id,
			          	remark:thiz.state.remark,
			          },
			          success:function(ret){
			                if(ret.respCode=="00"){
			                	// 提交返回数据
			                	thiz.retCopy = ret;

			                	// 设置提交订单成功
			                	thiz.setState({
			                		isOrdered:true,
			                		retCopy:ret,
			                		limitTime:"请在"+ret.data.orderInfo.surplusPayHours+"小时"+ret.data.orderInfo.surplusPayMinutes+"分内完成支付"
			                	});

			                	// 发送下单成功消息，刷新购物车界面
				  				thiz.emit("submit_order_success");
				  				
			                	// 获取支付模式
			                	thiz.request("payChannel/getPayMode",{
							          method:"POST",
							          data:{},
							          success:function(ret){
							                if(ret.respCode=="00"){
							                	thiz.setState({
								                      visible:false,
								                });
							                	// 备份支付模式
							                	thiz.payMode = ret.data;

							                	// 处理只有一种支付方式的情况，强制设置默认支付方式为这个唯一的支付方式
							                	if(ret.data.length==1){
							                		thiz.setState({
							                			paytype:ret.data[0].payChannel.type,
							                		});
							                	}

							                	// 显示支付方式选择
										  		// 必须有收货地址才能取付款
										    	if(thiz.state.data.defaultAddress){
										  			setTimeout(function(){
										  				thiz.setState({
													        payvisible:true,
													        payMode:ret.data,
													    });
										  			},500);
										  		}

							                }
							          },
							          error:function(err){
							                thiz.toast(err.msg);
							                thiz.setState({
							                      visible:false,
							                });
							          }
							    });

			                }
			          },
			          error:function(err){
			                thiz.toast(err.msg);
			                thiz.setState({
			                      visible:false,
			                });

			                // 判断是否需要上传身份证
			                if(err.errCode=="RECEIVER_ADDRESS_NEED_UPLOAD_ID_CARD_IMAGE"){
			                	// 跳转到地址编辑页面
			                	thiz.navigate('EditAddress',{title:"编辑地址",editDataID:thiz.state.receivAddress.id,from:"Querendingdan"});
			                }
			          }
			});
  		}else{// 未掉过下单接口
  			setTimeout(function(){
  				thiz.setState({
			        payvisible:true,
			    });
  			},500);
  		}		
  	}

  	//确认支付
  	ensurepay=()=>{
  		var thiz=this;
  		
  		if(thiz.canEnsure){
  			
  			thiz.canEnsure = false;
  			
  			// 定时关闭支付弹窗，并允许点击
  			setTimeout(function(){
  				thiz.canEnsure = true;
  				thiz.setState({
  					payvisible:false,
  				});
  			},200);
  			
  			// 接着调支付接口（仅仅测试用）
	    	let payChannelType=thiz.state.paytype;
	    	let payModeType = thiz.state.payMode&&thiz.state.payMode.length>0?thiz.state.payMode[0].type:"";
	    	
	    	if(!payChannelType){
	    		return;
	    	}

	    	// if(thiz.state.paytype=="wxPay"){
	    	// 	payChannelType = thiz.payMode[0].payChannel.type;
	    	// 	payModeType = thiz.payMode[0].type;	
	    	// }
	    	// if(thiz.state.paytype=="aliPay"){
	    	// 	payChannelType = thiz.payMode[1].payChannel.type;
	    	//  	payModeType = thiz.payMode[1].type;
	    	// }
	    	// if(thiz.state.paytype=="bank"){
	    	// 	return;
	    	// }

	  		// 接着调支付接口
	    	thiz.request("order/pay",{
		          method:"POST",
		          data:{
		          	businessOrderNo:thiz.retCopy.data.orderInfo.businessOrderNo,
		          	payParams:{
		          		payChannelType:payChannelType,
		          		payModeType:payModeType
		          	}
		          },
		          success:function(ret){
		          		thiz.setState({
		                      visible:false,
		                });
		                if(ret.respCode=="00"){
		                	
		                	// 备份数据
		                	// thiz.payData = ret.data;

					  		// 发送下单成功消息，刷新购物车界面
					  		thiz.emit("submit_order_success");

					  		if(ret.data){

					  			// 隐藏支付方式
					  			thiz.setState({
							        payvisible:false,
							    });

					  			// 接着调支付接口（仅仅测试用）
					         	if(thiz.state.paytype=="wxPay"){
									thiz.wxpay(ret.data);
								}
					        	if(thiz.state.paytype=="aliPay"){
					        		thiz.alipay(ret.data);
					        	}
					  		}


		                }
		          },
		          error:function(err){
		                thiz.toast(err.msg);
		                thiz.setState({
		                      visible:false,
		                });
		          }
		    });
  		}

  	}

  	cancelPay(){
  		let thiz = this;
  		thiz.emit("cancel_pay");
  		setTimeout(function(){
  			thiz.goBack();
  		},100);
  	}

	render(){
		let thiz = this;
		return (
			<View style={style.wrapper}>
                {/*状态栏占位*/}
                <View style={{backgroundColor:this.state.isvisible||this.state.visible||this.state.payvisible?'rgba(14,14,14,0.5)':"rgb(255,255,255)",
   								height:thiz.isIphoneX(30,BaseComponent.SH),
   								position:"relative",
    							zIndex:1,
    							opacity:1}}></View>
    			{/*转菊花*/}
		          <Modal
		            visible={this.state.visible}
		            onRequestClose={()=>{this.setState({visible:false})}}
		            transparent={true}
		            animationType={"fade"}>
		            <View style={{backgroundColor:"rgba(0,0,0,0.4)",width:"100%",width:"100%",flex:1,flexDirection:"column",alignItems:"center",justifyContent:"center",position:"relative"}}>
		              <View>
		                    <ActivityIndicator size="large" color='white'/>
		              </View>
		            </View>
		          </Modal>

                {/*顶部导航栏*/}
                <View style={style.navBar}>
                  <View style={{flex:1,flexDirection:"row",justifyContent:"flex-start"}}>

                    <TouchableWithoutFeedback onPress={()=>this.cancel()}>
                    <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>
                      <Image source={Imgs.back} style={{width:(BaseComponent.W*0.127*0.4),height:(BaseComponent.W*0.127*0.4),marginRight:BaseComponent.W*0.04}}></Image>
                    </View>
                    </TouchableWithoutFeedback>

                    <View style={{flexGrow:1,backgroundColor:"transparent",
                      marginTop:ti.select({ios:(BaseComponent.H*0.02)/2,android:(BaseComponent.H*0.016)/2}),borderRadius:ti.select({ios:(BaseComponent.H*0.05)/2,android:(BaseComponent.H*0.054)/2}),borderColor:"#0A0A0A",
                      borderWidth:0,}}>
                      <Text style={{color:"#0A0A0A",width:"100%",textAlign:"center",marginTop:ti.select({ios:"4.5%",android:"1.6%"}),fontSize:17}}>{this.params.title}</Text>
                    </View>
                    <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>
                    </View>
                </View>
            	</View>

            	{/*确认收货地址提示*/}
		        <Modal visible={this.state.addressHint}
		             transparent={true}
		                 onRequestClose={() => {this.setState({addressHint:false})}}>
		             <View style={styles.Modal}>
		              <View style={{width:BaseComponent.W*275/375,backgroundColor:'rgba(255, 255, 255,1)',borderRadius:BaseComponent.W*20/375}}>
		              <View style={{width:'100%',padding:15,}}>
		                <Text style={{fontSize:BaseComponent.W*18/375,color:'rgba(34, 34, 34, 1)',textAlign:"center"}}>请确认收件人信息</Text>
		              </View>
		              
		              {/*信息*/}
		              <View style={{width:"100%",paddingLeft:10,paddingRight:10,paddingBottom:15,backgroundColor:"white",}}>
		                <View style={{flexDirection:"row",width:"100%",marginBottom:ti.select({android:5,ios:8}),}}>
		                	<Text style={{width:"35%"}}>收件人:</Text>
		                	<Text style={{width:"65%"}}>{thiz.state.receivAddress?thiz.state.receivAddress.receiverName:""}</Text>
		                </View>
		                <View style={{flexDirection:"row",width:"100%",marginBottom:ti.select({android:5,ios:10}),}}>
		                	<Text style={{width:"35%",backgroundColor:"white"}}>电话号码:</Text>
		                	<Text style={{width:"65%"}}>{thiz.state.receivAddress?thiz.state.receivAddress.receiverPhoneNo:""}</Text>
		                </View>
		                <View style={{flexDirection:"row",width:"100%",marginBottom:ti.select({android:5,ios:10}),}}>
		                	<Text style={{width:"35%"}}>收货地址:</Text>
		                	<Text style={{width:"65%"}}>{thiz.state.receivAddress?thiz.state.receivAddress.fullAddress:""}</Text>
		                </View>
		              </View>
		              
		              <View style={{width:'100%',height:0.5,backgroundColor:'rgba(189, 189, 189, 1)'}}></View>

		              <View style={{width:'100%',flexDirection:'row',}}>

			              <TouchableWithoutFeedback onPress={()=>{
			              	thiz.setState({
			              		addressHint:false,
			              	});
			              }}> 
			                <View style={{width:'50%',justifyContent:'center',alignItems:'center',padding:15,}}>
			                  <Text style={{fontSize:BaseComponent.W*15/375,color:'rgba(94, 94, 94, 1)'}}>取消</Text>
			                </View>
			              </TouchableWithoutFeedback> 

			                <View style={{width:0.5,height:'100%',backgroundColor:'rgba(189, 189, 189, 1)'}}></View>

			                <TouchableWithoutFeedback onPress={()=>{
			                	thiz.setState({
				              		addressHint:false,
				              	});
				              	// 提交订单
				              	thiz.commit();
			                }}> 

			                <View style={{width:'50%',justifyContent:'center',alignItems:'center',padding:15,}}>
			                  <Text style={{fontSize:BaseComponent.W*15/375,color:'rgba(16, 98, 255, 1)'}}>确认</Text>
			                </View>
			                </TouchableWithoutFeedback>

			              </View>

		              </View>
		             </View>
		        </Modal>

            	{/*弹出对话框确认离开*/}
            	<Modal visible={this.state.isvisible}
            	 	   transparent={true}
                       onRequestClose={() => {
                       	thiz.setState({isvisible:false});
                       }}>
                   <View style={styles.Modal}>
                    <View style={{width:BaseComponent.W*275/375,height:BaseComponent.W*105/375,backgroundColor:'rgba(255, 255, 255,1)',borderRadius:BaseComponent.W*20/375}}>
						<View style={{width:'100%',height:BaseComponent.W*56/375,justifyContent:'center',alignItems:'center'}}>
							<Text style={{fontSize:BaseComponent.W*15/375,color:'rgba(34, 34, 34, 1)'}}>优惠不等人,再考虑考虑吧</Text>
						</View>
						<View style={{width:'100%',height:0.5,backgroundColor:'rgba(189, 189, 189, 1)'}}></View>
						<View style={{width:'100%',height:BaseComponent.W*49/375,flexDirection:'row'}}>
						<TouchableWithoutFeedback onPress={()=>this.cancelPay()}>	
							<View style={{width:'50%',height:BaseComponent.W*49/375,justifyContent:'center',alignItems:'center'}}>
								<Text style={{fontSize:BaseComponent.W*15/375,color:'#FED584'}}>暂时离开</Text>
							</View>
						</TouchableWithoutFeedback>	
							<View style={{width:0.5,height:'100%',backgroundColor:'rgba(189, 189, 189, 1)'}}></View>
							<TouchableWithoutFeedback onPress={()=>this.setState({isvisible:false})}>	
							<View style={{width:'50%',height:BaseComponent.W*49/375,justifyContent:'center',alignItems:'center'}}>
								<Text style={{fontSize:BaseComponent.W*15/375,color:'rgba(94, 94, 94, 1)'}}>我再想想</Text>
							</View>
							</TouchableWithoutFeedback>
						</View>
                    </View>
                   </View>
            	</Modal>
            	
				{/*弹出支付框*/}
            	<Modal visible={this.state.payvisible}
            	 	   transparent={true}
                       onRequestClose={() => {this.setState({payvisible:false})}}>
		        {/*选择支付方式*/}
		        <TouchableWithoutFeedback onPress={()=>this.setState({payvisible:false})}>	
		          <View style={{flex:1,backgroundColor:'rgba(14,14,14,0.5)'}}>
						<View style={{width:'100%',height:BaseComponent.W*300/375,backgroundColor:'#fff',position:'absolute',bottom:0}}>
		              <View style={{width:'100%',height:BaseComponent.W*43/375,flexDirection:'row',alignItems:'center'}}>
		                  <Text style={{fontSize:BaseComponent.W*15/375,color:'#0E0E0E',marginLeft:BaseComponent.W*139/375}}>请选择支付方式</Text>
		                  <TouchableWithoutFeedback onPress={()=>{
		                  	// 先隐藏支付弹窗
							thiz.setState({
								payvisible:false
							});
							setTimeout(function(){
								thiz.setState({isvisible:true});
							},200);
		                  }}>
		                  <View style={{width:BaseComponent.W*30/375,height:BaseComponent.W*43/375,marginLeft:BaseComponent.W*100/375,justifyContent:'center'}}>
		                  <Image style={{width:BaseComponent.W*18/375,height:BaseComponent.W*18/375}} source={require('../../image/home/close.png')}></Image>
		                  </View>
		                  </TouchableWithoutFeedback>
		              </View>
		              <View style={{width:'100%',height:0.5,backgroundColor:'#C8C7CC'}}></View>
		              <View style={{width:'100%',height:BaseComponent.W*60/375,flexDirection:'row',alignItems:'center'}}>

		                  <Text style={{textAlign:"center",width:'100%',textAlign:'center',fontSize:BaseComponent.W*15/375,color:'#0E0E0E',marginLeft:BaseComponent.W*10/375}}>{thiz.state.limitTime}&nbsp;&nbsp;金额&nbsp;&nbsp;<Text style={{fontSize:BaseComponent.W*15/375,color:'#E31436'}}>¥{this.state.data.feeComputeResult?this.state.data.feeComputeResult.payTotalAmount.toFixed(2):"0.00"}</Text>&nbsp;&nbsp;元</Text>

		              </View>

		            {/*银行卡快捷支付*/}
		              <View style={{width:'100%',height:BaseComponent.W*51/375,flexDirection:'row',marginTop:BaseComponent.W*10/375,alignItems:'center',display:'none'}}>
		                  <View style={{width:BaseComponent.W*29/375,height:BaseComponent.W*25/375,backgroundColor:'red',marginLeft:BaseComponent.W*10/375,display:'none'}}></View>
		                  <Text style={{fontSize:BaseComponent.W*15/375,color:'#0E0E0E',marginLeft:BaseComponent.W*20/375,display:'none'}}>银行卡快捷支付</Text>
		              </View>
		              <View style={{width:'100%',height:0.5,backgroundColor:'#C8C7CC'}}></View>

		            {
		            	thiz.state.payMode&&thiz.state.payMode.length>0?thiz.state.payMode.map(function(pv,pi){
		            		return (
		            			<View>
			            			<TouchableWithoutFeedback onPress={()=>{thiz.setState({paytype:pv.payChannel.type})}}>
						            <View style={{width:'100%',height:BaseComponent.W*51/375,flexDirection:'row',alignItems:'center'}}>
						                  <Image style={{width:BaseComponent.W*29/375,height:BaseComponent.W*25/375,marginLeft:BaseComponent.W*10/375}} source={(function(){
						                  	if(pv.payChannel.type=="aliPay"){
						                  		return require('../../image/home/alipay.png');
						                  	}
						                  	if(pv.payChannel.type=="wxPay"){
						                  		return require('../../image/home/wxpay.png');
						                  	}
						                  })()}></Image>
						                  <Text style={{fontSize:BaseComponent.W*15/375,color:'#0E0E0E',marginLeft:BaseComponent.W*20/375,width:BaseComponent.W*265/375}}>
						                  	{
						                  		(function(){
						                  			if(pv.payChannel.type=="wxPay"){
								                  		return "微信支付"
								                  	}
								                  	if(pv.payChannel.type=="aliPay"){
								                  		return "支付宝"
								                  	}
						                  		})()
						                  	}
						                  </Text>
						                  <Image style={{width:BaseComponent.W*21/375,height:BaseComponent.W*21/375,marginLeft:(function(){

						                  	let marginLeft = 0;
						                  	return marginLeft;

						                  })(),display:(function(){

						                  	let display = 'none';
						                  	if(pv.payChannel.type==thiz.state.paytype){
						                  		display = "flex";
						                  	}
						                  	return display;

						                  })()}} source={require('../../image/home/xuanzhong.png')}></Image>
						            </View>
						            </TouchableWithoutFeedback>
						            <View style={{width:'100%',height:0.5,backgroundColor:'#C8C7CC'}}></View>
					            </View>
		            		)
		            	}):null
		            }

		            {/*支付宝支付*/}
		            {/*<TouchableWithoutFeedback onPress={()=>{this.setState({paytype:"aliPay"})}}>
		            <View style={{width:'100%',height:BaseComponent.W*51/375,flexDirection:'row',alignItems:'center'}}>
		                  <Image style={{width:BaseComponent.W*29/375,height:BaseComponent.W*25/375,marginLeft:BaseComponent.W*10/375}} source={require('../../image/home/alipay.png')}></Image>
		                  <Text style={{fontSize:BaseComponent.W*15/375,color:'#0E0E0E',marginLeft:BaseComponent.W*20/375}}>支付宝</Text>
		                  <Image style={{width:BaseComponent.W*21/375,height:BaseComponent.W*21/375,marginLeft:BaseComponent.W*237/375,display:this.state.paytype=="aliPay"?'flex':'none'}} source={require('../../image/home/xuanzhong.png')}></Image>
		            </View>
		            </TouchableWithoutFeedback>
		            <View style={{width:'100%',height:0.5,backgroundColor:'#C8C7CC'}}></View>*/}

		            {/*微信支付*/}
		            {/*<TouchableWithoutFeedback onPress={()=>{this.setState({paytype:"wxPay"})}}>
		            <View style={{width:'100%',height:BaseComponent.W*51/375,flexDirection:'row',alignItems:'center'}}>
		                  <Image style={{width:BaseComponent.W*29/375,height:BaseComponent.W*25/375,marginLeft:BaseComponent.W*10/375}} source={require('../../image/home/wxpay.png')}></Image>
		                  <Text style={{fontSize:BaseComponent.W*15/375,color:'#0E0E0E',marginLeft:BaseComponent.W*20/375}}>微信支付</Text>
		                  <Image style={{width:BaseComponent.W*21/375,height:BaseComponent.W*21/375,marginLeft:BaseComponent.W*222/375,display:this.state.paytype=="wxPay"?'flex':'none'}} source={require('../../image/home/xuanzhong.png')}></Image>
		            </View>
		            </TouchableWithoutFeedback>
		            <View style={{width:'100%',height:0.5,backgroundColor:'#C8C7CC'}}></View>*/}

		            {/*确认支付*/}
		            <TouchableWithoutFeedback onPress={()=>this.ensurepay()}>
		            <View style={{width:'100%',height:BaseComponent.W*49/375,backgroundColor:'#FDD17A',position:'absolute',bottom:0,justifyContent:'center',alignItems:'center'}}>
		                <Text style={{color:'#0E0E0E',fontSize:BaseComponent.W*18/375}}>确认支付</Text>
		            </View>
		            </TouchableWithoutFeedback>

		            </View>
		          </View>
		          </TouchableWithoutFeedback>
          		</Modal>
				

				<ScrollView style={{flex:1,backgroundColor:'rgba(241,241,241,1)'}} showsVerticalScrollIndicator={false}>
				<KeyboardAwareScrollView>
				{/*收货人地址*/}
				<TouchableWithoutFeedback onPress={()=>{
					this.log("--------------------------thiz.state.receivAddress------------------------",thiz.state.receivAddress);
					if(!thiz.state.isOrdered){
						this.goPage("shouhuoAddress",{title:"收货地址",isBack:true,from:"Querendingdan",id:thiz.state.receivAddress.id})
					}
				}}>
				<View style={{backgroundColor:'white',flexDirection:"row"}}>
				<View style={{padding:10,alignItems:"center",justifyContent:"flex-start",backgroundColor:"white"}}>
					<Image style={{width:BaseComponent.W*20/375,height:BaseComponent.W*20/375,}} source={require('../../image/home/mine_dizhiguanli.png')}></Image>
            	</View>
            	<View style={[styles.shouhuoren,{flexGrow:1,paddingTop:10,paddingBottom:10,}]}>
	            	<View style={{width:BaseComponent.W*335/375-30,}}>
	            			<View style={{flexDirection:'space-between',flexDirection:'row'}}>
	            				<Text style={{fontSize:BaseComponent.W*14/375,color:'rgba(18, 18, 18, 1)'}}>{this.state.receivAddress?this.state.receivAddress.receiverName:""}&nbsp;&nbsp;&nbsp;&nbsp;<Text>{this.state.receivAddress?this.state.receivAddress.receiverPhoneNo:""}</Text></Text>
	            				<View style={{display:thiz.state.receivAddress&&thiz.state.receivAddress.defaultAddress?"flex":"none",justifyContent:'center',alignItems:'center',borderWidth:0.5,borderColor:'rgba(74, 74, 74, 1)',marginLeft:BaseComponent.W*16/375}}>
	            						<Text style={{fontSize:BaseComponent.W*11/375,color:'rgba(74, 74, 74, 1)',paddingLeft:5,paddingRight:5,}}>默认</Text>
	            				</View>
	            			</View>
	            			<Text style={{width:BaseComponent.W*335/375-40,fontSize:BaseComponent.W*14/375,color:'rgba(107, 107, 107, 1)',marginTop:BaseComponent.W*11/375}}>{this.state.receivAddress?this.state.receivAddress.fullAddress:""}</Text>
	            	</View>
	            	<View style={{width:BaseComponent.W*20/375,alignItems:"center",justifyContent:"center"}}>
            			<Image style={{width:BaseComponent.W*20/375,height:BaseComponent.W*30/375,}} source={require('../../image/home/youjiantou.png')}></Image>
            		</View>
            	</View>
            	</View>
            	</TouchableWithoutFeedback>

            	<View>
            		<Image resizeMode="contain" style={{width:"100%"}} source={require("../../image/home/querendingdan.png")}/>
            	</View>
				
				<View style={{width:'100%',height:BaseComponent.W*10/375,backgroundColor:'rgba(240, 240, 240, 1)'}}></View>
				
				{/*发货人地址*/}
				{/*<TouchableWithoutFeedback onPress={()=>{
					if(!thiz.state.isOrdered){
						this.goPage("DeliverGoods",{title:"发货地址"});
					}
				}}>
				<View style={{backgroundColor:"white",width:'100%',display:this.state.data.senderAddresses&&this.state.data.senderAddresses.length>0?"flex":"none",}}>

					<View style={{width:'100%',paddingTop:10,paddingBottom:10,justifyContent:'center',
						borderBottomWidth:0.5,borderBottomColor:"#ECECEC"}}>
						<Text style={{fontSize:BaseComponent.W*14/375,color:'#121212',marginLeft:BaseComponent.W*10/375}}>发货地址</Text>
					</View>

					<View style={{width:"100%",}}>
						{
							thiz.state.data.senderAddresses&&this.state.data.senderAddresses.length>0?thiz.state.data.senderAddresses.map(function(v,i){
								return (
									<View style={{width:BaseComponent.W*330/375,borderBottomWidth:i<thiz.state.data.senderAddresses.length-1?0.5:0,borderBottomColor:"#ECECEC",backgroundColor:"white",paddingTop:10,paddingBottom:10,}}>
										<View style={{flex:1,}}>
											<View style={{width:BaseComponent.W*330/375,height:'100%',marginLeft:BaseComponent.W*10/375}}>
												<Text style={{fontSizeL:BaseComponent.W*14/375,color:'#6B6B6B',}}>{(thiz.state.senderAddresses&&thiz.state.senderAddresses.length>0)?("["+thiz.state.senderAddresses[i].repertory.country.name+"]"):""}&nbsp;&nbsp;{thiz.state.senderAddresses&&thiz.state.senderAddresses.length>0?thiz.state.senderAddresses[i].senderName:""}</Text>
												<Text style={{fontSizeL:BaseComponent.W*14/375,color:'#6B6B6B',marginTop:BaseComponent.W*5/375}} numberOfLines={1}>{(thiz.state.senderAddresses&&thiz.state.senderAddresses.length>0)?thiz.state.senderAddresses[i].senderAddress:""}</Text>
											</View>
										</View>
									</View>
								);
							}):null
						}
						<View style={{backgroundColor:"white",height:"100%",position:"absolute",right:0,marginRight:BaseComponent.W*11/375,alignItems:"center",justifyContent:"center"}}>
							<Image style={{display:"flex",width:BaseComponent.W*20/375,height:BaseComponent.W*30/375,}} source={require('../../image/home/youjiantou.png')}></Image>
						</View>
					</View>

				</View>
				</TouchableWithoutFeedback>*/}

				{/*<View style={{width:'100%',height:BaseComponent.W*10/375,backgroundColor:'rgba(240, 240, 240, 1)'}}></View>*/}

				{/*商品数量*/}
            	{/*<View style={styles.goodsNumber}>
					<View style={{width:BaseComponent.W*280/375,height:'100%',flexDirection:'row',
									marginLeft:BaseComponent.W*10/375,alignItems:'center'}}>
									{
										
										this.state.data.imgs?this.state.data.imgs.map((value,index)=>{
											if(index<=3){
												return (
													<Image style={{width:BaseComponent.W*70/375,height:BaseComponent.W*70/375}} source={{uri:value}}></Image>
												)
											}
										}):null
									}
					</View>
					<Text style={{fontSize:BaseComponent.W*14/375,color:'rgba(107, 107, 107, 1)',marginLeft:BaseComponent.W*8/375}}>共{this.state.data.goodsNum?this.state.data.goodsNum:0}件</Text>
					<Image style={{width:BaseComponent.W*20/375,height:BaseComponent.W*30/375,position:'absolute',right:0,marginRight:BaseComponent.W*12/375}} source={require('../../image/home/youjiantou.png')}></Image>
            	</View>*/}

            	{/*新的按发货仓的布局*/}
            	<View style={{width:"100%",flexDirection:"column"}}>

            		{/*从allData中遍历每一项*/}
            		{
            			thiz.state.allData&&thiz.state.allData.length>0?thiz.state.allData.map(function(item,index){

            				return (
            					<View style={{width:"100%",flexDirection:"column"}}>


            						{/*每一个仓库发货人，是VIP就显示*/}
            						{
            							item.repertoryV2&&thiz.state.userInfo&&thiz.state.userInfo.userType=="AGENT"?(

            								<TouchableWithoutFeedback onPress={()=>{
												if(!thiz.state.isOrdered){

													// 构建仓库id
													let rids = [];
													if(item.repertoryV2&&item.repertoryV2.repertory){
														rids.push(item.repertoryV2&&item.repertoryV2.repertory.id);
													}
													thiz.goPage("DeliverGoods",{title:"发货地址",repertoryIds:rids});

												}
											}}>
											<View style={{backgroundColor:"white",width:'100%',}}>

												<View style={{width:'100%',paddingTop:10,paddingBottom:10,
													borderBottomWidth:0.5,borderBottomColor:"#ECECEC",flexDirection:"row"}}>
													<Image source={require("../../image/cart/shop.png")} style={{width:20,height:20,marginLeft:8,marginTop:ti.select({ios:-2,android:2}),}}/>
													<Text style={{fontSize:BaseComponent.W*14/375,color:'#121212',marginLeft:BaseComponent.W*10/375}}>{item.repertoryV2&&item.repertoryV2.repertory?item.repertoryV2.repertory.name:""}</Text>
												</View>

												<View style={{width:'100%',paddingTop:10,paddingBottom:10,justifyContent:'center',
													borderBottomWidth:0,borderBottomColor:"#ECECEC"}}>
													<Text style={{fontSize:BaseComponent.W*14/375,color:'#121212',marginLeft:BaseComponent.W*10/375}}>发货人：</Text>
												</View>

												<View style={{width:"100%",}}>

													<View style={{width:BaseComponent.W*330/375,borderBottomWidth:0.5,borderBottomColor:"#ECECEC",backgroundColor:"white",paddingTop:10,paddingBottom:10,}}>
														<View style={{flex:1,}}>
															<View style={{width:BaseComponent.W*330/375,height:'100%',marginLeft:BaseComponent.W*10/375}}>
																<View style={{flexDirection:"row"}}>
																	<Text style={{fontSizeL:BaseComponent.W*14/375,color:'#6B6B6B',}}>{(item.repertoryV2&&item.repertoryV2.senderAddress)?("["+item.repertoryV2.senderAddress.repertory.country.name+"]"):""}&nbsp;&nbsp;{item.repertoryV2&&item.repertoryV2.senderAddress?item.repertoryV2.senderAddress.senderName:""}</Text>
																	<Text style={{marginLeft:30,fontSizeL:BaseComponent.W*14/375,color:'#6B6B6B',}}>{item.repertoryV2&&item.repertoryV2.senderAddress?item.repertoryV2.senderAddress.senderPhoneNo:""}</Text>
																</View>
																<Text style={{fontSizeL:BaseComponent.W*14/375,color:'#6B6B6B',marginTop:BaseComponent.W*5/375}} numberOfLines={1}>{(item.repertoryV2&&item.repertoryV2.senderAddress)?item.repertoryV2.senderAddress.senderAddress:""}</Text>
															</View>
														</View>
													</View>

													<View style={{backgroundColor:"white",height:"100%",position:"absolute",right:0,marginRight:BaseComponent.W*11/375,alignItems:"center",justifyContent:"center"}}>
														<Image style={{display:"flex",width:BaseComponent.W*20/375,height:BaseComponent.W*30/375,}} source={require('../../image/home/youjiantou.png')}></Image>
													</View>

												</View>



											</View>
											</TouchableWithoutFeedback>

            							):null
            						}
            						

									{/*每一个仓库的商品*/}
									<View style={styles.goodsNumber}>
										<View style={{width:BaseComponent.W*280/375,height:'100%',flexDirection:'row',
														marginLeft:BaseComponent.W*10/375,alignItems:'center'}}>
														{
															
															item.shoppingCarItems?item.shoppingCarItems.map((v,i)=>{
																if(i<=2){
																	return (
																		<Image style={{width:BaseComponent.W*70/375,height:BaseComponent.W*70/375,marginRight:10,}} source={{uri:v.sku.imageAttachment.resourceFullAddress}}></Image>
																	)
																}
															}):null
														}
										</View>
										<Text style={{fontSize:BaseComponent.W*14/375,color:'rgba(107, 107, 107, 1)',marginLeft:BaseComponent.W*8/375}}>共{
											(function(){
												// 计算件数
												let num = 0;
												for(let i=0;i<item.shoppingCarItems.length;i++){
													num+=item.shoppingCarItems[i].num;
												}
												return num;
											})()
										}件</Text>

										{/*<Image style={{width:BaseComponent.W*20/375,height:BaseComponent.W*30/375,position:'absolute',right:0,marginRight:BaseComponent.W*12/375}} source={require('../../image/home/youjiantou.png')}></Image>*/}
					            		
					            	</View>

					            	{
					            		index<thiz.state.allData.length-1?(
					            			<View style={{width:'100%',height:BaseComponent.W*10/375,backgroundColor:'rgba(240, 240, 240, 1)'}}></View>
					            		):null
					            	}

            					</View>
            				)

            			}):null
            		}

            	</View>

            	<View style={{width:'100%',height:BaseComponent.W*10/375,backgroundColor:'rgba(240, 240, 240, 1)'}}></View>
				
        	
				{/*商品金额*/}
				<View style={{backgroundColor:'white',width:'100%',height:BaseComponent.W*30/375,flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
					<Text style={{color:'rgba(18, 18, 18, 1)',fontSize:BaseComponent.W*14/375,marginLeft:BaseComponent.W*11/375}}>商品金额</Text>
					<Text style={{color:'rgba(255, 64, 126, 1)',fontSize:BaseComponent.W*15/375,marginRight:BaseComponent.W*12/375}}>¥{this.state.data.feeComputeResult?this.state.data.feeComputeResult.goodsTotalAmount.toFixed(2):"0.00"}</Text>
				</View>
				{/*优惠*/}
				<View style={{backgroundColor:'white',width:'100%',height:BaseComponent.W*30/375,flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
					<Text style={{color:'rgba(18, 18, 18, 1)',fontSize:BaseComponent.W*14/375,marginLeft:BaseComponent.W*11/375}}>优惠</Text>
					<Text style={{color:'rgba(58, 58, 58, 1)',fontSize:BaseComponent.W*15/375,marginRight:BaseComponent.W*12/375}}>-¥{this.state.data.feeComputeResult?this.state.data.feeComputeResult.reduceTotalAmount.toFixed(2):"0.00"}</Text>
				</View>
				{/*税费*/}
				<View style={{backgroundColor:'white',width:'100%',height:BaseComponent.W*30/375,flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
					<Text style={{color:'rgba(18, 18, 18, 1)',fontSize:BaseComponent.W*14/375,marginLeft:BaseComponent.W*11/375}}>税费</Text>
					<Text style={{color:'rgba(58, 58, 58, 1)',fontSize:BaseComponent.W*15/375,marginRight:BaseComponent.W*12/375}}>¥{this.state.data.feeComputeResult?this.state.data.feeComputeResult.taxTotalAmount.toFixed(2):"0.00"}</Text>
				</View>

				{/*税费*/}
				<View style={{backgroundColor:'white',width:'100%',height:BaseComponent.W*30/375,flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
					<Text style={{color:'rgba(18, 18, 18, 1)',fontSize:BaseComponent.W*14/375,marginLeft:BaseComponent.W*11/375}}>服务</Text>
					<Text style={{color:'rgba(58, 58, 58, 1)',fontSize:BaseComponent.W*15/375,marginRight:BaseComponent.W*12/375}}>¥{this.state.data.feeComputeResult?this.state.data.feeComputeResult.specialServiceTotalAmount.toFixed(2):"0.00"}</Text>
				</View>

				{/*运费和购买协议*/}
				<View style={{width:'100%',backgroundColor:'white',marginBottom:10,}}>
					<View style={{width:'100%',height:BaseComponent.W*35/375,flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
						<Text style={{fontSize:BaseComponent.W*14/375,color:'rgba(18, 18, 18, 1)',marginLeft:BaseComponent.W*11/375}}>运费</Text>
						<Text style={{fontSize:BaseComponent.W*15/375,color:'rgba(58, 58, 58, 1)',marginRight:BaseComponent.W*12/375}}>¥{this.state.data.feeComputeResult?this.state.data.feeComputeResult.logisticTotalAmount.toFixed(2):"0.00"}</Text>
					</View>

					<View style={{width:'100%',height:0.5,backgroundColor:'#ececec',marginLeft:BaseComponent.W*12/375}}></View>

					<View style={{flex:1,justifyContent:'center',display:'none'}}>
						<Text style={{fontSize:BaseComponent.W*12/375,color:'rgba(153, 153, 153, 1)',marginLeft:BaseComponent.W*11/375}}>提交订单则表示您同意<Text style={{color:'rgba(58, 58, 58, 1)'}}>《用户购买协议》</Text></Text>
					</View>

				</View>

				{/*备注*/}
				<View style={{width:"100%",flexDirection:"column",backgroundColor:"white",paddingBottom:10,paddingTop:10,marginBottom:10,}}>
					<Text style={{fontSize:BaseComponent.W*14/375,color:'rgba(18, 18, 18, 1)',marginLeft:BaseComponent.W*11/375}}>备注：</Text>
					<TextInput style={{height:120,flexGrow:1,borderColor: 'transparent',borderWidth:0.5,borderRadius:5,marginLeft:BaseComponent.W*11/375,
						marginRight:BaseComponent.W*11/375,color:"#232326",marginTop:ti.select({ios:6,android:0}),paddingLeft:5,fontSize:BaseComponent.W*15/375,textAlignVertical:'top'}}
                       	underlineColorAndroid='transparent' maxLength={100} placeholder={""}
                        onChangeText={(event)=>{
                        	thiz.setState({
                        		remark:event,
                        	})
                        }} multiline={true}></TextInput>
				</View>
				</KeyboardAwareScrollView>
				</ScrollView>

				{/*提交订单*/}
				<View style={{width:'100%',height:BaseComponent.W*50/375,flexDirection:'row',justifyContent:'flex-end',alignItems:'center',backgroundColor:'white'}}>
					<Text style={{fontSize:BaseComponent.W*14/375,color:'rgba(58, 58, 58, 1)',marginRight:BaseComponent.W*10/375}}>实付款:</Text>
					<Text style={{fontSize:BaseComponent.W*17/375,color:'rgba(255, 64, 126, 1)',marginRight:BaseComponent.W*13/375}}>¥{this.state.data.feeComputeResult?this.state.data.feeComputeResult.payTotalAmount.toFixed(2):"0.00"}</Text>
					{/*onPress={()=>this.navigate('CartPay',{title:"购物车"})}*/}
					<TouchableWithoutFeedback onPress={()=>{
						if(!thiz.state.isOrdered){
							thiz.setState({
								addressHint:true,
							});
							return;
						}
						
						thiz.commit();
					}}>
					<View style={{width:BaseComponent.W*117/375,height:BaseComponent.W*50/375,backgroundColor:'#FED584',
								justifyContent:'center',alignItems:'center'}}>
						<Text style={{fontSize:BaseComponent.W*17/375,color:"#3A3A3A"}}>{thiz.state.isOrdered?"立即付款":"提交订单"}</Text>
					</View>
					</TouchableWithoutFeedback>
				</View>
				
        	</View>
		)
	}
};

const styles=StyleSheet.create({
		shouhuoren:{
			flexDirection:'row',
		},
		goodsNumber:{
			width:'100%',
			height:BaseComponent.W*100/375,
			flexDirection:'row',
			alignItems:'center',
			backgroundColor:'rgb(249,249,249)',
		},
		Modal:{
			flex:1,
			backgroundColor:'rgba(14,14,14,0.5)',
			justifyContent:'center',alignItems:'center',
			position:"relative",
			zIndex:10000,
		}
});