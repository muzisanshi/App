/**
 * @name AllBrand.js
 * @auhor 程浩
 * @date 2018.8.24
 * @desc 全部品牌界面
 */
import React,{Component} from 'react';
import {Text,View,StyleSheet,Image,TouchableWithoutFeedback,ScrollView,StatusBar,TextInput,FlatList,Modal,SectionList,ActivityIndicator} from 'react-native';
 // 导入工具函数库
import T from "../../lib/Tools";
// 导入公共样式
import St from "../../lib/Style";
// 导入图片资源库
import Imgs from "../../lib/Img";
import GoodsXiangQing from "./GoodsXiangQing";
import BaseComponent from '../BaseComponent';



// Tools实例
let ti = T.getInstance();

let style = StyleSheet.create({
  wrapper:{
    backgroundColor:"white",
    flex:1,
    flexDirection:"column",
    alignItems:"stretch",
    justifyContent:"flex-start",
    position:"relative",
  },
  statusBar:{
    backgroundColor:"rgb(255,255,255)",
    height:ti.select({ios:16,android:22}),
    position:"relative",
    zIndex:1,
    opacity:1,
  },
  navBar:{
    backgroundColor:"rgb(255,255,255)",
    height:"7%",
    position:"relative",
    zIndex:1,
    opacity:1,
    borderBottomWidth:1,
    borderBottomColor:"rgb(241,241,241)"
  },
});
// let QueryData=["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"];
// let data=["全部","美容彩妆","奶粉纸尿裤","母音专区","营养保健","11111","2222222","33333"];
// let SectionData=[{title:'A',data:[{img:require('../../image/home/good.png'),name:"a2",brand:'德国品牌'},{img:require('../../image/home/good.png'),name:"a2",brand:'德国品牌'}]},
// {title:'B',data:[{img:require('../../image/home/good.png'),name:"a2",brand:'德国品牌'},{img:require('../../image/home/good.png'),name:"a2",brand:'德国品牌'}]},
// {title:'C',data:[{img:require('../../image/home/good.png'),name:"a2",brand:'德国品牌'},{img:require('../../image/home/good.png'),name:"a2",brand:'德国品牌'}]},
// {title:'D',data:[{img:require('../../image/home/good.png'),name:"a2",brand:'德国品牌'},{img:require('../../image/home/good.png'),name:"a2",brand:'德国品牌'}]},
// {title:'E',data:[{img:require('../../image/home/good.png'),name:"a2",brand:'德国品牌'},{img:require('../../image/home/good.png'),name:"a2",brand:'德国品牌'}]},
// {title:'F',data:[{img:require('../../image/home/good.png'),name:"a2",brand:'德国品牌'},{img:require('../../image/home/good.png'),name:"a2",brand:'德国品牌'}]},
// {title:'G',data:[{img:require('../../image/home/good.png'),name:"a2",brand:'德国品牌'},{img:require('../../image/home/good.png'),name:"a2",brand:'德国品牌'}]},
// {title:'H',data:[{img:require('../../image/home/good.png'),name:"a2",brand:'德国品牌'},{img:require('../../image/home/good.png'),name:"a2",brand:'德国品牌'}]},
// {title:'I',data:[{img:require('../../image/home/good.png'),name:"a2",brand:'德国品牌'},{img:require('../../image/home/good.png'),name:"a2",brand:'德国品牌'}]},
// {title:'J',data:[{img:require('../../image/home/good.png'),name:"a2",brand:'德国品牌'},{img:require('../../image/home/good.png'),name:"a2",brand:'德国品牌'}]}];
export default class AllBrand extends BaseComponent{
constructor(props){
  super(props);
  this.state={
    data:[],//全部品牌数据
    QueryDataClick:"",//快速切换到点击的开头
    QueryData:[],//点击快速切换到点击开头的数据
    copyData:[],//复制全部品牌数据
    click:false,
    visible:false,//轉菊花
    netError:false,//true表示网络错误
  };
}
componentDidMount(){
  var thiz=this;
  thiz.setState({visible:true})
  thiz.request("brand/getPage",{
    method:"POST",
    success:function(ret){
      let data = ret.data;
      let newData = [];
      thiz.setState({visible:false});
      if(data.records&&data.records.length>0){
        for(let i=0;i<data.records.length;i++){
          let title = data.records[i].englishName[0].toUpperCase();
          let count = 0;
          for(let ii=0;ii<newData.length;ii++){
            if(newData[ii].title==title){
              newData[ii].data.push(data.records[i]);
              count++;
            }
          }
          if(count==0){
            newData.push({
              title:title,
              data:[data.records[i]]
            });
          }
        }
      };
      //从小到大排序
      for(var jj=0;jj<newData.length;jj++){
        for(var j=0;j<newData.length-1;j++)
        {
          if(newData[j].title>newData[j+1].title)
          {
            var Temp={};
            Temp=newData[j];
            newData[j]=newData[j+1];
            newData[j+1]=Temp;
          }
        }
      };
      //获取每个列表的头部标题
      var QueryData=[];
      for(var n=0;n<newData.length;n++)
      {
        var Query=newData[n].title;
        QueryData.push(Query);
      }
      thiz.log("--------newData--------",newData);
      thiz.setState({data:newData,QueryData:QueryData,copyData:newData});
    },
    error:function(err){
      thiz.setState({visible:false});
      if(err.code&&err.code==200){
      console.log("111111111111111111111111");
          thiz.setState({netError:true});
      } 
    }
  })
};
  keyExtractor=(item,index)=>item+index;
  //sectionHeader
  sectionHeader=(info)=>{
    
    return (
      <View>
      <View style={{width:"100%",height:BaseComponent.W*30/375,backgroundColor:'#e5e4e4',justifyContent:'center'}}>
          <Text style={{fontSize:BaseComponent.W*15/375,color:'#343434',marginLeft:BaseComponent.W*10/375}}>{info.section.title}</Text>
      </View>
      </View>
    )
  };
  _keyExtractor=(item,index)=>index.toString();
  //renderItemClick
  renderItemClick=(info)=>{
    var thiz=this;
    thiz.navigate('HomeSearchResult',{brandId:info.item.id});
  }
  //renderItem
  renderItem=(info)=>{
   var thiz=this;
   thiz.log("------------------------info------------------------",info);
    return (
      <View>
      <TouchableWithoutFeedback onPress={()=>{
          thiz.renderItemClick(info);
      }}>
      <View style={{width:'100%',height:BaseComponent.W*100/375,flexDirection:'row',alignItems:'center'}}>
          <View style={{width:BaseComponent.W*60/375,height:BaseComponent.W*60/375,marginLeft:BaseComponent.W*10/375}}>
              <Image style={{width:BaseComponent.W*60/375,height:BaseComponent.W*60/375}} source={{uri:info.item.imageAttachment?(info.item.imageAttachment.resourceFullAddress?info.item.imageAttachment.resourceFullAddress:"ADS"):"dad"}}></Image>
          </View>
          <View style={{width:BaseComponent.W*280/375,height:BaseComponent.W*60/375,marginLeft:BaseComponent.W*10/375,justifyContent:'center'}}>
              <Text style={{fontSize:BaseComponent.W*13/375,color:'#343434'}}>{info.item.englishName}&nbsp;&nbsp;{info.item.name}</Text>
              <Text style={{fontSize:BaseComponent.W*13/375,color:'#999899',marginTop:BaseComponent.W*10/375}}>{info.item.country?(info.item.country.name?info.item.country.name:""):""}品牌</Text>
          </View>
      </View>
      </TouchableWithoutFeedback>
      <View style={{width:BaseComponent.W*365/375,height:0.5,backgroundColor:'#F0F0F0'}}></View>
      </View>
    )
  };
  //输入框搜索点击事件
  search=(event)=>{
      var thiz=this;
      var searchWords=event.nativeEvent.text;
      //判断用户是否输入了搜索条件
      if(searchWords==""||searchWords==null)
      {
        return ;
      }
      thiz.setState({visible:true});
      thiz.request("brand/getPage",{
        method:"POST",
        data:{searchKeywords:searchWords,page:{}},
        success:function(ret){    
          let data = ret.data;
          let newData = [];
          if(data.records&&data.records.length>0){
            for(let i=0;i<data.records.length;i++){
              let title = data.records[i].englishName[0].toUpperCase();
              let count = 0;
              for(let ii=0;ii<newData.length;ii++){
                if(newData[ii].title==title){
                  newData[ii].data.push(data.records[i]);
                  count++;
                }
              }
              if(count==0){
                newData.push({
                  title:title,
                  data:[data.records[i]]
                });
              }
            }
          };
          thiz.log("--------newData--------",newData);
          thiz.setState({data:newData,click:true,visible:false});
        },
        error:function(err){
          thiz.setState({visible:false});
          if(err.code&&err.code==200){
          console.log("111111111111111111111111");
                thiz.setState({netError:true});
            } 
        }
      })
  };

  //
  render(){
    let thiz=this;
    return (
      <View style={style.wrapper}>
        {/*状态栏，沉浸式*/}
        <StatusBar translucent={true} backgroundColor={'transparent'} barStyle={'dark-content'}/>

        {/*状态栏占位*/}
        <View style={{ backgroundColor:this.state.visible?'rgba(14,14,14,0.5)':"rgb(255,255,255)",
                      height:thiz.isIphoneX(30,BaseComponent.SH),
                      position:"relative",
                      zIndex:1,
                      opacity:1,}}></View>
        {/*顶部导航栏*/}
        <View style={style.navBar}>
          <View style={{flex:1,flexDirection:"row",justifyContent:"flex-start"}}>
            <TouchableWithoutFeedback onPress={()=>this.goBack()}>
            <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:'center'}}>
               <Image source={Imgs.back} style={{width:(BaseComponent.W*0.127*0.4),height:(BaseComponent.W*0.127*0.4),marginLeft:BaseComponent.W*0.02}}></Image>
            </View>
            </TouchableWithoutFeedback>

           
            <View style={{height:ti.select({ios:(BaseComponent.H*0.05),android:(BaseComponent.H*0.054)}),flexGrow:1,backgroundColor:"transparent",
              marginTop:ti.select({ios:(BaseComponent.H*0.02)/2,android:(BaseComponent.H*0.016)/2}),borderRadius:ti.select({ios:(BaseComponent.H*0.05)/2,android:(BaseComponent.H*0.054)/2}),borderColor:"#C7C7C7",
              borderWidth:1,justifyContent:'center',alignItems:'center'}}>
              <TextInput style={{color:"#C7C7C7",width:'100%',marginLeft:BaseComponent.W*20/375,padding:0}}
               placeholder="搜索品牌" underlineColorAndroid='transparent' maxLength={30} onSubmitEditing={(event)=>this.search(event)}></TextInput>
            </View>
          
            <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>   
            </View>
          </View>
        </View>

        {/*转菊花*/}
        <Modal
          visible={this.state.visible}
          onRequestClose={()=>{this.setState({visible:false})}}
          transparent={true}
          animationType={"fade"}>
          <View style={{backgroundColor:"rgba(0,0,0,0.4)",width:"100%",width:"100%",flex:1,flexDirection:"column",alignItems:"center",justifyContent:"center",position:"relative"}}>
            <View>
                  <ActivityIndicator size="large" color='white'/>
            </View>
          </View>
        </Modal>
      
        {/*<View style={{width:'100%',height:BaseComponent.W*40/375,flexDirection:'row',alignItems:'center'}}>
            <ScrollView style={{flex:1}} 
                horizontal={true}
                showsHorizontalScrollIndicator={false}>
            <View style={{width:'100%',height:BaseComponent.W*40/375,flexDirection:'row',alignItems:'center'}}>    
                {
                  data.map((item,index)=>{
                   
                    return (
                        <View style={{height:'100%',marginLeft:BaseComponent.W*10/375,alignItems:'center'}} key={index}>
                              <Text style={{fontSize:BaseComponent.W*15/375,color:'#343434',marginTop:BaseComponent.W*8/375}}>{item}</Text>
                              <View style={{width:BaseComponent.W*30/375,height:2,backgroundColor:'#FDD17A',marginTop:BaseComponent.W*5/375,borderRadius:1}}></View>
                        </View>
                    )
                  })
                }
            </View>  
            </ScrollView>
        </View>*/}
        
        {
          thiz.state.netError?(
              <View style={{width:BaseComponent.W,height:BaseComponent.H*0.85,justifyContent:'center',alignItems:'center'}}>
                      
                  <Image style={{width:BaseComponent.W*180/375,height:BaseComponent.W*180/375,borderRadius:BaseComponent.W*90/375}} source={require('../../image/mine/neterror.png')}></Image>
                  

                  <TouchableWithoutFeedback onPress={()=>{
                    thiz.setState({netError:false,visible:true});
                    thiz.request("brand/getPage",{
                      method:"POST",
                      data:{page:{}},
                      success:function(ret){
                        let data = ret.data;
                        let newData = [];
                        thiz.setState({visible:false});
                        if(data.records&&data.records.length>0){
                          for(let i=0;i<data.records.length;i++){
                            let title = data.records[i].englishName[0].toUpperCase();
                            let count = 0;
                            for(let ii=0;ii<newData.length;ii++){
                              if(newData[ii].title==title){
                                newData[ii].data.push(data.records[i]);
                                count++;
                              }
                            }
                            if(count==0){
                              newData.push({
                                title:title,
                                data:[data.records[i]]
                              });
                            }
                          }
                        };
                        //从小到大排序
                        for(var jj=0;jj<newData.length;jj++){
                          for(var j=0;j<newData.length-1;j++)
                          {
                            if(newData[j].title>newData[j+1].title)
                            {
                              var Temp={};
                              Temp=newData[j];
                              newData[j]=newData[j+1];
                              newData[j+1]=Temp;
                            }
                          }
                        };
                        //获取每个列表的头部标题
                        var QueryData=[];
                        for(var n=0;n<newData.length;n++)
                        {
                          var Query=newData[n].title;
                          QueryData.push(Query);
                        }
                        thiz.log("--------newData--------",newData);
                        thiz.setState({data:newData,QueryData:QueryData,copyData:newData});
                      },
                      error:function(err){
                        thiz.setState({visible:false});
                        if(err.code&&err.code==200){
                        console.log("111111111111111111111111");
                            thiz.setState({netError:true});
                        } 
                      }
                    })
                    
                  }}>
                  <View style={{width:BaseComponent.W*70/375,height:BaseComponent.W*30/375,borderRadius:BaseComponent.W*15/375,borderWidth:0.5,borderColor:'#080808',
                            justifyContent:'center',alignItems:'center',marginTop:BaseComponent.W*15/375}}>
                        <Text style={{fontSize:BaseComponent.W*15/375,color:'#252525'}}>刷新</Text>          
                  </View>
                  </TouchableWithoutFeedback>
              </View>
            ):(
            thiz.state.data.length==0?( 
                  <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                    <Image style={{width:BaseComponent.W*180/375,height:BaseComponent.W*180/375,borderRadius:BaseComponent.W*90/375}} source={require('../../image/mine/nobrands.png')}></Image>
                        <TouchableWithoutFeedback onPress={()=>{
                        thiz.setState({netError:false,visible:true});
                        thiz.request("brand/getPage",{
                          method:"POST",
                          data:{page:{}},
                          success:function(ret){
                            let data = ret.data;
                            let newData = [];
                            thiz.setState({visible:false});
                            if(data.records&&data.records.length>0){
                              for(let i=0;i<data.records.length;i++){
                                let title = data.records[i].englishName[0].toUpperCase();
                                let count = 0;
                                for(let ii=0;ii<newData.length;ii++){
                                  if(newData[ii].title==title){
                                    newData[ii].data.push(data.records[i]);
                                    count++;
                                  }
                                }
                                if(count==0){
                                  newData.push({
                                    title:title,
                                    data:[data.records[i]]
                                  });
                                }
                              }
                            };
                            //从小到大排序
                            for(var jj=0;jj<newData.length;jj++){
                              for(var j=0;j<newData.length-1;j++)
                              {
                                if(newData[j].title>newData[j+1].title)
                                {
                                  var Temp={};
                                  Temp=newData[j];
                                  newData[j]=newData[j+1];
                                  newData[j+1]=Temp;
                                }
                              }
                            };
                            //获取每个列表的头部标题
                            var QueryData=[];
                            for(var n=0;n<newData.length;n++)
                            {
                              var Query=newData[n].title;
                              QueryData.push(Query);
                            }
                            thiz.log("--------newData--------",newData);
                            thiz.setState({data:newData,QueryData:QueryData,copyData:newData});
                          },
                          error:function(err){
                            thiz.setState({visible:false});
                            if(err.code&&err.code==200){
                            console.log("111111111111111111111111");
                                thiz.setState({netError:true});
                            } 
                          }
                        })
                        
                      }}>
                      <View style={{width:BaseComponent.W*70/375,height:BaseComponent.W*30/375,borderRadius:BaseComponent.W*15/375,borderWidth:0.5,borderColor:'#080808',
                                justifyContent:'center',alignItems:'center',marginTop:BaseComponent.W*15/375}}>
                            <Text style={{fontSize:BaseComponent.W*15/375,color:'#252525'}}>刷新</Text>          
                      </View>
                    </TouchableWithoutFeedback>
                  </View>

                  ):(
              <View style={{flex:1}}>      
             {/*品牌列表*/}  
                <View style={{flex:1}}>
                  <View style={{width:BaseComponent.W*40/375,height:BaseComponent.H*350/375,position:'absolute',right:0,justifyContent:'center',
                                alignItems:'center',zIndex:1000}}>
                        {
                          this.state.QueryData.map((item,index)=>{
                            
                            return (
                              <TouchableWithoutFeedback onPress={()=>{
                                this.log("-------------------item----------------",item);

                                if(index>=this.state.data.length)
                                {
                                  this.toast("不存在"+item+"");
                                  return ;
                                }
                                this.setState({QueryDataClick:item});
                                this.refs.sections.scrollToLocation({sectionIndex:index,itemIndex:0,viewOffset:BaseComponent.W*30/375,viewPosition:0});
                              }}>
                              <View style={{width:'100%',alignItems:'center',display:this.state.click?'none':'flex'}} key={index}>
                                <Text style={{fontSize:BaseComponent.W*13.2/375,color:this.state.QueryDataClick==item?'#FDD17A':'black'}}>{item}</Text>
                              </View>
                              </TouchableWithoutFeedback>
                            )
                          })
                        }        
                  </View>

                  <View style={{flex:1}}>
                      <SectionList
                        ref="sections"
                        sections={this.state.data}
                        renderSectionHeader={this.sectionHeader}
                        renderItem={this.renderItem}
                        keyExtractor={this.keyExtractor}
                        showsVerticalScrollIndicator = {false}
                        extraData={this.state}/>
                  </View>


                </View>
                </View>)
                )
        }

 
      </View>    
    )
  }
}