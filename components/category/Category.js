/**
 * @name Category.js
 * @auhor 程浩
 * @date 2018.8.24
 * @desc APP分类
 */

import React, {Component} from "react";
import {ActivityIndicator,Platform, StyleSheet, View,Text,Button,Image,TouchableWithoutFeedback,TextInput,RefreshControl} from "react-native";
import {StatusBar,ScrollView,FlatList,SectionList,Modal} from "react-native";

// 导入工具函数库
import T from "../../lib/Tools";
import BaseComponent from "../BaseComponent";
// 导入公共样式
import St from "../../lib/Style";
// 导入图片资源库
import Imgs from "../../lib/Img";
import FenleiXiangqing from './FenleiXiangqing';
import GoodsXiangQing from './GoodsXiangQing';
import AllBrand from './AllBrand';
// 转菊花
import Spinner from 'react-native-loading-spinner-overlay';

// Tools实例
let ti = T.getInstance();
let style = StyleSheet.create({
  wrapper:{
    backgroundColor:"#fff",
    flex:1,
    flexDirection:"column",
    alignItems:"stretch",
    justifyContent:"flex-start",
    position:"relative",
  },
  statusBar:{
    backgroundColor:"#fff",
    height:ti.select({ios:16,android:22}),
    position:"relative",
    zIndex:1,
    opacity:1,
  },
  navBar:{
    backgroundColor:"white",
    height:"7%",
    position:"relative",
    zIndex:1,
    opacity:1,
  },
});

export default class Category extends BaseComponent{
  constructor(props){
    super(props);
    this.state={
      onClick:0,//默认是品牌
      grid:false,//切换布局
      getPageParent:null,//获取分类名称,例如国家、品牌
      visible:false,
      getPageCountryPavilions:null,//获取国家分类数据,
      getBrand:[{key:'推荐品牌',data:[{name:"",img:"",id:null}]}],//推荐品牌
      sectionBrand:false,//点击品牌显示全部品牌字和推荐品牌,true为不显示.
      isRefreshing:false,//下拉刷新
      pageNumber:1,//当前页数
      pageSize:25,//每页的数据大小
      hasNext:"",//是否有下一页数据,true表示有太阳,false表示没有
      goodsGroupId:"",//点击的是分类的哪一个
    }
  };

  componentDidMount(){
    var thiz=this;
    //监听最近搜索数据
    thiz.listen('recent_records',function(ret){
      thiz.RecentlyRecords(ret);
      thiz.T.load("RecentlyRecords",function(data){
      thiz.log("----------------------------zero--------------------------",data);
      if(data===null)
      {
        thiz.setState({datasource:[]});
      }
      else{
        thiz.setState({datasource:data});
      }
      
    });
    });
    thiz.setState({visible:true});
    thiz.request("goodsGroup/getPageParent",{
        method:"POST",
        success:function(ret){
          // thiz.toast("数据加载成功");
          // console.log("-------fenlei-------",ret.data.records);
          let categoryData=[{id:null,name:"品牌"},{id:null,name:"国家"}];
          if(!ret.data.records){
            thiz.setState({visible:false});
            return ;
          }
          for(var i=0;i<ret.data.records.length;i++)
          {
              var id=ret.data.records[i].id;
              var name=ret.data.records[i].name;
              categoryData.push({
                id:id,
                name:name,
              });
          }
          thiz.setState({
            getPageParent:categoryData,
          });
          var data1={hot:true};
          data1.page={};
          data1.page.pageSize=thiz.state.pageSize;
          data1.page.pageNumber=thiz.state.pageNumber;
          thiz.request("brand/getPage",{
              method:"POST",
              data:data1,
              success:function(ret){

                thiz.setState({visible:false});

                  var data=[{key:'推荐品牌'}];
                  if(!ret.data.records){
                    return ;
                  };
                for(var n=0;n<ret.data.records.length;n++)
                {
                  var id=ret.data.records[n].id;
                  var name=ret.data.records[n].name;
                  if(!ret.data.records[n].imageAttachment){
                      ret.data.records[n].imageAttachment={};
                      if(!ret.data.records[n].imageAttachment.resourceFullAddress)
                      {
                        ret.data.records[n].imageAttachment.resourceFullAddress="sdada";
                      }
                  }
                  var imgPath=ret.data.records[n].imageAttachment.resourceFullAddress;
                
                  if(!data[0].data){
                    data[0].data = [];
                  }
                  data[0].data.push({
                    name:name,
                    img:imgPath,
                    id:id,
                  })
                }
                  thiz.log("------------------------------getBrand-----------------------------",data);
                   thiz.setState({
                    getBrand:data,
                    hasNext:ret.data.hasNext,
                  })
              },
              error:function(err){
                thiz.setState({visible:false});
                console.log("-------err--------",err);
              }
            })
           
        },
        error:function(err){
          thiz.setState({visible:false});
          // thiz.toast("获取分类失败");
          // console.log('--------------获取分类--------',err);
        }
    },false)

  };

  //下拉刷新
  onRefresh=()=>{
    var thiz=this;
    var state=thiz.state;
    state.getBrand=[];
    state.pageNumber=1;
    state.isRefreshing=true;
    state.hasNext=true;

    thiz.setState(state);
    thiz.getBrandData();
  };
  //获取全部品牌数据
  getBrandData=()=>{
    var thiz=this;
    var pageSize=thiz.state.pageSize;
    var pageNumber=thiz.state.pageNumber;
    var goodsGroupId=thiz.state.goodsGroupId;
    console.log("----------------goodsGroupId-------------------",goodsGroupId);
    var params={};
    if(thiz.state.isRefreshing)
    {
      console.log("--------------------getBrandData_isRefreshing_pageNumber---------------------------",pageNumber);
      thiz.setState({visible:true});
    }else{
      pageNumber++;
      console.log("--------------------getBrandData_load_pageNumber---------------------------",pageNumber);
      thiz.setState({visible:true})
    }
    params.page={};
    params.page.pageNumber=pageNumber;
    params.page.pageSize=pageSize;
    if(goodsGroupId!="")
    {
      params.goodsGroupId=goodsGroupId;
    }else{
      params.hot=true;
    }
    console.log("--------------------getBrandData_pageSize---------------------------",pageSize);
    console.log("--------------------getBrandData_pageNumber---------------------------",pageNumber);
    thiz.log("----------------------------getBrandData_params--------------------------",params);
    thiz.request("brand/getPage",{
      method:"POST",
      data:params,
      success:function(ret){
         thiz.setState({visible:false});
         if(thiz.state.isRefreshing)
         {
           
            if(!thiz.state.sectionBrand)
            {
               var data=[{key:"推荐品牌",data:[]}];
            }else{
               var data=[{key:"",data:[]}];
            }
         }else{
          var data=thiz.state.getBrand;
         }
          
          if(!ret.data.records){
              thiz.setState({isRefreshing:false,hasNext:ret.data.hasNext,getBrand:data});
              return ;
            };
          if(ret.data.records.length==0)
          {
            thiz.setState({isRefreshing:false,hasNext:ret.data.hasNext,getBrand:data});
            return ;
          }
          for(var n=0;n<ret.data.records.length;n++)
          {
            var id=ret.data.records[n].id;
            var name=ret.data.records[n].name;
            if(!ret.data.records[n].imageAttachment){
                ret.data.records[n].imageAttachment={};
                if(!ret.data.records[n].imageAttachment.resourceFullAddress)
                {
                  ret.data.records[n].imageAttachment.resourceFullAddress="sdada";
                }
            }
            var imgPath=ret.data.records[n].imageAttachment.resourceFullAddress;
          
            if(!data[0].data){
              data[0].data = [];
            }
            data[0].data.push({
              name:name,
              img:imgPath,
              id:id,
            })
          }
            thiz.log("------------------------------getBrand-----------------------------",data);
            thiz.setState({
              getBrand:data,
              isRefreshing:false,
              pageNumber:pageNumber,
              hasNext:ret.data.hasNext
            })
      },
      error:function(err){
        thiz.setState({
          isRefreshing:false,
          visible:false,
          // hasNext:ret.data.hasNext
        });
      }
    });
  };
  render(){
    let thiz=this;
    return (
         <View style={style.wrapper}>
           
           {/*转菊花*/}
          <Modal
          visible={this.state.visible}
          onRequestClose={()=>{this.setState({visible:false})}}
          transparent={true}
          animationType={"fade"}>
          <View style={{backgroundColor:"rgba(14,14,14,0.5)",width:"100%",width:"100%",flex:1,flexDirection:"column",alignItems:"center",justifyContent:"center",position:"relative"}}>
            <View>
                  <ActivityIndicator size="large" color='white'/>
            </View>
          </View>
        </Modal>

        {/*状态栏，沉浸式*/}
        <StatusBar translucent={true} backgroundColor={this.state.visible?'rgba(14,14,14,0.5)':'transparent'} barStyle={'dark-content'}/>

        {/*状态栏占位*/}
        <View style={{backgroundColor:"#fff",
                      height:thiz.isIphoneX(30,BaseComponent.SH),
                      position:"relative",
                      zIndex:1,
                      opacity:1,}}></View>
        {/*顶部导航栏*/}
        <View style={style.navBar}>
          <View style={{flex:1,flexDirection:"row",justifyContent:"flex-start"}}>

            <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent"}}></View>
           
            <View style={{height:ti.select({ios:(BaseComponent.H*0.05),android:(BaseComponent.H*0.054)}),flexGrow:1,backgroundColor:"transparent",
              marginTop:ti.select({ios:(BaseComponent.H*0.02)/2,android:(BaseComponent.H*0.016)/2}),borderRadius:ti.select({ios:(24),android:(BaseComponent.H*0.054)/2}),borderColor:"#C7C7C7",
              borderWidth:1,justifyContent:'center',alignItems:'center'}}>
              <TextInput style={{color:"#121212",width:'100%',textAlign:'center',padding:0,}} placeholderTextColor="#C7C7C7"
               placeholder="请输入商品名，品牌或国家" underlineColorAndroid='transparent' maxLength={30} onSubmitEditing={(event)=>this.search(event)}></TextInput>
            </View>
            
            <View style={{opacity:0,width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>
              <Image source={require('../../image/home/fenleimsg.png')} style={{width:(BaseComponent.W*0.127*0.5),height:(BaseComponent.W*0.127*0.5)}}></Image>
            </View>
          </View>
        </View>

      <View style={{width:'100%',height:1,backgroundColor:'#EBEBF2',marginTop:BaseComponent.W*0.01}}></View>
      
      <View style={{flexDirection:'row',flex:1}}>
      {/*分类*/}
      <View style={{width:BaseComponent.W*0.277,height:'100%',alignItems:'center'}}>
         <FlatList
          data={this.state.getPageParent}
          keyExtractor={this._keyExtractor}
          renderItem={this._renderItem}
          showsVerticalScrollIndicator = {false}
          extraData={this.state}
          />
      </View>

      <View style={{width:0.5,height:'100%',backgroundColor:'rgba(178, 178, 178, 1)',
                    position:'absolute',left:BaseComponent.W*0.277}}>
      </View>
      
      {/*详细国家名字*/}
      <View style={{flex:1,display:this.state.grid?'flex':'none'}}>
        <FlatList
            data={this.state.getPageCountryPavilions}
            keyExtractor={this._keyExtractor}
            renderItem={this._renderItem1}
            showsVerticalScrollIndicator = {false}
            numColumns={2}
            extraData={this.state}
            />
      </View>

      {/*品牌分类*/}
      <View style={{flex:1,display:this.state.grid?'none':'flex'}}>
          {/*全部品牌*/}
          <TouchableWithoutFeedback onPress={()=>this.navigate('AllBrand')}>
          <View style={[styles.Brand,{display:this.state.sectionBrand?'none':'flex'}]}>
              <View style={{width:BaseComponent.W*0.648,height:BaseComponent.W*0.064,
                borderRadius:BaseComponent.W*0.032,borderWidth:1,borderColor:'rgba(136, 136, 136, 1)',
                justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
                  <Text>全部品牌</Text>
                  <Image style={{width:BaseComponent.W*0.03,height:BaseComponent.W*0.055,marginLeft:BaseComponent.W*0.03}} source={require('../../image/home/youjiantou.png')}></Image>
              </View>
          </View>
          </TouchableWithoutFeedback>

        {/*品牌Item*/}
        <SectionList
          sections={this.state.getBrand}
          renderSectionHeader={this.state.sectionBrand?this.sectionHeader1:this.sectionHeader}
          renderItem={this.renderItem}
          keyExtractor={this.keyExtractor}
          showsVerticalScrollIndicator = {false}
          stickySectionHeadersEnabled={false}
          refreshing={this.state.isRefreshing}
          refreshControl={
          <RefreshControl
            refreshing={thiz.state.isRefreshing}
            onRefresh={()=>{thiz.onRefresh()}}
            colors={['#ff0000', '#00ff00','#0000ff','#3ad564']}
            progressBackgroundColor="#ffffff"
          />}
          onMomentumScrollEnd={(e)=>{
            var offsetY = e.nativeEvent.contentOffset.y; //滑动距离
            var contentSizeHeight = e.nativeEvent.contentSize.height; //scrollView contentSize高度
            var oriageScrollHeight = e.nativeEvent.layoutMeasurement.height; //scrollView高度
            console.log("---------------------滑动距离-------------------",offsetY);
            console.log("---------------------scrollView contentSize高度-------------------",contentSizeHeight);
            console.log("---------------------scrollView高度-------------------",oriageScrollHeight);
            if (offsetY + oriageScrollHeight >= contentSizeHeight)
            {
                 console.log("------------------------onEndReached_hasNext-------------------------",thiz.state.hasNext);
                  if(thiz.state.hasNext){
                  thiz.getBrandData();
                }
                else
                {
                  // thiz.toast("已经到底了");
                }
            }
        }}
        />

      </View>

      </View>
      </View>
    )
  };
  _keyExtractor=(item,index)=>index.toString();
  keyExtractor=(item,index)=>item+index;

  //输入框搜索点击事件
  search=(event)=>{
      var thiz=this;
      var searchWords=event.nativeEvent.text;
      //判断用户是否输入了搜索条件
      if(searchWords==""||searchWords==null)
      {
        return ;
      }
      this.emit('recent_records',{recent_records:searchWords});
      thiz.navigate('FenleiXiangqing',{searchKeywords:searchWords})
  };
   //保存最近搜索的记录，只保存10条
   RecentlyRecords=(record)=>{
    var thiz=this;
    thiz.log('------------------------recent_records-------------------------',record);
    //先拿到保存的最近搜索数组，如果不存在，则新建一个数组
    thiz.T.load("RecentlyRecords",function(ret,err){
      var data = [];
      if(ret){
        thiz.log("-----------------------------------ret--------------------------",ret);
        data = ret;
        for(var i=0;i<data.length;i++)
        {
          if(record.recent_records==data[i])
          {
            return ;
          }
        }
        data.push(record.recent_records);
        if(data.length>10)
        {
          data=thiz.T.delArr(data,0);
        }
      }else{
        thiz.log("------------------first_data----------------------",data);
        data.push(record.recent_records);
      }
      thiz.T.save("RecentlyRecords",data);
      thiz.log("------------------second_data----------------------",data);
    });

    
   };

  //sectionHeader有头列表的
  sectionHeader=(info)=>{
    return (
      <View style={{width:'100%',height:BaseComponent.W*0.1,justifyContent:'center',marginLeft:BaseComponent.W*0.026}}>
        <Text style={{fontSize:BaseComponent.W*0.0373,color:'rgba(62, 62, 62, 1)'}}>{info.section.key}</Text>
      </View>
    )
  }
   //sectionHeader没有头列表的
  sectionHeader1=(info)=>{
    return (
      <View></View>
    )
  }
  //sectionItem
  renderItem=(info)=>{
  this.log("------------------------section------------------------------",info);
    return (  
        info.index==info.section.data.length-1?(<View>
        <FlatList
          alwaysBounceVertical={false}
           data={info.section.data}
           keyExtractor={this._keyExtractor}
           renderItem={(info)=>{
            return (
            <TouchableWithoutFeedback onPress={()=>{
              if(!this.state.sectionBrand)
              {this.BrandOnclick(info)}
              else
              {this.goodsGroupOnclick(info)}
            }
            }>
            <View style={{width:BaseComponent.W*0.213,height:BaseComponent.W*0.29,
             marginLeft:BaseComponent.W*0.026,marginTop:BaseComponent.W*0.02,
              alignItems:'center'}}>
              <View style={{borderWidth:0.5,borderColor:'rgba(240, 240, 240, 1)'}}>
            <Image style={{width:BaseComponent.W*0.2,height:BaseComponent.W*0.2}} source={{uri:info.item.img}}></Image>
            </View>
            <Text style={{marginTop:BaseComponent.W*5/375}}>{info.item.name}</Text>
            </View>
            </TouchableWithoutFeedback>
           )}}
           numColumns={3}
        />
        </View>):(<View></View>)
    )
  }
 

  //推荐品牌点击事件
  BrandOnclick=(info)=>{
    var thiz=this;
    // thiz.toast(info.item.id);
    thiz.navigate('HomeSearchResult',{brandId:info.item.id})
  }
  //其余分类点击事件、母婴、个护之类的
  goodsGroupOnclick=(info)=>{
    var thiz=this;
   thiz.navigate('HomeSearchResult',{brandId:info.item.id})
  }

  /*分类、品牌、国家、服饰之类的*/
  _renderItem=(info)=>{

    return (
      <TouchableWithoutFeedback onPress={()=>{
        var thiz=this;
        thiz.setState({onClick:info.index});
        if(info.item.name=="国家"){
          thiz.setState({grid:true});
        }else{
          thiz.setState({grid:false});
        };
        //点击国家
        if(info.item.id==null&&info.item.name=="国家")
        {
          thiz.setState({visible:true});
          thiz.request("countryPavilion/getPage",{
            method:"POST",
            success:function(ret){

              thiz.setState({visible:false});


              // thiz.toast("获取国家信息成功");
              // console.log("--------国家---------",ret);
              var CountryData=[];
              if(!ret.data.records)
              {
                
                return ;
              }
              for(var j=0;j<ret.data.records.length;j++)
              {
                var id=ret.data.records[j].id;
                var countryImgpath=ret.data.records[j].imageAttachment.resourceFullAddress;
                if(!ret.data.records[j].imageAttachment)
                {
                  ret.data.records[j].imageAttachment={};
                  if(!ret.data.records[j].imageAttachment.resourceFullAddress)
                  {
                    ret.data.records[j].imageAttachment.resourceFullAddress="adsa";
                  }
                }
                CountryData.push({
                  img:countryImgpath,
                  id:id,
                })
              }
              thiz.setState({getPageCountryPavilions:CountryData})
              // console.log(thiz.state.getPageCountryPavilions);
            },
            error:function(err){
                thiz.setState({visible:false})
                // console.log("------国家错误--------",ret);
                thiz.toast("请检查网络，稍后再试");
            }
          })
        }
        else
        {
            var data2={};
            var data=[];
            if(info.item.id==null&&info.item.name=="品牌")
            {
              //点击品牌
              data2.page={ 
                      pageNumber:1,
                      pageSize:thiz.state.pageSize,
                    };
              data2.hot=true;      
              data=[{key:'推荐品牌'}];
              thiz.setState({sectionBrand:false,goodsGroupId:"",pageNumber:1})
            }else
            {
              //点击其它分类
              data2.goodsGroupId=info.item.id;
              data2.page={ 
                      pageNumber:1,
                      pageSize:thiz.state.pageSize,
                    }
               data=[{key:''}];
               thiz.setState({sectionBrand:true,goodsGroupId:info.item.id,pageNumber:1});
            }
            thiz.setState({visible:true});
            thiz.request("brand/getPage",{
              method:"POST",
              data:data2,
              success:function(ret){
                thiz.setState({visible:false});

                if(!ret.data.records)
                {
                  thiz.setState({getBrand:[{key:"",data:[]}]});
                  return ;
                }
                if(ret.data.records==0)
                {
                  thiz.setState({getBrand:[{key:"",data:[]}]});
                  return ;
                }
                // var data=[{key:'推荐品牌'}];
                for(var n=0;n<ret.data.records.length;n++)
                {
                  var id=ret.data.records[n].id;
                  var name=ret.data.records[n].name;
                  if(!ret.data.records[n].imageAttachment)
                  {
                      ret.data.records[n].imageAttachment={};
                      if(!ret.data.records[n].imageAttachment.resourceFullAddress)
                      {
                        ret.data.records[n].imageAttachment.resourceFullAddress="adsa";
                      }
                  }
                  var imgPath=ret.data.records[n].imageAttachment.resourceFullAddress;
                  if(!data[0].data){
                    data[0].data = [];
                  }
                  data[0].data.push({
                    name:name,
                    img:imgPath,
                    id:id,
                  })
                }
                   thiz.setState({getBrand:data})
                // console.log("--------data-------",data);
              },
              error:function(err){
                thiz.setState({visible:false});
                console.log("-------err--------",err);
              }
            })
        }   
      }
      }>  
      <View style={{ width:BaseComponent.W*0.213,
                     height:BaseComponent.W*0.067,
                      marginTop:BaseComponent.W*0.065,
                      justifyContent:'center',
                      alignItems:'center',
                      borderRadius:BaseComponent.W*0.03,
                      backgroundColor:info.index==this.state.onClick?'#FED584':'#fff'}}>
          <Text style={{fontSize:BaseComponent.W*0.04,color:info.index==this.state.onClick?'white':'black'}}>{info.item.name}</Text>
      </View>
      </TouchableWithoutFeedback>
    )
  };
  //详细国家名字
  _renderItem1=(info)=>{
    // console.log("---------info-----------",info);
    return (
        <TouchableWithoutFeedback onPress={()=>{
          // this.toast(info.item.id);
          this.navigate('FenleiXiangqing',{countryPavilionId:info.item.id});
        }}>
        <View style={{width:BaseComponent.W*0.32,height:BaseComponent.W*0.192,
          marginTop:BaseComponent.W*0.067,marginLeft:BaseComponent.W*0.0267}}>
          <Image style={{width:BaseComponent.W*0.32,height:BaseComponent.W*0.192,borderRadius:3,}} source={{uri:info.item.img}}/>
        </View>
        </TouchableWithoutFeedback>
    )
  };

};
const styles=StyleSheet.create({
    Brand:{
      width:'100%',
      height:BaseComponent.W*0.09,
      alignItems:'center',
      marginTop:BaseComponent.W*0.0293,
    },
     footer:{
        flexDirection:'row',
        height:24,
        justifyContent:'center',
        alignItems:'center',
        marginBottom:10,
    },
});