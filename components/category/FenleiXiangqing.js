/**
 * @name FenleiXiangqing.js
 * @auhor 程浩
 * @date 2018.8.24
 * @desc 分类详情
 */
import React,{Component} from 'react';
import {Text,View,StyleSheet,Image,TouchableWithoutFeedback,ScrollView,StatusBar,TextInput,FlatList,Modal,ActivityIndicator,Platform,RefreshControl} from 'react-native';
 // 导入工具函数库
import T from "../../lib/Tools";
// 导入公共样式
import St from "../../lib/Style";
// 导入图片资源库
import Imgs from "../../lib/Img";
import GoodsXiangQing from "./GoodsXiangQing";
import BaseComponent from '../BaseComponent';
// 转菊花
import Spinner from 'react-native-loading-spinner-overlay';
// let data=['美白','保湿','抗皱','祛痘','淡斑','去角质','遮瑕'];


// Tools实例
let ti = T.getInstance();

let style = StyleSheet.create({
  wrapper:{
    backgroundColor:"#F0F0F0",
    flex:1,
    flexDirection:"column",
    alignItems:"stretch",
    justifyContent:"flex-start",
    position:"relative",
  },
  statusBar:{
    backgroundColor:"rgb(255,255,255)",
    height:BaseComponent.SH,
    position:"relative",
    zIndex:1,
    opacity:1,
  },
  navBar:{
    backgroundColor:"rgb(255,255,255)",
    height:BaseComponent.H*0.07,
    position:"relative",
    zIndex:1,
    opacity:1,
    borderBottomWidth:1,
    borderBottomColor:"rgb(241,241,241)"
  },
});


export default class FenleiXiangqing extends BaseComponent{
  constructor(props){
    super(props);
    this.state={
      shitu:false,//切换视图布局,false表示一列，true表示两列
      isvisible:false,//筛选条件弹出框
      countryPavilionIds:[],//countryPavilionId国家ID
      brandIds:[],//brandId品牌ID
      columnGroupItemId:"",
      goodsGroupIds:[],//goodsGroupId其余分类的ID
      data:[],//商品数据
      pageNumber:1,//当前页数
      pageSize:BaseComponent.PAGE_SIZE,//每页显示多少条数据
      hasNext:"",//是否有下一条数据,
      totalPage:"",//数据总页数
      clickType:false,//筛选条件是否点击类型
      clickBrand:false,//筛选条件是否点击品牌
      clickCountry:false,//筛选条件是否点击国家
      clickGongneng:false,//筛选条件是否点击功能
      visible:false,
      goodsGroups:[],//筛选条件类型数据
      usageGroups:[],//筛选条件功能数据
      brands:[],//筛选条件品牌数据
      countries:[],//筛选条件国家数据
      
      searchKeywords:"",//搜索词
      flatlistData:[],//筛选数据
      isRefreshing:false,//下拉刷新,
      netError:false,//true表示网络错误
      selectedGoodsGroups:[],//选中的类型数据条件,只能有一个
      selectedUsageGroups:[],//选中的功能数据条件,只能有一个
      selectedBrands:[],//选中的品牌数据条件,只能有一个
      selectedCountries:[],//选中的国家数据条件,只能有一个
      isRefreshing:false,//下拉刷新,
      filterData:{},// 筛选数据
      SALES_NUM:0,//销量 默认是0,1代表升序,2代表降序
      pageData:{},// 页面传过来的参数
      PRICE:0,//价格 默认是0,1代表升序,2代表降序
      underline:true,//默认是true代表综合
      orders:{},//销量价格筛选
      params:{page:{pageNumber:1,pageSize:BaseComponent.PAGE_SIZE}},//传给后台的参数，这个很重要！！！！！,就是里面包含各种各样的筛选条件
    }
    
    this.curPageNum = 0;// 当前加载的页码
    this.isLoading = false;// 是否在加载中
  }
  componentDidMount(){
    
    var thiz=this;
    var data={};
    //监听最近搜索数据
    thiz.listen('recent_records',function(ret){
      thiz.RecentlyRecords(ret);
      thiz.T.load("RecentlyRecords",function(data){
      thiz.log("----------------------------zero--------------------------",data);
      if(data===null)
      {
        thiz.setState({datasource:[]});
      }
      else{
        thiz.setState({datasource:data});
      }
      
    });
    });

    //从国家点击进来的话接收国家ID
    if(thiz.params&&thiz.params.countryPavilionId){
      var countryPavilionIds=thiz.params.countryPavilionId;
      console.log("----------------countryPavilionIds---------------",countryPavilionIds);
      
      if(!data.countryPavilionIds){
        data.countryPavilionIds=[];

        data.countryPavilionIds.push(countryPavilionIds);
      }
      if(!thiz.state.params.countryPavilionIds)
      {
        thiz.state.params.countryPavilionIds=data.countryPavilionIds;
      }
        thiz.setState({
        countryPavilionIds:data.countryPavilionIds,
        params:thiz.state.params,
      });
    }
    
    //从搜索框点击进来
    if(thiz.params&&thiz.params.searchKeywords)
    {
      var searchKeywords=thiz.params.searchKeywords;
      console.log("--------------------searchKeywords----------------------",searchKeywords);
      if(!data.searchKeywords){
        data.searchKeywords=searchKeywords;
      }
      if(!thiz.state.params.searchKeywords)
      {
        thiz.state.params.searchKeywords=searchKeywords;
      }
      thiz.setState({searchKeywords:searchKeywords,params:thiz.state.params});
    }

    // 从首页的分类点进来
    if(thiz.params&&thiz.params.from=="index"){
      thiz.log("--------thiz.params.interParams--------",thiz.params.interParams);
      data = Object.assign(data,thiz.params.interParams);
      thiz.log("--------new_data--------",data);
      if(!thiz.state.params.goodsGroupIds)
      {
        thiz.state.params.goodsGroupIds=data.goodsGroupIds;
        thiz.setState({goodsGroupIds:data.goodsGroupIds})
      }
      if(!thiz.state.params.columnGroupItemId)
      {
        thiz.state.params.columnGroupItemId=data.columnGroupItemId;
        thiz.setState({columnGroupItemId:data.columnGroupItemId});
      }

      thiz.setState({
        // goodsGroupIds:data.goodsGroupIds,
        // columnGroupItemId:data.columnGroupItemId,
        selectedGoodsGroups:data.goodsGroupIds?data.goodsGroupIds:[],
        params:thiz.state.params
      });
    }


    thiz.setState({
      pageData:data,
    });

    if(!data.page){
      data.page={};
      data.page.pageSize=BaseComponent.PAGE_SIZE;
      data.page.pageNumber=1;
    }
    thiz.setState({visible:true});
    thiz.request("spu/getOnSalePage",{
      method:"POST",
      data:data,
      success:function(ret){
          thiz.log("--------FenleiXiangqing_ret--------",ret);
          var data1=thiz.state.data;
          thiz.screening();
          if(!ret.data.spuPage.records)
          {
            thiz.setState({visible:false})
            return ;
          }
          if(ret.data.spuPage.records.length==0){
              thiz.setState({visible:false})
              return ;
          }
          for(var i=0;i<ret.data.spuPage.records.length;i++)
          {
                var data2={};
                var img=ret.data.spuPage.records[i].attachment.resourceFullAddress;//商品图片路径;
                var id=ret.data.spuPage.records[i].id;//商品ID
                var ins=ret.data.spuPage.records[i].name;//商品介绍
                var price=ret.data.spuPage.records[i].buyUnitPrice;//商品卖出的价格
                // var cutprice=ret.data.spuPage.records[i].marketPrice;//商品市场价
                // var logisticPrice=ret.data.spuPage.records[i].logisticPrice;//logisticPrice
                // var taxPrice=ret.data.spuPage.records[i].taxPrice;//taxPrice
                // var surplusStockNum=ret.data.spuPage.records[i].surplusStockNum;//surplusStockNum
                data2.img=img;
                data2.id=id;
                data2.ins=ins;
                data2.price=price;
                // data2.cutprice=cutprice;
                // data2.logisticPrice=logisticPrice;
                // data2.taxPrice=taxPrice;
                // data2.surplusStockNum=surplusStockNum;
                data1.push(data2);
          }
          thiz.setState({
            data:data1,
            visible:false,
            hasNext:ret.data.spuPage.hasNext,
          });
          
      },
      error:function(err){
          thiz.setState({visible:false});
          if(err.code&&err.code==200){
            thiz.setState({netError:true});
          }   
      }
    });
  }
 
  // 渲染每一项筛选条件
  _renderItem1=(info)=>{
    var thiz=this;
    thiz.log("--------------------筛选info--------------------",info);
    return (
      <TouchableWithoutFeedback onPress={()=>{

        // 所有的数据
        let filterData = thiz.state.filterData;

        let backData = {
          goodsGroups:thiz.state.selectedGoodsGroups,//选中的类型数据条件,
          // usageGroups:thiz.state.selectedUsageGroups,//选中的功能数据条件,
          brands:thiz.state.selectedBrands,//选中的品牌数据条件,
          countries:thiz.state.selectedCountries,//选中的国家数据条件,
        }




        thiz.log("--------------backData_before-------------------",backData);
        // 点击某一项
        let item = info;
        
        // 该项所属筛选条件
        thiz.log("--------item.item--------",item.item);
        let filterType = item.item.filterType;
        // 该项的index
        let index = item.index;

        // 重置该项选中状态
        let isSelected = !item.item.isSelected;
        item.item.isSelected = isSelected;

        thiz.log("--------filterType_name--------",filterType);
        if(filterData[filterType]){
          thiz.log("--------names--------",filterData[filterType][index].name+","+item.item.name);
          filterData[filterType][index] = item.item;
        }

        // id去重
        for(let i=0;i<backData[filterType].length;i++){
          if(backData[filterType][i]==item.item.id){
            backData[filterType].splice(i,1);
          }
        }
        thiz.log("--------------backData_quchong-------------------",backData);

        if(isSelected&&backData[filterType]){
          backData[filterType].push(item.item.id);
        }

        thiz.log("--------------backData_after-------------------",backData);




        thiz.setState({
          filterData:filterData,
          selectedGoodsGroups:backData.goodsGroups,
          // selectedUsageGroups:backData.usageGroups,
          selectedBrands:backData.brands,
          selectedCountries:backData.countries
        });  

        
      }}>
      <View style={{width:BaseComponent.W*0.4,height:BaseComponent.W*43/375,flexDirection:'row',justifyContent:'space-between',alignItems:'center',
                    marginLeft:BaseComponent.W*18/375,borderBottomWidth:1,borderBottomColor:info.item.isSelected?'#FDD17A':'transparent'}}>
          <Text style={{fontSize:BaseComponent.W*15/375,color:'#2A2A2A'}}>{info.item.name}</Text>
          <Image style={{width:BaseComponent.W*15/375,height:BaseComponent.W*12/375,display:info.item.isSelected?'flex':'none'}} source={require('../../image/home/duihao.png')}></Image>
      </View>
      </TouchableWithoutFeedback>
    )
  };
  
  //重置筛选条件
  reset=()=>{
    var thiz=this;
    var selectedGoodsGroups=thiz.state.selectedGoodsGroups;
    var selectedBrands=thiz.state.selectedBrands;
    var selectedCountries=thiz.state.selectedCountries;
    var params=thiz.state.params;
    var filterData=thiz.state.filterData;
    thiz.log("-----------------------selectedGoodsGroups--------------------------------",selectedGoodsGroups);
    thiz.log("--------------------------selectedBrands----------------------------------",selectedBrands);
    thiz.log("---------------------------selectedCountries------------------------------",selectedCountries);
    thiz.log("----------------------------filterData------------------------------------",filterData);
    thiz.log("--------------------------params----------------------------------------",params);
    for(var key in filterData)
    {
      for(var i=0;i<filterData[key].length;i++)
      {
        filterData[key][i].isSelected=false;
      }
    }
    thiz.setState({selectedGoodsGroups:[],selectedBrands:[],selectedCountries:[],filterData:filterData,isvisible:false});
    if(!params.goodsGroupIds){
      params.goodsGroupIds=[];
    }else{
      params.goodsGroupIds=[];
    }

    if(!params.brandIds){
      params.brandIds=[];
    }else{
      params.brandIds=[];
    }

    if(!params.countryIds)
    {
      params.countryIds=[];
    }else{
      params.countryIds=[];
    }
    if(thiz.state.goodsGroupIds instanceof Array&&thiz.state.goodsGroupIds.length>0){
      params.goodsGroupIds=thiz.state.goodsGroupIds;
    }
    thiz.setState({visible:true});
    thiz.request("spu/getOnSalePage",{
        method:"POST",
        data:params,
        success:function(ret){
          var data=[];
          if(!ret.data.spuPage.records)
          {
            thiz.setState({
              visible:false,
              data:data,
              params:params,
              hasNext:ret.data.spuPage.hasNext
            });
            return;
          }
          //重构商品列表数据结构  
          for(var i=0;i<ret.data.spuPage.records.length;i++)
          {
                var data2={};
                var img=ret.data.spuPage.records[i].attachment.resourceFullAddress;//商品图片路径;
                var id=ret.data.spuPage.records[i].id;//商品ID
                var ins=ret.data.spuPage.records[i].name;//商品介绍
                var price=ret.data.spuPage.records[i].buyUnitPrice;//商品卖出的价格
                // var cutprice=ret.data.spuPage.records[i].marketPrice;//商品市场价
                // var logisticPrice=ret.data.spuPage.records[i].logisticPrice;//logisticPrice
                // var taxPrice=ret.data.spuPage.records[i].taxPrice;//taxPrice
                // var surplusStockNum=ret.data.spuPage.records[i].surplusStockNum;//surplusStockNum
                data2.img=img;
                data2.id=id;
                data2.ins=ins;
                data2.price=price;
                // data2.cutprice=cutprice;
                // data2.logisticPrice=logisticPrice;
                // data2.taxPrice=taxPrice;
                // data2.surplusStockNum=surplusStockNum;
                data.push(data2);
          }
            thiz.setState({
              visible:false,
              data:data,
              params:params,
              hasNext:ret.data.spuPage.hasNext
            });
        },
        error:function(err){
            thiz.setState({visible:false});
            if(err.code&&err.code==200){
              thiz.setState({netError:true});
            }   
        }
      });
    
  };

    //保存最近搜索的记录，只保存10条
   RecentlyRecords=(record)=>{
    var thiz=this;
    thiz.log('------------------------recent_records-------------------------',record);
    //先拿到保存的最近搜索数组，如果不存在，则新建一个数组
    thiz.T.load("RecentlyRecords",function(ret,err){
      var data = [];
      if(ret){
        thiz.log("-----------------------------------ret--------------------------",ret);
        data = ret;
        for(var i=0;i<data.length;i++)
        {
          if(record.recent_records==data[i])
          {
            return ;
          }
        }
        data.push(record.recent_records);
        if(data.length>10)
        {
          data=thiz.T.delArr(data,0);
        }
      }else{
        thiz.log("------------------first_data----------------------",data);
        data.push(record.recent_records);
      }
      thiz.T.save("RecentlyRecords",data);
      thiz.log("------------------second_data----------------------",data);
    });  
  };

  //筛选条件
  screening=()=>{
    var thiz=this;
    var countryPavilionIds=thiz.state.countryPavilionIds;
    var goodsGroupIds=thiz.state.goodsGroupIds;
    var brandIds=thiz.state.brandIds;
    var searchKeywords=thiz.state.searchKeywords;
    var columnGroupItemId=thiz.state.columnGroupItemId;
    var params={};
    console.log("---------------------------screening_columnGroupItemId--------------------------------------",columnGroupItemId);
    console.log("---------------------------screening_countryPavilionIds--------------------------------------",countryPavilionIds);
    console.log("---------------------------screening_goodsGroupIds--------------------------------------",goodsGroupIds);
    console.log("---------------------------screening_brandIds----------------------------------------",brandIds);
    console.log("--------------------------screening_searchKeywords-----------------------------------",searchKeywords);
    //判断从哪里点击进来的
    if(countryPavilionIds&&countryPavilionIds.length==0)
    {
      if(goodsGroupIds&&goodsGroupIds.length==0)
      {
        params.brandIds=brandIds;
      }
      if(brandIds&&brandIds.length==0)
      {
        params.goodsGroupIds=goodsGroupIds;
      }
    }
    if(goodsGroupIds&&goodsGroupIds.length==0)
    {
      if(brandIds&&brandIds.length==0)
      {
        params.countryPavilionIds=countryPavilionIds;
      }
      if(countryPavilionIds&&countryPavilionIds.length==0)
      {
        params.brandIds=brandIds;
      }
    }
    if(brandIds&&brandIds.length==0)
    {
      if(countryPavilionIds&&countryPavilionIds.length==0)
      {
        params.goodsGroupIds=goodsGroupIds;
      }
      if(goodsGroupIds&&goodsGroupIds.length==0)
      {
        params.countryPavilionIds=countryPavilionIds;
      }
    }
    params.searchKeywords=searchKeywords;
    params.columnGroupItemId=columnGroupItemId;
    thiz.log("------------------------params-----------------------",params);
    thiz.request("spu/getMultipleQueryParams",{
      method:"POST",
      data:params,
      success:function(ret){

          // 预初始化数据
          let finalData = ret.data;
          for(let key in finalData){
            for(let i =0;i<finalData[key].length;i++){
              finalData[key][i].filterType = key;
              finalData[key][i].isSelected = false;
            }
          }

          thiz.setState({
            filterData:finalData
          });

          thiz.log("--------firstLoadData---------",thiz.state.filterData);

      },
      error:function(err){

      }
    })
  };

  //获取通过筛选条件筛选得到的数据（点击确定筛选条件按钮）
  getScreendingData=()=>{
      var thiz=this;
      thiz.log("---------------------------getScreendingData_selectedGoodsGroups--------------------",thiz.state.selectedGoodsGroups);
      thiz.log("---------------------------getScreendingData_selectedBrands--------------------",thiz.state.selectedBrands);
      thiz.log("---------------------------getScreendingData_selectedCountries--------------------",thiz.state.selectedCountries);
      var orders=thiz.state.orders;
      
      thiz.log("---------------------------getScreendingData_orders----------------------------",orders);
      var paramsStr=thiz.state.params;
      // thiz.log("---------------------------selectedUsageGroups--------------------",thiz.state.selectedUsageGroups);

      var data={brandIds:[],countryIds:[],goodsGroupIds:[],page:{}};
     
      // data.usageGroupIds=thiz.state.selectedUsageGroups;
      var newGoodsGroupIds = thiz.state.selectedGoodsGroups;
      if(thiz.state.goodsGroupIds instanceof Array&&thiz.state.goodsGroupIds.length!=0){
      // for(var i=0;i<thiz.state.goodsGroupIds.length;i++)
      //   {
      //     var count=0;
      //     for(var ii=0;ii<newGoodsGroupIds.length;ii++)
      //     {
      //       if(thiz.state.goodsGroupIds[i]==newGoodsGroupIds[ii])
      //       {
      //         continue ;
      //       }else{
      //         count++;
      //       }

      //     }
      //     if(count>0)
      //     {
      //       newGoodsGroupIds.push(thiz.state.goodsGroupIds[i]);
      //     }
      //   }
        for(var i=0;i<thiz.state.goodsGroupIds.length;i++){
          if(newGoodsGroupIds.indexOf(thiz.state.goodsGroupIds[i])==-1){
            newGoodsGroupIds.push(thiz.state.goodsGroupIds[i]);
          }
        }
      }
      thiz.log("--------------newGoodsGroupIds----------------",newGoodsGroupIds);

      data.brandIds=thiz.state.selectedBrands;
      data.goodsGroupIds=newGoodsGroupIds;
      data.countryIds=thiz.state.selectedCountries;
      // 加上页面传过来的参数
      thiz.log("--------newGoodsGroupIds--------",thiz.state.pageData);
      data = Object.assign(data,thiz.state.pageData);
      var params=Object.assign(data,paramsStr);
      params.brandIds=thiz.state.selectedBrands;
      params.goodsGroupIds=thiz.state.selectedGoodsGroups;
      params.countryIds=thiz.state.selectedCountries;
      
      // 设置分页信息
      thiz.curPageNum = 0;
      params.page = {
        pageNumber:1,
        pageSize:BaseComponent.PAGE_SIZE,
      }
      params.page.orders=orders;
      thiz.log("------------------------------getScreendingData_params--------------------",params);
      thiz.log("--------getScreendingData_data--------",data);
      thiz.setState({visible:true});
      thiz.request("spu/getOnSalePage",{
        method:"POST",
        data:params,
        success:function(ret){

          // 构造数据
          var data1=[];
          if(!ret.data.spuPage.records || ret.data.spuPage.records.length==0){
              thiz.setState({
                data:[],
                clickBrand:false,
                clickCountry:false,
                clickGongneng:false,
                clickType:false,
                isvisible:false,
                visible:false,
                params:params,
                pageNumber:1,
              })
              return;
          }
          for(var i=0;i<ret.data.spuPage.records.length;i++){
                var data2={};
                var img=ret.data.spuPage.records[i].attachment.resourceFullAddress;//商品图片路径;
                var id=ret.data.spuPage.records[i].id;//商品ID
                var ins=ret.data.spuPage.records[i].name;//商品介绍
                var price=ret.data.spuPage.records[i].buyUnitPrice;//商品卖出的价格
                // var cutprice=ret.data.spuPage.records[i].marketPrice;//商品市场价
                // var logisticPrice=ret.data.spuPage.records[i].logisticPrice;//logisticPrice
                // var taxPrice=ret.data.spuPage.records[i].taxPrice;//taxPrice
                // var surplusStockNum=ret.data.spuPage.records[i].surplusStockNum;//surplusStockNum
                data2.img=img;
                data2.id=id;
                data2.ins=ins;
                data2.price=price;
                // data2.cutprice=cutprice;
                // data2.logisticPrice=logisticPrice;
                // data2.taxPrice=taxPrice;
                // data2.surplusStockNum=surplusStockNum;
                data1.push(data2);
          }

          // 关闭分类筛选
          thiz.setState({
            isvisible:false,
            clickType:false,
            clickBrand:false,
            clickCountry:false,
            clickGongneng:false,
            data:data1,
            hasNext:ret.data.spuPage.hasNext,
            visible:false,
            params:params,
            pageNumber:1,
          });
        },
        error:function(err){
          thiz.setState({
            isvisible:false,
            clickType:false,
            clickBrand:false,
            clickCountry:false,
            clickGongneng:false,
            visible:false
          });
        }
      });
  };

  //搜索框点击事件
  search=(event)=>{
    var thiz=this;
    var searchWords=event.nativeEvent.text;
    var params=thiz.state.params;
    thiz.setState({visible:true,data:[],searchKeywords:searchWords});
     //判断用户是否输入了搜索条件
      if(searchWords==""||searchWords==null)
      {
        thiz.setState({visible:false});
        return ;
      }
      this.emit('recent_records',{recent_records:searchWords});
      if(!params.searchKeywords)
      {
        params.searchKeywords=searchWords;
      }else
      {
        params.searchKeywords=searchWords;
      }
      params.page.pageNumber=1;

      thiz.setState({visible:true,});
      thiz.request("spu/getOnSalePage",{
        method:"POST",
        data:params,
        success:function(ret){
            var data1=[];
            thiz.screening();
          if(!ret.data.spuPage.records)
          {
            thiz.setState({visible:false,searchKeywords:searchWords});
            return ;
          }
          if(ret.data.spuPage.records.length==0){
            thiz.setState({visible:false,searchKeywords:searchWords});
              return ;
          }
          for(var i=0;i<ret.data.spuPage.records.length;i++)
          {
                var data2={};
                var img=ret.data.spuPage.records[i].attachment.resourceFullAddress;//商品图片路径;
                var id=ret.data.spuPage.records[i].id;//商品ID
                var ins=ret.data.spuPage.records[i].name;//商品介绍
                var price=ret.data.spuPage.records[i].buyUnitPrice;//商品卖出的价格
                // var cutprice=ret.data.spuPage.records[i].marketPrice;//商品市场价
                // var logisticPrice=ret.data.spuPage.records[i].logisticPrice;//logisticPrice
                // var taxPrice=ret.data.spuPage.records[i].taxPrice;//taxPrice
                // var surplusStockNum=ret.data.spuPage.records[i].surplusStockNum;//surplusStockNum
                data2.img=img;
                data2.id=id;
                data2.ins=ins;
                data2.price=price;
                // data2.cutprice=cutprice;
                // data2.logisticPrice=logisticPrice;
                // data2.taxPrice=taxPrice;
                // data2.surplusStockNum=surplusStockNum;
                data1.push(data2);
          }
          
          thiz.setState({
            data:data1,
            visible:false,
            searchKeywords:searchWords,
            hasNext:ret.data.spuPage.hasNext,
            params:params,
            pageNumber:1,
          })
        },
        error:function(err){
          thiz.setState({visible:false});
          if(err.code&&err.code==200){
            thiz.setState({netError:true});
          }   
        }
      })
  };

  //综合
  Comprehensive=()=>{
    var thiz=this;
    var state=thiz.state;
    var orders={};
    var filterData=thiz.state.filterData;
    for(var key in filterData)
    {
      for(var i=0;i<filterData[key].length;i++)
      {
        filterData[key][i].isSelected=false;
      }
    }
    state.orders=orders;
    state.PRICE=0;
    state.SALES_NUM=0;
    state.underline=true;
    state.filterData=filterData;
    state.params={page:{pageNumber:1,pageSize:BaseComponent.PAGE_SIZE}};
    state.selectedGoodsGroups=[];
    state.selectedCountries=[];
    state.selectedBrands=[];
    state.pageNumber=1;
    if(!state.params.countryIds)
    {
      state.params.countryIds=[];
      console.log("------------------state.params.countryIds1111111111111111-----------------------");
    }else{
      state.params.countryIds=[];
      console.log("------------------state.params.countryIds2222222222222222222-----------------------");
    }

    if(!state.params.brandIds)
    {
      state.params.brandIds=[];
      console.log("--------------------state.params.brandIds3333333333333333333------------------");
    }else{
      state.params.brandIds=[];
      console.log("--------------------state.params.brandIds44444444444444444444------------------");
    }

    thiz.setState(state);
    thiz.getScreenData(orders);
  };
   //销量筛选数据
  Sales_num=()=>{
    var thiz=this;
    thiz.setState({underline:false});
    var SALES_NUM=thiz.state.SALES_NUM;
    var orders={};
    var params=thiz.state.params;
    if(params.page)
    {
      if(!params.page.pageNumber)
      {
        params.page.pageNumber="";
      }
        params.page.pageNumber=1;
      
    }else{
      params.page={};
      if(!params.page.pageNumber)
      {
         params.page.pageNumber="";
      }
      params.page.pageNumber=1;
    }
    if(SALES_NUM==0)
    {
      //销量升序
      SALES_NUM++;
      // orders.SALES_NUM="ASC";
      if(thiz.state.PRICE==1)
      {
        orders.PRICE="ASC";
      }
      if(thiz.state.PRICE==2)
      {
        orders.PRICE="DESC";
      }
      orders.SALES_NUM="ASC";
      thiz.log("--------------------销量升序orders-----------------------",orders);
      thiz.setState({SALES_NUM:SALES_NUM,orders:orders,params:params,pageNumber:1});
      thiz.getScreenData(orders);//开始筛选数据
      return ;
    };
    if(SALES_NUM==1)
    {
      //销量降序
      SALES_NUM++;
      // orders.SALES_NUM="DESC";
      if(thiz.state.PRICE==1)
      {
        orders.PRICE="ASC";
      }
      if(thiz.state.PRICE==2)
      {
        orders.PRICE="DESC";
      }
      orders.SALES_NUM="DESC";
      thiz.log("----------------------------销量降序orders-----------------------",orders);
      thiz.setState({SALES_NUM:SALES_NUM,orders:orders,params:params,pageNumber:1});
      thiz.getScreenData(orders);//开始筛选数据
      return ;
    };
    if(SALES_NUM==2)
    {
      //销量升序
      SALES_NUM--;
      // orders.SALES_NUM="ASC";
      if(thiz.state.PRICE==1)
      {
        orders.PRICE="ASC";
      }
      if(thiz.state.PRICE==2)
      {
        orders.PRICE="DESC";
      }
      orders.SALES_NUM="ASC";
      thiz.log("----------------------------销量升序orders-----------------------",orders);
      thiz.setState({SALES_NUM:SALES_NUM,orders:orders,params:params,pageNumber:1});
      thiz.getScreenData(orders);//开始筛选数据
      return ;
    };
  };
  //价格筛选数据
  Price=()=>{
    var thiz=this;
    thiz.setState({underline:false});
    var PRICE=thiz.state.PRICE;
    var orders={};
    var params=thiz.state.params;
    if(params.page)
    {
      if(!params.page.pageNumber)
      {
        params.page.pageNumber="";
      }
        params.page.pageNumber=1;
      
    }else{
      params.page={};
      if(!params.page.pageNumber)
      {
         params.page.pageNumber="";
      }
      params.page.pageNumber=1;
    }

    if(PRICE==0)
    {
      //价格升序
      PRICE++;
      // orders.PRICE="ASC";
      if(thiz.state.SALES_NUM==1)
      {
        orders.SALES_NUM="ASC";
      }
      if(thiz.state.SALES_NUM==2)
      {
        orders.SALES_NUM="DESC";
      }
      orders.PRICE="ASC";
      thiz.log("---------------价格升序----------------",orders);
      thiz.setState({PRICE:PRICE,orders:orders,params:params,pageNumber:1});
      thiz.getScreenData(orders);//开始筛选数据
      return ;
    };
    if(PRICE==1)
    {
      //价格降序
      PRICE++;
      // orders.PRICE="DESC";
      if(thiz.state.SALES_NUM==1)
      {
        orders.SALES_NUM="ASC";
      }
      if(thiz.state.SALES_NUM==2)
      {
        orders.SALES_NUM="DESC";
      }
      orders.PRICE="DESC";
      thiz.log("---------------价格降序----------------",orders);
      thiz.setState({PRICE:PRICE,orders:orders,params:params,pageNumber:1});
      thiz.getScreenData(orders);//开始筛选数据
      return ;
    };
    if(PRICE==2)
    {
      //价格升序
      PRICE--;
      // orders.PRICE="ASC";
      if(thiz.state.SALES_NUM==1)
      {
        orders.SALES_NUM="ASC";
      }
      if(thiz.state.SALES_NUM==2)
      {
        orders.SALES_NUM="DESC";
      }
      orders.PRICE="ASC";
      thiz.log("---------------价格升序----------------",orders);
      thiz.setState({PRICE:PRICE,orders:orders,params:params,pageNumber:1});
      thiz.getScreenData(orders);//开始筛选数据
      return ;
    };
  };
  //获取销量价格综合筛选数据
  getScreenData=(orders)=>{
    var thiz=this;
    var countryPavilionIds=thiz.state.countryPavilionIds;//国家馆ID
    var goodsGroupIds=thiz.state.goodsGroupIds;//其余分类ID
    var brandIds=thiz.state.selectedBrands;
    var countryIds=thiz.state.selectedCountries;
    var params=thiz.state.params;
    var searchKeywords=thiz.state.searchKeywords;
    var columnGroupItemId=thiz.state.columnGroupItemId;
    var selectedGoodsGroups=thiz.state.selectedGoodsGroups;
    params.page.orders=orders;
    thiz.screening();
    thiz.log("---------------------first_columnGroupItemId---------------------",columnGroupItemId);
    thiz.log("---------------------first_params---------------------",params);
    thiz.log("--------------------getScreenData_brandIds----------------------",brandIds);
    thiz.log("--------------------getScreenData_countryIds----------------------",countryIds);
    thiz.log("---------------------getScreenData_thiz.state.selectedGoodsGroups------------",thiz.state.selectedGoodsGroups);

    if(!params.searchKeywords)
    {
      params.searchKeywords="";
    }
    params.searchKeywords=searchKeywords;
        
    if(!params.columnGroupItemId)
    {
      params.columnGroupItemId="";
    }
    params.columnGroupItemId=columnGroupItemId;
        
    if(!params.goodsGroupIds)
    {
      params.goodsGroupIds=[];
    }
    // params.goodsGroupIds=goodsGroupIds;
   // for(var i=0;i<thiz.state.goodsGroupIds.length;i++)
   //  {
   //    var count=0;
   //    for(var ii=0;ii<selectedGoodsGroups.length;ii++)
   //    {
   //      if(thiz.state.goodsGroupIds[i]==selectedGoodsGroups[ii])
   //      {
   //        continue ;
   //      }else{
   //        count++;
   //      }

   //    }
   //    if(count>0)
   //    {
   //      selectedGoodsGroups.push(thiz.state.goodsGroupIds[i]);
   //    }
   //  }
  //  if(selectedGoodsGroups&&goodsGroupIds){
  //  for(var ii=0;ii<selectedGoodsGroups.length;ii++)
  //   {
  //     var count=0;

  //     for(var i=0;i<goodsGroupIds.length;i++)
  //     {
  //       if(goodsGroupIds[i]==selectedGoodsGroups[ii])
  //       {
  //         continue ;
  //       }else{
  //         count++;
  //       }

  //     }
  //     if(count>0)
  //     {
  //       goodsGroupIds.push(selectedGoodsGroups[ii])
  //     }
  //   }
  // }
    // params.goodsGroupIds=selectedGoodsGroups;
    params.goodsGroupIds=goodsGroupIds;
        
    if(!params.countryPavilionIds)
    {
      params.countryPavilionIds=[];
    }
    params.countryPavilionIds=countryPavilionIds;
    if(thiz.state.selectedGoodsGroups&&params.goodsGroupIds){
    for(let m=0;m<thiz.state.selectedGoodsGroups.length;m++){
      let si = thiz.state.selectedGoodsGroups[m];
      let count = 0;
      for(let mm=0;mm<params.goodsGroupIds.length;mm++){
        if(si==params.goodsGroupIds[mm]){
          break;
        }else{
          count++
        }
      }
      if(count>0&&count==params.goodsGroupIds.length){
        
        params.goodsGroupIds.push(si);
      }
    }
  }
    thiz.log("-----------------second_params_goodGroups-------------------",params.goodsGroupIds)

    if(!params.countryIds)
    {
      params.countryIds=[];
    }
    
    params.countryIds=countryIds;
    
    if(!params.brandIds)
    {
      params.brandIds=[];
    }
    params.brandIds=brandIds;
    thiz.log("----------------------second_params----------------------",params);
    thiz.log("------------------------orders--------------------",orders);
    thiz.setState({visible:true})
    thiz.request("spu/getOnSalePage",{
      method:"POST",
      data:params,
      success:function(ret){
          var data=[];
          if(!ret.data.spuPage.records){
            thiz.setState({
              data:data,
              visible:false,
              params:params,
              hasNext:ret.data.spuPage.hasNext,
            });
            return;
          }
          //重构商品列表数据结构  
          for(var i=0;i<ret.data.spuPage.records.length;i++)
          {
                var data2={};
                var img=ret.data.spuPage.records[i].attachment.resourceFullAddress;//商品图片路径;
                var id=ret.data.spuPage.records[i].id;//商品ID
                var ins=ret.data.spuPage.records[i].name;//商品介绍
                var price=ret.data.spuPage.records[i].buyUnitPrice;//商品卖出的价格
                // var cutprice=ret.data.spuPage.records[i].marketPrice;//商品市场价
                // var logisticPrice=ret.data.spuPage.records[i].logisticPrice;//logisticPrice
                // var taxPrice=ret.data.spuPage.records[i].taxPrice;//taxPrice
                // var surplusStockNum=ret.data.spuPage.records[i].surplusStockNum;//surplusStockNum
                data2.img=img;
                data2.id=id;
                data2.ins=ins;
                data2.price=price;
                // data2.cutprice=cutprice;
                // data2.logisticPrice=logisticPrice;
                // data2.taxPrice=taxPrice;
                // data2.surplusStockNum=surplusStockNum;
                data.push(data2);
          }
          
          thiz.setState({
            data:data,
            visible:false,
            params:params,
            hasNext:ret.data.spuPage.hasNext,
          });
      },
      error:function(err){
          thiz.setState({visible:false});
          if(err.code&&err.code==200){
            thiz.setState({netError:true});
          }   
      }
    })
  };

  //下拉刷新
  onRefresh=()=>{
    var thiz=this;
    var state=thiz.state;
    state.data=[];
    state.pageNumber=1;
    state.isRefreshing=true;
    state.hasNext=true;
    thiz.setState(state);
    thiz.getData();
  };

   //下拉刷新，上拉加载数据
  getData=()=>{
    var thiz=this;
    var paramsStr=thiz.state.params;
    var countryPavilionIds=thiz.state.countryPavilionIds;
    var goodsGroupIds=thiz.state.goodsGroupIds;
    var selectedGoodsGroups=thiz.state.selectedGoodsGroups;
    var selectedBrands=thiz.state.selectedBrands;
    var selectedCountries=thiz.state.selectedCountries;
    var searchKeywords=thiz.state.searchKeywords;
    var pageNumber=thiz.state.pageNumber;
    var columnGroupItemId=thiz.state.columnGroupItemId;
    thiz.log("----------------------------goodsGroupIds---------------------------",goodsGroupIds);
    thiz.log("----------------------------selectedGoodsGroups---------------------------",selectedGoodsGroups);
    if(columnGroupItemId!=undefined)
    {
        thiz.log("----------columnGroupItemId--------------",columnGroupItemId);
    }
  
    var hasNext=thiz.state.hasNext;
   
    if(columnGroupItemId!="")
    {
      if(!paramsStr.columnGroupItemId)
      {
        paramsStr.columnGroupItemId="";
      }
      paramsStr.columnGroupItemId=columnGroupItemId;
    }
    
    if(!paramsStr.searchKeywords)
    {
      paramsStr.searchKeywords=searchKeywords;
    }
   
    // if(goodsGroupIds&&countryPavilionIds.length==0)
    // {
    //   paramsStr.goodsGroupIds=[];
    //   // paramsStr.goodsGroupIds=goodsGroupIds;
    //   console.log("------------------11111111111111111111111111111-------------------------");
    //   for(var i=0;i<thiz.state.goodsGroupIds.length;i++)
    //   {
    //     var count=0;
    //     for(var ii=0;ii<selectedGoodsGroups.length;ii++)
    //     {
    //       if(thiz.state.goodsGroupIds[i]==selectedGoodsGroups[ii])
    //       {      
    //         continue ;
    //       }else{   
    //         count++;
    //       }
    //     }
    //     if(count>0)
    //     {
    //       selectedGoodsGroups.push(thiz.state.goodsGroupIds[i]);
    //     }
    //   }
    //     paramsStr.goodsGroupIds=selectedGoodsGroups;
    // }
    if(goodsGroupIds&&countryPavilionIds.length==0)
    {
      paramsStr.goodsGroupIds=[];
      var newGoodsGroupIds=goodsGroupIds.concat();
      thiz.log("----------------------newGoodsGroupIds---------------------",newGoodsGroupIds);
      for(var i=0;i<selectedGoodsGroups.length;i++){
        if(newGoodsGroupIds.indexOf(selectedGoodsGroups[i])==-1){
          newGoodsGroupIds.push(selectedGoodsGroups[i]);
        }
      }
      paramsStr.goodsGroupIds=newGoodsGroupIds;
    }

  
    
    if(goodsGroupIds&&goodsGroupIds.length==0)
    {
      paramsStr.countryPavilionIds=[];
      // paramsStr.countryPavilionIds=countryPavilionIds;
      for(var i=0;i<countryPavilionIds.length;i++)
      {
          var data=countryPavilionIds[i];
          paramsStr.countryPavilionIds.push(data);
      }
    }
    if(!paramsStr.goodsGroupIds)
    {
      paramsStr.goodsGroupIds=[];
    }
    
    if(selectedGoodsGroups){
      for(let m=0;m<selectedGoodsGroups.length;m++){
        let si = selectedGoodsGroups[m];
        let count = 0;
        for(let mm=0;mm<paramsStr.goodsGroupIds.length;mm++){
          if(si==paramsStr.goodsGroupIds[mm]){
            break;
          }else{
            count++
          }
        }
        if(count>0&&count==paramsStr.goodsGroupIds.length){
          paramsStr.push(si);
        }
      }
    }
    console.log("------------------222222222222222222222222222222222222221-------------------------");

    paramsStr.countryIds=selectedCountries;
    paramsStr.brandIds=selectedBrands;

    //判断是下拉刷新还是上拉加载
    if(thiz.state.isRefreshing)
    {
      var data=[];
    }
    else
    {
      var data=thiz.state.data;
    }
    paramsStr.page.pageNumber=pageNumber;
    thiz.log("----------------------------paramsStr-----------------------------",paramsStr);
    thiz.log("----------------------------countryPavilionIds---------------------------",countryPavilionIds);
    thiz.log("----------------------------goodsGroupIds---------------------------",goodsGroupIds);
    console.log("----------------------------pageNumber---------------------------",pageNumber);
    console.log("----------------------------hasNext---------------------------",hasNext);
      if(hasNext){
      thiz.setState({visible:true});
      thiz.request("spu/getOnSalePage",{
        method:"POST",
        data:paramsStr,
        success:function(ret){
          if(!ret.data.spuPage.records)
          {
            thiz.setState({
              isRefreshing:false,
              visible:false,
              data:data,
              params:paramsStr,
              hasNext:ret.data.spuPage.hasNext
            });
            return;
          }
          //重构商品列表数据结构  
          for(var i=0;i<ret.data.spuPage.records.length;i++)
          {
                var data2={};
                var img=ret.data.spuPage.records[i].attachment.resourceFullAddress;//商品图片路径;
                var id=ret.data.spuPage.records[i].id;//商品ID
                var ins=ret.data.spuPage.records[i].name;//商品介绍
                var price=ret.data.spuPage.records[i].buyUnitPrice;//商品卖出的价格
                // var cutprice=ret.data.spuPage.records[i].marketPrice;//商品市场价
                // var logisticPrice=ret.data.spuPage.records[i].logisticPrice;//logisticPrice
                // var taxPrice=ret.data.spuPage.records[i].taxPrice;//taxPrice
                // var surplusStockNum=ret.data.spuPage.records[i].surplusStockNum;//surplusStockNum
                data2.img=img;
                data2.id=id;
                data2.ins=ins;
                data2.price=price;
                // data2.cutprice=cutprice;
                // data2.logisticPrice=logisticPrice;
                // data2.taxPrice=taxPrice;
                // data2.surplusStockNum=surplusStockNum;
                data.push(data2);
          }
            thiz.setState({
              isRefreshing:false,
              visible:false,
              data:data,
              params:paramsStr,
              hasNext:ret.data.spuPage.hasNext
            });
        },
        error:function(err){
             thiz.setState({visible:false,isRefreshing:false});
            if(err.code&&err.code==200){
              thiz.setState({netError:true});
            }   
        }
      });
    }
    else
    {
      thiz.setState({isRefreshing:false})
    }
  };
  //渲染界面
  render(){
    let thiz = this;
    return (
      <View style={style.wrapper}>
        {/*状态栏，沉浸式*/}
        <StatusBar translucent={true} backgroundColor={this.state.visible?"rgba(14,14,14,0.5)":"rgb(255,255,255)"} barStyle={'dark-content'}/>

        {/*状态栏占位*/}
        <View style={{backgroundColor:this.state.visible?"rgba(14,14,14,0.5)":"rgb(255,255,255)",
                      height:thiz.isIphoneX(30,BaseComponent.SH),
                      position:"relative",
                      zIndex:1,
                      opacity:1,}}></View>
        {/*顶部导航栏*/}
        <View style={style.navBar}>
          <View style={{flex:1,flexDirection:"row",justifyContent:"flex-start"}}>
            <TouchableWithoutFeedback onPress={()=>{this.goBack();this.emit("recent_records")}}>
            <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:'center'}}>
               <Image source={Imgs.back} style={{width:(BaseComponent.W*0.127*0.4),height:(BaseComponent.W*0.127*0.4),marginLeft:BaseComponent.W*0.02}}></Image>
            </View>
            </TouchableWithoutFeedback>

           
            <View style={{height:ti.select({ios:(BaseComponent.H*0.05),android:(BaseComponent.H*0.054)}),flexGrow:1,backgroundColor:"transparent",
              marginTop:ti.select({ios:(BaseComponent.H*0.02)/2,android:(BaseComponent.H*0.016)/2}),borderRadius:ti.select({ios:(24),android:(BaseComponent.H*0.054)/2}),borderColor:"#C7C7C7",
              borderWidth:1,alignItems:"center",justifyContent:"center"}}>
              <TextInput style={{color:"#121212",width:'100%',marginLeft:BaseComponent.W*30/375,padding:0,height:'100%'}} placeholderTextColor="#C7C7C7"
               placeholder="请输入商品名,品牌或国家" underlineColorAndroid='transparent' maxLength={30} onChangeText={(event)=>thiz.setState({searchKeywords:event})} onSubmitEditing={(event)=>thiz.search(event)} defaultValue={thiz.state.searchKeywords}></TextInput>
            </View>
          
            <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>   
            </View>
          </View>
        </View>

      {/*筛选条件*/}
      <View style={styles.choose}>
          <TouchableWithoutFeedback onPress={()=>{
            thiz.Comprehensive();
          }}>
          <View style={{width:BaseComponent.W*40/375,height:BaseComponent.W*38/375,justifyContent:'center',alignItems:'center'}}>
              <Text style={{fontSize:BaseComponent.W*15/375,color:'rgba(39, 39, 39, 1)'}}>综合</Text>
              <View style={{width:'90%',height:1,backgroundColor:'black',display:this.state.underline?'flex':'none'}}></View>
          </View>
          </TouchableWithoutFeedback>
          
          <TouchableWithoutFeedback onPress={()=>{
            thiz.Sales_num();
          }}>
          <View style={{width:BaseComponent.W*45/375,height:BaseComponent.W*38/375,justifyContent:'center'}}>
              <Text style={{fontSize:BaseComponent.W*15/375,color:'rgba(39, 39, 39, 1)'}}>销量</Text>
              <View style={{width:'90%',height:1,backgroundColor:'black',display:this.state.SALES_NUM==0?'none':'flex'}}></View>
              <Image style={{width:BaseComponent.W*10/375,height:BaseComponent.W*6/375,
                      position:'absolute',right:0,top:BaseComponent.W*12/375}} 
                      source={this.state.SALES_NUM==1?require('../../image/home/xuanzhongshangsanjiao.png'):require('../../image/home/weixuanzhongshangsanjiao.png')}></Image>
              <Image style={{width:BaseComponent.W*10/375,height:BaseComponent.W*6/375,
                      position:'absolute',right:0,top:BaseComponent.W*20/375}} 
                      source={this.state.SALES_NUM==2?require('../../image/home/xuanzhongxiasanjiao.png'):require('../../image/home/weixuanzhongxiasanjiao.png')}></Image>
          </View>
          </TouchableWithoutFeedback>

          <TouchableWithoutFeedback onPress={()=>{
              thiz.Price();
          }}>
          <View style={{width:BaseComponent.W*45/375,height:BaseComponent.W*38/375,justifyContent:'center'}}>
              <Text style={{fontSize:BaseComponent.W*15/375,color:'rgba(39, 39, 39, 1)'}}>价格</Text>
              <Image style={{width:BaseComponent.W*10/375,height:BaseComponent.W*6/375,
                      position:'absolute',right:0,top:BaseComponent.W*12/375}} 
                      source={this.state.PRICE==1?require('../../image/home/xuanzhongshangsanjiao.png'):require('../../image/home/weixuanzhongshangsanjiao.png')}></Image>
              <Image style={{width:BaseComponent.W*10/375,height:BaseComponent.W*6/375,
                      position:'absolute',right:0,top:BaseComponent.W*20/375}} 
                      source={this.state.PRICE==2?require('../../image/home/xuanzhongxiasanjiao.png'):require('../../image/home/weixuanzhongxiasanjiao.png')}></Image>
              <View style={{width:'90%',height:1,backgroundColor:'black',display:this.state.PRICE==0?'none':'flex'}}></View>
          </View>
          </TouchableWithoutFeedback>
        
          <TouchableWithoutFeedback onPress={()=>this.setState({shitu:!this.state.shitu})}>
          <View style={{width:BaseComponent.W*40/375,height:BaseComponent.W*38/375,justifyContent:'center',alignItems:'center'}}>
          <Image style={{width:BaseComponent.W*18/375,height:BaseComponent.W*19/375}} source={this.state.shitu?require('../../image/home/gengduo.png'):require('../../image/home/sigongge.png')}></Image> 
          </View>
          </TouchableWithoutFeedback>

      </View>

      {/*转菊花*/}
      <Modal
        visible={this.state.visible}
        onRequestClose={()=>{this.setState({visible:false})}}
        transparent={true}
        animationType={"fade"}>
        <View style={{backgroundColor:"rgba(14,14,14,0.5)",width:"100%",width:"100%",flex:1,flexDirection:"column",alignItems:"center",justifyContent:"center",position:"relative"}}>
          <View>
                <ActivityIndicator size="large" color='white'/>
          </View>
        </View>
      </Modal>
      
      {/*筛选条件弹出框*/}
      <Modal visible={this.state.isvisible}
             transparent={true}
             onRequestClose={() => {this.setState({isvisible:false,clickType:false,clickBrand:false,clickCountry:false,clickGongneng:false})}}>
          <TouchableWithoutFeedback onPress={()=>{
                    this.setState({
                      isvisible:false,
                      clickType:false,
                      clickBrand:false,
                      clickCountry:false,
                      clickGongneng:false,
                    })
          }}>
          <View style={{flex:1,backgroundColor:'rgba(255,255,255,0)',marginTop:thiz.isIphoneX(30,ti.select({ios:20,android:0}))}}>
              
              {/*顶部导航栏*/}
              <View style={[style.navBar,{backgroundColor:"white"}]}>
                <View style={{flex:1,flexDirection:"row",justifyContent:"flex-start"}}>
                  <TouchableWithoutFeedback onPress={()=>{
                        this.goBack();
                        thiz.setState({isvisible:false});
                  }}>
                    <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:'center'}}>
                       <Image source={Imgs.back} style={{width:(BaseComponent.W*0.127*0.4),height:(BaseComponent.W*0.127*0.4),marginLeft:BaseComponent.W*0.02}}></Image>
                    </View>
                  </TouchableWithoutFeedback>

                 
                  <View style={{height:ti.select({ios:(BaseComponent.H*0.05),android:(BaseComponent.H*0.054)}),flexGrow:1,backgroundColor:"transparent",
                        marginTop:ti.select({ios:(BaseComponent.H*0.02)/2,android:(BaseComponent.H*0.016)/2}),borderRadius:ti.select({ios:(24),android:(BaseComponent.H*0.054)/2}),borderColor:"#C7C7C7",
                        borderWidth:1,alignItems:"center",justifyContent:"center"}}>
                        <TextInput style={{color:"#121212",width:'100%',marginLeft:BaseComponent.W*30/375,padding:0,height:ti.select({ios:16,android:20})}} placeholderTextColor="#C7C7C7"
                         placeholder="请输入商品名,品牌或国家" underlineColorAndroid='transparent' maxLength={30} onChangeText={(event)=>thiz.setState({searchKeywords:event})} onSubmitEditing={(event)=>thiz.search(event)} defaultValue={thiz.state.searchKeywords}></TextInput>
                  </View>
                
                  <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>   
                  </View>
                </View>
              </View>
              {/*筛选条件*/}
                <View style={styles.choose}>
                    <TouchableWithoutFeedback onPress={()=>{
                      thiz.Comprehensive();
                    }}>
                    <View style={{width:BaseComponent.W*40/375,height:BaseComponent.W*38/375,justifyContent:'center',alignItems:'center'}}>
                        <Text style={{fontSize:BaseComponent.W*15/375,color:'rgba(39, 39, 39, 1)'}}>综合</Text>
                        <View style={{width:'90%',height:1,backgroundColor:'black',display:this.state.underline?'flex':'none'}}></View>
                    </View>
                    </TouchableWithoutFeedback>
                    
                    <TouchableWithoutFeedback onPress={()=>{
                      thiz.Sales_num();
                    }}>
                    <View style={{width:BaseComponent.W*45/375,height:BaseComponent.W*38/375,justifyContent:'center'}}>
                        <Text style={{fontSize:BaseComponent.W*15/375,color:'rgba(39, 39, 39, 1)'}}>销量</Text>
                        <View style={{width:'90%',height:1,backgroundColor:'black',display:this.state.SALES_NUM==0?'none':'flex'}}></View>
                        <Image style={{width:BaseComponent.W*10/375,height:BaseComponent.W*6/375,
                                position:'absolute',right:0,top:BaseComponent.W*12/375}} 
                                source={this.state.SALES_NUM==1?require('../../image/home/xuanzhongshangsanjiao.png'):require('../../image/home/weixuanzhongshangsanjiao.png')}></Image>
                        <Image style={{width:BaseComponent.W*10/375,height:BaseComponent.W*6/375,
                                position:'absolute',right:0,top:BaseComponent.W*20/375}} 
                                source={this.state.SALES_NUM==2?require('../../image/home/xuanzhongxiasanjiao.png'):require('../../image/home/weixuanzhongxiasanjiao.png')}></Image>
                    </View>
                    </TouchableWithoutFeedback>

                    <TouchableWithoutFeedback onPress={()=>{
                        thiz.Price();
                    }}>
                    <View style={{width:BaseComponent.W*45/375,height:BaseComponent.W*38/375,justifyContent:'center'}}>
                        <Text style={{fontSize:BaseComponent.W*15/375,color:'rgba(39, 39, 39, 1)'}}>价格</Text>
                        <Image style={{width:BaseComponent.W*10/375,height:BaseComponent.W*6/375,
                                position:'absolute',right:0,top:BaseComponent.W*12/375}} 
                                source={this.state.PRICE==1?require('../../image/home/xuanzhongshangsanjiao.png'):require('../../image/home/weixuanzhongshangsanjiao.png')}></Image>
                        <Image style={{width:BaseComponent.W*10/375,height:BaseComponent.W*6/375,
                                position:'absolute',right:0,top:BaseComponent.W*20/375}} 
                                source={this.state.PRICE==2?require('../../image/home/xuanzhongxiasanjiao.png'):require('../../image/home/weixuanzhongxiasanjiao.png')}></Image>
                        <View style={{width:'90%',height:1,backgroundColor:'black',display:this.state.PRICE==0?'none':'flex'}}></View>
                    </View>
                    </TouchableWithoutFeedback>
                  
                    <TouchableWithoutFeedback onPress={()=>this.setState({shitu:!this.state.shitu})}>
                    <View style={{width:BaseComponent.W*40/375,height:BaseComponent.W*38/375,justifyContent:'center',alignItems:'center'}}>
                    <Image style={{width:BaseComponent.W*18/375,height:BaseComponent.W*19/375}} source={this.state.shitu?require('../../image/home/gengduo.png'):require('../../image/home/sigongge.png')}></Image> 
                    </View>
                    </TouchableWithoutFeedback>

                </View>

               {/*筛选条件1*/}
              <View style={{backgroundColor:"white",width:'100%',height:BaseComponent.W*50/375,flexDirection:'row',borderTopWidth:0.5,borderTopColor:'#EFEFEF',justifyContent:'space-around'}}>
                 <View style={{width:'150%',height:0.5,position:'absolute',bottom:0,backgroundColor:'#EFEFEF',marginLeft:-BaseComponent.W*50/375}}></View>

                 <TouchableWithoutFeedback onPress={()=>{
                          if(this.state.clickType)
                          {
                            this.setState({
                              clickType:false,
                              isvisible:false,
                            })
                          }
                          else{
                            this.setState({
                              clickType:true,
                              clickBrand:false,
                              clickCountry:false,
                              clickGongneng:false,
                              flatlistData:this.state.filterData.goodsGroups,
                            })
                          }  
                  }}>
                  <View style={{width:BaseComponent.W*78/375,height:BaseComponent.W*40/375,marginTop:BaseComponent.W*10/375,zIndex:1000,
                              borderWidth:1,borderColor:this.state.clickType?'#EFEFEF':'transparent',borderBottomColor:this.state.clickType?'white':'transparent'}}>
                        <View style={{width:BaseComponent.W*78/375,height:BaseComponent.W*29/375,backgroundColor:this.state.clickType?'transparent':"#F5F5F5",borderRadius:2,
                                  justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
                              <Text style={{fontSize:BaseComponent.W*14/375,color:'#888888'}}>类型</Text>
                              <Image style={{width:BaseComponent.W*10/375,height:BaseComponent.W*6/375,marginLeft:BaseComponent.W*11/375}} source={this.state.clickType?require('../../image/home/weixuanzhongshangsanjiao.png'):require('../../image/home/weixuanzhongxiasanjiao.png')}></Image>
                        </View>      
                  </View>
                  </TouchableWithoutFeedback>
                  
                  <TouchableWithoutFeedback onPress={()=>{
                         
                          if(this.state.clickBrand)
                          {
                            this.setState({
                              clickBrand:false,
                              isvisible:false,
                            })
                          }
                          else{
                            this.setState({
                              clickType:false,
                              clickBrand:true,
                              clickCountry:false,
                              clickGongneng:false,
                              flatlistData:this.state.filterData.brands,
                            })
                          }
                  }}>
                  <View style={{width:BaseComponent.W*78/375,height:BaseComponent.W*40/375,marginLeft:BaseComponent.W*10/375,marginTop:BaseComponent.W*10/375,zIndex:1000,
                              borderWidth:1,borderColor:this.state.clickBrand?'#EFEFEF':'transparent',borderBottomColor:this.state.clickBrand?'#fff':'transparent'}}>
                        <View style={{width:BaseComponent.W*78/375,height:BaseComponent.W*29/375,backgroundColor:this.state.clickBrand?'transparent':"#F5F5F5",borderRadius:2,
                                  justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
                              <Text style={{fontSize:BaseComponent.W*14/375,color:'#888888'}}>品牌</Text>
                              <Image style={{width:BaseComponent.W*10/375,height:BaseComponent.W*6/375,marginLeft:BaseComponent.W*11/375}} source={this.state.clickBrand?require('../../image/home/weixuanzhongshangsanjiao.png'):require('../../image/home/weixuanzhongxiasanjiao.png')}></Image>
                        </View>      
                  </View>
                  </TouchableWithoutFeedback>

                  <TouchableWithoutFeedback onPress={()=>{
                         
                          if(this.state.clickCountry)
                          {
                            this.setState({
                              clickCountry:false,
                              isvisible:false,
                            })
                          }
                          else{
                            this.setState({
                              clickType:false,
                              clickBrand:false,
                              clickCountry:true,
                              clickGongneng:false,
                              flatlistData:this.state.filterData.countries,
                            })
                          }
                  }}>  
                  <View style={{width:BaseComponent.W*78/375,height:BaseComponent.W*40/375,marginLeft:BaseComponent.W*10/375,marginTop:BaseComponent.W*10/375,zIndex:1000,
                              borderWidth:1,borderColor:this.state.clickCountry?'#EFEFEF':'transparent',borderBottomColor:this.state.clickCountry?'#fff':'transparent'}}>
                        <View style={{width:BaseComponent.W*78/375,height:BaseComponent.W*29/375,backgroundColor:this.state.clickCountry?'transparent':"#F5F5F5",borderRadius:2,
                                  justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
                              <Text style={{fontSize:BaseComponent.W*14/375,color:'#888888'}}>国家</Text>
                              <Image style={{width:BaseComponent.W*10/375,height:BaseComponent.W*6/375,marginLeft:BaseComponent.W*11/375}} source={this.state.clickCountry?require('../../image/home/weixuanzhongshangsanjiao.png'):require('../../image/home/weixuanzhongxiasanjiao.png')}></Image>
                        </View>      
                  </View>
                  </TouchableWithoutFeedback>

                  <TouchableWithoutFeedback onPress={()=>{
                         
                          if(this.state.clickGongneng)
                          {
                            this.setState({
                              clickGongneng:false,
                              isvisible:false,
                            })
                          }
                          else{
                            this.setState({
                              clickType:false,
                              clickBrand:false,
                              clickCountry:false,
                              clickGongneng:true,
                              flatlistData:this.state.filterData.usageGroups,
                            })
                          }
                  }}>
                  <View style={{display:'none',width:BaseComponent.W*78/375,height:BaseComponent.W*40/375,marginLeft:BaseComponent.W*10/375,marginTop:BaseComponent.W*10/375,
                              borderWidth:1,borderColor:this.state.clickGongneng?'#EFEFEF':'transparent',borderBottomColor:this.state.clickGongneng?'#fff':'transparent'}}>
                        <View style={{width:BaseComponent.W*78/375,height:BaseComponent.W*29/375,backgroundColor:this.state.clickGongneng?'transparent':"#F5F5F5",borderRadius:2,
                                  justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
                              <Text style={{fontSize:BaseComponent.W*14/375,color:'#888888'}}>功能</Text>
                              <Image style={{width:BaseComponent.W*10/375,height:BaseComponent.W*6/375,marginLeft:BaseComponent.W*11/375}} source={this.state.clickGongneng?require('../../image/home/weixuanzhongshangsanjiao.png'):require('../../image/home/weixuanzhongxiasanjiao.png')}></Image>
                        </View>      
                  </View>
                  </TouchableWithoutFeedback>
              </View>

              {/*筛选条件列表*/}
              <View style={{width:'100%',height:BaseComponent.W*242/375,backgroundColor:"white"}}>
                  {/*详细内容*/}
                  <View style={{width:'100%',height:BaseComponent.W*197/375}}>
                      <FlatList
                          data={this.state.flatlistData}
                          keyExtractor={this._keyExtractor}
                          renderItem={this._renderItem1}
                          
                          showsVerticalScrollIndicator = {false}
                          extraData={this.state}
                          numColumns={'2'}
                      />
                  </View>

                  <View style={{width:'100%',height:BaseComponent.W*45/375,borderTopWidth:0.5,borderTopColor:'#E5E5E5',flexDirection:'row'}}>
                      <TouchableWithoutFeedback onPress={()=>{
                        thiz.reset();
                      }}> 
                      <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                          <Text style={{fontSize:BaseComponent.W*16/375,color:'#2A2A2A'}}>重置</Text>
                      </View>
                      </TouchableWithoutFeedback>

                      <TouchableWithoutFeedback onPress={()=>{
                            this.getScreendingData();
                      }}>  
                      <View style={{flex:1,justifyContent:'center',alignItems:'center',backgroundColor:'#FDD17A'}}>
                          <Text style={{fontSize:BaseComponent.W*16/375,color:'#2A2A2A'}}>确定</Text>
                      </View>
                      </TouchableWithoutFeedback>
                  </View>
              </View>

              <View style={{flex:1,backgroundColor:'rgba(14,14,14,0.5)'}}></View>

          </View>
          </TouchableWithoutFeedback>   
      </Modal>

      {/*筛选条件1*/}
      <View style={[styles.choose1,{borderBottomColor:this.state.isvisible?'transparent':'#EFEFEF'}]}>
          <TouchableWithoutFeedback onPress={()=>{
                this.setState({
                      isvisible:true,
                      clickType:true,
                      flatlistData:this.state.filterData.goodsGroups
                    });
              }}>
          <View style={{width:BaseComponent.W*78/375,height:BaseComponent.W*40/375,marginTop:BaseComponent.W*10/375,
                      borderWidth:1,borderColor:'transparent',borderBottomColor:'transparent',display:this.state.isvisible?'none':'flex'}}>
                <View style={{width:BaseComponent.W*78/375,height:BaseComponent.W*29/375,backgroundColor:"#F5F5F5",borderRadius:2,
                          justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
                      <Text style={{fontSize:BaseComponent.W*14/375,color:'#888888'}}>类型</Text>
                      <Image style={{width:BaseComponent.W*10/375,height:BaseComponent.W*6/375,marginLeft:BaseComponent.W*11/375}} source={require('../../image/home/weixuanzhongxiasanjiao.png')}></Image>
                </View>      
          </View>
          </TouchableWithoutFeedback>

          <TouchableWithoutFeedback onPress={()=>{
              this.setState({
                    isvisible:true,
                    clickBrand:true,
                    flatlistData:this.state.filterData.brands
                  });
            }}>
          <View style={{width:BaseComponent.W*78/375,height:BaseComponent.W*40/375,marginLeft:BaseComponent.W*10/375,marginTop:BaseComponent.W*10/375,
                      borderWidth:1,borderColor:'transparent',borderBottomColor:'transparent',display:this.state.isvisible?'none':'flex'}}>
                <View style={{width:BaseComponent.W*78/375,height:BaseComponent.W*29/375,backgroundColor:"#F5F5F5",borderRadius:2,
                          justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
                      <Text style={{fontSize:BaseComponent.W*14/375,color:'#888888'}}>品牌</Text>
                      <Image style={{width:BaseComponent.W*10/375,height:BaseComponent.W*6/375,marginLeft:BaseComponent.W*11/375}} source={require('../../image/home/weixuanzhongxiasanjiao.png')}></Image>
                </View>      
          </View>
          </TouchableWithoutFeedback>

          <TouchableWithoutFeedback onPress={()=>{
                  this.setState({
                        isvisible:true,
                        clickCountry:true,
                        flatlistData:this.state.filterData.countries
                      })
                }}>
          <View style={{width:BaseComponent.W*78/375,height:BaseComponent.W*40/375,marginLeft:BaseComponent.W*10/375,marginTop:BaseComponent.W*10/375,
                      borderWidth:1,borderColor:'transparent',borderBottomColor:'transparent',display:this.state.isvisible?'none':'flex'}}>
                <View style={{width:BaseComponent.W*78/375,height:BaseComponent.W*29/375,backgroundColor:"#F5F5F5",borderRadius:2,
                          justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
                      <Text style={{fontSize:BaseComponent.W*14/375,color:'#888888'}}>国家</Text>
                      <Image style={{width:BaseComponent.W*10/375,height:BaseComponent.W*6/375,marginLeft:BaseComponent.W*11/375}} source={require('../../image/home/weixuanzhongxiasanjiao.png')}></Image>
                </View>      
          </View>
          </TouchableWithoutFeedback>

          <TouchableWithoutFeedback onPress={()=>{
                  this.setState({
                        isvisible:true,
                        clickGongneng:true,
                        flatlistData:this.state.filterData.usageGroups
                      })
                }}>  
          <View style={{width:BaseComponent.W*78/375,height:BaseComponent.W*40/375,marginLeft:BaseComponent.W*10/375,marginTop:BaseComponent.W*10/375,
                      borderWidth:1,borderColor:'transparent',borderBottomColor:'transparent',display:'none'}}>
                <View style={{width:BaseComponent.W*78/375,height:BaseComponent.W*29/375,backgroundColor:"#F5F5F5",borderRadius:2,
                          justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
                      <Text style={{fontSize:BaseComponent.W*14/375,color:'#888888'}}>功能</Text>
                      <Image style={{width:BaseComponent.W*10/375,height:BaseComponent.W*6/375,marginLeft:BaseComponent.W*11/375}} source={require('../../image/home/weixuanzhongxiasanjiao.png')}></Image>
                </View>      
          </View>
          </TouchableWithoutFeedback>
      </View>
      

      {/*商品列表thiz.state.shitu?'#F0F0F0':*/}
      
      {
              thiz.state.netError?(
                  <View style={{width:BaseComponent.W,height:BaseComponent.H*0.7,justifyContent:'center',alignItems:'center'}}>
                                
                    <Image style={{width:BaseComponent.W*180/375,height:BaseComponent.W*180/375,borderRadius:BaseComponent.W*90/375}} source={require('../../image/mine/neterror.png')}></Image>
                    

                    <TouchableWithoutFeedback onPress={()=>{
                      thiz.setState({netError:false,visible:true});
                      var orders=thiz.state.orders;
                      
                      var params=thiz.state.params;
                      if(!params.orders)
                      {
                        params.orders=orders;
                      }
                      thiz.request("spu/getOnSalePage",{
                        method:"POST",
                        data:params,
                        success:function(ret){
                          var data1=thiz.state.data;

                                if(!ret.data.spuPage.records)
                                {
                                  thiz.setState({visible:false})
                                  return ;
                                }
                                if(ret.data.spuPage.records.length==0){
                                    return ;
                                }
                                for(var i=0;i<ret.data.spuPage.records.length;i++)
                                {
                                      var data2={};
                                      var img=ret.data.spuPage.records[i].attachment.resourceFullAddress;//商品图片路径;
                                      var id=ret.data.spuPage.records[i].id;//商品ID
                                      var ins=ret.data.spuPage.records[i].name;//商品介绍
                                      var price=ret.data.spuPage.records[i].buyUnitPrice;//商品卖出的价格
                                      // var cutprice=ret.data.spuPage.records[i].marketPrice;//商品市场价
                                      // var logisticPrice=ret.data.spuPage.records[i].logisticPrice;//logisticPrice
                                      // var taxPrice=ret.data.spuPage.records[i].taxPrice;//taxPrice
                                      // var surplusStockNum=ret.data.spuPage.records[i].surplusStockNum;//surplusStockNum
                                      data2.img=img;
                                      data2.id=id;
                                      data2.ins=ins;
                                      data2.price=price;
                                      // data2.cutprice=cutprice;
                                      // data2.logisticPrice=logisticPrice;
                                      // data2.taxPrice=taxPrice;
                                      // data2.surplusStockNum=surplusStockNum;
                                      data1.push(data2);
                                }
                                thiz.setState({
                                  data:data1,
                                  visible:false,
                                  hasNext:ret.data.spuPage.hasNext,
                                });
                                thiz.log("-------------------------filterData--------------------------",thiz.state.filterData);
                                thiz.screening();
                        },
                        error:function(err){
                          thiz.setState({visible:false});
                          thiz.toast(err.msg);
                          if(err.code&&err.code==200){
                            thiz.setState({netError:true});
                          } 
                        }
                      })
                      
                    }}>
                    <View style={{width:BaseComponent.W*70/375,height:BaseComponent.W*30/375,borderRadius:BaseComponent.W*15/375,borderWidth:0.5,borderColor:'#080808',
                              justifyContent:'center',alignItems:'center',marginTop:BaseComponent.W*15/375}}>
                          <Text style={{fontSize:BaseComponent.W*15/375,color:'#252525'}}>刷新</Text>          
                    </View>
                    </TouchableWithoutFeedback>
                  </View>
              ):(thiz.state.data.length==0?(
              <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                <Image style={{width:BaseComponent.W*180/375,height:BaseComponent.W*180/375,borderRadius:BaseComponent.W*90/375}} source={require('../../image/mine/nogoods.png')}></Image>
              </View>):
              (
                <View style={{flex:1,backgroundColor:'#F0F0F0'}}>
                  <View style={{backgroundColor:"white"}}>
                  <FlatList
                    
                    style={{backgroundColor:"#F0F0F0",height:'100%'}}
                    data={this.state.data}
                    keyExtractor={this._keyExtractor}
                    renderItem={thiz.state.shitu?thiz.renderItem:thiz._renderItem}
                    showsVerticalScrollIndicator = {false}
                    numColumns={this.state.shitu?'1':'2'}
                    extraData={this.state}
                    key={this.state.shitu}
                    ListFooterComponent={thiz.state.hasNext?null:thiz.nomore({bgcolor:'#F0F0F0'})}
                    refreshing={this.state.isRefreshing}
                    onMomentumScrollEnd={(e)=>{
                        var offsetY = e.nativeEvent.contentOffset.y; //滑动距离
                        var contentSizeHeight = e.nativeEvent.contentSize.height; //scrollView contentSize高度
                        var oriageScrollHeight = e.nativeEvent.layoutMeasurement.height; //scrollView高度
                        console.log("---------------------滑动距离-------------------",offsetY);
                        console.log("---------------------scrollView contentSize高度-------------------",contentSizeHeight);
                        console.log("---------------------scrollView高度-------------------",oriageScrollHeight);
                        if (offsetY + oriageScrollHeight >= contentSizeHeight-5)
                        {
                             console.log("------------------------onEndReached_hasNext-------------------------",thiz.state.hasNext);
                              if(thiz.state.hasNext){
                              var pageNumber=thiz.state.pageNumber;
                              var state=thiz.state;
                              
                              pageNumber++;
                              state.pageNumber=pageNumber;
                              thiz.setState(state);
                              thiz.getData();
                            }
                            else
                            {
                              // thiz.toast("已经到底了，没有更多商品了");
                            }
                        }
                    }}
                    refreshControl={
                      <RefreshControl
                        refreshing={thiz.state.isRefreshing}
                        onRefresh={()=>{thiz.onRefresh()}}
                        colors={['#ff0000', '#00ff00','#0000ff','#3ad564']}
                        progressBackgroundColor="#ffffff"
                      />}
                  />
                  </View>
                
                </View>
              )
            )
      }


      </View>        
    )
  }
  _keyExtractor=(item,index)=>index.toString();
  /*两列商品展示*/
  _renderItem=(info)=>{
    let thiz = this;
    
    return (
      <View style={{backgroundColor:'white',width:info.index==thiz.state.data.length-1&&thiz.state.data.length%2==1?'100%':'50%'}}>
      <TouchableWithoutFeedback onPress={()=>this.goodsOnclick(info)}>

      <View style={styles.item}>

      <TouchableWithoutFeedback onPress={()=>this.goodsOnclick(info)}>  
      <View style={{width:BaseComponent.W*171/375,height:BaseComponent.W*172/375,borderWidth:0.5,borderColor:'rgba(240, 240, 240, 1)',justifyContent:'center',alignItems:'center',backgroundColor:'white'}}>
      <Image style={{width:BaseComponent.W*153/375,height:BaseComponent.W*153/375}} source={{uri:info.item.img}}></Image>
      </View>
      </TouchableWithoutFeedback>

      <View style={{width:BaseComponent.W*169/375,height:BaseComponent.W*31/375,marginTop:BaseComponent.W*0.026,backgroundColor:'#fff'}}>
      <Text style={{fontSize:BaseComponent.W*13/375,color:'rgba(58, 58, 58, 1)',lineHeight:BaseComponent.W*20/375}} numberOfLines={2}>{info.item.ins}</Text>
      </View>

      <View style={{width:BaseComponent.W*171/375,height:BaseComponent.W*25/375,marginTop:BaseComponent.W*0.026,
                  flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
          <Text style={{fontSize:BaseComponent.W*15/375,color:'#FF407E',fontWeight:"bold"}}>¥{info.item.price}&nbsp;&nbsp;&nbsp;&nbsp;   
          
            {/*<Text style={{textDecorationLine:'line-through',fontSize:BaseComponent.W*11/375,color:'rgba(153, 153, 153, 1)',
                display:info.item.price>=info.item.cutprice?'none':'flex'}}>¥{info.item.cutprice}</Text>*/}
          </Text>

          {/*<TouchableWithoutFeedback onPress={()=>{thiz.addCart(info)}}>
          <Image style={{width:BaseComponent.W*25/375,height:BaseComponent.W*25/375,marginRight:BaseComponent.W*0.013}} source={require('../../image/home/shoppingCart.png')}></Image>
          </TouchableWithoutFeedback>*/}

      </View>

      </View>
      </TouchableWithoutFeedback>
      </View>
    )
  };

  // 加入购物车
  addCart(value){
    let thiz = this;
    thiz.log("--------addCart--------",value);
    if(value){
      thiz.setState({
            visible:true
      });
      thiz.request("shoppingCar/addGoods",{
            method:"POST",
            data:{
                  num:1,
                  skuId:value.item.id
            },
            success:function(ret){
                  thiz.setState({
                        visible:false
                  });
                  if(ret.respCode=="00"){
                        thiz.log("--------加入购物车成功--------","加入购物车成功");
                        thiz.toast("加入购物车成功");
                        // 关闭商品属性选择
                        thiz.setState({
                              visible:false,
                        });

                        // 发送添加购物车成功消息
                        thiz.emit("add_cart_success");
                  }
            },
            error:function(err){
                  thiz.toast(err.msg);
                  thiz.setState({
                        visible:false,
                  });
            }
      });
    }
  }

  //一列商品展示
  renderItem=(info)=>{
      let thiz = this;
      return (
        <View style={{backgroundColor:'white'}}>
      <TouchableWithoutFeedback onPress={()=>this.goodsOnclick(info)}>

      <View style={{width:'100%',height:BaseComponent.W*125/375,marginTop:BaseComponent.W*14/375,flexDirection:'row',backgroundColor:'white'}}>
          
        <View style={{width:BaseComponent.W*120/375,height:BaseComponent.W*120/375,
                      borderColor:'rgba(240, 240, 240, 1)',borderWidth:0.5,marginLeft:BaseComponent.W*0.026,
                    justifyContent:'center',alignItems:'center'}}>
            <Image style={{width:BaseComponent.W*110/375,height:BaseComponent.W*110/375}} source={{uri:info.item.img}}></Image>
        </View>

        <View style={{width:BaseComponent.W*220/375,height:BaseComponent.W*120/375,marginLeft:BaseComponent.W*0.026,justifyContent:'space-between'}}>
            <View style={{width:BaseComponent.W*220/375,height:BaseComponent.W*60/375}}>
                <Text style={{color:'rgba(58, 58, 58, 1)',fontSize:BaseComponent.W*15/375,lineHeight:BaseComponent.W*20/375}} numberOfLines={2}>{info.item.ins}</Text>
                <Text style={{color:'rgba(58, 58, 58, 1)',fontSize:BaseComponent.W*12/375,display:'none'}}></Text>
            </View>
            <View style={{width:BaseComponent.W*220/375,height:BaseComponent.W*25/375,flexDirection:'row',
                         justifyContent:'space-between'}}>
                <Text style={{color:'#FF407E',fontSize:BaseComponent.W*15/375,fontWeight:"bold"}}>¥{info.item.price}&nbsp;&nbsp;&nbsp;&nbsp;
                
                  {/*<Text style={{color:'rgba(153, 153, 153, 1)',fontSize:BaseComponent.W*11/375,textDecorationLine:'line-through',
                              display:info.item.price>=info.item.cutprice?'none':'flex'}}>¥{info.item.cutprice}</Text>*/}

                </Text>

                {/*<TouchableWithoutFeedback onPress={()=>{thiz.addCart(info)}}> 
                <Image style={{width:BaseComponent.W*25/375,height:BaseComponent.W*25/375,marginRight:BaseComponent.W*0.013}} source={require('../../image/home/shoppingCart.png')}></Image>
                </TouchableWithoutFeedback>*/}
            </View>
        </View>
      </View>
      </TouchableWithoutFeedback>
      </View>
    )
  };
  //商品点击事件
  goodsOnclick=(info)=>{
      var thiz=this;
      var id=info.item.id.toString();
      thiz.navigate('GoodsXiangQing',{id:id});
  }
};
const styles=StyleSheet.create({
  choose:{
    width:'100%',
    height:BaseComponent.W*38/375,
    flexDirection:'row',
    justifyContent:'space-around',
    alignItems:'center',
    backgroundColor:"white",
  },
  choose1:{
    width:'100%',
    height:BaseComponent.W*50/375,
    borderBottomWidth:0.5,
    borderTopWidth:0.5,
    borderTopColor:'rgba(241,241,241,1)',
    flexDirection:'row',
    justifyContent:'space-around',
    backgroundColor:"white",
  },
  item:{
    width:BaseComponent.W*171/375,
 
    marginLeft:BaseComponent.W*10/375,
    marginTop:BaseComponent.W*16/375,
    backgroundColor:'white'
  }
});