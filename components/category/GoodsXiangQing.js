/**
 * @name Mine.js
 * @auhor 程浩
 * @date 2018.8.16
 * @desc 商品详情界面
 */
import React,{Component}from "react";
import {Slider,WebView,Text, View, Image, StyleSheet,ImageBackground,TouchableWithoutFeedback,Modal,ScrollView,StatusBar,ActivityIndicator,CameraRoll} from 'react-native';
import BaseComponent from "../BaseComponent";
import Swiper from 'react-native-swiper';
import ImageViewer from 'react-native-image-zoom-viewer';

import Video from 'react-native-video';
// import Orientation from 'react-native-orientation';


import MyImage from "../../lib/MyImage";
// 导入工具函数库
import T from "../../lib/Tools";
// Tools实例
let ti = T.getInstance();
const RNFS = require('react-native-fs'); //文件处理

// 转菊花
import Spinner from 'react-native-loading-spinner-overlay';


let BannerData=[require('../../image/home/good.png'),require('../../image/home/gsShop1.png'),require('../../image/home/gsShop3.png')
			   ,require('../../image/home/good.png'),require('../../image/home/gsShop2.png')]


export default class GoodsXiangQing extends BaseComponent{
	constructor(props){
		super(props);
		this.state={
			SwiperIndex:0,//当前轮播图的index
			SwiperTotal:0,//轮播图总数
			isvisible:false,//属性弹窗 false表示不可见,
                  goodsDetail:{},
                  specs:{},//规格
                  selectedSpecs:{},
                  newData:[],//用来存放放大后的轮播图
                  services:{},// 服务
                  SavePicVisible:false,//点击图片放大是否可见
                  beforeSpecs:{},
                  beforeSeleced:{},
                  clickSkuId:null,
                  imageUrl:[],
                  visible:false,// 转菊花是否显示
                  buyNum:1,//购买数量
                  saveIndex:0,
                  isAddCart:true,// 是否添加到购物车
                  videoViseble:false,
                  paused:false,//控制视频播放还是暂停,false表示播放
                  smallScreenPaused:true,//控制小屏播放
                  totalTime:"",//视频总时长
                  currentTime:"",//当前时长
                  totalSecond:0,//视频总共多少秒
                  currentSecond:0,//当前描秒数
                  bannerVideo:false,//控制banner轮播图视频的播放
		};

            this.myState = {
                  SwiperIndex:0,//当前轮播图的index
                  SwiperTotal:0,//轮播图总数
                  isvisible:false,//属性弹窗 false表示不可见,
                  goodsDetail:{},
                  specs:{},//规格
                  selectedSpecs:{},
                  saveIndex:0,
                  services:{},// 服务

                  beforeSpecs:{},
                  beforeSeleced:{},
                  clickSkuId:null,

                  visible:false,// 转菊花是否显示
                  buyNum:1,//购买数量

                  isAddCart:true,// 是否添加到购物车
            };

            this.isRendering=false;// 是否正在渲染
            this.isSpecShow = false;// 属性弹窗是否已显示
            this.isFirstIn = true;// 是否第一次进入本页面
            this.ismember = this.params.from&&this.params.from==="Member"?true:false;
	}

      // 数据更新渲染完毕
      componentDidUpdate(){
            let thiz = this;
            // thiz.log("--------componentDidUpdate--------","componentDidUpdate "+thiz.renderingData);
            // thiz.renderingData = false;
            // if(thiz.state.visible&&!thiz.renderingData){
            //       thiz.renderingData = false;
            // }
      }

      //视频播放进度
      onProgress=(time)=>{
            // this.log("------------------------startIndex--------------------------",time);
            // console.log("---------------------------time-------------------------",parseInt(time.currentTime));
            var hour=parseInt(parseInt(time.currentTime)/3600);
            var minute=parseInt((parseInt(time.currentTime)-hour*3600)/60);
            var second=parseInt(time.currentTime)-hour*3600-minute*60;
            if(hour<10){
                  hour="0"+hour;
            }
            if(minute<10){
                  minute="0"+minute;
            }
            if(second<10){
                  second="0"+second;
            }
            var currentTime=hour+":"+minute+":"+second;
            // console.log("----------------------currentTime---------------------",currentTime);
            this.setState({currentTime:currentTime,currentSecond:parseInt(time.currentTime)});
      }
      //视频加载时
      onLoad=(allTime)=>{
            this.log("-------------------------allTime----------------------------",allTime);
            var time=parseInt(allTime.duration);
            var hour=parseInt(time/3600);
            var minute=parseInt((time-hour*3600)/60);
            var second=time-hour*3600-minute*60;
            if(hour<10){
                  hour="0"+hour;
            }
            if(minute<10){
                  minute="0"+minute;
            }
            if(second<10){
                  second="0"+second;
            }
            var totalTime=hour+":"+minute+":"+second;

            this.setState({totalTime:totalTime,totalSecond:time});
      }

      // 处理商品属性可以点击
      canSpecClick(){
            let thiz = this;
            // 判断是否已处理了规格数据
            let specs = thiz.myState.specs;
            let selectedSpecs = thiz.myState.selectedSpecs;
            thiz.log("--------specs_data--------",specs);
            thiz.log("--------selectedSpecs_data--------",selectedSpecs);
            if(specs.data&&selectedSpecs.data){
                  thiz.log("--------canSpecClick--------","canSpecClick");
                  if(selectedSpecs.data.length>0){

                        // 根据选中的属性来判断，其他属性哪些可以显示，根据拥有相同的skuId来判断
                        for(let i=0;i<selectedSpecs.data.length;i++){
                              // 再循环所有的规格属性，只判断与当前外层循环name不同的属性
                              for(let ii=0;ii<specs.data.length;ii++){
                                    // 如果不是相同类型的属性，继续循环
                                    if(selectedSpecs.data[i].goodsAttribute.name!=specs.data[ii][0].goodsAttribute.name){
                                          // 和每种其他类型的属性进行判断
                                          for(let iii=0;iii<specs.data[ii].length;iii++){
                                                // 判断是否拥有相同的skuId
                                                let hasSameSkuId = false;
                                                for(let m=0;m<selectedSpecs.data[i].skuIds.length;m++){
                                                      if(specs.data[ii][iii].skuIds.indexOf(selectedSpecs.data[i].skuIds[m])>=0){
                                                            hasSameSkuId = true;
                                                            specs.data[ii][iii].canClick = true;
                                                            break;
                                                      }
                                                }
                                                if(!hasSameSkuId){
                                                      thiz.log("--------hasSameSkuId--------",hasSameSkuId);
                                                      specs.data[ii][iii].canClick = false;
                                                }
                                          }
                                    }else{
                                          if(selectedSpecs.data.length==1){
                                                // 处理只有一个选中属性的情况，让对应类型属性的其他属性值都可以点击
                                                for(let kkk=0;kkk<specs.data[ii].length;kkk++){
                                                      specs.data[ii][kkk].canClick = true;
                                                }
                                          }
                                    }
                              }

                        }

                  }else{// 没有选中的项的时候
                        // 让所有的属性都可以点击
                        for(let ii=0;ii<specs.data.length;ii++){
                              for(let iii=0;iii<specs.data[ii].length;iii++){
                                    specs.data[ii][iii].canClick = true;
                                    specs.data[ii][iii].isSelected = false;
                              }
                        }
                  }
            }
            thiz.log("--------final_specs--------",thiz.myState.specs);

            // 判断是否是从首页等进来，需要显示商品属性选择
            if(thiz.params.operation=="show_specs" && thiz.isFirstIn){
                  thiz.log("--------thiz.params.operation--------","YES");
                  thiz.isSpecShow = true;
                  thiz.isFirstIn = false;
            }

            thiz.myState.isvisible = thiz.isSpecShow;

            thiz.log("--------thiz.myState.isvisible--------",thiz.myState.isvisible);

            setTimeout(function(){
                  thiz.setState(thiz.myState);
            },500);
            

      }

      // 处理商品规格
      handleSpecs(){
            let thiz = this;
            thiz.log("--------handleSpecs--------","handleSpecs");
            let specs = {};
            let selectedSpecs = {};
            thiz.log("--------handleSpecs--------",thiz.myState.goodsDetail);
            if(thiz.myState.goodsDetail.skuGroups&&thiz.myState.goodsDetail.skuGroups.length>0){
                  thiz.log("--------fuck--------","fuck11111111111111");
                  specs["curSkuId"] = thiz.myState.goodsDetail.sku.id;
                  specs["data"] = [];
                  for(let i=0;i<thiz.myState.goodsDetail.skuGroups.length;i++){
                        let skuItem = thiz.myState.goodsDetail.skuGroups[i];
                        for(let ii=0;ii<skuItem.goodsAttributeOptions.length;ii++){
                              let attrItem = skuItem.goodsAttributeOptions[ii];

                              // 默认不可点击
                              attrItem.canClick = false;
                              // 默认未选中状态
                              attrItem.isSelected = false;

                              attrItem.skuIds = [];
                              attrItem.skuIds.push(skuItem.skuId);
                              let name = attrItem.goodsAttribute.name;

                              // 判断是否已有该名字的数组了
                              let hasName = false;
                              for(let iii=0;iii<specs.data.length;iii++){
                                    if(specs.data[iii].length>0&&specs.data[iii][0].goodsAttribute.name==name){
                                          hasName = true;
                                          // 判断是否已存在相同的规格属性
                                          let hasSpec = false;
                                          for(let k=0;k<specs.data[iii].length;k++){
                                                let ti = specs.data[iii][k];
                                                if(ti.goodsAttributeOption.value==attrItem.goodsAttributeOption.value){
                                                      hasSpec = true;
                                                      specs.data[iii][k].skuIds.push(skuItem.skuId);
                                                      break; 
                                                }
                                          }
                                          if(!hasSpec){specs["data"][iii].push(attrItem);}
                                          break;
                                    }
                              }
                              // 不存在，就新增
                              if(!hasName){
                                    thiz.log("--------hasName--------","hasName");
                                    specs["data"].length = specs["data"].length+1;
                                    specs["data"][specs["data"].length-1] = [];
                                    specs["data"][specs["data"].length-1].push(attrItem);
                              }

                        }
                  }

                  // 从所有整理好的spec中获取当前选中的属性
                  selectedSpecs.skuId = thiz.myState.goodsDetail.sku.id;
                  for(let k = 0;k<specs.data.length;k++){
                        let item = specs.data[k];
                        for(let kk=0;kk<specs.data[k].length;kk++){
                              if(specs.data[k][kk].skuIds.indexOf(thiz.myState.goodsDetail.sku.id)>=0){
                                    // 标记处于选中状态
                                    specs.data[k][kk].isSelected = true;
                                    if(!selectedSpecs.data){
                                          selectedSpecs.data = [];
                                    }
                                    selectedSpecs.data.push(specs.data[k][kk]);
                              }
                        }
                  }
                  thiz.log("--------selectedSpecs111111--------",selectedSpecs);

                  // 判断没有sku选中的异常情况，正常情况是不会出现的
                  if(!selectedSpecs.data){
                        // 让所有属性都可以点击和选中
                        for(let k = 0;k<specs.data.length;k++){
                              let item = specs.data[k];
                              for(let kk=0;kk<specs.data[k].length;kk++){
                                    specs.data[k][kk].canClick = true;
                              }
                        }
                  }
            }
            thiz.log("--------specs--------",specs);
            thiz.log("--------selected_specs--------",selectedSpecs);

            thiz.myState.specs = specs;
            thiz.myState.beforeSpecs = specs;

            thiz.myState.selectedSpecs = selectedSpecs;
            thiz.myState.beforeSeleced = selectedSpecs;

            // 处理那些商品属性可以点击
            thiz.canSpecClick();
      }

      // 处理服务
      handleClickService(index,subindex){
            let thiz = this;
            let goodsDetail = thiz.myState.goodsDetail;
            let services = thiz.myState.goodsDetail.specialServices;
            let curItem = services[index].specialServiceOptions[subindex];
            curItem.isSelected = !curItem.isSelected;

            // 判断是否可以多选
            if(services[index].optionsSelectType=="SINGLE"){
                  // 禁止其他选中
                  for(let i=0;i<services[index].specialServiceOptions.length;i++){

                        if(curItem.id!=services[index].specialServiceOptions[i].id){
                              services[index].specialServiceOptions[i].isSelected = false;
                        }
                  }
            }

            thiz.setState({
                  goodsDetail:goodsDetail
            });
            
      }

      // 是否形成选中skuId
      getSlectedSkuid(){
            let thiz = this;
            thiz.log("-------getSlectedSkuid--------","getSlectedSkuid");
            let selectedSpecs = thiz.state.selectedSpecs;
            let specs = thiz.state.specs;
            let skuId = undefined;
            if(selectedSpecs&&selectedSpecs.data.length>0&&selectedSpecs.data.length==specs.data.length){
                  // 判断哪个skuId数量最多
                  let skus = {};
                  for(let i=0;i<selectedSpecs.data.length;i++){
                        for(let ii=0;ii<selectedSpecs.data[i].skuIds.length;ii++){
                              // 把skuId值当做key
                              let skuIdVK = selectedSpecs.data[i].skuIds[ii];
                              if(skus[skuIdVK]==undefined){
                                    skus[skuIdVK] = 0;
                              }
                              skus[skuIdVK] += 1;
                        }
                  }
                  // 获取数量最大的
                  let tempNum = 0;
                  let tempKey = undefined;
                  for(let key in skus){
                        if(skus[key]>tempNum){
                              tempNum = skus[key];
                              tempKey = key;
                        }
                  }
                  skuId = tempKey;
            }else{// 如果选中的类型数和总类型数不一致，也没有形成新的skuId
                  skuId = undefined;
            }
            thiz.log("--------新的选中的skuId是--------",skuId);

            return skuId;
      }

      // 点击规格属性
      onClickSpec(index,subindex,canClick,isSelected){
            let thiz = this;
            let specs = thiz.myState.specs;
            let item = specs.data[index][subindex];
            let selectedSpecs = thiz.myState.selectedSpecs;
            // 处理点击
            if(canClick){
                  thiz.log("---------onClickSpec--------","onClickSpec   "+isSelected);
                  // 判断是否是取消选择
                  if(isSelected){// 取消选择
                        // 从已选择的商品属性中取消当前属性
                        for(let i=0;i<selectedSpecs.data.length;i++){
                              if(item.goodsAttributeOption.value==selectedSpecs.data[i].goodsAttributeOption.value){

                                    // 删除相同的
                                    selectedSpecs.data.splice(i,1);
                                    // 获取最新的skuId
                                    let newSkuId = thiz.getSlectedSkuid();
                                    selectedSpecs.skuId = newSkuId;

                                    thiz.myState.specs = specs;
                                    thiz.myState.selectedSpecs = selectedSpecs;

                                    // 设置已选属性未选中
                                    specs.data[index][subindex].isSelected = false;

                                    thiz.log("--------click_specs-------",thiz.state.specs);
                                    thiz.log("--------click_selectedSpecs-------",thiz.state.selectedSpecs);
                                    // 重新计算哪些可以点击
                                    thiz.canSpecClick();
                                    break;
                              }
                        }

                  }else{// 选择新的或重新选择（先获取相同的skuId，然后再发请求）

                        /********************************************************************************************/
                        // 先判断同类型的有没有选中的，有的话，取消选中
                        for(let i=0;i<specs.data.length;i++){
                              if(item.goodsAttribute.name==specs.data[i][0].goodsAttribute.name){
                                    for(let ii=0;ii<specs.data[i].length;ii++){
                                          if(specs.data[i][ii].isSelected){
                                                specs.data[i][ii].isSelected = false;
                                                // 删除selectedSpecs中其他选中的
                                                for(let k=0;k<selectedSpecs.data.length;k++){
                                                      if(specs.data[i][ii].goodsAttributeOption.value==selectedSpecs.data[k].goodsAttributeOption.value){
                                                            selectedSpecs.data.splice(k,1);
                                                      }
                                                }
                                          }
                                    }
                                    break;
                              }
                        }
                        // 默认把当前的选中
                        specs.data[index][subindex].isSelected = true;

                        // 把当前选中的加入selectedSpecs
                        if(!selectedSpecs.data){selectedSpecs.data = [];}
                        selectedSpecs.data.push(specs.data[index][subindex]);
                        /********************************************************************************************/
                        
                        // // 获取最新的skuId
                        let newSkuId = thiz.getSlectedSkuid();
                        selectedSpecs.skuId = newSkuId;
                        thiz.log("--------click_specs222222222-------",specs);
                        thiz.log("--------click_selectedSpecs222222222-------",selectedSpecs);

                        thiz.myState.specs = specs;
                        thiz.myState.selectedSpecs = selectedSpecs;
                        // 重新计算哪些可以点击
                        thiz.canSpecClick();

                        // 判断是否发请求重新获取数据
                        if(newSkuId!=undefined){
                              thiz.loadData(newSkuId);
                        }
                  }
            }

      }

      // 处理增加或减少购买数量
      handleBuyNum(type){
            let thiz = this;
            if(type=="m"){// 减
                  thiz.log("--------thiz.myState.goodsDetail.sku.spu.singleOrderMinNum-------",thiz.myState.goodsDetail.sku.spu.singleOrderMinNum);
                  let minNum = thiz.myState.goodsDetail.sku.spu.singleOrderMinNum;
                  if(thiz.state.buyNum>minNum){
                        thiz.setState({
                              buyNum:thiz.state.buyNum-1
                        });
                  }else{
                        thiz.toast("至少购买"+minNum+"件该商品");
                  }
                  return;
            }
            if(type=="a"){// 加
                  let stockNum = thiz.myState.goodsDetail.sku.canSalesStockNum;
                  let maxNum = thiz.myState.goodsDetail.sku.spu.singleOrderMaxNum;
                  if(thiz.state.buyNum<maxNum&&thiz.state.buyNum<stockNum){
                        thiz.setState({
                              buyNum:thiz.state.buyNum+1
                        });
                  }else{
                        let max = stockNum<maxNum?stockNum:maxNum;
                        thiz.toast("至多购买"+max+"件该商品");
                  }
            }
      }

      // 处理点击服务
      

      // 添加商品到购物车
      addCart(){
            let thiz = this;

            //判断有没有可以添加的商品
            let selectedSpecs = thiz.state.selectedSpecs;

            // 获取选中的服务
            let selectedServices = [];
            if(thiz.myState.goodsDetail.specialServices){
                  for(let i=0;i<thiz.myState.goodsDetail.specialServices.length;i++){
                        let item = thiz.myState.goodsDetail.specialServices[i];
                        for(let ii=0;ii<item.specialServiceOptions.length;ii++){
                              if(item.specialServiceOptions[ii].isSelected){
                                    selectedServices.push({
                                          specialServiceOptionId:item.specialServiceOptions[ii].id
                                    });
                              }
                        }
                  }
            }
            
            thiz.log("--------selectedServices--------",selectedServices);
            thiz.log("--------selectedSpecs--------",selectedSpecs);

            if(selectedSpecs.skuId!=undefined){

                  // 判断是加入购物车还是立即购买
                  if(thiz.state.isAddCart){
                        thiz.setState({
                              visible:true
                        });
                        thiz.request("shoppingCar/addGoods",{
                              method:"POST",
                              data:{
                                    num:thiz.state.buyNum,
                                    skuId:selectedSpecs.skuId,
                                    specialServicesSelectItems:selectedServices
                              },
                              success:function(ret){
                                    thiz.setState({
                                          visible:false
                                    });
                                    if(ret.respCode=="00"){
                                          thiz.log("--------加入购物车成功--------","加入购物车成功");
                                          thiz.toast("加入购物车成功",1);
                                          // 关闭商品属性选择
                                          thiz.setState({
                                                visible:false,
                                          });

                                          // 发送添加购物车成功消息
                                          thiz.emit("add_cart_success");
                                    }
                              },
                              error:function(err){
                                    thiz.toast(err.msg);
                                    thiz.setState({
                                          visible:false,
                                    });
                              }
                        });     
                  }else{

                        // 立即购买
                        thiz.setState({
                              visible:true
                        });
                        thiz.request("shoppingCar/buyNow",{
                              method:"POST",
                              data:{
                                    num:thiz.state.buyNum,
                                    skuId:selectedSpecs.skuId,
                                    specialServicesSelectItems:selectedServices
                              },
                              success:function(ret){
                                    
                                    if(ret.respCode=="00"){

                                          // 调settleConfirm
                                          thiz.request("shoppingCar/settleConfirmV2",{
                                          // thiz.request("shoppingCar/settleConfirm",{
                                                method:"POST",
                                                data:{
                                                      num:thiz.state.buyNum,
                                                      skuId:selectedSpecs.skuId,
                                                      specialServicesSelectItems:selectedServices
                                                },
                                                success:function(ret){
                                                      thiz.setState({
                                                            visible:false
                                                      });
                                                      if(ret.respCode=="00"){
                                                            thiz.log("--------shoppingCar/settleConfirm_ret------",ret);
                                                            // 隐藏弹窗
                                                            thiz.isSpecShow = false;
                                                            thiz.setState({
                                                                  isvisible:false,
                                                            });

                                                            /**
                                                             * 判断是否有收货地址，没有的话，就去添加一个
                                                             */
                                                            if(!ret.data.receiverAddresses){
                                                                  // 跳转到新增收货地址
                                                                  thiz.navigate("AddAddress",{title:"添加地址",from:"GoodsXiangQing"});
                                                                  return;
                                                            }
                                                            if(ret.data.receiverAddresses.length==0){
                                                                  // 跳转到新增收货地址
                                                                  thiz.navigate("AddAddress",{title:"添加地址",from:"GoodsXiangQing"});
                                                                  return;
                                                            }


                                                            // 跳转到确认订单页面
                                                            let finalData = ret.data;

                                                            // 过滤收货地址
                                                            let count = 0;
                                                            for(let i=0;i<ret.data.receiverAddresses.length;i++){
                                                              if(ret.data.receiverAddresses[i].defaultAddress){
                                                                  count++;
                                                                  finalData.defaultAddress = ret.data.receiverAddresses[i];
                                                                break;
                                                              }
                                                            }
                                                            // 判断是否有默认收货地址，没有就取第一个
                                                            if(count==0){
                                                                  finalData.defaultAddress = ret.data.receiverAddresses[0];
                                                            }
                                                            
                                                            // 处理电话号码
                                                            let phone = finalData.defaultAddress.receiverPhoneNo;
                                                            finalData.defaultAddress.receiverPhoneNo = phone.substring(0,3)+"****"+phone.substring(phone.length-4);
                                                            // 过滤商品图片
                                                            let imgs = [];
                                                            let goodsNum = 0;

                                                            for(let i=0;i<ret.data.shoppingCarGroupItemsV2.length;i++){
                                                              let item = ret.data.shoppingCarGroupItemsV2[i];
                                                              for(let ii=0;ii<item.shoppingCarItems.length;ii++){
                                                                // 判断商品是否处于选中状态
                                                                if(item.shoppingCarItems[ii].checked){
                                                                  imgs.push(item.shoppingCarItems[ii].sku.imageAttachment.resourceFullAddress);
                                                                  goodsNum += item.shoppingCarItems[ii].num;
                                                                }
                                                              }
                                                            }

                                                            finalData.imgs = imgs;
                                                            finalData.goodsNum = goodsNum;
                                                            thiz.goPage("Querendingdan",{title:"确认订单",data:finalData,allData:ret.data.shoppingCarGroupItemsV2,from:"GoodsXiangQing"});

                                                      }
                                                },
                                                error:function(err){
                                                      thiz.toast(err.msg);
                                                      thiz.setState({
                                                            visible:false,
                                                      });
                                                }
                                          });

                                    }
                              },
                              error:function(err){
                                    thiz.toast(err.msg);
                                    thiz.setState({
                                          visible:false,
                                    });
                              }
                        });
                  }
            }else{
                  thiz.toast("请选择商品属性");
            }
      }

      // 显示规格
      showSpecs(){
            let thiz = this;
            // thiz.log("--------showSpecs--------","showSpecs");

            let result = (<View></View>);
            // 判断是否已经加载了数据
            if(this.state.specs.curSkuId){
                  result = (
                        <View style={{width:"100%",flexDirection:"column"}}>
                              {
                                    thiz.state.specs["data"].map(function(item,index){
                                          return (
                                                <View style={{width:"100%",flexDirection:"column"}}>
                                                      <View style={{width:'100%'}}>
                                                            <Text style={{fontSize:BaseComponent.W*15/375,color:'#474747',marginLeft:BaseComponent.W*10/375,marginTop:BaseComponent.W*15/375}}>{item[0].goodsAttribute.name}</Text>
                                                            <View style={{width:'100%',flexDirection:'row',marginTop:BaseComponent.W*10/375,flexWrap:"wrap"}}>
                                                                  {
                                                                        item.map(function(value,id){
                                                                              return (
                                                                                    <TouchableWithoutFeedback onPress={()=>thiz.onClickSpec(index,id,value.canClick,value.isSelected)}>
                                                                                    <View style={{height:BaseComponent.W*25/375,paddingLeft:5,paddingRight:5,marginLeft:BaseComponent.W*10/375,
                                                                                    borderWidth:value.isSelected?0:0.5,
                                                                                    backgroundColor:value.canClick?(value.isSelected?'#FDD17A':"#ffffff"):"#efefef",
                                                                                    borderRadius:3,
                                                                                    borderColor:value.canClick?(value.isSelected?"#474747":"#474747"):"#efefef",
                                                                                    justifyContent:'center',alignItems:'center',marginTop:10}}>
                                                                                                <Text style={{color:value.canClick?(value.isSelected?"#474747":"#474747"):"#BFBFBF",fontSize:BaseComponent.W*14/375}}>{value.goodsAttributeOption.value}</Text>           
                                                                                    </View>
                                                                                    </TouchableWithoutFeedback>
                                                                              );
                                                                        })
                                                                  }
                                                            </View>
                                                      </View>
                                                      <View style={{width:'100%',height:0.5,backgroundColor:"#E7E7E7",marginTop:BaseComponent.W*15/375}}></View>
                                                </View>
                                          );
                                    })
                              }

                              
                              
                              {/*物流*/}
                              <View style={{width:'100%',height:BaseComponent.W*151/375,display:"none"}}>
                                    <Text style={{color:"#474747",fontSize:BaseComponent.W*15/375,marginLeft:BaseComponent.W*10/375,marginTop:BaseComponent.W*15/375}}>物流</Text>
                                    <View style={{width:BaseComponent.W*190/375,height:BaseComponent.W*25/375,backgroundColor:'#FDD17A',
                                                      borderRadius:3,justifyContent:'center',alignItems:'center',marginLeft:BaseComponent.W*10/375,marginTop:BaseComponent.W*11/375}}>
                                          <Text style={{fontSize:BaseComponent.W*14/375,color:'#474747'}}>EMS国际 包邮包税 BC清关</Text>                
                                    </View>
                                    <View style={{width:BaseComponent.W*190/375,height:BaseComponent.W*25/375,borderColor:'#AFAFAF',borderWidth:1,
                                                      borderRadius:3,justifyContent:'center',alignItems:'center',marginLeft:BaseComponent.W*10/375,marginTop:BaseComponent.W*11/375}}>
                                          <Text style={{fontSize:BaseComponent.W*14/375,color:'#AFAFAF'}}>EMS国际 包邮包税 CC清关</Text>                
                                    </View>
                                    <View style={{width:BaseComponent.W*260/375,height:BaseComponent.W*25/375,borderColor:'#AFAFAF',borderWidth:1,
                                                      borderRadius:3,justifyContent:'center',alignItems:'center',marginLeft:BaseComponent.W*10/375,marginTop:BaseComponent.W*11/375}}>
                                          <Text style={{fontSize:BaseComponent.W*14/375,color:'#AFAFAF'}}>EMS国际 包邮包税 快件清关-免证件图</Text>                 
                                    </View>
                              </View>
                              <View style={{width:'100%',height:0.5,backgroundColor:"#E7E7E7",display:"none"}}></View>


                        </View>
                  );
            }
            return result;
      }

      // 显示服务
      showServices(){
            let thiz = this;
            // thiz.log("--------showServices--------","showServices");

            let result = (<View></View>);
            // 判断是否已经加载了数据
            if(thiz.myState.goodsDetail.specialServices){
                  result = (
                        <View style={{width:"100%",flexDirection:"column"}}>
                              {
                                    thiz.myState.goodsDetail.specialServices.map(function(item,index){
                                          return (

                                                <View>
                                                      <View style={{width:"100%",flexDirection:"column"}}>
                                                            <View style={{width:'100%'}}>
                                                                  <Text style={{fontSize:BaseComponent.W*15/375,color:'#474747',marginLeft:BaseComponent.W*10/375,marginTop:BaseComponent.W*15/375}}>{item.name}</Text>
                                                                  <View style={{width:'100%',flexDirection:'column',marginTop:BaseComponent.W*10/375}}>
                                                                        {
                                                                              item.specialServiceOptions.map(function(value,id){
                                                                                    return (
                                                                                          <TouchableWithoutFeedback onPress={()=>thiz.handleClickService(index,id)}>
                                                                                          
                                                                                          <View style={{height:BaseComponent.W*25/375,marginLeft:BaseComponent.W*10/375,
                                                                                          justifyContent:'center',alignItems:'flex-start',marginTop:id==0?0:8,}}>

                                                                                                      <Text style={{
                                                                                                            color:value.isSelected?"#474747":"#474747",
                                                                                                            fontSize:BaseComponent.W*14/375,
                                                                                                            borderWidth:value.isSelected?0:0.5,
                                                                                                            backgroundColor:value.isSelected?"#FDD17A":"white",
                                                                                                            borderRadius:3,
                                                                                                            borderColor:value.isSelected?"white":"#474747",
                                                                                                            paddingLeft:5,paddingRight:5,
                                                                                                            height:"100%",
                                                                                                            lineHeight:BaseComponent.W*25/375,
                                                                                                            }}>

                                                                                                            {value.name}

                                                                                                      </Text>           
                                                                                          </View>
                                                                                          </TouchableWithoutFeedback>
                                                                                    );
                                                                              })
                                                                        }
                                                                  </View>
                                                            </View>
                                                            <View style={{width:'100%',height:0.5,backgroundColor:"#E7E7E7",marginTop:BaseComponent.W*15/375,}}></View>
                                                      </View>
                                                </View>

                                                
                                          );
                                    })
                              }


                        </View>
                  );
            }
            return result;
      }

      // 通过skuId加载
      loadData(skuId){
            let thiz=this;
            thiz.log("--------loadData--------","loadData");
            if(skuId){
                  thiz.setState({
                        visible:true
                  });
                  thiz.request("sku/getById",{
                    method:"POST",
                    data:{id:skuId},
                    success:function(ret){
                        thiz.log("--------商品详情aaaaaaaaa--------",ret);
                        if(ret&&ret.respCode=="00"){
                              thiz.log("--------shit_success--------","shit_success");
                              // 设置正在处理数据
                              thiz.renderingData = true;
                              thiz.myState.isvisible = thiz.isSpecShow;
                              // 取最小购买量作为显示购买数量
                              thiz.myState.buyNum = ret.data.sku.spu.singleOrderMinNum;
                              thiz.log("--------该sku最少购买数量--------",thiz.myState.buyNum);
                              
                              thiz.myState.goodsDetail = ret.data;
                              if(ret.data.sliderAttachments&&ret.data.sliderAttachments.length>0){
                                    thiz.log("--------shit_success11111111--------","shit_success11111111");
                                    let state = thiz.myState;
                                    state.SwiperIndex = 1;
                                    state.SwiperTotal = ret.data.sliderAttachments.length;
                                    thiz.log("--------商品详情--------","fffffffffffffffff");
                              }
                              
                              // 处理规格
                              thiz.handleSpecs();
                              
                        }else{// 其他状态码
                              thiz.setState({
                                    visible:false
                              });
                        }
                    },
                    error:function(err){
                        thiz.log("--------ffffffffff_err--------",err);
                        thiz.toast(err.msg);
                        thiz.setState({
                              visible:false,
                        });
                    }
                  });
            }
      }

      // 通过首次spuId加载
      loadDataBySpu(spuId){
            let thiz=this;
            thiz.log("--------loadDataBySpu--------","loadDataBySpu");
            if(spuId){
                  thiz.setState({
                        visible:true
                  });
                  thiz.request("spu/getSkuData",{
                    method:"POST",
                    data:{id:spuId},
                    success:function(ret){
                        thiz.log("--------商品详情aaaaaaaaa--------",ret);
                        if(ret&&ret.respCode=="00"){
                              thiz.log("--------shit_success--------","shit_success");
                              // 设置正在处理数据
                              thiz.renderingData = true;
                              thiz.myState.isvisible = thiz.isSpecShow;
                              // 取最小购买量作为显示购买数量
                              thiz.myState.buyNum = ret.data.sku.spu.singleOrderMinNum;
                              thiz.log("--------该sku最少购买数量--------",thiz.myState.buyNum);
                              
                              thiz.myState.goodsDetail = ret.data;
                              if(ret.data.sliderAttachments&&ret.data.sliderAttachments.length>0){
                                    thiz.log("--------shit_success11111111--------","shit_success11111111");
                                    // var newData={video:'http://183.60.197.31/16/p/d/p/o/pdpocxixmtlhhrblafoutulwaglanj/hc.yinyuetai.com/A829016892782BC5F7C7905C3634EF01.mp4?sc=b6639eb24692a29e'}
                                    // ret.data.sliderAttachments.push(newData);

                                    let state = thiz.myState;
                                    state.SwiperIndex = 1;
                                    state.SwiperTotal = ret.data.sliderAttachments.length;
                                    thiz.log("--------商品详情--------","fffffffffffffffff");
                              }
                              
                              // 处理规格
                              thiz.handleSpecs();
                              
                        }else{// 其他状态码
                              thiz.setState({
                                    visible:false
                              });
                        }
                    },
                    error:function(err){
                        thiz.log("--------ffffffffff_err--------",err);
                        thiz.toast(err.msg);
                        thiz.setState({
                              visible:false,
                        });
                    }
                  });
            }
      }

	componentDidMount(){
		let thiz = this;
            var id=thiz.params.id;
            // thiz.loadData(id);
            
            // 判断是否是从购物车跳转过来的
            if(thiz.params.from&&thiz.params.from=="Cart"){
                  thiz.loadData(id);
                  return;
            }

            // 其他情况
            thiz.loadDataBySpu(id);
            
	}

      getDetail(){
            let thiz = this;
            if(thiz.myState.goodsDetail.detailAttachments&&thiz.myState.goodsDetail.detailAttachments.length>0){
                  return thiz.myState.goodsDetail.detailAttachments.map((item,index)=>{
                        return (
                              <MyImage url={item.resourceFullAddress} width={BaseComponent.W} onPress={()=>{
                                    thiz.log("------------------你点击了啊啊啊啊啊啊啊啊啊啊啊-----------------------------",thiz.myState.goodsDetail.detailAttachments);
                                    var newData=[];
                                    var imageUrl=[];
                                    
                                    for(var ii=0;ii<thiz.myState.goodsDetail.detailAttachments.length;ii++){ 
                                          newData.push(thiz.myState.goodsDetail.detailAttachments[ii])     
                                    }

                                    for(var iii=0;iii<newData.length;iii++){
                                          var image={};
                                          image.url=newData[iii].resourceFullAddress;
                                          imageUrl.push(image);
                                    }
                                    thiz.log("----------------------imageUrl----------------------------",imageUrl);

                                    thiz.setState({
                                          newData:newData,
                                          SavePicVisible:true,
                                          imageUrl:imageUrl
                                    });
                                    thiz.myState.saveIndex=index;
                              }}/> 
                        );
                  });
            }else{
                  return (
                        <View></View>
                  );
            }
            
      }

      // 跳转到客服
      toService(){
            let thiz = this;
            thiz.log("--------toService--------","toService");
            thiz.goPage("Service",{title:"客服"});
      }

	render(){
            let thiz = this;
            console.log("--------------------刷新了render------------------------------","啊啊啊啊啊啊啊啊啊");
		return (
			<View style={{backgroundColor:thiz.state.videoViseble?'black':'white',flex:1,position:"relative",}}>

                  {/*转菊花*/}
                      <Modal
                        visible={this.state.visible}
                        onRequestClose={()=>{this.setState({visible:false})}}
                        transparent={true}
                        animationType={"fade"}>
                        <View style={{backgroundColor:"rgba(0,0,0,0.4)",width:"100%",width:"100%",flex:1,flexDirection:"column",alignItems:"center",justifyContent:"center",position:"relative"}}>
                          <View>
                                <ActivityIndicator size="large" color='white'/>
                          </View>
                        </View>
                      </Modal>

                      <Modal
                        visible={this.state.videoViseble}
                        supportedOrientations={['portrait', 'landscape']}
                        onRequestClose={()=>{this.setState({videoViseble:false,smallScreenPaused:false});
                              // Orientation.lockToPortrait();
                        }}
                        onShow={()=>{
                            //   console.log("----------------show-----------------","show");
                            //   // 判断横竖屏幕
                            // const initial1 = Orientation.getInitialOrientation();
                            // if (initial1 === 'PORTRAIT') {
                            //   console.log("-------------------当前是竖屏----------------------","竖屏");
                            // } else {
                            //   console.log("-------------------当前是横屏----------------------","横屏");
                            // }
                        }}
                        transparent={true}>
                              {/*视频播放*/}
                              <TouchableWithoutFeedback onPress={()=>{
                                    // console.log("-------------------------------不全屏--------------------------------------","不全屏")
                                    // thiz.setState({videoViseble:false});
                                    // Orientation.lockToPortrait();
                              }}>
                                    <View style={{width:'100%',height:'100%',alignItems:'center',justifyContent:'center'}}>
                                          <Video source={{uri: "http://183.60.197.31/16/p/d/p/o/pdpocxixmtlhhrblafoutulwaglanj/hc.yinyuetai.com/A829016892782BC5F7C7905C3634EF01.mp4?sc=b6639eb24692a29e"}}
                                                ref='player'
                                                resizeMode="cover"  
                                                style={{width:'100%',height:'85%'}}
                                                paused={thiz.state.paused}
                                                onProgress={this.onProgress}
                                                onLoad={this.onLoad}/>
                                          <View style={{width:'100%',height:'10%',flexDirection:'row',alignItems:'center'}}>
                                                <TouchableWithoutFeedback onPress={()=>{
                                                      thiz.setState({paused:!thiz.state.paused})
                                                }}>
                                                      <View style={{width:'10%',height:'100%',backgroundColor:'black',justifyContent:'center',alignItems:'center'}}>
                                                            <Image style={{width:BaseComponent.W*0.05,height:BaseComponent.W*0.05}} resizeMode="cover" source={thiz.state.paused?require('../../image/home/play.png'):require('../../image/home/stop.png')}/>
                                                      </View>
                                                </TouchableWithoutFeedback>
                                                <View style={{width:'75%',height:'100%',flexDirection:'row',alignItems:'center'}}>
                                                      <Text style={{width:BaseComponent.W*0.2,color:'white'}}>{thiz.state.currentTime}</Text>                              
                                                      <View style={{width:'55%',height:'100%',backgroundColor:'black'}}>
                                                            <Slider style={{width:'100%'}}
                                                                  minimumValue={0}
                                                                  maximumValue={thiz.state.totalSecond}
                                                                  maximumTrackTintColor="white"
                                                                  value={thiz.state.currentSecond}
                                                                  onSlidingComplete={(index)=>{
                                                                        thiz.refs.player.seek(index);
                                                                  }}
                                                            />    
                                                      </View>
                                                      <Text style={{width:BaseComponent.W*0.2,color:'white',marginLeft:10}}>{thiz.state.totalTime}</Text>    
                                                </View>  
                                                <TouchableWithoutFeedback onPress={()=>{
                                                      thiz.setState({videoViseble:false,smallScreenPaused:false});
                                                      // Orientation.lockToPortrait();
                                                }}>          
                                                      <View style={{width:'10%',height:'100%',justifyContent:'center',alignItems:'center'}}>
                                                            <Image style={{width:BaseComponent.W*0.07,height:BaseComponent.W*0.07}} source={require('../../image/home/narrow.png')}/>
                                                      </View>
                                                </TouchableWithoutFeedback>       
                                          </View>     
                                    </View>
                              </TouchableWithoutFeedback>                                
                      </Modal>

				<StatusBar translucent={true} backgroundColor={this.state.isvisible?'rgba(14,14,14,0.5)':(thiz.state.SavePicVisible?'black':'transparent')} barStyle={'dark-content'}/>
				
                        {/*属性弹窗*/}
				<Modal visible={this.state.isvisible}
                   	   transparent={true}
                   	   onRequestClose={() => {this.setState({isvisible:false});this.isSpecShow = false;}}>

                   	      <View style={{flex:1,backgroundColor:'rgba(14,14,14,0.5)',flexDirection:"column"}}>
                                    {/*占位*/}
                                    <TouchableWithoutFeedback onPress={()=>{this.setState({isvisible:false});this.isSpecShow = false;}}>
                                    <View style={{width:"100%",flex:1}}></View>
                                    </TouchableWithoutFeedback>
                   	            {/*商品图片*/}
                   	            <Image style={{bottom:BaseComponent.H*2/3-BaseComponent.W*55/375,width:BaseComponent.W*75/375,height:BaseComponent.W*75/375,position:'absolute',marginLeft:BaseComponent.W*10/375,zIndex:100000}} source={{uri:this.state.goodsDetail.sku&&this.state.goodsDetail.sku.imageAttachment?this.state.goodsDetail.sku.imageAttachment.resourceFullAddress:""}}></Image>
                               	
                                    <View style={{width:'100%',height:BaseComponent.H*2/3,backgroundColor:'white',position:'absolute',bottom:0,flexDirection:"column"}}>

                               		{/*价格、数量*/}
                               		<View style={{width:'100%',height:BaseComponent.W*67/375,flexDirection:'row'}}>
                               			<View style={{height:'100%',marginLeft:BaseComponent.W*100/375}}>
                               				<Text style={{fontSize:BaseComponent.W*18/375,color:'#FF407E',marginTop:BaseComponent.W*10/375}}>¥{this.state.goodsDetail.sku?this.state.goodsDetail.sku.realSalePrice:"0"}<Text style={{fontSize:BaseComponent.W*12/375,color:'#4a4a4a'}}>&nbsp;&nbsp;|&nbsp;&nbsp;单件{this.state.goodsDetail.sku?this.state.goodsDetail.sku.buyUnitPrice:"0"}元</Text></Text>
                               				<Text style={{fontSize:BaseComponent.W*12/375,color:'#9A9A9A',marginTop:BaseComponent.W*10/375}}>{this.state.goodsDetail.sku&&this.state.goodsDetail.sku.canSalesStockNum>=50?"库存充足":"库存紧张"}</Text>
                               			</View>
                               			<View style={{display:"none",justifyContent:'center',backgroundColor:'#FDD17A',height:BaseComponent.W*18/375,marginTop:BaseComponent.W*15/375,marginLeft:BaseComponent.W*16/375,}}>
                               				<Text style={{fontSize:BaseComponent.W*12/375,color:'#FFFFFF',margin:1}}>税费补贴</Text>
                               			</View>
                               		</View>
                               		<View style={{width:'100%',height:0.5,backgroundColor:"#E7E7E7"}}></View>

                               		<ScrollView style={{flex:1}}  showsVerticalScrollIndicator = {false}>
                                          
                                          {/*规格展示区域*/}
                                          {this.showSpecs()}
                                          
                                          {/*服务展示区域*/}
                                          {this.showServices()}
                                          
                               		{/*购买数量*/}
                               		<View style={{width:'100%',height:BaseComponent.W*32/375,flexDirection:'row',justifyContent:'space-between',alignItems:'center',marginTop:BaseComponent.W*10/375}}>
                               			<Text style={{fontSize:BaseComponent.W*15/375,color:'#474747',marginLeft:BaseComponent.W*10/375}}>购买数量</Text>
                               			<View style={{width:BaseComponent.W*110/375,height:BaseComponent.W*32/375,borderRadius:3,marginRight:BaseComponent.W*10/375,
                               						borderWidth:1,borderColor:'#535353',flexDirection:'row'}}>
                                                      {/*减*/}
                                                      <TouchableWithoutFeedback onPress={()=>this.handleBuyNum("m")}>
                               				<View style={{width:BaseComponent.W*32/375,height:BaseComponent.W*32/375,justifyContent:'center',alignItems:'center'}}>
                               					<Image style={{width:BaseComponent.W*19/375,height:1}} source={require('../../image/home/jian.png')}></Image>
                               				</View>
                                                      </TouchableWithoutFeedback>	

                               				<View style={{width:0.5,height:BaseComponent.W*30/375,backgroundColor:'#535353'}}></View>

                                                      {/*数量显示*/}
                               				<View style={{width:BaseComponent.W*45/375,height:BaseComponent.W*32/375,justifyContent:'center',alignItems:'center'}}>
                               					<Text style={{color:'#212121',fontSize:BaseComponent.W*18/375}}>{this.state.buyNum}</Text>
                               				</View>	
                               				<View style={{width:0.5,height:BaseComponent.W*30/375,backgroundColor:'#535353'}}></View>

                                                      {/*加*/}
                                                      <TouchableWithoutFeedback onPress={()=>this.handleBuyNum("a")}>
                               				<View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
            									<Image style={{width:BaseComponent.W*19/375,height:BaseComponent.W*19/375}} source={require('../../image/home/plus1.png')}></Image>
                               				</View>
                                                      </TouchableWithoutFeedback>

                               			</View>
                               		</View>
                               		<View style={{width:'100%',height:BaseComponent.W*80/375}}></View>
                               		</ScrollView>

                               		{/*加入购物车*/}
                                          <TouchableWithoutFeedback onPress={()=>{
                                                // 判断用户是否已登录
                                                thiz.T.load("account",function(ret){
                                                      if(ret){
                                                            thiz.addCart();
                                                      }else{
                                                            // 隐藏弹窗
                                                            thiz.isSpecShow = false;
                                                            thiz.setState({
                                                                  isvisible:false,
                                                            });
                                                            setTimeout(function(){
                                                                  thiz.navigate("Login",{});
                                                            },100);
                                                            
                                                      }
                                                });
                                          }}>
                               		<View style={{width:'100%',height:BaseComponent.W*50/375,backgroundColor:(function(){
                                                let color = '#FDD17A';
                                                if(thiz.state.goodsDetail&&thiz.state.goodsDetail.sku&&thiz.state.goodsDetail.sku.canSalesStockNum==0){
                                                      color = "#B3B3B3";
                                                }
                                                return color;
                                          })(),justifyContent:'center',alignItems:'center'}}>
                               			<Text style={{fontSize:BaseComponent.W*18/375,color:'#727272'}}>{this.state.isAddCart?"加入购物车":"立即购买"}</Text>
                               		</View>
                                          </TouchableWithoutFeedback>
                                    </View>   
					</View>
				</Modal>

                        {/*点击放大图片后可以保存*/}
                        <Modal visible={this.state.SavePicVisible}
                           transparent={true}
                           animationType={"slide"}
                           onRequestClose={() => {this.setState({SavePicVisible:false})}}>
                             <View style={{width:'100%',height:'100%',backgroundColor:'rgba(0,0,0,1)'}}>
                                    
                               <ImageViewer
                                    imageUrls={thiz.state.imageUrl} 
                                    enableImageZoom={true} 
                                    saveToLocalByLongPress={false} 
                                    index={thiz.myState.saveIndex} 
                                    menuContext={{ "saveToLocal": "保存图片", "cancel": "取消" }}
                                    onChange={(index) =>{
                                          var newIndex=index;
                                          console.log("-----------------newIndex----------------------------------------",newIndex);
                                          thiz.myState.saveIndex=index;
                                    }} 
                                    onClick={() => { 
                                        console.log("---------------onCancel---------------");
                                        thiz.setState({SavePicVisible:false});
                                    }}
                                    
                                    renderFooter={()=>{
                                          return (
                                                <TouchableWithoutFeedback onPress={()=>{
                                                      var saveIndex=thiz.myState.saveIndex;
                                                      console.log("--------------saveIndex-------------------------",saveIndex);
                                                      console.log("------------------------点击了下载图片---------------------------------",thiz.state.newData[saveIndex]);
                                                      if(BaseComponent.OS=='ios'){
                                                            var promise = CameraRoll.saveToCameraRoll(thiz.state.newData[saveIndex].resourceFullAddress);
                                                            promise.then(function(result) {
                                                               thiz.toast("保存成功");
                                                            }).catch(function(error) {
                                                                  thiz.log("---------------error--------------",error);
                                                               thiz.toast("保存失败");
                                                            })
                                                            
                                                            // thiz.toast("图片保存成功");
                                                      }else{
                                                           thiz.T.requestAndroidPermission("WRITE_EXTERNAL_STORAGE",function(){
                                                                 const storeLocation = `${RNFS.ExternalDirectoryPath}`;
                                                                 let pathName = new Date().getTime() + "savePic.png"
                                                                 let downloadDest = `${storeLocation}/${pathName}`;
                                                                 console.log("---------------------------downloadDest---------------------------",downloadDest);
                                                                  const ret = RNFS.downloadFile({fromUrl:thiz.state.newData[saveIndex].resourceFullAddress,toFile:downloadDest});
                                                                  ret.promise.then(res => {
                                                                    if(res && res.statusCode === 200){
                                                                        var promise = CameraRoll.saveToCameraRoll(downloadDest);
                                                                        promise.then(function(result) {
                                                                           thiz.toast("保存成功");
                                                                        }).catch(function(error) {
                                                                              thiz.log("---------------error--------------",error);
                                                                           thiz.toast("保存失败");
                                                                        })
                                                                    }
                                                                  })
                                                           });     
                                                      }        
                                                }}>
                                                      <View style={{width:BaseComponent.W,height:BaseComponent.W*40/375,justifyContent:'center',alignItems:'center',marginBottom:thiz.isIphoneX(25,15)}}>
                                                            <Image style={{width:BaseComponent.W*40/375,height:BaseComponent.W*40/375}} source={require('../../image/mine/downPicture1.png')}></Image>
                                                      </View>
                                                </TouchableWithoutFeedback>
                                          )
                                    }}

                                />
                                    
                             </View>
                        </Modal>    
                             
				{/*左箭头返回*/}
                        <TouchableWithoutFeedback onPress={()=>this.goBack()}>
				<View style={{display:thiz.state.videoViseble?'none':'flex',width:BaseComponent.W*60/375,height:BaseComponent.W*60/375,marginTop:BaseComponent.W*40/375,
							backgroundColor:'transparent',marginLeft:BaseComponent.W*10/375,justifyContent:'center',alignItems:'center',position:'absolute',top:-18,left:-18,zIndex:50000}}>

                                    <View style={{display:thiz.state.videoViseble?'none':'flex',width:BaseComponent.W*24/375,height:BaseComponent.W*24/375,borderRadius:BaseComponent.W*12/375,
                                          backgroundColor:'#000000',justifyContent:'center',alignItems:'center',}}>
                                          <Image style={{width:BaseComponent.W*15/375,height:BaseComponent.W*20/375,marginRight:BaseComponent.W*2/375}} source={require('../../image/home/zuojiantou.png')}></Image>
                                    </View>
			      </View>
                        </TouchableWithoutFeedback>
				<ScrollView style={{flex:1}}  showsVerticalScrollIndicator = {false}>
                        
                        {/*轮播区域*/}
                        <View style={{position:"relative",width:"100%",display:thiz.state.videoViseble?'none':'flex'}}>
      				<Swiper 
      					style={{height:BaseComponent.W}}
                                    height={BaseComponent.W}
                                    key={thiz.myState.SwiperTotal}
                                    horizontal={true}
      					autoplay={false}
      					loop={false}
      					showsPagination={false}
      					showsButtons={false}
                                    autoplayTimeout={5}
      					onIndexChanged={(index)=>
                                          this.changeIndex(index)
                                    }>
      						{this.getBanner(this.state.goodsDetail.sliderAttachments?this.state.goodsDetail.sliderAttachments:[])}
      				</Swiper>
      				{/*轮播页数当前页*/}
      				<View style={{width:BaseComponent.W*20/375,height:BaseComponent.W*20/375,backgroundColor:'red',marginRight:'5%',marginLeft:BaseComponent.W*340/375,position:'absolute',top:BaseComponent.W-BaseComponent.W*25/375,right:0,
      							borderRadius:BaseComponent.W*10/375,backgroundColor:'gray',justifyContent:'center',alignItems:'center',marginBottom:BaseComponent.W*5/375}}>
      						<Text ref='txt' style={{fontSize:BaseComponent.W*10/375,color:'white'}}>{this.state.SwiperIndex}/{this.state.SwiperTotal}</Text>	
      				</View>

                              {/*无货提示*/}
                              <View style={{display:(function(){
                                    return thiz.state.goodsDetail&&thiz.state.goodsDetail.sku&&thiz.state.goodsDetail.sku.canSalesStockNum==0?"flex":"none";
                              })(),position:(function(){
                                    return thiz.state.goodsDetail&&thiz.state.goodsDetail.sku&&thiz.state.goodsDetail.sku.canSalesStockNum==0?"absolute":"relative";
                              })(),width:"100%",paddingTop:10,paddingBottom:10,bottom:0,backgroundColor:"rgba(0,0,0,0.4)"}}>
                                    <Text style={{color:"white",width:"100%",textAlign:"center",fontSize:14,}}>此商品已无货</Text>
                              </View>
                        </View>
			
				<View style={{marginBottom:BaseComponent.W*49/375,display:thiz.state.videoViseble?'none':'flex'}}>
				{/*价格*/}
				<View style={{width:'100%',height:BaseComponent.W*50/375,alignItems:'center',flexDirection:'row',justifyContent:'space-between'}}>
                              <View style={{flexDirection:'row',height:BaseComponent.W*50/375,alignItems:'center'}}>
                                  {/*销售价格*/}
                                  <Text style={{fontSize:BaseComponent.W*24/375,color:'#FF407E',marginLeft:BaseComponent.W*11/375,marginBottom:BaseComponent.W*2/375}}>¥{this.state.goodsDetail.sku?this.state.goodsDetail.sku.realSalePrice:"0"}</Text>    
                                  {/*市场价格*/}
                                  <Text style={{textDecorationLine:'line-through',fontSize:BaseComponent.W*13/375,color:'#999999',marginLeft:BaseComponent.W*13/375}}>¥{this.state.goodsDetail.sku?this.state.goodsDetail.sku.marketPrice:"0"}</Text>
                                  {/*已优惠*/}
                                  <View style={{marginLeft:BaseComponent.W*13/375,paddingTop:0,paddingBottom:0,paddingLeft:5,paddingRight:5,backgroundColor:"#000000",
                                          display:this.state.goodsDetail.sku&&this.state.goodsDetail.sku.discountPrice?"flex":"none"}}>
                                    <Text style={{fontSize:12,color:'#FDD17A',marginBottom:BaseComponent.W*1/375}}>已优惠￥{this.state.goodsDetail.sku?this.state.goodsDetail.sku.discountPrice:"0"}</Text>    
                                  </View>
                              </View>
					
					<View style={{opacity:0,width:BaseComponent.W*53/375,height:BaseComponent.W*15/375,backgroundColor:"#FDD17A",marginLeft:BaseComponent.W*14/375,alignItems:"center",justifyContent:'center',display:"none"}}>
					    <Text style={{fontSize:BaseComponent.W*12/375,color:'#ffffff'}}>税费补贴</Text>
					</View>
                              <View style={{height:BaseComponent.W*50/375,flexDirection:'row',alignItems:'center'}}>
      				    <Image style={{width:BaseComponent.W*22/375,height:BaseComponent.W*17/375,backgroundColor:'transparent',marginRight:BaseComponent.W*10/375}} source={this.state.goodsDetail.sku&&this.state.goodsDetail.sku.spu.brand.country.imageAttachment?{uri:this.state.goodsDetail.sku.spu.brand.country.imageAttachment.resourceFullAddress}:""}></Image>
      				    <Text style={{fontSize:BaseComponent.W*13/375,color:'#666666',marginRight:BaseComponent.W*12/375}}>{this.state.goodsDetail.sku?this.state.goodsDetail.sku.spu.brand.country.name+"品牌":""}</Text>
				      </View>  
                        </View>
				{/*几件装*/}
				<View style={{flexDirection:'row',display:thiz.state.videoViseble?'none':'flex'}}>
                              
					<View style={{
								marginLeft:BaseComponent.W*11/375,borderWidth:0,justifyContent:'center',alignItems:'center'}}>
						<Text style={{fontSize:BaseComponent.W*13/375,color:'#3a3a3a'}} onPress={()=>{
                                          // this.setState({isvisible:true});
                                    }}>单件{this.state.goodsDetail.sku?this.state.goodsDetail.sku.buyUnitPrice:"0"}元</Text>
					</View>

					{/*<View style={{display:"none",width:BaseComponent.W*125/375,height:BaseComponent.W*30/375,borderRadius:BaseComponent.W*15/375,
								marginLeft:BaseComponent.W*11/375,borderColor:"#D7D7D7",borderWidth:1,justifyContent:'center',alignItems:'center'}}>
						<Text style={{fontSize:BaseComponent.W*13/375,color:'#B7B7B7'}}>2件装 | 单件1588元</Text>
					</View>*/}
				</View>
				{/*商品说明*/}
				<View style={{width:'100%',marginTop:BaseComponent.W*14/375,display:thiz.state.videoViseble?'none':'flex'}}>
					<View style={{width:BaseComponent.W*353/375,marginLeft:BaseComponent.W*11/375}}>
						<Text style={{color:'#1A1A1A',fontSize:BaseComponent.W*17/375,lineHeight:BaseComponent.W*24/375}}>{this.state.goodsDetail.sku?this.state.goodsDetail.sku.name:""}</Text>
						<Text style={{fontSize:BaseComponent.W*14/375,color:'#5A5A5A',marginTop:BaseComponent.W*5/375}}>{this.state.goodsDetail.sku?this.state.goodsDetail.sku.summary:""}</Text>
					</View>
				</View>
				{/*海外直邮、正品保证*/}

				
				<View style={{display:thiz.state.videoViseble?'none':'flex',width:'100%',height:BaseComponent.W*43/375,backgroundColor:'#F6F6F6',flexDirection:'row',alignItems:'center',marginTop:BaseComponent.W*10/375,paddingRight:BaseComponent.W*10/375,justifyContent:'space-between'}}>
					<View style={{flexDirection:'row',height:BaseComponent*43/375,alignItems:'center'}}>
                              <Image style={{width:BaseComponent.W*20/375,height:BaseComponent.W*20/375,marginLeft:BaseComponent.W*9/375}} source={require('../../image/home/goodszhiyou.png')}></Image>
					<Text style={{fontSize:BaseComponent.W*14/375,color:'#919090',marginLeft:BaseComponent.W*7/375}}>海外直邮</Text>
					</View>
                              <View style={{flexDirection:'row',height:BaseComponent*43/375,alignItems:'center'}}>
                              <Image style={{width:BaseComponent.W*20/375,height:BaseComponent.W*20/375}} source={require('../../image/home/goodszhengping.png')}></Image>
					<Text style={{fontSize:BaseComponent.W*14/375,color:'#919090',marginLeft:BaseComponent.W*7/375}}>正品保证</Text>
					</View>
                              <View style={{flexDirection:'row',height:BaseComponent*43/375,alignItems:'center',marginRight:BaseComponent.W*9/375}}>
                              <Image style={{width:BaseComponent.W*20/375,height:BaseComponent.W*20/375}} source={require('../../image/home/goodswuliu.png')}></Image>
					<Text style={{fontSize:BaseComponent.W*14/375,color:'#919090',marginLeft:BaseComponent.W*7/375}}>物流无忧</Text>
				      </View>  
                        </View>
				<View style={{width:'100%',height:BaseComponent.W*15/375,backgroundColor:'#F0F0F0',display:thiz.state.videoViseble?'none':'flex'}}></View>
				
                        {/*包邮包税(this.state.goodsDetail.sku&&(this.state.goodsDetail.sku.logisticTemplate.logisticTemplateType=="FREE"||this.state.goodsDetail.sku.taxPrice==0))?"flex":"none"*/}
				<View style={{flexDirection:'row',display:"none",alignItems:'center'}}>
					<View style={{height:BaseComponent.W*25/375,borderWidth:1,justifyContent:'center',alignItems:'center',
						borderRadius:BaseComponent.W*5/375,borderColor:'#FDD17A',marginTop:BaseComponent.W*10/375,marginLeft:BaseComponent.W*10/375}}>
						<Text style={{fontSize:BaseComponent.W*13/375,color:'#FDD17A',margin:5}}>{this.state.goodsDetail.sku&&this.state.goodsDetail.sku.logisticTemplate.logisticTemplateType!="FREE"?"":"包邮"}{this.state.goodsDetail.sku&&this.state.goodsDetail.sku.taxPrice>0?"":"包税"}</Text>
					</View>
					<Text style={{fontSize:BaseComponent.W*13/375,color:'#5A5A5A',marginLeft:BaseComponent.W*13/375,marginTop:BaseComponent.W*12/375}}>此商品{this.state.goodsDetail.sku&&this.state.goodsDetail.sku.logisticTemplate.logisticTemplateType!="FREE"?"":"包邮"}{this.state.goodsDetail.sku&&this.state.goodsDetail.sku.taxPrice>0?"":"包税"}</Text>
				</View>
				{/*满2减40*/}
				<View style={{flexDirection:'row',display:"none"}}>
					<View style={{height:BaseComponent.W*23/375,borderWidth:1,justifyContent:'center',alignItems:'center',
						borderRadius:BaseComponent.W*5/375,borderColor:'#8F20DE',marginTop:BaseComponent.W*10/375,marginLeft:BaseComponent.W*10/375}}>
						<Text style={{fontSize:BaseComponent.W*13/375,color:'#8F20DE',margin:5}}>满2减40</Text>
					</View>
					<Text style={{fontSize:BaseComponent.W*13/375,color:'#5A5A5A',marginLeft:BaseComponent.W*13/375,marginTop:BaseComponent.W*12/375}}>参加满2个商品立减40元活动</Text>
				</View>
				{/*满199送面膜*/}
				<View style={{flexDirection:'row',display:"none"}}>
					<View style={{height:BaseComponent.W*23/375,borderWidth:1,justifyContent:'center',alignItems:'center',
						borderRadius:BaseComponent.W*5/375,borderColor:'#8F20DE',marginTop:BaseComponent.W*10/375,marginLeft:BaseComponent.W*10/375}}>
						<Text style={{fontSize:BaseComponent.W*13/375,color:'#8F20DE',margin:5}}>满199送面膜</Text>
					</View>
					<Text style={{fontSize:BaseComponent.W*13/375,color:'#5A5A5A',marginLeft:BaseComponent.W*13/375,marginTop:BaseComponent.W*12/375}}>参加新用户满199送两张面膜活动</Text>
				</View>

                        
                        {/*保质期*/}
                        <View style={{flexDirection:'row',display:this.state.goodsDetail.sku&&this.state.goodsDetail.sku.spu.shelfLifeDate?"flex":"none"}}>
                              <View style={{justifyContent:'center',alignItems:'center',marginLeft:BaseComponent.W*15/375}}>
                                    <Text style={{fontSize:BaseComponent.W*15/375,color:'#555555',marginTop:BaseComponent.W*10/375}}>保质期</Text>
                              </View>
                              <View style={{justifyContent:'center',marginLeft:BaseComponent.W*15/375}}>
                                    <Text style={{fontSize:BaseComponent.W*15/375,color:'#555555',marginTop:BaseComponent.W*10/375}}>{this.state.goodsDetail.sku&&this.state.goodsDetail.sku.spu.shelfLifeDate?this.state.goodsDetail.sku.spu.shelfLifeDate:""}</Text>
                              </View>        
                        </View>

                        <View style={{width:'100%',height:0.5,backgroundColor:'#C2C1C1',marginLeft:BaseComponent.W*10/375,marginTop:BaseComponent.W*15/375}}></View>
                        {/*税费*/}
                        <View style={{flexDirection:'row'}}>
                              <View style={{width:BaseComponent.W*40/375,justifyContent:'center',alignItems:'center',marginLeft:BaseComponent.W*10/375}}>
                              <Text style={{fontSize:BaseComponent.W*15/375,color:'#555555',marginTop:BaseComponent.W*10/375}}>税费</Text>
                              </View>
                              <View style={{width:BaseComponent.W*315/375,justifyContent:'center',marginLeft:BaseComponent.W*10/375}}>
                              <Text style={{fontSize:BaseComponent.W*15/375,color:'#555555',marginTop:BaseComponent.W*10/375}}>{this.state.goodsDetail.sku&&this.state.goodsDetail.sku.taxPrice>0?"￥"+this.state.goodsDetail.sku.taxPrice:"包税"}</Text>
                              </View>        
                        </View>

				<View style={{width:'100%',height:0.5,backgroundColor:'#C2C1C1',marginLeft:BaseComponent.W*10/375,marginTop:BaseComponent.W*15/375}}></View>
				{/*运费*/}
				<View style={{flexDirection:'row'}}>
                              <View style={{width:BaseComponent.W*40/375,justifyContent:'center',alignItems:'center',marginLeft:BaseComponent.W*10/375}}>
					<Text style={{fontSize:BaseComponent.W*15/375,color:'#555555',marginTop:BaseComponent.W*10/375}}>运费</Text>
					</View>
                              <View style={{width:BaseComponent.W*315/375,justifyContent:'center',marginLeft:BaseComponent.W*10/375}}>
                              <Text style={{fontSize:BaseComponent.W*15/375,color:'#555555',marginTop:BaseComponent.W*10/375}}>{this.state.goodsDetail.sku&&this.state.goodsDetail.sku.logisticTemplate.summary?this.state.goodsDetail.sku.logisticTemplate.summary:""}</Text>
				      </View>        
                        </View>

				<View style={{width:'100%',height:0.5,backgroundColor:'#C2C1C1',marginLeft:BaseComponent.W*10/375,marginTop:BaseComponent.W*15/375}}></View>
				{/*说明*/}
				<View style={{flexDirection:'row',display:"none"}}>
					<Text style={{fontSize:BaseComponent.W*15/375,color:'#555555',marginLeft:BaseComponent.W*10/375,marginTop:BaseComponent.W*10/375}}>说明</Text>
					<Text style={{fontSize:BaseComponent.W*15/375,color:'#555555',marginLeft:BaseComponent.W*30/375,marginTop:BaseComponent.W*10/375}}>不支持7天无理由退货,境外直邮</Text>
				</View>

                        {/*视频播放*/}
                        <TouchableWithoutFeedback onPress={()=>{
                              console.log("-------------------------------全屏--------------------------------------","全屏")
                              thiz.setState({videoViseble:true});
                              // Orientation.lockToLandscape();
                              thiz.setState({smallScreenPaused:false})
                        }}>
                              <View style={{width:BaseComponent.W,height:200,display:thiz.state.videoViseble?'none':'none'}}>
                                    <Video source={{uri: "http://183.60.197.31/16/p/d/p/o/pdpocxixmtlhhrblafoutulwaglanj/hc.yinyuetai.com/A829016892782BC5F7C7905C3634EF01.mp4?sc=b6639eb24692a29e"}}
                                          ref='player'
                                          resizeMode="cover"  
                                          paused={thiz.state.smallScreenPaused}
                                          style={{width:BaseComponent.W,height:200}}/>
                              </View>
                        </TouchableWithoutFeedback>  

                        {/*图文详情*/}
				<View style={{width:'100%',backgroundColor:'#F0F0F0',marginTop:BaseComponent.W*10/375,flexDirection:"column"}}></View>
	                             
				     {this.getDetail()}
	
				</View>

				</ScrollView>
				{/*底部固定栏*/}
				<View style={{display:thiz.state.videoViseble?'none':'flex',position:thiz.state.videoViseble?'relative':'absolute',bottom:0,width:'100%',height:BaseComponent.W*49/375,flexDirection:'row',zIndex:10000,backgroundColor:'#ffffff'}}>
					{/*客服*/}
                              <TouchableWithoutFeedback onPress={()=>{this.toService()}}>
					<View style={{height:BaseComponent.W*49/375,width:BaseComponent.W*65/375,flexDirection:'column',justifyContent:'center',alignItems:'center'}}>
						<Image style={{width:BaseComponent.W*25/375,height:BaseComponent.W*25/375}} source={require('../../image/home/goodskefu.png')}></Image>
						<Text style={{fontSize:BaseComponent.W*10/375,color:'#929292'}}>客服</Text>
					</View>
                              </TouchableWithoutFeedback>
                              
					<View style={{width:0.5,height:BaseComponent.W*49/375,backgroundColor:'#DDDDDD'}}></View>

					{/*购物车*/}
                              <TouchableWithoutFeedback onPress={()=>{
                                    // 先判断是否需要登录
                                    thiz.isLogin(function(ret,err){
                                          if(ret){
                                                // 发消息通知购物车刷新
                                                thiz.emit("refresh_cart");
                                                // 延时跳转购物车页面
                                                setTimeout(function(){
                                                      BaseComponent.CUR_TAB = "Cart";
                                                      thiz.navigate("Cart");
                                                },100);
                                                
                                          }else{
                                                thiz.navigate('Login');
                                          }
                                    });
                              }}>
					<View style={{height:BaseComponent.W*49/375,width:BaseComponent.W*65/375,flexDirection:'column',justifyContent:'center',alignItems:'center'}}>
						<Image style={{width:BaseComponent.W*25/375,height:BaseComponent.W*25/375}} source={require('../../image/home/goodsshopcar.png')}></Image>
						<Text style={{fontSize:BaseComponent.W*10/375,color:'#929292'}}>购物车</Text>
					</View>
                              </TouchableWithoutFeedback>

                              <View style={{width:1,height:BaseComponent.W*49/375,backgroundColor:'#DDDDDD'}}></View>

					{/*加入购物车*/}
                              <TouchableWithoutFeedback onPress={()=>{
                                    // if(thiz.state.goodsDetail&&thiz.state.goodsDetail.sku.canSalesStockNum==0){
                                    //       return;
                                    // }
                                    // 如果是会员
                                    if(thiz.ismember){
                                          return;
                                    }
                                    thiz.setState({isvisible:true,isAddCart:true,});
                                    thiz.isSpecShow = true;
                                    thiz.myState.isAddCart = true;
                              }}>
					<View style={{width:BaseComponent.W*123/375,height:BaseComponent.W*49/375,backgroundColor:(function(){
                                    let color = '#3D3D3C';
                                    // if(thiz.state.goodsDetail&&thiz.state.goodsDetail.sku.canSalesStockNum==0){
                                    //       color = "#B3B3B3";
                                    // }
                                    // 如果是会员商品
                                    if(thiz.ismember){
                                          color = "#B3B3B3";
                                    }
                                    return color;
                              })(),justifyContent:'center',alignItems:'center',display:thiz.ismember?"none":"flex"}}>
						<Text style={{fontSize:BaseComponent.W*16/375,color:'#ffffff'}}>加入购物车</Text>
					</View>
                              </TouchableWithoutFeedback>
                              
					{/*立即购买*/}
                              <TouchableWithoutFeedback onPress={()=>{
                                    if(thiz.state.goodsDetail&&thiz.state.goodsDetail.sku.canSalesStockNum==0){
                                          return;
                                    }
                                    this.setState({isvisible:true,isAddCart:false});
                                    this.isSpecShow = true;
                                    this.myState.isAddCart = false;
                              }}>
					<View style={{width:thiz.ismember?BaseComponent.W*246/375:BaseComponent.W*123/375,height:BaseComponent.W*49/375,backgroundColor:(function(){
                                    let color = '#FDD17A';
                                    if(thiz.state.goodsDetail&&thiz.state.goodsDetail.sku&&thiz.state.goodsDetail.sku.canSalesStockNum==0){
                                          color = "#B3B3B3";
                                    }
                                    return color;
                              })(),justifyContent:'center',alignItems:'center'}}>
						<Text style={{fontSize:BaseComponent.W*16/375,color:'#727272'}}>立即购买</Text>
					</View>
                              </TouchableWithoutFeedback>
				</View>
			</View>	
		)
	}

	//改变当前轮播下标
	changeIndex=(index)=>{  
            let thiz = this;
            let state = thiz.state;
            console.log("---------------------index------------------------------",index);
            index++;
            state.SwiperIndex = index;
		this.setState(state);
	}

      //轮播图
      getBanner=(data)=>{
            var thiz=this;            
            return data.map((item,index)=>{
                  return (
                        <View>
                        {
                              item.video?(
                                    <View style={{width:BaseComponent.W,height:BaseComponent.W}}>
                              <Video source={{uri: item.video}}
                                     ref='player'
                                     resizeMode="cover"
                                     paused={thiz.state.bannerVideo} 
                                     style={{width:BaseComponent.W,height:BaseComponent.W}}
                                     />
                              <TouchableWithoutFeedback onPress={()=>{
                                    thiz.setState({bannerVideo:!thiz.state.bannerVideo});
                              }}>       
                                    <View style={{width:BaseComponent.W*40/375,height:BaseComponent.W*40/375,position:'absolute',bottom:0,left:BaseComponent.W*10/375}}>
                                          <Image style={{width:BaseComponent.W*25/375,height:BaseComponent.W*25/375}} source={thiz.state.bannerVideo?require('../../image/home/play.png'):require('../../image/home/stop.png')}/>
                                    </View>
                              </TouchableWithoutFeedback>             
                              </View>
                              ):(
                              <TouchableWithoutFeedback onPress={()=>{
                                    thiz.setState({SavePicVisible:true});
                                    var newData=[];
                                    var imageUrl=[];

                                    for(var ii=0;ii<data.length;ii++){
                                          //判断是否是视频地址，如果是，则跳过
                                          // if(data[ii].video){
                                          //       continue;
                                          // }
                                          newData.push(data[ii])      
                                    }
                                    thiz.log("---------------newData--------------------",newData);
                                    for(var iii=0;iii<newData.length;iii++){
                                          var image={};
                                          image.url=newData[iii].resourceFullAddress;
                                          imageUrl.push(image);
                                    }
                                    thiz.log("----------------------imageUrl----------------------------",imageUrl);
                                    thiz.setState({newData:newData,imageUrl:imageUrl});
                                    thiz.myState.saveIndex=index;
                              }}>
                                    <View>
                                          <Image style={{width:BaseComponent.W,height:BaseComponent.W}} source={{uri:item.resourceFullAddress}} resizeMode="cover">
                                          </Image>
                                    </View>
                              </TouchableWithoutFeedback>
                              )
                        }
                        </View>
                  )   
            });
      }
      //点击放大图片后可以保存
      getAmplificationBanner=(data)=>{
            var thiz=this;
            thiz.log("-----------------getAmplificationBanner-------------------------",data);
            return data.map((item,index)=>{
                  return (
                                    
                        <View style={{width:'100%'}}>  
                              <MyImage onload={(info)=>{
                                    thiz.log("--------info--------",info);
                              }} width={BaseComponent.W} url={item.resourceFullAddress} onPress={()=>thiz.setState({SavePicVisible:false})}>
                              </MyImage>        
                        </View>    
                        
                  )
            });
      }
}