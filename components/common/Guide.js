/**
 * @name Guide.js
 * @auhor 李磊
 * @date 2018.11.10
 * @desc 引导页面
 */
import React,{Component}from "react";
import {BackHandler,AsyncStorage,Text,WebView,StatusBar,View, Image, StyleSheet,ImageBackground,TouchableWithoutFeedback,Modal,ActivityIndicator} from 'react-native';

import BaseComponent from '../BaseComponent';

// 导入工具函数库
import T from "../../lib/Tools";
// 导入图片资源库
import Imgs from "../../lib/Img";

import Swiper from "react-native-swiper";

// Tools实例
let ti = T.getInstance();


export default class Guide extends BaseComponent{
    constructor(props){
        super(props);
        
        // 设置页面不可物理返回
        BaseComponent.canHardBack = false;

        this.state = {
            imgs:[
                require("../../image/guide_one.png"),
                require("../../image/guide_two.png"),
                require("../../image/guide_four.png"),
            ]
        };

    }
    
    render(){
        let thiz = this;
        return (
            <View style={{width:BaseComponent.W,height:BaseComponent.H}}>
                {/*轮播*/}
                <Swiper
                    ref="swiper"
                    style={{height:BaseComponent.H}}
                    height={BaseComponent.H}
                    autoplay={false}
                    loop={false}
                    horizontal={true}
                    dot={
                        <View style={{backgroundColor:'transparent',width:0,height:0,}}/>
                    }
                    activeDot={
                        <View style={{backgroundColor:'transparent',width:0,height:0,}}/>
                    }>

                    {
                        thiz.state.imgs.map(function(v,i){
                            return (
                                <TouchableWithoutFeedback onPress={()=>{
                                    thiz.log("--------click_Guide--------","click_Guide");
                                    if(i==thiz.state.imgs.length-1){
                                        // 开启物理返回
                                        BaseComponent.canHardBack = true;
                                        // 保存数据
                                        thiz.T.save("isFirst","NO");
                                        // 定时关闭
                                        setTimeout(function(){
                                            thiz.emit("launch_or_guide_over");
                                            if(BaseComponent.OS=="android"){
                                                thiz.navigate("Home");
                                            }
                                        },100);
                                    }
                                    
                                }}>
                                    <View>
                                      <Image style={{height:"100%",width:"100%"}} source={v}/>
                                    </View>
                                </TouchableWithoutFeedback>
                            );
                        })
                    }

                </Swiper>
            </View>
        )
    }
}