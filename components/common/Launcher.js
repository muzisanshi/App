/**
 * @name Launcher.js
 * @auhor 李磊
 * @date 2018.11.10
 * @desc 启动图页面
 */
import React,{Component}from "react";
import {Linking,AppRegistry,AsyncStorage,Text,WebView,StatusBar,View, Image, StyleSheet,ImageBackground,TouchableWithoutFeedback,Modal,ActivityIndicator} from 'react-native';

import BaseComponent from '../BaseComponent';

// 导入工具函数库
import T from "../../lib/Tools";
// 导入图片资源库
import Imgs from "../../lib/Img";


// Tools实例
let ti = T.getInstance();


export default class Launcher extends BaseComponent{
    constructor(props){
        super(props);
        BaseComponent.canHardBack = false;
    }

    componentDidMount(){
        let thiz = this;
        
        
        setTimeout(function(){
          
          thiz.T.load("isFirst",function(ret){
            if(!ret){
              thiz.navigate("Guide");
            }else{
              // 是否可以物理返回
              BaseComponent.canHardBack = true;
              // 发消息
              thiz.emit("launch_or_guide_over",{type:"launcher"});
              // 返回首页
              thiz.T.load('isFromWeb',function(ret){
                console.log("----------------------------------------------------------Launcher_ret-------------------------",ret);
                if(ret&&ret.isFromWeb=='yes'){
                  thiz.navigate('Register',{title:'注册'});
                  thiz.T.remove('isFromWeb');
                }else{
                  thiz.navigate("Home");
                }
              })
            }
          });
          
          
        },3000);
    }
    
    render(){
        return (
            <View style={{width:BaseComponent.W,height:BaseComponent.H}}>
                <Image source={require("../../image/launcher2.png")} style={{width:BaseComponent.W,height:BaseComponent.H}} resizeMode={"stretch"}/>
            </View>
        )
    }
}
