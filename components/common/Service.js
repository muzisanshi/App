/**
 * @name Service.js
 * @auhor 程浩
 * @date 2018.8.16
 * @desc 客服页面
 */
import React,{Component}from "react";
import {Text,WebView,StatusBar,View, Image, StyleSheet,ImageBackground,TouchableWithoutFeedback,Modal,ActivityIndicator} from 'react-native';

import BaseComponent from '../BaseComponent';

// 导入工具函数库
import T from "../../lib/Tools";
// 导入图片资源库
import Imgs from "../../lib/Img";

// 转菊花
import Spinner from 'react-native-loading-spinner-overlay';
import WebViewAndroid from 'react-native-webview-android';

// Tools实例
let ti = T.getInstance();

let style = StyleSheet.create({
  wrapper:{
    backgroundColor:"#fff",
    flex:1,
    flexDirection:"column",
    alignItems:"stretch",
    justifyContent:"flex-start",
    position:"relative",
  },
  statusBar:{
    backgroundColor:"#fff",
    height:ti.select({ios:16,android:22}),
    position:"relative",
    zIndex:1,
    opacity:1,
  },
  navBar:{
    backgroundColor:"#fff",
    height:"7%",
    position:"relative",
    zIndex:1,
    opacity:1,
  },
});

export default class Service extends BaseComponent{
    constructor(props){
        super(props);
        let thiz = this;
        this.state={
            visible:false,
            isvisible:false,
            url:"../pages/browser.html",
            // url:"http://192.168.2.103/browser.html",
            // url:"https://kefu.easemob.com/webim/im.html?configId=c3124d33-43cc-4887-89c0-6eb98f4bd725",
            // url:"https://www.baidu.com",
            userInfo:null,
        };
        this.onShouldStartLoadWithRequest = this.onShouldStartLoadWithRequest.bind(this);
        this.onNavigationStateChange = this.onNavigationStateChange.bind(this);
        this.onMessage = this.onMessage.bind(this);
        this.sendMessage = this.sendMessage.bind(this);

        this.onLoadStart = this.onLoadStart.bind(this);
        this.onError = this.onError.bind(this);
        this.onLoaded = this.onLoaded.bind(this);
        this.onLoadEnd = this.onLoadEnd.bind(this);

        // 加载用户信息
        this.getAccount(function(ret,err){
          if(ret){
            thiz.setState({
              userInfo:ret.userInfo
            });
          }
        });
    }

    componentDidMount(){
        let thiz = this;
    }

    /**
     * android回调区域
     */
    // 加载状态的改变(android)
    onNavigationStateChange(state){
      let thiz = this;
      thiz.log("--------android_onNavigationStateChange--------",state);
    }

    /**
     * ios回调区域
     */
    // 加载开始(ios)
    onLoadStart(state){
      let thiz = this;
      thiz.log("--------ios_onLoadStart--------","ios_onLoadStart");
    }
    // 加载失败(ios)
    onError(state){
      let thiz = this;
      thiz.log("--------ios_onError--------","ios_onError");
    }
    // 加载成功(ios) 
    onLoaded(state){
      let thiz = this;
      thiz.log("--------ios_onLoaded--------","ios_onLoaded");

    }
    // 加载完毕，成功或者失败(ios)
    onLoadEnd(state){
      let thiz = this;
      thiz.log("--------ios_onLoadEnd--------","ios_onLoadEnd");
    }


    /**
     * 公用函数回调区域
     */
    // 加载页面前的回调(android/ios)
    onShouldStartLoadWithRequest(state){
      let thiz = this;
      thiz.log("--------onShouldStartLoadWithRequest--------",state);
      return true;
    }
    // 收到webview发送的消息(android/ios)
    // android:   window.webView.postMessage(msg);
    // ios:       window.postMessage(msg);
    onMessage(msg){
      let thiz = this;
      thiz.log("--------onMessage--------","onMessage");
      if(msg){
        var message = {};
        if(BaseComponent.OS=="android"){
          message = JSON.parse(msg.message);
        }
        if(BaseComponent.OS=="ios"){
          message = JSON.parse(msg.nativeEvent.data);
        }
        thiz.log("--------onMessage--------",message);
        if(message&&message.type=="chat_loaded"){// 判断聊天窗口是否加载完毕
          // thiz.toast("收到webview的消息");
          // 客服页面加载完毕，删除环信图标
          thiz.exeJS('\
            var icont = $(".easemobim-chat-panel").contents();\
            icont.find(".powered-by-link").remove();\
            icont.find(".btn-keyboard").remove();\
          ');
          // // 发送额外信息
          // thiz.exeJS('easemobim.sendExt({'+
          //     'ext:{'+
          //         '"imageName": "mallImage3.png",'+
          //         '"type": "custom",'+
          //         '"msgtype": {'+
          //             '"track": {'+
          //                 '"title": "我正在看",'+
          //                 '"price": "¥:235.00",'+
          //                 '"desc": "女装小香风气质蕾丝假两件短袖",'+
          //                 '"img_url":"http://img5.imgtn.bdimg.com/it/u=3630891488,1766843887&fm=26&gp=0.jpg",'+
          //                 '"item_url": "https://www.baidu.com",'+
          //             '}'+
          //         '}'+
          //     '}'+
          // '});');
          
        }
      }
    }
    // 发送消息给webview(android/ios)
    // android上面发消息，webview接收有问题，现统一android和ios用exeJS代发消息
    sendMessage(msg){
      let thiz = this;
      thiz.log("--------sendMessage--------",msg);
      // if(thiz.refs.webBrowser&&msg){
      //   thiz.refs.webBrowser.postMessage(msg);
      // }
      thiz.exeJS('window.onMessage("'+msg+'")');
    }
    // 执行js脚本(android/ios)
    exeJS(js){
      let thiz = this;
      thiz.log("--------exeJS--------",js);
      if(thiz.refs.webBrowser&&js){
        thiz.log("--------thiz.refs.webBrowser--------","存在");
        // 执行脚本
        thiz.refs.webBrowser.injectJavaScript(js);
      }
    }

    // 构建webview
    getWebView(userInfo){
      let thiz = this;
      let final = null;
      let htmlTpl = '<!DOCTYPE html>\
                  <html>\
                  <head><title>客服</title><meta charset="utf-8">\
                  <meta name="viewport" content="maximum-scale=1.0,minimum-scale=1.0,user-scalable=0,initial-scale=1.0,width=device-width" />\
                  <meta name="format-detection" content="telephone=no,email=no,date=no,address=no"></head>\
                  <body>\
                  </body>\
                  <script src="https://cdn.bootcss.com/jquery/3.1.1/jquery.min.js"></script>\
                  <script src="http://kefu.easemob.com/webim/easemob.js"></script>\
                  <script type="text/javascript">\
                    $(function(){\
                      window.OS = "'+BaseComponent.OS+'";\
                      window.sendMessage = function(msg){\
                        if(window.OS=="android"){\
                          window.webView.postMessage(JSON.stringify(msg));\
                        }else{\
                          window.postMessage(JSON.stringify(msg));\
                        }\
                      };\
                      /*监听RN传过来的消息*/\
                      window.onMessage = function(msg){\
                      };\
                      window.easemobim = window.easemobim || {};\
                      easemobim.config = {\
                          hide: true,\
                          autoConnect: true,\
                          visitor: {\
                              trueName:"'+(userInfo&&userInfo.phoneNo?userInfo.phoneNo:'')+'",\
                              weixin:"'+(userInfo&&userInfo.wxNo?userInfo.wxNo:'')+'",\
                              phone:"'+(userInfo&&userInfo.phoneNo?userInfo.phoneNo:'')+'",\
                              companyName:"",\
                              userNickname:"'+(userInfo&&userInfo.nickname?userInfo.nickname:'')+'",\
                              description:"",\
                              email:"'+(userInfo&&userInfo.email?userInfo.email:'')+'"\
                          },\
                          onready:function(){\
                            /*聊天窗口加载完毕回调*/\
                            var msg = {\
                              type:"chat_loaded"\
                            };\
                            /*通知RN客服页面加载完毕*/\
                            window.sendMessage(msg);\
                          },\
                          onmessage:function(){\
                            \
                          },\
                          onsessionclosed:function(){\
                            \
                          }\
                      };\
                      easemobim.bind({configId: "c3124d33-43cc-4887-89c0-6eb98f4bd725"});\
                    });\
                  </script>\
                  </html>';

      if(userInfo){

        let android = (
          <WebViewAndroid
            ref="webBrowser"
            javaScriptEnabled={true}
            geolocationEnabled={true}
            builtInZoomControls={false}
            onShouldStartLoadWithRequest={thiz.onShouldStartLoadWithRequest}
            onNavigationStateChange={thiz.onNavigationStateChange}
            onMessage={thiz.onMessage}
            html={htmlTpl}
            style={{flex:1,}} />
        );

        let ios = (<WebView
            ref="webBrowser"
            originWhitelist={['*']}
            source={{html:htmlTpl}}
            style={{width:'100%',height:'100%'}}
            onShouldStartLoadWithRequest={thiz.onShouldStartLoadWithRequest}
            onLoadStart={thiz.onLoadStart}
            onLoadEnd={thiz.onLoadEnd}
            onLoad={thiz.onLoaded}
            onError={thiz.onError}
            onMessage={thiz.onMessage}
            // 注入脚本，解决webview的bug
            injectedJavaScript={'(function() {'+
                'var originalPostMessage = window.postMessage;'+

                'var patchedPostMessage = function(message, targetOrigin, transfer) {'+
                  'originalPostMessage(message, targetOrigin, transfer);'+
                '};'+

                'patchedPostMessage.toString = function() {'+ 
                  'return String(Object.hasOwnProperty).replace("hasOwnProperty", "postMessage");'+ 
                '};'+

                'window.postMessage = patchedPostMessage;'+
              '})();'
            }
            />);
        
        final = BaseComponent.OS=="ios"?ios:android;
      }
      
      return final;
    }
    
    render(){
        let thiz = this;
        return (
            <View style={style.wrapper}>
                {/*转菊花*/}
                <Modal
                  visible={this.state.visible}
                  onRequestClose={()=>{this.setState({visible:false})}}
                  transparent={true}
                  animationType={"fade"}>
                  <View style={{backgroundColor:"rgba(0,0,0,0.4)",width:"100%",width:"100%",flex:1,flexDirection:"column",alignItems:"center",justifyContent:"center",position:"relative"}}>
                    <View>
                          <ActivityIndicator size="large" color='white'/>
                    </View>
                  </View>
                </Modal>
                
                {/*状态栏占位*/}
                <View style={{backgroundColor:this.state.isvisible?'rgba(14,14,14,0.5)':"#fff",
                              height:ti.select({ios:16,android:StatusBar.currentHeight}),
                              position:"relative",
                              zIndex:1,
                              opacity:1,}}></View>
                {/*顶部导航栏*/}
                <View style={style.navBar}>
                  <View style={{flex:1,flexDirection:"row",justifyContent:"flex-start",alignItems:'center'}}>

                    <TouchableWithoutFeedback onPress={()=>this.goBack()}>
                    <View style={{width:"15%",height:"100%",backgroundColor:"transparent",justifyContent:"center",marginLeft:BaseComponent.W*10/375}}>
                      <Image source={Imgs.back} style={{width:(BaseComponent.W*0.127*0.4),height:(BaseComponent.W*0.127*0.4),marginTop:ti.select({ios:BaseComponent.W*0.02,android:0})}}></Image>
                    </View>
                    </TouchableWithoutFeedback>

                    <View style={{flexGrow:1,backgroundColor:"transparent",borderWidth:0,}}>
                      <Text style={{color:"#0A0A0A",width:"100%",textAlign:"center",marginTop:ti.select({ios:"4%",android:"1.6%"}),fontSize:BaseComponent.W*18/375}}>{this.params.title}</Text>
                    </View>

                    <TouchableWithoutFeedback>  
                    <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center",}}>
                        <Text style={{fontSize:BaseComponent.W*0.037,color:'black',opacity:0}}>保存</Text>
                    </View>
                    </TouchableWithoutFeedback>
                  </View>
                </View>

                {/*浏览器组件*/}
                {this.getWebView(thiz.state.userInfo?thiz.state.userInfo:null)}
                
            </View>
        )
    }
}