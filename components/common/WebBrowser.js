/**
 * @name WebBrowser.js
 * @auhor 程浩
 * @date 2018.8.16
 * @desc 浏览器页面
 */
import React,{Component}from "react";
import {Text,WebView,StatusBar,View, Image, StyleSheet,ImageBackground,TouchableWithoutFeedback,Modal,ActivityIndicator,CameraRoll} from 'react-native';

import BaseComponent from '../BaseComponent';

// 导入工具函数库
import T from "../../lib/Tools";
// 导入图片资源库
import Imgs from "../../lib/Img";

// 转菊花
import Spinner from 'react-native-loading-spinner-overlay';
import WebViewAndroid from 'react-native-webview-android';

// 测试保存base64图片
import base64Save from "../../lib/SaveBase64ImageToCameraRoll";
const RNFS = require('react-native-fs'); //文件处理

import ImagePicker from 'react-native-image-crop-picker'; 

import Wxpay from '@yyyyu/react-native-wechat';

import Downloader from 'react-native-background-downloader';



// Tools实例
let ti = T.getInstance();

let style = StyleSheet.create({
  wrapper:{
    backgroundColor:"#fff",
    flex:1,
    flexDirection:"column",
    alignItems:"stretch",
    justifyContent:"flex-start",
    position:"relative",
  },
  statusBar:{
    backgroundColor:"#fff",
    height:ti.select({ios:16,android:22}),
    position:"relative",
    zIndex:1,
    opacity:1,
  },
});

export default class WebBrowser extends BaseComponent{
    constructor(props){
        super(props);
        this.state={
            visible:false,
            isvisible:false,
            // url:"../pages/browser.html",
            // url:"http://192.168.2.103/browser.html",
            // url:"https://kefu.easemob.com/webim/im.html?configId=12dd36aa-eb6c-49e1-9850-debe8547009f",
            url:this.params.url?this.params.url:"",
        };
        this.onShouldStartLoadWithRequest = this.onShouldStartLoadWithRequest.bind(this);
        this.onNavigationStateChange = this.onNavigationStateChange.bind(this);
        this.onMessage = this.onMessage.bind(this);
    }

    componentDidMount(){
        let thiz = this;
        // thiz.toast("WebBrowser");
    }

    onShouldStartLoadWithRequest(){
      let thiz = this;
      thiz.log("--------onShouldStartLoadWithRequest--------","onShouldStartLoadWithRequest");
    }
    
    onNavigationStateChange(){
      let thiz = this;
      thiz.log("--------onNavigationStateChange--------","onNavigationStateChange");
    }

    saveImg(data){
      let thiz = this;
      // 把base64转换为图片并保存
      if(base64Save&&data){
        if(BaseComponent.OS=="android"){
          thiz.T.requestAndroidPermission("WRITE_EXTERNAL_STORAGE",function(){
            base64Save(data,function(){
              thiz.toast("保存成功");
            },function(){
              thiz.toast("保存失败");
            });
            
          });
        }else{
          base64Save(data,function(){
            thiz.toast("保存成功");
          },function(){
            thiz.toast("保存失败");
          });
        }
      }

    }

    // 下载图片并保存（暂时弃用，兼容性不好）
    downImg(url){
      let thiz = this;
      if(url){
          if(BaseComponent.OS=='ios'){
                CameraRoll.saveToCameraRoll(url);
                thiz.toast("保存成功");
          }else{
               thiz.T.requestAndroidPermission("WRITE_EXTERNAL_STORAGE",function(){
                      const storeLocation = `${RNFS.ExternalDirectoryPath}`;
                      let pathName = new Date().getTime() + "savePic.png"
                      let downloadDest = `${storeLocation}/${pathName}`;
                      console.log("---------------------------downloadDest---------------------------",downloadDest);

                      const ret = RNFS.downloadFile({fromUrl:url,toFile:downloadDest});
                      ret.promise.then(res => {

                        thiz.log("--------downImg_ret--------",res);
                        if(res && res.statusCode === 200){

                            var promise = CameraRoll.saveToCameraRoll(downloadDest);
                            promise.then(function(result) {
                               thiz.toast("保存成功");
                            }).catch(function(error) {
                                thiz.log("---------------error--------------",error);
                                thiz.toast("保存失败");
                            })

                        }
                      }).catch(function(err){
                        thiz.log("---------------downErr--------------",err);
                      })
               });     
          } 
      }
    }

    downImg2(url){

      let thiz = this;

      if(BaseComponent.OS=='ios'){
            CameraRoll.saveToCameraRoll(url);
            thiz.toast("保存成功");
      }else{
        // 有的手机需要请求权限 WRITE_EXTERNAL_STORAGE REQUEST_INSTALL_PACKAGES
        thiz.T.requestAndroidPermission("WRITE_EXTERNAL_STORAGE",function(ret){

          if(Downloader&&url){

            const storeLocation = `${RNFS.ExternalDirectoryPath}`;
            let pathName = new Date().getTime() + "savePic.png"
            let downloadDest = `${storeLocation}/${pathName}`;

            thiz.log("--------destination--------",downloadDest);


            let task = Downloader.download({
              id: 'file'+(new Date()).getTime()/1000,
              url: url,
              destination: downloadDest
            }).begin((expectedBytes) => {
              
            }).progress((percent) => {
              
            }).done(() => {
              thiz.log("--------save_success--------","保存成功");

              var promise = CameraRoll.saveToCameraRoll(downloadDest);
              promise.then(function(result) {
                  thiz.log("---------------result--------------",result);
                  thiz.toast("保存成功");
              }).catch(function(error) {
                  thiz.log("---------------error--------------",error);
                  thiz.toast("保存失败");
              })

            }).error((error) => {
              thiz.log("--------save_err--------","保存失败");
              thiz.toast("保存失败");
            });


          }

        });
      }

    }

    exeJS(script){
      let thiz = this;
      if(thiz.refs.webview){
        thiz.refs.webview.injectJavaScript(script);
      }
    }

    openAlbum(){
      console.log("openAlbum");
      let thiz = this;
      setTimeout(function(){
        ImagePicker.openPicker({  
          width: 300,  
          height: 400,  
          cropping:false,
          compressImageQuality:0.5,
      }).then(function(ret){
        thiz.log("--------addImg--------",ret);
        if(ret&&ret.path){
          thiz.log("--------ret.path--------","ret.path");
          thiz.exeJS("window.onGetImg('"+ret.path+"')");

          // 开始上传
          let url = ret.path;
          if(url&&BaseComponent.OS=="ios"){
            url = "file:///"+url;
          }
          thiz.upload(url,{
            progress:function(pro){
              
            },
            success:function(ret){
              thiz.toast("上传成功");
              thiz.exeJS("window.onUploadImg('"+JSON.stringify(ret.data)+"')");
            },
            error:function(err){
              setTimeout(function(){
                thiz.setState({
                  visible:false,
                });
              },100);
            }

          });

        }
      });
      },500);
    }

    share(json){
      let thiz = this;
      // 分享操作，暂时没法分享多张图片
      Wxpay.sendImage({
        image:json.url[0],
        scene:json.type,
      });
    }

    onMessage(e){
      let thiz = this;
      let data = e.nativeEvent.data;
      if(data){
        let json = JSON.parse(data);

        if(json&&json.op=="share"){
          thiz.share(json);
          return;
        }

        if(json&&json.op=="saveImg"){
          thiz.saveImg(json.data);
          return;
        }

        if(json&&json.op=="downImg"){

          if(json.url instanceof Array){

            for(let i=0;i<json.url.length;i++){ 
              setTimeout(function(){
                thiz.downImg2(json.url[i]);
              },200);
            }

          }else{
            thiz.downImg2(json.url);
          }

          return;
        }

        if(json&&json.op=="getImg"){
          thiz.openAlbum();
          return;
        }

        if(json&&json.op=="closeWebView"){
          // thiz.toast("webview监听到关闭消息");
          thiz.goBack();
        }

        if(json&&json.op=="withdrawSuccess"){
          thiz.emit("withdrawSuccess");
        }

      }
    }



    getWebView(){
      let thiz = this;
      // let android = (
      //   <WebViewAndroid
      //     ref="webview"
      //     javaScriptEnabled={true}
      //     geolocationEnabled={true}
      //     builtInZoomControls={false}
      //     onShouldStartLoadWithRequest={thiz.onShouldStartLoadWithRequest}
      //     onNavigationStateChange={thiz.onNavigationStateChange}
      //     onMessage={thiz.onMessage}
      //     url={thiz.state.url}
      //     style={{flex:1,}} />
      // );

      let android = (<WebView ref="webview" source={{uri:thiz.state.url}} style={{width:'100%',height:'100%'}} onMessage={thiz.onMessage}/>);
      let ios = (<WebView ref="webview" source={{uri:thiz.state.url}} style={{width:'100%',height:'100%'}} onMessage={thiz.onMessage}/>);
      let final = BaseComponent.OS=="ios"?ios:android;
      return final;
    }
    
    render(){
        let thiz = this;
        return (
            <View style={style.wrapper}>
                {/*转菊花*/}
                <Modal
                  visible={this.state.visible}
                  onRequestClose={()=>{this.setState({visible:false})}}
                  transparent={true}
                  animationType={"fade"}>
                  <View style={{backgroundColor:"rgba(0,0,0,0.4)",width:"100%",width:"100%",flex:1,flexDirection:"column",alignItems:"center",justifyContent:"center",position:"relative"}}>
                    <View>
                          <ActivityIndicator size="large" color='white'/>
                    </View>
                  </View>
                </Modal>
                
                {/*状态栏占位*/}
                {/*<View style={{backgroundColor:this.state.isvisible?'rgba(14,14,14,0.5)':"#fff",
                              height:ti.select({ios:16,android:StatusBar.currentHeight}),
                              position:"relative",
                              zIndex:1,
                              opacity:1,}}></View>*/}

                {/*顶部导航栏*/}
                <View style={{backgroundColor:"#fff",height:"7%",position:"relative",zIndex:1,opacity:1,display:thiz.params.showNav?"block":"none"}}>
                  <View style={{flex:1,flexDirection:"row",justifyContent:"flex-start",alignItems:'center'}}>

                    <TouchableWithoutFeedback onPress={()=>this.goBack()}>
                    <View style={{width:"15%",height:"100%",backgroundColor:"transparent",justifyContent:"center",marginLeft:BaseComponent.W*10/375}}>
                      <Image source={Imgs.back} style={{width:(BaseComponent.W*0.127*0.4),height:(BaseComponent.W*0.127*0.4),marginTop:ti.select({ios:BaseComponent.W*0.02,android:0})}}></Image>
                    </View>
                    </TouchableWithoutFeedback>

                    <View style={{flexGrow:1,backgroundColor:"transparent",borderWidth:0,}}>
                      <Text style={{color:"#0A0A0A",width:"100%",textAlign:"center",marginTop:ti.select({ios:"4%",android:"1.6%"}),fontSize:BaseComponent.W*18/375}}>{this.params.title}</Text>
                    </View>

                    <TouchableWithoutFeedback>  
                    <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center",}}>
                        <Text style={{fontSize:BaseComponent.W*0.037,color:'black',opacity:0}}>保存</Text>
                    </View>
                    </TouchableWithoutFeedback>
                  </View>
                </View>

                {/*浏览器组件*/}
                {this.getWebView()}
                
            </View>
        )
    }
}
