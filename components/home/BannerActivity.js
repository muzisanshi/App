
/**
 * @name BannerActivity.js
 * @auhor 李磊
 * @date 2018.8.22
 * @desc banner活动页面
 */

import React, {Component} from "react";
import {Platform, StyleSheet, View,Text,Button,Image,TouchableWithoutFeedback} from "react-native";
import {StatusBar,ScrollView} from "react-native";
// 导入工具函数库
import T from "../../lib/Tools";
import BaseComponent from "../BaseComponent";
// 导入公共样式
import St from "../../lib/Style";
// 导入图片资源库
import Imgs from "../../lib/Img";

// Tools实例
let ti = T.getInstance();

let style = StyleSheet.create({
  wrapper:{
    backgroundColor:"white",
    flex:1,
    flexDirection:"column",
    alignItems:"stretch",
    justifyContent:"flex-start",
    position:"relative",
  },
  statusBar:{
    backgroundColor:"rgb(255,255,255)",
    height:ti.select({ios:16,android:22}),
    position:"relative",
    zIndex:1,
    opacity:1,
  },
  navBar:{
    backgroundColor:"rgb(255,255,255)",
    height:"7%",
    position:"relative",
    zIndex:1,
    opacity:1,
    borderBottomWidth:1,
    borderBottomColor:"rgb(241,241,241)"
  },
});


export default class BannerActivity extends BaseComponent{

  goods = [
    {
      img:require("../../image/home/aptamil1.png"),
      name:"Dior/迪奥小姐花漾淡香氛30ml50ml100ml玫瑰甜心香水女士",
      price:"618",
      bprice:"760",
      cartLogo:Imgs.shoppingCart,
    },
    {
      img:require("../../image/home/aptamil1.png"),
      name:"Dior/迪奥小姐花漾淡香氛30ml50ml100ml玫瑰甜心香水女士",
      price:"618",
      bprice:"760",
      cartLogo:Imgs.shoppingCart,
    },
    {
      img:require("../../image/home/aptamil1.png"),
      name:"Dior/迪奥小姐花漾淡香氛30ml50ml100ml玫瑰甜心香水女士",
      price:"618",
      bprice:"760",
      cartLogo:Imgs.shoppingCart,
    },
    {
      img:require("../../image/home/aptamil1.png"),
      name:"Dior/迪奥小姐花漾淡香氛30ml50ml100ml玫瑰甜心香水女士",
      price:"618",
      bprice:"760",
      cartLogo:Imgs.shoppingCart,
    },
    {
      img:require("../../image/home/aptamil1.png"),
      name:"Dior/迪奥小姐花漾淡香氛30ml50ml100ml玫瑰甜心香水女士",
      price:"618",
      bprice:"760",
      cartLogo:Imgs.shoppingCart,
    }
  ];


  constructor(props){
    super(props);

  }

  goodsTpl(data){
    return data.map(function(value,index){  
      return (
        <View style={{width:(BaseComponent.W/2-18),marginLeft:(index%2==0?0:8),marginRight:(index%2==0?8:0),marginBottom:18,}}>
          <Image source={value.img} style={{width:"100%",borderWidth:1,borderColor:"rgb(241,241,241)",height:(BaseComponent.W/2-20),}}/>
          <Text numberOfLines={2} style={{marginTop:10}}>
            Dior/迪奥小姐花漾淡香氛30ml50ml100ml玫瑰甜心香水女士
          </Text>
          <View style={{flexDirection:"row",marginTop:10,position:"relative"}}>
            <Text style={{color:"#FF407E",position:"relative",backgroundColor:"transparent",width:"100%"}}>￥799<Text style={{color:"#999999",fontSize:9,}}>&nbsp;&nbsp;</Text><Text style={{color:"#999999",fontSize:9,textDecorationLine:'line-through'}}>￥960</Text></Text>
            <Image source={Imgs.shoppingCart} style={{width:26,height:26,position:"absolute",right:0,}}/>
          </View>
        </View>
      );
    });
  }

  render() {
    return (
      <View style={style.wrapper}>
        {/*状态栏占位*/}
        <View style={style.statusBar}></View>
        {/*顶部导航栏*/}
        <View style={style.navBar}>
          <View style={{flex:1,flexDirection:"row",justifyContent:"flex-start"}}>

            <TouchableWithoutFeedback onPress={()=>this.goBack()}>
            <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>
              <Image source={Imgs.back} style={{width:(BaseComponent.W*0.127*0.4),height:(BaseComponent.W*0.127*0.4),marginTop:ti.select({ios:BaseComponent.W*0.02,android:0}),marginRight:BaseComponent.W*0.04}}></Image>
            </View>
            </TouchableWithoutFeedback>

            <View style={{flexGrow:1,backgroundColor:"transparent",
              marginTop:ti.select({ios:(BaseComponent.H*0.02)/2,android:(BaseComponent.H*0.016)/2}),borderRadius:ti.select({ios:(BaseComponent.H*0.05)/2,android:(BaseComponent.H*0.054)/2}),borderColor:"#0A0A0A",
              borderWidth:0,}}>
              <Text style={{color:"#0A0A0A",width:"100%",textAlign:"center",marginTop:ti.select({ios:"4%",android:"1.6%"}),fontSize:17}}>{this.params.title}</Text>
            </View>
            <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>
              <Image source={require('../../image/home/msg.png')} style={{width:(BaseComponent.W*0.127*0.5),height:(BaseComponent.W*0.127*0.5),display:"none"}}></Image>
            </View>

          </View>
        </View>

        <ScrollView showsVerticalScrollIndicator={false} style={{flex:1}}>
          {/*主要内容*/}
          <View>
            {/*描述区域*/}
            <View>
              <Image source={require("../../image/temp/img1.png")} style={{width:"100%"}}/>
              <View style={{width:(BaseComponent.W),padding:10,}}>
                <Image source={require("../../image/temp/img2.png")} style={{width:"100%",borderRadius:5}}/>
              </View>
              <View style={{width:(BaseComponent.W),padding:10,}}>
                <Image source={require("../../image/temp/img3.png")} style={{width:"100%",}}/>
              </View>
            </View>
            {/*商品列表*/}
            <View style={{flexDirection:"row",alignItems:"flex-start",justifyContent:"flex-start",flexWrap: "wrap",padding:10,}}>
              {this.goodsTpl(this.goods)}
            </View>
          </View>
        </ScrollView>

      </View>
    );
  }

}
