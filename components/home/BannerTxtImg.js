
/**
 * @name BannerTxtImg.js
 * @auhor 李磊
 * @date 2018.8.20
 * @desc banner图文页面
 */

import React, {Component} from "react";
import {Platform,Modal, StyleSheet, View,Text,Button,Image,TouchableWithoutFeedback,ActivityIndicator} from "react-native";
import {StatusBar,ScrollView} from "react-native";
// 导入工具函数库
import T from "../../lib/Tools";
import BaseComponent from "../BaseComponent";
// 导入公共样式
import St from "../../lib/Style";
// 导入图片资源库
import Imgs from "../../lib/Img";

// 转菊花
import Spinner from 'react-native-loading-spinner-overlay';

import MyImage from "../../lib/MyImage";

// Tools实例
let ti = T.getInstance();

let style = StyleSheet.create({
  wrapper:{
    backgroundColor:"#F0F0F0",
    flex:1,
    flexDirection:"column",
    alignItems:"stretch",
    justifyContent:"flex-start",
    position:"relative",
  },
  statusBar:{
    backgroundColor:"rgb(255,255,255)",
    height:ti.select({ios:16,android:22}),
    position:"relative",
    zIndex:1,
    opacity:1,
  },
  navBar:{
    backgroundColor:"rgb(255,255,255)",
    height:"7%",
    position:"relative",
    zIndex:1,
    opacity:1,
    borderBottomWidth:1,
    borderBottomColor:"rgb(241,241,241)"
  },
});


export default class BannerTxtImg extends BaseComponent{

  constructor(props){
    super(props);
    this.state = {
      data:this.params.data,
      visible:false,
      hasNext:false,
      imgs:{},// 所有图片
      hotGoods:{},// 商品列表
    };
    this.hotCurPage = 0;
    this.isHotLoading = false;
  }

  // 渲染图片
  getImgs(imgs){
    let thiz = this;
    if(imgs&&imgs.length>0){
      return imgs.map(function(value,index){
        return (
          <MyImage url={value.resourceFullAddress} width={BaseComponent.W}/>
        );
      });
    }
  }

  // 跳转到确认订单页面
  goBuy(value){
    let thiz = this;
    thiz.log("--------goBuy--------",value);
    thiz.setState({
          visible:true
    });
    thiz.request("shoppingCar/buyNow",{
          method:"POST",
          data:{
                num:1,
                skuId:value.id,
          },
          success:function(ret){
                
                if(ret.respCode=="00"){

                      // 调settleConfirm
                      thiz.request("shoppingCar/settleConfirm",{
                            method:"POST",
                            data:{
                                  num:1,
                                  skuId:value.id,
                            },
                            success:function(ret){
                                  thiz.setState({
                                        visible:false
                                  });
                                  if(ret.respCode=="00"){
                                        thiz.log("--------settleConfirm_success------","settleConfirm_success");

                                        // 判断有没有收货地址
                                        if(ret.data.receiverAddresses.length==0){
                                          thiz.navigate("AddAddress",{title:"添加地址"});
                                          return;
                                        }

                                        // 跳转到确认订单页面
                                        let finalData = ret.data;
                                        

                                        // 过滤收货地址
                                        let count = 0;
                                        for(let i=0;i<ret.data.receiverAddresses.length;i++){
                                          if(ret.data.receiverAddresses[i].defaultAddress){
                                              count++;
                                              finalData.defaultAddress = ret.data.receiverAddresses[i];
                                            break;
                                          }
                                        }

                                        if(count==0){
                                          finalData.defaultAddress = ret.data.receiverAddresses[0];
                                        }


                                        // 处理电话号码
                                        let phone = finalData.defaultAddress.receiverPhoneNo;
                                        if(phone){
                                          finalData.defaultAddress.receiverPhoneNo = phone.substring(0,3)+"****"+phone.substring(phone.length-4);
                                        }
                                        // 过滤商品图片
                                        let imgs = [];
                                        let goodsNum = 0;

                                        for(let i=0;i<ret.data.shoppingCarGroupItems.length;i++){
                                          let item = ret.data.shoppingCarGroupItems[i];
                                          for(let ii=0;ii<item.shoppingCarItems.length;ii++){
                                            // 判断商品是否处于选中状态
                                            if(item.shoppingCarItems[ii].checked){
                                              imgs.push(item.shoppingCarItems[ii].sku.imageAttachment.resourceFullAddress);
                                              goodsNum += item.shoppingCarItems[ii].num;
                                            }
                                          }
                                        }

                                        finalData.imgs = imgs;
                                        finalData.goodsNum = goodsNum;
                                        thiz.goPage("Querendingdan",{title:"确认订单",data:finalData});

                                  }
                            },
                            error:function(err){
                                  thiz.toast(err.msg);
                                  thiz.setState({
                                        visible:false,
                                  });
                            }
                      });

                }
          },
          error:function(err){
                thiz.toast(err.msg);
                thiz.setState({
                      visible:false,
                });
          }
    });
  }

  // 渲染商品
  getGoods(goods){
    let thiz = this;
    if(goods&&goods.length>0){
      return goods.map(function(value,index){
        thiz.log("--------buyUnitPrice--------",value.buyUnitPrice);
        return (
          <TouchableWithoutFeedback onPress={()=>{thiz.goGoodsDetail(value)}}>
          <View style={{padding:10,flex:1,flexDirection:"row",justifyContent:"flex-start",backgroundColor:"white"}}>
            <Image source={{uri:value.attachment.resourceFullAddress}} style={{width:BaseComponent.W*0.3,height:BaseComponent.W*0.3,borderWidth:1,borderColor:"rgb(241,241,241)"}}/>
            <View style={{position:"relative",height:"100%",backgroundColor:"transparent",width:(BaseComponent.W*0.7-20),paddingLeft:15}}>
              <View>
                <Text style={{fontSize:14,color:"#0A0A0A",lineHeight:20,}}>{value.name}</Text>
                <Text style={{fontSize:12,color:"#0A0A0A",lineHeight:20,}}></Text>
              </View>
              <View style={{position:"absolute",bottom:0,left:0,width:"100%",flex:1,flexDirection:"row",paddingLeft:15}}>
                <Text style={{color:"#FF407E",position:"relative",backgroundColor:"transparent",width:"100%"}}>￥{value.buyUnitPrice.toFixed(2)}<Text style={{color:"#999999",fontSize:9,}}>&nbsp;&nbsp;</Text>
                  {/*<Text style={{color:"#999999",fontSize:9,textDecorationLine:'line-through'}}>￥{value.marketPrice.toFixed(2)}</Text>*/}
                </Text>

                {/*<TouchableWithoutFeedback onPress={()=>{thiz.goBuy(value)}}>
                <Text style={{borderWidth:1,borderColor:"#FF407E",paddingTop:2,paddingRight:8,paddingBottom:2,paddingLeft:8,borderRadius:ti.select({ios:BaseComponent.W*0.022,android:BaseComponent.W*0.032}),fontSize:12,color:"#FF407E",position:"absolute",right:-10,bottom:3,}}>立即购买</Text>
                </TouchableWithoutFeedback>*/}
              </View>
            </View>
          </View>
          </TouchableWithoutFeedback>
        );
      });
    }
  }

  // 滚动回调
  onScroll(e){
    let thiz = this;
    console.log("------------------pageNumber-------------------------",thiz.hotCurPage+1);
    thiz.log("--------onScroll--------",e.nativeEvent);
    var offsetY = e.nativeEvent.contentOffset.y; //滑动距离
    var contentSizeHeight = e.nativeEvent.contentSize.height; //scrollView contentSize高度
    var oriageScrollHeight = e.nativeEvent.layoutMeasurement.height; //scrollView高度
    if (Math.abs((offsetY + oriageScrollHeight) - contentSizeHeight)<5){
        thiz.log("--------onScroll--------","滚动到了底部");
        thiz.log("---------------thiz.state.hotGoods----------------------",thiz.state.hasNext);
        // 判断是否还有更多商品
        if(thiz.state.hasNext){
          thiz.log("--------thiz.isHotLoading--------",thiz.isHotLoading);
          // 判断是否正在加载
          if(!thiz.isHotLoading){
            // 加载热销商品
            thiz.isHotLoading = true;
            thiz.setState({visible:true});
            thiz.request("spu/getOnSalePage",{
                method:"POST",
                data:{
                  bannerGroupItemId:thiz.state.data.id,
                  page:{
                    pageSize:BaseComponent.PAGE_SIZE,
                    pageNumber:thiz.hotCurPage+1
                  }
                },
                success:function(ret){
                      thiz.isHotLoading = false;
                      thiz.setState({
                            visible:false,
                      });
                      if(ret.respCode=="00"){
                            let hotGoods = thiz.state.hotGoods;
                            let items = hotGoods.records;
                            if(items){
                              if(ret.data.spuPage.records.length>0){
                                for(let i=0;i<ret.data.spuPage.records.length;i++){
                                  items.push(ret.data.spuPage.records[i]);
                                }
                                hotGoods.records = items;
                                thiz.setState({
                                      hotGoods:hotGoods,
                                      hasNext:ret.data.spuPage.hasNext
                                });
                                thiz.hotCurPage = ret.data.spuPage.pageNumber;
                              }
                            }

                      }
                },
                error:function(err){
                      thiz.isHotLoading = false;
                      thiz.toast(err.msg);
                      thiz.setState({
                            visible:false
                      });
                }
            });
          }
        }else{
          // thiz.toast("没有更多商品了");
        }
    }
  }

  // 处理点击每日上新等的单个商品
  goGoodsDetail(value){
    let thiz = this;
    thiz.log("--------goGoodsDetail--------",value);
    if(value){
      // 跳转到商品详情
      thiz.goPage("GoodsXiangQing",{id:value.id});
    }
  }

  render() {
    let thiz = this;
    return (
      <View style={style.wrapper}>
        {/*状态栏占位*/}
        <View style={{ backgroundColor:"rgb(255,255,255)",
                      height:thiz.isIphoneX(30,BaseComponent.SH),
                      position:"relative",
                      zIndex:1,
                      opacity:1,}}></View>

        {/*转菊花*/}
        <Modal
          visible={this.state.visible}
          onRequestClose={()=>{this.setState({visible:false})}}
          transparent={true}
          animationType={"fade"}>
          <View style={{backgroundColor:"rgba(0,0,0,0.4)",width:"100%",width:"100%",flex:1,flexDirection:"column",alignItems:"center",justifyContent:"center",position:"relative"}}>
            <View>
                  <ActivityIndicator size="large" color='white'/>
            </View>
          </View>
        </Modal>

        {/*顶部导航栏*/}
        <View style={style.navBar}>
          <View style={{flex:1,flexDirection:"row",justifyContent:"flex-start"}}>

            <TouchableWithoutFeedback onPress={()=>this.goBack()}>
            <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>
              <Image source={Imgs.back} style={{width:(BaseComponent.W*0.127*0.4),height:(BaseComponent.W*0.127*0.4),marginRight:BaseComponent.W*0.04}}></Image>
            </View>
            </TouchableWithoutFeedback>

            <View style={{flexGrow:1,backgroundColor:"transparent",
              marginTop:ti.select({ios:(BaseComponent.H*0.02)/2,android:(BaseComponent.H*0.016)/2}),borderRadius:ti.select({ios:(BaseComponent.H*0.05)/2,android:(BaseComponent.H*0.054)/2}),borderColor:"#0A0A0A",
              borderWidth:0,}}>
              <Text style={{color:"#0A0A0A",width:"100%",textAlign:"center",marginTop:ti.select({ios:"4.5%",android:"1.6%"}),fontSize:17}}>{this.params.title}</Text>
            </View>
            <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>
              <Image source={require('../../image/home/msg.png')} style={{width:(BaseComponent.W*0.127*0.5),height:(BaseComponent.W*0.127*0.5),display:"none"}}></Image>
            </View>

          </View>
        </View>

        <ScrollView showsVerticalScrollIndicator={false}
          alwaysBounceVertical={false}
          
          onMomentumScrollEnd={(e)=>{thiz.onScroll(e)}}>
          {/*主要内容*/}
          <View>
            {/*图文描述区域*/}
            <View>
              {
                this.getImgs(this.state.imgs.attachments&&this.state.imgs.attachments.length>0?this.state.imgs.attachments:[])
              }
            </View>
            {/*商品列表*/}
            <View>
              {
                this.getGoods(this.state.hotGoods.records&&this.state.hotGoods.records.length>0?this.state.hotGoods.records:[])
              }
            </View>
             {
                thiz.state.hasNext?null:thiz.nomore({bgcolor:'#F0F0F0'})
              }
          </View>
        </ScrollView>

      </View>
    );
  }

  // 加载数据
  loadData(callback){
    let thiz = this;
    if(thiz.state.data&&thiz.state.data.id){
      thiz.setState({
            visible:true,
      });
      thiz.request("bannerGroupItem/getDetailById",{
          method:"POST",
          data:{
            id:thiz.state.data.id
          },
          success:function(ret){
                if(ret.respCode=="00"){

                  thiz.setState({
                    imgs:ret.data,
                  });

                  thiz.isHotLoading = true;
                  // 加载商品列表
                  thiz.request("spu/getOnSalePage",{
                      method:"POST",
                      data:{
                        bannerGroupItemId:thiz.state.data.id,
                        page:{
                          pageSize:BaseComponent.PAGE_SIZE,
                          pageNumber:thiz.hotCurPage+1
                        }
                      },
                      success:function(ret){
                            thiz.setState({
                                  visible:false,
                            });
                            if(ret.respCode=="00"){

                                  if(ret.data.spuPage.records&&ret.data.spuPage.records.length>0){
                                    thiz.setState({
                                          hotGoods:ret.data.spuPage,
                                          hasNext:ret.data.spuPage.hasNext
                                    });

                                    thiz.hotCurPage = ret.data.spuPage.pageNumber;
                                  }

                            }

                            thiz.isHotLoading = false;

                            if(callback){
                              callback();
                            }

                      },
                      error:function(err){
                            thiz.toast(err.msg);
                            thiz.setState({
                                  visible:false,
                            });

                            thiz.isHotLoading = false;

                            if(callback){
                              callback();
                            }
                      }
                  });
                }
          },
          error:function(err){
                thiz.toast(err.msg);
                thiz.setState({
                      visible:false
                });
          }
      });
    }
  }

  componentDidMount(){
    let thiz = this;
    thiz.loadData();
  }

}
