
/**
 * @name Home.js
 * @auhor 李磊
 * @date 2018.8.14
 * @desc APP首页
 */

import React, {Component} from "react";
import {Platform, StyleSheet, View,Text,Button,Image,TouchableWithoutFeedback} from "react-native";
import {StatusBar,ScrollView} from "react-native";
import {createBottomTabNavigator} from 'react-navigation';
import Home from "../Home";
import Swiper from "react-native-swiper";
// 导入工具函数库
import T from "../../lib/Tools";
import BaseComponent from "../BaseComponent";
// 导入公共样式
import St from "../../lib/Style";
// 导入图片资源库
import Imgs from "../../lib/Img";

// Tools实例
let ti = T.getInstance();

let style = StyleSheet.create({
  wrapper:{
    backgroundColor:"white",
    flex:1,
    flexDirection:"column",
    alignItems:"stretch",
    justifyContent:"flex-start",
    position:"relative",
  },
  statusBar:{
    backgroundColor:"rgb(255,255,255)",
    height:ti.select({ios:16,android:22}),
    position:"relative",
    zIndex:1,
    opacity:1,
  },
  navBar:{
    backgroundColor:"rgb(255,255,255)",
    height:"7%",
    position:"relative",
    zIndex:1,
    opacity:1,
    borderBottomWidth:1,
    borderBottomColor:"rgb(241,241,241)"
  },
});


export default class Country extends BaseComponent{
  //隐藏上导航栏
  static navigationOptions={
    header:null
  };
  
  constructor(props){
    super(props);
    this.state={
      data:[],//国家馆数据
    }
  };
  componentWillMount(){
      var thiz=this;
      thiz.request("countryPavilion/getPage",{
        method:"POST",
        success:function(ret){
            if(ret.respCode=="00" && ret.data.records)
            {
              thiz.setState({data:ret.data.records});
            }
        },
        error:function(err){

        }
      })
  };
  //分类模板函数
  getCountryTpl(data){
    var thiz=this;
    thiz.log("---------------------------data----------------------------------",data)
    return (
      <View style={{felx:1,flexDirection:'row',flexWrap:'wrap'}}>
        {
          data.map(function(value,index){
            
            return (
              <TouchableWithoutFeedback onPress={()=>{
                thiz.log("---------------------------value-----------------------------",value);
                thiz.navigate("FenleiXiangqing",{countryPavilionId:value.id})
              }}>
              <View style={{flexDirection:'row',flexWrap:'wrap',}} key={index}>

                <Image style={{width:BaseComponent.W*170/375,marginLeft:10,marginTop:10,height:102,borderRadius:3}} source={{uri:value.imageAttachment.resourceFullAddress?value.imageAttachment.resourceFullAddress:"ds"}}></Image> 

              </View>
              </TouchableWithoutFeedback>
            );
          })
        }
      </View>
    );
  };
  //列表点击事件
  //渲染界面
  render() {
    let thiz=this;
    return (
        <View style={style.wrapper}>
            {/*状态栏占位*/}
                <View style={{ backgroundColor:"rgba(255,255,255)",
                      height:thiz.isIphoneX(30,BaseComponent.SH),
                      position:"relative",
                      zIndex:1,
                      opacity:1,}}></View>
                {/*顶部导航栏*/}
              <View style={style.navBar}>
                <View style={{flex:1,flexDirection:"row",justifyContent:"flex-start"}}>

                    <TouchableWithoutFeedback onPress={()=>this.goBack()}>
                    <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>
                      <Image source={Imgs.back} style={{width:(BaseComponent.W*0.127*0.4),height:(BaseComponent.W*0.127*0.4),marginRight:BaseComponent.W*0.04}}></Image>
                    </View>
                    </TouchableWithoutFeedback>

                    <View style={{flexGrow:1,backgroundColor:"transparent",
                      marginTop:ti.select({ios:(BaseComponent.H*0.025)/2,android:(BaseComponent.H*0.016)/2}),borderRadius:ti.select({ios:(BaseComponent.H*0.05)/2,android:(BaseComponent.H*0.054)/2}),borderColor:"#0A0A0A",
                      borderWidth:0,}}>
                      <Text style={{color:"#0A0A0A",width:"100%",textAlign:"center",marginTop:ti.select({ios:"4%",android:"1.6%"}),fontSize:BaseComponent.W*17/375,marginLeft:'4.3%'}}>{this.params.title}</Text>
                    </View>
                    <View style={{width:"14%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center",marginRight:BaseComponent.W*5/375}}>
                        
                    </View>
                </View>
              </View>
                    
              {/*国家分类*/}
              {thiz.getCountryTpl(this.state.data)}  
      </View>   
    );
  }

}
