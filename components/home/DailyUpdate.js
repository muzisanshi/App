
/**
 * @name Home.js
 * @auhor 李磊
 * @date 2018.8.14
 * @desc APP首页
 */

/**
 * by zh at time 7.24
 */

import React,{Component} from "react";
import {Image,Modal,Text,View,StyleSheet,ScrollView,TouchableWithoutFeedback,ActivityIndicator} from 'react-native';
// 导入工具函数库
import T from "../../lib/Tools";
// 导入公共样式
import St from "../../lib/Style";
// 导入图片资源库
import Imgs from "../../lib/Img";
import BaseComponent from '../BaseComponent';

// 转菊花
import Spinner from 'react-native-loading-spinner-overlay';

import MyImage from "../../lib/MyImage";

// Tools实例
let ti = T.getInstance();

let style = StyleSheet.create({
  wrapper:{
    backgroundColor:"#F0F0F0",
    flex:1,
    flexDirection:"column",
    alignItems:"stretch",
    justifyContent:"flex-start",
    position:"relative",
  },
  statusBar:{
    backgroundColor:"rgb(255,255,255)",
    height:ti.select({ios:16,android:22}),
    position:"relative",
    zIndex:1,
    opacity:1,
  },
  navBar:{
    backgroundColor:"rgb(255,255,255)",
    height:"7%",
    position:"relative",
    zIndex:1,
    opacity:1,
    borderBottomWidth:1,
    borderBottomColor:"rgb(241,241,241)"
  },
});
let data=[{name:'米兰达 可儿 跨界设计',ins:'施华洛世奇DUO恶魔之眼项链时尚气质女短款锁骨链首饰',price:'¥799',img:require('../../image/home/good.png')},
          {name:'米兰达 可儿 跨界设计',ins:'施华洛世奇DUO恶魔之眼项链时尚气质女短款锁骨链首饰',price:'¥799',img:require('../../image/home/good.png')}];
let data1=[{img:require('../../image/home/good.png'),ins1:'FOREO LUNA go清洁毛孔电动硅胶清洁仪美容洁面仪',ins2:'轻盈净透 随行所欲 轻松焕发活力',price:380,prePrice:680},
           {img:require('../../image/home/good.png'),ins1:'FOREO LUNA go清洁毛孔电动硅胶清洁仪美容洁面仪',ins2:'轻盈净透 随行所欲 轻松焕发活力',price:380,prePrice:680},
           {img:require('../../image/home/good.png'),ins1:'FOREO LUNA go清洁毛孔电动硅胶清洁仪美容洁面仪',ins2:'轻盈净透 随行所欲 轻松焕发活力',price:380,prePrice:680},
           {img:require('../../image/home/good.png'),ins1:'FOREO LUNA go清洁毛孔电动硅胶清洁仪美容洁面仪',ins2:'轻盈净透 随行所欲 轻松焕发活力',price:380,prePrice:680}];          
export default class DailyUpdate extends BaseComponent{

    constructor(props){
      super(props);
      
      // 维护一个页面栈
      this.state = {
        visible:false,
        isRefreshing:false,// 是否正在刷新
        hotGoods:{},// 每日上新商品列表
        hasNext:false
      }
      this.hotCurPage = 0,// 加载页码
      this.isHotLoading = false;

    }

    // 滚动回调
    onScroll(e){
      let thiz = this;
      thiz.log("--------onScroll--------",e.nativeEvent);
      var offsetY = e.nativeEvent.contentOffset.y; //滑动距离
      var contentSizeHeight = e.nativeEvent.contentSize.height; //scrollView contentSize高度
      var oriageScrollHeight = e.nativeEvent.layoutMeasurement.height; //scrollView高度
      if (Math.abs((offsetY + oriageScrollHeight) - contentSizeHeight)<5){
          thiz.log("--------onScroll--------","滚动到了底部");
          // 判断是否还有更多商品
          if(thiz.state.hasNext){
            thiz.log("--------thiz.isHotLoading--------",thiz.isHotLoading);
            // 判断是否正在加载
            if(!thiz.isHotLoading){
              // 加载热销商品
              thiz.isHotLoading = true;
              thiz.setState({visible:true});
              thiz.request("spu/getOnSalePage",{
                  method:"POST",
                  data:{
                    columnGroupItemId:thiz.params.id,
                    page:{
                      pageNumber:thiz.hotCurPage+1,
                      pageSize:BaseComponent.PAGE_SIZE,
                    }
                  },
                  success:function(ret){
                        thiz.isHotLoading = false;
                        thiz.setState({
                              visible:false,
                        });
                        if(ret.respCode=="00"){
                              let hotGoods = thiz.state.hotGoods;
                              let items = hotGoods.records;
                              if(items){
                                if(ret.data.spuPage.records.length>0){
                                  for(let i=0;i<ret.data.spuPage.records.length;i++){
                                    items.push(ret.data.spuPage.records[i]);
                                  }
                                  hotGoods.records = items;
                                  thiz.setState({
                                        hotGoods:hotGoods,
                                        hasNext:ret.data.spuPage.hasNext,
                                  });
                                  thiz.hotCurPage = ret.data.spuPage.pageNumber;
                                }
                              }

                        }
                  },
                  error:function(err){
                        thiz.isHotLoading = false;
                        thiz.toast(err.msg);
                        thiz.setState({
                              visible:false
                        });
                  }
              });
            }
          }else{
            // thiz.toast("没有更多商品了");
          }
      }
    }

    // 加入购物车
    addCart(value){
      let thiz = this;
      thiz.log("--------addCart--------",value);
      if(value){
        thiz.setState({
              visible:true
        });
        thiz.request("shoppingCar/addGoods",{
              method:"POST",
              data:{
                    num:1,
                    skuId:value.id
              },
              success:function(ret){
                    thiz.setState({
                          visible:false
                    });
                    if(ret.respCode=="00"){
                          thiz.log("--------加入购物车成功--------","加入购物车成功");
                          thiz.toast("加入购物车成功");
                          // 关闭商品属性选择
                          thiz.setState({
                                visible:false,
                          });

                          // 发送添加购物车成功消息
                          thiz.emit("add_cart_success");
                    }
              },
              error:function(err){
                    thiz.toast(err.msg);
                    thiz.setState({
                          visible:false,
                    });
              }
        });
      }
    }

    // 处理点击每日上新等的单个商品
  goGoodsDetail(value){
    let thiz = this;
    thiz.log("--------goGoodsDetail--------",value);
    if(value){
      // 跳转到商品详情
      thiz.goPage("GoodsXiangQing",{id:value.id});
    }
  }

    render() {
        let thiz = this;
        return (      
          <View style={style.wrapper}>   
                {/*状态栏占位*/}
                <View style={{ backgroundColor:"rgb(255,255,255)",
                            height:thiz.isIphoneX(30,BaseComponent.SH),
                            position:"relative",
                            zIndex:1,
                            opacity:1,}}></View>

                {/*转菊花*/}
                <Modal
                  visible={this.state.visible}
                  onRequestClose={()=>{this.setState({visible:false})}}
                  transparent={true}
                  animationType={"fade"}>
                  <View style={{backgroundColor:"rgba(0,0,0,0.4)",width:"100%",width:"100%",flex:1,flexDirection:"column",alignItems:"center",justifyContent:"center",position:"relative"}}>
                    <View>
                          <ActivityIndicator size="large" color='white'/>
                    </View>
                  </View>
                </Modal>

                {/*顶部导航栏*/}
                <View style={style.navBar}>
                  <View style={{flex:1,flexDirection:"row",justifyContent:"flex-start"}}>

                    <TouchableWithoutFeedback onPress={()=>this.goBack()}>
                    <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>
                      <Image source={Imgs.back} style={{width:(BaseComponent.W*0.127*0.4),height:(BaseComponent.W*0.127*0.4),marginRight:BaseComponent.W*0.04}}></Image>
                    </View>
                    </TouchableWithoutFeedback>

                    <View style={{flexGrow:1,backgroundColor:"transparent",
                      marginTop:ti.select({ios:(BaseComponent.H*0.02)/2,android:(BaseComponent.H*0.016)/2}),borderRadius:ti.select({ios:(BaseComponent.H*0.05)/2,android:(BaseComponent.H*0.054)/2}),borderColor:"#0A0A0A",
                      borderWidth:0,}}>
                      <Text style={{color:"#0A0A0A",width:"100%",textAlign:"center",marginTop:ti.select({ios:"4.5%",android:"1.6%"}),fontSize:17}}>{this.params.title}</Text>
                    </View>
                    <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>
                       
                    </View>
                  </View>
                </View> 
                <ScrollView showsVerticalScrollIndicator={false}
                  onMomentumScrollEnd={(e)=>{thiz.onScroll(e)}}
                  showsVerticalScrollIndicator={true}>
              {/*
                  data.map((item,index)=>{
                      return (
                        <View style={{display:"none",width:BaseComponent.W*355/375,height:BaseComponent.W*100/375,backgroundColor:index%2==0?'#F6F7FF':'#FFE9F2',
                                    marginTop:BaseComponent.W*11/375,marginLeft:BaseComponent.W*10/375,flexDirection:'row'}}>
                            <View style={{width:BaseComponent.W*251/375,height:'100%',marginLeft:BaseComponent.W*13/375}}>
                                <Text style={{marginTop:BaseComponent.W*20/375,fontSize:BaseComponent.W*15/375,color:'#3A3A3A'}}>米兰达 可儿 跨界设计</Text>
                                <Text style={{fontSize:BaseComponent.W*11/375,color:'#828282',marginTop:BaseComponent.W*8/375}} numberOfLines={1}>施华洛世奇DUO恶魔之眼项链时尚气质女短款锁骨链首饰</Text>
                                <Text style={{fontSize:BaseComponent.W*18/375,color:'#FF407E',marginTop:BaseComponent.W*10/375}}>¥380</Text>
                            </View>
                            <Image style={{flex:1}} source={require('../../image/home/good.png')}></Image>
                        </View>
                    )
                  })
              */}

              {
                  thiz.state.hotGoods.records&&thiz.state.hotGoods.records.length>0?thiz.state.hotGoods.records.map((value,index)=>{
                    thiz.log("--------每日上新列表--------","每日上新列表");
                    return (
                        <View style={{backgroundColor:'rgba(255,255,255,1)'}}>
                        <TouchableWithoutFeedback onPress={()=>{thiz.goGoodsDetail(value)}}>
                        <View style={{width:'100%',height:BaseComponent.W*125/375,flexDirection:'row',backgroundColor:'white',marginTop:10}}>
                            
                            <View style={{width:BaseComponent.W*119/375,height:BaseComponent.W*120/375,borderWidth:0.5,borderColor:'rgba(240, 240, 240, 1)',
                                          marginLeft:BaseComponent.W*10/375,justifyContent:'center',alignItems:'center'}}>
                                <Image style={{width:BaseComponent.W*110/375,height:BaseComponent.W*110/375}} source={{uri:value.attachment.resourceFullAddress}}></Image>
                            </View>
                            <View style={{width:BaseComponent.W*220/375,height:BaseComponent.W*120/375,marginLeft:BaseComponent.W*16/375,justifyContent:'space-between'}}>
                                <View style={{width:BaseComponent.W*220/375,height:BaseComponent.W*60/375,}}>
                                    <Text style={{fontSize:BaseComponent.W*15/375,color:'#3A3A3A'}}>{value.name}</Text>
                                    <Text style={{fontSize:BaseComponent.W*12/375,color:'#3A3A3A',display:'none'}}></Text>
                                </View>
                                <View style={{width:BaseComponent.W*220/375,height:BaseComponent.W*25/375,flexDirection:'row',justifyContent:'space-between',alignItems:'flex-end',}}>
                                    <Text style={{fontSize:BaseComponent.W*15/375,color:'#FF407E'}}>¥{value.buyUnitPrice.toFixed(2)}&nbsp;&nbsp;&nbsp;&nbsp;
                                      {/*<Text style={{fontSize:BaseComponent.W*11/375,color:'#999999',textDecorationLine:'line-through'}}>¥{value.marketPrice.toFixed(2)}</Text>*/}
                                    </Text>
                                    
                                    {/*<TouchableWithoutFeedback onPress={()=>{thiz.addCart(value)}}>
                                    <Image style={{width:BaseComponent.W*25/375,height:BaseComponent.W*25/375}} source={require('../../image/home/shoppingCart.png')}></Image>
                                    </TouchableWithoutFeedback>*/}
                                </View>
                            </View>
                        </View>
                        </TouchableWithoutFeedback>
                        </View>
                    )
                  }):null
              }
              {
                thiz.state.hasNext?null:thiz.nomore({bgcolor:'#F0F0F0'})
              }
              </ScrollView>
          </View>   
        );
    }

    loadData(){
      let thiz = this;

      if(!thiz.isHotLoading){
        thiz.setState({
              visible:true,
        });
        thiz.isHotLoading = true;
        thiz.request("spu/getOnSalePage",{
            method:"POST",
            data:{
              columnGroupItemId:thiz.params.id,
              page:{
                pageNumber:thiz.hotCurPage+1,
                pageSize:BaseComponent.PAGE_SIZE,
              }
            },
            success:function(ret){
                  thiz.setState({
                        visible:false,
                  });
                  if(ret.respCode=="00"){

                        if(ret.data.spuPage.records.length>0){
                          thiz.setState({
                                hotGoods:ret.data.spuPage,
                                hasNext:ret.data.spuPage.hasNext,
                          });

                          thiz.hotCurPage = ret.data.spuPage.pageNumber;
                        }

                  }

                  thiz.isHotLoading = false;

            },
            error:function(err){
                  thiz.toast(err.msg);
                  thiz.setState({
                        visible:false,
                  });

                  thiz.isHotLoading = false;

            }
        });
      }

    }

    componentDidMount(){
      let thiz = this;
      thiz.loadData();
    }

}
