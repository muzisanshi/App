/**
 * @name IntegralDetails.js
 * @auhor 程浩
 * @date 2018.8.23
 * @desc 积分详情
 */
import React,{Component} from 'react';
import {Text,View,StyleSheet,Image,TouchableWithoutFeedback,ScrollView,TextInput,StatusBar,BackHandler,Modal,ActivityIndicator,RefreshControl,FlatList} from 'react-native';
 // 导入工具函数库
import T from "../../lib/Tools";
// 导入公共样式
import St from "../../lib/Style";
// 导入图片资源库
import Imgs from "../../lib/Img";
import BaseComponent from '../BaseComponent';
// 转菊花
import Spinner from 'react-native-loading-spinner-overlay';

// Tools实例
let ti = T.getInstance();

let style = StyleSheet.create({
  wrapper:{
    backgroundColor:"#F0F0F0",
    flex:1,
    flexDirection:"column",
    alignItems:"stretch",
    justifyContent:"flex-start",
    position:"relative",
  },
  statusBar:{
    backgroundColor:"rgb(255,255,255)",
    height:ti.select({ios:16,android:22}),
    position:"relative",
    zIndex:1,
    opacity:1,
  },
  navBar:{
    backgroundColor:"rgb(255,255,255)",
    height:"7%",
    position:"relative",
    zIndex:1,
    opacity:1,
    // borderBottomWidth:1,
    // borderBottomColor:"rgb(241,241,241)",
    flexDirection:'row',
    alignItems:'center',
  },
});

export default class IntegralDetails extends BaseComponent{
  constructor(props){
    super(props);
    this.state={
      visible:false,
      ActuallyRealized:true,//true表示实到积分,false表示未到积分
      data:[1,1,1],
    }
  };
  componentDidMount(){
    var thiz = this;
    
    
  };
  _keyExtractor=(item,index)=>index.toString();

  //积分列表
  _renderItem=()=>{
    let thiz = this;
      return (
        <View style={{width:BaseComponent.W*362/375,height:BaseComponent.W*55/375,marginTop:5,flexDirection:'row',alignItems:'center',backgroundColor:'white',borderRadius:10,marginLeft:BaseComponent.W*6/375}}>
            <View style={{width:BaseComponent.W*187/375,height:'100%',flexDirection:'row',alignItems:'center'}}>
                <Text style={{fontSize:BaseComponent.W*15/375,color:'#1E1E1E',marginLeft:BaseComponent.W*16/375}}>2018.12.25&nbsp;&nbsp;&nbsp;&nbsp;16:15:56</Text>
            </View>
            <View style={{width:BaseComponent.W*155/375,height:'100%',flexDirection:"row",justifyContent:'flex-end',alignItems:'center',}}>
                <Text style={{fontSize:BaseComponent.W*15/375,color:'#1E1E1E'}}>积分:&nbsp;&nbsp;</Text>
                <Text style={{fontSize:BaseComponent.W*15/375,color:'#F52645',maxWidth:BaseComponent.W*130/375}} numberOfLines={1}>4554</Text>
            </View>
        </View>
      )
  }

  render(){
    let thiz=this;
    return (
      <View style={style.wrapper}>

        {/*转菊花*/}
        <Modal
          visible={this.state.visible}
          onRequestClose={()=>{this.setState({visible:false})}}
          transparent={true}
          animationType={"fade"}>
          <View style={{backgroundColor:"rgba(14,14,14,0.5)",width:"100%",width:"100%",flex:1,flexDirection:"column",alignItems:"center",justifyContent:"center",position:"relative"}}>
            <View>
                  <ActivityIndicator size="large" color='white'/>
            </View>
          </View>
        </Modal>
     
        {/*状态栏占位*/}
        <View style={{backgroundColor:this.state.visible?"rgba(14,14,14,0.5)":"#fff",
                      height:BaseComponent.SH,
                      position:"relative",
                      zIndex:1,
                      opacity:1,}}></View>
        {/*顶部导航栏*/}
        <View style={style.navBar}>
          <View style={{flex:1,flexDirection:"row",justifyContent:"flex-start",alignItems:'center'}}>

            <TouchableWithoutFeedback onPress={()=>this.goBack()}>
            <View style={{width:"15%",height:"100%",backgroundColor:"transparent",justifyContent:"center",marginLeft:BaseComponent.W*10/375}}>
              <Image source={Imgs.back} style={{width:(BaseComponent.W*0.127*0.4),height:(BaseComponent.W*0.127*0.4),marginBottom:ti.select({ios:BaseComponent.W*7/375,android:5})}}></Image>
            </View>
            </TouchableWithoutFeedback>

            <View style={{flexGrow:1,backgroundColor:"transparent",borderWidth:0}}>
              <Text style={{color:"#0A0A0A",width:"100%",textAlign:"center",marginTop:ti.select({ios:"4%",android:"1.6%"}),fontSize:BaseComponent.W*18/375}}>{this.params.title}</Text>
            </View>
             
            <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center",}}>
                <Text style={{fontSize:BaseComponent.W*0.037,color:'black'}}></Text>
            </View>
          </View>
        </View>
        
        {/*实到积分和未到积分*/} 
        <View style={{width:'100%',height:BaseComponent.W*45/375,backgroundColor:'white',flexDirection:'row',justifyContent:'center'}}>
            <TouchableWithoutFeedback onPress={()=>{
              thiz.setState({ActuallyRealized:true});
            }}>
                <View style={{width:BaseComponent.W*50/375,height:'100%',flexDirection:'column',alignItems:'center'}}>
                    <Text style={{fontSize:thiz.state.ActuallyRealized?BaseComponent.W*18/375:BaseComponent.W*15/375,color:thiz.state.ActuallyRealized?'#1E1E1E':'#585858',marginTop:10}}>实到</Text>
                    <View style={{width:BaseComponent.W*40/375,height:1.5,borderRadius:0.75,backgroundColor:'#FDD17A',marginTop:2,display:thiz.state.ActuallyRealized?'flex':'none'}}></View>
                </View>
            </TouchableWithoutFeedback>

            <TouchableWithoutFeedback onPress={()=>{
              thiz.setState({ActuallyRealized:false});
            }}>
                <View style={{width:BaseComponent.W*50/375,height:'100%',marginLeft:BaseComponent.W*100/375,flexDirection:'column',alignItems:'center'}}>
                    <Text style={{fontSize:thiz.state.ActuallyRealized?BaseComponent.W*15/375:BaseComponent.W*18/375,color:thiz.state.ActuallyRealized?'#585858':'#1E1E1E',marginTop:10}}>未到</Text>
                    <View style={{width:BaseComponent.W*40/375,height:1.5,borderRadius:0.75,backgroundColor:'#FDD17A',marginTop:2,display:thiz.state.ActuallyRealized?'none':'flex'}}></View>
                </View>
            </TouchableWithoutFeedback>
        </View>
         
        {/*积分列表*/}
        <FlatList
          data={this.state.data}
          keyExtractor={this._keyExtractor}
          renderItem={this._renderItem}
          showsVerticalScrollIndicator = {false}
          extraData={this.state}
        /> 
      </View>          
    )
  }
}
