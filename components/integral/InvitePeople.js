/**
 * @name InvitePeople.js
 * @auhor 程浩
 * @date 2018.8.23
 * @desc 邀请的人
 */
import React,{Component} from 'react';
import {Text,View,StyleSheet,Image,TouchableWithoutFeedback,ScrollView,TextInput,StatusBar,BackHandler,Modal,ActivityIndicator,RefreshControl,FlatList} from 'react-native';
 // 导入工具函数库
import T from "../../lib/Tools";
// 导入公共样式
import St from "../../lib/Style";
// 导入图片资源库
import Imgs from "../../lib/Img";
import BaseComponent from '../BaseComponent';
import SelectAddressOrDate from '../SelectAddressOrDate';
// 转菊花
import Spinner from 'react-native-loading-spinner-overlay';
//实现颜色渐变
import LinearGradient from 'react-native-linear-gradient';

// Tools实例
let ti = T.getInstance();

let style = StyleSheet.create({
  wrapper:{
    backgroundColor:"#F0F0F0",
    flex:1,
    flexDirection:"column",
    alignItems:"stretch",
    justifyContent:"flex-start",
    position:"relative",
  },
  statusBar:{
    backgroundColor:"rgb(255,255,255)",
    height:ti.select({ios:16,android:22}),
    position:"relative",
    zIndex:1,
    opacity:1,
  },
  navBar:{
    backgroundColor:"rgb(255,255,255)",
    height:"7%",
    position:"relative",
    zIndex:1,
    opacity:1,
    borderBottomWidth:1,
    borderBottomColor:"rgb(241,241,241)",
    flexDirection:'row',
    alignItems:'center',
  },
});

export default class InvitePeople extends BaseComponent{
  constructor(props){
    super(props);
    this.state={
      visible:false,
      data:[1,1,1,1],
      isVisible:false,
      date:[],//日期年月
      DottedLine:[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,11,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],//只是单纯的实现虚线
    }
  };

  componentWillMount(){
    let thiz = this;
    //获取当前的年月
    var date=new Date();
    var year=date.getFullYear();
    var month=date.getMonth()+1;
    var dateArray=[];
    dateArray.push(year);
    dateArray.push(month);
    thiz.log("-------------------dateArray---------------------",dateArray);
    thiz.setState({date:dateArray});
  };

  componentDidMount(){
    var thiz = this;
    //监听日期取消按钮
    thiz.listen("cancel_selected_check_integral_date",function(){
      thiz.setState({isVisible:false});
    })  
  };

    //打开时间选择器
  open(){
      this.refs.SelectAddressOrDate.showDate(this.state.date);
  };
  //确定按钮的回调
  callBackDateValue(value){
      this.log("------------------------InvitePeople------------------------------","InvitePeople");
      this.setState({
          date:value,
          isVisible:false,
      })
  };

  _keyExtractor=(item,index)=>index.toString();
  _renderItem=()=>{
    let thiz = this;
    console.log("----------------------renderItem-------------------------","renderItem");
    return (
      <View style={{marginBottom:BaseComponent.W*10/375}}>
          <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={['#F0E0D3', '#EADACD', '#C2ADA1']} style={{width:BaseComponent.W*345/375,marginLeft:BaseComponent.W*15/375,marginTop:BaseComponent.W*15/375,borderRadius:10}}>
              <View style={{width:'100%',flexDirection:'row',paddingTop:BaseComponent.W*20/375}}>
                  <Text style={{fontSize:BaseComponent.W*15/375,color:'#653A22',width:BaseComponent.W*200/375,marginLeft:BaseComponent.W*17/375}} numberOfLines={1}>昵称:&nbsp;&nbsp;Boateng</Text>
                  <TouchableWithoutFeedback onPress={()=>{
                    thiz.navigate('TaInvitePeople',{title:'TA邀请的人'});
                  }}>
                      <View style={{width:BaseComponent.W*110/375,flexDirection:'row',alignItems:'center'}}>
                          <Text style={{fontSize:BaseComponent.W*12/375,color:'#653A22'}}>查看TA邀请的人</Text>
                          <View style={{width:BaseComponent.W*13/375,height:BaseComponent.W*13/375,backgroundColor:'red',marginLeft:BaseComponent.W*7/375}}></View>
                      </View>
                  </TouchableWithoutFeedback>    
              </View>
              <Text style={{fontSize:BaseComponent.W*15/375,color:'#653A22',marginLeft:BaseComponent.W*17/375,marginTop:BaseComponent.W*15/375}}>手机:&nbsp;&nbsp;18280462126</Text>
              <Text style={{fontSize:BaseComponent.W*15/375,color:'#653A22',marginLeft:BaseComponent.W*17/375,marginTop:BaseComponent.W*15/375}}>注册时间:&nbsp;&nbsp;2018.12.30 10：19：20</Text>
              <View style={{width:BaseComponent.W*326/375,height:1,flexDirection:'row',marginLeft:BaseComponent.W*9/375,marginTop:BaseComponent.W*18/375,overflow:'hidden'}}>
                  {
                    thiz.state.DottedLine.map(()=>{
                      return (
                        <View style={{width:3,height:1,backgroundColor:'#653F27',marginLeft:2}}></View>
                      )
                    })
                  }
              </View>
              <TouchableWithoutFeedback onPress={()=>{
                thiz.setState({isVisible:true});
              }}>
                  <View style={{flexDirection:'row',marginLeft:BaseComponent.W*132/375,marginTop:BaseComponent.W*14/375,width:BaseComponent.W*100/375}}>
                      <Text style={{fontSize:BaseComponent.W*14/375,color:'#653A22'}}>{thiz.state.date[0]}.{thiz.state.date[1]}</Text>
                      <View style={{width:BaseComponent.W*17/375,height:BaseComponent.W*17/375,backgroundColor:'red',marginLeft:5}}></View>
                  </View>
              </TouchableWithoutFeedback>    

              <View style={{width:'100%',flexDirection:'row',marginTop:BaseComponent.W*20/375,paddingBottom:BaseComponent.W*14/375}}>
                  <View style={{width:BaseComponent.W*115/375,flexDirection:'column',alignItems:'center'}}>
                      <Text style={{fontSize:BaseComponent.W*22/375,color:'#1A1A1A',maxWidth:BaseComponent.W*115/375}} numberOfLines={1}>350</Text>
                      <Text style={{fontSize:BaseComponent.W*12/375,color:'#653A22'}}>总积分</Text>
                  </View>

                  <View style={{width:BaseComponent.W*115/375,flexDirection:'column',alignItems:'center'}}>
                      <Text style={{fontSize:BaseComponent.W*22/375,color:'#1A1A1A',maxWidth:BaseComponent.W*115/375}} numberOfLines={1}>680</Text>
                      <Text style={{fontSize:BaseComponent.W*12/375,color:'#653A22'}}>实到积分</Text>
                  </View>

                  <View style={{width:BaseComponent.W*115/375,flexDirection:'column',alignItems:'center'}}>
                      <Text style={{fontSize:BaseComponent.W*22/375,color:'#1A1A1A',maxWidth:BaseComponent.W*115/375}} numberOfLines={1}>330</Text>
                      <Text style={{fontSize:BaseComponent.W*12/375,color:'#653A22'}}>未到积分</Text>
                  </View>

              </View>
          </LinearGradient>
      </View>
    )
  }

  render(){
    let thiz=this;
    return (
      <View style={style.wrapper}>

        {/*转菊花*/}
        <Modal
          visible={this.state.visible}
          onRequestClose={()=>{this.setState({visible:false})}}
          transparent={true}
          animationType={"fade"}>
          <View style={{backgroundColor:"rgba(14,14,14,0.5)",width:"100%",width:"100%",flex:1,flexDirection:"column",alignItems:"center",justifyContent:"center",position:"relative"}}>
            <View>
                  <ActivityIndicator size="large" color='white'/>
            </View>
          </View>
        </Modal>
     
        {/*状态栏占位*/}
        <View style={{backgroundColor:this.state.visible?"rgba(14,14,14,0.5)":"#fff",
                      height:BaseComponent.SH,
                      position:"relative",
                      zIndex:1,
                      opacity:1,}}></View>
        
        {/*顶部导航栏*/}
        <View style={style.navBar}>
          <View style={{flex:1,flexDirection:"row",justifyContent:"flex-start",alignItems:'center'}}>

            <TouchableWithoutFeedback onPress={()=>this.goBack()}>
            <View style={{width:"15%",height:"100%",backgroundColor:"transparent",justifyContent:"center",marginLeft:BaseComponent.W*10/375}}>
              <Image source={Imgs.back} style={{width:(BaseComponent.W*0.127*0.4),height:(BaseComponent.W*0.127*0.4),marginBottom:ti.select({ios:BaseComponent.W*7/375,android:5})}}></Image>
            </View>
            </TouchableWithoutFeedback>

            <View style={{flexGrow:1,backgroundColor:"transparent",borderWidth:0}}>
              <Text style={{color:"#0A0A0A",width:"100%",textAlign:"center",marginTop:ti.select({ios:"4%",android:"1.6%"}),fontSize:BaseComponent.W*18/375}}>{this.params.title}</Text>
            </View>
             
            <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center",}}>
                <Text style={{fontSize:BaseComponent.W*0.037,color:'black'}}></Text>
            </View>
          </View>
        </View>

        <Modal visible={this.state.isVisible}
               onRequestClose={()=>{
                thiz.refs.SelectAddressOrDate.hide();
                this.setState({isVisible:false});
              }}
               transparent={true}
               onShow={()=>{
                thiz.open();
               }}>
               <View style={{flex:1,backgroundColor:'rgba(0,0,0,0.8)'}}></View>
        </Modal>

        <SelectAddressOrDate ref={'SelectAddressOrDate'} callBackDateValue={this.callBackDateValue.bind(this)}/>
        
        {/*邀请的人列表*/}
        <FlatList
          data={this.state.data}
          keyExtractor={this._keyExtractor}
          renderItem={this._renderItem}
          showsVerticalScrollIndicator = {false}
          extraData={this.state}
        /> 
        
      </View>          
    )
  }
}
