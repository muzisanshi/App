/**
 * @name PromotionCode.js
 * @auhor 程浩
 * @date 2018.8.23
 * @desc 邀请码
 */
import React,{Component} from 'react';
import {Animated,Text,View,StyleSheet,Image,TouchableWithoutFeedback,ScrollView,TextInput,StatusBar,BackHandler,Modal,ActivityIndicator,RefreshControl,FlatList,ImageBackground} from 'react-native';
 // 导入工具函数库
import T from "../../lib/Tools";
// 导入公共样式
import St from "../../lib/Style";
// 导入图片资源库
import Imgs from "../../lib/Img";
import BaseComponent from '../BaseComponent';

import WXShare from '@yyyyu/react-native-wechat';
import {WechatError} from '@yyyyu/react-native-wechat';

// 转菊花
import Spinner from 'react-native-loading-spinner-overlay';

// 测试保存base64图片
import base64Save from "../../lib/SaveBase64ImageToCameraRoll";

import { captureRef } from "react-native-view-shot";


// Tools实例
let ti = T.getInstance();

let style = StyleSheet.create({
  wrapper:{
    backgroundColor:"#F0F0F0",
    flex:1,
    flexDirection:"column",
    alignItems:"stretch",
    justifyContent:"flex-start",
    position:"relative",
  },
  statusBar:{
    backgroundColor:"rgb(255,255,255)",
    height:ti.select({ios:16,android:22}),
    position:"relative",
    zIndex:1,
    opacity:1,
  },
  navBar:{
    backgroundColor:"rgb(255,255,255)",
    height:"7%",
    position:"relative",
    zIndex:1,
    opacity:1,
    borderBottomWidth:1,
    borderBottomColor:"rgb(241,241,241)",
    flexDirection:'row',
    alignItems:'center',
  },
});

export default class PromotionCode extends BaseComponent{
  constructor(props){
    super(props);
    this.state={
      visible:false,
      bottom:new Animated.Value(0),
      isvisible:false,

      invitation:null,

      invitationCode:"",

      qrcode:"",
    }
  };

  componentWillMount(){
   
  }

  componentDidMount(){
    let thiz = this;
    thiz.loadData();
  };

   _translateAni = ()=>{
        this.state.bottom.setValue(0);
        Animated.timing(
            this.state.bottom,
            {
                toValue: BaseComponent.W*87/375,
                duration: 500,
            },
        ).start();
    }


  // 分享,type的值可以是session和timeline
  shareWX(type,data){
    let thiz = this;
    thiz.log("--------shareWX_data--------",data);
    if(type&&data){
      thiz.log("--------shareWX_data222--------",data);
      WXShare.sendLink({

        link: data.link, // 链接地址
        title: data.title, // 标题
        desc: data.desc, // 描述
        thumb: data.thumb, // 缩略图
        scene: type // 发送场景 optional('session')

      }).then(function(ret){
        thiz.log("--------shareWX_ret--------",ret);
        // if(ret&&ret.errCode==0){
        //   thiz.toast("分享成功");
        //   return;
        // }
        // if(ret&&ret.errCode==1){
        //   thiz.toast("分享失败");
        // }
      }).catch(function(err){
        thiz.log("--------shareWX_err--------",err);
        thiz.toast("分享失败");
      });

    }
  }

  // 加载数据
  loadData(){
    let thiz = this;
    thiz.setState({
      visible:true,
    });
    thiz.request("user/getInvitationCode",{
      method:"POST",
      data:{},
      success:function(ret){
        thiz.setState({
          visible:false,
        });
        if(ret.respCode=="00"){
          // 构造分享所需数据
          let invitation = {};
          invitation.link=ret.data.invitationCodeShare&&ret.data.invitationCodeShare.url?ret.data.invitationCodeShare.url:"",
          invitation.title=ret.data.invitationCodeShare&&ret.data.invitationCodeShare.title?ret.data.invitationCodeShare.title:"",
          invitation.desc=ret.data.invitationCodeShare&&ret.data.invitationCodeShare.desc?ret.data.invitationCodeShare.desc:"",
          invitation.thumb=ret.data.invitationCodeShare&&ret.data.invitationCodeShare.imageAttachment&&ret.data.invitationCodeShare.imageAttachment.resourceFullAddress?ret.data.invitationCodeShare.imageAttachment.resourceFullAddress:"",

          thiz.setState({
            invitation:invitation,
            invitationCode:ret.data.invitationCode,
          });

        }
      },
      error:function(err){
        thiz.toast(err.msg);
        thiz.setState({
          visible:false,
        });
      }
    });

    // 加载二维码
    // thiz.getAccount(function(ret){
    //   thiz.log("--------account--------",ret);
    //   thiz.request("user/testGenQrCode",{
    //     method:"POST",
    //     headers: {
    //         "Accept": "application/json",
    //         "Content-Type": "application/json",
    //         "token":ret.userToken,
    //     },
    //     data:{},
    //     success:function(ret){
    //       if(ret.respCode=="00"){
    //         if(ret.data){
    //           thiz.setState({
    //             qrcode:ret.data
    //           });

    //           // 把base64转换为图片并保存
    //           if(base64Save){
    //             if(BaseComponent.OS=="android"){
    //               thiz.T.requestAndroidPermission("WRITE_EXTERNAL_STORAGE",function(){

    //                 base64Save(ret.data,function(){
    //                   thiz.log("--------success--------","success");
    //                 },function(){
    //                   thiz.log("--------failed--------","failed");
    //                 });
                    
    //               });
    //             }else{
    //               base64Save(ret.data,function(){
    //                 thiz.log("--------success--------","success");
    //               },function(){
    //                 thiz.log("--------failed--------","failed");
    //               });
    //             }

    //           }

    //         }
    //       }
    //     },
    //     error:function(err){
    //       thiz.toast(err.msg);
    //     }
    //   });
    // });

    // 测试合成图片
    // captureRef(thiz.refs.imgComb, {
    //   format: "png",
    //   quality: 0.8,
    //   result: "tmpfile",
    //   snapshotContentContainer: true
    // }).then(
    //   uri => console.log("Image saved to", uri),
    //   error => console.error("Oops, snapshot failed", error)
    // );

  }
 

  render(){
    let thiz=this;
    return (
      <View style={style.wrapper}>

        {/*转菊花*/}
        <Modal
          visible={this.state.visible}
          onRequestClose={()=>{this.setState({visible:false})}}
          transparent={true}
          animationType={"fade"}>
          <View style={{backgroundColor:"rgba(14,14,14,0.5)",width:"100%",width:"100%",flex:1,flexDirection:"column",alignItems:"center",justifyContent:"center",position:"relative"}}>
            <View>
                  <ActivityIndicator size="large" color='white'/>
            </View>
          </View>
        </Modal>

        {/*弹出框选择*/}
        <Modal visible={this.state.isvisible}
               transparent={true}
               onRequestClose={() => {this.setState({isvisible:false})}}>
            <TouchableWithoutFeedback onPress={()=>this.setState({isvisible:false})}>   
            <View style={{flex:1,backgroundColor:'rgba(14,14,14,0.5)',justifyContent:'flex-end',alignItems:'center'}}>
                  <Animated.View style={{width:BaseComponent.W*355/375,height:BaseComponent.W*87/375,backgroundColor:'#fff',marginBottom:-BaseComponent.W*77/375,bottom:this.state.bottom,borderRadius:BaseComponent.W*10/375}}>
                      <TouchableWithoutFeedback onPress={()=>{
                        thiz.setState({
                          isvisible:false,
                        });
                        if(thiz.state.invitation){
                          thiz.shareWX("session",thiz.state.invitation);
                        }
                      }}>  
                      <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                          <Text style={{fontSize:BaseComponent.W*18/375,color:'#313131'}}>分享微信好友</Text>
                      </View>
                      </TouchableWithoutFeedback>
                      
                      <View style={{width:BaseComponent.W*345/375,height:0.5,backgroundColor:'#D6D6D6',marginLeft:BaseComponent.W*5/375}}></View>
                      <TouchableWithoutFeedback onPress={()=>{
                        thiz.setState({
                          isvisible:false,
                        });
                        if(thiz.state.invitation){
                          thiz.shareWX("timeline",thiz.state.invitation);
                        }
                      }}> 
                       <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                          <Text style={{fontSize:BaseComponent.W*18/375,color:'#313131'}}>分享微信朋友圈</Text>
                      </View>
                      </TouchableWithoutFeedback>
                  </Animated.View>
            </View>
            </TouchableWithoutFeedback>
        </Modal>
     
        {/*状态栏占位*/}
        <View style={{backgroundColor:this.state.visible?"rgba(14,14,14,0.5)":"#fff",
                      height:BaseComponent.SH,
                      position:"relative",
                      zIndex:1,
                      opacity:1,}}></View>
        
        {/*顶部导航栏*/}
        <View style={style.navBar}>
          <View style={{flex:1,flexDirection:"row",justifyContent:"flex-start",alignItems:'center'}}>

            <TouchableWithoutFeedback onPress={()=>this.goBack()}>
            <View style={{width:"15%",height:"100%",backgroundColor:"transparent",justifyContent:"center",marginLeft:BaseComponent.W*10/375}}>
              <Image source={Imgs.back} style={{width:(BaseComponent.W*0.127*0.4),height:(BaseComponent.W*0.127*0.4),marginBottom:ti.select({ios:BaseComponent.W*7/375,android:5})}}></Image>
            </View>
            </TouchableWithoutFeedback>

            <View style={{flexGrow:1,backgroundColor:"transparent",borderWidth:0}}>
              <Text style={{color:"#0A0A0A",width:"100%",textAlign:"center",marginTop:ti.select({ios:"4%",android:"1.6%"}),fontSize:BaseComponent.W*18/375}}>{this.params.title}</Text>
            </View>
            <TouchableWithoutFeedback onPress={()=>{
              thiz.setState({isvisible:true});
              thiz._translateAni();
            }}>
                <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center",}}>
                     <Image source={require('../../image/home/share.png')} style={{width:(BaseComponent.W*0.127*0.5),height:(BaseComponent.W*0.127*0.5),display:"flex"}}></Image>
                </View>
            </TouchableWithoutFeedback>    
          </View>
        </View>
        
        <ImageBackground style={{width:BaseComponent.W*351/375,height:BaseComponent.W*416/375,marginLeft:BaseComponent.W*12/375,display:"none"}} source={require('../../image/home/shareBg.png')}>
            <Text style={{fontSize:BaseComponent.W*15/375,color:'#2D2D2D',textAlign:'center',marginTop:BaseComponent.W*60/375}}>您的邀请码</Text>
            <Text style={{fontSize:BaseComponent.W*30/375,color:'#2D2D2D',textAlign:'center',marginTop:BaseComponent.W*16/375}}>{thiz.state.invitationCode?thiz.state.invitationCode:""}</Text>
            <View style={{width:'100%',flexDirection:'row',justifyContent:'space-around',marginTop:BaseComponent.W*17/375}}>
                <View style={{alignItems:'center',paddingLeft:5}}>
                    <Image style={{width:BaseComponent.W*45/375,height:BaseComponent.W*45/375}} source={require('../../image/home/wxShare.png')}/>
                    <Text style={{fontSize:BaseComponent.W*12/375,color:'#787878',marginTop:5}}>点击右上角分享</Text>
                    <Text style={{fontSize:BaseComponent.W*12/375,color:'#787878',marginTop:2}}>给微信好友</Text>
                </View>
                <View style={{alignItems:'center'}}>
                    <Image style={{width:BaseComponent.W*45/375,height:BaseComponent.W*45/375}} source={require('../../image/home/wxShare.png')}/>
                    <Text style={{fontSize:BaseComponent.W*12/375,color:'#787878',marginTop:5}}>好友在分享页面</Text>
                    <Text style={{fontSize:BaseComponent.W*12/375,color:'#787878',marginTop:2}}>注册成功</Text>
                </View>
                <View style={{alignItems:'center',paddingRight:5}}>
                    <Image style={{width:BaseComponent.W*45/375,height:BaseComponent.W*45/375}} source={require('../../image/home/wxShare.png')}/>
                    <Text style={{fontSize:BaseComponent.W*12/375,color:'#787878',marginTop:5}}>好友下载app登录</Text>
                    <Text style={{fontSize:BaseComponent.W*12/375,color:'#787878',marginTop:2}}>即完成邀请</Text>
                </View>
            </View>
            <Text style={{fontSize:BaseComponent.W*14/375,color:'#2E2E2E',marginLeft:BaseComponent.W*27/375,marginTop:BaseComponent.W*44/375}}>1.邀请码是唯一且不可更改的;</Text>
            <Text style={{fontSize:BaseComponent.W*14/375,color:'#2E2E2E',marginLeft:BaseComponent.W*27/375,marginTop:BaseComponent.W*5/375,paddingRight:BaseComponent.W*15/375,lineHeight:BaseComponent.W*25/375}}>2.分享邀请码页面给好友,好友注册成功后下单,您可获得相应的推广积分;</Text>
            <Text style={{fontSize:BaseComponent.W*14/375,color:'#2E2E2E',marginLeft:BaseComponent.W*27/375,marginTop:BaseComponent.W*5/375}}>3.您邀请的好友可在邀请的人中查看</Text>
            
        </ImageBackground>

        <View style={{width:320,height:320,backgroundColor:"red"}} ref="imgComb">
          <ImageBackground style={{width:320,height:320,justifyContent:"center",alignItems:"center"}} source={require('../../image/home/aptamil2.png')}>
              <Image source={require("../../image/mine/avatar.png")} style={{width:160,height:160}}></Image>
              {/*<Image source={{uri:thiz.state.qrcode}} style={{width:160,height:160}}></Image>*/}
          </ImageBackground>
        </View>

      </View>          
    )
  }
}
