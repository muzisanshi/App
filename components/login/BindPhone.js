import React,{Component} from 'react';
import {Text,View,StyleSheet,Image,TouchableWithoutFeedback,StatusBar,TextInput,Modal,ImageBackground,Keyboard} from 'react-native';
import BaseComponent from '../BaseComponent';
// 导入工具函数库
import T from "../../lib/Tools";
// 导入公共样式
import St from "../../lib/Style";
// 导入图片资源库
import Imgs from "../../lib/Img";

// Tools实例
let ti = T.getInstance();

let style = StyleSheet.create({
  wrapper:{
    backgroundColor:"#FAFAFA",
    flex:1,
    flexDirection:"column",
    alignItems:"stretch",
    justifyContent:"flex-start",
    position:"relative",
  },
  statusBar:{
    backgroundColor:"#FAFAFA",
    height:ti.select({ios:16,android:22}),
    position:"relative",
    zIndex:1,
    opacity:1,
  },
  navBar:{
    backgroundColor:"#FAFAFA",
    height:"7%",
    position:"relative",
    zIndex:1,
    opacity:1,
  
  },
});

export default class BindPhone extends BaseComponent{
  constructor(props){
      super(props);
      this.state={
        isvisible:false,//对话框是否可见
        showPwd:false,//提示密码输入错误
        showYanzhengma:false,//提示验证码输入错误
      }
  }
    render(){
      return (
        <TouchableWithoutFeedback onPress={()=>Keyboard.dismiss()}>  
        <View style={style.wrapper}>
                {/*状态栏占位*/}
                <View style={{backgroundColor:this.state.isvisible?'rgba(14,14,14,0.5)':"#FAFAFA",
                              height:ti.select({ios:16,android:StatusBar.currentHeight}),
                              position:"relative",
                              zIndex:1,
                              opacity:1,}}></View>
                {/*顶部导航栏*/}
                <View style={style.navBar}>
                  <View style={{flex:1,flexDirection:"row",justifyContent:"flex-start",alignItems:'center'}}>

                    <TouchableWithoutFeedback onPress={()=>this.goBack()}>
                    <View style={{width:"15%",height:"100%",backgroundColor:"transparent",justifyContent:"center",marginLeft:BaseComponent.W*10/375}}>
                      <Image source={Imgs.back} style={{width:(BaseComponent.W*0.127*0.4),height:(BaseComponent.W*0.127*0.4),marginTop:ti.select({ios:BaseComponent.W*0.02,android:0})}}></Image>
                    </View>
                    </TouchableWithoutFeedback>

                    <View style={{flexGrow:1,backgroundColor:"transparent",borderWidth:0,}}>
                      <Text style={{color:"#0A0A0A",width:"100%",textAlign:"center",marginTop:ti.select({ios:"4%",android:"1.6%"}),fontSize:BaseComponent.W*18/375}}>{this.params.title}</Text>
                    </View>
                    <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"flex-end"}}>
                    
                    </View>
                  </View>
                </View>
              {/*弹出框*/}
              <Modal visible={this.state.isvisible}
                     transparent={true}
                     onRequestClose={() => {this.setState({isvisible:false})}}>
                  <View style={{flex:1,backgroundColor:'rgba(14,14,14,0.5)',alignItems:'center'}}>
                      <View style={{width:BaseComponent.W*281/375,height:BaseComponent.W*160/375,backgroundColor:'#fff',marginTop:BaseComponent.W*177/375}}>
                            <Text style={{fontSize:BaseComponent.W*15/375,color:'#000000',marginTop:BaseComponent.W*34/375,marginLeft:BaseComponent.W*37/375}}>手机号码已被注册，可直接登录账号。</Text>
                            <View style={{width:'100%',height:0.5,backgroundColor:"#CCCCCC",marginTop:BaseComponent.W*28/375}}></View>
                            <TouchableWithoutFeedback onPress={()=>this.setState({isvisible:false})}>
                            <View style={{flex:1,justifyContent:"center",alignItems:'center'}}>
                                <Text style={{fontSize:BaseComponent.W*16/375,color:'#FAAF1A'}}>确定</Text>      
                            </View>  
                            </TouchableWithoutFeedback>
                      </View>
                  </View>
              </Modal>  

             <View style={{flex:1,backgroundColor:'#FAFAFA'}}>   
           
              
            {/*输入手机号码*/}
              <View style={{width:BaseComponent.W*280/375,height:BaseComponent.W*40/375,borderWidth:1,borderRadius:BaseComponent.W*20/375,
                             borderColor:'#DEDEDE',marginTop:BaseComponent.W*40/375,marginLeft:BaseComponent.W*48/375,flexDirection:'row',alignItems:'center'}}>
                      <Image style={{width:BaseComponent.W*16/375,height:BaseComponent.W*21/375,marginLeft:BaseComponent.W*16/375}} source={require('../../image/home/phone.png')}></Image>
                      <View style={{width:BaseComponent.W*15/375,height:BaseComponent.W*12/375,backgroundColor:'red',marginLeft:BaseComponent.W*8/375}}></View>
                      <Text style={{fontSize:BaseComponent.W*15/375,color:'#252525',marginLeft:BaseComponent.W*7/375}}>+86</Text>
                      <View style={{width:1,height:BaseComponent.W*26/375,backgroundColor:'#CBCBCB',marginLeft:BaseComponent.W*11/375}}></View>
                      <TextInput placeholder="请输入手机号" style={{flex:1,fontSize:BaseComponent.W*15/375,color:'#BABABA',marginLeft:BaseComponent.W*11/375,padding:BaseComponent.W*4/375}} 
                                  underlineColorAndroid='transparent' maxLength={11}></TextInput>
              </View>
              <Text style={{fontSize:BaseComponent.W*10/375,color:'#FE7070',marginTop:BaseComponent.W*14/375,marginLeft:BaseComponent.W*223/375,display:this.state.showPwd?'flex':'none'}}>*请输入正确的手机号码</Text>

             {/*获取验证码*/}
                <View style={{width:BaseComponent.W*280/375,height:BaseComponent.W*40/375,borderWidth:1,borderRadius:BaseComponent.W*20/375,
                             borderColor:'#DEDEDE',marginTop:this.state.showPwd?BaseComponent.W*12/375:BaseComponent.W*26/375,marginLeft:BaseComponent.W*48/375,flexDirection:'row',alignItems:'center'}}>
                      <Image style={{width:BaseComponent.W*19/375,height:BaseComponent.W*21/375,marginLeft:BaseComponent.W*14/375}} source={require('../../image/home/dunpai.png')}></Image>       
                      <TextInput placeholder="请输入验证码" underlineColorAndroid='transparent' style={{width:BaseComponent.W*100/375,marginLeft:BaseComponent.W*12/375,fontSize:BaseComponent.W*15/375,color:'#BABABA',padding:BaseComponent.W*4/375}} maxLength={10}></TextInput>
                      <View style={{width:1,height:BaseComponent.W*26/375,backgroundColor:'#CBCBCB',marginLeft:BaseComponent.W*30/375}}></View>
                      <Text style={{fontSize:BaseComponent.W*15/375,color:'#252525',marginLeft:BaseComponent.W*13/375}}>发送验证码</Text> 
                </View>
                <Text style={{fontSize:BaseComponent.W*10/375,color:'#FE7070',marginTop:BaseComponent.W*14/375,marginLeft:BaseComponent.W*223/375,display:this.state.showYanzhengma?'flex':'none'}}>*请输入正确的验证码</Text>

            {/*确定*/}  
            <TouchableWithoutFeedback onPress={()=>this.setState({isvisible:true})}>                
            <View style={{width:BaseComponent.W*280/375,height:BaseComponent.W*40/375,borderRadius:BaseComponent.W*20/375,backgroundColor:'#CCCCCC',
                          marginTop:BaseComponent.W*45/375,marginLeft:BaseComponent.W*48/375,justifyContent:'center',alignItems:'center'}}>
                  <Text style={{fontSize:BaseComponent.W*18/375,color:'#fff'}}>确    定</Text>
            </View>
            </TouchableWithoutFeedback>
            
            {/*阅读条款*/}
            <View style={{flexDirection:'row'}}>
            <Image style={{width:BaseComponent.W*10/375,height:BaseComponent.W*10/375,marginLeft:BaseComponent.W*109/375,marginTop:BaseComponent.W*13/375}} source={require('../../image/home/disagree.png')}></Image>
            <View>
            <Text style={{fontSize:BaseComponent.W*9/375,color:'#969696',marginLeft:BaseComponent.W*6/375,marginTop:BaseComponent.W*12/375}}>我同意”服务条款”和“隐私权相关政策”</Text>
            <View style={{flexDirection:'row'}}>
            <View style={{width:BaseComponent.W*37/375,height:0.5,backgroundColor:'#BBBBBB',marginLeft:BaseComponent.W*36/375}}></View>
            <View style={{width:BaseComponent.W*61/375,height:0.5,backgroundColor:'#BBBBBB',marginLeft:BaseComponent.W*16/375}}></View>
            </View>
            </View>
            </View>
            </View>              
        </View>
        </TouchableWithoutFeedback>
      )
    }
}