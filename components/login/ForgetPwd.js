import React,{Component} from 'react';
import {Text,View,StyleSheet,Image,TouchableWithoutFeedback,StatusBar,TextInput,Keyboard,Modal,ActivityIndicator} from 'react-native';
import BaseComponent from '../BaseComponent';
import SubmitPwd from './SubmitPwd';
// 导入工具函数库
import T from "../../lib/Tools";
// 导入公共样式
import St from "../../lib/Style";
// 导入图片资源库
import Imgs from "../../lib/Img";
// 转菊花
import Spinner from 'react-native-loading-spinner-overlay';
// 区域码选择
import Selector from 'react-native-modal-dropdown';

// Tools实例
let ti = T.getInstance();

let style = StyleSheet.create({
  wrapper:{
    backgroundColor:"white",
    flex:1,
    flexDirection:"column",
    alignItems:"stretch",
    justifyContent:"flex-start",
    position:"relative",
  },
  statusBar:{
    backgroundColor:"rgb(255,255,255)",
    height:ti.select({ios:16,android:22}),
    position:"relative",
    zIndex:1,
    opacity:1,
  },
  navBar:{
    backgroundColor:"rgb(255,255,255)",
    height:"7%",
    position:"relative",
    zIndex:1,
    opacity:1,
    borderBottomWidth:1,
    borderBottomColor:"rgb(241,241,241)"
  },
});

export default class ForgetPwd extends BaseComponent{

  constructor(props){
    super(props);
    this.state={
      countTxt:"发送验证码",
      isSend:false,
      phone:"",
      code:"",
      isCountingDown:false,// 是否正在倒计时

      visible:false,

      // 区域码
      areaCodes:[],
      curArea:{},
    }
  }

  // 发送验证码
  getVerifyCode(){
    var thiz = this;
    if(!this.state.phone){this.toast("请输入手机号码");return;}
    if(this.state.curArea.id==undefined){this.toast("请选择区域");return;}
    thiz.log("--------getVerifyCode--------","getVerifyCode");
    // 测试获取用户信息
    if(!thiz.state.isCountingDown){
      let data = {
        phoneNoData: {
          countryId: this.state.curArea.id,
          phoneNo: thiz.state.phone
        },
        smsType:"RETRIEVE_PASSWORD"
      };

      // 开始转菊花
      thiz.setState({visible:true});
      
      thiz.getCode(data,function(ret,err){
        thiz.setState({visible:false});
        if(ret&&ret.respCode==="00"){
          thiz.toast("验证码已发送");
          // 执行倒计时
          thiz.setState({
            isCountingDown:true
          });
          thiz.T.countDown(60,function(num){
            if(num>0){
              thiz.setState({
                countTxt:num+"s"
              });
            }else{
              thiz.setState({
                countTxt:"发送验证码",
                isCountingDown:false
              });
            }
          });
        }else{
          thiz.toast(err.msg);
          thiz.setState({
            isCountingDown:false
          });
        }

      });

    }

  }

  // 处理输入
  oninput(value,type){

    var state = this.state;
    state[type] = value;

    this.setState(state);
  }

  next(){
    let thiz = this;
    if(!this.state.phone){thiz.toast("请输入手机号码");return;}
    var num=/^[\d]+$/g;//验证手机号格式是否为纯数字
  
    if(!num.test(thiz.state.phone))
    {
      thiz.toast("手机号格式不正确");
      return ;
    }
    if(!this.state.code){
      thiz.toast("请输入验证码");
      return;
    }else{
      var numcode=/^\d{6}\b/;
      var code=this.state.code;
      if(!numcode.test(code))
      {
        thiz.toast("请检查验证码格式正确");
        return ;
      }
    }
   


    // 校验验证码
    thiz.setState({visible:true});
    thiz.request("sms/checkVerifyCode",{
      method:"POST",
      data:{
        phoneNoData: {
          countryId: this.state.curArea.id,
          phoneNo: thiz.state.phone
        },
        smsVerifyCode:thiz.state.code,
        smsType:"RETRIEVE_PASSWORD"
      },
      success:function(ret){
        thiz.setState({visible:false});
        thiz.log("--------sms/checkVerifyCode-------",ret);
        if(ret&&ret.respCode==="00"){
          // 跳转到下一页
          thiz.navigate('SubmitPwd',{
            title:'忘记密码',
            phone:thiz.state.phone,
            code:thiz.state.code,
            trustId:ret.data.trustId,
            curArea:thiz.state.curArea,
          });
        }
      },
      error:function(err){
        thiz.setState({visible:false});
        thiz.toast(err.msg);
      }
    });
  }

  // 渲染区域码行
  areaRow(item){
    return (
      <View style={{padding:10,flexDirection:"row"}}>
        <Image source={{uri:item.imageAttachment.resourceFullAddress}} style={{width:BaseComponent.W*15/375,height:BaseComponent.W*12/375,marginTop:5,marginRight:5,}}/>
        <Text>
          {item.name+"("+item.phoneNoCode+")"}
        </Text>
      </View>
    );
  }
  // 选择
  onAreaSlect(index,value){
    this.setState({
      curArea:value,
    });
  }

  // 获取区域码
  getArea(){
    let thiz = this;
    thiz.getAreaCode(function(ret,err){
      if(ret&&ret.respCode==="00"){
        thiz.setState({
          areaCodes:ret.data,
          curArea:ret.data[0],
        })
      }else{
        thiz.toast(err.msg);
      }
    });
  }

  render(){
     var len =6;
        var arr = [];
        for(let i=0; i<len; i++){
            arr.push(i);
        }
        let thiz=this;
    return (
        <TouchableWithoutFeedback onPress={()=>Keyboard.dismiss()}>
        <View style={style.wrapper}>

                {/*转菊花*/}
                <Modal
                  visible={this.state.visible}
                  onRequestClose={()=>{this.setState({visible:false})}}
                  transparent={true}
                  animationType={"fade"}>
                  <View style={{backgroundColor:"rgba(0,0,0,0.4)",width:"100%",width:"100%",flex:1,flexDirection:"column",alignItems:"center",justifyContent:"center",position:"relative"}}>
                    <View>
                          <ActivityIndicator size="large" color='white'/>
                    </View>
                  </View>
                </Modal>

                {/*状态栏占位*/}
                <View style={{backgroundColor:"rgb(255,255,255)",
                          height:this.isIphoneX(30,BaseComponent.SH),
                          position:"relative",
                          zIndex:1,
                          opacity:1,}}></View>
                {/*顶部导航栏*/}
                <View style={style.navBar}>
                  <View style={{flex:1,flexDirection:"row",justifyContent:"flex-start"}}>

                    <TouchableWithoutFeedback onPress={()=>this.goBack()}>
                    <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>
                      <Image source={Imgs.back} style={{width:(BaseComponent.W*0.127*0.4),height:(BaseComponent.W*0.127*0.4),marginRight:BaseComponent.W*0.04}}></Image>
                    </View>
                    </TouchableWithoutFeedback>

                    <View style={{flexGrow:1,backgroundColor:"transparent",
                      marginTop:ti.select({ios:(BaseComponent.H*0.02)/2,android:(BaseComponent.H*0.016)/2}),borderRadius:ti.select({ios:(BaseComponent.H*0.05)/2,android:(BaseComponent.H*0.054)/2}),borderColor:"#0A0A0A",
                      borderWidth:0,}}>
                      <Text style={{color:"#0A0A0A",width:"100%",textAlign:"center",marginTop:ti.select({ios:"4.5%",android:"1.6%"}),fontSize:17}}>{this.params.title}</Text>
                    </View>
                    <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>
                      <Image source={require('../../image/home/msg.png')} style={{width:(BaseComponent.W*0.127*0.5),height:(BaseComponent.W*0.127*0.5),display:"none"}}></Image>
                    </View>
                  </View>
                </View>

                <View style={{flex:1,backgroundColor:'#FAFAFA'}}>
                  <View style={{flexDirection:'row'}}>
                  <View style={{width:BaseComponent.W*18/375,height:BaseComponent.W*18/375,backgroundColor:"#FED584",
                                marginTop:BaseComponent.W*31/375,marginLeft:BaseComponent.W*80/375,borderRadius:BaseComponent.W*10/375,
                                justifyContent:'center',alignItems:'center'}}>
                        <Text style={{fontWeight:'bold',color:'black'}}>1</Text>
                  </View>
                {/*arr.map实现虚线功能*/}
                  <View style={{marginTop:BaseComponent.W*40/375,flexDirection:'row'}}>
                      {
                        arr.map((item,index)=>{
                              return (
                                <View style={{width:BaseComponent.W*10/375,height:1,marginLeft:index==0?0:5,backgroundColor:'#CCCCCC'}}></View>
                              )
                        })
                      }          
                  </View>
                  <Image style={{width:BaseComponent.W*25/375,height:BaseComponent.W*25/375,marginTop:BaseComponent.W*28.5/375}} source={require('../../image/home/feiji.png')}></Image>
                  <View style={{marginTop:BaseComponent.W*40/375,flexDirection:'row'}}>
                      {
                        arr.map((item,index)=>{
                              return (
                                <View style={{width:BaseComponent.W*10/375,height:1,marginLeft:index==0?0:5,backgroundColor:'#CCCCCC'}}></View>
                              )
                        })
                      }          
                  </View>
                  <View style={{width:BaseComponent.W*18/375,height:BaseComponent.W*18/375,backgroundColor:"#CCCCCC",
                                marginTop:BaseComponent.W*31/375,borderRadius:BaseComponent.W*10/375,
                                justifyContent:'center',alignItems:'center'}}>
                        <Text style={{fontWeight:'bold',color:'black'}}>2</Text>
                  </View>
                  </View>

                {/*输入手机号*/}
                <View style={{width:BaseComponent.W*280/375,height:BaseComponent.W*40/375,borderWidth:1,borderRadius:BaseComponent.W*20/375,
                             borderColor:'#DEDEDE',marginTop:BaseComponent.W*40/375,marginLeft:BaseComponent.W*48/375,flexDirection:'row',alignItems:'center'}}>
                      <Image style={{width:BaseComponent.W*16/375,height:BaseComponent.W*21/375,marginLeft:BaseComponent.W*16/375}} source={require('../../image/home/phone.png')}></Image>
                      
                      <View style={{width:BaseComponent.W*37/375,flexDirection:"row",height:BaseComponent.W*26/375,position:"relative",}}>
                        <Image source={this.state.curArea&&this.state.curArea.imageAttachment?{uri:this.state.curArea.imageAttachment.resourceFullAddress}:""} style={{width:BaseComponent.W*15/375,height:BaseComponent.W*12/375,marginTop:ti.select({android:5,ios:7}),marginLeft:5,}}/>
                        <Text style={{fontSize:BaseComponent.W*15/375,color:'#252525',marginLeft:BaseComponent.W*7/375,marginTop:ti.select({ios:3,android:0}),}}>{this.state.curArea&&this.state.curArea.phoneNoCode?this.state.curArea.phoneNoCode:""}</Text>
                        <View style={{width:1,height:BaseComponent.W*26/375,backgroundColor:'#CBCBCB',marginLeft:BaseComponent.W*11/375}}></View>

                        {/*选择区域码*/}
                        <Selector onSelect={(index,value)=>this.onAreaSlect(index,value)} renderRow={(item)=>this.areaRow(item)} 
                          style={{position:"absolute",left:0,top:0,opacity:0,
                          backgroundColor:"transparent",height:BaseComponent.W*26/375,
                          alignItems:"center",justifyContent:"center",}} 
                          options={this.state.areaCodes}/>

                      </View>
                      
                      <TextInput onChangeText={(phone)=>this.oninput(phone,'phone')} keyboardType="numeric" placeholder="请输入手机号" style={{flex:1,fontSize:BaseComponent.W*15/375,color:'#3a3a3a',marginLeft:BaseComponent.W*44/375,padding:BaseComponent.W*4/375}} 
                                 placeholderTextColor="##BABABA" underlineColorAndroid='transparent' maxLength={11}></TextInput>
                </View>

                {/*获取验证码*/}
                <View style={{width:BaseComponent.W*280/375,height:BaseComponent.W*40/375,borderWidth:1,borderRadius:BaseComponent.W*20/375,
                             borderColor:'#DEDEDE',marginTop:BaseComponent.W*40/375,marginLeft:BaseComponent.W*48/375,flexDirection:'row',alignItems:'center'}}>
                      <Image style={{width:BaseComponent.W*19/375,height:BaseComponent.W*21/375,marginLeft:BaseComponent.W*14/375}} source={require('../../image/home/dunpai.png')}></Image>       
                      <TextInput onChangeText={(code)=>this.oninput(code,'code')} keyboardType="numeric" placeholder="请输入验证码" placeholderTextColor="##BABABA" underlineColorAndroid='transparent' style={{width:BaseComponent.W*100/375,marginLeft:BaseComponent.W*12/375,fontSize:BaseComponent.W*15/375,color:'#3a3a3a',padding:BaseComponent.W*4/375}} maxLength={6}></TextInput>
                      <View style={{width:1,height:BaseComponent.W*26/375,backgroundColor:'#CBCBCB',marginLeft:BaseComponent.W*30/375}}></View>
                      <Text onPress={()=>{this.getVerifyCode()}} style={{fontSize:BaseComponent.W*15/375,color:'#252525',marginLeft:BaseComponent.W*13/375}}>{this.state.countTxt}</Text> 
                </View>    

                {/*下一步*/}
                <TouchableWithoutFeedback onPress={()=>this.next()}>  
                <View style={{width:BaseComponent.W*280/375,height:BaseComponent.W*40/375,borderRadius:BaseComponent.W*20/375,backgroundColor:thiz.state.phone==""||thiz.state.code==""?"#CCCCCC":'#FED584',
                             marginTop:BaseComponent.W*77/375,marginLeft:BaseComponent.W*48/375,alignItems:'center',justifyContent:'center'}}>
                        <Text style={{fontSize:BaseComponent.W*18/375,color:"#FFFFFF"}}>下一步</Text>
                </View>
                </TouchableWithoutFeedback>                      
              </View>
        </View>
      </TouchableWithoutFeedback>  
    )
  }

  componentDidMount(){
    
    // 加载数据
    this.getArea();

    this.emit("add_page",{
      name:"ForgetPwd",
      data:{
        key:this.props.navigation.state.key,
      }
    });

  }

}