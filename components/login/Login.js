import React,{Component} from 'react';
import {Text,View,StyleSheet,Image,TouchableWithoutFeedback,StatusBar,TextInput,Modal,ImageBackground,Keyboard,ActivityIndicator} from 'react-native';
import BaseComponent from '../BaseComponent';
import ForgetPwd from './ForgetPwd';
import Register from './Register';
import BindPhone from './BindPhone';
// 导入工具函数库
import T from "../../lib/Tools";
// 导入公共样式
import St from "../../lib/Style";
// 导入图片资源库
import Imgs from "../../lib/Img";
// 转菊花
import Spinner from 'react-native-loading-spinner-overlay';
// 区域码选择
import Selector from 'react-native-modal-dropdown';

import Wxpay from '@yyyyu/react-native-wechat';

// Tools实例
let ti = T.getInstance();

let style = StyleSheet.create({
  wrapper:{
    backgroundColor:"white",
    flex:1,
    flexDirection:"column",
    alignItems:"stretch",
    justifyContent:"flex-start",
    position:"relative",
  },
  statusBar:{
    backgroundColor:"rgb(255,255,255)",
    height:ti.select({ios:16,android:22}),
    position:"relative",
    zIndex:1,
    opacity:1,
  },
  navBar:{
    backgroundColor:"rgb(255,255,255)",
    height:"7%",
    position:"relative",
    zIndex:1,
    opacity:1,
  },
});

export default class Login extends BaseComponent{
  constructor(props){
    super(props);
    this.state={
      isPwdLogin:true,
      countTxt:"发送验证码",
      isSend:false,
      phone:"",
      pwd:"",
      code:"",
      isCountingDown:false,// 是否正在倒计时
      loginType:0,//0是账号密码登录，1是验证码登录，2是微信登录

      visible:false,

      // 区域码
      areaCodes:[],
      curArea:{},
    }
  }

  // 发送验证码
  getVerifyCode(){
    var thiz = this;
    if(!this.state.phone){this.toast("请输入手机号码");return;}
    if(this.state.curArea.id==undefined){this.toast("请选择区域");return;}
    thiz.log("--------getVerifyCode--------","getVerifyCode");
    // 测试获取用户信息
    if(!thiz.state.isCountingDown){
      let data = {
        phoneNoData: {
          countryId: this.state.curArea.id,
          phoneNo: thiz.state.phone
        },
        smsType:"LOGIN"
      };

      // 开始转菊花
      thiz.setState({visible:true});
      
      thiz.getCode(data,function(ret,err){
        thiz.setState({visible:false});
        if(ret&&ret.respCode==="00"){
          thiz.toast("验证码已发送");
          // 执行倒计时
          thiz.setState({
            isCountingDown:true
          });
          thiz.T.countDown(60,function(num){
            if(num>0){
              thiz.setState({
                countTxt:num+"s"
              });
            }else{
              thiz.setState({
                countTxt:"发送验证码",
                isCountingDown:false
              });
            }
          });
        }else{
          thiz.toast(err.msg);
          thiz.setState({
            isCountingDown:false
          });
        }

      });

    }

  };

  //验证手机号是否是纯数字
  verifyphone=(phone)=>{
    var thiz=this;
    var num=/^[\d]+$/g;
    thiz.log("-------------------------phone---------------------------",phone);

    if(!num.test(phone))
    {
      thiz.toast("手机号格式不正确");
      return ;
    }
  };

  // 处理输入
  oninput(value,type){

    var state = this.state;
    state[type] = value;

    this.setState(state);
  }

  // 登录
  login(){

    let thiz = this;
    var pwd=/^[a-zA-Z]\w{5,17}$/;//验证密码格式是否正确;
    thiz.log("--------login-------","login");
    //测试账号
    // let phone = "13800138000";
    // let pwd = "111111";

    // 校验表单输入
    if(!this.state.phone){this.toast("请输入手机号码");return;}
    var num=/^[\d]+$/g;//验证手机号格式是否为纯数字
  
    if(!num.test(thiz.state.phone))
    {
      thiz.toast("手机号格式不正确");
      return ;
    }
    if(this.state.curArea.id==undefined){this.toast("请选择区域");return;}
    if(this.state.isPwdLogin){
      if(!this.state.pwd){this.toast("请输入密码");return;}
       // if(!pwd.test(thiz.state.pwd))
       //  {
       //    thiz.toast("密码格式不正确");
       //    console.log("1111111111111111111111111111111111111");
       //    return ;
       //  }
    }
    else
    {
      if(!this.state.code&&!this.state.isPwdLogin){this.toast("请输入验证码");return;}
      var numcode=/^\d{6}\b/;
      var code=this.state.code;
      if(!numcode.test(code))
      {
        thiz.toast("请检查验证码格式正确");
        return ;
      }
    }
   

    let data = {};

    if(this.state.loginType==0){
      data = {
        loginType:"PHONE_NO_PASSWORD_LOGIN",
        phoneNoPasswordLoginData:{
          phoneNoData: {
            countryId: this.state.curArea.id,
            phoneNo: thiz.state.phone
          },
          password:thiz.state.pwd,
        }
      }
    }
    if(this.state.loginType==1){
      data = {
        loginType:"SMS_LOGIN",
        smsLoginData:{
          phoneNoData: {
            countryId: this.state.curArea.id,
            phoneNo: thiz.state.phone
          },
          smsVerifyCode:thiz.state.code,
        }
      }
    }
    thiz.setState({visible:true});
    // 直接前往登录
    thiz.doLogin(data,function(ret,err){
      thiz.log("--------doLogin_ret-------",ret);
      thiz.log("--------doLogin_err-------",err);
      
      // thiz.setState({visible:false});
      
      if(ret&&ret.respCode==="00"){// 登录成功
        // thiz.toast("登录成功");
        // 保存用户账号
        thiz.saveAccount(ret.data);
        thiz.emit("login_success","登录成功");
        // 延时关闭
        setTimeout(function(){
          thiz.setState({visible:false});
          thiz.goBack();
        },1000);
        
      }else{// 登录失败
        thiz.toast(err.msg);
        thiz.setState({visible:false});
      }
      
    });

  }

  // 获取区域码
  getArea(){
    let thiz = this;
    thiz.getAreaCode(function(ret,err){
      if(ret&&ret.respCode==="00"){
        thiz.setState({
          areaCodes:ret.data,
          curArea:ret.data[0],
        })
      }else{
        thiz.toast(err.msg);
      }
    });
  }

  // 渲染区域码行
  areaRow(item){
    return (
      <View style={{padding:10,flexDirection:"row"}}>
        <Image source={item.imageAttachment?{uri:item.imageAttachment.resourceFullAddress}:""} style={{width:BaseComponent.W*15/375,height:BaseComponent.W*12/375,marginTop:5,marginRight:5,}}/>
        <Text>
          {item.name+"("+item.phoneNoCode+")"}
        </Text>
      </View>
    );
  }
  // 选择
  onAreaSlect(index,value){
    if(value){
      this.setState({
        curArea:value,
      });
    }
  }

  wxLogin(){
    let thiz = this;
    // this.navigate('BindPhone',{title:'绑定手机号码'});
    // 判断微信是否安装
    let callback = async function(isInstalled){
      if(!isInstalled){
        thiz.toast("请先安装微信");
        return;
      }
      Wxpay.sendAuthRequest({
        state:"app_login_request"
      }).then(function(ret){
        thiz.log("--------wxLogin_ret--------",ret);
      });
    };
    Wxpay.isWXAppInstalled().then(callback);
  }

  render(){
    let thiz = this;
    return (
        <TouchableWithoutFeedback onPress={()=>{Keyboard.dismiss()}}>
        
        <View style={style.wrapper}>

                {/*转菊花*/}
                <Modal
                  visible={this.state.visible}
                  onRequestClose={()=>{this.setState({visible:false})}}
                  transparent={true}
                  animationType={"fade"}>
                  <View style={{backgroundColor:"rgba(0,0,0,0.4)",width:"100%",width:"100%",flex:1,flexDirection:"column",alignItems:"center",justifyContent:"center",position:"relative"}}>
                    <View>
                          <ActivityIndicator size="large" color='white'/>
                    </View>
                  </View>
                </Modal>

                {/*状态栏占位*/}
                <View style={style.statusBar}></View>
                {/*顶部导航栏*/}
                <View style={style.navBar}>
                  <View style={{flex:1,flexDirection:"row",justifyContent:"flex-start"}}>

                    <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>
                    </View>

                    <View style={{flexGrow:1,backgroundColor:"transparent",
                      marginTop:ti.select({ios:(BaseComponent.H*0.02)/2,android:(BaseComponent.H*0.016)/2}),borderRadius:ti.select({ios:(BaseComponent.H*0.05)/2,android:(BaseComponent.H*0.054)/2}),borderColor:"#0A0A0A",
                      borderWidth:0,}}>
                      <Text style={{color:"#0A0A0A",width:"100%",textAlign:"center",marginTop:ti.select({ios:"4%",android:"1.6%"}),fontSize:17}}></Text>
                    </View>
                    <TouchableWithoutFeedback onPress={()=>{
                      thiz.navigate("Home");
                    }}>
                    <View style={{width:"25%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>
                      <Text style={{color:'#232323',fontSize:BaseComponent.W*15/375,marginRight:BaseComponent.W*12/375}}>随便逛逛>></Text>
                    </View>
                    </TouchableWithoutFeedback>

                  </View>
                </View>

             {/*app名字*/}
              <Image style={{width:BaseComponent.W*120/375,height:BaseComponent.W*50/375,marginTop:BaseComponent.W*10/375,marginLeft:BaseComponent.W*132/375}} source={require('../../image/home/xiaoyangjiang.png')}></Image>   
        
             {/*选择登陆方式*/}
             <View style={{flexDirection:'row'}}>
             <TouchableWithoutFeedback onPress={()=>this.setState({isPwdLogin:true,loginType:0})}>
             <View style={{width:BaseComponent.W*60/375,height:BaseComponent.W*25/375,marginTop:BaseComponent.W*33/375,marginLeft:BaseComponent.W*100/375,justifyContent:'space-between',alignItems:'center'}}>
                <Text style={{fontSize:BaseComponent.W*13/375,color:'#252525'}}>密码登陆</Text>
                <View style={{width:"100%",height:2,backgroundColor:'#606060',display:this.state.isPwdLogin?'flex':'none'}}></View>
             </View>
             </TouchableWithoutFeedback>
             <TouchableWithoutFeedback onPress={()=>this.setState({isPwdLogin:false,loginType:1})}>   
             <View style={{width:BaseComponent.W*70/375,height:BaseComponent.W*25/375,marginTop:BaseComponent.W*33/375,justifyContent:'space-between',alignItems:'center',marginLeft:BaseComponent.W*54/375}}>
                <Text style={{fontSize:BaseComponent.W*13/375,color:'#252525'}}>验证码登陆</Text>
                <View style={{width:"100%",height:2,backgroundColor:'#606060',display:this.state.isPwdLogin?'none':'flex'}}></View>
             </View>
             </TouchableWithoutFeedback>
             </View>

             {/*输入手机号码*/}
            <View style={{width:BaseComponent.W*280/375,height:BaseComponent.W*40/375,borderWidth:1,borderRadius:BaseComponent.W*20/375,
                             borderColor:'#DEDEDE',marginTop:BaseComponent.W*30/375,marginLeft:BaseComponent.W*48/375,flexDirection:'row',alignItems:'center'}}>
                      <Image style={{width:BaseComponent.W*16/375,height:BaseComponent.W*21/375,marginLeft:BaseComponent.W*16/375}} source={require('../../image/home/phone.png')}></Image>
                      
                      <View style={{width:BaseComponent.W*37/375,flexDirection:"row",height:BaseComponent.W*26/375,position:"relative",}}>
                        <Image source={this.state.curArea&&this.state.curArea.imageAttachment?{uri:this.state.curArea.imageAttachment.resourceFullAddress}:""} style={{width:BaseComponent.W*15/375,height:BaseComponent.W*12/375,marginTop:ti.select({android:5,ios:7}),marginLeft:5,}}/>
                        <Text style={{fontSize:BaseComponent.W*15/375,color:'#252525',marginLeft:BaseComponent.W*7/375,marginTop:ti.select({ios:3,android:0}),}}>{this.state.curArea&&this.state.curArea.phoneNoCode?this.state.curArea.phoneNoCode:""}</Text>
                        <View style={{width:1,height:BaseComponent.W*26/375,backgroundColor:'#CBCBCB',marginLeft:BaseComponent.W*11/375}}></View>
                        
                        {/*选择区域码*/}
                        <Selector onSelect={(index,value)=>this.onAreaSlect(index,value)} renderRow={(item)=>this.areaRow(item)} 
                          style={{position:"absolute",left:0,top:0,opacity:0,
                          backgroundColor:"transparent",height:BaseComponent.W*26/375,
                          alignItems:"center",justifyContent:"center",}} 
                          options={this.state.areaCodes}/>
                          
                      </View>

                      <TextInput onChangeText={(phone)=>this.oninput(phone,'phone')} keyboardType="numeric" placeholderTextColor="##BABABA" placeholder="请输入手机号" style={{flex:1,fontSize:BaseComponent.W*15/375,color:'#3a3a3a',marginLeft:BaseComponent.W*44/375,padding:BaseComponent.W*4/375}} 
                                  underlineColorAndroid='transparent' maxLength={11}></TextInput>
            </View>

          {/*输入密码*/}
          <View style={{width:BaseComponent.W*280/375,height:BaseComponent.W*40/375,borderWidth:1,borderRadius:BaseComponent.W*20/375,display:this.state.isPwdLogin?"flex":'none',
                             borderColor:'#DEDEDE',marginTop:BaseComponent.W*30/375,marginLeft:BaseComponent.W*48/375,flexDirection:'row',alignItems:'center'}}>
                  <Image style={{width:BaseComponent.W*17/375,height:BaseComponent.W*20/375,marginLeft:BaseComponent.W*15/375}} source={require('../../image/home/suo.png')}></Image>
                  <TextInput  secureTextEntry={true} onChangeText={(pwd)=>this.oninput(pwd,'pwd')} placeholderTextColor="##BABABA" placeholder="请输入密码" underlineColorAndroid='transparent' style={{fontSize:BaseComponent.W*15/375,color:'#3a3a3a',flex:1,marginLeft:BaseComponent.W*15/375,padding:BaseComponent.W*4/375}} maxLength={18}></TextInput>
          </View>

          {/*输入验证码*/}
          <View style={{width:BaseComponent.W*280/375,height:BaseComponent.W*40/375,borderWidth:1,borderRadius:BaseComponent.W*20/375,display:this.state.isPwdLogin?"none":'flex',
                             borderColor:'#DEDEDE',marginTop:BaseComponent.W*30/375,marginLeft:BaseComponent.W*48/375,flexDirection:'row',alignItems:'center'}}>
                    <Image style={{width:BaseComponent.W*19/375,height:BaseComponent.W*21/375,marginLeft:BaseComponent.W*14/375}} source={require('../../image/home/dunpai.png')}></Image>       
                    <TextInput onChangeText={(code)=>this.oninput(code,'code')} placeholderTextColor="##BABABA" placeholder="请输入验证码" underlineColorAndroid='transparent' style={{width:BaseComponent.W*100/375,marginLeft:BaseComponent.W*12/375,fontSize:BaseComponent.W*15/375,color:'#3a3a3a',padding:BaseComponent.W*4/375}} maxLength={6}></TextInput>
                    <View style={{width:1,height:BaseComponent.W*26/375,backgroundColor:'#CBCBCB',marginLeft:BaseComponent.W*30/375}}></View>
                    <Text style={{fontSize:BaseComponent.W*15/375,color:'#252525',textAlign:'center',flex:1}} onPress={()=>this.getVerifyCode()}>{this.state.countTxt}</Text> 
          </View>

          {/*登陆*/}
          <TouchableWithoutFeedback onPress={()=>this.login()}>  
          <View style={{width:BaseComponent.W*280/375,height:BaseComponent.W*40/375,borderRadius:BaseComponent.W*20/375,backgroundColor:thiz.state.phone==""||(thiz.state.pwd==""&&thiz.state.code=="")?'#CCCCCC':'#FED584',
                          marginTop:BaseComponent.W*45/375,marginLeft:BaseComponent.W*48/375,justifyContent:'center',alignItems:'center'}}>
                  <Text style={{fontSize:BaseComponent.W*18/375,color:'#fff'}}>登    录</Text>
          </View>
          </TouchableWithoutFeedback>

          {/*忘记密码*/}
          <TouchableWithoutFeedback onPress={()=>this.navigate('ForgetPwd',{title:'忘记密码'})}>  
          <Text style={{fontSize:BaseComponent.W*14/375,color:'#808080',marginTop:BaseComponent.W*10/375,marginLeft:BaseComponent.W*256/375,display:this.state.isPwdLogin?'flex':'none'}}>忘记密码?</Text>
          </TouchableWithoutFeedback>

          {/*去注册*/}
          <TouchableWithoutFeedback onPress={()=>this.navigate('Register',{title:'注册'})}>
          <View style={{width:BaseComponent.W*280/375,height:BaseComponent.W*40/375,borderRadius:BaseComponent.W*20/375,borderColor:'#FED584',borderWidth:1,
                          marginTop:BaseComponent.W*23/375,marginLeft:BaseComponent.W*48/375,justifyContent:'center',alignItems:'center'}}>
                  <Text style={{fontSize:BaseComponent.W*18/375,color:'#FED584'}}>去注册</Text>
          </View>
          </TouchableWithoutFeedback>

          {/*微信登陆*/}
          <TouchableWithoutFeedback onPress={()=>{this.wxLogin()}}>
          <View style={{flex:1,justifyContent:'center',alignItems:'center',display:"none"}}>
                <ImageBackground style={{width:BaseComponent.W*50/375,height:BaseComponent.W*50/375,borderRadius:BaseComponent.W*25/375,justifyContent:'center',alignItems:'center'}} source={require('../../image/home/weixin.png')}>
                    <Image style={{width:BaseComponent.W*25/375,height:BaseComponent.W*25/375}} source={require('../../image/home/weixin1.png')}></Image>
                </ImageBackground>
                <Text style={{fontSize:BaseComponent.W*14/375,color:'#A9A9A9',marginTop:BaseComponent.W*10/375}}>微信登录</Text>
          </View>
          </TouchableWithoutFeedback>   
        </View>
        </TouchableWithoutFeedback>
    )
  }

  componentDidMount(){
    // 加载数据
    this.getArea();
    this.emit("add_page",{
      name:"Login",
      data:{
        key:this.props.navigation.state.key,
      }
    });
  }
}