import React,{Component} from 'react';
import {Text,View,StyleSheet,Image,TouchableWithoutFeedback,StatusBar,TextInput,Modal,ImageBackground,Keyboard,ActivityIndicator,Linking} from 'react-native';
import BaseComponent from '../BaseComponent';
// 导入工具函数库
import T from "../../lib/Tools";
// 导入公共样式
import St from "../../lib/Style";
// 导入图片资源库
import Imgs from "../../lib/Img";
// 转菊花
import Spinner from 'react-native-loading-spinner-overlay';
// 区域码选择
import Selector from 'react-native-modal-dropdown';

// Tools实例
let ti = T.getInstance();

let style = StyleSheet.create({
  wrapper:{
    backgroundColor:"#FAFAFA",
    flex:1,
    flexDirection:"column",
    alignItems:"stretch",
    justifyContent:"flex-start",
    position:"relative",
  },
  statusBar:{
    backgroundColor:"#FAFAFA",
    height:ti.select({ios:16,android:22}),
    position:"relative",
    zIndex:1,
    opacity:1,
  },
  navBar:{
    backgroundColor:"#FAFAFA",
    height:"7%",
    position:"relative",
    zIndex:1,
    opacity:1,
  
  },
});

export default class Register extends BaseComponent{

  constructor(props){
      super(props);
      this.state={
        isvisible:false,//对话框是否可见
        showPwd:false,//提示密码输入错误
        showYanzhengma:false,//提示验证码输入错误,
        phone:"",
        code:"",
        pwd:"",
        isReadProtocol:false,
        isCountingDown:false,
        countTxt:"发送验证码",
        phonecorrectly:true,//判断输入的电话号码是否是纯数字,true表示正确
        visible:false,
        codecorrectly:true,//判断输入的验证码是否是纯数字，true表示正确
        // 区域码
        areaCodes:[],
        curArea:{},
      }
  } 

  // 获取区域码
  getArea(){
    let thiz = this;
    thiz.getAreaCode(function(ret,err){
      thiz.log("------------------------------获取区域码--------------------------------",ret);
      if(ret&&ret.respCode==="00"){
        thiz.setState({
          areaCodes:ret.data,
          curArea:ret.data[0],
        })
      }else{
        thiz.toast(err.msg);
      }
    });
  }

  // 显示国家选择
  showAreaCode(){
    let thiz = this;
    thiz.log("--------showAreaCode--------","showAreaCode");
    Selector.show();
  }

  // 发送验证码
  getVerifyCode(){
    var thiz = this;
    if(!this.state.phone){this.toast("请输入手机号码");return;}
    thiz.log("--------getVerifyCode--------","getVerifyCode");
    if(this.state.curArea.id==undefined){this.toast("请选择区域");return;}
    // 测试获取用户信息
    if(!thiz.state.isCountingDown){
      let data = {
        phoneNoData: {
          countryId: thiz.state.curArea.id,
          phoneNo: thiz.state.phone
        },
        smsType:"REGISTER"
      };
      thiz.setState({visible:true});
      thiz.getCode(data,function(ret,err){
        thiz.setState({visible:false});
        if(ret&&ret.respCode==="00"){
          thiz.toast("验证码已发送");
          // 执行倒计时
          thiz.setState({
            isCountingDown:true
          });
          thiz.T.countDown(60,function(num){
            if(num>0){
              thiz.setState({
                countTxt:num+"s"
              });
            }else{
              thiz.setState({
                countTxt:"发送验证码",
                isCountingDown:false
              });
            }
          });
        }else{
          thiz.toast(err.msg);
          thiz.setState({
            isCountingDown:false
          });
        }

      });

    }

  }

  // 处理输入
  oninput(value,type){
    
    var state = this.state;
    state[type] = value;
    
    this.setState(state);
  };
  // verifyPwd=(password)=>{
  //   var thiz=this;
  //   var pwd=/^[a-zA-Z]\w{5,17}$/;
  //   thiz.log("-----------------password---------------------",password);
  //   if(pwd.test(password))
  //   {
  //     console.log("密码格式正确");
  //   }else{
  //     console.log("密码格式不正确");
  //   }
  // }

  // 注册
  register(){
    var thiz = this;
    var pwd=/^[a-zA-Z]\w{5,17}$/;
    console.log("------------------password----------------------",thiz.state.pwd);
    
    // this.setState({isvisible:true});
    // 校验表单输入
    if(!this.state.phone){this.toast("请输入手机号码");return;}
    if(this.state.curArea.id==undefined){this.toast("请选择区域");return;}
    thiz.log("-------------------------------thiz.state.curArea---------------------------------",this.state.curArea);
    // if(!this.state.phone){this.toast("请输入手机号码");}
    if(!this.state.code){this.toast("请输入验证码");return;}
    if(!this.state.pwd){this.toast("请输入密码");return;}
    if(!this.state.isReadProtocol){this.toast("请同意服务条款和隐私相关政策");return;}
    if(!this.state.phonecorrectly){
      this.toast("请检查手机号码格式是否正确");
      return ;
    }
    if(!this.state.codecorrectly)
    {
      this.toast("请检查验证码格式是否正确");
      return ;
    }
    if(!pwd.test(thiz.state.pwd))
    {
      thiz.toast("密码格式不正确，以字母开头");
      console.log("1111111111111111111111111111111111111");
      return ;
    }
    if(this.state.phone==""||this.state.code==""||this.state.pwd==""||!this.state.isReadProtocol||!this.state.phonecorrectly||!this.state.codecorrectly)
    {
      return ;
    }
    thiz.setState({visible:true});

    thiz.request("user/register",{
      method:"POST",
      data:{
        phoneNoData: {
          countryId: thiz.state.curArea.id,
          phoneNo: thiz.state.phone
        },
        smsVerifyCode:thiz.state.code,
        password:thiz.state.pwd
      },
      success:function(ret){
        // thiz.setState({visible:false});
        thiz.log("--------user/register-------",ret);
        if(ret&&ret.respCode==="00"){
          thiz.toast("注册成功");
          let ldata = {
            loginType:"PHONE_NO_PASSWORD_LOGIN",
            phoneNoPasswordLoginData:{
              phoneNoData: {
                countryId: thiz.state.curArea.id,
                phoneNo: thiz.state.phone
              },
              password:thiz.state.pwd,
            }
          };
          
          thiz.doLogin(ldata,function(ret,err){
            
            thiz.setState({visible:false});
            
            if(ret&&ret.respCode==="00"){// 登录成功
              thiz.toast("登录成功");
              // 保存用户账号
              thiz.saveAccount(ret.data);
              thiz.emit("login_success","登录成功");
              // 延时关闭
              setTimeout(function(){
                thiz.navigate("Home");
              },500);

            }else{// 登录失败
              thiz.toast(err.msg);
            }
            
          });

        }
      },
      error:function(err){
        thiz.setState({isvisible:true});
        // thiz.toast(err.msg);
      }
    });
  };

  //验证手机号是否是纯数字
  verifyphone=(phone)=>{
    var thiz=this;
    var num=/^[\d]+$/g;
    thiz.log("-------------------------phone---------------------------",phone);

    if(num.test(phone)||phone.length==0||phone=="")
    {
      thiz.setState({phonecorrectly:true});
    }else{
      thiz.setState({phonecorrectly:false});
    }
  };

  //验证验证码是否是纯数字和长度是否为6位
  verifyCode=(code)=>{
      var thiz=this;
      var num=/^\d{6}\b/;
      thiz.log("-------------------------code---------------------------",code);
      if(num.test(code)||code=="")
      {
        thiz.setState({codecorrectly:true,showYanzhengma:true});
      }else{
        thiz.setState({codecorrectly:false,showYanzhengma:false});
      }
  };

  
  // 渲染区域码行
  areaRow(item){
    return (
      <View style={{padding:10,flexDirection:"row"}}>
        <Image source={{uri:item.imageAttachment.resourceFullAddress}} style={{width:BaseComponent.W*15/375,height:BaseComponent.W*12/375,marginTop:5,marginRight:5,}}/>
        <Text>
          {item.name+"("+item.phoneNoCode+")"}
        </Text>
      </View>
    );
  }
  // 选择
  onAreaSlect(index,value){
    this.setState({
      curArea:value,
    });
  }

  render(){
    let thiz=this;
    return (
      <TouchableWithoutFeedback onPress={()=>Keyboard.dismiss()}>  
      <View style={style.wrapper}>

              {/*转菊花*/}
              <Modal
                visible={this.state.visible}
                onRequestClose={()=>{this.setState({visible:false})}}
                transparent={true}
                animationType={"fade"}>
                <View style={{backgroundColor:"rgba(0,0,0,0.4)",width:"100%",width:"100%",flex:1,flexDirection:"column",alignItems:"center",justifyContent:"center",position:"relative"}}>
                  <View>
                        <ActivityIndicator size="large" color='white'/>
                  </View>
                </View>
              </Modal>

              {/*状态栏占位*/}

              <View style={{backgroundColor:this.state.isvisible?'rgba(14,14,14,0.5)':"#FAFAFA",
                            height:thiz.isIphoneX(30,BaseComponent.SH),
                            position:"relative",
                            zIndex:1,
                            opacity:1,}}></View>
              {/*顶部导航栏*/}
              <View style={style.navBar}>
                <View style={{flex:1,flexDirection:"row",justifyContent:"flex-start",alignItems:'center'}}>

                  <TouchableWithoutFeedback onPress={()=>this.goBack()}>
                  <View style={{width:"15%",height:"100%",backgroundColor:"transparent",justifyContent:"center",marginLeft:BaseComponent.W*10/375}}>
                    <Image source={Imgs.back} style={{width:(BaseComponent.W*0.127*0.4),height:(BaseComponent.W*0.127*0.4),marginBottom:3}}></Image>
                  </View>
                  </TouchableWithoutFeedback>

                  <View style={{flexGrow:1,backgroundColor:"transparent"}}>
                    <Text style={{color:"#0A0A0A",width:"100%",textAlign:"center",fontSize:BaseComponent.W*18/375,marginLeft:BaseComponent.W*0.075}}>{this.params.title}</Text>
                  </View>
                  <TouchableWithoutFeedback  onPress={()=>{
                      thiz.navigate("Home");
                  }}>
                  <View style={{width:"30%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"flex-end"}}>
                    <Text style={{fontSize:BaseComponent.W*15/375,color:'#232323',marginRight:BaseComponent.W*12/375}}>随便逛逛>></Text>
                  </View>
                  </TouchableWithoutFeedback>
                </View>
              </View>
            {/*弹出框*/}
            <Modal visible={this.state.isvisible}
                   transparent={true}
                   onRequestClose={() => {this.setState({isvisible:false})}}>
                <View style={{flex:1,backgroundColor:'rgba(14,14,14,0.5)',alignItems:'center'}}>
                    <View style={{width:BaseComponent.W*281/375,height:BaseComponent.W*160/375,backgroundColor:'#fff',marginTop:BaseComponent.W*177/375}}>
                          <Text style={{fontSize:BaseComponent.W*15/375,color:'#000000',marginTop:BaseComponent.W*34/375,marginLeft:BaseComponent.W*37/375}}>手机号码已被注册，可直接登录账号。</Text>
                          <View style={{width:'100%',height:0.5,backgroundColor:"#CCCCCC",marginTop:BaseComponent.W*28/375}}></View>
                          <TouchableWithoutFeedback onPress={()=>this.setState({isvisible:false})}>
                          <View style={{flex:1,justifyContent:"center",alignItems:'center'}}>
                              <Text style={{fontSize:BaseComponent.W*16/375,color:'#FAAF1A'}}>确定</Text>      
                          </View>  
                          </TouchableWithoutFeedback>
                    </View>
                </View>
            </Modal>  

           <View style={{flex:1,backgroundColor:'#FAFAFA'}}>   
          {/*app名字*/}
           <Image style={{width:BaseComponent.W*120/375,height:BaseComponent.W*50/375,marginTop:BaseComponent.W*10/375,marginLeft:BaseComponent.W*132/375}} source={require('../../image/home/xiaoyangjiang.png')}></Image>   
            
          {/*输入手机号码*/}
            <View style={{width:BaseComponent.W*280/375,height:BaseComponent.W*40/375,borderWidth:1,borderRadius:BaseComponent.W*20/375,
                           borderColor:'#DEDEDE',marginTop:BaseComponent.W*40/375,marginLeft:BaseComponent.W*48/375,flexDirection:'row',alignItems:'center'}}>
                    <Image style={{width:BaseComponent.W*16/375,height:BaseComponent.W*21/375,marginLeft:BaseComponent.W*16/375}} source={require('../../image/home/phone.png')}></Image>

                    <View style={{width:BaseComponent.W*37/375,flexDirection:"row",height:BaseComponent.W*26/375,position:"relative",}}>
                      <Image source={this.state.curArea&&this.state.curArea.imageAttachment?{uri:this.state.curArea.imageAttachment.resourceFullAddress}:""} style={{width:BaseComponent.W*15/375,height:BaseComponent.W*12/375,marginTop:ti.select({android:5,ios:7}),marginLeft:5,}}/>
                      <Text style={{fontSize:BaseComponent.W*15/375,color:'#252525',marginLeft:BaseComponent.W*7/375,marginTop:ti.select({ios:3,android:0}),}}>{this.state.curArea&&this.state.curArea.phoneNoCode?this.state.curArea.phoneNoCode:""}</Text>
                      <View style={{width:1,height:BaseComponent.W*26/375,backgroundColor:'#CBCBCB',marginLeft:BaseComponent.W*11/375}}></View>

                      {/*选择区域码*/}
                      <Selector onSelect={(index,value)=>this.onAreaSlect(index,value)} renderRow={(item)=>this.areaRow(item)} 
                        style={{position:"absolute",left:0,top:0,opacity:0,
                        backgroundColor:"transparent",height:BaseComponent.W*26/375,
                        alignItems:"center",justifyContent:"center",}} 
                        options={this.state.areaCodes}/>

                    </View>
                    
                    <TextInput onChangeText={(phone)=>{this.oninput(phone,'phone');this.verifyphone(phone)}} keyboardType="numeric" placeholderTextColor="##BABABA" placeholder="请输入手机号" style={{flex:1,fontSize:BaseComponent.W*15/375,color:'#3a3a3a',marginLeft:BaseComponent.W*44/375,padding:BaseComponent.W*4/375}} 
                                underlineColorAndroid='transparent' maxLength={11}></TextInput>
            </View>
            <Text style={{fontSize:BaseComponent.W*10/375,color:'#FE7070',marginTop:BaseComponent.W*14/375,marginLeft:BaseComponent.W*223/375,display:this.state.phonecorrectly?'none':'flex'}}>*请输入正确的手机号码</Text>

           {/*获取验证码*/}
              <View style={{width:BaseComponent.W*280/375,height:BaseComponent.W*40/375,borderWidth:1,borderRadius:BaseComponent.W*20/375,
                           borderColor:'#DEDEDE',marginTop:this.state.showPwd?BaseComponent.W*12/375:BaseComponent.W*26/375,marginLeft:BaseComponent.W*48/375,flexDirection:'row',alignItems:'center'}}>
                    <Image style={{width:BaseComponent.W*19/375,height:BaseComponent.W*21/375,marginLeft:BaseComponent.W*14/375}} source={require('../../image/home/dunpai.png')}></Image>       
                    <TextInput onChangeText={(code)=>{this.oninput(code,'code');this.verifyCode(code)}} keyboardType="numeric" placeholderTextColor="##BABABA" placeholder="请输入验证码" underlineColorAndroid='transparent' style={{width:BaseComponent.W*100/375,marginLeft:BaseComponent.W*12/375,fontSize:BaseComponent.W*15/375,color:'#3a3a3a',padding:BaseComponent.W*4/375}} maxLength={6}></TextInput>
                    <View style={{width:1,height:BaseComponent.W*26/375,backgroundColor:'#CBCBCB',marginLeft:BaseComponent.W*30/375}}></View>

                    <Text onPress={()=>this.getVerifyCode()} style={{fontSize:BaseComponent.W*15/375,color:'#252525',marginLeft:BaseComponent.W*13/375,textAlign:'center',flex:1}}>{this.state.countTxt}</Text> 
              </View>
              <Text style={{fontSize:BaseComponent.W*10/375,color:'#FE7070',marginTop:BaseComponent.W*14/375,marginLeft:BaseComponent.W*223/375,display:!this.state.codecorrectly?'flex':'none'}}>*请输入正确的验证码</Text>

          {/*输入密码*/}        
          <View style={{width:BaseComponent.W*280/375,height:BaseComponent.W*40/375,borderWidth:1,borderRadius:BaseComponent.W*20/375,
                           borderColor:'#DEDEDE',marginTop:this.state.showYanzhengma?BaseComponent.W*12/375:BaseComponent.W*26/375,marginLeft:BaseComponent.W*48/375,flexDirection:'row',alignItems:'center'}}>
                <Image style={{width:BaseComponent.W*17/375,height:BaseComponent.W*20/375,marginLeft:BaseComponent.W*15/375}} source={require('../../image/home/suo.png')}></Image>            
                <TextInput secureTextEntry={true} onChangeText={(pwd)=>{this.oninput(pwd,'pwd')}} placeholder="请输入密码" placeholderTextColor="##BABABA" underlineColorAndroid='transparent' style={{fontSize:BaseComponent.W*15/375,width:BaseComponent.W*90/375,color:"#3a3a3a",marginLeft:BaseComponent.W*14/375,padding:BaseComponent.W*4/375}} maxLength={18}></TextInput>
                <Text style={{fontSize:BaseComponent.W*10/375,color:'#989898'}}>(字母开头，长度在6-18位)</Text>
          </View>   
          {/*确定*/}  
          <TouchableWithoutFeedback onPress={()=>this.register()}>                
          <View style={{width:BaseComponent.W*280/375,height:BaseComponent.W*40/375,borderRadius:BaseComponent.W*20/375,backgroundColor:this.state.phone==""||this.state.code==""||this.state.pwd==""||!this.state.isReadProtocol?'#CCCCCC':'#FED584',
                        marginTop:BaseComponent.W*45/375,marginLeft:BaseComponent.W*48/375,justifyContent:'center',alignItems:'center'}}>
                <Text style={{fontSize:BaseComponent.W*18/375,color:'#fff'}}>确    定</Text>
          </View>
          </TouchableWithoutFeedback>
          
          {/*阅读条款*/}
          <View style={{flexDirection:'row'}}>
              <TouchableWithoutFeedback onPress={()=>{
                this.setState({isReadProtocol:!this.state.isReadProtocol});
              }}>
                  <Image style={{width:BaseComponent.W*15/375,height:BaseComponent.W*15/375,marginLeft:BaseComponent.W*100/375,marginTop:ti.select({ios:BaseComponent.W*10.5/375,android:BaseComponent.W*13/375})}} source={this.state.isReadProtocol?require('../../image/home/agree.png'):require('../../image/home/disagree.png')}></Image>
              </TouchableWithoutFeedback>
              <View>
                  <Text style={{fontSize:BaseComponent.W*11/375,color:'#969696',marginLeft:BaseComponent.W*6/375,marginTop:BaseComponent.W*12/375}}>我同意<Text onPress={()=>{this.navigate("ServiceClause",{title:"服务条款"})}}>”服务条款”</Text>和<Text onPress={()=>{this.navigate("PrivacyPolicy",{title:"隐私政策"})}}>“隐私权相关政策”</Text></Text>
                  <View style={{flexDirection:'row',marginTop:2}}>
                      <View style={{width:BaseComponent.W*37/375,height:0.5,backgroundColor:'#BBBBBB',marginLeft:BaseComponent.W*50/375}}></View>
                      <View style={{width:BaseComponent.W*61/375,height:0.5,backgroundColor:'#BBBBBB',marginLeft:ti.select({ios:BaseComponent.W*36/375,android:BaseComponent.W*36/375})}}></View>
                  </View>
              </View>
          </View>

        </View>               
      </View>
    </TouchableWithoutFeedback>  
    )
  }

  componentDidMount(){
    
    // 加载数据
    this.getArea();
  }
}