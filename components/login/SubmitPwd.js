import React,{Component} from 'react';
import {Text,View,StyleSheet,Image,TouchableWithoutFeedback,StatusBar,TextInput,Modal,Keyboard,ActivityIndicator} from 'react-native';
import BaseComponent from '../BaseComponent';
// 导入工具函数库
import T from "../../lib/Tools";
// 导入公共样式
import St from "../../lib/Style";
// 导入图片资源库
import Imgs from "../../lib/Img";
// 转菊花
import Spinner from 'react-native-loading-spinner-overlay';

// Tools实例
let ti = T.getInstance();

let style = StyleSheet.create({
  wrapper:{
    backgroundColor:"white",
    flex:1,
    flexDirection:"column",
    alignItems:"stretch",
    justifyContent:"flex-start",
    position:"relative",
  },
  statusBar:{
    backgroundColor:"rgb(255,255,255)",
    height:ti.select({ios:16,android:22}),
    position:"relative",
    zIndex:1,
    opacity:1,
  },
  navBar:{
    backgroundColor:"rgb(255,255,255)",
    height:"7%",
    position:"relative",
    zIndex:1,
    opacity:1,
    borderBottomWidth:1,
    borderBottomColor:"rgb(241,241,241)"
  },
});

export default class SubmitPwd extends BaseComponent{

  constructor(props){
    super(props);
    this.state={
      isvisible:false,

      newpwd:"",
      reNewpwd:"",

      visible:false,
      curArea:{},
    }

  }

  // 处理输入
  oninput(value,type){
    
    var state = this.state;
    state[type] = value;
    
    this.setState(state);
  }

  commit(){
    let thiz = this;
    var pwd=/^[a-zA-Z]\w{5,17}$/;//验证密码格式是否正确;
    if(!this.state.newpwd){thiz.toast("请输入新密码");return;}
    if(!this.state.reNewpwd){thiz.toast("请再次输入新密码");return;}
    if(this.state.newpwd!=this.state.reNewpwd){thiz.toast("两次输入密码不一致");return;}
    if(!pwd.test(thiz.state.newpwd))
    {
      thiz.toast("密码格式不正确,以字母开头");
      return ;
    }
    thiz.setState({visible:true});
    thiz.request("user/resetPassword",{
      method:"POST",
      data:{
        phoneNoData: {
          countryId: this.state.curArea.id,
          phoneNo: thiz.params.phone,
        },
        password:thiz.state.newpwd,
        repeatPassword:thiz.state.reNewpwd,
        trustId:thiz.params.trustId,
      },
      success:function(ret){
        thiz.setState({visible:false});
        thiz.log("--------user/resetPassword-------",ret);
        if(ret&&ret.respCode==="00"){
          thiz.toast("修改密码成功");
          thiz.emit("reset_pwd_success");

          // 删除账号并返回登录页面返回登录页面
          thiz.rmAccount();

          setTimeout(function(){
            thiz.emit("get_page",{name:"ForgetPwd",action:"close"});
          },200);

        }
      },
      error:function(err){
        thiz.setState({visible:false});
        thiz.toast(err.msg);
      }
    });

  }

  render(){
    let thiz = this;
     var len =6;
        var arr = [];
        for(let i=0; i<len; i++){
            arr.push(i);
        }
    return (
        <TouchableWithoutFeedback onPress={()=>Keyboard.dismiss()}>
        <View style={style.wrapper}>

                {/*转菊花*/}
                <Modal
                  visible={this.state.visible}
                  onRequestClose={()=>{this.setState({visible:false})}}
                  transparent={true}
                  animationType={"fade"}>
                  <View style={{backgroundColor:"rgba(0,0,0,0.4)",width:"100%",width:"100%",flex:1,flexDirection:"column",alignItems:"center",justifyContent:"center",position:"relative"}}>
                    <View>
                          <ActivityIndicator size="large" color='white'/>
                    </View>
                  </View>
                </Modal>

                {/*状态栏占位*/}
                <View style={{backgroundColor:this.state.isvisible?'rgba(14,14,14,0.5)':"rgb(255,255,255)",
                        height:this.isIphoneX(30,BaseComponent.SH),
                        position:"relative",
                        zIndex:1,
                        opacity:1,}}></View>
                {/*顶部导航栏*/}
                <View style={style.navBar}>
                  <View style={{flex:1,flexDirection:"row",justifyContent:"flex-start"}}>

                    <TouchableWithoutFeedback onPress={()=>this.goBack()}>
                    <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>
                      <Image source={Imgs.back} style={{width:(BaseComponent.W*0.127*0.4),height:(BaseComponent.W*0.127*0.4),marginRight:BaseComponent.W*0.04}}></Image>
                    </View>
                    </TouchableWithoutFeedback>

                    <View style={{flexGrow:1,backgroundColor:"transparent",
                      marginTop:ti.select({ios:(BaseComponent.H*0.02)/2,android:(BaseComponent.H*0.016)/2}),borderRadius:ti.select({ios:(BaseComponent.H*0.05)/2,android:(BaseComponent.H*0.054)/2}),borderColor:"#0A0A0A",
                      borderWidth:0,}}>
                      <Text style={{color:"#0A0A0A",width:"100%",textAlign:"center",marginTop:ti.select({ios:"4.5%",android:"1.6%"}),fontSize:17}}>{this.params.title}</Text>
                    </View>
                    <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>
                      <Image source={require('../../image/home/msg.png')} style={{width:(BaseComponent.W*0.127*0.5),height:(BaseComponent.W*0.127*0.5),display:"none"}}></Image>
                    </View>
                  </View>
                </View>

                {/*弹出框*/}
                <Modal visible={this.state.isvisible}
                     transparent={true}
                     onRequestClose={() => {this.setState({isvisible:false})}}>
                    <View style={{flex:1,backgroundColor:'rgba(14,14,14,0.5)',alignItems:'center'}}>
                        <View style={{width:BaseComponent.W*281/375,height:BaseComponent.W*156/375,backgroundColor:'#fff',marginTop:BaseComponent.W*126/375-StatusBar.currentHeight}}>
                            <View style={{width:'100%',height:BaseComponent.W*102/375,justifyContent:'center',alignItems:'center'}}>
                                <Text style={{fontSize:BaseComponent.W*15/375,color:'#000000'}}>两次新密码不一样，请检查后提交。</Text>
                            </View>
                            <View style={{width:'100%',height:0.5,backgroundColor:'#CCCCCC'}}></View>
                            <TouchableWithoutFeedback onPress={()=>this.setState({isvisible:false})}>
                            <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                                <Text style={{color:'#FAAF1A',fontSize:BaseComponent.W*16/375}}>确  定</Text>
                            </View>
                            </TouchableWithoutFeedback>
                        </View>
                    </View>
                </Modal>

                <View style={{flex:1,backgroundColor:'#FAFAFA'}}>
                  <View style={{flexDirection:'row'}}>
                  <View style={{width:BaseComponent.W*18/375,height:BaseComponent.W*18/375,backgroundColor:"#FED584",
                                marginTop:BaseComponent.W*31/375,marginLeft:BaseComponent.W*80/375,borderRadius:BaseComponent.W*10/375,
                                justifyContent:'center',alignItems:'center'}}>
                        <Text style={{fontWeight:'bold',color:'black'}}>1</Text>
                  </View>
                {/*arr.map实现虚线功能*/}
                  <View style={{marginTop:BaseComponent.W*40/375,flexDirection:'row'}}>
                      {
                        arr.map((item,index)=>{
                              return (
                                <View style={{width:BaseComponent.W*10/375,height:1,marginLeft:index==0?0:5,backgroundColor:'#CCCCCC'}}></View>
                              )
                        })
                      }          
                  </View>
                  <Image style={{width:BaseComponent.W*25/375,height:BaseComponent.W*25/375,marginTop:BaseComponent.W*28.5/375}} source={require('../../image/home/feiji2.png')}></Image>
                  {/*arr.map实现虚线功能*/}
                  <View style={{marginTop:BaseComponent.W*40/375,flexDirection:'row'}}>
                      {
                        arr.map((item,index)=>{
                              return (
                                <View style={{width:BaseComponent.W*10/375,height:1,marginLeft:index==0?0:5,backgroundColor:'#CCCCCC'}}></View>
                              )
                        })
                      }          
                  </View>
                  <View style={{width:BaseComponent.W*18/375,height:BaseComponent.W*18/375,backgroundColor:"#FED584",
                                marginTop:BaseComponent.W*31/375,borderRadius:BaseComponent.W*10/375,
                                justifyContent:'center',alignItems:'center'}}>
                        <Text style={{fontWeight:'bold',color:'black'}}>2</Text>
                  </View>
                  </View>

                {/*输入新密码*/}
                <View style={{width:BaseComponent.W*280/375,height:BaseComponent.W*40/375,borderWidth:1,borderRadius:BaseComponent.W*20/375,
                             borderColor:'#DEDEDE',marginTop:BaseComponent.W*40/375,marginLeft:BaseComponent.W*48/375,flexDirection:'row',alignItems:'center'}}>
                     <TextInput secureTextEntry={true} onChangeText={(newpwd)=>thiz.oninput(newpwd,'newpwd')} placeholderTextColor="##BABABA" placeholder="请输入新密码" underlineColorAndroid='transparent' style={{flex:1,fontSize:BaseComponent.W*15/375,color:'#3a3a3a',padding:BaseComponent.W*4/375,marginLeft:BaseComponent.W*20/375}} maxLength={18}></TextInput>
                </View>

                {/*确认新密码*/}
                <View style={{width:BaseComponent.W*280/375,height:BaseComponent.W*40/375,borderWidth:1,borderRadius:BaseComponent.W*20/375,
                             borderColor:'#DEDEDE',marginTop:BaseComponent.W*40/375,marginLeft:BaseComponent.W*48/375,flexDirection:'row',alignItems:'center'}}>
                      <TextInput secureTextEntry={true} onChangeText={(reNewpwd)=>thiz.oninput(reNewpwd,'reNewpwd')}  placeholder="请再次输入新密码" underlineColorAndroid='transparent' placeholderTextColor="##BABABA" style={{flex:1,fontSize:BaseComponent.W*15/375,color:'#3a3a3a',padding:BaseComponent.W*4/375,marginLeft:BaseComponent.W*20/375}} maxLength={18}></TextInput>
                </View>    

                {/*提交*/}
                <TouchableWithoutFeedback onPress={()=>this.commit()}>  
                <View style={{width:BaseComponent.W*280/375,height:BaseComponent.W*40/375,borderRadius:BaseComponent.W*20/375,backgroundColor:thiz.state.newpwd==""||thiz.state.reNewpwd==""||(thiz.state.newpwd!=thiz.state.reNewpwd)?"#CCCCCC":'#FED584',
                             marginTop:BaseComponent.W*77/375,marginLeft:BaseComponent.W*48/375,alignItems:'center',justifyContent:'center'}}>
                        <Text style={{fontSize:BaseComponent.W*18/375,color:"#FFFFFF"}}>提    交</Text>
                </View>  
                </TouchableWithoutFeedback>                    
              </View>
        </View>
      </TouchableWithoutFeedback>
    )
  }

  componentDidMount(){
    let thiz = this;
    this.setState({
      curArea:this.params.curArea,
    });

    // 监听返回page信息的消息
    this.listen("return_page",function(ret,err){
      if(ret){
        if(ret.action&&ret.action=="close"){
          thiz.backTo(ret.data.key);
        }
      }
    });

  }

}