/**
 * @name Member.js
 * @auhor 李磊
 * @date 2019.3.1
 * @desc 会员首页
 */
import React,{Component} from 'react';
import {Text,View,StyleSheet,Image,TouchableWithoutFeedback,ScrollView,
ImageBackground,TextInput,StatusBar,Modal,FlatList,Picker,ActivityIndicator} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
// 导入工具函数库
import T from "../../lib/Tools";
// 导入公共样式
import St from "../../lib/Style";
// 导入图片资源库
import Imgs from "../../lib/Img";
import BaseComponent from '../BaseComponent';



// Tools实例
let ti = T.getInstance();

let style = StyleSheet.create({
  wrapper:{
    backgroundColor:"#EEEEEE",
    flex:1,
    flexDirection:"column",
    alignItems:"stretch",
    justifyContent:"flex-start",
    position:"relative",
  },
  statusBar:{
    backgroundColor:"rgb(255,255,255)",
    height:ti.select({ios:16,android:22}),
    position:"relative",
    zIndex:1,
    opacity:1,
  },
  navBar:{
    backgroundColor:"transparent",
    height:"7%",
    position:"relative",
    zIndex:1,
    opacity:1,
    // borderBottomWidth:1,
    // borderBottomColor:"rgb(241,241,241)"
  },
});
export default class Member extends BaseComponent{
constructor(props){
    super(props);
    this.state = {
      data:this.params.data,
      visible:false,
      hasNext:false,
      imgs:{},// 所有图片
      hotGoods:{},// 商品列表
      rules:[
      // {
      //   problem:"你今年",
      //   answer:"你妹的，你姐夫的非奸即盗",
      // },{
      //   problem:"你今年",
      //   answer:"你妹的，你姐夫的非奸即盗",
      // },{
      //   problem:"你今年",
      //   answer:"你妹的，你姐夫的非奸即盗",
      // },{
      //   problem:"你今年",
      //   answer:"你妹的，你姐夫的非奸即盗",
      // }
      ],// 规则
      isRulesVisible:false,
    };
    this.hotCurPage = 0;
    this.isHotLoading = false;
}

// 加载数据
  loadData(callback){
    let thiz = this;
    thiz.isHotLoading = true;
    // 加载商品列表
    thiz.setState({
          visible:true,
    }); 
    thiz.request("spu/getOnSalePage",{
        method:"POST",
        data:{
          goodsType:"VIP_UPGRADE",
          page:{
            pageSize:BaseComponent.PAGE_SIZE,
            pageNumber:thiz.hotCurPage+1
          }
        },
        success:function(ret){
              thiz.setState({
                    visible:false,
              });
              if(ret.respCode=="00"){

                    if(ret.data.spuPage.records&&ret.data.spuPage.records.length>0){
                      thiz.setState({
                            hotGoods:ret.data.spuPage,
                            hasNext:ret.data.spuPage.hasNext
                      });

                      thiz.hotCurPage = ret.data.spuPage.pageNumber;
                    }

              }

              thiz.isHotLoading = false;

              if(callback){
                callback();
              }

        },
        error:function(err){
              thiz.toast(err.msg);
              thiz.setState({
                    visible:false,
              });

              thiz.isHotLoading = false;

              if(callback){
                callback();
              }
        }
    });

    thiz.request("question/getPage",{
        method:"POST",
        data:{
          questionType:"VIP_UPGRADE",
        },
        success:function(ret){
          thiz.log("--------question/getPage_ret--------",ret);
          if(ret.respCode=="00"){

                if(ret.data.records&&ret.data.records.length>0){
                  thiz.setState({
                    rules:ret.data.records,
                  });
                }

          }
        },
        error:function(err){
          thiz.toast(err.msg);
        }
    });

  }

  componentDidMount(){
    let thiz = this;
    thiz.loadData();
  }

  // 滚动回调
  onScroll(e){
    let thiz = this;
    console.log("------------------pageNumber-------------------------",thiz.hotCurPage+1);
    thiz.log("--------onScroll--------",e.nativeEvent);
    var offsetY = e.nativeEvent.contentOffset.y; //滑动距离
    var contentSizeHeight = e.nativeEvent.contentSize.height; //scrollView contentSize高度
    var oriageScrollHeight = e.nativeEvent.layoutMeasurement.height; //scrollView高度
    if (Math.abs((offsetY + oriageScrollHeight) - contentSizeHeight)<5){
        thiz.log("--------onScroll--------","滚动到了底部");
        thiz.log("---------------thiz.state.hotGoods----------------------",thiz.state.hasNext);
        // 判断是否还有更多商品
        if(thiz.state.hasNext){
          thiz.log("--------thiz.isHotLoading--------",thiz.isHotLoading);
          // 判断是否正在加载
          if(!thiz.isHotLoading){
            // 加载热销商品
            thiz.isHotLoading = true;
            thiz.setState({visible:true});
            thiz.request("spu/getOnSalePage",{
                method:"POST",
                data:{
                  goodsType:"VIP_UPGRADE",
                  page:{
                    pageSize:BaseComponent.PAGE_SIZE,
                    pageNumber:thiz.hotCurPage+1
                  }
                },
                success:function(ret){
                      thiz.isHotLoading = false;
                      thiz.setState({
                            visible:false,
                      });
                      if(ret.respCode=="00"){
                            let hotGoods = thiz.state.hotGoods;
                            let items = hotGoods.records;
                            if(items){
                              if(ret.data.spuPage.records.length>0){
                                for(let i=0;i<ret.data.spuPage.records.length;i++){
                                  items.push(ret.data.spuPage.records[i]);
                                }
                                hotGoods.records = items;
                                thiz.setState({
                                      hotGoods:hotGoods,
                                      hasNext:ret.data.spuPage.hasNext
                                });
                                thiz.hotCurPage = ret.data.spuPage.pageNumber;
                              }
                            }

                      }
                },
                error:function(err){
                      thiz.isHotLoading = false;
                      thiz.toast(err.msg);
                      thiz.setState({
                            visible:false
                      });
                }
            });
          }
        }else{
          // thiz.toast("没有更多商品了");
        }
    }
  }

  render(){
    let thiz = this;
    return (
      <View style={style.wrapper}>

          {/*状态栏占位*/}
          <View style={{ backgroundColor:"transparent",
            height:this.isIphoneX(30,BaseComponent.SH),
            position:"relative",
            zIndex:1,
            opacity:1,}}>
          </View>

          {/*转菊花*/}
        <Modal
          visible={this.state.visible}
          onRequestClose={()=>{this.setState({visible:false})}}
          transparent={true}
          animationType={"fade"}>
          <View style={{backgroundColor:"rgba(0,0,0,0.4)",width:"100%",width:"100%",flex:1,flexDirection:"column",alignItems:"center",justifyContent:"center",position:"relative"}}>
            <View>
                  <ActivityIndicator size="large" color='white'/>
            </View>
          </View>
        </Modal>

        {/*会员升级规则*/}
        <Modal
          visible={this.state.isRulesVisible}
          onRequestClose={()=>{this.setState({isRulesVisible:false})}}
          transparent={true}
          animationType={"fade"}>
          <View style={{backgroundColor:"rgba(0,0,0,0.4)",width:"100%",width:"100%",flex:1,flexDirection:"column",alignItems:"center",justifyContent:"center",position:"relative"}}>
            
            <View style={{backgroundColor:"white",width:"80%",paddingTop:20,paddingBottom:30,paddingLeft:30,paddingRight:30,borderRadius:10,}}>
              <Text style={{color:"#875940",width:"100%",textAlign:"center",marginBottom:20,
              fontSize:18,}}>升级帮助</Text>
              <View>
                {
                  thiz.state.rules&&thiz.state.rules.length>0?thiz.state.rules.map(function(item,index){

                    return (
                      <View style={{marginTop:10,}}>
                        <Text style={{fontSize:14,color:"black"}}>Q：{item.problem}</Text>
                        <Text style={{fontSize:14,color:"black"}}>A：{item.answer}</Text>
                      </View>
                    )

                  }):null
                }
              </View>
            </View>

            <TouchableWithoutFeedback onPress={()=>{
              thiz.setState({
                isRulesVisible:false,
              });
            }}>
            <View style={{width:"100%",justifyContent:"center",alignItems:"center",marginTop:30,height:30,backgroundColor:"transparent"}}>
              <Image source={require("../../image/delete.png")} style={{width:40,height:40,}} resizeMode="contain"/>
            </View>
            </TouchableWithoutFeedback>
          
          </View>
        </Modal>


          {/*顶部导航栏*/}
          <View style={style.navBar}>
            <View style={{flex:1,flexDirection:"row",justifyContent:"flex-start"}}>

                <TouchableWithoutFeedback onPress={()=>this.goBack()}>
                  <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>
                    <Image source={Imgs.back} style={{width:(BaseComponent.W*0.127*0.4),height:(BaseComponent.W*0.127*0.4),marginRight:BaseComponent.W*0.04}}></Image>
                  </View>
                </TouchableWithoutFeedback>

                <View style={{flexGrow:1,backgroundColor:"transparent",
                  marginTop:ti.select({ios:(BaseComponent.H*0.02)/2,android:(BaseComponent.H*0.016)/2}),borderRadius:ti.select({ios:(BaseComponent.H*0.05)/2,android:(BaseComponent.H*0.054)/2}),borderColor:"#0A0A0A",
                  borderWidth:0,}}>
                  <Text style={{color:"#0A0A0A",width:"100%",textAlign:"center",marginTop:ti.select({ios:"4.5%",android:"1.6%"}),fontSize:BaseComponent.W*17/375,marginLeft:'4.3%'}}>{this.params.title}</Text>
                </View>

                <TouchableWithoutFeedback onPress={()=>{
                  thiz.setState({
                    isRulesVisible:true,
                  });
                }}>
                <View style={{flexDirection:"row",width:"20%",height:"100%",backgroundColor:"transparent",alignItems:"center",marginRight:BaseComponent.W*10/375,}}>
                    <Image style={{width:BaseComponent.W*0.05,height:BaseComponent.W*0.05}} source={require("../../image/home/msg.png")}/>
                    <Text style={{fontSize:14,color:"white",marginLeft:5,}}>升级帮助</Text>
                </View>
                </TouchableWithoutFeedback>

            </View>
          </View>



          {/*商品列表*/}
          <ScrollView style={{position:"absolute",top:0}}
            showsVerticalScrollIndicator={false}
            alwaysBounceVertical={false}
          
          onMomentumScrollEnd={(e)=>{thiz.onScroll(e)}}>

            {/*顶部banner*/}
            <ImageBackground style={{width:'100%',height:BaseComponent.H*25/75,}} resizeMode='stretch' source={require('../../image/mine/member.jpg')}>
            
            </ImageBackground>

            <View style={{paddingTop:20,paddingLeft:BaseComponent.W*15/375,paddingRight:BaseComponent.W*15/375,backgroundColor:"transparent"}}>

              {

                thiz.state.hotGoods.records&&thiz.state.hotGoods.records.length>0?thiz.state.hotGoods.records.map(function(item,index){
                  thiz.log("--------turn--------","turn");
                  return (
                    <TouchableWithoutFeedback onPress={()=>{
                      thiz.navigate("GoodsXiangQing",{title:"",id:item.id,from:"Member"});
                    }}>
                    <View style={{flexDirection:"row",paddingLeft:BaseComponent.W*20/375,paddingRight:BaseComponent.W*20/375,
                      paddingTop:BaseComponent.W*20/375,paddingBottom:BaseComponent.W*20/375,backgroundColor:"white",borderRadius:10,
                      marginBottom:12,}}>
                        <Image style={{width:90,height:90,}} source={{uri:item.attachment&&item.attachment.resourceFullAddress?item.attachment.resourceFullAddress:""}}/>
                        <View style={{width:BaseComponent.W*305/375-100,marginLeft:10,position:"relative"}}>

                          <View style={{width:"100%",backgroundColor:"transparent"}}>
                            <Text numberOfLines={2} style={{width:"100%",color:"#000000"}}>{item.name}</Text>
                          </View>

                          <View style={{width:"100%",backgroundColor:"transparent",position:"absolute",bottom:0,}}>
                            <View style={{flexDirection:"row",position:"relative"}}>
                              <Text style={{color:"#000"}}>单  价：<Text style={{color:"rgb(251,17,36)"}}>￥{item.buyUnitPrice}</Text></Text>
                              {/*<Text style={{position:"absolute",right:0,bottom:0,color:"#000000"}}>升级价：<Text style={{fontSize:20,color:"rgb(251,17,36)"}}>¥{item.buyUnitPrice}</Text></Text>*/}
                            </View>
                          </View>

                        </View>
                    </View>
                    </TouchableWithoutFeedback>
                  );
                }):null

              }
              {
                thiz.state.hasNext?null:thiz.nomore({bgcolor:'#F0F0F0'})
              }
            </View>

          </ScrollView>

    </View>
    )
  }
}              