/**
 * @name AboutMe.js
 * @auhor 程浩
 * @date 2018.10.8
 * @desc 关于小洋匠
 */
import React,{Component} from 'react';
import {Text,View,StyleSheet,Image,TouchableWithoutFeedback,ScrollView,
ImageBackground,TextInput,StatusBar,Modal,FlatList,Picker} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
// 导入工具函数库
import T from "../../lib/Tools";
// 导入公共样式
import St from "../../lib/Style";
// 导入图片资源库
import Imgs from "../../lib/Img";
import BaseComponent from '../BaseComponent';



// Tools实例
let ti = T.getInstance();

let style = StyleSheet.create({
  wrapper:{
    backgroundColor:"white",
    flex:1,
    flexDirection:"column",
    alignItems:"stretch",
    justifyContent:"flex-start",
    position:"relative",
  },
  statusBar:{
    backgroundColor:"rgb(255,255,255)",
    height:ti.select({ios:16,android:22}),
    position:"relative",
    zIndex:1,
    opacity:1,
  },
  navBar:{
    backgroundColor:"rgb(255,255,255)",
    height:"7%",
    position:"relative",
    zIndex:1,
    opacity:1,
    borderBottomWidth:1,
    borderBottomColor:"rgb(241,241,241)"
  },
});
export default class AboutMe extends BaseComponent{
constructor(props){
    super(props);
    this.state={
     appVersion:BaseComponent.V,
    }
}
  render(){
    let thiz = this;
    return (
      <View style={style.wrapper}>   
                {/*状态栏占位*/}
                <View style={{ backgroundColor:"rgba(255,255,255)",
                      height:this.isIphoneX(30,BaseComponent.SH),
                      position:"relative",
                      zIndex:1,
                      opacity:1,}}></View>
                {/*顶部导航栏*/}
              <View style={style.navBar}>
                <View style={{flex:1,flexDirection:"row",justifyContent:"flex-start"}}>

                    <TouchableWithoutFeedback onPress={()=>this.goBack()}>
                    <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>
                      <Image source={Imgs.back} style={{width:(BaseComponent.W*0.127*0.4),height:(BaseComponent.W*0.127*0.4),marginRight:BaseComponent.W*0.04}}></Image>
                    </View>
                    </TouchableWithoutFeedback>

                    <View style={{flexGrow:1,backgroundColor:"transparent",
                      marginTop:ti.select({ios:(BaseComponent.H*0.02)/2,android:(BaseComponent.H*0.016)/2}),borderRadius:ti.select({ios:(BaseComponent.H*0.05)/2,android:(BaseComponent.H*0.054)/2}),borderColor:"#0A0A0A",
                      borderWidth:0,}}>
                      <Text style={{color:"#0A0A0A",width:"100%",textAlign:"center",marginTop:ti.select({ios:"4.5%",android:"1.6%"}),fontSize:BaseComponent.W*17/375,marginLeft:'4.3%'}}>{this.params.title}</Text>
                    </View>
                    <View style={{width:"14%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center",marginRight:BaseComponent.W*5/375}}>
                        
                    </View>
                </View>
              </View>

            {/*小洋匠名字加版本*/}
            <View style={{width:"100%",height:BaseComponent.W*144/375,backgroundColor:'#F0F0F0',justifyContent:'center',alignItems:'center'}}>
                  <Image style={{width:BaseComponent.W*113/375,height:BaseComponent.W*45/375}} source={require('../../image/home/xiaoyangjiang1.png')}></Image>
                  <Text style={{fontSize:BaseComponent.W*16/375,color:'#999999',marginTop:BaseComponent.W*14/375}}>版本v{thiz.state.appVersion}</Text>
            </View>
            {/*小洋匠简介*/}
            <TouchableWithoutFeedback onPress={()=>{this.navigate('Introduction',{title:'小洋匠简介'})}}>
            <View style={{width:'100%',height:BaseComponent.W*49/375,flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
                  <Text style={{fontSize:BaseComponent.W*15/375,color:'#333333',marginLeft:BaseComponent.W*18/375}}>小洋匠简介</Text>
                  <Image style={{width:BaseComponent.W*25/375,height:BaseComponent.W*25/375,marginRight:BaseComponent.W*8/375}} source={require('../../image/home/youjiantou.png')}></Image>
            </View>
            </TouchableWithoutFeedback>
            <View style={{width:'100%',height:0.5,backgroundColor:'#C8C7CC'}}></View> 

            {/*服务条款*/}
            <TouchableWithoutFeedback onPress={()=>this.navigate('ServiceClause',{title:"服务条款"})}>
            <View style={{width:'100%',height:BaseComponent.W*49/375,flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
                  <Text style={{fontSize:BaseComponent.W*15/375,color:'#333333',marginLeft:BaseComponent.W*18/375}}>服务条款</Text>
                  <Image style={{width:BaseComponent.W*25/375,height:BaseComponent.W*25/375,marginRight:BaseComponent.W*8/375}} source={require('../../image/home/youjiantou.png')}></Image>
            </View>
            </TouchableWithoutFeedback>
            <View style={{width:'100%',height:0.5,backgroundColor:'#C8C7CC'}}></View>

            {/*隐私权相关政策*/}
            <TouchableWithoutFeedback onPress={()=>this.navigate('PrivacyPolicy',{title:"隐私政策"})}>
            <View style={{width:'100%',height:BaseComponent.W*49/375,flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
                  <Text style={{fontSize:BaseComponent.W*15/375,color:'#333333',marginLeft:BaseComponent.W*18/375}}>隐私权相关政策</Text>
                  <Image style={{width:BaseComponent.W*25/375,height:BaseComponent.W*25/375,marginRight:BaseComponent.W*8/375}} source={require('../../image/home/youjiantou.png')}></Image>
            </View>
            </TouchableWithoutFeedback>

            <View style={{flex:1,backgroundColor:'#F0F0F0',alignItems:'center'}}>
                <Text style={{fontSize:BaseComponent.W*12/375,color:'#989898',marginTop:BaseComponent.W*48/375}}>俊介科技版权所有</Text>
            </View>


    </View>
    )
  }
}              