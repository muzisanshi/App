/**
 * @name AccountManagement
 * @auhor 程浩
 * @date 2018.10.8
 * @desc 账号管理
 */
import React,{Component} from 'react';
import {Text,View,StyleSheet,Image,TouchableWithoutFeedback,ScrollView,
ImageBackground,TextInput,StatusBar,Modal,FlatList,Picker} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
// 导入工具函数库
import T from "../../lib/Tools";
// 导入公共样式
import St from "../../lib/Style";
// 导入图片资源库
import Imgs from "../../lib/Img";
import BaseComponent from '../BaseComponent';



// Tools实例
let ti = T.getInstance();

let style = StyleSheet.create({
  wrapper:{
    backgroundColor:"white",
    flex:1,
    flexDirection:"column",
    alignItems:"stretch",
    justifyContent:"flex-start",
    position:"relative",
  },
  statusBar:{
    backgroundColor:"rgb(255,255,255)",
    height:ti.select({ios:16,android:22}),
    position:"relative",
    zIndex:1,
    opacity:1,
  },
  navBar:{
    backgroundColor:"rgb(255,255,255)",
    height:"7%",
    position:"relative",
    zIndex:1,
    opacity:1,
    borderBottomWidth:1,
    borderBottomColor:"rgb(241,241,241)"
  },
});
export default class AccountManagement extends BaseComponent{
constructor(props){
    super(props);
    
}
  render(){
    return (
      <View style={style.wrapper}>   
                {/*状态栏占位*/}
                <View style={{ backgroundColor:"rgba(255,255,255)",
                      height:ti.select({ios:16,android:StatusBar.currentHeight}),
                      position:"relative",
                      zIndex:1,
                      opacity:1,}}></View>
                {/*顶部导航栏*/}
              <View style={style.navBar}>
                <View style={{flex:1,flexDirection:"row",justifyContent:"flex-start"}}>

                    <TouchableWithoutFeedback onPress={()=>this.goBack()}>
                    <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>
                      <Image source={Imgs.back} style={{width:(BaseComponent.W*0.127*0.4),height:(BaseComponent.W*0.127*0.4),marginTop:ti.select({ios:BaseComponent.W*0.02,android:0}),marginRight:BaseComponent.W*0.04}}></Image>
                    </View>
                    </TouchableWithoutFeedback>

                    <View style={{flexGrow:1,backgroundColor:"transparent",
                      marginTop:ti.select({ios:(BaseComponent.H*0.02)/2,android:(BaseComponent.H*0.016)/2}),borderRadius:ti.select({ios:(BaseComponent.H*0.05)/2,android:(BaseComponent.H*0.054)/2}),borderColor:"#0A0A0A",
                      borderWidth:0,}}>
                      <Text style={{color:"#0A0A0A",width:"100%",textAlign:"center",marginTop:ti.select({ios:"4%",android:"1.6%"}),fontSize:BaseComponent.W*17/375,marginLeft:'4.3%'}}>{this.params.title}</Text>
                    </View>
                    <View style={{width:"14%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center",marginRight:BaseComponent.W*5/375}}>
                        
                    </View>
                </View>
              </View>

              {/*主账号*/}
              <View style={{width:'100%',height:BaseComponent.W*36/375,backgroundColor:'#F0F0F0',justifyContent:'center'}}>
                  <Text style={{fontSize:BaseComponent.W*15/375,color:'#333333',marginLeft:BaseComponent.W*10/375}}>我的主账号</Text>
              </View>
              {/*手机账号*/}
              <View style={{width:'100%',height:BaseComponent.W*50/375,backgroundColor:'white',flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
                  <View style={{height:'100%',flexDirection:'row',alignItems:'center'}}>
                  <Image style={{width:BaseComponent.W*33/375,height:BaseComponent.W*33/375,borderRadius:BaseComponent.W*16.5/375,marginLeft:BaseComponent.W*14/375}} source={require('../../image/home/accountphone.png')}></Image>
                  <View style={{width:BaseComponent.W*120/375,height:'100%',justifyContent:'center',marginLeft:BaseComponent.W*13/375}}>
                      <Text style={{fontSize:BaseComponent.W*13/375,color:'#333333'}}>手机账号</Text>
                      <Text style={{fontSize:BaseComponent.W*11/375,color:'#999999'}}>187****8081</Text>
                  </View>
                  </View>

                  <View style={{height:'100%',flexDirection:'row',alignItems:'center',marginRight:BaseComponent.W*10/375}}>
                  <Text style={{fontSize:BaseComponent.W*13/375,color:'#999999'}}>更换手机账号</Text>
                  <Image style={{width:BaseComponent.W*20/375,height:BaseComponent.W*20/375}} source={require('../../image/home/youjiantou.png')}></Image>
                  </View>
              </View>

              {/*我的主账号*/}
              <View style={{width:'100%',height:BaseComponent.W*41/375,backgroundColor:'#F0F0F0',justifyContent:'center'}}>
                  <Text style={{fontSize:BaseComponent.W*15/375,color:'#333333',marginLeft:BaseComponent.W*10/375}}>我的主账号</Text>
              </View>

              {/*微信账号*/}
              <View style={{width:'100%',height:BaseComponent.W*50/375,backgroundColor:'white',flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
                  <View style={{height:'100%',flexDirection:'row',alignItems:'center'}}>
                  <Image style={{width:BaseComponent.W*33/375,height:BaseComponent.W*33/375,borderRadius:BaseComponent.W*16.5/375,marginLeft:BaseComponent.W*14/375}} source={require('../../image/home/accountwx.png')}></Image>
                  <View style={{width:BaseComponent.W*120/375,height:'100%',justifyContent:'center',marginLeft:BaseComponent.W*13/375}}>
                      <Text style={{fontSize:BaseComponent.W*13/375,color:'#333333'}}>微信</Text>
                  </View>
                  </View>

                  <View style={{height:'100%',flexDirection:'row',alignItems:'center',marginRight:BaseComponent.W*10/375}}>
                  <Text style={{fontSize:BaseComponent.W*13/375,color:'#999999'}}>未绑定</Text>
                  <Image style={{width:BaseComponent.W*20/375,height:BaseComponent.W*20/375}} source={require('../../image/home/youjiantou.png')}></Image>
                  </View>
              </View>

              <View style={{flex:1,backgroundColor:'#F0F0F0'}}></View>
      </View>
    )
  }
}              