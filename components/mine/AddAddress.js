/**
 * @name AddAddress.js
 * @auhor 程浩
 * @date 2018.8.21
 * @desc 添加地址
 */
import React,{Component} from 'react';
import {Text,View,StyleSheet,Image,TouchableWithoutFeedback,ScrollView,Button,ActivityIndicator,
ImageBackground,TextInput,StatusBar,Modal,FlatList,Picker} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import ImagePicker from 'react-native-image-crop-picker'; 
// 导入工具函数库
import T from "../../lib/Tools";
// 导入公共样式
import St from "../../lib/Style";
// 导入图片资源库
import Imgs from "../../lib/Img";
import BaseComponent from '../BaseComponent';
import DeliverGoods from '../cart/DeliverGoods';
import SelectAddressOrDate  from './SelectAddressOrDate';

import PickerNew from 'react-native-picker';


// Tools实例
let ti = T.getInstance();

let style = StyleSheet.create({
  wrapper:{
    backgroundColor:"white",
    flex:1,
    flexDirection:"column",
    alignItems:"stretch",
    justifyContent:"flex-start",
    position:"relative",
  },
  statusBar:{
    backgroundColor:"rgb(255,255,255)",
    height:ti.select({ios:16,android:22}),
    position:"relative",
    zIndex:1,
    opacity:1,
  },
  navBar:{
    backgroundColor:"rgb(255,255,255)",
    height:"7%",
    position:"relative",
    zIndex:1,
    opacity:1,
    borderBottomWidth:1,
    borderBottomColor:"rgb(241,241,241)"
  },
});

export default class AddAddress extends BaseComponent{
  constructor(props){
    super(props);
    this.viewabilityConfig = {
      waitForInteraction: true,
      viewAreaCoveragePercentThreshold: 250
    };
    this.state={
      inputAddress:"",// 输入待识别的地址
     address:'请选择省市区',
     shouhuorenName:"",//收货人名字
     phoneNo:"",//收货人手机
     shouhuorenAddress:"",//收货人详细地址
     idCardImageFrontAttachment:" ",//身份证正面照片
     idCardImageBackAttachment:" ",//身份证反面照片
     idCardImageFrontAttachmentID:"",//身份证正面的ID
     idCardImageBackAttachmentID:"",//身份证反面的ID
     receiverIdCardNo:"",//身份证号码
     isvisible:false,//选择省市区，false表示不可见
     province:[],//省
     city:[],//市
     area:[],//区
     provinceName:"",//省名字
     cityName:"",//市名字
     areaName:"",//区名字
     visible:false,//转菊花

     isAddrOpen:false,// 默认地址选择器关闭

     // 省、市、区选中数据及状态
     provinceState:{
      show:true,
      pid:null,
      title:"请选择",
      selected:true,
     },
     cityState:{
      show:false,
      cid:null,
      title:"请选择",
      selected:false,
     },
     areaState:{
      show:false,
      aid:null,
      title:"请选择",
      selected:false,
     },
     hasArea:true,

     // 省数据
     provinceData:[],
     cityData:[],
     areaData:[],

     finalAddr:{
      default:"请选择省市区",
      province:{
        name:"",
      },
      city:{
        name:""
      },
      area:{
        name:""
      }
     },

     finalAreaId:null,

     // 是否显示收货人信息识别框
     showRecInput:true,

     ismsgVisible:false,// 是否显示覆盖地址提示
     msg:"",//覆盖地址消息
  } 

    this.pickerData = [];// 地址选择器显示的数据
    this.pickerProvinceData = [];// 地址省原始数据
    this.pickerCityData = [];// 地址市原始数据
    this.pickerAreaData = [];// 地址区县原始数据

    // 记录选中的省市区
    this.sd = {
      province:null,
      city:null,
      area:null
    };

    // 选择器初始化计数
    this.initCount = 0;

};
    _flatList;//省的flatlist的ID
    _flatListcity;//市的flatlist的ID
    _flatListarea;//区的flatlist的ID
    open(){
      this.refs.SelectAddressOrDate.showAddress(this.state.address)
    }
    callBackAddressValue(value){
        this.setState({
            address:value
        })
    }

    // 获取省数据
    // var provinceDataStr=[
    //       {id:"",name:""},
    //       {id:"",name:""}
    //   ];
    //   var provinceData=provinceDataStr.concat(ret.data);
    //   provinceData=provinceData.concat(provinceDataStr);
    //   thiz.log("--------------------------cityData-----------------------------",provinceData);
    //   thiz.setState({
    //     province:provinceData,
    //   });
    //   thiz.getCity();
    //   thiz.getArea();

    getProvinceData(callback){
      var thiz=this;
      thiz.request("area/getAllOneLevel",{
        method:"POST",
        success:function(ret){
          thiz.log("--------getProvinceData_ret--------",ret);
          callback(ret);
        },
        error:function(err){
          callback(null);
        }
      });
    }

    // 获取市数据
    getCityData(pid,callback){
      let thiz = this;
      thiz.request("area/getByParentId",{
          method:"POST",
          data:{parentAreaId:pid},
          success:function(ret){
            thiz.log("--------getCityData_ret--------",ret);
            callback(ret);
          },
          error:function(err){
            callback(null);
          }
      });
    }

    // 获取区县数据
    getAreaData(cid,callback){
      let thiz = this;
      thiz.request("area/getByParentId",{
          method:"POST",
          data:{parentAreaId:cid},
          success:function(ret){
            thiz.log("--------getAreaData_ret--------",ret);
            callback(ret);
          },
          error:function(err){
            callback(null);
          }
      })
    }

    // 加载地址数据
    loadAddrData(){
      let thiz = this;
      // 默认只获取省的数据
      thiz.getProvinceData(function(ret){
        if(ret&&ret.data&&ret.data.length>0){
          // 处理数据
          for(let i=0;i<ret.data.length;i++){
            ret.data[i].selected = false;
          }
          thiz.setState({
            provinceData:ret.data,
          });
        }
      });
    }

    recognize(){
      // 判断输入情况
      let thiz = this;
      if(!thiz.state.inputAddress){
        thiz.toast("请粘贴收货人信息");
        return;
      }
      // 识别
      thiz.setState({
        visible:true,
      });
      thiz.request("receiverAddress/recognize",{
          method:"POST",
          data:{
            content:thiz.state.inputAddress,
          },
          success:function(ret){
            thiz.setState({
              visible:false,
            });
            thiz.log("--------recognize_ret--------",ret);
            if(ret&&ret.respCode=="00"){
              thiz.setState({
                shouhuorenName:ret.data.name?ret.data.name:"",
                shouhuorenAddress:ret.data.address&&ret.data.address.detail?ret.data.address.detail:"",
                phoneNo:ret.data.phone?ret.data.phone:"",
                receiverIdCardNo:ret.data.idCard?ret.data.idCard:"",
                address:(function(){
                  let total = "";
                  if(ret.data.address&&ret.data.address.province&&ret.data.address.province.name){
                    total+=ret.data.address.province.name;
                  }
                  if(ret.data.address&&ret.data.address.city&&ret.data.address.city.name){
                    total+=ret.data.address.city.name;
                  }
                  if(ret.data.address&&ret.data.address.county&&ret.data.address.county.name){
                    total+=ret.data.address.county.name;
                  }
                  console.log("------total--------",total);
                  return total;
                })(),
                finalAddr:{
                  default:"请选择省市区",
                  province:{
                    name:ret.data.address&&ret.data.address.province&&ret.data.address.province.name?ret.data.address.province.name:"",
                  },
                  city:{
                    name:ret.data.address&&ret.data.address.city&&ret.data.address.city.name?ret.data.address.city.name:"",
                  },
                  area:{
                    name:ret.data.address&&ret.data.address.county&&ret.data.address.county.name?ret.data.address.county.name:"",
                  }
                },
                finalAreaId:(function(){
                  if(ret.data.address&&ret.data.address.county&&ret.data.address.county.id){
                    return ret.data.address.county.id;
                  }
                  if(ret.data.address&&ret.data.address.city&&ret.data.address.city.id){
                    return ret.data.address.city.id;
                  }
                  return null;
                })(),
              })
            }
          },
          error:function(err){
            thiz.setState({
              visible:false,
            });
          }
      })
    }

    // // 构造地址数据
    // getAddrData(){
    //   let thiz = this;
    //   let pickerData = [];
    //   thiz.log("--------getAddrData--------","getAddrData");
    //   if(thiz.pickerProvinceData.data.length>0&&thiz.pickerCityData.data.length>0&&thiz.pickerAreaData.data.length>0){
    //     // 开始构造数据
    //     // 构造省数据
    //     let indexp = thiz.pickerCityData.index;
    //     for(let i=0;i<thiz.pickerProvinceData.data.length;i++){
    //       let it = thiz.pickerProvinceData.data[i];
    //       let tempp = {};
    //       tempp[it.name] = [];
    //       // 处理没有数据的情况
    //       if(indexp!=i){
    //         tempp[it.name].push({
    //           "":[""]
    //         }); 
    //       }
    //       pickerData.push(tempp);
    //     }

    //     // 构造市数据
    //     let pname = thiz.pickerCityData.pname;
    //     let cityData = [];
    //     for(let ii=0;ii<thiz.pickerCityData.data.length;ii++){
    //       let iit = thiz.pickerCityData.data[ii];
    //       let tempc = {};
    //       tempc[iit.name] = [];
    //       cityData.push(tempc);
    //     }
    //     pickerData[indexp][pname] = cityData;

    //     // 构造区县数据
    //     let indexc = thiz.pickerAreaData.index;
    //     let cname = thiz.pickerAreaData.cname;
    //     let areaData = [];
    //     for(let iii=0;iii<thiz.pickerAreaData.data.length;iii++){
    //       let iiit = thiz.pickerAreaData.data[iii];
    //       areaData.push(iiit.name);
    //     }
    //     pickerData[indexp][pname][indexc][cname] = areaData;

    //     thiz.log("--------getAddrData--------",pickerData);
    //     thiz.pickerData = pickerData;
        
    //   }else{
    //     thiz.log("--------getAddrData--------","数据不充足");
    //   }
    // }

    // // 获取元素（市、区）
    // getElementByName(type,name){
    //   let thiz = this;
    //   let result = null;
    //   let queryArray = thiz[type]?thiz[type].data:null;

    //   if(queryArray){
    //     for(let i=0;i<queryArray.length;i++){
    //       if(queryArray[i].name==name){
    //         result = queryArray[i];
    //         result.index = i;
    //         break;
    //       }
    //     }
    //   }
    //   return result;
    // }

    // // 初始化地址选择器
    // initAddrPicker(data){
    //   let thiz = this;
    //   if(data.length>0){
    //     // picker没有显示才调用显示函数
    //     PickerNew.isPickerShow(function(ret){
    //       thiz.log("--------PickerNew.isPickerShow_ret--------",ret);
    //       if(!ret){
    //         PickerNew.init({
    //           pickerData: data,
    //           onPickerConfirm: data => {
    //             thiz.log("--------onPickerConfirm--------",data);
    //           },
    //           onPickerCancel: data => {
    //             thiz.log("--------onPickerCancel--------",data);
    //           },
    //           onPickerSelect: data => {
    //             thiz.log("--------onPickerSelect--------",data);
    //             // 判断省是否改变了
    //             if(data&&data.length==3&&data[0]!=thiz.sd.province.name){// 省改变
    //               // 加载市、区的数据
    //               // 根据选中的name来获取省的id
    //               let ele = thiz.getElementByName("pickerProvinceData",data[0]);
    //               thiz.log("--------ele--------",ele);
    //               // 重置选中的省
    //               thiz.sd.province = ele;
    //               if(ele){
    //                 // 获取市和区的数据
    //                 thiz.getCityData(ele.index,ele.name,ele.id,function(retc){
    //                   if(retc&&retc.data&&retc.data.length>0){
    //                     thiz.sd.city = retc.data[0];
    //                     thiz.pickerCityData = retc;

    //                     // 再获取区县
    //                     thiz.getAreaData(0,retc.data[0].name,retc.data[0].id,function(reta){
    //                       if(reta&&reta.data&&reta.data.length>0){
    //                         thiz.sd.area = reta.data[0];
    //                         // 只有获取完所有数据才能得到要显示的数据
    //                         thiz.pickerAreaData = reta;
    //                         // 构造显示的数据
    //                         thiz.getAddrData();

    //                         // 重新设置数据
    //                         let sv = [];
    //                         sv.push(thiz.sd.province?thiz.sd.province.name:"");
    //                         sv.push(thiz.sd.city?thiz.sd.city.name:"");
    //                         sv.push(thiz.sd.area?thiz.sd.area.name:"");
    //                         thiz.log("--------thiz.pickerData--------",thiz.pickerData);
    //                         thiz.log("--------sv--------",sv);
    //                         PickerNew.setData(thiz.pickerData,sv,function(a,b){
    //                           thiz.log("--------a--------",a);
    //                           thiz.log("--------b--------",b);
    //                         });
    //                       }
    //                     });
    //                   }
    //                 });

    //               }

    //             }else{// 省未变
    //               // 判断市是否改变了
    //             }

    //           }
    //         });
    //         PickerNew.show();
    //       }else{
    //         PickerNew.setData(thiz.pickerData);
    //       }
    //     });
    //   }
    // }
    
    componentDidMount(){
      var thiz=this;
    
      // 第一次获取地址数据
      thiz.loadAddrData();

    };
    //获取当前选中的省
    getVisibleRows=(info)=>{
            var thiz=this;
            thiz.log("------------info-----------",info);
            var name=info.viewableItems[2].item.name;
            var id=info.viewableItems[2].item.id;
            console.log("---------------------id-------------------",id);
            console.log("------------------name--------------------",name);
            thiz.setState({
              provinceId:id,
              provinceName:name,
            })
    }
    //获取当前选中的市
    getVisibleCityRows=(info)=>{
            var thiz=this;
            thiz.log("------------info111111-----------",info);
            if(info.viewableItems.length<3)
            {
                var cityname=info.viewableItems[0].item.name;
                var cityid=info.viewableItems[0].item.id;
                console.log("---------------------Cityid-------------------",cityid);
                console.log("------------------Cityname--------------------",cityname);
                thiz.setState({
                  cityId:cityid,
                  cityName:cityname
                });
            }
            else{
            var cityname=info.viewableItems[2].item.name;
            var cityid=info.viewableItems[2].item.id;
            console.log("---------------------Cityid-------------------",cityid);
            console.log("------------------Cityname--------------------",cityname);
            thiz.setState({
              cityId:cityid,
              cityName:cityname
            });
          }
    }
    //获取当前选中的区
    getVisibleAreaRows=(info)=>{
            var thiz=this;
            thiz.log("------------info111111-----------",info);
            if(info.viewableItems.length<5)
            {
              return ;
            }
            var areaname=info.viewableItems[2].item.name;
            var areaid=info.viewableItems[2].item.id;
            console.log("---------------------areaid-------------------",areaid);
            console.log("------------------areaname--------------------",areaname);
            thiz.setState({
              areaId:areaid,
              areaName:areaname,
            })
    }
   
    //打开正面身份证相册
   openAlbum=()=>{

      let thiz = this;
      ImagePicker.openPicker({  
          width: 300,  
          height: 400,  
          cropping:false,
          compressImageQuality:0.5,
      }).then(image => {  
        console.log("----------image.path---------",image.path);
        thiz.setState({
            visible:true,
        })
        let url = image.path;
        if(url&&BaseComponent.OS=="ios"){
          url = "file:///"+url;
        }
        thiz.upload(url,{
          progress:function(pro){

          },
          success:function(ret){
            thiz.setState({
              idCardImageFrontAttachmentID:ret.data.id,
              idCardImageFrontAttachment:image.path,
              visible:false
            })
          },
          error:function(err){
            thiz.setState({visible:false});
            console.log("--------------------调用上传接口失败------------------");
          }
        });
      })
    }
    //打开反面身份证相册
   openAlbum1=()=>{

      let thiz = this;
      ImagePicker.openPicker({  
          width: 300,  
          height: 400,  
          cropping:false,
          compressImageQuality:0.5,
      }).then(image => {  
        console.log("----------image.path---------",image.path);
        thiz.setState({
           visible:true,
        })
        let url = image.path;
        
        if(url&&BaseComponent.OS=="ios"){
          url = "file:///"+url;
        }
        thiz.upload(url,{
          progress:function(pro){

          },
          success:function(ret){
            thiz.setState({
              idCardImageBackAttachmentID:ret.data.id,
              idCardImageBackAttachment:image.path,
              visible:false,
            })
          },
          error:function(err){
            thiz.setState({visible:false});
            console.log("--------------------调用上传接口失败------------------");
          }
        });
      })
    }
    //save保存按钮点击事件
    save=(forceType)=>{
      var thiz=this;
      var reg = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/; 
      thiz.log("-------------------thiz.state.areaState.aid-----------------------",thiz.state.areaState.aid);
      var shouhuorenName=thiz.state.shouhuorenName;
      var phoneNo=thiz.state.phoneNo;
      var shouhuorenAddress=thiz.state.shouhuorenAddress;
      var receiverIdCardNo=thiz.state.receiverIdCardNo;
      var idCardImageFrontAttachmentID=thiz.state.idCardImageFrontAttachmentID;
      var idCardImageBackAttachmentID=thiz.state.idCardImageBackAttachmentID;
      var defaultAddress=false;
      var receiverArea=thiz.state.finalAreaId;

      if(shouhuorenName=="")
      {
        thiz.toast('请填写收货人姓名');
        return ;
      }
      if(phoneNo=="")
      {
        thiz.toast('请填写手机号');
        return ;
      }

      //判断是2级id还是三级id

      if(!receiverArea){
        thiz.toast("请选择地区");
        return;
      }
      if(shouhuorenAddress=="")
      {
        thiz.toast('请填写详细地址');
        return ;
      }
      // if(receiverIdCardNo=="")
      // {
      //   thiz.toast('请填写身份证号');
      //   return ;
      // }
      if(!reg.test(receiverIdCardNo))
      {
        thiz.toast("身份证格式不正确");
        return ;
      }
      // if(idCardImageFrontAttachmentID=="")
      // {
      //   thiz.toast('请上传身份证正面照片');
      //   return ;
      // }
      // if(idCardImageBackAttachmentID=="")
      // {
      //   thiz.toast('请上传身份证反面照片');
      //   return ;
      // }

      let paras = {
          defaultAddress:defaultAddress,
          idCardImageBackAttachment:{id:idCardImageBackAttachmentID},
          idCardImageFrontAttachment:{id:idCardImageFrontAttachmentID},
          receiverAddress:shouhuorenAddress,
          receiverArea:{id:receiverArea},
          receiverIdCardNo:receiverIdCardNo,
          receiverName:shouhuorenName,
          receiverPhoneNo:phoneNo,

          forceUpdateType:forceType?forceType:"NONE",
      };
      
      thiz.request("receiverAddress/saveOrUpdate",{
        method:"POST",
        data:paras,
        success:function(ret){

            if(ret&&ret.respCode=="00"){
              // 判断是否是从  购物车 和 确认订单  页面跳转过来的，如果是，默认就返回设置保存的地址，不需要再回到地址列表
              if(thiz.params.from=="Cart" || thiz.params.from=="Querendingdan" ||  thiz.params.from=="GoodsXiangQing"){
                // 发送保存返回的数据，设置新的地址数据
                thiz.emit("save_receiveraddr_success",{address:ret.data.receiverAddress});

                setTimeout(function(){
                  thiz.navigate(thiz.params.from);
                },200);
                
              }else{// 其他情况

                thiz.emit("save_receiveraddr_success");
                
                setTimeout(function(){
                  thiz.goBack();
                },200);

              }
            }
            
        },
        error:function(err){
          
          if(err.respCode=="-1" && err.errCode=="RECEIVER_ADDRESS_REPEATED"){// 地址重复

            // 弹窗提示是否覆盖
            thiz.setState({
              ismsgVisible:true,
              msg:err.msg,
            });

          }else{
            thiz.toast(err.msg);
          }

        }
      })
    }
    //渲染省
    _renderItemProvince=(info)=>{
      var thiz=this;

        return (
          <View style={{width:'100%',height:BaseComponent.W*50/375,justifyContent:'center',alignItems:'center'}}>
              <Text style={{fontSize:15,color:'black'}}>{info.item.name}</Text>
          </View>
        )
    }
    //渲染市
    _renderItemCity=(info)=>{
      var thiz=this;
      // thiz.log("---------------------------data----------------------",info);
        return (
          <View style={{width:'100%',height:BaseComponent.W*50/375,justifyContent:'center',alignItems:'center'}}>
              <Text style={{fontSize:15,color:'black'}}>{info.item.name}</Text>
          </View>
        )
    }
    //渲染区
    _renderItemArea=(info)=>{
      var thiz=this;
      // thiz.log("---------------------------data----------------------",info);
        return (
          <View style={{width:'100%',height:BaseComponent.W*50/375,justifyContent:'center',alignItems:'center'}}>
              <Text style={{fontSize:15,color:'black'}}>{info.item.name}</Text>
          </View>
        )
    }
    //获取市
    getCity=()=>{
      var thiz=this;
        thiz.request("area/getByParentId",{
          method:"POST",
          data:{parentAreaId:this.state.provinceId},
          success:function(ret){

            thiz.setState({
            city:ret.data,
            })
          },
          error:function(err){

          }
      })
    }
    //获取区县
    getArea=()=>{
      var thiz=this;
        thiz.request("area/getByParentId",{
          method:"POST",
          data:{parentAreaId:this.state.cityId},
          success:function(ret){
            thiz.setState({
            area:ret.data
            })
          },
          error:function(err){

          }
      })
    }
    //省市区选择器确定按钮
    ensure=()=>{
      var thiz=this;
      var provinceName=thiz.state.provinceName;
      var cityName=thiz.state.cityName;
      var areaName=thiz.state.areaName;
      thiz.setState({
        address:provinceName+cityName+areaName,
        isvisible:false,
      });
    }
    //唯一key
    _keyExtractor=(item,index)=>index.toString();


     render(){
      let thiz = this;
        return(   
          <View style={style.wrapper}>   
                {/*状态栏占位*/}
                <View style={{ backgroundColor:this.state.isvisible?'rgba(14,14,14,0.5)':"rgb(255,255,255)",
                      height:thiz.isIphoneX(30,BaseComponent.SH),
                      position:"relative",
                      zIndex:1,
                      opacity:1,}}></View>
                {/*顶部导航栏*/}
                <View style={style.navBar}>
                  <View style={{flex:1,flexDirection:"row",justifyContent:"flex-start"}}>

                    <TouchableWithoutFeedback onPress={()=>this.goBack()}>
                    <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>
                      <Image source={Imgs.back} style={{width:(BaseComponent.W*0.127*0.4),height:(BaseComponent.W*0.127*0.4),marginRight:BaseComponent.W*0.04}}></Image>
                    </View>
                    </TouchableWithoutFeedback>

                    <View style={{flexGrow:1,backgroundColor:"transparent",
                      marginTop:ti.select({ios:(BaseComponent.H*0.02)/2,android:(BaseComponent.H*0.016)/2}),borderRadius:ti.select({ios:(BaseComponent.H*0.05)/2,android:(BaseComponent.H*0.054)/2}),borderColor:"#0A0A0A",
                      borderWidth:0,}}>
                      <Text style={{color:"#0A0A0A",width:"100%",textAlign:"center",marginTop:ti.select({ios:"4.5%",android:"1.6%"}),fontSize:17}}>{this.params.title}</Text>
                    </View>

                    <TouchableWithoutFeedback onPress={()=>this.save("NONE")}>
                    <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>
                        <Text style={{fontSize:BaseComponent.W*0.037,color:'#0A0A0A'}}>保存</Text>
                    </View>
                    </TouchableWithoutFeedback>

                  </View>
                </View> 
             {/*转菊花*/}
              <Modal
                visible={this.state.visible}
                onRequestClose={()=>{this.setState({visible:false})}}
                transparent={true}
                animationType={"fade"}>
                <View style={{backgroundColor:"rgba(0,0,0,0.4)",width:"100%",width:"100%",flex:1,flexDirection:"column",alignItems:"center",justifyContent:"center",position:"relative"}}>
                  <View>
                        <ActivityIndicator size="large" color='white'/>
                  </View>
                </View>
              </Modal>

              {/*是否覆盖地址弹窗*/}
              <Modal visible={this.state.ismsgVisible}
                   transparent={true}
                       onRequestClose={() => {
                        thiz.setState({ismsgVisible:false});
                       }}>
                   <View style={styles.Modal}>
                    <View style={{width:BaseComponent.W*275/375,height:BaseComponent.W*105/375,backgroundColor:'rgba(255, 255, 255,1)',borderRadius:BaseComponent.W*20/375}}>
                      <View style={{width:'100%',height:BaseComponent.W*56/375,justifyContent:'center',alignItems:'center'}}>
                        <Text style={{fontSize:BaseComponent.W*15/375,color:'rgba(34, 34, 34, 1)'}}>{thiz.state.msg}</Text>
                      </View>
                      <View style={{width:'100%',height:0.5,backgroundColor:'rgba(189, 189, 189, 1)'}}></View>
                      <View style={{width:'100%',height:BaseComponent.W*49/375,flexDirection:'row'}}>
                      <TouchableWithoutFeedback onPress={()=>{
                        thiz.setState({ismsgVisible:false});
                        thiz.save("NEW");
                      }}> 
                        <View style={{width:'50%',height:BaseComponent.W*49/375,justifyContent:'center',alignItems:'center'}}>
                          <Text style={{fontSize:BaseComponent.W*15/375,color:'#FED584'}}>新增</Text>
                        </View>
                      </TouchableWithoutFeedback> 
                        <View style={{width:0.5,height:'100%',backgroundColor:'rgba(189, 189, 189, 1)'}}></View>
                        <TouchableWithoutFeedback onPress={()=>{
                          this.setState({ismsgVisible:false});
                          // 重置地址
                          thiz.save("UPDATE");
                        }}> 
                        <View style={{width:'50%',height:BaseComponent.W*49/375,justifyContent:'center',alignItems:'center'}}>
                          <Text style={{fontSize:BaseComponent.W*15/375,color:'rgba(94, 94, 94, 1)'}}>覆盖</Text>
                        </View>
                        </TouchableWithoutFeedback>
                      </View>
                    </View>
                   </View>
              </Modal>

            {/*新的省市区选择*/}
            <Modal visible={this.state.isAddrOpen}
                   transparent={true}
                   animationType={"fade"}
                   onShow={()=>{}}
                   onRequestClose={() => {this.setState({isAddrOpen:false})}}>
                   <TouchableWithoutFeedback onPress={()=>{
                    thiz.setState({
                      isAddrOpen:false,
                       provinceState:{
                        show:true,
                        pid:null,
                        title:"请选择",
                        selected:true,
                       },
                       cityState:{
                        show:false,
                        cid:null,
                        title:"请选择",
                        selected:false,
                       },
                       areaState:{
                        show:false,
                        aid:null,
                        title:"请选择",
                        selected:false,
                       },
                    });
                   }}>
                   <View style={{flex:1,flexDirection:"column",backgroundColor:"rgba(0,0,0,0.1)",position:"relative"}}>

                    <View style={{position:"absolute",bottom:0,left:0,right:0,width:"100%",backgroundColor:"white"}}>

                      {/*顶部按钮栏*/}
                      <View style={{width:"100%",flexDirection:"row",
                        
                        justifyContent:"space-between",backgroundColor:"white",
                        borderBottomWidth:1,borderBottomColor:"#eee"}}>

                        <TouchableWithoutFeedback onPress={()=>{
                          thiz.setState({
                            isAddrOpen:false,
                          });
                        }}>
                          <View style={{paddingTop:10,paddingBottom:10,paddingLeft:20,paddingRight:20}}>
                            <Text style={{fontSize:14,color:"#ccc"}}>取消</Text>
                          </View>
                        </TouchableWithoutFeedback>

                        <TouchableWithoutFeedback onPress={()=>{}}>
                          <View style={{paddingTop:10,paddingBottom:10,}}>
                            <Text style={{fontSize:16,fontColor:"#3a3a3a"}}>地区选择</Text>
                          </View>
                        </TouchableWithoutFeedback>
                        
                        <TouchableWithoutFeedback onPress={()=>{
                          if(thiz.state.hasArea){
                              if(!thiz.state.provinceState.pid || !thiz.state.cityState.cid || !thiz.state.areaState.aid){
                                thiz.setState({
                                  isAddrOpen:false,
                                });
                                return;
                              }
                              let address = thiz.state.provinceState.title+" "+thiz.state.cityState.title+" "+thiz.state.areaState.title;
                              thiz.setState({
                                isAddrOpen:false,
                                address:address,
                                finalAddr:{
                                  default:"请选择省市区",
                                  province:{
                                    name:thiz.state.provinceState.title,
                                  },
                                  city:{
                                    name:thiz.state.cityState.title,
                                  },
                                  area:{
                                    name:thiz.state.areaState.title,
                                  }
                                },
                                finalAreaId:thiz.state.areaState.aid
                              });
                            }else{
                              if(!thiz.state.provinceState.pid || !thiz.state.cityState.cid){
                                thiz.setState({
                                  isAddrOpen:false,
                                });
                                return;
                              }
                              let address = thiz.state.provinceState.title+" "+thiz.state.cityState.title;
                              thiz.setState({
                                isAddrOpen:false,
                                address:address,
                                finalAddr:{
                                  default:"请选择省市区",
                                  province:{
                                    name:thiz.state.provinceState.title,
                                  },
                                  city:{
                                    name:thiz.state.cityState.title,
                                  },
                                  area:{
                                    name:"",
                                  }
                                },
                                finalAreaId:thiz.state.cityState.cid
                              });
                            }
                        }}>
                          <View style={{paddingTop:10,paddingBottom:10,paddingLeft:20,paddingRight:20}}>
                            <Text style={{fontSize:14,color:"rgb(48,117,254)"}}>确定</Text>
                          </View>
                        </TouchableWithoutFeedback>

                      </View>

                      {/*地址显示区域*/}
                      <View style={{width:"100%",flexDirection:"column",backgroundColor:"white"}}>

                        {/*选中地址显示*/}
                        <View style={{width:"100%",borderBottomWidth:1,borderBottomColor:"#eee",flexDirection:"row",paddingLeft:10,paddingRight:10,}}>

                          <TouchableWithoutFeedback onPress={()=>{
                            // 先判断是否处于选中状态
                            if(!thiz.state.provinceState.selected){
                              let provinceState = thiz.state.provinceState;
                              let cityState = thiz.state.cityState;
                              let areaState = thiz.state.areaState;

                              provinceState.selected = true;
                              cityState.selected = false;
                              areaState.selected = false;
                              
                              thiz.setState({
                                provinceState:provinceState,
                                areaState:areaState,
                                cityState:cityState,
                              });
                            }
                          }}>
                          <View style={{flexDirection:"column",position:"relative",paddingTop:10,paddingBottom:10,marginRight:10,}}>
                            <Text numberOfLines={1} style={{color:thiz.state.provinceState.selected?"red":"#3a3a3a",maxWidth:BaseComponent.W*0.33-20,}}>{thiz.state.provinceState.title}</Text>
                            <View style={{height:2,width:"100%",backgroundColor:thiz.state.provinceState.selected?"red":"white",position:"absolute",bottom:0,left:0,right:0}}></View>
                          </View>
                          </TouchableWithoutFeedback>

                          <TouchableWithoutFeedback onPress={()=>{
                            // 先判断是否处于选中状态
                            if(!thiz.state.cityState.selected){
                              let provinceState = thiz.state.provinceState;
                              let cityState = thiz.state.cityState;
                              let areaState = thiz.state.areaState;

                              provinceState.selected = false;
                              cityState.selected = true;
                              areaState.selected = false;
                              
                              thiz.setState({
                                provinceState:provinceState,
                                areaState:areaState,
                                cityState:cityState,
                              });
                            }
                          }}>
                          <View style={{display:thiz.state.cityState.show?"flex":"none",flexDirection:"column",
                            position:"relative",paddingTop:10,paddingBottom:10,marginLeft:10,marginRight:10}}>
                            <Text numberOfLines={1} style={{color:thiz.state.cityState.selected?"red":"#3a3a3a",maxWidth:BaseComponent.W*0.33-20,}}>{thiz.state.cityState.title}</Text>
                            <View style={{height:2,width:"100%",backgroundColor:thiz.state.cityState.selected?"red":"white",position:"absolute",bottom:0,left:0,right:0}}></View>
                          </View>
                          </TouchableWithoutFeedback>

                          <TouchableWithoutFeedback onPress={()=>{
                            // 先判断是否处于选中状态
                            if(!thiz.state.areaState.selected){
                              let provinceState = thiz.state.provinceState;
                              let cityState = thiz.state.cityState;
                              let areaState = thiz.state.areaState;

                              provinceState.selected = false;
                              cityState.selected = false;
                              areaState.selected = true;
                              
                              thiz.setState({
                                provinceState:provinceState,
                                areaState:areaState,
                                cityState:cityState,
                              });
                            }
                          }}>
                          <View style={{display:thiz.state.areaState.show?"flex":"none",flexDirection:"column",
                            position:"relative",paddingTop:10,paddingBottom:10,marginLeft:10}}>
                            <Text numberOfLines={1} style={{color:thiz.state.areaState.selected?"red":"#3a3a3a",maxWidth:BaseComponent.W*0.33-20,}}>{thiz.state.areaState.title}</Text>
                            <View style={{height:2,width:"100%",backgroundColor:thiz.state.areaState.selected?"red":"white",position:"absolute",bottom:0,left:0,right:0}}></View>
                          </View>
                          </TouchableWithoutFeedback>

                        </View>

                        {/*地址滚动视图*/}
                        {/*省*/}
                        <View style={{display:thiz.state.provinceState.selected?"flex":"none",width:"100%",height:BaseComponent.H*0.35,backgroundColor:"white"}}>
                          <ScrollView showsVerticalScrollIndicator={false}
                            style={{width:"100%",height:"100%"}}
                            horizontal={false}>

                            {
                              thiz.state.provinceData.length>0?thiz.state.provinceData.map(function(v,i){
                                return (
                                  <TouchableWithoutFeedback onPress={()=>{
                                    // 重置选中状态
                                    // if(!v.selected){

                                      // 加载市的数据
                                      thiz.getCityData(v.id,function(ret){
                                        
                                        if(ret&&ret.data&&ret.data.length>0){
                                          let provinceData = thiz.state.provinceData;
                                          for(let m=0;m<provinceData.length;m++){
                                            if(m!=i){
                                              provinceData[m].selected = false;
                                            }else{
                                              provinceData[m].selected = true;
                                            }
                                          }
                                          // 处理数据
                                          for(let k=0;k<ret.data.length;k++){
                                            ret.data[k].selected = false;
                                          }
                                          thiz.setState({
                                            provinceData:provinceData,
                                            cityData:ret.data,
                                            areaData:[],
                                            provinceState:{
                                              title:v.name,
                                              pid:v.id,
                                              show:true,
                                              selected:false,
                                            },
                                            cityState:{
                                              title:"请选择",
                                              cid:null,
                                              show:true,
                                              selected:true,
                                            },
                                            areaState:{
                                              title:"请选择",
                                              cid:null,
                                              show:false,
                                              selected:false,
                                            }
                                          });
                                        }
                                      });
                                    // }
                                  }}>
                                  <View style={{padding:10,position:"relative"}}>
                                    <Text style={{color:v.selected?"red":"#3a3a3a"}}>{v.name}</Text>
                                  </View>
                                  </TouchableWithoutFeedback>
                                );
                              }):null
                            }

                          </ScrollView>
                        </View>

                        {/*市*/}
                        <View style={{display:thiz.state.cityState.selected?"flex":"none",width:"100%",height:BaseComponent.H*0.35,backgroundColor:"white"}}>
                          <ScrollView showsVerticalScrollIndicator={false}
                            style={{width:"100%",height:"100%"}}
                            horizontal={false}>

                            {
                              thiz.state.cityData.length>0?thiz.state.cityData.map(function(v,i){
                                return (
                                  <TouchableWithoutFeedback onPress={()=>{
                                    // 重置选中状态
                                    thiz.log("------------------v---------------------",v);
                                    // if(!v.selected){

                                      // 加载市的数据
                                      thiz.getAreaData(v.id,function(ret){

                                        if(ret&&ret.data&&ret.data.length>0){
                                          let cityData = thiz.state.cityData;
                                          for(let m=0;m<cityData.length;m++){
                                            if(m!=i){
                                              cityData[m].selected = false;
                                            }else{
                                              cityData[m].selected = true;
                                            }
                                          }
                                          // 处理数据
                                          for(let k=0;k<ret.data.length;k++){
                                            ret.data[k].selected = false;
                                          }

                                          let provinceState = thiz.state.provinceState;
                                          provinceState.selected = false;

                                          thiz.setState({
                                            cityData:cityData,
                                            areaData:ret.data,
                                            provinceState:provinceState,
                                            cityState:{
                                              title:v.name,
                                              cid:v.id,
                                              show:true,
                                              selected:false,
                                            },
                                            areaState:{
                                              title:"请选择",
                                              aid:null,
                                              show:true,
                                              selected:true,
                                            },
                                            hasArea:true,
                                          });
                                        }
                                        //返回数据没有第三级时
                                        if(ret&&ret.data&&ret.data.length==0){
                                          let cityData = thiz.state.cityData;
                                          for(let m=0;m<cityData.length;m++){
                                            if(m!=i){
                                              cityData[m].selected = false;
                                            }else{
                                              cityData[m].selected = true;
                                            }
                                          }
                                          let provinceState = thiz.state.provinceState;
                                          provinceState.selected = false;

                                           thiz.setState({
                                            cityData:cityData,
                                            areaData:ret.data,
                                            provinceState:provinceState,
                                            cityState:{
                                              title:v.name,
                                              cid:v.id,
                                              show:true,
                                              selected:true,
                                            },
                                            areaState:{
                                              title:"请选择",
                                              aid:null,
                                              show:false,
                                              selected:false,
                                            },
                                            hasArea:false,
                                          });
                                          thiz.log("----------------------cityData--------------------------",cityData);
                                        }
                                      });
                                    // }
                                  }}>
                                  <View style={{padding:10,position:"relative"}}>
                                    <Text style={{color:v.selected?"red":"#3a3a3a"}}>{v.name}</Text>
                                  </View>
                                  </TouchableWithoutFeedback>
                                );
                              }):null
                            }

                          </ScrollView>
                        </View>

                        {/*区、县*/}
                        <View style={{display:thiz.state.areaState.selected?"flex":"none",width:"100%",height:BaseComponent.H*0.35,backgroundColor:"white"}}>
                          <ScrollView showsVerticalScrollIndicator={false}
                            style={{width:"100%",height:"100%"}}
                            horizontal={false}>

                            {
                              thiz.state.areaData.length>0?thiz.state.areaData.map(function(v,i){
                                return (
                                  <TouchableWithoutFeedback onPress={()=>{
                                    // 重置选中状态
                                    let areaData = thiz.state.areaData;
                                    for(let m=0;m<areaData.length;m++){
                                      if(m!=i){
                                        areaData[m].selected = false;
                                      }else{
                                        areaData[m].selected = true;
                                      }
                                    }
                                    thiz.setState({
                                      areaData:areaData,
                                      areaState:{
                                        title:v.name,
                                        aid:v.id,
                                        show:true,
                                        selected:true,
                                      },
                                    });
                                  }}>
                                  <View style={{padding:10,position:"relative"}}>
                                    <Text style={{color:v.selected?"red":"#3a3a3a"}}>{v.name}</Text>
                                  </View>
                                  </TouchableWithoutFeedback>
                                );
                              }):null
                            }

                          </ScrollView>
                        </View>

                      </View>

                    </View>

                   </View>
                   </TouchableWithoutFeedback>

            </Modal>

            <ScrollView style={{width:'100%',height:'100%'}} showsVerticalScrollIndicator = {false}>
            <KeyboardAwareScrollView>
            {/*识别地址*/}
            <View style={{width:'100%',height:BaseComponent.W*0.5,backgroundColor:'#F0F0F0',alignItems:'center',display:'flex'}}>

                <TouchableWithoutFeedback onPress={()=>{
                  thiz.setState({
                    showRecInput:true,
                  });
                }}>
                <ImageBackground style={styles.ImageBackground} source={require('../../image/home/tianjiadizhi.png')}>

                        <Text style={{fontSize:BaseComponent.W*0.04,color:'#3A3A3A',display:thiz.state.showRecInput?"none":"flex"}}>在此区域粘贴收货人信息</Text>
                        <Text style={{fontSize:BaseComponent.W*0.032,color:'#A1A1A1',marginTop:BaseComponent.W*0.029,display:thiz.state.showRecInput?"none":"flex"}}>姓名,详细地址,电话,身份证号码</Text>

                        <TextInput style={{display:thiz.state.showRecInput?"flex":"none",width:BaseComponent.W*0.938-BaseComponent.W*10/375,height:BaseComponent.W*0.339-ti.select({ios:30,android:20}),
                        borderColor: 'transparent',borderWidth:0.5,borderRadius:0,marginLeft:BaseComponent.W*5/375,
                          marginRight:BaseComponent.W*5/375,color:"#232326",paddingLeft:10,fontSize:BaseComponent.W*15/375,textAlignVertical:'top'}}
                                      underlineColorAndroid='transparent' maxLength={500} placeholder={"在此区域粘贴收货人信息，姓名，详细地址，电话，身份证号码"}
                                      onChangeText={(event)=>this.setState({inputAddress:event})} multiline={true}></TextInput>

                </ImageBackground>
                </TouchableWithoutFeedback>

                <TouchableWithoutFeedback onPress={()=>{
                  thiz.recognize();
                }}>
                <View style={[styles.shibie,{backgroundColor:thiz.state.inputAddress?'#FED584':'#fff'}]}>
                    <Text style={{fontSize:BaseComponent.W*0.04,color:thiz.state.inputAddress?'#000':'#CDCDCD'}}>识别地址</Text>
                </View>
                </TouchableWithoutFeedback>

            </View>
            {/*填写地址*/}
            <View style={styles.write}>
                <View style={styles.write1}>
                <Text style={{fontSize:BaseComponent.W*0.037,color:'#121212'}}><Text style={{color:'#FF0000'}}>*</Text>收货人</Text>
                <TextInput value={thiz.state.shouhuorenName} placeholder="输入姓名" style={{width:'100%',height:BaseComponent.W*0.106,padding:0,marginLeft:BaseComponent.W*0.104,fontSize:BaseComponent.W*0.037,color:'#121212'}} placeholderTextColor="#C7C7CD"
                        underlineColorAndroid='transparent'  onChangeText={(event)=>this.setState({shouhuorenName:event})}></TextInput>
                <View style={{position:'absolute',bottom:0,width:"100%",height:0.5,backgroundColor:'#E4E4E4'}}></View>
                </View>  

                <View style={styles.write1}>
                <Text style={{fontSize:BaseComponent.W*0.037,color:'#121212'}}><Text style={{color:'#FF0000'}}>*</Text>手机    </Text>
                <TextInput value={thiz.state.phoneNo} placeholder="输入联系方式" style={{width:'100%',height:BaseComponent.W*0.106,padding:0,marginLeft:BaseComponent.W*0.104,fontSize:BaseComponent.W*0.037,color:'#121212'}} placeholderTextColor="#C7C7CD"
                        underlineColorAndroid='transparent' onChangeText={(event)=>this.setState({phoneNo:event})}></TextInput>
                <View style={{position:'absolute',bottom:0,width:"100%",height:0.5,backgroundColor:'#E4E4E4'}}></View>
                </View>
                <SelectAddressOrDate  ref={'SelectAddressOrDate'}  callBackAddressValue={this.callBackAddressValue.bind(this)}/>
                
                {/*选择省市区*/}
                <TouchableWithoutFeedback onPress={()=>{
                  let thiz = this;
                  thiz.setState({
                    isAddrOpen:!thiz.state.isAddrOpen,
                  })
                }}>
                <View style={styles.write1}>
                  <Text style={{fontSize:BaseComponent.W*0.037,color:'#121212'}}><Text style={{color:'#FF0000'}}>*</Text>地区</Text>
                  <View style={{flexDirection:"row",position:"absolute",right:30,}}>
                    <Text numberOfLines={1} style={{maxWidth:BaseComponent.W*0.33,fontSize:BaseComponent.W*0.037,color:this.state.finalAddr.province.name?'#3a3a3a':'#C7C7CD',marginRight:5}}>{this.state.finalAddr.province.name?this.state.finalAddr.province.name:this.state.finalAddr.default}</Text>
                    <Text numberOfLines={1} style={{maxWidth:BaseComponent.W*0.33,fontSize:BaseComponent.W*0.037,color:this.state.finalAddr.city.name?'#3a3a3a':'#C7C7CD',marginRight:5}}>{this.state.finalAddr.city.name}</Text>
                    <Text numberOfLines={1} style={{maxWidth:BaseComponent.W*0.33,fontSize:BaseComponent.W*0.037,color:this.state.finalAddr.area.name?"#3a3a3a":'#C7C7CD',marginRight:0}}>{this.state.finalAddr.area.name}</Text>
                  </View>
                  <Image style={{width:BaseComponent.W*0.02,height:BaseComponent.W*0.07,position:'absolute',right:0,marginRight:BaseComponent.W*0.03}} 
                        source={require('../../image/home/youjiantou.png')}></Image>
                  <View style={{position:'absolute',bottom:0,width:"100%",height:0.5,backgroundColor:'#E4E4E4'}}></View>
                </View>
                </TouchableWithoutFeedback>
                

                <View style={{width:BaseComponent.W*0.968,height:BaseComponent.W*0.25,marginLeft:BaseComponent.W*0.032}}>
                  <TextInput value={thiz.state.shouhuorenAddress} placeholder="详细地址（请勿填写省市区信息）" style={{width:'100%',height:'100%',fontSize:BaseComponent.W*0.037,color:'#121212',textAlignVertical:'top'}} placeholderTextColor="#C7C7CD"
                         underlineColorAndroid='transparent' multiline={true} onChangeText={(event)=>this.setState({shouhuorenAddress:event})}></TextInput>
                  <View style={{position:'absolute',bottom:0,width:"100%",height:0.5,backgroundColor:'#E4E4E4'}}></View>
                </View>

                <View style={styles.write1}>
                  <Text style={{fontSize:BaseComponent.W*0.037,color:'#121212'}}><Text style={{color:'#FF0000'}}>*</Text>身份证号</Text>
                  <TextInput value={thiz.state.receiverIdCardNo} placeholder="输入身份证号码" style={{width:'100%',height:BaseComponent.W*0.106,padding:0,marginLeft:BaseComponent.W*0.067,fontSize:BaseComponent.W*0.037,color:'#121212'}} placeholderTextColor="#C7C7CD"
                        underlineColorAndroid='transparent' onChangeText={(event)=>this.setState({receiverIdCardNo:event})} maxLength={18}></TextInput>
                  <View style={{position:'absolute',bottom:0,width:"100%",height:0.5,backgroundColor:'#E4E4E4'}}></View>
                </View>


                <View style={{width:'100%',height:BaseComponent.W*0.4}}>

                <View style={{flexDirection:'row',marginTop:BaseComponent.W*0.045,marginLeft:BaseComponent.W*0.2}}>
                  
                  <Text style={{fontSize:BaseComponent.W*0.032,color:'#666666'}}>请提供身份证正反面以供清关</Text>
                  <Text style={{fontSize:BaseComponent.W*0.029,color:'#C7C7CD',marginLeft:BaseComponent.W*0.018}}>(信息与收货人一致)</Text>
                </View>
   
                <View style={{width:BaseComponent.W*0.563,height:BaseComponent.W*0.192,
                              flexDirection:'row',marginLeft:BaseComponent.W*0.2,
                              justifyContent:'space-between',marginTop:BaseComponent.W*0.026}}>
                  <TouchableWithoutFeedback onPress={()=>this.openAlbum()}>                 
                  <View>  
                  <Image style={{width:BaseComponent.W*0.192,height:BaseComponent.W*0.192}} source={this.state.idCardImageFrontAttachment==" "?require('../../image/home/shizijia.png'):{uri:this.state.idCardImageFrontAttachment}}></Image>
                  </View>
                  </TouchableWithoutFeedback>

                  <TouchableWithoutFeedback onPress={()=>this.openAlbum1()}> 
                  <View>
                  <Image style={{width:BaseComponent.W*0.192,height:BaseComponent.W*0.192}} source={this.state.idCardImageBackAttachment==" "?require('../../image/home/shizijia.png'):{uri:this.state.idCardImageBackAttachment}}></Image>
                  </View>
                  </TouchableWithoutFeedback>
                </View>
                 

                <View style={{width:BaseComponent.W*0.563,height:BaseComponent.W*0.06,
                              flexDirection:'row',marginLeft:BaseComponent.W*0.2,
                              justifyContent:'space-between',marginTop:BaseComponent.W*0.026}}>
                              
                    <View style={{width:BaseComponent.W*0.192,height:BaseComponent.W*0.053,backgroundColor:'#E3E3E3',
                                justifyContent:'center',alignItems:'center'}}>
                          <Text style={{fontSize:BaseComponent.W*0.029,color:'#fff'}}>正面</Text>
                    </View>

                    <View style={{width:BaseComponent.W*0.192,height:BaseComponent.W*0.053,backgroundColor:'#E3E3E3',
                                justifyContent:'center',alignItems:'center'}}>
                          <Text style={{fontSize:BaseComponent.W*0.029,color:'#fff'}}>反面</Text>
                    </View>
                </View>

                </View>
                
            </View>
            </KeyboardAwareScrollView>  
            </ScrollView>
        </View>
        )
    }
}
const styles=StyleSheet.create({
   recognition:{
        width:'100%',
        height:BaseComponent.W*0.45,
        backgroundColor:'#F0F0F0'
   },
   ImageBackground:{
    width:BaseComponent.W*0.938,
    height:BaseComponent.W*0.339,
    marginTop:BaseComponent.W*0.032,
    justifyContent:'center',
    alignItems:'center'
   },
   shibie:{
    width:BaseComponent.W*0.48,
    height:BaseComponent.W*0.08,
    // backgroundColor:'#fff',
    borderRadius:BaseComponent.W*0.04,
    marginTop:BaseComponent.W*0.027,
    justifyContent:'center',
    alignItems:'center'
   },
   write:{
    width:'100%',
    flexDirection:'column'
   },
   write1:{
    width:BaseComponent.W*0.968,
    height:BaseComponent.W*0.12,
    alignItems:'center',
    flexDirection:'row',
    marginLeft:BaseComponent.W*0.032
   },
   modal:{
    width:'100%',
    height:'100%',
    backgroundColor:'rgba(14,14,14,0.5)',
    justifyContent:'flex-end'
   },
   Modal:{
    flex:1,
    backgroundColor:'rgba(14,14,14,0.5)',
    justifyContent:'center',alignItems:'center',
    position:"relative",
    zIndex:10000,
  }
});