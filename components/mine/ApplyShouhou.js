/**
 * @name ApplyShouhou.js
 * @auhor 程浩
 * @date 2018.8.21
 * @desc 申请售后退货退款界面
 */
import React,{Component} from 'react';
import {Text,View,StyleSheet,Image,TouchableWithoutFeedback,ScrollView,Modal,TextInput,StatusBar,BackHandler,ActivityIndicator} from 'react-native';
 // 导入工具函数库
import T from "../../lib/Tools";
// 导入公共样式
import St from "../../lib/Style";
// 导入图片资源库
import Imgs from "../../lib/Img";
import BaseComponent from '../BaseComponent';
// 转菊花
import Spinner from 'react-native-loading-spinner-overlay';
import ImagePicker from 'react-native-image-crop-picker'; 
import SelectAddressOrDate from '../SelectAddressOrDate';
// Tools实例
let ti = T.getInstance();

let style = StyleSheet.create({
  wrapper:{
    backgroundColor:"rgba(255,255,255,1)",
    flex:1,
    flexDirection:"column",
    alignItems:"stretch",
    justifyContent:"flex-start",
    position:"relative",
  },
  statusBar:{
  	backgroundColor:"rgb(255,255,255)",
    height:ti.select({ios:16,android:22}),
    position:"relative",
    zIndex:1,
    opacity:1,
  },
  navBar:{
    backgroundColor:"rgb(255,255,255)",
    height:"7%",
    position:"relative",
    zIndex:1,
    opacity:1,
    borderBottomWidth:1,
    borderBottomColor:"rgb(241,241,241)",
    flexDirection:'row',
    alignItems:'center',
  },
});
export default class ApplyShouhou extends BaseComponent{
  constructor(props){
    super(props);
      this.state={
        data:{sku:{imageAttachment:{resourceFullAddress:""},goodsAttributeOptions:[{goodsAttributeOption:""},{goodsAttributeOption:""}]},maxRefundAmount:""},//退货商品信息
        num:1,//申请数量
        problemDesc:"",//问题描述
        applyRefundAmount:"",//退款金额
        refundReasonId:"",//退货原因的ID
        attachmentIds:[],//上传图片
        refundReason:[],//选择退货原因
        visible:false,//转菊花
        picture:[],//图片路径
        isvisible:false,//选择退货原因弹框
        remindvisible:false,//提交按钮提醒框
        reason:[],
        netError:false,//true表示网络错误
      }
  };
  componentDidMount(){
    var thiz=this;
    var data=thiz.params.data;
    thiz.log("------------------------------退货退款data------------------------------",data);
    thiz.setState({data:data,visible:true});
    //获取选择退货原因的数据
    thiz.request("refundReason/getAll",{
      method:"POST",
      data:{},
      success:function(ret){  
          if(!ret.data)
          {
            thiz.setState({visible:false});
            return ;
          }
          thiz.setState({visible:false,refundReason:ret.data});
      },
      error:function(err){
          thiz.setState({visible:false});
          if(err.code&&err.code==200){
            console.log("11111111111111111111111111111111");
            thiz.setState({netError:true});
          }  
      }
    });

    //监听选择取消ID的消息
    thiz.listen("selected_cancel_id",function(ret){
      thiz.log("----------------------------------ret_id--------------------------",ret);
      thiz.setState({refundReasonId:ret.id});
    });
  };
  //申请数量减法
  minusNum=()=>{
    var thiz=this;
    var num=thiz.state.num;
    if(num<=1)
    {
      console.log("--------------------------------------------");
      return ;
    }
    else
    {
      num--;
      thiz.setState({num:num});
    } 

  };
  //申请数量加法
  addNum=()=>{
    var thiz=this;
    var num=thiz.state.num;
    if(num<thiz.state.data.num)
    {
      num++;
      
      thiz.setState({num:num});
    }
    else
    {
      console.log("+++++++++++++++++++++++++++++++++++++++++++++++++++");
      return ;
    }
  };

//上传照片
  openAlbum=()=>{
      let thiz = this;
      ImagePicker.openPicker({  
          width: 300,  
          height: 400,  
          cropping:false,
          compressImageQuality:0.5,
      }).then(image => {  
        console.log("----------image.path---------",image.path);
       
       
        let url = image.path;
        if(url&&BaseComponent.OS=="ios"){
          url = "file:///"+url;
        }
        thiz.setState({visible:true});
        thiz.upload(url,{
          progress:function(pro){

          },
          success:function(ret){
            var picture=thiz.state.picture;
            picture.push(image.path);
            var attachmentIds=thiz.state.attachmentIds;
            attachmentIds.push(ret.data.id);
            
            thiz.setState({
                attachmentIds:attachmentIds,
                picture:picture,
                visible:false,
              });

          },
          error:function(err){
            thiz.setState({visible:false});
            console.log("--------------------调用上传接口失败------------------");
          }
        });
      })
    };
    //提交按钮
  summit=()=>{
      var thiz=this;
      if(thiz.state.refundReasonId=="")
      {
        thiz.toast("请选择退货原因");
        return ;
      }
      if(thiz.state.applyRefundAmount=="")
      {
        thiz.toast("请输入退款金额");
        return ;
      }
      if(thiz.state.problemDesc=="")
      {
        thiz.toast("请填写详细描述");
        return ;
      }
      if(thiz.state.attachmentIds.length==0)
      {
        thiz.toast("请上传至少一张图片");
        return ;
      }
      if(thiz.state.refundReasonId==""||thiz.state.applyRefundAmount==""||thiz.state.problemDesc==""||thiz.state.attachmentIds.length==0)
      {
        console.log("-------------------------------------------------");
        return ;
      }
      else
      {
       thiz.setState({remindvisible:true});
      }
  };

  callBackAddressValue(value){
          this.setState({
              reason:value
          })
  };
  open(){
       this.refs.SelectAddressOrDate.showAddress(this.state.reason)
  };
  render(){
    let thiz=this;
    return (
      <View style={style.wrapper}>
                {/*状态栏占位*/}
              <View style={{backgroundColor:this.state.visible||this.state.isvisible?"rgba(14,14,14,0.5)":"rgb(255,255,255)",
                  height:this.isIphoneX(30,BaseComponent.SH),
                  position:"relative",
                  zIndex:1,
                  opacity:1}}></View>
                {/*顶部导航栏*/}
                <View style={style.navBar}>
                <TouchableWithoutFeedback onPress={()=>this.goBack()}>
                <Image style={{width:BaseComponent.W*0.127*0.4,height:BaseComponent.W*0.127*0.4,marginLeft:BaseComponent.W*8/375}} source={Imgs.back}></Image>
                </TouchableWithoutFeedback> 
                <Text style={{color:'#0A0A0A',fontSize:BaseComponent.W*18/375,marginLeft:BaseComponent.W*130/375}}>{this.params.title}</Text>
                <Text style={{fontSize:BaseComponent.W*15/375,color:'#343434',marginLeft:BaseComponent.W*35/375}}>售后政策</Text>
                <TouchableWithoutFeedback onPress={()=>{
                      // 跳转客服
                      thiz.goPage("Service",{title:"客服"});
                    }}>  
                <Image style={{width:BaseComponent.W*21/375,height:BaseComponent.W*22/375,marginLeft:BaseComponent.W*17/375}} source={require('../../image/home/kefu.png')}></Image>
                </TouchableWithoutFeedback>
              </View>
              <SelectAddressOrDate ref={'SelectAddressOrDate'} callBackAddressValue={this.callBackAddressValue.bind(this)}/>
              {/*转菊花*/}
              <Modal
                visible={this.state.visible}
                onRequestClose={()=>{this.setState({visible:false})}}
                transparent={true}
                animationType={"fade"}>
                <View style={{backgroundColor:"rgba(14,14,14,0.5)",width:"100%",width:"100%",flex:1,flexDirection:"column",alignItems:"center",justifyContent:"center",position:"relative"}}>
                  <View>
                        <ActivityIndicator size="large" color='white'/>
                  </View>
                </View>
              </Modal>

              {/*选择退货原因弹框*/}
              <Modal visible={this.state.isvisible}
                     transparent={true}
                     onRequestClose={() => {this.setState({isvisible:false})}}>
                  <TouchableWithoutFeedback onPress={()=>this.setState({isvisible:false})}>
                  <View style={{flex:1,backgroundColor:'rgba(14,14,14,0.5)',justifyContent:'center',alignItems:'center'}}>
                      <View style={{width:BaseComponent.W*0.7,alignItems:'center',backgroundColor:'white',borderRadius:10}}>
                          {
                            this.state.refundReason.map((item,index)=>{
                              this.log("------------------------------item----------------------------",item);
                              return (
                                  <TouchableWithoutFeedback onPress={()=>{
                                    this.setState({refundReasonId:item.id,reason:item.value,isvisible:false});
                                  }}>
                                  <View style={{width:'100%',height:BaseComponent.W*40/375,justifyContent:'center',alignItems:'center',borderRadius:BaseComponent.W*5/375}} key={index}>
                                      <Text style={{fontSize:BaseComponent.W*15/375,color:'black'}}>{item.value}</Text>
                                      <View style={{width:'100%',height:0.5,marginTop:5,backgroundColor:'#F0F0F0'}}></View>
                                  </View>
                                  </TouchableWithoutFeedback>
                              )
                            })
                          }
                      </View>
                  </View>
                  </TouchableWithoutFeedback>
              </Modal>

                 {/*提醒提交订单*/}
                <Modal visible={this.state.remindvisible}
                       onRequestClose={()=>{this.setState({remindvisible:false})}}
                       transparent={true}>
                       <View style={{flex:1,backgroundColor:'rgba(14,14,14,0.5)',justifyContent:'center',alignItems:'center'}}>
                          <View style={{width:BaseComponent.W*275/375,height:BaseComponent.W*105/375,backgroundColor:'rgba(255, 255, 255,1)',borderRadius:BaseComponent.W*20/375}}>
                              <View style={{width:'100%',height:BaseComponent.W*56/375,justifyContent:'center',alignItems:'center'}}>
                                <Text style={{fontSize:BaseComponent.W*15/375,color:'rgba(94, 94, 94, 1)'}}>确定要提交该售后吗?</Text>
                              </View>
                              <View style={{width:'100%',height:0.5,backgroundColor:'rgba(189, 189, 189, 1)'}}></View>
                              <View style={{width:'100%',height:BaseComponent.W*49/375,flexDirection:'row'}}>
                              <TouchableWithoutFeedback onPress={()=>this.setState({remindvisible:false})}> 
                                <View style={{width:'50%',height:BaseComponent.W*49/375,justifyContent:'center',alignItems:'center'}}>
                                  <Text style={{fontSize:BaseComponent.W*15/375,color:'rgba(94, 94, 94, 1)'}}>取消提交</Text>
                                </View>
                              </TouchableWithoutFeedback> 
                                <View style={{width:0.5,height:'100%',backgroundColor:'rgba(189, 189, 189, 1)'}}></View>
                                <TouchableWithoutFeedback onPress={()=>{
                                   var refundReasonId=thiz.state.refundReasonId;
                                    var applyRefundAmount=thiz.state.applyRefundAmount;
                                    var problemDesc=thiz.state.problemDesc;
                                    var attachmentIds=thiz.state.attachmentIds;
                                    var orderInfoItemId=thiz.state.data.orderInfoItemId;
                                    var num=thiz.state.num;
                                    thiz.setState({remindvisible:false});
                                    if(applyRefundAmount<=thiz.state.data.maxRefundAmount)
                                    {
                                        console.log("-----refundReasonId----",refundReasonId);
                                        console.log("-----applyRefundAmount----",applyRefundAmount);
                                        console.log("-----problemDesc----",problemDesc);
                                        console.log("-----attachmentIds----",attachmentIds);
                                        console.log("-----orderInfoItemId----",orderInfoItemId);
                                        console.log("-------num--------------",num);  
                                        thiz.setState({visible:true});
                                        thiz.request("afterSale/applyRefund",{
                                          method:"POST",
                                          data:{afterSaleRefundType:'RETURN_GOODS_REFUND_AMOUNT',
                                            applyRefundAmount:applyRefundAmount,
                                            attachmentIds:attachmentIds,
                                            orderInfoItemId:orderInfoItemId,
                                            problemDesc:problemDesc,
                                            refundReasonId:refundReasonId,
                                            num:num
                                          },
                                          success:function(ret){
                                              thiz.setState({visible:false});
                                              thiz.navigate('Wodedingdan',{title:"我的订单"});
                                          },
                                          error:function(err){
                                              thiz.setState({visible:false});
                                          }
                                        })
                                    }
                                    else
                                    {
                                      thiz.toast("退款金额最多为"+thiz.state.data.maxRefundAmount+"");
                                    }
                                }}> 
                                <View style={{width:'50%',height:BaseComponent.W*49/375,justifyContent:'center',alignItems:'center'}}>
                                  <Text style={{fontSize:BaseComponent.W*15/375,color:'#FED584'}}>确认提交</Text>
                                </View>
                                </TouchableWithoutFeedback>
                              </View>
                          </View>
                       </View>
                </Modal>

            {
              thiz.state.netError?(
                    <View style={{width:BaseComponent.W,height:BaseComponent.H*0.85,justifyContent:'center',alignItems:'center'}}>
                    <Image style={{width:BaseComponent.W*180/375,height:BaseComponent.W*180/375,borderRadius:BaseComponent.W*90/375}} source={require('../../image/mine/neterror.png')}></Image>
                    <TouchableWithoutFeedback onPress={()=>{
                      thiz.setState({netError:false,visible:true});
                      thiz.request("refundReason/getAll",{
                        method:"POST",
                        data:{},
                        success:function(ret){  
                            if(!ret.data)
                            {
                              thiz.setState({visible:false});
                              return ;
                            }
                            thiz.setState({visible:false,refundReason:ret.data});
                        },
                        error:function(err){
                            thiz.setState({visible:false});
                            if(err.code&&err.code==200){
                              console.log("11111111111111111111111111111111");
                              thiz.setState({netError:true});
                            }  
                        }
                      })
                    }}>
                    <View style={{width:BaseComponent.W*70/375,height:BaseComponent.W*30/375,borderRadius:BaseComponent.W*15/375,borderWidth:0.5,borderColor:'#080808',
                              justifyContent:'center',alignItems:'center',marginTop:BaseComponent.W*15/375}}>
                          <Text style={{fontSize:BaseComponent.W*15/375,color:'#252525'}}>刷新</Text>          
                    </View>
                    </TouchableWithoutFeedback>
                  </View>):(
                  <TouchableWithoutFeedback onPress={()=>{
                    console.log("------------------------------------------------cao--------------------------------------");
                    thiz.refs.SelectAddressOrDate.hide();
                  }}>
                  <View style={{flex:1}}>

                      <ScrollView>  
                      {/*商品信息*/}
                      <View style={{width:'100%',height:BaseComponent.W*100/375,flexDirection:'row'}}>
                        <View style={{width:BaseComponent.W*79/375,height:'100%',justifyContent:'center',alignItems:'center'}}>
                            <Image style={{width:BaseComponent.W*60/375,height:BaseComponent.W*60/375}} source={{uri:this.state.data.sku.imageAttachment.resourceFullAddress?this.state.data.sku.imageAttachment.resourceFullAddress:""}}></Image>
                        </View>
                        <View style={{width:BaseComponent.W*220/375,height:'100%'}}>
                            <Text style={{fontSize:BaseComponent.W*14/375,color:'#040000',marginTop:BaseComponent.W*15/375}} numberOfLines={1}>{this.state.data.skuName}</Text>
                            <Text style={{color:'#999899',fontSize:BaseComponent.W*12/375,marginTop:BaseComponent.W*11/375}}>{this.state.data.sku.goodsAttributeOptions[0].goodsAttributeOption.value}&nbsp;&nbsp;{this.state.data.sku.goodsAttributeOptions[1]?this.state.data.sku.goodsAttributeOptions[1].goodsAttributeOption.value:""}</Text>
                            <Text style={{color:'#040000',fontSize:BaseComponent.W*14/375,marginTop:BaseComponent.W*17/375}}>实付金额 ¥ {this.state.data.payAmount}</Text>
                        </View>
                        <View style={{width:BaseComponent.W*66/375,height:'100%',alignItems:'flex-end'}}>
                          <Text style={{fontSize:BaseComponent.W*13/375,color:'#393A3A',marginTop:BaseComponent.W*15/375}}>¥{this.state.data.buyUnitAmount}</Text>
                          <Text style={{color:'#666666',fontSize:BaseComponent.W*12/375,marginTop:BaseComponent.W*3/375}}>×{this.state.data.num}</Text>
                        </View>
                      </View>  

                      <View style={{width:'100%',height:BaseComponent.W*10/375,backgroundColor:"#F0F0F0",marginTop:BaseComponent.W*10/375}}></View>
                    
                    

                    {/*退货原因*/}
                    <View style={{width:'100%',height:BaseComponent.W*268/375}}>
                      <Text style={{fontSize:BaseComponent.W*15/375,color:'#2D2D2D',marginTop:BaseComponent.W*17/375,marginLeft:BaseComponent.W*10/375}}>退货原因</Text>
                      <Text style={{fontSize:BaseComponent.W*13/375,color:'#E41436',position:'absolute',left:0,marginLeft:BaseComponent.W*70/375,marginTop:BaseComponent.W*13/375}}>*</Text>
                      {/*选择退货原因*/}
                      <TouchableWithoutFeedback onPress={()=>{
                            thiz.open();
                      }}>
                      <View style={{width:BaseComponent.W*355/375,height:BaseComponent.W*40/375,marginLeft:BaseComponent.W*10/375,backgroundColor:'#F0F0F0',
                                    marginTop:BaseComponent.W*10/375,justifyContent:'center',borderRadius:BaseComponent.W*7/375}}>
                            <Text style={{fontSize:BaseComponent.W*15/375,color:thiz.state.reason.length==0?'#656565':"#121212",marginLeft:BaseComponent.W*13/375}}>{thiz.state.reason.length==0?"请选择退货原因":thiz.state.reason[0]}</Text>          
                      </View>
                      </TouchableWithoutFeedback>
                      <Text style={{fontSize:BaseComponent.W*15/375,color:'#2D2D2D',marginTop:BaseComponent.W*22/375,marginLeft:BaseComponent.W*10/375}}>申请数量</Text>
                      <Text style={{fontSize:BaseComponent.W*13/375,color:'#E41436',position:'absolute',left:0,marginLeft:BaseComponent.W*70/375,marginTop:ti.select({ios:BaseComponent.W*100/375,android:BaseComponent.W*105/375})}}>*</Text>
                      {/*退货数量*/}
                      <View style={{width:BaseComponent.W*110/375,height:BaseComponent.W*32/375,marginLeft:BaseComponent.W*10/375,marginTop:BaseComponent.W*12/375,
                                  borderRadius:BaseComponent.W*3/375,borderColor:'#535353',borderWidth:1,flexDirection:'row'}}>
                            <TouchableWithoutFeedback onPress={()=>{
                              this.minusNum();
                            }}>
                            <View style={{width:BaseComponent.W*32/375,height:'100%',justifyContent:'center',alignItems:'center'}}>
                            <Image style={{width:BaseComponent.W*22/375,height:BaseComponent.W*22/375}} source={require('../../image/home/jian.png')}></Image>
                            </View>
                            </TouchableWithoutFeedback>

                            <View style={{width:0.5,height:'100%',backgroundColor:'#535353'}}></View>
                            <View style={{width:BaseComponent.W*45/375,height:'100%',justifyContent:'center',alignItems:'center'}}>
                              <Text style={{fontSize:BaseComponent.W*18/375,color:'#212121'}}>{this.state.num}</Text>
                            </View>

                            <View style={{width:0.4,height:'100%',backgroundColor:'#535353'}}></View>
                            <TouchableWithoutFeedback onPress={()=>{
                              this.addNum();
                            }}>
                            <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                              <Image style={{width:BaseComponent.W*22/375,height:BaseComponent.W*22/375}} source={require('../../image/home/plus.png')}></Image>
                            </View>
                            </TouchableWithoutFeedback>
                      </View>

                    {/*退款金额*/}
                    <Text style={{fontSize:BaseComponent.W*15/375,color:'#2D2D2D',marginTop:BaseComponent.W*24/375,marginLeft:BaseComponent.W*10/375}}>退款金额</Text>
                    <Text style={{fontSize:BaseComponent.W*13/375,color:'#E41436',position:'absolute',left:0,marginLeft:BaseComponent.W*70/375,marginTop:ti.select({ios:BaseComponent.W*184/375,android:BaseComponent.W*194/375})}}>*</Text>
                    <Text style={{fontSize:BaseComponent.W*12/375,color:'#CACACA',position:'absolute',left:0,marginLeft:BaseComponent.W*90/375,marginTop:ti.select({ios:BaseComponent.W*190/375,android:BaseComponent.W*201/375})}}>最多{this.state.data.maxRefundAmount}元</Text>
                    <Text style={{fontSize:BaseComponent.W*12/375,color:'#1AB2EA',position:'absolute',right:0,marginRight:BaseComponent.W*10/375,marginTop:ti.select({ios:BaseComponent.W*190/375,android:BaseComponent.W*201/375})}} onPress={()=>this.navigate('RefundInstructions',{title:"退款说明"})}>退款说明?</Text>
                    <View style={{width:BaseComponent.W*355/375,height:BaseComponent.W*40/375,backgroundColor:'#F0F0F0',marginLeft:BaseComponent.W*10/375,
                                  marginTop:BaseComponent.W*11/375,justifyContent:'center',borderRadius:BaseComponent.W*7/375}}>
                          <TextInput style={{fontSize:BaseComponent.W*15/375,color:'#121212',marginLeft:BaseComponent.W*13/375,padding:0,width:'100%'}} underlineColorAndroid='transparent' maxLength={10}  keyboardType="numeric" onChangeText={(event)=>this.setState({applyRefundAmount:event})} onFocus={()=>{thiz.refs.SelectAddressOrDate.hide();}}></TextInput>          
                    </View>
                    </View>
                    <View style={{width:'100%',height:BaseComponent.W*10/375,backgroundColor:'#F0F0F0',marginTop:BaseComponent.W*19/375}}></View>

                    {/*问题描述*/}
                    <Text style={{fontSize:BaseComponent.W*15/375,color:'#2D2D2D',marginTop:BaseComponent.W*15/375,marginLeft:BaseComponent.W*10/375}}>问题描述</Text>
                    <View style={{width:BaseComponent.W*355/375,height:BaseComponent.W*105/375,marginTop:BaseComponent.W*10/375,marginLeft:BaseComponent.W*10/375,backgroundColor:'#F0F0F0',borderRadius:BaseComponent.W*5/375}}>
                    <TextInput placeholder="请在此描述详细问题" placeholderTextColor="#F0F0F0" underlineColorAndroid='transparent' multiline={true} style={{marginLeft:BaseComponent.W*12/375,color:'#121212',width:BaseComponent.W*343/375,height:BaseComponent.W*105/375}} maxLength={200} onChangeText={(event)=>this.setState({problemDesc:event})} onFocus={()=>{thiz.refs.SelectAddressOrDate.hide();}}></TextInput>
                    <Text style={{fontSize:BaseComponent.W*12/375,color:'#A5A5A5',position:'absolute',bottom:0,right:0,marginRight:BaseComponent.W*8/375,marginBottom:BaseComponent.W*8/375}}>200</Text>
                    </View>

                    {/*上传图片*/}
                    <View style={{flexDirection:'row',alignItems:'flex-end'}}> 
                    <Text style={{fontSize:BaseComponent.W*15/375,color:'#2D2D2D',marginTop:BaseComponent.W*20/375,marginLeft:BaseComponent.W*10/375}}>上传图片</Text>
                    <Text style={{fontSize:BaseComponent.W*12/375,color:'#CACACA',marginLeft:BaseComponent.W*15/375}}>最多8张</Text>
                    </View>
                    <View style={{width:BaseComponent.W*355/375,height:BaseComponent.W*80/375,marginLeft:BaseComponent.W*10/375,marginTop:BaseComponent.W*10/375,
                                flexDirection:"row",justifyContent:'center',alignItems:'center',backgroundColor:'#F0F0F0',borderRadius:BaseComponent.W*5/375}}>
                          {
                            this.state.picture.map((imgpath,index)=>{
                              this.log("-----------------------------------------imgpath----------------------------",imgpath);
                                return (
                                  <View style={{width:BaseComponent.W*0.1,height:BaseComponent.W*80/375,marginLeft:5}} key={index}>
                                      <Image style={{width:BaseComponent.W*0.1,height:BaseComponent.W*80/375}} source={{uri:imgpath}} resizeMode="stretch"></Image>
                                  </View>
                                )
                            })
                          }
                          <TouchableWithoutFeedback onPress={()=>{this.openAlbum();thiz.refs.SelectAddressOrDate.hide()}}>
                          <View style={{height:BaseComponent.W*80/375,flexDirection:'row',justifyContent:'center',alignItems:'center',display:this.state.picture.length<=7?'flex':'none'}}>
                              <Image style={{width:BaseComponent.W*20/375,height:BaseComponent.W*20/375}} source={require('../../image/home/plus1.png')}/>      
                              <Text style={{fontSize:BaseComponent.W*14/375,color:'#878787',marginLeft:BaseComponent.W*10/375,display:this.state.picture.length<=6?'flex':'none'}}>添加图片</Text>
                          </View>
                          </TouchableWithoutFeedback>  
                    
                    </View>
                    </ScrollView>

                    
                    {/*提交按钮*/}
                    <TouchableWithoutFeedback onPress={()=>{this.summit();}}>
                    <View style={{width:'100%',height:BaseComponent.W*50/375,backgroundColor:this.state.refundReasonId==""||this.state.applyRefundAmount==""||this.state.problemDesc==""||this.state.attachmentIds.length==0?'#CCCCCC':'#FED584',justifyContent:'center',alignItems:'center'}}>
                      <Text style={{fontSize:BaseComponent.W*16/375,color:'#2D2D2D'}}>提  交</Text>
                    </View>
                    </TouchableWithoutFeedback>                  
                  </View>
                  </TouchableWithoutFeedback>)
            }    

           

      </View>
    )
  }
}