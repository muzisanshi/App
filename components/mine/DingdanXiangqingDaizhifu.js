/**
 * @name DingdanXiangqing.js
 * @auhor 程浩
 * @date 2018.8.21
 * @desc 订单详情待支付界面
 */
import React,{Component} from 'react';
import {Text,View,StyleSheet,Image,TouchableWithoutFeedback,ScrollView,Modal,ActivityIndicator,StatusBar} from 'react-native';
// 导入工具函数库
import T from "../../lib/Tools";
// 导入公共样式
import St from "../../lib/Style";
// 导入图片资源库
import Imgs from "../../lib/Img";
import BaseComponent from '../BaseComponent';
// 转菊花
import Spinner from 'react-native-loading-spinner-overlay';
import  SelectAddressOrDate  from './SelectAddressOrDate';
// 支付
import Alipay from 'react-native-payment-alipay';
// import * as Wxpay from 'react-native-wechat'
import Wxpay from '@yyyyu/react-native-wechat';

// Tools实例
let ti = T.getInstance();

let style = StyleSheet.create({
  wrapper:{
    backgroundColor:"#F0F0F0",
    flex:1,
    flexDirection:"column",
    alignItems:"stretch",
    justifyContent:"flex-start",
    position:"relative",
  },
  statusBar:{
    backgroundColor:"rgb(255,255,255)",
    height:ti.select({ios:16,android:22}),
    position:"relative",
    zIndex:1,
    opacity:1,
  },
  navBar:{
    backgroundColor:"rgb(255,255,255)",
    height:"7%",
    position:"relative",
    zIndex:1,
    opacity:1,
    borderBottomWidth:1,
    borderBottomColor:"rgb(241,241,241)"
  },
});

export default class DingdanXiangqingDaizhifu extends BaseComponent{
  constructor(props){
    super(props);
    this.state={
      visible:false,//转菊花
      GoodsData:{orderPackages:[],orderDetail:{receiverPhoneNo:""},channelOrder:{payMode:{payChannel:{name:""}}}},//订单数据
      businessOrderNo:"",//订单编号
      payvisible:false,//弹出支付框
      paytype:"wxPay",//默认是微信支付，ali表示支付宝支付
      isvisible:false,//true表示弹出放弃选择框.
      reason:[],//取消原因
      deletevisible:false,//提醒确认删除该订单
      netError:false,//true表示网络错误

      // 支付限时
      limitTime:"请在0小时0分内完成支付",

      payMode:null,

    }

    // 解决快速点击确认支付的问题
    this.canEnsure = true;

  }

  getOrderInfo(showFlower){
    var thiz=this;
    var businessOrderNo=thiz.params.businessOrderNo;
    console.log("----------------------businessOrderNo------------------------------",businessOrderNo);
    thiz.setState({visible:showFlower})
    thiz.request("order/getOrderInfo",{
      method:"POST",
      data:{businessOrderNo:businessOrderNo},
      success:function(ret){
          if(ret.respCode=="00")
          {
            thiz.log("-----------------------------------DingdanXiangqing_ret--------------------------------",ret.data);
            thiz.setState({
              GoodsData:ret.data,
              businessOrderNo:businessOrderNo,
              visible:false,

              limitTime:"请在"+ret.data.surplusPayHours+"小时"+ret.data.surplusPayMinutes+"分内完成支付",
            })
          }
      },
      error:function(err){
          thiz.setState({visible:false});
           if(err.code&&err.code==200){
            console.log("11111111111111111111111111111111");
            thiz.setState({netError:true});
          }  
      }
    });
  }

  componentDidMount(){
    let thiz = this;
    thiz.getOrderInfo(true);
    thiz.listen("cancel_order_success",function(){
      thiz.goBack();
    })
  };
  //隐藏部分电话号码
  PlacePhone=()=>{
    var phone=this.state.GoodsData.orderDetail.receiverPhoneNo;
    var num;
    if(phone.length=11)
    {
      num=phone.toString().replace(/^(\d{3})\d{4}(\d{4})$/, '$1****$2');
    }else
    {
       num=phone; 
    }
    return num;
};
//订单左边按钮
leftBtnClick=()=>{
  var thiz=this;
    if(thiz.state.GoodsData.orderStatus=="WAIT_FOR_PAY")
    {
      //取消订单
      thiz.open();
    
      thiz.emit("cancel_order",{businessOrderNo:thiz.state.businessOrderNo});

    };
    if(thiz.state.GoodsData.orderStatus=="CANCELED")
    { 
      //删除订单
      thiz.setState({deletevisible:true});
    };
};
//订单右边按钮
rightBtnClick=()=>{
  var thiz=this;
    if(thiz.state.GoodsData.orderStatus=="WAIT_FOR_PAY")
    {
        //立即支付
        thiz.commit();
    };
    if(thiz.state.GoodsData.orderStatus=="CANCELED")
    {
        //再次购买
        thiz.request("order/buyAgain",{
            method:"POST",
            data:{businessOrderNo:thiz.state.businessOrderNo},
            success:function(ret){
                thiz.emit("click_buyAgain");
                BaseComponent.CUR_TAB = "Cart";
                thiz.navigate("Cart");
            },
            error:function(error){
                
            }
        }); 
    };
};

 callBackAddressValue(value){
        this.setState({
            reason:value
        })
};
open(){
      this.refs.SelectAddressOrDate.showAddress(this.state.reason)
};

// 微信支付接口
    wxpay(info){
      let thiz = this;

      thiz.setState({
        payvisible:false,
      });

      let data = info;

      // 判断微信是否安装
      let callback = async function(isInstalled){
        if(!isInstalled){
          thiz.toast("请先安装微信");
          return;
        }
        let payP = {

          appId:"wx928c9e134253166e",
          partnerId:data.clientInvokePayData.partnerid,
          prepayId:data.clientInvokePayData.prepayid,
          nonceStr:data.clientInvokePayData.noncestr,
          timestamp:data.clientInvokePayData.timestamp,
          packageSign:data.clientInvokePayData.package,
          sign:data.clientInvokePayData.sign

        };
        thiz.log("--------payP--------",payP);
        Wxpay.pay(payP).then(function(ret){
          thiz.log("--------wxpay_ret--------",ret);

          if(ret&&ret.errCode==0){
            // 通知后端支付成功
            thiz.setState({
              payvisible:false,
            });
            thiz.setState({
              visible:true
            });
            thiz.request("order/paySuccessNotify",{
                method:"POST",
                  data:{
                    channelOrderNo:info.channelOrderNo
                  },
                  success:function(ret){
                      thiz.setState({
                              visible:false,
                        });
                        if(ret.respCode=="00"){
                          thiz.log("--------wxpay_channelOrderNo--------",info.channelOrderNo);
                          thiz.navigate('FinishPay',{
                            title:'支付完成',
                            totalMoney:thiz.state.GoodsData.payAmount,
                            orderNo:thiz.params.businessOrderNo,
                            from:"DingdanXiangqingDaizhifu",
                          });
                          thiz.setState({
                            payvisible:false,
                          });
                          
                          setTimeout(function(){
                            thiz.emit("pay_order_success");
                          },1000);

                        }
                  },
                  error:function(err){
                        thiz.toast(err.msg);
                        thiz.setState({
                              visible:false,
                        });
                  }
            });

          }else{
            thiz.toast("微信支付失败");
            thiz.setState({
                visible:true
            });
              // 避免模块的异常和ios上从左上角点击返回导致的app无法判断是否支付成功
              thiz.request("order/queryPayResult",{
                method:"POST",
                  data:{
                    channelOrderNo:info.channelOrderNo
                  },
                  success:function(ret){
                      thiz.setState({
                            visible:false,
                      });
                      thiz.log("--------order/queryPayResult--------",ret);
                        if(ret.respCode=="00"&&ret.data.channelOrderStatus=="SUCCESS"){

                          // 通知后端支付完成
                          thiz.navigate('FinishPay',{
                            title:'支付完成',
                            totalMoney:thiz.state.GoodsData.payAmount,
                            orderNo:thiz.params.businessOrderNo,
                            from:"DingdanXiangqingDaizhifu",
                          });
                          thiz.setState({
                            payvisible:false,
                          });

                    return;
                        }
                        
                    //     if(ret.respCode=="00"&&ret.data.channelOrderStatus=="PROCESSING"){
                    //       // 支付失败
                    //       thiz.toast("支付处理中");
                    // thiz.setState({
                    //             visible:false,
                    //       });
                    //       return;
                    //     }
                    //     if(ret.respCode=="00"&&ret.data.channelOrderStatus=="CLOSE"){
                    //       // 支付失败
                    //       thiz.toast("支付关闭");
                    // thiz.setState({
                    //             visible:false,
                    //       });
                    //       return;
                    //     }
                    //     if(ret.respCode=="00"&&ret.data.channelOrderStatus=="FAIL"){
                    //       // 支付失败
                    //       thiz.toast("支付失败");
                    // thiz.setState({
                    //             visible:false,
                    //       });
                    //       return;
                    //     }

                        // 其他情况
                        // thiz.toast("支付失败");
                    thiz.navigate('Payfailure',{title:'支付结果',from:'DingdanXiangqingDaizhifu',businessOrderNo:thiz.params.businessOrderNo,limitTime:thiz.state.limitTime});
                  },
                  error:function(err){
                      // thiz.toast("支付失败");
                      thiz.setState({
                            visible:false,
                      });
                      thiz.navigate('Payfailure',{title:'支付结果',from:'DingdanXiangqingDaizhifu',businessOrderNo:thiz.params.businessOrderNo,limitTime:thiz.state.limitTime});
                  }
            });

          }
          


          /**
           * 老的支付代码
           **/
          // if(ret&&ret.errCode==0){
          //   // 通知后端支付成功
          //   thiz.setState({
          //       visible:true
          //   });
          //   thiz.request("order/paySuccessNotify",{
          //       method:"POST",
          //         data:{
          //           channelOrderNo:info.channelOrderNo
          //         },
          //         success:function(ret){
          //             thiz.setState({
          //                     visible:false,
          //               });
          //               if(ret.respCode=="00"){
          //                 thiz.log("--------wxpay_channelOrderNo--------",info.channelOrderNo);
          //                 thiz.navigate('FinishPay',{
          //                   title:'支付完成',
          //                   totalMoney:thiz.state.GoodsData.payAmount,
          //                   orderNo:thiz.params.businessOrderNo,
          //                   from:"DingdanXiangqingDaizhifu",
          //                 });
          //                 thiz.setState({
          //                   payvisible:false,
          //                 });
                          
          //                 setTimeout(function(){
          //                   thiz.emit("pay_order_success");
          //                 },1000)

          //               }
          //         },
          //         error:function(err){
          //               thiz.toast(err.msg);
          //               thiz.setState({
          //                     visible:false,
          //               });
          //         }
          //   });
          // }else{
          //   thiz.toast("支付失败");
          //   thiz.setState({
          //         visible:false,
          //   });
          // }


        }).catch(function(err){
          thiz.log("--------wxpay_err--------",err);
          if(err&&err.errCode == -2){
            thiz.log("--------wxpay_err--------","微信支付取消");
          }
          // thiz.toast("支付失败");
          thiz.setState({
                visible:false,
          });
          thiz.navigate('Payfailure',{title:'支付结果',from:'DingdanXiangqingDaizhifu',businessOrderNo:thiz.params.businessOrderNo,limitTime:thiz.state.limitTime});
        });
        
      };
      Wxpay.isWXAppInstalled().then(callback);
    }

    // 测试支付接口
  alipay(info){
      let thiz = this;

      thiz.setState({
        payvisible:false,
      });

      // 调支付接口
      thiz.log("--------alipay_info--------",info);
      Alipay.pay(info.clientInvokePayData).then(function(data){
          thiz.log("--------pay_success--------",data);
          

        if(BaseComponent.OS=="android"){
          if(data && typeof(data) == "string"){
            let result = data.split(";")[2].split("result=")[1];
            // 去除前后的花括号（二逼的支付宝）
            result = result.substring(1,result.length-1);
            result = JSON.parse(result);
            status = result.alipay_trade_app_pay_response.code;
          }
        }else{
          if(data&&data.length>0){
            let result = data[0].result;
            result = JSON.parse(result);
            status = result.alipay_trade_app_pay_response.code;
          }
        }
        thiz.log("--------status--------",status);
          
          // 判断支付状态
          if(status>=9000){
            // 通知后端支付成功
            thiz.setState({
                visible:true
            });
            thiz.request("order/paySuccessNotify",{
                  method:"POST",
                  data:{
                    channelOrderNo:info.channelOrderNo
                  },
                  success:function(ret){
                      thiz.setState({
                              visible:false,
                        });
                        if(ret.respCode=="00"){
                          thiz.navigate('FinishPay',{
                            title:'支付完成',
                            totalMoney:thiz.state.GoodsData.payAmount,
                            orderNo:thiz.params.businessOrderNo,
                            from:"DingdanXiangqingDaizhifu",
                          });
                          thiz.setState({
                            payvisible:false,
                          });
                          setTimeout(function(){
                            thiz.emit("pay_order_success");
                          },1000)
                          
                        }
                  },
                  error:function(err){
                        thiz.toast(err.msg);
                        thiz.setState({
                              visible:false,
                        });
                  }
            });
          }else{
            // thiz.toast("支付失败");
            thiz.setState({
                  visible:false,
            });
            thiz.navigate('Payfailure',{title:'支付结果',from:'DingdanXiangqingDaizhifu',businessOrderNo:thiz.params.businessOrderNo,limitTime:thiz.state.limitTime});
          }

      }).catch(function (err) {
          thiz.log("--------pay_err--------",err);
          // thiz.toast("支付失败");
          thiz.setState({
                visible:false,
          });
          thiz.navigate('Payfailure',{title:'支付结果',from:'DingdanXiangqingDaizhifu',businessOrderNo:thiz.params.businessOrderNo,limitTime:thiz.state.limitTime});
      });
  }

    // 提交订单
    commit=()=>{
      let thiz = this;
        thiz.setState({
          visible:true,
        });
        // 获取支付模式
        thiz.request("payChannel/getPayMode",{
          method:"POST",
          data:{},
          success:function(ret){
            if(ret.respCode=="00"){
              thiz.setState({
                    visible:false,
              });
              // 备份支付模式
              thiz.payMode = ret.data;

              if(ret.data.length==1){
                thiz.setState({
                  paytype:ret.data[0].payChannel.type,
                });
              }

              setTimeout(function(){
                thiz.setState({
                    payvisible:true,
                    payMode:ret.data,
                });
              },500);

            }
          },
          error:function(err){
            thiz.toast(err.msg);
            thiz.setState({
                  visible:false,
            });
          }   
        });               
      };
    //确认支付
    ensurepay=()=>{
      var thiz=this;

      if(thiz.canEnsure){

        thiz.canEnsure = false;

        // 定时关闭支付弹窗，并允许点击
        setTimeout(function(){
          thiz.canEnsure = true;
          thiz.setState({
            payvisible:false,
          });
        },200);

        // 接着调支付接口（仅仅测试用）
        // let payChannelType="";
        // let payModeType = "";
        // if(thiz.state.paytype=="wx"){
        //   payChannelType = thiz.payMode[0].payChannel.type;
        //   payModeType = thiz.payMode[0].type; 
        // }
        // if(thiz.state.paytype=="ali"){
        //   payChannelType = thiz.payMode[1].payChannel.type;
        //   payModeType = thiz.payMode[1].type;
        // }
        // if(thiz.state.paytype=="bank"){
        //   return;
        // }

        // 接着调支付接口（仅仅测试用）
        let payChannelType=thiz.state.paytype;
        let payModeType = thiz.state.payMode&&thiz.state.payMode.length>0?thiz.state.payMode[0].type:"";

        if(!payChannelType){
          return;
        }

        // 接着调支付接口（仅仅测试用）
        thiz.request("order/pay",{
              method:"POST",
              data:{
                businessOrderNo:thiz.state.businessOrderNo,
                payParams:{
                  payChannelType:payChannelType,
                  payModeType:payModeType
                }
              },
              success:function(ret){
                  thiz.setState({
                          visible:false,
                    });
                if(ret.respCode=="00"){
                      
                      // // 备份数据
                      // thiz.payData = ret.data;

                // 发送下单成功消息，刷新购物车界面
                thiz.emit("submit_order_success");
                
                if(ret.data){

                  // 隐藏支付方式
                  thiz.setState({
                      payvisible:false,
                  });

                  // 接着调支付接口（仅仅测试用）
                    if(thiz.state.paytype=="wxPay")
                    {
                      thiz.wxpay(ret.data);
                    }
                    if(thiz.state.paytype=="aliPay"){
                      thiz.alipay(ret.data);
                    }
                }
              }
              },
              error:function(err){
                    thiz.toast(err.msg);
                    thiz.setState({
                          visible:false,
                    });
              }
        });
      }
    };
    render() {
      let thiz=this;
        return ( 
             <View style={style.wrapper}>
                {/*状态栏占位*/}
                <StatusBar translucent={true} backgroundColor={this.state.visible||this.state.payvisible||this.state.deletevisible||this.state.isvisible?"rgba(14,14,14,0.5)":"rgb(255,255,255)"} barStyle={'dark-content'}/>
                <View style={{backgroundColor:this.state.visible||this.state.payvisible||this.state.deletevisible||this.state.isvisible?"rgba(14,14,14,0.5)":"rgb(255,255,255)",
                              height:thiz.isIphoneX(30,BaseComponent.SH),
                              position:"relative",
                              zIndex:1,
                              opacity:1}}></View>
                {/*顶部导航栏*/}
                <View style={style.navBar}>
                  <View style={{flex:1,flexDirection:"row",justifyContent:"flex-start"}}>

                    <TouchableWithoutFeedback onPress={()=>this.goBack()}>
                    <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>
                      <Image source={Imgs.back} style={{width:(BaseComponent.W*0.127*0.4),height:(BaseComponent.W*0.127*0.4),marginRight:BaseComponent.W*0.04}}></Image>
                    </View>
                    </TouchableWithoutFeedback>

                    <View style={{flexGrow:1,backgroundColor:"transparent",
                      marginTop:ti.select({ios:(BaseComponent.H*0.02)/2,android:(BaseComponent.H*0.016)/2}),borderRadius:ti.select({ios:(BaseComponent.H*0.05)/2,android:(BaseComponent.H*0.054)/2}),borderColor:"#0A0A0A",
                      borderWidth:0,}}>
                      <Text style={{color:"#0A0A0A",width:"100%",textAlign:"center",marginTop:ti.select({ios:"4.5%",android:"1.6%"}),fontSize:17}}>{this.params.title}</Text>
                    </View>
                    <TouchableWithoutFeedback onPress={()=>{
                      // 跳转客服
                      thiz.goPage("Service",{title:"客服"});
                    }}>
                    <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>
                      <Image source={require('../../image/home/kefu.png')} style={{width:(BaseComponent.W*0.127*0.5),height:(BaseComponent.W*0.127*0.5),display:"flex"}}></Image>
                    </View>
                    </TouchableWithoutFeedback>

                  </View>
                </View>
                

                {/*转菊花*/}
                <Modal
                  visible={this.state.visible}
                  onRequestClose={()=>{this.setState({visible:false})}}
                  transparent={true}
                  animationType={"fade"}>
                  <View style={{backgroundColor:"rgba(14,14,14,0.5)",width:"100%",width:"100%",flex:1,flexDirection:"column",alignItems:"center",justifyContent:"center",position:"relative"}}>
                    <View>
                          <ActivityIndicator size="large" color='white'/>
                    </View>
                  </View>
                </Modal>

                {/*弹出放弃支付框*/}
                <Modal visible={this.state.isvisible}
                       transparent={true}
                       onRequestClose={() => {this.setState({isvisible:false})}}>
                    <View style={{flex:1,backgroundColor:'rgba(14,14,14,0.5)'}}>
                        <View style={{width:BaseComponent.W*275/375,height:BaseComponent.W*151/375,backgroundColor:'white',
                                    marginTop:BaseComponent.W*80/375,marginLeft:BaseComponent.W*52/375,borderRadius:BaseComponent.W*10/375}}>
                        <View style={{width:'100%',height:BaseComponent.W*25/375,marginTop:BaseComponent.W*10/375,alignItems:'center'}}>
                            <Text style={{fontSize:BaseComponent.W*18/375,color:'#222222'}}>确认放弃支付吗？</Text>
                        </View>
                        <View style={{width:BaseComponent.W*228/375,height:BaseComponent.W*38/375,marginLeft:BaseComponent.W*24/375,marginTop:BaseComponent.W*15/375}}>
                            <Text style={{fontSize:BaseComponent.W*14/375,color:'#666666',textAlign:'center',lineHeight:BaseComponent.W*25/375}}>超过订单支付时效后,订单将被取消,请尽快完成支付。</Text>
                        </View>      
                        <View style={{width:'100%',height:0.5,backgroundColor:'#BDBDBD',marginTop:BaseComponent.W*20/375}}></View>      
                        <View style={{flex:1,flexDirection:"row"}}>
                            <TouchableWithoutFeedback onPress={()=>{
                              this.setState({isvisible:false})
                            }}>
                            <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                                <Text style={{fontSize:BaseComponent.W*15/375,color:'#007AFF'}}>继续支付</Text>
                            </View>
                            </TouchableWithoutFeedback>

                            <View style={{width:0.5,height:'100%',backgroundColor:'#BDBDBD'}}></View>
                            <TouchableWithoutFeedback onPress={()=>{
                              this.setState({
                                isvisible:false,
                                payvisible:false
                              })
                            }}>
                            <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                                <Text style={{fontSize:BaseComponent.W*15/375,color:'#5E5E5E'}}>放弃</Text>
                            </View>
                            </TouchableWithoutFeedback>
                        </View>
                        </View>

                    </View>   
                </Modal>

                {/*弹出支付框*/}
                <Modal visible={this.state.payvisible}
                     transparent={true}
                      onRequestClose={() => {this.setState({payvisible:false})}}>
                    {/*选择支付方式*/}
                    <TouchableWithoutFeedback onPress={()=>this.setState({payvisible:false})}>  
                      <View style={{flex:1,backgroundColor:'rgba(14,14,14,0.5)'}}>
                    <View style={{width:'100%',height:BaseComponent.W*300/375,backgroundColor:'#fff',position:'absolute',bottom:0}}>
                          <View style={{width:'100%',height:BaseComponent.W*43/375,flexDirection:'row',alignItems:'center'}}>
                              <Text style={{fontSize:BaseComponent.W*15/375,color:'#0E0E0E',marginLeft:BaseComponent.W*139/375}}>请选择支付方式</Text>
                              <TouchableWithoutFeedback onPress={()=>this.setState({payvisible:false})}>
                              <View style={{width:BaseComponent.W*30/375,height:BaseComponent.W*43/375,marginLeft:BaseComponent.W*100/375,justifyContent:'center'}}>
                              <Image style={{width:BaseComponent.W*18/375,height:BaseComponent.W*18/375}} source={require('../../image/home/close.png')}></Image>
                              </View>
                              </TouchableWithoutFeedback>
                          </View>
                          <View style={{width:'100%',height:0.5,backgroundColor:'#C8C7CC'}}></View>
                          <View style={{width:'100%',height:BaseComponent.W*60/375,flexDirection:'row',alignItems:'center'}}>
                              <Text style={{width:"100%",textAlign:"center",fontSize:BaseComponent.W*15/375,color:'#0E0E0E',marginLeft:BaseComponent.W*10/375}}>{thiz.state.limitTime}&nbsp;&nbsp;金额&nbsp;&nbsp;<Text style={{fontSize:BaseComponent.W*15/375,color:'#E31436'}}>¥{this.state.GoodsData.payAmount}</Text>&nbsp;&nbsp;元</Text>
                          </View>

                        {/*开始选择支付方式*/}
                          <View style={{width:'100%',height:BaseComponent.W*51/375,flexDirection:'row',marginTop:BaseComponent.W*10/375,alignItems:'center',display:'none'}}>
                              <View style={{width:BaseComponent.W*29/375,height:BaseComponent.W*25/375,backgroundColor:'red',marginLeft:BaseComponent.W*10/375,display:'none'}}></View>
                              <Text style={{fontSize:BaseComponent.W*15/375,color:'#0E0E0E',marginLeft:BaseComponent.W*20/375,display:'none'}}>银行卡快捷支付</Text>
                          </View>
                          <View style={{width:'100%',height:0.5,backgroundColor:'#C8C7CC'}}></View>
                          
                        {
                          thiz.state.payMode&&thiz.state.payMode.length>0?thiz.state.payMode.map(function(pv,pi){
                            return (
                              <View>
                                <TouchableWithoutFeedback onPress={()=>{thiz.setState({paytype:pv.payChannel.type})}}>
                                <View style={{width:'100%',height:BaseComponent.W*51/375,flexDirection:'row',alignItems:'center'}}>
                                      <Image style={{width:BaseComponent.W*29/375,height:BaseComponent.W*25/375,marginLeft:BaseComponent.W*10/375}} source={(function(){
                                        if(pv.payChannel.type=="aliPay"){
                                          return require('../../image/home/alipay.png');
                                        }
                                        if(pv.payChannel.type=="wxPay"){
                                          return require('../../image/home/wxpay.png');
                                        }
                                      })()}></Image>
                                      <Text style={{fontSize:BaseComponent.W*15/375,color:'#0E0E0E',marginLeft:BaseComponent.W*20/375,width:BaseComponent.W*255/375}}>
                                        {
                                          (function(){
                                            if(pv.payChannel.type=="wxPay"){
                                              return "微信新"
                                            }
                                            if(pv.payChannel.type=="aliPay"){
                                              return "支付宝"
                                            }
                                          })()
                                        }
                                      </Text>
                                      <Image style={{width:BaseComponent.W*21/375,height:BaseComponent.W*21/375,marginLeft:(function(){

                                        let marginLeft = 0;
                                        return marginLeft;

                                      })(),display:(function(){

                                        let display = 'none';
                                        if(pv.payChannel.type==thiz.state.paytype){
                                          display = "flex";
                                        }
                                        return display;

                                      })()}} source={require('../../image/home/xuanzhong.png')}></Image>
                                </View>
                                </TouchableWithoutFeedback>
                                <View style={{width:'100%',height:0.5,backgroundColor:'#C8C7CC'}}></View>
                              </View>
                            )
                          }):null
                        }

                        {/*确认支付*/}
                        <TouchableWithoutFeedback onPress={()=>{this.ensurepay();}}> 
                        <View style={{width:'100%',height:BaseComponent.W*49/375,backgroundColor:'#FDD17A',position:'absolute',bottom:0,justifyContent:'center',alignItems:'center'}}>
                            <Text style={{color:'#0E0E0E',fontSize:BaseComponent.W*18/375}}>确认支付</Text>
                        </View>
                        </TouchableWithoutFeedback>
                        
                        </View>
                      </View>
                      </TouchableWithoutFeedback>
                </Modal>

                 {/*提醒确认删除订单*/}
                <Modal visible={this.state.deletevisible}
                       onRequestClose={()=>{this.setState({deletevisible:false})}}
                       transparent={true}>
                       <View style={{flex:1,backgroundColor:'rgba(14,14,14,0.5)',justifyContent:'center',alignItems:'center'}}>
                          <View style={{width:BaseComponent.W*275/375,height:BaseComponent.W*105/375,backgroundColor:'rgba(255, 255, 255,1)',borderRadius:BaseComponent.W*20/375}}>
                              <View style={{width:'100%',height:BaseComponent.W*56/375,justifyContent:'center',alignItems:'center'}}>
                                <Text style={{fontSize:BaseComponent.W*15/375,color:'rgba(94, 94, 94, 1)'}}>确定要删除该订单吗?</Text>
                              </View>
                              <View style={{width:'100%',height:0.5,backgroundColor:'rgba(189, 189, 189, 1)'}}></View>
                              <View style={{width:'100%',height:BaseComponent.W*49/375,flexDirection:'row'}}>
                              <TouchableWithoutFeedback onPress={()=>this.setState({deletevisible:false})}> 
                                <View style={{width:'50%',height:BaseComponent.W*49/375,justifyContent:'center',alignItems:'center'}}>
                                  <Text style={{fontSize:BaseComponent.W*15/375,color:'rgba(94, 94, 94, 1)'}}>取消</Text>
                                </View>
                              </TouchableWithoutFeedback> 
                                <View style={{width:0.5,height:'100%',backgroundColor:'rgba(189, 189, 189, 1)'}}></View>
                                <TouchableWithoutFeedback onPress={()=>{
                                      // 订单确认删除
                                      thiz.request("order/delete",{
                                        method:"POST",
                                        data:{businessOrderNo:thiz.state.businessOrderNo},
                                        success:function(ret){
                                            console.log("-----------------------删除订单成功----------------------");
                                            thiz.setState({deletevisible:false});
                                            thiz.emit('delete_order_success');
                                            thiz.goBack();
                                        },
                                        error:function(err){
                                          console.log("-----------------------删除订单成功----------------------");
                                        }
                                      })
                                }}> 
                                <View style={{width:'50%',height:BaseComponent.W*49/375,justifyContent:'center',alignItems:'center'}}>
                                  <Text style={{fontSize:BaseComponent.W*15/375,color:'#FED584'}}>确认删除</Text>
                                </View>
                                </TouchableWithoutFeedback>
                              </View>
                          </View>
                       </View>
                </Modal>
            
            {
              thiz.state.netError?(
                  <View style={{width:BaseComponent.W,height:BaseComponent.H*0.85,justifyContent:'center',alignItems:'center'}}>
                    <Image style={{width:BaseComponent.W*180/375,height:BaseComponent.W*180/375,borderRadius:BaseComponent.W*90/375}} source={require('../../image/mine/neterror.png')}></Image>
                    <TouchableWithoutFeedback onPress={()=>{
                      thiz.setState({netError:false});
                      thiz.getOrderInfo(true);
                    }}>
                    <View style={{width:BaseComponent.W*70/375,height:BaseComponent.W*30/375,borderRadius:BaseComponent.W*15/375,borderWidth:0.5,borderColor:'#080808',
                              justifyContent:'center',alignItems:'center',marginTop:BaseComponent.W*15/375}}>
                          <Text style={{fontSize:BaseComponent.W*15/375,color:'#252525'}}>刷新</Text>          
                    </View>
                    </TouchableWithoutFeedback>
                  </View>):(
                      <View style={{flex:1}}>
                    <ScrollView style={{flex:1}} showsVerticalScrollIndicator={false}> 
                    <SelectAddressOrDate ref={'SelectAddressOrDate'} callBackAddressValue={this.callBackAddressValue.bind(this)}/>
                     {/*收货人地址*/}
                     <View style={styles.Address}>
                          <Image style={styles.AddressImg} source={require('../../image/home/mine_dizhiguanli.png')}></Image>
                          <Text style={{fontSize:BaseComponent.W*0.037,marginLeft:BaseComponent.W*0.093,color:'#121212',marginTop:BaseComponent.W*0.056}}>{this.state.GoodsData.orderDetail.receiverName}&nbsp;&nbsp;<Text>{this.PlacePhone()}</Text></Text>
                          <Text style={{fontSize:BaseComponent.W*0.037,marginLeft:BaseComponent.W*0.093,marginTop:BaseComponent.W*0.015,color:'#6B6B6B'}} numberOfLines={1}>{this.state.GoodsData.orderDetail.fullAddress}</Text>
                     </View>
                     
                      {
                      this.state.GoodsData.orderPackages.map((item,index)=>{
                          
                          return (
                              <View key={index}>
                                <View style={{width:'100%',height:BaseComponent.W*0.027,backgroundColor:'#F0F0F0'}}></View>
                                <View style={{width:'100%',height:BaseComponent.W*31/375,flexDirection:'row',alignItems:'center',justifyContent:'space-between',backgroundColor:'white'}}>
                                    <Text style={{fontSize:BaseComponent.W*14/375,color:'#2F2F2F',marginLeft:BaseComponent.W*10/375}}>{item.name}</Text>
                                    <Text style={{fontSize:BaseComponent.W*14/375,color:'#FF407E',marginRight:BaseComponent.W*10/375}}>
                                        {(()=>{
                                          if(this.state.GoodsData.orderStatus=="WAIT_FOR_PAY"){return ""};
                                          if(this.state.GoodsData.orderStatus=="CANCELED"){return ""};
                                        })()} 
                                    </Text>
                                </View>
                                {
                                  item.orderInfoItems.map((item1,index1)=>{
                                      this.log("-------------------------item1-------------------------",item1);
                                    return (
                                      <View key={index1} style={{backgroundColor:'white'}}>
                                          <View style={{width:BaseComponent.W*355/375,height:0.5,backgroundColor:'#DDDDDD',marginLeft:BaseComponent.W*10/375}}></View>
                                              <View style={styles.Item}>
                                                  <View style={styles.ItemImg}>
                                                      <Image style={{width:BaseComponent.W*0.16,height:BaseComponent.W*0.16}} source={{uri:item1.sku.imageAttachment.resourceFullAddress}}></Image>
                                                  </View>
                                                  <View style={styles.ItemIns}>
                                                      <Text style={{fontSize:BaseComponent.W*0.035,color:'#040000'}} numberOfLines={2}>{item1.skuName}</Text>
                                                      <Text style={{fontSize:BaseComponent.W*0.032,color:'#999899'}}>{item1.sku.goodsAttributeOptions[0].goodsAttributeOption.value}&nbsp;&nbsp;{item1.sku.goodsAttributeOptions[1]?item1.sku.goodsAttributeOptions[1].goodsAttributeOption.value:""}</Text>
                                                  </View>   
                                                  <View style={{width:BaseComponent.W*80/375,height:BaseComponent.W*60/375,flexDirection:'column',alignItems:'flex-end',marginLeft:BaseComponent.W*20/375}}>
                                                        <Text style={{color:'#393A3A',fontSize:BaseComponent.W*0.035}}>¥{item1.buyUnitAmount}</Text>
                                                        <Text style={{fontSize:BaseComponent.W*0.032,color:'#666666'}}>×{item1.num}</Text>
                                                  </View>
                                              </View>
                                              <View style={{width:'100%',height:0.5,backgroundColor:'#F0F0F0'}}></View> 
                                      </View>
                                    )
                                  })
                                }
                              </View>    
                            )
                          })
                        } 
                    
                    <View style={{width:'100%',height:BaseComponent.W*0.027,backgroundColor:'#F0F0F0'}}></View>

                    {/*费用明细*/}
                    <View style={styles.CostBreakdown}>
                        <View style={{width:'100%',height:BaseComponent.W*0.112,flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
                          <Text style={{fontSize:BaseComponent.W*0.037,color:"#121212",marginLeft:BaseComponent.W*0.027}}>应付总额</Text>
                          <Text style={{fontSize:BaseComponent.W*0.037,color:"#FF407E",marginRight:BaseComponent.W*0.029}}>¥{this.state.GoodsData.payAmount}</Text>
                        </View>
                        <View style={{width:'100%',height:0.5,backgroundColor:'#DDDDDD',marginLeft:BaseComponent.W*0.027}}></View>
                        <View style={styles.CostBreakdownTongyong}>
                            <Text style={{fontSize:BaseComponent.W*0.032,color:'#6B6B6B',marginLeft:BaseComponent.W*0.027}}>商品总额</Text>
                            <Text style={{fontSize:BaseComponent.W*0.037,color:"#121212",marginRight:BaseComponent.W*0.029}}>¥{this.state.GoodsData.goodsAmount}</Text>
                        </View>
                        <View style={{width:'100%',height:0.5,backgroundColor:'#DDDDDD',marginLeft:BaseComponent.W*0.027}}></View>
                        <View style={styles.CostBreakdownTongyong}>
                            <Text style={{fontSize:BaseComponent.W*0.032,color:'#6B6B6B',marginLeft:BaseComponent.W*0.027}}>税费</Text>
                            <Text style={{fontSize:BaseComponent.W*0.037,color:"#121212",marginRight:BaseComponent.W*0.029}}>¥{this.state.GoodsData.taxAmount}</Text>
                        </View>
                        <View style={{width:'100%',height:0.5,backgroundColor:'#DDDDDD',marginLeft:BaseComponent.W*0.027}}></View>
                        <View style={styles.CostBreakdownTongyong}>
                            <Text style={{fontSize:BaseComponent.W*0.032,color:'#6B6B6B',marginLeft:BaseComponent.W*0.027}}>服务</Text>
                            <Text style={{fontSize:BaseComponent.W*0.037,color:"#121212",marginRight:BaseComponent.W*0.029}}>¥{this.state.GoodsData.specialServiceAmount}</Text>
                        </View>
                        <View style={{width:'100%',height:0.5,backgroundColor:'#DDDDDD',marginLeft:BaseComponent.W*0.027}}></View>
                         <View style={styles.CostBreakdownTongyong}>
                            <Text style={{fontSize:BaseComponent.W*0.032,color:'#6B6B6B',marginLeft:BaseComponent.W*0.027}}>邮费</Text>
                            <Text style={{fontSize:BaseComponent.W*0.037,color:"#121212",marginRight:BaseComponent.W*0.029}}>¥{this.state.GoodsData.logisticAmount}</Text>
                        </View>
                        
                    </View>
                        <View style={{width:'100%',height:BaseComponent.W*10/375,backgroundColor:'#F0F0F0'}}></View>
                  {/*下单时间和编号*/}
                    <View style={{width:'100%',height:BaseComponent.W*0.173,flexDirection:'column',backgroundColor:'white'}}>
                        <Text style={{marginLeft:BaseComponent.W*0.0293,color:'#121212',fontSize:BaseComponent.W*0.0346,marginTop:BaseComponent.W*10/375}}>订单编号:&nbsp;&nbsp;&nbsp;{this.state.businessOrderNo}</Text>
                        <Text style={{marginLeft:BaseComponent.W*0.0293,color:'#121212',fontSize:BaseComponent.W*0.0346,marginTop:BaseComponent.W*10/375}}>下单时间:&nbsp;&nbsp;&nbsp;{this.state.GoodsData.orderDatetime}</Text>
                    </View>

                    

                    </ScrollView>  

                  {/*支付区域*/}
                  <View style={{width:'100%',height:BaseComponent.W*0.133,flexDirection:'row',justifyContent:'space-between',backgroundColor:"rgb(255,255,255)",borderTopWidth:0.5,borderTopColor:'#F0F0F0'}}>
                      <View style={{width:BaseComponent.W*0.272,height:BaseComponent.W*0.133,
                                    marginLeft:BaseComponent.W*0.032,flexDirection:'row',
                                    alignItems:'center',}}>
                            <Text style={{fontSize:BaseComponent.W*0.0373,color:'#3A3A3A'}}>总计:&nbsp;&nbsp;</Text> 
                            <Text style={{fontSize:BaseComponent.W*0.048,color:'#FF407E'}}>¥{this.state.GoodsData.payAmount}</Text>
                      </View>
                      <View style={{width:BaseComponent.W*0.424,height:BaseComponent.W*0.133,
                                    marginRight:BaseComponent.W*0.027,flexDirection:'row',
                                    justifyContent:'flex-end',alignItems:'center',
                                    position:'absolute',right:0}}>
                            <TouchableWithoutFeedback onPress={()=>{this.leftBtnClick();}}>        
                            <View style={{width:BaseComponent.W*70/375,height:BaseComponent.W*30/375,
                                          borderWidth:0.5,borderColor:'#999899',borderRadius:BaseComponent.W*15/375,
                                         justifyContent:'center',alignItems:'center',
                                         marginRight:BaseComponent.W*0.027}}>
                                <Text style={{fontSize:BaseComponent.W*14/375,color:'#B6B6B6'}}>
                                    {(()=>{
                                      if(this.state.GoodsData.orderStatus=="WAIT_FOR_PAY"){return "取消订单"};
                                      if(this.state.GoodsData.orderStatus=="CANCELED"){return "删除订单"};
                                    })()} 
                                </Text>
                            </View>
                            </TouchableWithoutFeedback>

                            <TouchableWithoutFeedback onPress={()=>{this.rightBtnClick();}}>
                            <View style={{width:BaseComponent.W*70/375,height:BaseComponent.W*30/375,
                                          borderWidth:0.5,borderColor:'#FF407E',borderRadius:BaseComponent.W*15/375,
                                          justifyContent:'center',alignItems:'center',}}>
                                 <Text style={{fontSize:BaseComponent.W*14/375,color:'#FF407E'}}>
                                    {(()=>{
                                      if(this.state.GoodsData.orderStatus=="WAIT_FOR_PAY"){return "立即支付"};
                                      if(this.state.GoodsData.orderStatus=="CANCELED"){return "再次购买"};
                                    })()} 
                                 </Text>
                            </View>
                            </TouchableWithoutFeedback>
                      </View>
                  </View>
                  </View>)
            }
          

          </View>      
        )
    }
}
const styles=StyleSheet.create({
    Address:{
          width:'100%',
          height:BaseComponent.W*0.2,
          flexDirection:'column',
          backgroundColor:'#FFF6E3'
    },
    AddressImg:{
        position:'absolute',
        width:BaseComponent.W*0.045,
        height:BaseComponent.W*0.045,
        left:0,
        marginTop:BaseComponent.W*0.047,
        marginLeft:BaseComponent.W*0.027
    },
    Item:{
        width:'100%',
        height:BaseComponent.H*0.129,
        flexDirection:'row',
        alignItems:'center',
        backgroundColor:'white'
    },
    ItemImg:{
        width:BaseComponent.W*0.211,
        height:BaseComponent.H*0.129,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'white'
    },
    ItemIns:{
        width:BaseComponent.W*0.5,
        height:BaseComponent.W*0.16,
        flexDirection:'column',
        justifyContent:'space-between',
        backgroundColor:'white',
    },
    CostBreakdown:{
      width:BaseComponent.W,
      
      flexDirection:'column',
      backgroundColor:'white',
    },
    CostBreakdownTongyong:{
      width:'100%',
      height:BaseComponent.W*0.0773,
      flexDirection:'row',
      justifyContent:'space-between',
      alignItems:'center',
    }
});