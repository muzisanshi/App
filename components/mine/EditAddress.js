/**
 * @name EditAddress.js
 * @auhor 程浩
 * @date 2018.8.21
 * @desc 编辑地址
 */
import React,{Component} from 'react';
import {Text,View,StyleSheet,Image,TouchableWithoutFeedback,ScrollView,ActivityIndicator,
ImageBackground,TextInput,StatusBar,Modal,FlatList,Picker} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import ImagePicker from 'react-native-image-crop-picker'; 
// 导入工具函数库
import T from "../../lib/Tools";
// 导入公共样式
import St from "../../lib/Style";
// 导入图片资源库
import Imgs from "../../lib/Img";
import BaseComponent from '../BaseComponent';
import DeliverGoods from '../cart/DeliverGoods';
import SelectAddressOrDate  from './SelectAddressOrDate';

import PickerNew from 'react-native-picker';


// Tools实例
let ti = T.getInstance();

let style = StyleSheet.create({
  wrapper:{
    backgroundColor:"white",
    flex:1,
    flexDirection:"column",
    alignItems:"stretch",
    justifyContent:"flex-start",
    position:"relative",
  },
  statusBar:{
    backgroundColor:"rgb(255,255,255)",
    height:ti.select({ios:16,android:22}),
    position:"relative",
    zIndex:1,
    opacity:1,
  },
  navBar:{
    backgroundColor:"rgb(255,255,255)",
    height:"7%",
    position:"relative",
    zIndex:1,
    opacity:1,
    borderBottomWidth:1,
    borderBottomColor:"rgb(241,241,241)"
  },
});

export default class EditAddress extends BaseComponent{
  constructor(props){
    super(props);
    this.viewabilityConfig = {
      waitForInteraction: true,
      viewAreaCoveragePercentThreshold: 250
    };
    this.state={
      inputAddress:"",// 输入待识别的地址
     address:'请选择省市区',
     defaultAddress:"",//是否是默认地址
     id:"",//地址ID
     shouhuorenName:"",//收货人名字
     phoneNo:"",//收货人手机
     shouhuorenAddress:"",//收货人详细地址
     idCardImageFrontAttachment:" ",//身份证正面照片
     idCardImageBackAttachment:" ",//身份证反面照片
     idCardImageFrontAttachmentID:"",//身份证正面的ID
     idCardImageBackAttachmentID:"",//身份证反面的ID
     receiverIdCardNo:"",//身份证号码
     isvisible:false,//选择省市区，false表示不可见
     province:[],//省
     city:[],//市
     area:[],//区
     provinceName:"",//省名字
     cityName:"",//市名字
     areaName:"",//区名字
     provinceId:110000,//默认是北京省
     cityId:110001,//默认是北京市
     areaId:110105,//默认是朝阳区
     visible:false,//转菊花
     isAddrOpen:false,// 默认地址选择器关闭
     
     // 省、市、区选中数据及状态
     provinceState:{
      show:true,
      pid:null,
      title:"请选择",
      selected:true,
     },
     cityState:{
      show:false,
      cid:null,
      title:"请选择",
      selected:false,
     },
     areaState:{
      show:false,
      aid:null,
      title:"请选择",
      selected:false,
     },
     hasArea:true,

     // 省数据
     provinceData:[],
     cityData:[],
     areaData:[],

     finalAddr:{
      default:"请选择省市区",
      province:{
        name:"",
      },
      city:{
        name:""
      },
      area:{
        name:""
      }
     },

     finalAreaId:null,


     // 是否显示收货人信息识别框
     showRecInput:true,
  } 
  // 判断是否打开了地址选择器
  this.isAddrOpen = false;
};
    _flatList;//省的flatlist的ID
    _flatListcity;//市的flatlist的ID
    _flatListarea;//区的flatlist的ID

    open(){
      this.refs.SelectAddressOrDate.showAddress(this.state.address)
    }
    callBackAddressValue(value){
        this.setState({
            address:value
        });
    }

    getProvinceData(callback){
      var thiz=this;
      thiz.request("area/getAllOneLevel",{
        method:"POST",
        success:function(ret){
          thiz.log("--------getProvinceData_ret--------",ret);
          callback(ret);
        },
        error:function(err){
          callback(null);
        }
      });
    }

    // 获取市数据
    getCityData(pid,callback){
      let thiz = this;
      thiz.request("area/getByParentId",{
          method:"POST",
          data:{parentAreaId:pid},
          success:function(ret){
            thiz.log("--------getCityData_ret--------",ret);
            callback(ret);
          },
          error:function(err){
            callback(null);
          }
      });
    }

    // 获取区县数据
    getAreaData(cid,callback){
      let thiz = this;
      thiz.request("area/getByParentId",{
          method:"POST",
          data:{parentAreaId:cid},
          success:function(ret){
            thiz.log("--------getAreaData_ret--------",ret);
            callback(ret);
          },
          error:function(err){
            callback(null);
          }
      })
    }

    // 加载地址数据
    loadAddrData(){
      let thiz = this;
      // 默认只获取省的数据
      thiz.getProvinceData(function(ret){
        if(ret&&ret.data&&ret.data.length>0){
          // 处理数据
          for(let i=0;i<ret.data.length;i++){
            ret.data[i].selected = false;
          }
          thiz.setState({
            provinceData:ret.data,
          });
        }
      });
    }

    recognize(){
      // 判断输入情况
      let thiz = this;
      if(!thiz.state.inputAddress){
        thiz.toast("请粘贴收货人信息");
        return;
      }
      // 识别
      thiz.setState({
        visible:true,
      });
      thiz.request("receiverAddress/recognize",{
          method:"POST",
          data:{
            content:thiz.state.inputAddress,
          },
          success:function(ret){
            thiz.setState({
              visible:false,
            });
            thiz.log("--------recognize_ret--------",ret);
            if(ret&&ret.respCode=="00"){
              thiz.setState({
                shouhuorenName:ret.data.name?ret.data.name:"",
                shouhuorenAddress:ret.data.address&&ret.data.address.detail?ret.data.address.detail:"",
                phoneNo:ret.data.phone?ret.data.phone:"",
                receiverIdCardNo:ret.data.idCard?ret.data.idCard:"",
                address:(function(){
                  let total = "";
                  if(ret.data.address&&ret.data.address.province&&ret.data.address.province.name){
                    total+=ret.data.address.province.name;
                  }
                  if(ret.data.address&&ret.data.address.city&&ret.data.address.city.name){
                    total+=ret.data.address.city.name;
                  }
                  if(ret.data.address&&ret.data.address.county&&ret.data.address.county.name){
                    total+=ret.data.address.county.name;
                  }
                  console.log("------total--------",total);
                  return total;
                })(),
                finalAddr:{
                  default:"请选择省市区",
                  province:{
                    name:ret.data.address&&ret.data.address.province&&ret.data.address.province.name?ret.data.address.province.name:"",
                  },
                  city:{
                    name:ret.data.address&&ret.data.address.city&&ret.data.address.city.name?ret.data.address.city.name:"",
                  },
                  area:{
                    name:ret.data.address&&ret.data.address.county&&ret.data.address.county.name?ret.data.address.county.name:"",
                  }
                },
                finalAreaId:(function(){
                  if(ret.data.address&&ret.data.address.county&&ret.data.address.county.id){
                    return ret.data.address.county.id;
                  }
                  if(ret.data.address&&ret.data.address.city&&ret.data.address.city.id){
                    return ret.data.address.city.id;
                  }
                  return null;
                })(),
              })
            }
          },
          error:function(err){
            thiz.setState({
              visible:false,
            });
          }
      })
    }
    
    componentDidMount(){
      var thiz=this;
      var editDataid=thiz.params.editDataID;
      thiz.log("-------------------edit-------------",editDataid);
      //根据ID获取收货人地址数据
      thiz.request("receiverAddress/getById",{
        method:"POST",
        data:{id:editDataid},
        success:function(ret){
          thiz.log("--------------------------------------收货人地址数据--------------------------------",ret);
           if(!ret.data.idCardImageFrontAttachment)
            {
              ret.data.idCardImageFrontAttachment={};
              if(!ret.data.idCardImageFrontAttachment.resourceFullAddress)
              {
                 ret.data.idCardImageFrontAttachment.resourceFullAddress="";
              }
            }
            if(!ret.data.idCardImageBackAttachment)
            {
              ret.data.idCardImageBackAttachment={};
              if(!ret.data.idCardImageBackAttachment.resourceFullAddress){
              ret.data.idCardImageBackAttachment.resourceFullAddress="";
              }
            }
          thiz.setState({
            defaultAddress:ret.data.defaultAddress,
            id:ret.data.id,//地址ID
            shouhuorenName:ret.data.receiverName,//收货人名字
            phoneNo:ret.data.receiverPhoneNo,//收货人手机
            shouhuorenAddress:ret.data.receiverAddress,//收货人详细地址
            idCardImageFrontAttachment:ret.data.idCardImageFrontAttachment.resourceFullAddress,//身份证正面照片
            idCardImageBackAttachment:ret.data.idCardImageBackAttachment.resourceFullAddress,//身份证反面照片
            idCardImageFrontAttachmentID:ret.data.idCardImageFrontAttachment.id,//身份证正面的ID
            idCardImageBackAttachmentID:ret.data.idCardImageBackAttachment.id,//身份证反面的ID
            receiverIdCardNo:ret.data.receiverIdCardNo,//身份证号码
            address:ret.data.fullAreaAddress,//省市区
            
            areaState:{
              aid:ret.data.receiverArea.id,
            },

            finalAddr:{
              default:"请选择省市区",
              province:{
                name:ret.data.areas[0].name,
              },
              city:{
                name:ret.data.areas[1].name,
              },
              area:{
                name:ret.data.areas[2]?ret.data.areas[2].name:"",
              }
            },

            finalAreaId:ret.data.receiverArea.id,

          })
        },
        error:function(err){

        }
      })
      // //获取省市区数据
      // thiz.request("area/getAllOneLevel",{
      //   method:"POST",
      //   success:function(ret){
      //     var provinceDataStr=[
      //           {id:"",name:""},
      //           {id:"",name:""}
      //       ];
      //       var provinceData=provinceDataStr.concat(ret.data);
      //       provinceData=provinceData.concat(provinceDataStr);
      //       thiz.log("--------------------------cityData-----------------------------",provinceData);
      //       thiz.setState({
      //         province:provinceData,
      //       });
      //       thiz.getCity();
      //       thiz.getArea();
      //   },
      //   error:function(err){

      //   }
      // })
      thiz.loadAddrData();
    };
    //获取当前选中的省
    getVisibleRows=(info)=>{
            var thiz=this;
            thiz.log("------------info-----------",info);
            var name=info.viewableItems[2].item.name;
            var id=info.viewableItems[2].item.id;
            console.log("---------------------id-------------------",id);
            console.log("------------------name--------------------",name);
            thiz.setState({
              provinceId:id,
              provinceName:name,
            })
    }
    //获取当前选中的市
    getVisibleCityRows=(info)=>{
            var thiz=this;
            thiz.log("------------info111111-----------",info);
            if(info.viewableItems.length<3)
            {
              var cityname=info.viewableItems[0].item.name;
              var cityid=info.viewableItems[0].item.id;
              console.log("---------------------Cityid-------------------",cityid);
              console.log("------------------Cityname--------------------",cityname);
              thiz.setState({
                cityId:cityid,
                cityName:cityname
              })
            }else{
              var cityname=info.viewableItems[2].item.name;
              var cityid=info.viewableItems[2].item.id;
              console.log("---------------------Cityid-------------------",cityid);
              console.log("------------------Cityname--------------------",cityname);
              thiz.setState({
                cityId:cityid,
                cityName:cityname
              })
          }
    }
    //获取当前选中的区
    getVisibleAreaRows=(info)=>{
            var thiz=this;
            thiz.log("------------info111111-----------",info);
            if(info.viewableItems.length<5)
            {
              return ;
            }
            var areaname=info.viewableItems[2].item.name;
            var areaid=info.viewableItems[2].item.id;
            console.log("---------------------areaid-------------------",areaid);
            console.log("------------------areaname--------------------",areaname);
            thiz.setState({
              areaId:areaid,
              areaName:areaname,
            })
    }
   
    //打开正面身份证相册
   openAlbum=()=>{

      let thiz = this;
      ImagePicker.openPicker({  
          width: 300,  
          height: 400,  
          cropping:false,
          compressImageQuality:0.5,
      }).then(image => {  
        console.log("----------image.path---------",image.path);
        thiz.setState({
            visible:true,
        })
        let url = image.path;
        if(url&&BaseComponent.OS=="ios"){
          url = "file:///"+url;
        }
        thiz.upload(url,{
          progress:function(pro){

          },
          success:function(ret){
            thiz.setState({
              idCardImageFrontAttachmentID:ret.data.id,
              idCardImageFrontAttachment:image.path,
              visible:false,
            })
            
          },
          error:function(err){
            thiz.setState({visible:false});
            console.log("--------------------调用上传接口失败------------------");
          }
        });
      })
    }
    //打开反面身份证相册
   openAlbum1=()=>{

      let thiz = this;
      ImagePicker.openPicker({  
          width: 300,  
          height: 400,  
          cropping:false,
          compressImageQuality:0.5,
      }).then(image => {  
        console.log("----------image.path---------",image.path);
        thiz.setState({
            visible:true,
        })
        let url = image.path;
        if(url&&BaseComponent.OS=="ios"){
          url = "file:///"+url;
        }
        thiz.upload(url,{
          progress:function(pro){

          },
          success:function(ret){
            thiz.setState({
              idCardImageBackAttachmentID:ret.data.id,
              idCardImageBackAttachment:image.path,
              visible:false,
            })
            
          },
          error:function(err){
            thiz.setState({visible:false});
            console.log("--------------------调用上传接口失败------------------");
          }
        });
      })
    }
    //save保存按钮点击事件
    save=(forceType)=>{
      var thiz=this;
      var reg = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
      var shouhuorenName=thiz.state.shouhuorenName;
      var phoneNo=thiz.state.phoneNo;
      var shouhuorenAddress=thiz.state.shouhuorenAddress;
      var receiverIdCardNo=thiz.state.receiverIdCardNo;
      var idCardImageFrontAttachmentID=thiz.state.idCardImageFrontAttachmentID;
      var idCardImageBackAttachmentID=thiz.state.idCardImageBackAttachmentID;
      var defaultAddress=thiz.state.defaultAddress;
      var receiverArea=thiz.state.finalAreaId;

      if(shouhuorenName=="")
      {
        thiz.toast('请填写收货人姓名');
        return ;
      }
      if(phoneNo=="")
      {
        thiz.toast('请填写手机号');
        return ;
      }
      if(!receiverArea){
        thiz.toast("请选择地区");
        return;
      }
      if(shouhuorenAddress=="")
      {
        thiz.toast('请填写详细地址');
        return ;
      }
      // if(receiverIdCardNo=="")
      // {
      //   thiz.toast('请填写身份证号');
      //   return ;
      // }
      if(!reg.test(receiverIdCardNo))
      {
        thiz.toast("身份证格式不正确");
        return ;
      }
      // if(idCardImageFrontAttachmentID=="")
      // {
      //   thiz.toast('请上传身份证正面照片');
      //   return ;
      // }
      // if(idCardImageBackAttachmentID=="")
      // {
      //   thiz.toast('请上传身份证反面照片');
      //   return ;
      // }

      let paras = {
          id:thiz.state.id,
          defaultAddress:defaultAddress,
          idCardImageBackAttachment:{id:idCardImageBackAttachmentID},
          idCardImageFrontAttachment:{id:idCardImageFrontAttachmentID},
          receiverAddress:shouhuorenAddress,
          receiverArea:{id:receiverArea},
          receiverIdCardNo:receiverIdCardNo,
          receiverName:shouhuorenName,
          receiverPhoneNo:phoneNo,

          forceUpdateType:forceType?forceType:"NONE",
        }
      
      thiz.request("receiverAddress/saveOrUpdate",{
        method:"POST",
        data:paras,
        success:function(ret){

            if(ret&&ret.respCode=="00"){
              // 判断是否是从  购物车 和 确认订单  页面跳转过来的，如果是，默认就返回设置保存的地址，不需要再回到地址列表
              if(thiz.params.from=="Cart" || thiz.params.from=="Querendingdan" || thiz.params.from=="GoodsXiangQing"){
                // 发送保存返回的数据，设置新的地址数据
                thiz.emit("save_receiveraddr_success",{address:ret.data.receiverAddress});

                setTimeout(function(){
                  thiz.navigate(thiz.params.from);
                },200);

              }else{// 其他情况

                thiz.emit("save_receiveraddr_success",{address:ret.data.receiverAddress});
              
                setTimeout(function(){
                  thiz.goBack();
                },200);

              }
            }

            // thiz.emit("save_receiveraddr_success");
            // thiz.goBack();
        },
        error:function(err){
          thiz.toast(err.msg);
        } 
      })
    }
    //渲染省
    _renderItemProvince=(info)=>{
      var thiz=this;
      // thiz.log("---------------------------data----------------------",info);
        return (
          <View style={{width:'100%',height:BaseComponent.W*50/375,justifyContent:'center',alignItems:'center'}}>
              <Text style={{fontSize:15,color:'black'}}>{info.item.name}</Text>
          </View>
        )
    }
    //渲染市
    _renderItemCity=(info)=>{
      var thiz=this;
      // thiz.log("---------------------------data----------------------",info);
        return (
          <View style={{width:'100%',height:BaseComponent.W*50/375,justifyContent:'center',alignItems:'center'}}>
              <Text style={{fontSize:15,color:'black'}}>{info.item.name}</Text>
          </View>
        )
    }
    //渲染区
    _renderItemArea=(info)=>{
      var thiz=this;
      // thiz.log("---------------------------data----------------------",info);
        return (
          <View style={{width:'100%',height:BaseComponent.W*50/375,justifyContent:'center',alignItems:'center'}}>
              <Text style={{fontSize:15,color:'black'}}>{info.item.name}</Text>
          </View>
        )
    }
    //获取市
    getCity=()=>{
      var thiz=this;
        thiz.request("area/getByParentId",{
          method:"POST",
          data:{parentAreaId:this.state.provinceId},
          success:function(ret){

            thiz.setState({
            city:ret.data,
            })
          },
          error:function(err){

          }
      })
    }
    //获取区县
    getArea=()=>{
      var thiz=this;
        thiz.request("area/getByParentId",{
          method:"POST",
          data:{parentAreaId:this.state.cityId},
          success:function(ret){
            thiz.setState({
            area:ret.data
            })
          },
          error:function(err){

          }
      })
    }
    //省市区选择器确定按钮
    ensure=()=>{
      var thiz=this;
      var provinceName=thiz.state.provinceName;
      var cityName=thiz.state.cityName;
      var areaName=thiz.state.areaName;
      thiz.setState({
        address:provinceName+cityName+areaName,
        isvisible:false,
      });
    }
    //唯一key
    _keyExtractor=(item,index)=>index.toString();

     render(){
      let thiz = this;
        return(   
          <View style={style.wrapper}>   
                {/*状态栏占位*/}
                <View style={{ backgroundColor:this.state.isvisible?'rgba(14,14,14,0.5)':"rgb(255,255,255)",
                      height:this.isIphoneX(30,BaseComponent.SH),
                      position:"relative",
                      zIndex:1,
                      opacity:1,}}></View>
                {/*顶部导航栏*/}
                <View style={style.navBar}>
                  <View style={{flex:1,flexDirection:"row",justifyContent:"flex-start"}}>

                    <TouchableWithoutFeedback onPress={()=>this.goBack()}>
                    <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>
                      <Image source={Imgs.back} style={{width:(BaseComponent.W*0.127*0.4),height:(BaseComponent.W*0.127*0.4),marginRight:BaseComponent.W*0.04}}></Image>
                    </View>
                    </TouchableWithoutFeedback>

                    <View style={{flexGrow:1,backgroundColor:"transparent",
                      marginTop:ti.select({ios:(BaseComponent.H*0.02)/2,android:(BaseComponent.H*0.016)/2}),borderRadius:ti.select({ios:(BaseComponent.H*0.05)/2,android:(BaseComponent.H*0.054)/2}),borderColor:"#0A0A0A",
                      borderWidth:0,}}>
                      <Text style={{color:"#0A0A0A",width:"100%",textAlign:"center",marginTop:ti.select({ios:"4%",android:"1.6%"}),fontSize:17}}>{this.params.title}</Text>
                    </View>

                    <TouchableWithoutFeedback onPress={()=>this.save("NONE")}>
                    <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>
                        <Text style={{fontSize:BaseComponent.W*0.037,color:'#0A0A0A',marginTop:ti.select({ios:"4%",android:"1.6%"})}}>保存</Text>
                    </View>
                    </TouchableWithoutFeedback>

                  </View>
                </View> 

                {/*转菊花*/}
                <Modal
                  visible={this.state.visible}
                  onRequestClose={()=>{this.setState({visible:false})}}
                  transparent={true}
                  animationType={"fade"}>
                  <View style={{backgroundColor:"rgba(0,0,0,0.4)",width:"100%",width:"100%",flex:1,flexDirection:"column",alignItems:"center",justifyContent:"center",position:"relative"}}>
                    <View>
                          <ActivityIndicator size="large" color='white'/>
                    </View>
                  </View>
                </Modal>
            
            {/*新的省市区选择*/}
            <Modal visible={this.state.isAddrOpen}
                   transparent={true}
                   animationType={"fade"}
                   onShow={()=>{}}
                   onRequestClose={() => {this.setState({isAddrOpen:false})}}>
                   <TouchableWithoutFeedback onPress={()=>{
                    thiz.setState({
                      isAddrOpen:false,
                      
                    });
                   }}>
                   <View style={{flex:1,flexDirection:"column",backgroundColor:"rgba(0,0,0,0.1)",position:"relative"}}>

                    <View style={{position:"absolute",bottom:0,left:0,right:0,width:"100%",backgroundColor:"white"}}>
                      {/*顶部按钮栏*/}
                      <View style={{width:"100%",flexDirection:"row",
                        justifyContent:"space-between",backgroundColor:"white",
                        borderBottomWidth:1,borderBottomColor:"#eee"}}>

                        <TouchableWithoutFeedback onPress={()=>{
                            thiz.setState({
                              isAddrOpen:false,

                            });
                        }}>
                          <View style={{paddingTop:10,paddingBottom:10,paddingLeft:20,paddingRight:20}}>
                            <Text style={{fontSize:14,color:"#ccc"}}>取消</Text>
                          </View>
                        </TouchableWithoutFeedback>
                        
                        <TouchableWithoutFeedback onPress={()=>{}}>
                          <View style={{paddingTop:10,paddingBottom:10,}}>
                            <Text style={{fontSize:16,fontColor:"#3a3a3a"}}>地区选择</Text>
                          </View>
                        </TouchableWithoutFeedback>
                        
                        <TouchableWithoutFeedback onPress={()=>{
                          if(thiz.state.hasArea){
                              if(!thiz.state.provinceState.pid || !thiz.state.cityState.cid || !thiz.state.areaState.aid){
                                thiz.setState({
                                  isAddrOpen:false,
                                });
                                return;
                              }
                              let address = thiz.state.provinceState.title+" "+thiz.state.cityState.title+" "+thiz.state.areaState.title;
                              thiz.setState({
                                isAddrOpen:false,
                                address:address,
                                finalAddr:{
                                  default:"请选择省市区",
                                  province:{
                                    name:thiz.state.provinceState.title,
                                  },
                                  city:{
                                    name:thiz.state.cityState.title,
                                  },
                                  area:{
                                    name:thiz.state.areaState.title,
                                  }
                                },
                                finalAreaId:thiz.state.areaState.aid
                              });
                            }else{
                              if(!thiz.state.provinceState.pid || !thiz.state.cityState.cid){
                                thiz.setState({
                                  isAddrOpen:false,
                                });
                                return;
                              }
                              let address = thiz.state.provinceState.title+" "+thiz.state.cityState.title;
                              thiz.setState({
                                isAddrOpen:false,
                                address:address,
                                finalAddr:{
                                  default:"请选择省市区",
                                  province:{
                                    name:thiz.state.provinceState.title,
                                  },
                                  city:{
                                    name:thiz.state.cityState.title,
                                  },
                                  area:{
                                    name:"",
                                  }
                                },
                                finalAreaId:thiz.state.cityState.cid
                              });
                            }
                        }}>
                          <View style={{paddingTop:10,paddingBottom:10,paddingLeft:20,paddingRight:20}}>
                            <Text style={{fontSize:14,color:"rgb(48,117,254)"}}>确定</Text>
                          </View>
                        </TouchableWithoutFeedback>

                      </View>

                      {/*地址显示区域*/}
                      <View style={{width:"100%",flexDirection:"column",backgroundColor:"white"}}>

                        {/*选中地址显示*/}
                        <View style={{width:"100%",borderBottomWidth:1,borderBottomColor:"#eee",flexDirection:"row",paddingLeft:10,paddingRight:10,}}>

                          <TouchableWithoutFeedback onPress={()=>{
                            // 先判断是否处于选中状态
                            if(!thiz.state.provinceState.selected){
                              let provinceState = thiz.state.provinceState;
                              let cityState = thiz.state.cityState;
                              let areaState = thiz.state.areaState;

                              provinceState.selected = true;
                              cityState.selected = false;
                              areaState.selected = false;
                              
                              thiz.setState({
                                provinceState:provinceState,
                                areaState:areaState,
                                cityState:cityState,
                              });
                            }
                          }}>
                          <View style={{flexDirection:"column",position:"relative",paddingTop:10,paddingBottom:10,marginRight:10,}}>
                            <Text numberOfLines={1} style={{color:thiz.state.provinceState.selected?"red":"#3a3a3a",maxWidth:BaseComponent.W*0.33-20,}}>{thiz.state.provinceState.title}</Text>
                            <View style={{height:2,width:"100%",backgroundColor:thiz.state.provinceState.selected?"red":"white",position:"absolute",bottom:0,left:0,right:0}}></View>
                          </View>
                          </TouchableWithoutFeedback>

                          <TouchableWithoutFeedback onPress={()=>{
                            // 先判断是否处于选中状态
                            if(!thiz.state.cityState.selected){
                              let provinceState = thiz.state.provinceState;
                              let cityState = thiz.state.cityState;
                              let areaState = thiz.state.areaState;

                              provinceState.selected = false;
                              cityState.selected = true;
                              areaState.selected = false;
                              
                              thiz.setState({
                                provinceState:provinceState,
                                areaState:areaState,
                                cityState:cityState,
                              });
                            }
                          }}>
                          <View style={{display:thiz.state.cityState.show?"flex":"none",flexDirection:"column",
                            position:"relative",paddingTop:10,paddingBottom:10,marginLeft:10,marginRight:10}}>
                            <Text numberOfLines={1} style={{color:thiz.state.cityState.selected?"red":"#3a3a3a",maxWidth:BaseComponent.W*0.33-20,}}>{thiz.state.cityState.title}</Text>
                            <View style={{height:2,width:"100%",backgroundColor:thiz.state.cityState.selected?"red":"white",position:"absolute",bottom:0,left:0,right:0}}></View>
                          </View>
                          </TouchableWithoutFeedback>

                          <TouchableWithoutFeedback onPress={()=>{
                            // 先判断是否处于选中状态
                            if(!thiz.state.areaState.selected){
                              let provinceState = thiz.state.provinceState;
                              let cityState = thiz.state.cityState;
                              let areaState = thiz.state.areaState;

                              provinceState.selected = false;
                              cityState.selected = false;
                              areaState.selected = true;
                              
                              thiz.setState({
                                provinceState:provinceState,
                                areaState:areaState,
                                cityState:cityState,
                              });
                            }
                          }}>
                          <View style={{display:thiz.state.areaState.show?"flex":"none",flexDirection:"column",
                            position:"relative",paddingTop:10,paddingBottom:10,marginLeft:10,}}>
                            <Text numberOfLines={1} style={{color:thiz.state.areaState.selected?"red":"#3a3a3a",maxWidth:BaseComponent.W*0.33-20,}}>{thiz.state.areaState.title}</Text>
                            <View style={{height:2,width:"100%",backgroundColor:thiz.state.areaState.selected?"red":"white",position:"absolute",bottom:0,left:0,right:0}}></View>
                          </View>
                          </TouchableWithoutFeedback>

                        </View>

                        {/*地址滚动视图*/}
                        {/*省*/}
                        <View style={{display:thiz.state.provinceState.selected?"flex":"none",width:"100%",height:BaseComponent.H*0.35,backgroundColor:"white"}}>
                          <ScrollView showsVerticalScrollIndicator={false}
                            style={{width:"100%",height:"100%"}}
                            horizontal={false}>

                            {
                              thiz.state.provinceData.length>0?thiz.state.provinceData.map(function(v,i){
                                return (
                                  <TouchableWithoutFeedback onPress={()=>{
                                    // 重置选中状态
                                    // if(!v.selected){

                                      // 加载市的数据
                                      thiz.getCityData(v.id,function(ret){
                                        
                                        if(ret&&ret.data&&ret.data.length>0){
                                          let provinceData = thiz.state.provinceData;
                                          for(let m=0;m<provinceData.length;m++){
                                            if(m!=i){
                                              provinceData[m].selected = false;
                                            }else{
                                              provinceData[m].selected = true;
                                            }
                                          }
                                          // 处理数据
                                          for(let k=0;k<ret.data.length;k++){
                                            ret.data[k].selected = false;
                                          }
                                          thiz.setState({
                                            provinceData:provinceData,
                                            cityData:ret.data,
                                            areaData:[],
                                            provinceState:{
                                              title:v.name,
                                              pid:v.id,
                                              show:true,
                                              selected:false,
                                            },
                                            cityState:{
                                              title:"请选择",
                                              cid:null,
                                              show:true,
                                              selected:true,
                                            },
                                            areaState:{
                                              title:"请选择",
                                              cid:null,
                                              show:false,
                                              selected:false,
                                            }
                                          });
                                        }
                                      });
                                    // }
                                  }}>
                                  <View style={{padding:10,position:"relative"}}>
                                    <Text style={{color:v.selected?"red":"#3a3a3a"}}>{v.name}</Text>
                                  </View>
                                  </TouchableWithoutFeedback>
                                );
                              }):null
                            }

                          </ScrollView>
                        </View>

                        {/*市*/}
                        <View style={{display:thiz.state.cityState.selected?"flex":"none",width:"100%",height:BaseComponent.H*0.35,backgroundColor:"white"}}>
                          <ScrollView showsVerticalScrollIndicator={false}
                            style={{width:"100%",height:"100%"}}
                            horizontal={false}>

                            {
                              thiz.state.cityData.length>0?thiz.state.cityData.map(function(v,i){
                                return (
                                  <TouchableWithoutFeedback onPress={()=>{
                                    // 重置选中状态
                                    // if(!v.selected){

                                      // 加载市的数据
                                      thiz.getAreaData(v.id,function(ret){
                                        if(ret&&ret.data&&ret.data.length>0){
                                          let cityData = thiz.state.cityData;
                                          for(let m=0;m<cityData.length;m++){
                                            if(m!=i){
                                              cityData[m].selected = false;
                                            }else{
                                              cityData[m].selected = true;
                                            }
                                          }
                                          // 处理数据
                                          for(let k=0;k<ret.data.length;k++){
                                            ret.data[k].selected = false;
                                          }

                                          let provinceState = thiz.state.provinceState;
                                          provinceState.selected = false;

                                          thiz.setState({
                                            cityData:cityData,
                                            areaData:ret.data,
                                            provinceState:provinceState,
                                            cityState:{
                                              title:v.name,
                                              cid:v.id,
                                              show:true,
                                              selected:false,
                                            },
                                            areaState:{
                                              title:"请选择",
                                              aid:null,
                                              show:true,
                                              selected:true,
                                            },
                                            hasArea:true,
                                          });
                                        }
                                        //返回数据没有第三级时
                                        if(ret&&ret.data&&ret.data.length==0){
                                          let cityData = thiz.state.cityData;
                                          for(let m=0;m<cityData.length;m++){
                                            if(m!=i){
                                              cityData[m].selected = false;
                                            }else{
                                              cityData[m].selected = true;
                                            }
                                          }
                                          let provinceState = thiz.state.provinceState;
                                          provinceState.selected = false;

                                           thiz.setState({
                                            cityData:cityData,
                                            areaData:ret.data,
                                            provinceState:provinceState,
                                            cityState:{
                                              title:v.name,
                                              cid:v.id,
                                              show:true,
                                              selected:true,
                                            },
                                            areaState:{
                                              title:"请选择",
                                              aid:null,
                                              show:false,
                                              selected:false,
                                            },
                                            hasArea:false,
                                          });
                                          thiz.log("----------------------cityData--------------------------",cityData);
                                        }
                                      });
                                    // }
                                  }}>
                                  <View style={{padding:10,position:"relative"}}>
                                    <Text style={{color:v.selected?"red":"#3a3a3a"}}>{v.name}</Text>
                                  </View>
                                  </TouchableWithoutFeedback>
                                );
                              }):null
                            }

                          </ScrollView>
                        </View>

                        {/*区、县*/}
                        <View style={{display:thiz.state.areaState.selected?"flex":"none",width:"100%",height:BaseComponent.H*0.35,backgroundColor:"white"}}>
                          <ScrollView showsVerticalScrollIndicator={false}
                            style={{width:"100%",height:"100%"}}
                            horizontal={false}>

                            {
                              thiz.state.areaData.length>0?thiz.state.areaData.map(function(v,i){
                                return (
                                  <TouchableWithoutFeedback onPress={()=>{
                                    // 重置选中状态
                                    let areaData = thiz.state.areaData;
                                    for(let m=0;m<areaData.length;m++){
                                      if(m!=i){
                                        areaData[m].selected = false;
                                      }else{
                                        areaData[m].selected = true;
                                      }
                                    }
                                    thiz.setState({
                                      areaData:areaData,
                                      areaState:{
                                        title:v.name,
                                        aid:v.id,
                                        show:true,
                                        selected:true,
                                      },
                                    });
                                  }}>
                                  <View style={{padding:10,position:"relative"}}>
                                    <Text style={{color:v.selected?"red":"#3a3a3a"}}>{v.name}</Text>
                                  </View>
                                  </TouchableWithoutFeedback>
                                );
                              }):null
                            }

                          </ScrollView>
                        </View>

                      </View>

                    </View>

                   </View>
                   </TouchableWithoutFeedback>
            </Modal>


            <ScrollView style={{width:'100%',height:'100%'}} showsVerticalScrollIndicator = {false}>
            <KeyboardAwareScrollView>

            {/*识别地址*/}
            <View style={{width:'100%',height:BaseComponent.W*0.5,backgroundColor:'#F0F0F0',alignItems:'center',display:'flex'}}>

                <TouchableWithoutFeedback onPress={()=>{
                  thiz.setState({
                    showRecInput:true,
                  });
                }}>
                <ImageBackground style={styles.ImageBackground} source={require('../../image/home/tianjiadizhi.png')}>

                        <Text style={{fontSize:BaseComponent.W*0.04,color:'#3A3A3A',display:thiz.state.showRecInput?"none":"flex"}}>在此区域粘贴收货人信息</Text>
                        <Text style={{fontSize:BaseComponent.W*0.032,color:'#A1A1A1',marginTop:BaseComponent.W*0.029,display:thiz.state.showRecInput?"none":"flex"}}>姓名,详细地址,电话,身份证号码</Text>

                        <TextInput style={{display:thiz.state.showRecInput?"flex":"none",width:BaseComponent.W*0.938-BaseComponent.W*10/375,height:BaseComponent.W*0.339-ti.select({ios:30,android:20}),
                        borderColor: 'transparent',borderWidth:0.5,borderRadius:0,marginLeft:BaseComponent.W*5/375,
                          marginRight:BaseComponent.W*5/375,color:"#232326",paddingLeft:10,fontSize:BaseComponent.W*15/375,textAlignVertical:'top'}}
                                      underlineColorAndroid='transparent' maxLength={500} placeholder={"在此区域粘贴收货人信息，姓名，详细地址，电话，身份证号码"}
                                      onChangeText={(event)=>this.setState({inputAddress:event})} multiline={true}></TextInput>

                </ImageBackground>
                </TouchableWithoutFeedback>

                <TouchableWithoutFeedback onPress={()=>{
                  thiz.recognize();
                }}>
                <View style={[styles.shibie,{backgroundColor:thiz.state.inputAddress?'#FED584':'#fff'}]}>
                    <Text style={{fontSize:BaseComponent.W*0.04,color:thiz.state.inputAddress?'#000':'#CDCDCD'}}>识别地址</Text>
                </View>
                </TouchableWithoutFeedback>

            </View>

            {/*填写地址*/}
            <View style={styles.write}>
                <View style={styles.write1}>
                <Text style={{fontSize:BaseComponent.W*0.037,color:'#121212'}}><Text style={{color:'#FF0000'}}>*</Text>收货人</Text>
                <TextInput value={thiz.state.shouhuorenName} placeholder="输入姓名" style={{width:BaseComponent.W*0.3,height:BaseComponent.W*0.106,padding:0,marginLeft:BaseComponent.W*0.104,fontSize:BaseComponent.W*0.037,color:'#121212'}} placeholderTextColor="#C7C7CD"
                        underlineColorAndroid='transparent'  onChangeText={(event)=>this.setState({shouhuorenName:event})} defaultValue={this.state.shouhuorenName}></TextInput>
                <View style={{position:'absolute',bottom:0,width:"100%",height:0.5,backgroundColor:'#E4E4E4'}}></View>
                </View>  

                <View style={styles.write1}>
                <Text style={{fontSize:BaseComponent.W*0.037,color:'#121212'}}><Text style={{color:'#FF0000'}}>*</Text>手机    </Text>
                <TextInput value={thiz.state.phoneNo} placeholder="输入联系方式" style={{width:BaseComponent.W*0.3,height:BaseComponent.W*0.106,padding:0,marginLeft:BaseComponent.W*0.104,fontSize:BaseComponent.W*0.037,color:'#121212'}} placeholderTextColor="#C7C7CD"
                        underlineColorAndroid='transparent' onChangeText={(event)=>this.setState({phoneNo:event})} defaultValue={this.state.phoneNo}></TextInput>
                <View style={{position:'absolute',bottom:0,width:"100%",height:0.5,backgroundColor:'#E4E4E4'}}></View>
                </View>
                
                {/*选择省市区*/}
                <TouchableWithoutFeedback onPress={()=>{
                  let thiz = this;
                  thiz.setState({
                    isAddrOpen:!thiz.state.isAddrOpen,
                  })
                }}>
                <View style={styles.write1}>
                  <Text style={{fontSize:BaseComponent.W*0.037,color:'#121212'}}><Text style={{color:'#FF0000'}}>*</Text>地区</Text>
                  <View style={{flexDirection:"row",position:"absolute",right:30,}}>
                    <Text numberOfLines={1} style={{maxWidth:BaseComponent.W*0.33,fontSize:BaseComponent.W*0.037,color:this.state.finalAddr.province.name?'#3a3a3a':'#C7C7CD',marginRight:5}}>{this.state.finalAddr.province.name?this.state.finalAddr.province.name:this.state.finalAddr.default}</Text>
                    <Text numberOfLines={1} style={{maxWidth:BaseComponent.W*0.33,fontSize:BaseComponent.W*0.037,color:this.state.finalAddr.city.name?'#3a3a3a':'#C7C7CD',marginRight:5}}>{this.state.finalAddr.city.name}</Text>
                    <Text numberOfLines={1} style={{maxWidth:BaseComponent.W*0.33,fontSize:BaseComponent.W*0.037,color:this.state.finalAddr.area.name?'#3a3a3a':'#C7C7CD',marginRight:0}}>{this.state.finalAddr.area.name}</Text>
                  </View>
                  <Image style={{width:BaseComponent.W*0.02,height:BaseComponent.W*0.07,position:'absolute',right:0,marginRight:BaseComponent.W*0.03}} 
                        source={require('../../image/home/youjiantou.png')}></Image>
                  <View style={{position:'absolute',bottom:0,width:"100%",height:0.5,backgroundColor:'#E4E4E4'}}></View>
                </View>
                </TouchableWithoutFeedback>
                    
                <View style={{width:BaseComponent.W*0.968,height:BaseComponent.W*0.25,marginLeft:BaseComponent.W*0.032}}>
                  <TextInput value={thiz.state.shouhuorenAddress} placeholder="详细地址（请勿填写省市区信息）" style={{fontSize:BaseComponent.W*0.037,color:'#121212',textAlignVertical:'top'}} placeholderTextColor="#C7C7CD"
                         underlineColorAndroid='transparent' multiline={true} onChangeText={(event)=>this.setState({shouhuorenAddress:event})} defaultValue={this.state.shouhuorenAddress}></TextInput>
                  <View style={{position:'absolute',bottom:0,width:"100%",height:0.5,backgroundColor:'#E4E4E4'}}></View>
                </View>

                <View style={styles.write1}>
                  <Text style={{fontSize:BaseComponent.W*0.037,color:'#121212'}}><Text style={{color:'#FF0000'}}>*</Text>身份证号</Text>
                  <TextInput value={thiz.state.receiverIdCardNo} placeholder="输入身份证号码" style={{width:'100%',height:BaseComponent.W*0.106,padding:0,marginLeft:BaseComponent.W*0.067,fontSize:BaseComponent.W*0.037,color:'#121212'}} placeholderTextColor="#C7C7CD"
                        underlineColorAndroid='transparent' onChangeText={(event)=>this.setState({receiverIdCardNo:event})} defaultValue={this.state.receiverIdCardNo} maxLength={18}></TextInput>
                  <View style={{position:'absolute',bottom:0,width:"100%",height:0.5,backgroundColor:'#E4E4E4'}}></View>
                </View>


                <View style={{width:'100%',height:BaseComponent.W*0.4}}>

                <View style={{flexDirection:'row',marginTop:BaseComponent.W*0.045,marginLeft:BaseComponent.W*0.2}}>
                  
                  <Text style={{fontSize:BaseComponent.W*0.032,color:'#666666'}}>请提供身份证正反面以供清关</Text>
                  <Text style={{fontSize:BaseComponent.W*0.029,color:'#C7C7CD',marginLeft:BaseComponent.W*0.018}}>(信息与收货人一致)</Text>
                </View>
   
                <View style={{width:BaseComponent.W*0.563,height:BaseComponent.W*0.192,
                              flexDirection:'row',marginLeft:BaseComponent.W*0.2,
                              justifyContent:'space-between',marginTop:BaseComponent.W*0.026}}>
                  <TouchableWithoutFeedback onPress={()=>this.openAlbum()}>                 
                  <View>  
                  <ImageBackground style={{width:BaseComponent.W*0.25,height:BaseComponent.W*0.192}} source={{uri:this.state.idCardImageFrontAttachment}}>
                      <View style={{width:BaseComponent.W*0.25,height:BaseComponent.W*0.192,backgroundColor:'rgba(14,14,14,0.5)',justifyContent:'center',alignItems:'center'}}>
                          <Text style={{color:'white'}}>{this.state.idCardImageFrontAttachment?'已上传':'未上传'}</Text>
                      </View>
                  </ImageBackground>
                  </View>
                  </TouchableWithoutFeedback>

                  <TouchableWithoutFeedback onPress={()=>this.openAlbum1()}> 
                  <View>
                  <ImageBackground style={{width:BaseComponent.W*0.25,height:BaseComponent.W*0.192}} source={{uri:this.state.idCardImageBackAttachment}}>
                       <View style={{width:BaseComponent.W*0.25,height:BaseComponent.W*0.192,backgroundColor:'rgba(14,14,14,0.5)',justifyContent:'center',alignItems:'center'}}>
                          <Text style={{color:'white'}}>{this.state.idCardImageBackAttachment?'已上传':'未上传'}</Text>
                      </View> 
                  </ImageBackground>
                  </View>
                  </TouchableWithoutFeedback>
                </View>

                </View>
                
            </View>
            </KeyboardAwareScrollView>  
            </ScrollView>
        </View>
        )
    }
}
const styles=StyleSheet.create({
   recognition:{
        width:'100%',
        height:BaseComponent.W*0.45,
        backgroundColor:'#F0F0F0'
   },
   ImageBackground:{
    width:BaseComponent.W*0.938,
    height:BaseComponent.W*0.339,
    marginTop:BaseComponent.W*0.032,
    justifyContent:'center',
    alignItems:'center'
   },
   shibie:{
    width:BaseComponent.W*0.48,
    height:BaseComponent.W*0.08,
    // backgroundColor:'#fff',
    borderRadius:BaseComponent.W*0.04,
    marginTop:BaseComponent.W*0.027,
    justifyContent:'center',
    alignItems:'center'
   },
   write:{
    width:'100%',
    flexDirection:'column'
   },
   write1:{
    width:BaseComponent.W*0.968,
    height:BaseComponent.W*0.12,
    alignItems:'center',
    flexDirection:'row',
    marginLeft:BaseComponent.W*0.032
   },
   modal:{
    width:'100%',
    height:'100%',
    backgroundColor:'rgba(14,14,14,0.5)',
    justifyContent:'flex-end'
   }
});