/**
 * @name LogisticsTracking.js
 * @auhor 程浩
 * @date 2018.8.21
 * @desc 物流追踪界面
 */
import React,{Component} from 'react';
import {Text,View,StyleSheet,Image,TouchableWithoutFeedback,ScrollView,FlatList,Modal,ActivityIndicator,Clipboard} from 'react-native';
// 导入工具函数库
import T from "../../lib/Tools";
// 导入公共样式
import St from "../../lib/Style";
// 导入图片资源库
import Imgs from "../../lib/Img";
import BaseComponent from '../BaseComponent';

// 转菊花
import Spinner from 'react-native-loading-spinner-overlay';

// Tools实例
let ti = T.getInstance();


let style = StyleSheet.create({
  wrapper:{
    backgroundColor:"white",
    flex:1,
    flexDirection:"column",
    alignItems:"stretch",
    justifyContent:"flex-start",
    position:"relative",
  },
  statusBar:{
    backgroundColor:"rgb(255,255,255)",
    height:ti.select({ios:16,android:22}),
    position:"relative",
    zIndex:1,
    opacity:1,
  },
  navBar:{
    backgroundColor:"rgb(255,255,255)",
    height:"7%",
    position:"relative",
    zIndex:1,
    opacity:1,
    borderBottomWidth:1,
    borderBottomColor:"rgb(241,241,241)"
  },
});


export default class LogisticsTracking extends BaseComponent{
  constructor(props){
      super(props);
      this.state={
        num:[1,1,1,1,1,1,1,1,1,1],//只是为了画虚线循环而已;
        data:{logisticCompany:{name:""},logisticNo:"",logisticFlowLines:[]},
        visible:false,//转菊花
        netError:false,//true表示网络错误
        logisData:true,//true代表有物流信息
        orderNo:"",
        canCopyOrderNo:false,//有快递单号就复制快递单号,true表示有
        isSelfTake:false,//是否是自提,true表示是自提
        index:-1,//判断快递单号在第几
      };
  }

  componentDidMount(){
      var thiz=this;
      thiz.getLogisticsTracking();
  };
  //获取物流数据
  getLogisticsTracking=()=>{
    var thiz=this;
    var id=thiz.params.id;
    console.log('-------------------------------------------------------id-----------------------------',id);
    thiz.setState({visible:true});
    thiz.request("orderPackage/getLogisticDetail",{
      method:"POST",
      data:{id:id},
      success:function(ret){
          if(!ret.data)
          {
            thiz.setState({visible:false,logisData:false,isSelfTake:true});
            return ;
          }
          if(ret.data.logisticType&&ret.data.logisticType=="SELF_MENTION"){
            thiz.setState({isSelfTake:true});
          }
          thiz.setState({visible:false,data:ret.data});
      },
      error:function(err){
          thiz.setState({visible:false});
          if(err.code&&err.code==200){
          console.log("111111111111111111111111");
                thiz.setState({netError:true});
            } 
      }
    });
  };
  //复制到剪贴板
  copyClipboard=()=>{
    console.log("--------------------------------------点击了复制-----------------------------");
    Clipboard.setString(this.state.data.logisticNo);
    this.toast("复制成功");
    console.log("--------------------------------------复制成功-----------------------------");
  }

  _keyExtractor=(item,index)=>index.toString();
  _renderItem=(info)=>{
    if(info.item.context.indexOf("单号")!=-1){
      
      var orderNo = info.item.context.split('单号:')[1];
      var state=this.state;
      state.canCopyOrderNo=true;
      // state.orderNo=orderNo;
      state.index=info.index;
      this.setState(state);
      this.log("-------------------------aaaaaaaaaaaaaaaaaaaaaaaaa----------------------------",orderNo);
    }
        return (
          <View style={{width:'100%',height:info.index==this.state.data.logisticFlowLines.length-1?BaseComponent.W*60/375:BaseComponent.W*120/375,flexDirection:'row',}}>
            <View style={{width:BaseComponent.W*15/375,height:'100%',marginLeft:BaseComponent.W*27/375,alignItems:'center'}}>
                <Image style={{width:'100%',height:BaseComponent.W*15/375}} source={info.index==0?require('../../image/home/yiqianshou.png'):require('../../image/home/weiqianshou.png')}></Image>
                {
                  info.index==this.state.data.logisticFlowLines.length-1?(<View></View>):
                    (<View>
                      {
                      this.state.num.map(()=>{
                        return (
                        <View style={{width:1,height:BaseComponent.W*6/375,backgroundColor:'#CCCCCC',marginTop:BaseComponent.W*5/375}}></View>
                        )
                      })
                      }
                    </View>)
                }
            </View>
            <View style={{width:'68%',height:'100%',marginLeft:BaseComponent.W*15/375}}>
                <Text style={{fontSize:BaseComponent.W*12/375,color:'#6B6B6B'}}>{info.item.time}</Text>
                <Text style={{fontSize:BaseComponent.W*14/375,color:info.index==0?'#ba3a3e':'#121212',marginTop:5,lineHeight:20}}>{info.item.context}</Text>    
            </View>
            <TouchableWithoutFeedback onPress={()=>{
                    let NewOrderNo="";
                    if(info.item.context.indexOf("单号")!=-1){
                    	NewOrderNo = info.item.context.split('单号:')[1];
                    }
                    Clipboard.setString(NewOrderNo);
                    this.log("---------------info---------------",info);
                    this.log("-------------------------NewOrderNo------------------------",NewOrderNo);
                    this.toast("复制成功");
                }}>
                    <View style={{display:this.state.index==info.index&&this.state.canCopyOrderNo?'flex':'none',width:BaseComponent.W*50/375,height:BaseComponent.W*20/375,borderWidth:1,borderRadius:BaseComponent.W*10/375,borderColor:'#3a3a3a',
                              justifyContent:'center',alignItems:'center',position:this.state.index==info.index&&this.state.canCopyOrderNo?'absolute':'relative',right:BaseComponent.W*10/375,marginTop:BaseComponent.W*30/375}}>
                          <Text style={{fontSize:BaseComponent.W*14/375,color:'#3a3a3a',display:this.state.index==info.index&&this.state.canCopyOrderNo?'flex':'none'}}>复制</Text>      
                    </View>
            </TouchableWithoutFeedback>
          </View>
        )
  }

    render() {
      let thiz=this;
        return ( 
             <View style={style.wrapper}>
                {/*状态栏占位*/}
                <View style={{backgroundColor:"rgb(255,255,255)",
                      height:this.isIphoneX(30,BaseComponent.SH),
                      position:"relative",
                      zIndex:1,
                      opacity:1,}}></View>
                {/*顶部导航栏*/}
                <View style={style.navBar}>
                  <View style={{flex:1,flexDirection:"row",justifyContent:"flex-start"}}>

                    <TouchableWithoutFeedback onPress={()=>this.goBack()}>
                    <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>
                      <Image source={Imgs.back} style={{width:(BaseComponent.W*0.127*0.4),height:(BaseComponent.W*0.127*0.4),marginRight:BaseComponent.W*0.04}}></Image>
                    </View>
                    </TouchableWithoutFeedback>

                    <View style={{flexGrow:1,backgroundColor:"transparent",
                      marginTop:ti.select({ios:(BaseComponent.H*0.02)/2,android:(BaseComponent.H*0.016)/2}),borderRadius:ti.select({ios:(BaseComponent.H*0.05)/2,android:(BaseComponent.H*0.054)/2}),borderColor:"#0A0A0A",
                      borderWidth:0,}}>
                      <Text style={{color:"#0A0A0A",width:"100%",textAlign:"center",marginTop:ti.select({ios:"4.5%",android:"1.6%"}),fontSize:17}}>{this.params.title}</Text>
                    </View>
                    <TouchableWithoutFeedback onPress={()=>{
                      // 跳转客服
                      thiz.log("--------toService--------","toService");
                      thiz.goPage("Service",{title:"客服"});
                    }}>
                    <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>
                      <Image source={require('../../image/home/kefu.png')} style={{width:(BaseComponent.W*0.127*0.5),height:(BaseComponent.W*0.127*0.5),display:"flex"}}></Image>
                    </View>
                    </TouchableWithoutFeedback>

                  </View>
                </View>

               {/*转菊花*/}
                <Modal
                  visible={this.state.visible}
                  onRequestClose={()=>{this.setState({visible:false})}}
                  transparent={true}
                  animationType={"fade"}>
                  <View style={{backgroundColor:"rgba(0,0,0,0.4)",width:"100%",width:"100%",flex:1,flexDirection:"column",alignItems:"center",justifyContent:"center",position:"relative"}}>
                    <View>
                          <ActivityIndicator size="large" color='white'/>
                    </View>
                  </View>
                </Modal>

              {/*物流单号物流公司*/}
              <View style={{width:'100%',height:BaseComponent.W*72/375,backgroundColor:'#f4e4c2',flexDirection:'row',alignItems:'center'}}>
                  <Image style={{width:BaseComponent.W*30/375,height:BaseComponent.W*25/375,marginLeft:BaseComponent.W*15/375}} source={require('../../image/home/wuliu.png')}></Image>
                  <View style={{width:BaseComponent.W*250/375,height:'100%'}}>
                      <Text style={{fontSize:BaseComponent.W*14/375,color:'#6B6B6B',marginLeft:BaseComponent.W*13/375,marginTop:BaseComponent.W*16/375}}>物流公司:&nbsp;&nbsp;<Text style={{fontSize:BaseComponent.W*14/375,color:'#121212'}}>{this.state.isSelfTake?"无":this.state.data.logisticCompany.name}</Text></Text>
                      <Text style={{fontSize:BaseComponent.W*14/375,color:'#6B6B6B',marginLeft:BaseComponent.W*13/375,marginTop:BaseComponent.W*7/375}}>物流单号:&nbsp;&nbsp;<Text style={{fontSize:BaseComponent.W*14/375,color:'#121212'}}>{this.state.isSelfTake?"无":this.state.data.logisticNo}</Text></Text>  
                  </View>
                  <TouchableWithoutFeedback onPress={()=>{
                    this.copyClipboard();
                  }}>  
                  <View style={{width:BaseComponent.W*50/375,height:BaseComponent.W*20/375,borderWidth:1,borderRadius:BaseComponent.W*10/375,borderColor:'#3a3a3a',
                            justifyContent:'center',alignItems:'center',marginLeft:BaseComponent.W*10/375,position:this.state.isSelfTake?'relative':'absolute',right:BaseComponent.W*10/375,display:this.state.isSelfTake?'none':'flex'}}>
                        <Text style={{fontSize:BaseComponent.W*14/375,color:'#3a3a3a',display:this.state.isSelfTake?'none':'flex'}}>复制</Text>      
                  </View>
                  </TouchableWithoutFeedback>
              </View>
              <View style={{width:'100%',height:BaseComponent.W*20/375}}></View>

              {
                thiz.state.netError?(
                    <View style={{width:BaseComponent.W,height:BaseComponent.H*0.7,justifyContent:'center',alignItems:'center'}}>
                      
                      <Image style={{width:BaseComponent.W*180/375,height:BaseComponent.W*180/375,borderRadius:BaseComponent.W*90/375}} source={require('../../image/mine/neterror.png')}></Image>
                      
                      <TouchableWithoutFeedback onPress={()=>{
                        thiz.setState({netError:false});
                        
                        thiz.getLogisticsTracking();
                      }}>
                      <View style={{width:BaseComponent.W*70/375,height:BaseComponent.W*30/375,borderRadius:BaseComponent.W*15/375,borderWidth:0.5,borderColor:'#080808',
                                justifyContent:'center',alignItems:'center',marginTop:BaseComponent.W*15/375}}>
                            <Text style={{fontSize:BaseComponent.W*15/375,color:'#252525'}}>刷新</Text>          
                      </View>
                      </TouchableWithoutFeedback>
                    </View>):(thiz.state.logisData?(
                    <View style={{flex:1,marginBottom:BaseComponent.W*10/375,}}>
                      <FlatList data={this.state.data.logisticFlowLines}
                                keyExtractor={this._keyExtractor}
                                renderItem={this._renderItem}
                                showsVerticalScrollIndicator={false}
                                inverted={false}
                      />
                    </View>):(
                      <View style={{width:BaseComponent.W,height:BaseComponent.H*0.7,justifyContent:'center',alignItems:'center'}}>
                          <Image style={{width:BaseComponent.W*180/375,height:BaseComponent.W*180/375,borderRadius:BaseComponent.W*90/375}} source={require('../../image/mine/nologis.png')}></Image>
                      </View>))
              }
            
             

              
          </View>      
        )
    }
}
