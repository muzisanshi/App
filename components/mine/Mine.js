/**
 * @name Mine.js
 * @auhor 程浩
 * @date 2018.8.16
 * @desc 我的页面
 */
import React,{Component}from "react";
import {ActivityIndicator,Text, View, Image, StyleSheet,ImageBackground,TouchableWithoutFeedback,Modal,ScrollView} from 'react-native';

import Wodedingdan from './Wodedingdan';
import BaseComponent from '../BaseComponent';
import shouhuoAddress from './shouhuoAddress';
import Login from '../login/Login';
import Personal from './Personal';
import Set from './Set';
import DeliverGoods from '../cart/DeliverGoods';
import LogisticsTracking from './LogisticsTracking';
import ShouhouType from './ShouhouType';
// 导入工具函数库
import T from "../../lib/Tools";
// Tools实例
let ti = T.getInstance();
// 导入公共样式
import St from "../../lib/Style";
// 导入图片资源库
import Imgs from "../../lib/Img";
import MyImage from "../../lib/MyImage";

// 转菊花
import Spinner from 'react-native-loading-spinner-overlay';

import ApplyShouhou from './ApplyShouhou';
// 测试保存base64图片
import base64Save from "../../lib/SaveBase64ImageToCameraRoll";
import config from '../../config';

export default class Mine extends BaseComponent{
    constructor(props){
        super(props);
        this.state={
            visible:false,
            userInfo:{},
            //获取待付款，待发货，待收货，待售后的数量
            OrderInfoStatistics:{
                waitForPayTotal:0,
                waitForDeliveryTotal:0,
                waitForReceivedTotal:0
            },
            totalAmount:"0.00",//获取本月的总订单额
            qrcode:"",
            isShowQrcode:false,
            webConfig:null,
        }
    }
    saveImg(data){
      let thiz = this;
      // 把base64转换为图片并保存
      if(base64Save&&data){
        if(BaseComponent.OS=="android"){
          thiz.T.requestAndroidPermission("WRITE_EXTERNAL_STORAGE",function(){
            base64Save(data,function(){
              thiz.toast("保存成功");
            },function(){
              thiz.toast("保存失败");
            });
            
          });
        }else{
          base64Save(data,function(){
            thiz.toast("保存成功");
          },function(){
            thiz.toast("保存失败");
          });
        }
      }

    }
    loadData(){
        let thiz = this;
        thiz.log("--------mine_loadData--------","mine_loadData");
        // 延时加载数据，为从本地加载数据显示做准备
        setTimeout(function(){
            thiz.setState({visible:true});
            thiz.request("user/getInfo",{
              method:"POST",
              success:function(ret){
                thiz.setState({visible:false});
                thiz.log("--------mine_user/getInfo-------",ret);
                if(ret&&ret.respCode==="00"){
                    thiz.setState({
                        userInfo:ret.data,
                        visible:false,
                    });
                    // 缓存到本地
                    thiz.T.save("cacheUserInfo",ret.data);
                }
              },
              error:function(err){
                thiz.setState({visible:false});
                thiz.toast(err.msg);
              }
            });
        },50);
    }

    goUserDetail(){
        let thiz = this;
        thiz.log("--------goUserDetail--------","goUserDetail");
        thiz.isLogin(function(ret,err){
            // thiz.log("--------userinfo--------",ret);
            if(ret){
                thiz.navigate('Personal',{title:'个人资料编辑',userInfo:thiz.state.userInfo});
            }else{
                thiz.navigate('Login');
                // thiz.toast("请登录");
            }
        });
    }

    // 加载邀请二维码
    getQrcode(){
        let thiz = this;
        // thiz.setState({visible:true});
        thiz.request("user/genInvitationQrCode",{
          method:"POST",
          success:function(ret){
            thiz.setState({visible:false});
            thiz.log("--------user/genInvitationQrCode-------",ret);
            if(ret&&ret.respCode==="00"){
                thiz.setState({
                    qrcode:ret.data,
                    visible:false,
                });
            }
          },
          error:function(err){
            // thiz.setState({visible:false});
            thiz.toast(err.msg);
          }
        });
    }

    // 获取跳转webapp的配置
    getWebConfig(){
        let thiz = this;
        console.log("------------getWebConfig----------");
        if(!thiz.state.webConfig){
            thiz.request("systemConfig/getWebappConfig",{
              method:"POST",
              success:function(ret){
                thiz.log("--------systemConfig/getWebappConfig-------",ret);
                if(ret&&ret.respCode==="00"){
                    thiz.setState({
                        webConfig:ret.data,
                    })
                }
              },
              error:function(err){
                thiz.toast(err.msg);
              }
            });
        }
    }


    componentDidMount(){
        let thiz = this;

        thiz.getOrderInfoStatistics();
        thiz.getOrderAmountStatistics();
        // thiz.getQrcode();
        thiz.getWebConfig();

        // 先显示本地缓存的用户信息
        thiz.T.load("cacheUserInfo",function(ret,err){
            if(ret){
                thiz.setState({
                    userInfo:ret,
                });
            }
        });

        // 判断用户是否已登录
        thiz.isLogin(function(ret,err){
            if(ret){
                // 请求用户详情信息
                thiz.loadData();
                thiz.getOrderInfoStatistics();
                thiz.getOrderAmountStatistics();
                // thiz.getQrcode();
            }
        });

        thiz.listen("login_success",function(){
            thiz.log("--------mine_login_success--------","监听到登录成功消息");
            // 加载数据
            thiz.loadData();
            thiz.getOrderInfoStatistics();
            thiz.getOrderAmountStatistics();
            // thiz.getQrcode();
        });

        thiz.listen("modify_user_info_success",function(){
            thiz.log("--------modify_user_info_success--------","监听到修改头像成功消息");
            // 加载数据
            thiz.loadData();
            thiz.getOrderInfoStatistics();    
        });

        thiz.listen("reset_pwd_success",function(){
            thiz.log("--------reset_pwd_success--------","监听到重置密码成功消息");
            thiz.setState({
                userInfo:{},
            });
        });

        // 监听退出登录
        thiz.listen("quit_login",function(){
            // 先显示本地缓存的用户信息
            thiz.setState({
                userInfo:{},
                OrderInfoStatistics:{
                    waitForPayTotal:0,
                    waitForDeliveryTotal:0,
                    waitForReceivedTotal:0
                },
                totalAmount:"0.00",
            });

        });
        //监听消息确认收货
        thiz.listen("confirm_Received",function(){
            thiz.getOrderInfoStatistics();
        });
        //监听点击导航栏
        thiz.listen("click_mine",function(){
            // 判断用户是否已登录
            thiz.isLogin(function(ret,err){
                if(ret){
                    // 请求用户详情信息
                    thiz.loadData();
                    thiz.getOrderInfoStatistics();
                    thiz.getOrderAmountStatistics();
                    // thiz.getQrcode();
                    thiz.getWebConfig();
                }
            });
        });
        //监听订单支付成功消息
        thiz.listen("pay_order_success",function(){
            thiz.loadData();
            thiz.getOrderInfoStatistics();
            thiz.getOrderAmountStatistics();
        });
        //监听取消订单成功
        thiz.listen("cancel_order_success",function(){
            thiz.getOrderInfoStatistics();
            thiz.getOrderAmountStatistics();
        });
        //监听包裹确认收货
        thiz.listen("orderPackage_confirmReceived",function(){
          thiz.getOrderInfoStatistics();
        });

        thiz.listen("withdrawSuccess",function(){
            thiz.loadData();
        });

        // thiz.listen("open_all_order_list",function(){
        //     thiz.navigate("Wodedingdan",{title:"我的订单",page:0});
        // });
        
    }

    //获取本月订单额
    getOrderAmountStatistics=()=>{
      var thiz=this;
      thiz.request("order/getOrderAmountStatistics",{
        method:"POST",
        success:function(ret){
          if(ret&&ret.data){
            thiz.log("--------order/getOrderAmountStatistics--------",ret);
            thiz.setState({
              totalAmount:ret.data.totalAmount,
            })
          }
        },
        error:function(){

        }
      })
    }

    toWebView(){
        let thiz = this;
        this.goPage("Service",{title:"客服"});
    };

    //获取待付款，待发货，待收货，待售后的数量
    getOrderInfoStatistics=()=>{
        var thiz=this;
        thiz.setState({visible:true});
        thiz.request("order/getOrderInfoStatistics",{
            method:"POST",
            success:function(ret){
               if(ret.respCode=="00")
               {
                  // thiz.log("---------------------------------order/getOrderInfoStatistics------------------------",ret);
                  thiz.setState({visible:false,OrderInfoStatistics:ret.data});
               }     
            },
            error:function(err){
                thiz.setState({visible:false});
            }
        });    
    };

    render(){
        let thiz=this;
        return (
            <View style={{backgroundColor:'#EEEEEE'}}>

                    {/*转菊花*/}
                    <Modal
                      visible={this.state.visible}
                      onRequestClose={()=>{this.setState({visible:false})}}
                      transparent={true}
                      animationType={"fade"}>
                      <View style={{backgroundColor:"rgba(0,0,0,0.4)",width:"100%",width:"100%",flex:1,
                        flexDirection:"column",alignItems:"center",justifyContent:"center",position:"relative"}}>
                        <View>
                              <ActivityIndicator size="large" color='white'/>
                        </View>
                      </View>
                    </Modal>

                    {/*二维码弹出窗口*/}
                    <Modal visible={this.state.isShowQrcode}
                       transparent={true}
                       onRequestClose={() => {
                        thiz.setState({isShowQrcode:false});
                       }}>
                       <TouchableWithoutFeedback onPress={()=>{
                                    thiz.setState({isShowQrcode:false});
                                }}>
                       <View style={styles.Modal}>
                        <View style={{width:BaseComponent.W*275/375,backgroundColor:'rgba(255, 255, 255,1)',borderRadius:BaseComponent.W*20/375}}>
                            <View style={{width:'100%',height:BaseComponent.W*56/375,justifyContent:'center',alignItems:'center'}}>
                                <Text style={{fontSize:BaseComponent.W*15/375,color:'rgba(34, 34, 34, 1)'}}>
                                {thiz.state.userInfo&&thiz.state.userInfo.nickname?thiz.state.userInfo.nickname:""}的小洋匠二维码</Text>
                            </View>

                            <View style={{width:'100%',backgroundColor:'rgba(255, 255, 255,1)'}}>
                                <Image style={{width:BaseComponent.W*205/375,height:BaseComponent.W*205/375,marginLeft:BaseComponent.W*35/375}} 
                                    resizeMode="contain" source={{uri:thiz.state.qrcode}}/> 
                            </View>

                            <View style={{width:'100%',height:BaseComponent.W*49/375,flexDirection:'row'}}>
                                <TouchableWithoutFeedback onPress={()=>{

                                    if(thiz.state.qrcode){
                                        thiz.saveImg(thiz.state.qrcode);
                                    }

                                }}>
                                    <View style={{width:'100%',height:BaseComponent.W*49/375,justifyContent:'center',alignItems:'center'}}>
                                        <Text style={{fontSize:BaseComponent.W*15/375,color:'#FED584',textAlign:"center"}}>保存二维码</Text>
                                    </View>
                                </TouchableWithoutFeedback> 
                            </View>

                        </View>
                       </View>
                       </TouchableWithoutFeedback> 

                    </Modal>

                    <ScrollView>
                    <ImageBackground style={{width:'100%',height:BaseComponent.W*46/75,position:"relative"}} 
                        resizeMode='stretch' source={require('../../image/mine/ybg.jpg')}>

                        {/*用户头像等*/}
                        <View style={styles.userTop}>
                            
                            {/*用户信息*/}
                            <View style={{flexDirection:'row',width:'100%',marginTop:BaseComponent.W*65/375}}>

                                <TouchableWithoutFeedback onPress={()=>this.goUserDetail()}> 
                                    <View style={styles.usericonArea}>
                                        <Image style={styles.usericon} 
                                            source={this.state.userInfo.headImageAttachment&&this.state.userInfo.headImageAttachment.resourceFullAddress?{uri:this.state.userInfo.headImageAttachment.resourceFullAddress}:require("../../image/mine/avatar.png")}></Image>
                                    </View>
                                </TouchableWithoutFeedback>

                                <View style={{marginLeft:BaseComponent.W*6/375,width:BaseComponent.W*230/375,marginTop:ti.select({android:0,ios:3}),flexDirection:"column"}}>
                                    <View style={{flexDirection:"row"}}>
                                        <Text style={styles.username} onPress={()=>this.goUserDetail()}>{this.state.userInfo.nickname?this.state.userInfo.nickname:"登录/注册"}</Text>
                                        <View style={{marginTop:BaseComponent.W*0.065,marginLeft:10,backgroundColor:"#1F1F1F",height:18,borderRadius:9,
                                            paddingLeft:8,paddingRight:8,
                                            display:this.state.userInfo.userType&&this.state.userInfo.userType.indexOf("VIP")>=0?"flex":"none"}}>
                                            <Text style={{color:"#FF9529",fontSize:12}}>{this.state.userInfo.userType&&this.state.userInfo.userType.indexOf("VIP")>=0?this.state.userInfo.userType:""}</Text>
                                        </View>
                                    </View>
                                    <Text style={{color:"white",fontSize:BaseComponent.W*12/375,marginTop:ti.select({android:1,ios:5})}} 
                                        numberOfLines={1}>{thiz.state.userInfo.wxNo?"微信号:  "+thiz.state.userInfo.wxNo:"请填写微信号"}</Text>
                                </View>
                                
                            </View>
                            
                            <View style={{width:'100%',flexDirection:'column',alignItems:'center'}}>
                                {/*积分*/}
                                <TouchableWithoutFeedback onPress={()=>{
                                    thiz.navigate('MyIntegral',{title:'我的积分'});
                                }}>
                                    <View style={{marginTop:1,width:BaseComponent.W*280/375,height:BaseComponent.W*34/375,flexDirection:'row',justifyContent:'center',alignItems:'center',backgroundColor:'#1C1C1C',borderTopRightRadius:5,borderTopLeftRadius:5,display:'none'}}>
                                        <View style={{width:BaseComponent.W*280/375,height:BaseComponent.W*34/375,flexDirection:'row',justifyContent:'center',alignItems:'center',backgroundColor:'#313131',borderTopRightRadius:5,borderTopLeftRadius:5}}>
                                            <View  style={{marginLeft:BaseComponent.W*80/375,width:BaseComponent.W*160/375,flexDirection:'row'}}>
                                                <Text style={{fontSize:BaseComponent.W*13/375,color:'rgba(255, 208, 116, 1)'}}>积分:&nbsp;&nbsp;</Text>
                                                <Text style={{fontSize:BaseComponent.W*16/375,fontWeight:'bold',width:BaseComponent.W*100/375,color:'rgba(255, 208, 116, 1)'}} numberOfLines={1}>0</Text>
                                            </View>
                                            <Image style={{height:BaseComponent.W*0.05,width:BaseComponent.W*0.06}} source={require('../../image/home/right_yellow_arrow.png')}></Image>
                                       </View>
                                    </View>
                                </TouchableWithoutFeedback>

                               {/*每月订单额*/}
                               <TouchableWithoutFeedback onPress={()=>{
                                  thiz.navigate("OrderAmountStatistics",{title:"订单额统计"})
                               }}>
                                   <View style={{display:"none",width:BaseComponent.W*302/375,height:BaseComponent.W*46/375,flexDirection:'row',justifyContent:'center',alignItems:'center',backgroundColor:'#313131',borderTopRightRadius:5,borderTopLeftRadius:5}}>
                                        <View  style={{marginLeft:BaseComponent.W*50/375,width:BaseComponent.W*190/375,flexDirection:'row'}}>
                                            <Text style={{fontSize:BaseComponent.W*13/375,color:'rgba(255, 208, 116, 1)'}}>本月订单额(元):&nbsp;&nbsp;</Text>
                                            <Text style={{fontSize:BaseComponent.W*16/375,fontWeight:'bold',width:BaseComponent.W*100/375,color:'rgba(255, 208, 116, 1)'}} numberOfLines={1}>￥{thiz.state.totalAmount}</Text>
                                        </View>
                                        <Image style={{height:BaseComponent.W*0.05,width:BaseComponent.W*0.06}} source={require('../../image/home/right_yellow_arrow.png')}></Image>
                                   </View>
                                </TouchableWithoutFeedback>

                            </View>    
                        </View>
                       
                        

                        <View style={styles.set}>  
                        <TouchableWithoutFeedback onPress={()=>this.navigate('Set',{title:"设置",userInfo:this.state.userInfo})}>    
                            <Image style={styles.setIcon} source={require('../../image/home/mine_set.png')}></Image>
                        </TouchableWithoutFeedback>
                        </View>

                        {/*消息中心*/}
                        <TouchableWithoutFeedback onPress={()=>{
                            // this.navigate("Msgcenter",{title:"消息中心"});
                        }}>
                        <View style={[styles.msg],{opacity:0,}}>
                            <Image style={styles.setIcon} source={require('../../image/home/msg.png')}></Image>
                        </View>
                        </TouchableWithoutFeedback>

                        {/*推广二维码*/}
                        <TouchableWithoutFeedback onPress={()=>{
                            // 弹出二维码
                            thiz.isLogin(function(ret,err){
                                if(ret){
                                    thiz.setState({
                                        isShowQrcode:true,
                                    });
                                    // 调接口获取二维码
                                    thiz.getQrcode();
                                }else{
                                    thiz.navigate("Login");
                                }
                            });
                        }}>
                        <View style={{flexDirection:"row",paddingTop:6,paddingLeft:16,paddingRight:8,paddingBottom:6,
                        position:"absolute",right:0,top:BaseComponent.H*0.12,backgroundColor:"#282828",alignItems:"center",
                        borderTopLeftRadius:BaseComponent.H*0.06,borderBottomLeftRadius:BaseComponent.H*0.06,}}>
                            <Image style={{width:30,height:30,}} source={require('../../image/qrcode.jpg')}></Image>
                            <View style={{width:50,marginLeft:6,display:"none"}}>
                                <Text numberOfLines={1} style={{color:"#FCEDB4",fontSize:13,}}>推广</Text>
                                <Text numberOfLines={1} style={{color:"#FCEDB4",fontSize:13,}}>二维码</Text>
                            </View>
                        </View>
                        </TouchableWithoutFeedback>

                    </ImageBackground>


                    {/*列表*/}
                    <View style={{position:"relative",backgroundColor:"transparent",paddingLeft:BaseComponent.W*15/375,paddingRight:BaseComponent.W*15/375,top:-BaseComponent.H*23/375}}>
                        
                        <View style={{width:"100%",position:"relative",backgroundColor:"transparent",}}>

                            {/*各种币*/}
                            <View style={{flexDirection:"column",backgroundColor:"#282828",borderRadius:10,}}>

                                <View style={{flexDirection:"row",paddingTop:16,paddingBottom:16,backgroundColor:"white",borderRadius:10,}}>

                                    <TouchableWithoutFeedback onPress={()=>{

                                        thiz.getAccount(function(ret,err){
                                            if(ret){
                                                
                                                if(thiz.state.webConfig){
                                                    thiz.log("--------Coupons_buy_url--------",thiz.state.webConfig.webappOuterMainUrl+"?baseURL="+thiz.state.webConfig.apiGatewayOuterMainUrl+"&token="+(ret.userToken?ret.userToken:"")+"&cointype=buy"+"#/Coupons");
                                                    thiz.navigate('WebBrowser',{
                                                        title:'',
                                                        url:thiz.state.webConfig.webappOuterMainUrl+"?baseURL="+thiz.state.webConfig.apiGatewayOuterMainUrl+"&token="+(ret.userToken?ret.userToken:"")+"&cointype=buy"+"#/Coupons",
                                                        // url:config.webappURL+"?baseURL="+thiz.state.webConfig.apiGatewayOuterMainUrl+"&token="+(ret.userToken?ret.userToken:"")+"&cointype=buy"+"#/Coupons",
                                                        showNav:false,
                                                    });
                                                }else{
                                                    thiz.getWebConfig();
                                                }

                                            }else{
                                                thiz.navigate('Login');
                                            }

                                        });
                                    }}>
                                            <View style={{flex:1}} >
                                                <Text style={{textAlign:"center",color:"#101010",fontSize:20,}}>￥{thiz.state.userInfo&&thiz.state.userInfo.wallet?thiz.state.userInfo.wallet.allShoppingCoin.toFixed(2):"0.00"}</Text>
                                                <Text  style={{textAlign:"center",color:"#101010",marginTop:5,}}>购物币</Text>
                                            </View>
                                    </TouchableWithoutFeedback>

                                    <TouchableWithoutFeedback onPress={()=>{
                                        
                                        thiz.getAccount(function(ret,err){

                                            if(ret){

                                                if(thiz.state.webConfig){
                                                    thiz.log("--------Coupons_bonus_url--------",thiz.state.webConfig.webappOuterMainUrl+"?baseURL="+thiz.state.webConfig.apiGatewayOuterMainUrl+"&token="+(ret.userToken?ret.userToken:"")+"&cointype=bonus"+"#/Coupons");
                                                    thiz.navigate('WebBrowser',{
                                                        title:'',
                                                        url:thiz.state.webConfig.webappOuterMainUrl+"?baseURL="+thiz.state.webConfig.apiGatewayOuterMainUrl+"&token="+(ret.userToken?ret.userToken:"")+"&cointype=bonus"+"#/Coupons",
                                                        // url:config.webappURL+"?baseURL="+thiz.state.webConfig.apiGatewayOuterMainUrl+"&token="+(ret.userToken?ret.userToken:"")+"&cointype=bonus"+"#/Coupons",
                                                        showNav:false,
                                                    });
                                                }else{
                                                    thiz.getWebConfig();
                                                }

                                            }else{
                                                thiz.navigate('Login');
                                            }

                                        });

                                    }}>
                                    <View style={{flex:1,position:"relative"}}>
                                        <View style={{position:"absolute",height:"66%",width:1,backgroundColor:"#D9D9D9",left:0,top:"19%"}}></View>
                                        <Text  style={{textAlign:"center",color:"#101010",fontSize:20,}}>￥{thiz.state.userInfo&&thiz.state.userInfo.wallet?thiz.state.userInfo.wallet.allBonusCoin.toFixed(2):"0.00"}</Text>
                                        <Text  style={{textAlign:"center",color:"#101010",marginTop:5,}}>奖金币</Text>
                                        <View style={{position:"absolute",height:"66%",width:1,backgroundColor:"#D9D9D9",right:0,top:"19%"}}></View>
                                    </View>
                                    </TouchableWithoutFeedback>

                                    <TouchableWithoutFeedback onPress={()=>{

                                        thiz.getAccount(function(ret,err){

                                            if(ret){

                                                if(thiz.state.webConfig){
                                                    thiz.log("--------Coupons_young_url--------",thiz.state.webConfig.webappOuterMainUrl+"?baseURL="+thiz.state.webConfig.apiGatewayOuterMainUrl+"&token="+(ret.userToken?ret.userToken:"")+"&cointype=young"+"#/Coupons");
                                                    thiz.navigate('WebBrowser',{
                                                        title:'',
                                                        url:thiz.state.webConfig.webappOuterMainUrl+"?baseURL="+thiz.state.webConfig.apiGatewayOuterMainUrl+"&token="+(ret.userToken?ret.userToken:"")+"&cointype=young"+"#/Coupons",
                                                        // url:config.webappURL+"?baseURL="+thiz.state.webConfig.apiGatewayOuterMainUrl+"&token="+(ret.userToken?ret.userToken:"")+"&cointype=young"+"#/Coupons",
                                                        showNav:false,
                                                    });
                                                }else{
                                                    thiz.getWebConfig();
                                                }

                                            }else{
                                                thiz.navigate('Login');
                                            }

                                        });

                                    }}>
                                    <View style={{flex:1,}}>
                                        <Text  style={{textAlign:"center",color:"#101010",fontSize:20,}}>￥{thiz.state.userInfo&&thiz.state.userInfo.wallet?thiz.state.userInfo.wallet.allYoungCoin.toFixed(2):"0.00"}</Text>
                                        <Text  style={{textAlign:"center",color:"#101010",marginTop:5,}}>洋匠币</Text>
                                    </View>
                                    </TouchableWithoutFeedback>

                                </View>
                                {/*订单额*/}
                                <TouchableWithoutFeedback onPress={()=>{
                                    thiz.navigate("OrderAmountStatistics",{title:"订单额统计"});
                                }}>
                                    <View style={{paddingTop:10,paddingBottom:10,}}>
                                        <Text style={{textAlign:"center",color:"#FDEAC1",fontSize:16,}}>本月订单额(元)：<Text style={{color:"#FDEAC1",fontSize:BaseComponent.W*20/375}}>￥{thiz.state.totalAmount?parseFloat(thiz.state.totalAmount).toFixed(2):"0.00"}</Text></Text>
                                    </View>
                                </TouchableWithoutFeedback>
                            </View>

                            {/*订单*/}
                            <View style={{backgroundColor:"white",borderRadius:10,marginTop:20,paddingTop:10,}}>

                                {/*订单title*/}
                                <View style={styles.MineDingdan}>
                                    <Text style={styles.MineDingdanWord}>我的订单</Text>
                                    <Text style={styles.LookMineDingdan} onPress={()=>{
                                        // 先判断是否需要登录
                                        thiz.isLogin(function(ret,err){
                                                thiz.log("--------my_order--------",ret);
                                              if(ret){
                                                  thiz.navigate('Wodedingdan',{title:"我的订单",page:0});
                                              }else{
                                                  thiz.navigate('Login');
                                              }
                                        });
                                        }
                                        }>查看全部</Text>
                                    <Image style={[styles.back,{marginTop:ti.select({ios:-2,android:0})}]} source={require('../../image/home/youjiantou.png')}></Image>
                                </View>
                                
                                <View style={{width:'100%',height:1,backgroundColor:'#F6F6F6'}}></View>

                                {/*订单区域*/}
                                <View style={styles.shouhuoArea}>

                                    {/*待付款订单*/}
                                    <TouchableWithoutFeedback onPress={()=>{
                                        // 先判断是否需要登录
                                        thiz.isLogin(function(ret,err){
                                            thiz.log("--------daifukuan_order--------",ret);
                                              if(ret){
                                                  thiz.navigate('Wodedingdan',{title:"我的订单",page:1});
                                              }else{
                                                  thiz.navigate('Login');
                                              }
                                        });
                                    }
                                    }>
                                       <View style={styles.shouhuoItem}>
                                                <Image style={{width:BaseComponent.W*0.06,height:BaseComponent.W*0.06}} source={require('../../image/home/mine_daifukuan.png')}></Image>   
                                                <Text style={{color:'#232326',fontSize:BaseComponent.W*12/375,marginTop:BaseComponent.W*9/375}}>待付款</Text> 
                                                <View style={{paddingLeft:this.state.OrderInfoStatistics.waitForPayTotal>=100?BaseComponent.W*3/375:0,paddingRight:this.state.OrderInfoStatistics.waitForPayTotal>=100?BaseComponent.W*3/375:0,maxWidth:BaseComponent.W*30/375,minWidth:BaseComponent.W*12.5/375,height:BaseComponent.W*12.5/375,backgroundColor:"#FF3B30",position:this.state.OrderInfoStatistics.waitForPayTotal==0?'relative':'absolute',top:0,display:this.state.OrderInfoStatistics.waitForPayTotal==0?'none':'flex',
                                                            borderRadius:BaseComponent.W*6.25/375,left:BaseComponent.W*52.5/375,marginTop:BaseComponent.W*10/375,justifyContent:'center',alignItems:'center'}}>
                                                        <Text style={{fontSize:BaseComponent.W*7/375,color:'#ffffff'}}>{this.state.OrderInfoStatistics.waitForPayTotal}</Text>
                                                </View> 
                                       </View>
                                    </TouchableWithoutFeedback>

                                    {/*待发货订单*/}
                                    <TouchableWithoutFeedback onPress={()=>{
                                            // 先判断是否需要登录
                                        thiz.isLogin(function(ret,err){
                                              if(ret){
                                                  thiz.navigate('Wodedingdan',{title:"我的订单",page:2});
                                              }else{
                                                  thiz.navigate('Login');
                                              }
                                        });
                                            
                                        }}>
                                       <View style={styles.shouhuoItem}>
                                                <Image style={{width:BaseComponent.W*0.06,height:BaseComponent.W*0.06}} source={require('../../image/home/mine_daifahuo.png')}></Image>   
                                                <Text style={{color:'#232326',fontSize:BaseComponent.W*12/375,marginTop:BaseComponent.W*9/375}}>待发货</Text>
                                                <View style={{paddingLeft:this.state.OrderInfoStatistics.waitForDeliveryTotal>=100?BaseComponent.W*3/375:0,paddingRight:this.state.OrderInfoStatistics.waitForDeliveryTotal>=100?BaseComponent.W*3/375:0,maxWidth:BaseComponent.W*30/375,minWidth:BaseComponent.W*12.5/375,height:BaseComponent.W*12.5/375,backgroundColor:"#FF3B30",position:this.state.OrderInfoStatistics.waitForDeliveryTotal==0?'relative':'absolute',top:0,display:this.state.OrderInfoStatistics.waitForDeliveryTotal==0?'none':'flex',
                                                            borderRadius:BaseComponent.W*6.25/375,left:BaseComponent.W*52.5/375,marginTop:BaseComponent.W*10/375,justifyContent:'center',alignItems:'center'}}>
                                                        <Text style={{fontSize:BaseComponent.W*7/375,color:'#ffffff'}}>{this.state.OrderInfoStatistics.waitForDeliveryTotal}</Text>
                                                </View> 
                                       </View>
                                    </TouchableWithoutFeedback>

                                    {/*待收货订单*/}
                                    <TouchableWithoutFeedback onPress={()=>{
                                            // 先判断是否需要登录
                                        thiz.isLogin(function(ret,err){
                                              if(ret){
                                                  thiz.navigate('Wodedingdan',{title:"我的订单",page:3});
                                              }else{
                                                  thiz.navigate('Login');
                                              }
                                        });
                                            
                                        }}>
                                       <View style={styles.shouhuoItem}>
                                                <Image style={{width:BaseComponent.W*0.06,height:BaseComponent.W*0.06}} source={require('../../image/home/mine_daishouhuo.png')}></Image>   
                                                <Text style={{color:'#232326',fontSize:BaseComponent.W*12/375,marginTop:BaseComponent.W*9/375}}>待收货</Text>
                                                <View style={{paddingLeft:this.state.OrderInfoStatistics.waitForReceivedTotal>=100?BaseComponent.W*3/375:0,paddingRight:this.state.OrderInfoStatistics.waitForReceivedTotal>=100?BaseComponent.W*3/375:0,maxWidth:BaseComponent.W*30/375,minWidth:BaseComponent.W*12.5/375,height:BaseComponent.W*12.5/375,backgroundColor:"#FF3B30",position:this.state.OrderInfoStatistics.waitForReceivedTotal==0?'relative':'absolute',top:0,display:this.state.OrderInfoStatistics.waitForReceivedTotal==0?'none':'flex',
                                                            borderRadius:BaseComponent.W*6.25/375,left:BaseComponent.W*52.5/375,marginTop:BaseComponent.W*10/375,justifyContent:'center',alignItems:'center'}}>
                                                        <Text style={{fontSize:BaseComponent.W*7/375,color:'#ffffff'}}>{this.state.OrderInfoStatistics.waitForReceivedTotal}</Text>
                                                </View>  
                                       </View>
                                    </TouchableWithoutFeedback>   

                                    {/*退款/售后订单*/}
                                    <TouchableWithoutFeedback onPress={()=>{
                                        // 先判断是否需要登录
                                        thiz.isLogin(function(ret,err){
                                              if(ret){
                                                  thiz.navigate('Tuihuotuikuan',{title:"退货退款"});
                                              }else{
                                                  thiz.navigate('Login');
                                              }
                                        });
                                        
                                       }}> 
                                       <View style={styles.shouhuoItem}>
                                                <Image style={{width:BaseComponent.W*0.06,height:BaseComponent.W*0.06}} source={require('../../image/home/mine_kefu.png')}></Image>   
                                                <Text style={{color:'#232326',fontSize:BaseComponent.W*12/375,marginTop:BaseComponent.W*9/375}}>退款/售后</Text> 
                                       </View>
                                    </TouchableWithoutFeedback>
                                </View>
                            </View>

                            {/*地址管理*/}
                            <View style={{backgroundColor:"white",borderRadius:10,marginTop:12,}}>
                                <TouchableWithoutFeedback onPress={()=>{
                                    // 先判断是否需要登录
                                    thiz.isLogin(function(ret,err){
                                          if(ret){
                                              thiz.navigate('shouhuoAddress',{title:'收货地址',isBack:false});
                                          }else{
                                              thiz.navigate('Login');
                                          }
                                    });
                                }}>
                                <View style={styles.RunAdress}>
                                    <Image style={{width:BaseComponent.W*0.06,height:BaseComponent.W*0.06,marginLeft:BaseComponent.W*0.046}} source={require('../../image/home/mine_dizhiguanli.png')}></Image>
                                    <Text style={{
                                        fontSize:BaseComponent.W*0.04,
                                        marginLeft:BaseComponent.W*0.038,
                                        color:'#232326'
                                    }}>地址管理</Text>
                                    <Image style={styles.RunAdressback} source={require('../../image/home/youjiantou.png')}></Image>
                                </View>
                                </TouchableWithoutFeedback>
                            </View>

                            <View style={{marginTop:12,borderRadius:10,backgroundColor:"white"}}>

                                {/*会员*/}
                                <View style={{backgroundColor:"transparent",}}>
                                    <TouchableWithoutFeedback onPress={()=>{
                                        // 先判断是否需要登录
                                        thiz.isLogin(function(ret,err){
                                              if(ret){
                                                  thiz.navigate('Member',{title:'',isBack:false});
                                              }else{
                                                  thiz.navigate('Login');
                                              }
                                        });
                                    }}>
                                    <View style={styles.RunAdress}>
                                        <Image style={styles.RunAdressIcon} source={require('../../image/mine/h1.png')}></Image>
                                        <Text style={styles.RunAdressWord }>会员</Text>
                                        <Image style={styles.RunAdressback} source={require('../../image/home/youjiantou.png')}></Image>
                                    </View>
                                    </TouchableWithoutFeedback>
                                </View>

                                <View style={{width:'100%',height:1,backgroundColor:'#F6F6F6'}}></View>

                                {/*银行卡*/}
                                <View style={{backgroundColor:"transparent",}}>
                                    <TouchableWithoutFeedback onPress={()=>{
                                        // 先判断是否需要登录
                                        // thiz.isLogin(function(ret,err){
                                        //       if(ret){

                                        //            thiz.navigate('WebBrowser',{title:'',url:config.webappURL+"?account="+thiz.getAccount()?});
                                        //       }else{
                                        //            thiz.navigate('Login');
                                        //       }
                                        // });
                                        thiz.getAccount(function(ret,err){
                                            if(ret){
                                                
                                                if(thiz.state.webConfig){
                                                    thiz.log("--------MyBankCard_url--------",thiz.state.webConfig.webappOuterMainUrl+"?baseURL="+thiz.state.webConfig.apiGatewayOuterMainUrl+"&token="+(ret.userToken?ret.userToken:"")+"#/MyBankCard");
                                                    thiz.navigate('WebBrowser',{
                                                        title:'',
                                                        url:thiz.state.webConfig.webappOuterMainUrl+"?baseURL="+thiz.state.webConfig.apiGatewayOuterMainUrl+"&token="+(ret.userToken?ret.userToken:"")+"#/MyBankCard",
                                                        // url:config.webappURL+"?baseURL="+thiz.state.webConfig.apiGatewayOuterMainUrl+"&token="+(ret.userToken?ret.userToken:"")+"#/MyBankCard",
                                                        showNav:false,
                                                    });
                                                }else{
                                                    thiz.getWebConfig();
                                                }

                                            }else{
                                                thiz.navigate('Login');
                                            }
                                        });
                                        
                                    }}>
                                    <View style={styles.RunAdress}>
                                        <Image style={styles.RunAdressIcon} source={require('../../image/mine/h3.png')}></Image>
                                        <Text style={styles.RunAdressWord }>银行卡</Text>
                                        <Image style={styles.RunAdressback} source={require('../../image/home/youjiantou.png')}></Image>
                                    </View>
                                    </TouchableWithoutFeedback>
                                </View>

                                <View style={{width:'100%',height:1,backgroundColor:'#F6F6F6'}}></View>

                                {/*推广中心*/}
                                <View style={{backgroundColor:"transparent",}}>
                                    <TouchableWithoutFeedback onPress={()=>{

                                        // 先判断是否需要登录
                                        thiz.isLogin(function(ret,err){
                                              if(ret){

                                                if(thiz.state.webConfig){
                                                    thiz.log("--------webConfig--------",thiz.state.webConfig);
                                                    thiz.log("--------Popularize_url--------",config.webappURL+"?baseURL="+thiz.state.webConfig.apiGatewayOuterMainUrl+"&token="+(ret.userToken?ret.userToken:"")+"#/Popularize");
                                                    thiz.navigate('WebBrowser',{
                                                        title:'',
                                                        // url:thiz.state.webConfig.webappOuterMainUrl+"?baseURL="+thiz.state.webConfig.apiGatewayOuterMainUrl+"&token="+(ret.userToken?ret.userToken:"")+"#/Popularize",
                                                        url:config.webappURL+"?baseURL="+thiz.state.webConfig.apiGatewayOuterMainUrl+"&token="+(ret.userToken?ret.userToken:"")+"#/Popularize",
                                                        showNav:false,
                                                    });
                                                }else{
                                                    thiz.getWebConfig();
                                                }

                                              }else{
                                                  thiz.navigate('Login');
                                              }
                                        });

                                    }}>
                                    <View style={styles.RunAdress}>
                                        <Image style={styles.RunAdressIcon} source={require('../../image/mine/h2.png')}></Image>
                                        <Text style={styles.RunAdressWord }>推广中心</Text>
                                        <Image style={styles.RunAdressback} source={require('../../image/home/youjiantou.png')}></Image>
                                    </View>
                                    </TouchableWithoutFeedback>
                                </View>
                            </View>

                        </View>

                    </View>


                    {/*暂时弃用*/}
                    <View style={{display:"none"}}>
                        <TouchableWithoutFeedback onPress={()=>{
                            thiz.navigate('PromotionCode',{title:'邀请码'});
                        }}>
                            <View style={styles.RunAdress}>
                                <Image style={styles.RunAdressIcon} source={require('../../image/home/invitecode.png')}></Image>
                                <Text style={styles.RunAdressWord }>推广码</Text>
                                <Image style={styles.RunAdressback} source={require('../../image/home/youjiantou.png')}></Image>
                                <View style={{width:BaseComponent.W*328/375,height:0.5,marginLeft:BaseComponent.W*47/375,backgroundColor:'#E1E1E1',position:'absolute',bottom:0}}></View>
                            </View>
                        </TouchableWithoutFeedback>    

                        <TouchableWithoutFeedback onPress={()=>{
                          thiz.navigate('InvitePeople',{title:"邀请的人"});
                        }}>
                            <View style={styles.RunAdress}>
                                <Image style={styles.RunAdressIcon} source={require('../../image/home/invitepeople.png')}></Image>
                                <Text style={styles.RunAdressWord }>邀请的人</Text>
                                <Image style={styles.RunAdressback} source={require('../../image/home/youjiantou.png')}></Image>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                

                </ScrollView>  

                
            </View>
        )
    }
}


const styles = StyleSheet.create({
    Modal:{
        flex:1,
        backgroundColor:'rgba(14,14,14,0.5)',
        justifyContent:'center',alignItems:'center',
        position:"relative",
        zIndex:10000,
    },
    userTop:{
        height:BaseComponent.W*46/75,
        width:'100%',
        justifyContent:'space-between',
        alignItems:'center',
    },

    usericonArea:{
        width:BaseComponent.W*0.18,
        height:BaseComponent.W*0.18,
        marginLeft:BaseComponent.W*15/375,
        borderRadius:BaseComponent.W*0.09,
        // borderWidth:BaseComponent.W*0.01,
        borderWidth:BaseComponent.W*0,
        borderColor:'#FEE5B8',
        justifyContent:'center',
        alignItems:'center'
    },
    usericon:{
        width:BaseComponent.W*0.16,
        height:BaseComponent.W*0.16,
        borderRadius:BaseComponent.W*0.08,
    },

    set:{
        position:'absolute',
        right:0,
        width:BaseComponent.W*0.056,
        height:BaseComponent.W*0.056,
        marginTop:BaseComponent.H*0.046,
        marginRight:BaseComponent.W*0.03
    },
    msg:{
        position:'absolute',
        right:0,
        width:BaseComponent.W*0.056,
        height:BaseComponent.W*0.056,
        marginTop:BaseComponent.H*0.046,
        marginRight:BaseComponent.W*0.125
    },
    setIcon:{
        width:BaseComponent.W*0.07,
        height:BaseComponent.W*0.07
    },
    username:{
        color:'white',
        marginTop:BaseComponent.W*0.053,
        fontSize:BaseComponent.W*20/375,
    },
    MineDingdan:{
        width:'100%',
        height:30,
        // marginTop:BaseComponent.W*0.053,
        marginLeft:BaseComponent.W*0.029,
        flexDirection:'row',
        
    },
    MineDingdanWord:{
        fontSize:BaseComponent.W*0.04,
        color:'#0D0D0D'
    },
    LookMineDingdan:{
        fontSize:BaseComponent.W*12/375,
        position:'absolute',
        right:0,
        marginRight:BaseComponent.W*0.1,
        marginTop:BaseComponent.W*0,
        color:'#909090'
    },
    back:{
        height:BaseComponent.W*0.05,
        width:BaseComponent.W*0.06,
        position:'absolute',
        right:0,
        marginRight:BaseComponent.W*0.04,
    },
    shouhuoArea:{
        width:'100%',
        height:BaseComponent.W*70/375,
        flexDirection:'row',
        justifyContent:'space-around',
        
    },
    shouhuoItem:{
        flex:1,
        flexDirection:'column',
        justifyContent:'center',
        alignItems:'center',
    },
    RunAdress:{
        height:BaseComponent.W*0.128,
        width:'100%',
        flexDirection:'row',
        alignItems:'center',
    },
    RunAdressIcon:{
        width:BaseComponent.W*0.08,
        height:BaseComponent.W*0.08,
        marginLeft:BaseComponent.W*0.035
    },
    RunAdressWord:{
        fontSize:BaseComponent.W*0.04,
        marginLeft:BaseComponent.W*0.032,
        color:'#232326'
    },
    RunAdressback:{
        height:BaseComponent.W*0.06,
        width:BaseComponent.W*0.06,
        position:'absolute',
        right:0,
        marginRight:BaseComponent.W*0.02
    },
});