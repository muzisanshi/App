/**
 * @name MinePhoneNo.js
 * @auhor 程浩
 * @date 2018.10.8
 * @desc 我的手机号
 */
import React,{Component} from 'react';
import {Text,View,StyleSheet,Image,TouchableWithoutFeedback,ScrollView,
ImageBackground,TextInput,StatusBar,Modal,FlatList,Picker} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
// 导入工具函数库
import T from "../../lib/Tools";
// 导入公共样式
import St from "../../lib/Style";
// 导入图片资源库
import Imgs from "../../lib/Img";
import BaseComponent from '../BaseComponent';




// Tools实例
let ti = T.getInstance();

let style = StyleSheet.create({
  wrapper:{
    backgroundColor:"white",
    flex:1,
    flexDirection:"column",
    alignItems:"stretch",
    justifyContent:"flex-start",
    position:"relative",
  },
  statusBar:{
    backgroundColor:"rgb(255,255,255)",
    height:ti.select({ios:16,android:22}),
    position:"relative",
    zIndex:1,
    opacity:1,
  },
  navBar:{
    backgroundColor:"rgb(255,255,255)",
    height:"7%",
    position:"relative",
    zIndex:1,
    opacity:1,
    borderBottomWidth:1,
    borderBottomColor:"rgb(241,241,241)"
  },
});
export default class Msgcenter extends BaseComponent{
constructor(props){
    super(props);
   
}
  render(){
    this.log("---------------------------userinfo------------------",this.params.userInfo);
    return (
      <View style={style.wrapper}>   
                {/*状态栏占位*/}
                <View style={{ backgroundColor:"rgba(255,255,255)",
                      height:ti.select({ios:16,android:StatusBar.currentHeight}),
                      position:"relative",
                      zIndex:1,
                      opacity:1,}}></View>
                {/*顶部导航栏*/}
              <View style={style.navBar}>
                <View style={{flex:1,flexDirection:"row",justifyContent:"flex-start"}}>

                    <TouchableWithoutFeedback onPress={()=>this.goBack()}>
                    <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>
                      <Image source={Imgs.back} style={{width:(BaseComponent.W*0.127*0.4),height:(BaseComponent.W*0.127*0.4),marginTop:ti.select({ios:BaseComponent.W*0.02,android:0}),marginRight:BaseComponent.W*0.04}}></Image>
                    </View>
                    </TouchableWithoutFeedback>

                    <View style={{flexGrow:1,backgroundColor:"transparent",
                      marginTop:ti.select({ios:(BaseComponent.H*0.02)/2,android:(BaseComponent.H*0.016)/2}),borderRadius:ti.select({ios:(BaseComponent.H*0.05)/2,android:(BaseComponent.H*0.054)/2}),borderColor:"#0A0A0A",
                      borderWidth:0,}}>
                      <Text style={{color:"#0A0A0A",width:"100%",textAlign:"center",marginTop:ti.select({ios:"4%",android:"1.6%"}),fontSize:BaseComponent.W*17/375,marginLeft:'4.3%'}}>{this.params.title}</Text>
                    </View>
                    <View style={{width:"14%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center",marginRight:BaseComponent.W*5/375}}>
                        
                    </View>
                </View>
              </View>

            <View style={{flex:1,backgroundColor:'#F0F0F0',alignItems:'center'}}>
                  <Text style={{fontSize:BaseComponent.W*13/375,color:'#333333',marginTop:BaseComponent.W*32/375}}>你可以使用该手机号直接登录小洋匠</Text>
                  <View style={{width:'100%',height:BaseComponent.W*40/375,flexDirection:'row',marginTop:BaseComponent.W*26/375,justifyContent:'center',alignItems:'center'}}>
                        <Text style={{fontSize:BaseComponent.W*15/375,color:'#333333'}}>已绑定:</Text>
                        <Text style={{fontSize:BaseComponent.W*24/375,color:'#333333',marginLeft:BaseComponent.W*10/375}}>{this.params.userInfo.phoneNo}</Text>
                  </View>
                  <View style={{display:"none",width:BaseComponent.W*340/375,height:BaseComponent.W*43/375,borderRadius:BaseComponent.W*21.5/375,backgroundColor:'#FDD17A',justifyContent:'center',alignItems:'center',marginTop:BaseComponent.W*27/375}}>
                        <Text style={{fontSize:BaseComponent.W*16/375,color:'#333333'}}>更改手机号码</Text>
                  </View>
                  <TouchableWithoutFeedback onPress={()=>this.navigate('ForgetPwd',{title:'更改密码'})}>
                  <View style={{width:BaseComponent.W*340/375,height:BaseComponent.W*43/375,borderRadius:BaseComponent.W*21.5/375,borderColor:'#FDD17A',justifyContent:'center',alignItems:'center',marginTop:BaseComponent.W*27/375,borderWidth:1}}>
                        <Text style={{fontSize:BaseComponent.W*16/375,color:'#333333'}}>更改密码</Text>
                  </View>
                  </TouchableWithoutFeedback>
                  
            </View>  
      </View>
    )
  }
}              