/**
 * @name MsgNotice.js
 * @auhor 程浩
 * @date 2018.10.8
 * @desc 通知消息、物流消息等
 */
import React,{Component} from 'react';
import {Text,View,StyleSheet,Image,TouchableWithoutFeedback,ScrollView,
ImageBackground,TextInput,StatusBar,Modal,FlatList,Picker} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
// 导入工具函数库
import T from "../../lib/Tools";
// 导入公共样式
import St from "../../lib/Style";
// 导入图片资源库
import Imgs from "../../lib/Img";
import BaseComponent from '../BaseComponent';


// Tools实例
let ti = T.getInstance();

let style = StyleSheet.create({
  wrapper:{
    backgroundColor:"white",
    flex:1,
    flexDirection:"column",
    alignItems:"stretch",
    justifyContent:"flex-start",
    position:"relative",
  },
  statusBar:{
    backgroundColor:"rgb(255,255,255)",
    height:ti.select({ios:16,android:22}),
    position:"relative",
    zIndex:1,
    opacity:1,
  },
  navBar:{
    backgroundColor:"rgb(255,255,255)",
    height:"7%",
    position:"relative",
    zIndex:1,
    opacity:1,
    borderBottomWidth:1,
    borderBottomColor:"rgb(241,241,241)"
  },
});

export default class MsgNotice extends BaseComponent{

  render(){
    var data=[];
    for(var i=0;i<3;i++)
    {
      data.push(i);
    }
    return (
      <View style={style.wrapper}>   
                {/*状态栏占位*/}
                <View style={{ backgroundColor:"rgb(255,255,255)",
                      height:ti.select({ios:16,android:22}),
                      position:"relative",
                      zIndex:1,
                      opacity:1,}}></View>
                {/*顶部导航栏*/}
              <View style={style.navBar}>
                  <View style={{flex:1,flexDirection:"row",justifyContent:"flex-start"}}>

                    <TouchableWithoutFeedback onPress={()=>this.goBack()}>
                    <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>
                      <Image source={Imgs.back} style={{width:(BaseComponent.W*0.127*0.4),height:(BaseComponent.W*0.127*0.4),marginTop:ti.select({ios:BaseComponent.W*0.02,android:0}),marginRight:BaseComponent.W*0.04}}></Image>
                    </View>
                    </TouchableWithoutFeedback>

                    <View style={{flexGrow:1,backgroundColor:"transparent",
                      marginTop:ti.select({ios:(BaseComponent.H*0.02)/2,android:(BaseComponent.H*0.016)/2}),borderRadius:ti.select({ios:(BaseComponent.H*0.05)/2,android:(BaseComponent.H*0.054)/2}),borderColor:"#0A0A0A",
                      borderWidth:0,}}>
                      <Text style={{color:"#0A0A0A",width:"100%",textAlign:"center",marginTop:ti.select({ios:"4%",android:"1.6%"}),fontSize:17,marginLeft:'4.3%'}}>{this.params.title}</Text>
                    </View>
                    <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center",marginRight:BaseComponent.W*5/375}}>
                        <Text></Text>
                    </View>
                  </View>
                </View> 
              {/*通知消息*/}   
             <View style={{flex:1,backgroundColor:'#F0F0F0',display:this.params.title=="通知消息"?'flex':'none'}}>
                <ScrollView style={{flex:1}} showsVerticalScrollIndicator = {false}>
                { data.map(()=>{
                    return (
                      <View style={{width:BaseComponent.W*355/375,height:BaseComponent.W*155/375,marginLeft:BaseComponent.W*10/375,marginTop:BaseComponent.W*10/375,backgroundColor:'#ffffff'}}>
                          <View style={{width:'100%',height:BaseComponent.W*36/375,justifyContent:'center'}}>
                              <Text style={{color:'#333333',fontSize:BaseComponent.W*15/375,marginLeft:BaseComponent.W*15/375}}>【凉秋换装折扣季】低至60%OFF</Text>
                          </View>
                          <View style={{width:BaseComponent.W*328/375,height:0.5,marginLeft:BaseComponent.W*14/375,backgroundColor:'#EAEAEA'}}></View>
                            {/*商品介绍*/}
                          <View style={{width:'100%',height:BaseComponent.W*86/375,flexDirection:'row',alignItems:'center'}}>
                              <Image style={{width:BaseComponent.W*60/375,height:BaseComponent.W*60/375,marginLeft:BaseComponent.W*14/375}} source={require('../../image/home/good.png')}></Image>
                              <View style={{width:BaseComponent.W*255/375,height:BaseComponent.W*58/375,marginLeft:BaseComponent.W*14/375}}>
                                  <Text style={{color:'#727272',fontSize:BaseComponent.W*13/375}}>任你挑选:百搭牛仔系列低至5.5折,连衣裙系列最高直降299,100+秋季新品限时4折起,给你意想不到的低价>></Text>
                              </View>
                          </View>
                          <View style={{width:BaseComponent.W*328/375,height:0.5,marginLeft:BaseComponent.W*14/375,backgroundColor:'#EAEAEA'}}></View>
                          
                          <View style={{flex:1,flexDirection:'row',alignItems:'center'}}>
                              <Text style={{color:'#A7A7A7',fontSize:BaseComponent.W*10/375,marginLeft:BaseComponent.W*16/375}}>15:30</Text>
                              <Text style={{color:'#727272',fontSize:BaseComponent.W*12/375,marginLeft:BaseComponent.W*230/375}}>查看详情</Text>
                              <Image style={{width:BaseComponent.W*15/375,height:BaseComponent.W*20/375,marginLeft:BaseComponent.W*8/375}} source={require('../../image/home/youjiantou.png')}></Image>
                          </View>
                      </View>
                    )
                  })
                }
                </ScrollView>
             </View>   

              {/*物流消息*/}   
             <View style={{flex:1,backgroundColor:'#F0F0F0',display:this.params.title=="物流消息"?'flex':'none'}}>
                <ScrollView style={{flex:1}} showsVerticalScrollIndicator = {false}>
                { data.map(()=>{
                    return (
                      <View style={{width:BaseComponent.W*355/375,height:BaseComponent.W*155/375,marginLeft:BaseComponent.W*10/375,marginTop:BaseComponent.W*10/375,backgroundColor:'#ffffff'}}>
                          <View style={{width:'100%',height:BaseComponent.W*36/375,justifyContent:'center'}}>
                              <Text style={{color:'#333333',fontSize:BaseComponent.W*15/375,marginLeft:BaseComponent.W*7/375}}>订单201810091234658已发货</Text>
                          </View>
                          <View style={{width:BaseComponent.W*328/375,height:0.5,marginLeft:BaseComponent.W*14/375,backgroundColor:'#EAEAEA'}}></View>
                            {/*商品介绍*/}
                          <View style={{width:'100%',height:BaseComponent.W*86/375,flexDirection:'row',alignItems:'center'}}>
                              <Image style={{width:BaseComponent.W*60/375,height:BaseComponent.W*60/375,marginLeft:BaseComponent.W*14/375}} source={require('../../image/home/good.png')}></Image>
                              <View style={{width:BaseComponent.W*255/375,height:BaseComponent.W*58/375,marginLeft:BaseComponent.W*14/375}}>
                                  <View style={{width:'100%',height:BaseComponent.W*23/375,flexDirection:"row",justifyContent:'space-between',alignItems:'center'}}>
                                      <View style={{width:BaseComponent.W*176/375,height:BaseComponent.W*23/375,justifyContent:'center'}}>
                                          <Text style={{fontSize:BaseComponent.W*13/375,color:'#333333'}} numberOfLines={1} ellipsizeMode="tail">雅诗兰黛气垫BB霜 保湿遮瑕裸11111111111111111111111111111111111…</Text>
                                      </View>
                                      <Text style={{color:'#333333',fontSize:BaseComponent.W*13/375}}>包裹1</Text>
                                  </View>
                                  <View style={{width:'100%',height:BaseComponent.W*18/375,marginTop:BaseComponent.W*14/375,flexDirection:"row",justifyContent:'space-between',alignItems:'center'}}>
                                      <Text style={{fontSize:BaseComponent.W*12/375,color:'#727272'}}>共1种商品</Text>
                                      <Text style={{fontSize:BaseComponent.W*12/375,color:'#727272'}}>EMS快递</Text>
                                  </View>
                              </View>
                          </View>
                          <View style={{width:BaseComponent.W*328/375,height:0.5,marginLeft:BaseComponent.W*14/375,backgroundColor:'#EAEAEA'}}></View>
                          
                          <View style={{flex:1,flexDirection:'row',alignItems:'center'}}>
                              <Text style={{color:'#A7A7A7',fontSize:BaseComponent.W*10/375,marginLeft:BaseComponent.W*16/375}}>15:30</Text>
                              <Text style={{color:'#727272',fontSize:BaseComponent.W*12/375,marginLeft:BaseComponent.W*230/375}}>查看详情</Text>
                              <Image style={{width:BaseComponent.W*15/375,height:BaseComponent.W*20/375,marginLeft:BaseComponent.W*8/375}} source={require('../../image/home/youjiantou.png')}></Image>
                          </View>
                      </View>
                    )
                  })
                }
                </ScrollView>
             </View>   
    </View>
    )
  }            
}  