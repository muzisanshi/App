/**
 * @name Msgcenter.js
 * @auhor 程浩
 * @date 2018.10.8
 * @desc 消息中心
 */
import React,{Component} from 'react';
import {Text,View,StyleSheet,Image,TouchableWithoutFeedback,ScrollView,
ImageBackground,TextInput,StatusBar,Modal,FlatList,Picker} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
// 导入工具函数库
import T from "../../lib/Tools";
// 导入公共样式
import St from "../../lib/Style";
// 导入图片资源库
import Imgs from "../../lib/Img";
import BaseComponent from '../BaseComponent';
import MsgNotice from './MsgNotice';


// Tools实例
let ti = T.getInstance();

let style = StyleSheet.create({
  wrapper:{
    backgroundColor:"white",
    flex:1,
    flexDirection:"column",
    alignItems:"stretch",
    justifyContent:"flex-start",
    position:"relative",
  },
  statusBar:{
    backgroundColor:"rgb(255,255,255)",
    height:ti.select({ios:16,android:22}),
    position:"relative",
    zIndex:1,
    opacity:1,
  },
  navBar:{
    backgroundColor:"rgb(255,255,255)",
    height:"7%",
    position:"relative",
    zIndex:1,
    opacity:1,
    borderBottomWidth:1,
    borderBottomColor:"rgb(241,241,241)"
  },
});

export default class Msgcenter extends BaseComponent{

  render(){
    return (
      <View style={style.wrapper}>   
                {/*状态栏占位*/}
                <View style={{ backgroundColor:"rgb(255,255,255)",
                      height:ti.select({ios:16,android:22}),
                      position:"relative",
                      zIndex:1,
                      opacity:1,}}></View>
                {/*顶部导航栏*/}
              <View style={style.navBar}>
                <View style={{flex:1,flexDirection:"row",justifyContent:"flex-start"}}>

                    <TouchableWithoutFeedback onPress={()=>this.goBack()}>
                    <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>
                      <Image source={Imgs.back} style={{width:(BaseComponent.W*0.127*0.4),height:(BaseComponent.W*0.127*0.4),marginTop:ti.select({ios:BaseComponent.W*0.02,android:0}),marginRight:BaseComponent.W*0.04}}></Image>
                    </View>
                    </TouchableWithoutFeedback>

                    <View style={{flexGrow:1,backgroundColor:"transparent",
                      marginTop:ti.select({ios:(BaseComponent.H*0.02)/2,android:(BaseComponent.H*0.016)/2}),borderRadius:ti.select({ios:(BaseComponent.H*0.05)/2,android:(BaseComponent.H*0.054)/2}),borderColor:"#0A0A0A",
                      borderWidth:0,}}>
                      <Text style={{color:"#0A0A0A",width:"100%",textAlign:"center",marginTop:ti.select({ios:"4%",android:"1.6%"}),fontSize:17,marginLeft:'4.3%'}}>{this.params.title}</Text>
                    </View>
                    <View style={{width:"17%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center",marginRight:BaseComponent.W*5/375}}>
                        <Text style={{fontSize:BaseComponent.W*0.037,color:'#9A9A9A',marginTop:5}}>全部已读</Text>
                    </View>
                </View>
              </View>

              {/*客服助手*/}
              <TouchableWithoutFeedback onPress={()=>this.navigate('MsgNotice',{title:"通知消息"})}>
              <View style={{width:'100%',height:BaseComponent.W*75/375,flexDirection:'row',alignItems:'center'}}>
                <View style={{width:BaseComponent.W*10/375,height:BaseComponent.W*10/375,borderRadius:BaseComponent.W*5/375,backgroundColor:'#E31436',position:'absolute',top:0,
                            marginTop:BaseComponent.W*20/375,marginLeft:BaseComponent.W*45/375,zIndex:1000}}></View>
                <View style={{width:BaseComponent.W*40/375,height:BaseComponent.W*40/375,borderRadius:BaseComponent.W*20/375,backgroundColor:'#E8BD4F',marginLeft:BaseComponent.W*10/375}}></View>
                <View style={{width:'100%',height:BaseComponent.W*75/375,justifyContent:'center'}}>
                  <Text style={{fontSize:BaseComponent.W*16/375,color:'#333333',marginLeft:BaseComponent.W*10/375}}>客服助手</Text>
                  <Text style={{fontSize:BaseComponent.W*14/375,color:'#9A9A9A',marginLeft:BaseComponent.W*10/375,marginTop:5}}>点击查看您与客服的沟通记录</Text>
                </View>
              </View>
              </TouchableWithoutFeedback>
              <View style={{width:BaseComponent.W*365/375,height:0.5,backgroundColor:'#EFEFEF',marginLeft:BaseComponent.W*10/375}}></View>
              

              {/*物流助手*/}
              <TouchableWithoutFeedback onPress={()=>this.navigate('MsgNotice',{title:'物流消息'})}>
              <View style={{width:'100%',height:BaseComponent.W*75/375,flexDirection:'row',alignItems:'center'}}>
                <View style={{width:BaseComponent.W*10/375,height:BaseComponent.W*10/375,borderRadius:BaseComponent.W*5/375,backgroundColor:'#E31436',position:'absolute',top:0,
                            marginTop:BaseComponent.W*20/375,marginLeft:BaseComponent.W*45/375,zIndex:1000}}></View>
                <View style={{width:BaseComponent.W*40/375,height:BaseComponent.W*40/375,borderRadius:BaseComponent.W*20/375,backgroundColor:'#75BEAF',marginLeft:BaseComponent.W*10/375}}></View>
                <View style={{width:BaseComponent.W*315/375,height:BaseComponent.W*75/375,justifyContent:'center'}}>
                  <View style={{width:BaseComponent.W*315/375,height:BaseComponent.W*18/375,flexDirection:'row',justifyContent:'space-between'}}>
                    <Text style={{fontSize:BaseComponent.W*16/375,color:'#333333',marginLeft:BaseComponent.W*10/375}}>物流助手</Text>
                    <Text style={{fontSize:BaseComponent.W*12/375,color:'#999999',marginTop:3}}>12:17</Text>
                  </View>
                  <Text style={{fontSize:BaseComponent.W*14/375,color:'#9A9A9A',marginLeft:BaseComponent.W*10/375,marginTop:5}}>您有新的物流信息，请查看</Text>
                </View>
              </View>
              </TouchableWithoutFeedback>
              <View style={{width:BaseComponent.W*365/375,height:0.5,backgroundColor:'#EFEFEF',marginLeft:BaseComponent.W*10/375}}></View>
              
              {/*通知消息*/}
              <View style={{width:'100%',height:BaseComponent.W*75/375,flexDirection:'row',alignItems:'center'}}>
                <View style={{display:'none',width:BaseComponent.W*10/375,height:BaseComponent.W*10/375,borderRadius:BaseComponent.W*5/375,backgroundColor:'#E31436',position:'relative',top:0,
                            marginTop:BaseComponent.W*20/375,marginLeft:BaseComponent.W*45/375,zIndex:500,}}></View>
                <View style={{width:BaseComponent.W*40/375,height:BaseComponent.W*40/375,borderRadius:BaseComponent.W*20/375,backgroundColor:'#F18755',marginLeft:BaseComponent.W*10/375}}></View>
                <View style={{width:BaseComponent.W*315/375,height:BaseComponent.W*75/375,justifyContent:'center'}}>
                  <View style={{width:BaseComponent.W*315/375,height:BaseComponent.W*18/375,flexDirection:'row',justifyContent:'space-between'}}>
                    <Text style={{fontSize:BaseComponent.W*16/375,color:'#333333',marginLeft:BaseComponent.W*10/375}}>通知消息</Text>
                    <Text style={{fontSize:BaseComponent.W*12/375,color:'#999999',marginTop:3}}>2018.9.28</Text>
                  </View>
                  <Text style={{fontSize:BaseComponent.W*14/375,color:'#9A9A9A',marginLeft:BaseComponent.W*10/375,marginTop:5}}>双十一优惠，有你想要的！</Text>
                </View>
              </View>    
               <View style={{width:BaseComponent.W*365/375,height:0.5,backgroundColor:'#EFEFEF',marginLeft:BaseComponent.W*10/375}}></View>
      </View>          
    )
  }
}