/**
 * @name DingdanXiangqingDaifahuo.js
 * @auhor 程浩
 * @date 2018.8.21
 * @desc 订单详情待发货界面
 */
import React,{Component} from 'react';
import {Text,View,StyleSheet,Image,TouchableWithoutFeedback,ScrollView,Modal,ActivityIndicator,StatusBar,Clipboard} from 'react-native';
// 导入工具函数库
import T from "../../lib/Tools";
// 导入公共样式
import St from "../../lib/Style";
// 导入图片资源库
import Imgs from "../../lib/Img";
import BaseComponent from '../BaseComponent';
// 转菊花
import Spinner from 'react-native-loading-spinner-overlay';
import  SelectAddressOrDate  from './SelectAddressOrDate';

// Tools实例
let ti = T.getInstance();

let style = StyleSheet.create({
  wrapper:{
    backgroundColor:"#F0F0F0",
    flex:1,
    flexDirection:"column",
    alignItems:"stretch",
    justifyContent:"flex-start",
    position:"relative",
  },
  statusBar:{
    backgroundColor:"rgb(255,255,255)",
    height:ti.select({ios:16,android:22}),
    position:"relative",
    zIndex:1,
    opacity:1,
  },
  navBar:{
    backgroundColor:"rgb(255,255,255)",
    height:"7%",
    position:"relative",
    zIndex:1,
    opacity:1,
    borderBottomWidth:1,
    borderBottomColor:"rgb(241,241,241)"
  },
});


export default class DingdanXiangqingDaifahuo extends BaseComponent{
  constructor(props){
    super(props);
    this.state={
      businessOrderNo:"",//订单编号
      reason:[],//取消原因
      visible:false,//转菊花
      isvisible:false,//提醒删除订单
      shouhuovisible:false,//提醒订单确认收货
      CancelorderMark:true,//true表示可以显示取消订单按钮,false表示不显示取消订单按钮
      GoodsData:{orderPackages:[],orderDetail:{receiverPhoneNo:""},payChannelOrder:{payMode:{payChannel:{name:""}}}},//订单数据
      netError:false,//true表示网络错误
      from:"",//从哪个界面过来
      blankVisible:false,

      userInfo:null,

    }
  }
  componentWillMount(){
    var thiz=this;
    if(thiz.params.from){
      var from=thiz.params.from;
      console.log("-----------------from----------------------",from);
      thiz.setState({from:from})
    }

    // thiz.getAccount(function(ret){
    //   thiz.log("--------account--------",ret);

    //   if(ret&&ret.userInfo){
    //     thiz.setState({
    //       userInfo:ret.userInfo,
    //     });
    //   }

    // });

    // 从服务器加载用户信息
    thiz.request("user/getInfo",{
        method:"POST",
        success:function(ret){
          thiz.setState({visible:false});
          thiz.log("--------querendingdan_user/getInfo-------",ret);
          if(ret&&ret.respCode==="00"){
              thiz.setState({
                  userInfo:ret.data,
              });
          }
        },
        error:function(err){
          thiz.toast(err.msg);
        }
    });

  }

  componentDidMount(){
    var thiz=this;
    thiz.getData();
    //监听包裹确认收货
    thiz.listen('orderPackage_confirmReceived',function(){
        thiz.getData();
    });
    //监听刷新数据
    thiz.listen("cancel_order_success",function(){
      // if(thiz.state.from){
      //   thiz.navigate("Home");
      // }else{
      //   thiz.goBack();
      // }   

      if(BaseComponent.CUR_TAB!="Mine"){
        // 先返回首页、分类、购物车，再跳到全部订单列表
        // thiz.navigate(BaseComponent.CUR_TAB);
        // 定时发送打开全部订单列表的消息
        setTimeout(function(){
          thiz.emit("open_all_order_list");
        },0);
      }else{
        thiz.goBack();
      }

    });
    //监听订单确认收货
    thiz.listen('confirm_Received',function(){
      thiz.getData();
    });

    thiz.listen("hide_blank",function(){
      thiz.setState({
        blankVisible:false,
      });
    });
  }
  //获取订单数据
  getData=()=>{
    var thiz=this;
    var businessOrderNo=thiz.params.businessOrderNo;
    thiz.log("--------DingdanXiangqingDaifahuo_orderNo--------",businessOrderNo);
    thiz.setState({
      visible:true,
    })
    
    // thiz.request("order/getOrderInfo",{
    thiz.request("order/getOrderInfoWithRepertory",{
      method:"POST",
      data:{businessOrderNo:businessOrderNo},
      success:function(ret){
          thiz.log("--------------------------------------ret-------------------------------",ret);
          if(ret.respCode=="00")
          {

            // 重新构造orderPackages
            // ret.data.orderPackages = [];
            // thiz.log("-----------------------------------ret.data.orderPackages_before--------------------------------",ret.data.orderPackages);
            // for(let i=0;i<ret.data.repertoryList.length;i++){
            //   for(let k=0;k<ret.data.repertoryList[i].orderPackages.length;k++){
            //     ret.data.orderPackages.push(ret.data.repertoryList[i].orderPackages[k]);
            //   }
            // }
            // thiz.log("-----------------------------------ret.data.orderPackages_after--------------------------------",ret.data.orderPackages);

            // if(ret.data.orderStatus=="WAIT_FOR_DELIVERY"){
            //   if(ret.data.orderPackages){
            //     for(var i=0;i<ret.data.orderPackages.length;i++){
            //       var CancelorderMark;
            //       var mark=ret.data.orderPackages[i].deliveryStatus;
            //       if(mark=="DELIVERED"){
            //         CancelorderMark=false;
            //         thiz.setState({CancelorderMark:CancelorderMark});
            //         break ;
            //       }
            //     }
            //   }
            // }

            // 处理仓库中的包裹数据
            if(ret.data.orderStatus=="WAIT_FOR_DELIVERY"){
              if(ret.data.repertoryList.orderPackages){
                for(var i=0;i<ret.data.repertoryList.orderPackages.length;i++){
                  var CancelorderMark;
                  var mark=ret.data.repertoryList.orderPackages[i].deliveryStatus;
                  if(mark=="DELIVERED"){
                    CancelorderMark=false;
                    thiz.setState({CancelorderMark:CancelorderMark});
                    break ;
                  }
                }
              }
            }

            thiz.setState({
              visible:false,
              businessOrderNo:businessOrderNo,
              GoodsData:ret.data,
            })

          }
      },
      error:function(err){
          thiz.setState({visible:false});
          if(err.code&&err.code==200){
            console.log("11111111111111111111111111111111");
            thiz.setState({netError:true});
          } 
      }
    });
  };
  //隐藏部分电话号码
  PlacePhone=()=>{
    var phone=this.state.GoodsData.orderDetail.receiverPhoneNo;
    var num;
    if(phone.length=11)
    {
      num=phone.toString().replace(/^(\d{3})\d{4}(\d{4})$/, '$1****$2');
    }else
    {
       num=phone; 
    }
    return num;
}
//订单左边按钮点击事件
  leftBtnClick=()=>{
    var thiz=this;
    if(thiz.state.GoodsData.orderStatus=="WAIT_FOR_DELIVERY"||thiz.state.GoodsData.orderStatus=="WAIT_FOR_RECEIVED")
    {
      //订单再次购买
      thiz.request("order/buyAgain",{
          method:"POST",
          data:{businessOrderNo:thiz.state.businessOrderNo},
          success:function(ret){
              thiz.emit('click_buyAgain');
              BaseComponent.CUR_TAB = "Cart";
              thiz.navigate("Cart");
          },
          error:function(error){

          }
      });
    }
    if(thiz.state.GoodsData.orderStatus=="COMPLETED")
    {
      //删除订单
      thiz.setState({isvisible:true});
    }
};

//订单右边按钮点击事件
  rightBtnClick=()=>{
    var thiz=this;
    if(thiz.state.GoodsData.orderStatus=="WAIT_FOR_DELIVERY")
    {
      // thiz.toast("取消订单");
      thiz.open();
      thiz.emit("cancel_order",{businessOrderNo:thiz.state.businessOrderNo});
      return;
    }
    if(thiz.state.GoodsData.orderStatus=="WAIT_FOR_RECEIVED")
    {
      thiz.setState({shouhuovisible:true});
      // 订单确认收货
      return;
    }
    if(thiz.state.GoodsData.orderStatus=="COMPLETED"||this.state.GoodsData.orderStatus=="COMPLETED"||this.state.GoodsData.orderStatus=="APPLY_REFUND"||this.state.GoodsData.orderStatus=="REFUND_PROCESSING"||this.state.GoodsData.orderStatus=="REFUND_SUCCESS")
    {
      //订单再次购买
      thiz.request("order/buyAgain",{
          method:"POST",
          data:{businessOrderNo:thiz.state.businessOrderNo},
          success:function(ret){
              thiz.emit('click_buyAgain');
              BaseComponent.CUR_TAB = "Cart";
              thiz.navigate("Cart");
          },
          error:function(error){

          }
      });
    }

};
//包裹确认收货
  confirmReceived=(item)=>{
    var thiz=this;
    thiz.log("-------------------------------------确认收货----------------------------------------",item);
    thiz.request("orderPackage/confirmReceived",{
      method:"POST",
      data:{id:item.id},
      success:function(ret){
          console.log("-------------------------------已经确认收货---------------------------------");
          thiz.emit('orderPackage_confirmReceived');
      },
      error:function(err){

      }
    })
  };
//包裹追踪物流
  TrackTheLogistics=(item)=>{
    var thiz=this;
    // thiz.toast("追踪物流");
    thiz.navigate('LogisticsTracking',{title:'物流追踪',id:item.id});
  };
//包裹退换
  returnGoods=(item)=>{
    var thiz=this;
    thiz.log("----------------------------------------------------ShouhouType--------------------------------------------------",item);
    thiz.navigate('ShouhouType',{title:'选择售后类型',id:item.id});
  };  

  //复制到剪贴板
  copyClipboard=()=>{
    console.log("--------------------------------------点击了复制-----------------------------");
    Clipboard.setString(this.state.businessOrderNo);
    this.toast("复制成功");
    console.log("--------------------------------------复制成功-----------------------------");
  }
callBackAddressValue(value){
        this.setState({
            reason:value
        })
};
open(){
      // this.refs.SelectAddressOrDate.showAddress(this.state.reason)
      let thiz = this;
    this.setState({
      blankVisible:true,
    });
    setTimeout(function(){
      thiz.refs.SelectAddressOrDate.showAddress(thiz.state.reason);
    },500);
};  
close(){
  this.refs.SelectAddressOrDate.hide();
}
    render() {
      let thiz=this;
        return ( 
             <View style={style.wrapper}>
                {/*状态栏占位*/}
                <StatusBar translucent={true} backgroundColor={this.state.isvisible||this.state.visible||this.state.shouhuovisible?'rgba(14,14,14,0.5)':"rgb(255,255,255)"} barStyle={'dark-content'}/>
                <View style={{backgroundColor:this.state.isvisible||this.state.visible||this.state.shouhuovisible?'rgba(14,14,14,0.5)':"rgb(255,255,255)",
                          height:thiz.isIphoneX(30,BaseComponent.SH),
                          position:"relative",
                          zIndex:1,
                          opacity:1,}}></View>
                {/*顶部导航栏*/}
                <View style={style.navBar}>
                  <View style={{flex:1,flexDirection:"row",justifyContent:"flex-start"}}>

                    <TouchableWithoutFeedback onPress={()=>{
                      // 判断返回哪个页面
                      if(thiz.params.from=="Cart"){
                        thiz.navigate("Cart");
                        return;
                      }
                      if(thiz.params.from=="GoodsXiangQing"){
                        thiz.navigate("GoodsXiangQing");
                        return;
                      }
                      if(thiz.params.from=="DingdanXiangqingDaizhifu"){
                        thiz.navigate('Wodedingdan',{title:"我的订单",page:2});
                        return;
                      }

                      // 其他情况，直接返回就是
                      thiz.goBack();

                    }}>
                    <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>
                      <Image source={Imgs.back} style={{width:(BaseComponent.W*0.127*0.4),height:(BaseComponent.W*0.127*0.4),marginRight:BaseComponent.W*0.04}}></Image>
                    </View>
                    </TouchableWithoutFeedback>

                    <View style={{flexGrow:1,backgroundColor:"transparent",
                      marginTop:ti.select({ios:(BaseComponent.H*0.02)/2,android:(BaseComponent.H*0.016)/2}),borderRadius:ti.select({ios:(BaseComponent.H*0.05)/2,android:(BaseComponent.H*0.054)/2}),borderColor:"#0A0A0A",
                      borderWidth:0,}}>
                      <Text style={{color:"#0A0A0A",width:"100%",textAlign:"center",marginTop:ti.select({ios:"4.5%",android:"1.6%"}),fontSize:17}}>{this.params.title}</Text>
                    </View>
                    <TouchableWithoutFeedback onPress={()=>{
                      // 跳转客服
                      thiz.log("--------toService--------","toService");
                      thiz.goPage("Service",{title:"客服"});
                    }}>
                    <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>
                      <Image source={require('../../image/home/kefu.png')} style={{width:(BaseComponent.W*0.127*0.5),height:(BaseComponent.W*0.127*0.5),display:"flex"}}></Image>
                    </View>
                    </TouchableWithoutFeedback>

                  </View>
                </View>

                {/*空白Modal*/}
                <Modal visible={this.state.blankVisible} transparent={true}>
                  
                  <View style={{flex:1,backgroundColor:"transparent"}}></View>
                  
                </Modal>

                {/*转菊花*/}
                <Modal
                  visible={this.state.visible}
                  onRequestClose={()=>{this.setState({visible:false})}}
                  transparent={true}
                  animationType={"fade"}>
                  <View style={{backgroundColor:"rgba(14,14,14,0.5)",width:"100%",width:"100%",flex:1,flexDirection:"column",alignItems:"center",justifyContent:"center",position:"relative"}}>
                    <View>
                          <ActivityIndicator size="large" color='white'/>
                    </View>
                  </View>
                </Modal>
                
                {/*提醒删除订单*/}
                <Modal visible={this.state.isvisible}
                       onRequestClose={()=>{this.setState({isvisible:false})}}
                       transparent={true}>
                       <View style={{flex:1,backgroundColor:'rgba(14,14,14,0.5)',justifyContent:'center',alignItems:'center'}}>
                          <View style={{width:BaseComponent.W*275/375,height:BaseComponent.W*105/375,backgroundColor:'rgba(255, 255, 255,1)',borderRadius:BaseComponent.W*20/375}}>
                              <View style={{width:'100%',height:BaseComponent.W*56/375,justifyContent:'center',alignItems:'center'}}>
                                <Text style={{fontSize:BaseComponent.W*15/375,color:'rgba(94, 94, 94, 1)'}}>确定要删除该订单吗?</Text>
                              </View>
                              <View style={{width:'100%',height:0.5,backgroundColor:'rgba(189, 189, 189, 1)'}}></View>
                              <View style={{width:'100%',height:BaseComponent.W*49/375,flexDirection:'row'}}>
                              <TouchableWithoutFeedback onPress={()=>this.setState({isvisible:false})}> 
                                <View style={{width:'50%',height:BaseComponent.W*49/375,justifyContent:'center',alignItems:'center'}}>
                                  <Text style={{fontSize:BaseComponent.W*15/375,color:'rgba(94, 94, 94, 1)'}}>取消删除</Text>
                                </View>
                              </TouchableWithoutFeedback> 
                                <View style={{width:0.5,height:'100%',backgroundColor:'rgba(189, 189, 189, 1)'}}></View>
                                <TouchableWithoutFeedback onPress={()=>{
                                      thiz.request("order/delete",{
                                        method:"POST",
                                        data:{businessOrderNo:thiz.state.businessOrderNo},
                                        success:function(ret){
                                            console.log("-----------------------删除订单成功----------------------");
                                            thiz.setState({isvisible:false});
                                            thiz.emit('delete_order_success');
                                            thiz.goBack();
                                        },
                                        error:function(err){
                                          console.log("-----------------------删除订单失败----------------------");
                                        }
                                      });
                                }}> 
                                <View style={{width:'50%',height:BaseComponent.W*49/375,justifyContent:'center',alignItems:'center'}}>
                                  <Text style={{fontSize:BaseComponent.W*15/375,color:'#FED584'}}>确认删除</Text>
                                </View>
                                </TouchableWithoutFeedback>
                              </View>
                          </View>
                       </View>
                </Modal>

                {/*提醒确认收货订单*/}
                <Modal visible={this.state.shouhuovisible}
                       onRequestClose={()=>{this.setState({shouhuovisible:false})}}
                       transparent={true}>
                       <View style={{flex:1,backgroundColor:'rgba(14,14,14,0.5)',justifyContent:'center',alignItems:'center'}}>
                          <View style={{width:BaseComponent.W*275/375,height:BaseComponent.W*105/375,backgroundColor:'rgba(255, 255, 255,1)',borderRadius:BaseComponent.W*20/375}}>
                              <View style={{width:'100%',height:BaseComponent.W*56/375,justifyContent:'center',alignItems:'center'}}>
                                <Text style={{fontSize:BaseComponent.W*15/375,color:'#3a3a3a'}}>确认收货吗?</Text>
                              </View>
                              <View style={{width:'100%',height:0.5,backgroundColor:'rgba(189, 189, 189, 1)'}}></View>
                              <View style={{width:'100%',height:BaseComponent.W*49/375,flexDirection:'row'}}>
                              <TouchableWithoutFeedback onPress={()=>this.setState({shouhuovisible:false})}> 
                                <View style={{width:'50%',height:BaseComponent.W*49/375,justifyContent:'center',alignItems:'center'}}>
                                  <Text style={{fontSize:BaseComponent.W*15/375,color:'#3a3a3a'}}>取消</Text>
                                </View>
                              </TouchableWithoutFeedback> 
                                <View style={{width:0.5,height:'100%',backgroundColor:'rgba(189, 189, 189, 1)'}}></View>
                                <TouchableWithoutFeedback onPress={()=>{
                                      // 订单确认收货
                                      thiz.request("order/confirmReceived",{
                                        method:"POST",
                                        data:{businessOrderNo:thiz.state.businessOrderNo},
                                        success:function(ret){
                                          thiz.setState({shouhuovisible:false});
                                          thiz.emit('confirm_Received');
                                          thiz.goBack();
                                        },
                                        error:function(err){
                                          
                                        }
                                      })
                                }}> 
                                <View style={{width:'50%',height:BaseComponent.W*49/375,justifyContent:'center',alignItems:'center'}}>
                                  <Text style={{fontSize:BaseComponent.W*15/375,color:'#FED584'}}>确认收货</Text>
                                </View>
                                </TouchableWithoutFeedback>
                              </View>
                          </View>
                       </View>
                </Modal>
            {
              thiz.state.netError?(
                 <View style={{width:BaseComponent.W,height:BaseComponent.H*0.85,justifyContent:'center',alignItems:'center'}}>
                    <Image style={{width:BaseComponent.W*180/375,height:BaseComponent.W*180/375,borderRadius:BaseComponent.W*90/375}} source={require('../../image/mine/neterror.png')}></Image>
                    <TouchableWithoutFeedback onPress={()=>{
                      thiz.setState({netError:false});
                      thiz.getData();
                    }}>
                    <View style={{width:BaseComponent.W*70/375,height:BaseComponent.W*30/375,borderRadius:BaseComponent.W*15/375,borderWidth:0.5,borderColor:'#080808',
                              justifyContent:'center',alignItems:'center',marginTop:BaseComponent.W*15/375}}>
                          <Text style={{fontSize:BaseComponent.W*15/375,color:'#252525'}}>刷新</Text>          
                    </View>
                    </TouchableWithoutFeedback>
                  </View>):(
                    <View style={{flex:1}}>
                    <ScrollView style={{flex:1}} showsVerticalScrollIndicator={false}> 
                    <SelectAddressOrDate ref={'SelectAddressOrDate'} callBackAddressValue={this.callBackAddressValue.bind(this)}/>
                     {/*收货人地址*/}
                     <View style={styles.Address}>
                          <Image style={styles.AddressImg} source={require('../../image/home/mine_dizhiguanli.png')}></Image>
                          <Text style={{fontSize:BaseComponent.W*0.037,marginLeft:BaseComponent.W*0.093,color:'#121212',marginTop:BaseComponent.W*0.056}}>{this.state.GoodsData.orderDetail.receiverName}&nbsp;&nbsp;<Text>{this.PlacePhone()}</Text></Text>
                          <Text style={{fontSize:BaseComponent.W*0.037,marginLeft:BaseComponent.W*0.093,marginTop:BaseComponent.W*0.015,color:'#6B6B6B'}} numberOfLines={1}>{this.state.GoodsData.orderDetail.fullAddress}</Text>
                     </View>

                     {
                              this.state.GoodsData.repertoryList&&this.state.GoodsData.repertoryList.length>0?this.state.GoodsData.repertoryList.map((iitem,iindex)=>{

                                return (
                                  <View>
                                    {/*仓库分割线*/}
                                    <View style={{width:'100%',height:BaseComponent.W*0.027,backgroundColor:'#F0F0F0'}}></View>
                                    {/*仓库和发货人信息*/}
                                    <View style={{backgroundColor:"white",width:'100%',}}>

                                      <View style={{width:'100%',paddingTop:10,paddingBottom:10,
                                        borderBottomWidth:0.5,borderBottomColor:"#ECECEC",flexDirection:"row"}}>
                                        <Image source={require("../../image/cart/shop.png")} style={{width:20,height:20,marginLeft:8,marginTop:ti.select({ios:-2,android:2}),}}/>
                                        <Text style={{fontSize:BaseComponent.W*14/375,color:'#121212',marginLeft:BaseComponent.W*10/375}}>{iitem.name?iitem.name:""}</Text>
                                      </View>


                                      {
                                        thiz.state.userInfo&&thiz.state.userInfo.userType=="AGENT"?(
                                          <View>

                                            <View style={{width:'100%',paddingTop:10,paddingBottom:10,justifyContent:'center',
                                              borderBottomWidth:0,borderBottomColor:"#ECECEC"}}>
                                              <Text style={{fontSize:BaseComponent.W*14/375,color:'#121212',marginLeft:BaseComponent.W*10/375}}>发货人：</Text>
                                            </View>

                                            <View style={{width:"100%",}}>

                                              <View style={{width:BaseComponent.W*330/375,borderBottomWidth:0.5,borderBottomColor:"#ECECEC",backgroundColor:"white",paddingTop:10,paddingBottom:10,}}>
                                                <View style={{flex:1,}}>
                                                  <View style={{width:BaseComponent.W*330/375,height:'100%',marginLeft:BaseComponent.W*10/375}}>
                                                    <View style={{flexDirection:"row"}}>
                                                      <Text style={{fontSizeL:BaseComponent.W*14/375,color:'#6B6B6B',}}>{(iitem.country)?("["+iitem.country.name+"]"):""}&nbsp;&nbsp;{iitem.senderAddress?iitem.senderAddress.senderName:""}</Text>
                                                      <Text style={{marginLeft:30,fontSizeL:BaseComponent.W*14/375,color:'#6B6B6B',}}>{iitem.senderAddress?iitem.senderAddress.senderPhoneNo:""}</Text>
                                                    </View>
                                                    
                                                    <Text style={{fontSizeL:BaseComponent.W*14/375,color:'#6B6B6B',marginTop:BaseComponent.W*5/375}} numberOfLines={1}>{(iitem.senderAddress)?iitem.senderAddress.senderAddress:""}</Text>
                                                  </View>
                                                </View>
                                              </View>

                                            </View>

                                          </View>
                                        ):null
                                      }


                                    </View>

                                    {
                                      iitem.orderPackages.map((item,index)=>{
                                          return (
                                                <View key={index} style={{backgroundColor:'white'}}>

                                                    <View style={{width:'100%',height:BaseComponent.W*31/375,flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
                                                        <Text style={{fontSize:BaseComponent.W*14/375,color:'#2F2F2F',marginLeft:BaseComponent.W*10/375}}>{item.name}</Text>
                                                        <Text style={{fontSize:BaseComponent.W*14/375,color:this.state.GoodsData.orderStatus=="COMPLETED"?"#30B507":(item.deliveryStatus=="NOT_DELIVER"?'#FF407E':(item.deliveryStatus=="DELIVERED"&&item.receiverReceivingStatus=="NOT_RECEIVED"?'#7D7D7D':'#30B507')),marginRight:BaseComponent.W*10/375}}>
                                                            {(()=>{
                                                              if(this.state.GoodsData.orderStatus=="COMPLETED"){
                                                                return "交易完成"
                                                              }else{
                                                                if(this.state.GoodsData.orderStatus=="APPLY_REFUND"){return "退款申请中"};
                                                                if(this.state.GoodsData.orderStatus=="REFUND_PROCESSING"){return "退款中"};
                                                                if(this.state.GoodsData.orderStatus=="REFUND_SUCCESS"){return "退款成功"};
                                                                if(item.deliveryStatus=="NOT_DELIVER"){return "待发货"};
                                                                if(item.deliveryStatus=="DELIVERED"&&item.receiverReceivingStatus=="NOT_RECEIVED"){return "待收货"};
                                                                if(item.deliveryStatus=="DELIVERED"&&item.receiverReceivingStatus=="CONFIRM_RECEIVED"){return "交易完成"}
                                                              }  
                                                            })()} 
                                                        </Text>
                                                    </View>
                                                    {
                                                        item.orderInfoItems.map((orderInfoItems,index1)=>{
                                                          this.log("------------orderInfoItems-----------------------",orderInfoItems);
                                                            return (
                                                              <View key={index1} style={{backgroundColor:"white"}}>  
                                                                  <View style={{width:BaseComponent.W*355/375,height:0.5,backgroundColor:'#DDDDDD',marginLeft:BaseComponent.W*10/375}}></View>
                                                                  <View style={styles.Item}>
                                                                        <View style={styles.ItemImg}>
                                                                          <Image style={{width:BaseComponent.W*0.16,height:BaseComponent.W*0.16}} source={{uri:orderInfoItems.sku.imageAttachment.resourceFullAddress?orderInfoItems.sku.imageAttachment.resourceFullAddress:""}}></Image>
                                                                        </View>
                                                                        <View style={styles.ItemIns}>
                                                                          <Text style={{fontSize:BaseComponent.W*0.035,color:'#040000'}} numberOfLines={2}>{orderInfoItems.skuName}</Text>
                                                                          <Text style={{fontSize:BaseComponent.W*0.032,color:'#999899'}}>{orderInfoItems.sku.goodsAttributeOptions[0].goodsAttributeOption.value}&nbsp;&nbsp;{orderInfoItems.sku.goodsAttributeOptions[1]?orderInfoItems.sku.goodsAttributeOptions[1].goodsAttributeOption.value:""}</Text>
                                                                        </View>   
                                                                        <View style={{width:BaseComponent.W*80/375,height:BaseComponent.W*60/375,flexDirection:'column',alignItems:'flex-end',marginLeft:BaseComponent.W*20/375}}>
                                                                            <Text style={{color:'#393A3A',fontSize:BaseComponent.W*0.035}}>¥{orderInfoItems.buyUnitAmount}</Text>
                                                                            <Text style={{fontSize:BaseComponent.W*0.032,color:'#666666'}}>×{orderInfoItems.num}</Text>
                                                                        </View>
                                                                  </View>
                                                              </View>
                                                            )
                                                        })
                                                    }
                                                    <View style={{width:BaseComponent.W*355/375,height:0.5,backgroundColor:'#DDDDDD',marginLeft:BaseComponent.W*10/375}}></View>
                                                        {/*退换/售后、追踪物流、确认收货*/}
                                                        <View style={{width:'100%',height:BaseComponent.W*50/375,flexDirection:'row',justifyContent:'flex-end',display:this.state.GoodsData.orderStatus=="APPLY_REFUND"||this.state.GoodsData.orderStatus=="REFUND_PROCESSING"||this.state.GoodsData.orderStatus=="REFUND_SUCCESS"?'none':'flex',backgroundColor:'white'}}>
                                                              <TouchableWithoutFeedback onPress={()=>{
                                                                  this.returnGoods(item);
                                                              }}>
                                                              <View style={{width:BaseComponent.W*70/375,height:BaseComponent.W*30/375,borderRadius:BaseComponent.W*15/375,
                                                                            borderWidth:0.5,borderColor:'#606060',marginRight:BaseComponent.W*16/375,marginTop:BaseComponent.W*9/375,
                                                                            justifyContent:'center',alignItems:'center',display:item.receiverReceivingStatus=="CONFIRM_RECEIVED"?'flex':'none'}}>
                                                                    <Text style={{fontSize:BaseComponent.W*14/375,color:'#252525'}}>退换/售后</Text>          
                                                              </View>
                                                              </TouchableWithoutFeedback>

                                                              <TouchableWithoutFeedback onPress={()=>{
                                                                  this.TrackTheLogistics(item);
                                                              }}>
                                                              <View style={{width:BaseComponent.W*70/375,height:BaseComponent.W*30/375,borderRadius:BaseComponent.W*15/375,
                                                                            borderWidth:0.5,borderColor:'#606060',marginRight:BaseComponent.W*16/375,marginTop:BaseComponent.W*9/375,
                                                                            justifyContent:'center',alignItems:'center'}}>
                                                                    <Text style={{fontSize:BaseComponent.W*14/375,color:'#252525'}}>追踪物流</Text>          
                                                              </View>
                                                              </TouchableWithoutFeedback>

                                                              <TouchableWithoutFeedback onPress={()=>{
                                                                  this.confirmReceived(item);
                                                              }}>
                                                              <View style={{width:BaseComponent.W*70/375,height:BaseComponent.W*30/375,borderRadius:BaseComponent.W*15/375,
                                                                            borderWidth:0.5,borderColor:'#606060',marginRight:BaseComponent.W*12/375,marginTop:BaseComponent.W*9/375,
                                                                            justifyContent:'center',alignItems:'center',display:item.deliveryStatus=="DELIVERED"&&item.receiverReceivingStatus=="NOT_RECEIVED"?'flex':'none'}}>
                                                                    <Text style={{fontSize:BaseComponent.W*14/375,color:'#252525'}}>确认收货</Text>          
                                                              </View>
                                                              </TouchableWithoutFeedback>
                                                        
                                                        </View>
                                                    <View style={{width:'100%',height:0.5,backgroundColor:'#F0F0F0'}}></View>   
                                                </View>
                                          )
                                      })
                                    } 
                                  </View>
                                );

                              }):null
                            }
                     
                    
                    
                    <View style={{width:'100%',height:BaseComponent.W*0.027,backgroundColor:'#F0F0F0'}}></View>

                    {/*费用明细*/}
                    <View style={styles.CostBreakdown}>
                        <View style={{width:'100%',height:BaseComponent.W*0.112,flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
                          <Text style={{fontSize:BaseComponent.W*0.037,color:"#121212",marginLeft:BaseComponent.W*0.027}}>应付总额</Text>
                          <Text style={{fontSize:BaseComponent.W*0.037,color:"#FF407E",marginRight:BaseComponent.W*0.029}}>¥{this.state.GoodsData.payAmount?this.state.GoodsData.payAmount.toFixed(2):"0.00"}</Text>
                        </View>
                        <View style={{width:'100%',height:0.5,backgroundColor:'#DDDDDD',marginLeft:BaseComponent.W*0.027}}></View>
                        <View style={styles.CostBreakdownTongyong}>
                            <Text style={{fontSize:BaseComponent.W*0.032,color:'#6B6B6B',marginLeft:BaseComponent.W*0.027}}>商品总额</Text>
                            <Text style={{fontSize:BaseComponent.W*0.037,color:"#121212",marginRight:BaseComponent.W*0.029}}>¥{this.state.GoodsData.goodsAmount?this.state.GoodsData.goodsAmount.toFixed(2):"0.00"}</Text>
                        </View>
                        <View style={{width:'100%',height:0.5,backgroundColor:'#DDDDDD',marginLeft:BaseComponent.W*0.027}}></View>
                        <View style={styles.CostBreakdownTongyong}>
                            <Text style={{fontSize:BaseComponent.W*0.032,color:'#6B6B6B',marginLeft:BaseComponent.W*0.027}}>优惠</Text>
                            <Text style={{fontSize:BaseComponent.W*0.037,color:"#121212",marginRight:BaseComponent.W*0.029}}>-¥{this.state.GoodsData.reduceAmount?this.state.GoodsData.reduceAmount.toFixed(2):"0.00"}</Text>
                        </View>
                        <View style={{width:'100%',height:0.5,backgroundColor:'#DDDDDD',marginLeft:BaseComponent.W*0.027}}></View>
                        <View style={styles.CostBreakdownTongyong}>
                            <Text style={{fontSize:BaseComponent.W*0.032,color:'#6B6B6B',marginLeft:BaseComponent.W*0.027}}>税费</Text>
                            <Text style={{fontSize:BaseComponent.W*0.037,color:"#121212",marginRight:BaseComponent.W*0.029}}>¥{this.state.GoodsData.taxAmount?this.state.GoodsData.taxAmount.toFixed(2):"0.00"}</Text>
                        </View>
                        <View style={{width:'100%',height:0.5,backgroundColor:'#DDDDDD',marginLeft:BaseComponent.W*0.027}}></View>
                        <View style={styles.CostBreakdownTongyong}>
                            <Text style={{fontSize:BaseComponent.W*0.032,color:'#6B6B6B',marginLeft:BaseComponent.W*0.027}}>服务</Text>
                            <Text style={{fontSize:BaseComponent.W*0.037,color:"#121212",marginRight:BaseComponent.W*0.029}}>¥{this.state.GoodsData.specialServiceAmount?this.state.GoodsData.specialServiceAmount.toFixed(2):"0.00"}</Text>
                        </View>
                        <View style={{width:'100%',height:0.5,backgroundColor:'#DDDDDD',marginLeft:BaseComponent.W*0.027}}></View>
                         <View style={styles.CostBreakdownTongyong}>
                            <Text style={{fontSize:BaseComponent.W*0.032,color:'#6B6B6B',marginLeft:BaseComponent.W*0.027}}>邮费</Text>
                            <Text style={{fontSize:BaseComponent.W*0.037,color:"#121212",marginRight:BaseComponent.W*0.029}}>¥{this.state.GoodsData.logisticAmount?this.state.GoodsData.logisticAmount.toFixed(2):"0.00"}</Text>
                        </View>
                        
                    </View>
                        <View style={{width:'100%',height:BaseComponent.W*0.027,backgroundColor:'#F0F0F0'}}></View>
                  {/*下单时间和编号*/}
                    <View style={{width:BaseComponent.W,height:BaseComponent.W*115/375,flexDirection:'column',backgroundColor:'white'}}>
                        <View style={{width:'100%',flexDirection:'row',justifyContent:'space-between',marginLeft:BaseComponent.W*0.0293}}>
                            <Text style={{color:'#121212',fontSize:BaseComponent.W*0.0346,marginTop:BaseComponent.W*15/375}}>订单编号:&nbsp;&nbsp;{this.state.businessOrderNo}</Text>
                            <TouchableWithoutFeedback onPress={()=>{
                              thiz.copyClipboard();
                            }}>
                            <View style={{width:BaseComponent.W*50/375,height:BaseComponent.W*20/375,marginTop:BaseComponent.W*15/375,marginRight:BaseComponent.W*20/375,
                                        borderRadius:BaseComponent.W*10/375,borderWidth:1,borderColor:'#CECECE',justifyContent:'center',alignItems:'center'}}>
                                  <Text style={{fontSize:BaseComponent.W*13/375,color:'#999999'}}>复制</Text>
                            </View>
                            </TouchableWithoutFeedback>
                        </View>
                        <Text style={{color:'#121212',fontSize:BaseComponent.W*0.0346,marginTop:BaseComponent.W*1/375,marginLeft:BaseComponent.W*0.0293}}>支付方式:&nbsp;&nbsp;{this.state.GoodsData.payChannelOrder.payMode.payChannel.name}</Text>
                        <Text style={{color:'#121212',fontSize:BaseComponent.W*0.0346,marginTop:BaseComponent.W*5/375,marginLeft:BaseComponent.W*0.0293}}>支付交易单号:&nbsp;&nbsp;{this.state.GoodsData.payChannelOrder.channelOrderNo}</Text>
                        <Text style={{color:'#121212',fontSize:BaseComponent.W*0.0346,marginTop:BaseComponent.W*5/375,marginLeft:BaseComponent.W*0.0293}}>下单时间:&nbsp;&nbsp;{this.state.GoodsData.orderDatetime}</Text>
                    </View>
                    <View style={{width:'100%',height:BaseComponent.W*10/375,backgroundColor:'#F0F0F0'}}></View> 
                    </ScrollView>  

                  {/*支付区域*/}
                  <View style={{width:'100%',height:BaseComponent.W*50/375,flexDirection:'row',justifyContent:'flex-end',alignItems:'center',backgroundColor:'white',borderTopWidth:0.5,borderTopColor:'#F0F0F0'}}>
                      
                      <TouchableWithoutFeedback onPress={()=>{
                            this.leftBtnClick();
                      }}>
                      <View style={{width:BaseComponent.W*70/375,height:BaseComponent.W*30/375,borderRadius:BaseComponent.W*15/375,borderWidth:0.5,borderColor:'#606060',marginRight:BaseComponent.W*16/375,
                                    justifyContent:'center',alignItems:'center',display:this.state.GoodsData.orderStatus=="APPLY_REFUND"||this.state.GoodsData.orderStatus=="REFUND_PROCESSING"||this.state.GoodsData.orderStatus=="REFUND_SUCCESS"?'none':'flex'}}>       
                            <Text style={{fontSize:BaseComponent.W*15/375,color:'#252525'}}>
                                {(()=>{
                                    if(this.state.GoodsData.orderStatus=="WAIT_FOR_DELIVERY"||this.state.GoodsData.orderStatus=="WAIT_FOR_RECEIVED"){return "再次购买"};
                                    if(this.state.GoodsData.orderStatus=="COMPLETED"){return "删除订单"}
                                })()} 
                            </Text>          
                      </View>
                      </TouchableWithoutFeedback>
                      
                      <TouchableWithoutFeedback onPress={()=>{
                            this.rightBtnClick();
                      }}>
                      <View style={{width:BaseComponent.W*70/375,height:BaseComponent.W*30/375,borderRadius:BaseComponent.W*15/375,borderWidth:0.5,borderColor:this.state.GoodsData.orderStatus=="WAIT_FOR_DELIVERY"?'#999899':'#080808',marginRight:BaseComponent.W*10/375,
                                    justifyContent:'center',alignItems:'center',display:this.state.CancelorderMark?'flex':'none'}}>
                            <Text style={{fontSize:BaseComponent.W*15/375,color:this.state.GoodsData.orderStatus=="WAIT_FOR_DELIVERY"?'#B6B6B6':'#252525'}}>
                                {(()=>{
                                    if(this.state.GoodsData.orderStatus=="WAIT_FOR_DELIVERY"){return "取消订单"};
                                    if(this.state.GoodsData.orderStatus=="WAIT_FOR_RECEIVED"){return "确认收货"};
                                    if(this.state.GoodsData.orderStatus=="COMPLETED"||this.state.GoodsData.orderStatus=="APPLY_REFUND"||this.state.GoodsData.orderStatus=="REFUND_PROCESSING"||this.state.GoodsData.orderStatus=="REFUND_SUCCESS"){return "再次购买"}
                                })()} 
                            </Text>          
                      </View>
                      </TouchableWithoutFeedback>

                  </View>
                  </View>
                  )
            }
            

 
          </View>      
        )
    }
}
const styles=StyleSheet.create({
    Address:{
          width:'100%',
          height:BaseComponent.W*0.2,
          flexDirection:'column',
          backgroundColor:'#FFF6E3'
    },
    AddressImg:{
        position:'absolute',
        width:BaseComponent.W*0.045,
        height:BaseComponent.W*0.045,
        left:0,
        marginTop:BaseComponent.W*0.047,
        marginLeft:BaseComponent.W*0.027
    },
    Item:{
        width:'100%',
        height:BaseComponent.H*0.129,
        flexDirection:'row',
        alignItems:'center'
    },
    ItemImg:{
        width:BaseComponent.W*0.211,
        height:BaseComponent.H*0.129,
        justifyContent:'center',
        alignItems:'center'
    },
    ItemIns:{
        width:BaseComponent.W*0.5,
        height:BaseComponent.W*0.16,
        flexDirection:'column',
        justifyContent:'space-between',
    },
    CostBreakdown:{
      width:BaseComponent.W,
      flexDirection:'column',
      backgroundColor:'white'
    },
    CostBreakdownTongyong:{
      width:'100%',
      height:BaseComponent.W*0.0773,
      flexDirection:'row',
      justifyContent:'space-between',
      alignItems:'center',
    }
});