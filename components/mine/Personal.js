/**
 * @name Personal.js
 * @auhor 程浩
 * @date 2018.10.8
 * @desc 个人资料编辑,由我的主页点头像直接进来
 */
import React,{Component} from 'react';
import {Text,View,StyleSheet,Image,TouchableWithoutFeedback,StatusBar,TextInput,Modal,ImageBackground,Animated,ActivityIndicator} from 'react-native';
import BaseComponent from '../BaseComponent';
import ImagePicker from 'react-native-image-crop-picker'; 
// 导入工具函数库
import T from "../../lib/Tools";
// 导入公共样式
import St from "../../lib/Style";
// 导入图片资源库
import Imgs from "../../lib/Img";
// 转菊花
import Spinner from 'react-native-loading-spinner-overlay';

import WXShare from '@yyyyu/react-native-wechat';
import {WechatError} from '@yyyyu/react-native-wechat';

// Tools实例
let ti = T.getInstance();

let style = StyleSheet.create({
  wrapper:{
    backgroundColor:"#fff",
    flex:1,
    flexDirection:"column",
    alignItems:"stretch",
    justifyContent:"flex-start",
    position:"relative",
  },
  statusBar:{
    backgroundColor:"#fff",
    height:ti.select({ios:16,android:22}),
    position:"relative",
    zIndex:1,
    opacity:1,
  },
  navBar:{
    backgroundColor:"#fff",
    height:"7%",
    position:"relative",
    zIndex:1,
    opacity:1,
    borderBottomWidth:1,
    borderBottomColor:"rgb(241,241,241)"
  },
});
export default class Personal extends BaseComponent{
  constructor(props){
    super(props);
    this.state={
      isvisible:false,
      bottom:new Animated.Value(0),
      photos:null,
      nickname:this.params.userInfo.nickname,//用户昵称
      // personalitySignature:this.params.userInfo.personalitySignature,//用户签名
      visible:false,
      userInfo:this.params.userInfo,
      wxNo:this.params.userInfo.wxNo,
    }
  }
  
  //修改用户信息
  updateUserInfo=()=>{
    let thiz=this;
    if(thiz.params.userInfo.nickname==thiz.state.nickname&&thiz.params.userInfo.wxNo==thiz.state.wxNo)
    {
      thiz.toast("不能修改相同的信息");
    }else{
       thiz.request("user/updateUserInfo",{
      method:"POST",
      data:{
        "nickname": thiz.state.nickname,
        "wxNo":thiz.state.wxNo,
      },
      success:function(){
        thiz.toast("修改信息成功");
        setTimeout(function(){
          thiz.emit("modify_user_info_success");
        },500);
        
      },
      error:function(){
        
        thiz.toast("修改信息失败");
        
      }
    });
    }
   
  }

  //打开相册
  openAlbum=()=>{
      let thiz = this;

      thiz.setState({
        isvisible:false,
      });
      
      setTimeout(function(){
        ImagePicker.openPicker({  
          width: 300,  
          height: 400,  
          cropping:false,
          compressImageQuality:0.5,
      }).then(image => {  

        // 上传图片
        // 开始转菊花
        thiz.setState({visible:true});

        let url = image.path;
        thiz.log("--------image.path11111111111--------",url); 
        if(url&&BaseComponent.OS=="ios"){
          url = "file:///"+url;
        }
        thiz.upload(url,{
          progress:function(pro){
            
          },
          success:function(ret){

            let userInfo = thiz.state.userInfo;
            if(!userInfo.headImageAttachment){
              userInfo.headImageAttachment = {};
            }

            userInfo.headImageAttachment.resourceFullAddress = ret.data.resourceFullAddress;

            // 调用提交接口
            thiz.request("user/updateHeadImage",{
              method:"POST",
              data:{
                attachmentId:ret.data.id,
              },
              success:function(ret,err){
                setTimeout(function(){
                  thiz.setState({
                    visible:false,
                  });
                },100);
                

                if(ret&&ret.respCode==="00"){
                  thiz.emit("modify_user_info_success","上传头像成功");
                  thiz.setState({
                    userInfo:userInfo,
                  });
                }

              },
              error:function(ret,err){
                thiz.toast(err.msg);
                setTimeout(function(){
                  thiz.setState({
                    visible:false,
                  });
                },100);
              }
            });

          },
          error:function(err){
            setTimeout(function(){
              thiz.setState({
                visible:false,
              });
            },100);
          }
        });
      });
      },500);


      // WXShare.sendLink({
      //   link: 'http://young-global.com/', // 链接地址
      //   title: '过年好', // 标题
      //   desc: '新的一年里万象更新', // 描述
      //   thumb: require('../../image/tab/home_active.png'), // 缩略图
      //   scene: 'session' // 发送场景 optional('session')
      // });
      
      // let share = async function(){

      //   try{
      //     let res = await WXShare.sendText({
      //       text:"分享测试",
      //       scene: 'session'
      //     });
      //     return res;
      //   }catch(e){
      //     if (e instanceof WechatError) {
      //         thiz.log("--------share_err--------",e);
      //     }
      //   }

      // };

      // let ret = share();
      // thiz.log("--------share_ret--------",ret);
      
  }

   //打开相机
   openCamera=()=>{
      let thiz = this;

      thiz.setState({
        isvisible:false,
      });
      
      setTimeout(function(){
      ImagePicker.openCamera({
        width:300,
        height:400,
        cropping:false,
        compressImageQuality:0.1,
      }).then(image =>{
        
        let url = image.path;
        if(url&&BaseComponent.OS=="ios"){
          url = "file:///"+url;
        }

        thiz.setState({visible:true});

        thiz.upload(url,{
          progress:function(pro){
            
          },
          success:function(ret){

            let userInfo = thiz.state.userInfo;
            if(!userInfo.headImageAttachment){
              userInfo.headImageAttachment = {};
            }
            userInfo.headImageAttachment.resourceFullAddress = ret.data.resourceFullAddress;

            // thiz.setState({userInfo:userInfo});
            
            thiz.log("--------upload_success--------",ret);
            // 调用提交接口
            thiz.request("user/updateHeadImage",{
              method:"POST",
              data:{
                attachmentId:ret.data.id,
              },
              success:function(ret,err){

                setTimeout(function(){
                  thiz.setState({
                    visible:false,
                  });
                },100);

                thiz.log("--------user/updateHeadImage-------",ret);
                if(ret&&ret.respCode==="00"){
                  thiz.emit("modify_user_info_success","上传头像成功");
                  thiz.setState({userInfo:userInfo});
                }
              },
              error:function(ret,err){
                setTimeout(function(){
                  thiz.setState({
                    visible:false,
                  });
                },100);
                thiz.toast(err.msg);
              }
            });

          },
          error:function(err){
            setTimeout(function(){
              thiz.setState({
                visible:false,
              });
            },100);
          }
        });
      });
    },500);
   }

    _translateAni = ()=>{
        this.state.bottom.setValue(0);
        Animated.timing(
            this.state.bottom,
            {
                toValue: BaseComponent.W*87/375,
                duration: 500,
            },
        ).start();
    }
  render(){
    return (
      <View style={style.wrapper}>

                {/*转菊花*/}
                <Modal
                  visible={this.state.visible}
                  onRequestClose={()=>{this.setState({visible:false})}}
                  transparent={true}
                  animationType={"fade"}>
                  <View style={{backgroundColor:"rgba(14,14,14,0.5)",width:"100%",width:"100%",flex:1,flexDirection:"column",alignItems:"center",justifyContent:"center",position:"relative"}}>
                    <View>
                          <ActivityIndicator size="large" color='white' animating={this.state.visible}/>
                    </View>
                  </View>
                </Modal>
                
                {/*状态栏占位*/}
                <View style={{backgroundColor:this.state.isvisible?'rgba(14,14,14,0.5)':"#fff",
                              height:ti.select({ios:16,android:StatusBar.currentHeight}),
                              position:"relative",
                              zIndex:1,
                              opacity:1,}}></View>
                {/*顶部导航栏*/}
                <View style={style.navBar}>
                  <View style={{flex:1,flexDirection:"row",justifyContent:"flex-start",alignItems:'center'}}>

                    <TouchableWithoutFeedback onPress={()=>this.goBack()}>
                    <View style={{width:"15%",height:"100%",backgroundColor:"transparent",justifyContent:"center",marginLeft:BaseComponent.W*10/375}}>
                      <Image source={Imgs.back} style={{width:(BaseComponent.W*0.127*0.4),height:(BaseComponent.W*0.127*0.4),marginTop:ti.select({ios:BaseComponent.W*0.02,android:0})}}></Image>
                    </View>
                    </TouchableWithoutFeedback>

                    <View style={{flexGrow:1,backgroundColor:"transparent",borderWidth:0,}}>
                      <Text style={{color:"#0A0A0A",width:"100%",textAlign:"center",marginTop:ti.select({ios:"4%",android:"1.6%"}),fontSize:BaseComponent.W*18/375}}>{this.params.title}</Text>
                    </View>
                    <TouchableWithoutFeedback onPress={()=>this.updateUserInfo()}>  
                    <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center",marginTop:ti.select({ios:"2%",android:5})}}>
                        <Text style={{fontSize:BaseComponent.W*0.037,color:'black'}}>保存</Text>
                    </View>
                    </TouchableWithoutFeedback>
                  </View>
                </View>
            {/*弹出框选择*/}
            <Modal visible={this.state.isvisible}
                   transparent={true}
                   onRequestClose={() => {this.setState({isvisible:false})}}>
                <TouchableWithoutFeedback onPress={()=>this.setState({isvisible:false})}>   
                <View style={{flex:1,backgroundColor:'rgba(14,14,14,0.5)',justifyContent:'flex-end',alignItems:'center'}}>
                      <Animated.View style={{width:BaseComponent.W*355/375,height:BaseComponent.W*87/375,backgroundColor:'#fff',marginBottom:-BaseComponent.W*77/375,bottom:this.state.bottom,borderRadius:BaseComponent.W*10/375}}>
                          <TouchableWithoutFeedback onPress={()=>{this.openAlbum();}}>  
                          <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                              <Text style={{fontSize:BaseComponent.W*18/375,color:'#313131'}}>获取相册</Text>
                          </View>
                          </TouchableWithoutFeedback>
                          
                          <View style={{width:BaseComponent.W*345/375,height:0.5,backgroundColor:'#D6D6D6',marginLeft:BaseComponent.W*5/375}}></View>
                          <TouchableWithoutFeedback onPress={()=>{this.openCamera();}}> 
                           <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                              <Text style={{fontSize:BaseComponent.W*18/375,color:'#313131'}}>拍照</Text>
                          </View>
                          </TouchableWithoutFeedback>
                      </Animated.View>
                </View>
                </TouchableWithoutFeedback>
            </Modal>
            <View style={{width:'100%',height:BaseComponent.W*10/375,backgroundColor:'#F0F0F0'}}></View> 
            {/*头像选取*/}
            <TouchableWithoutFeedback onPress={()=>{
                  this.setState({isvisible:true})
                  this._translateAni()}}>
            <View style={{width:'100%',height:BaseComponent.W*48/375,flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
                <Text style={{fontSize:BaseComponent.W*15/375,color:'#838383',marginLeft:BaseComponent.W*10/375}}>头像</Text>
                <Image style={{width:BaseComponent.W*38/375,height:BaseComponent.W*38/375,borderRadius:BaseComponent.W*19/375,marginRight:BaseComponent.W*12/375}} source={this.state.userInfo.headImageAttachment&&this.state.userInfo.headImageAttachment.resourceFullAddress?{uri:this.state.userInfo.headImageAttachment.resourceFullAddress}:require("../../image/mine/avatar.png")}></Image>
            </View>  
            </TouchableWithoutFeedback>
            <View style={{width:'100%',height:BaseComponent.W*10/375,backgroundColor:'#F0F0F0'}}></View>
            {/*修改昵称*/}
            <View style={{width:'100%',height:BaseComponent.W*48/375,flexDirection:'row',alignItems:'center'}}>
                <Text style={{fontSize:BaseComponent.W*15/375,color:'#838383',marginLeft:BaseComponent.W*10/375}}>昵称</Text>
                <TextInput style={{marginLeft:10,color:"#232326",width:'100%',padding:0,fontSize:BaseComponent.W*15/375}}
                           underlineColorAndroid='transparent' maxLength={10} defaultValue={this.state.userInfo.nickname}
                           onChangeText={(event)=>this.setState({nickname:event})}></TextInput>
            </View>   
            <View style={{width:BaseComponent.W*365/375,height:0.5,marginLeft:BaseComponent.W*10/375,backgroundColor:'#E1E1E1'}}></View>
            
            {/*微信号*/}
            <View style={{width:'100%',height:BaseComponent.W*48/375,flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
                <Text style={{fontSize:BaseComponent.W*15/375,color:'#838383',marginLeft:BaseComponent.W*10/375}}>微信号</Text>
                 <TextInput style={{marginLeft:10,color:"#232326",width:'100%',padding:0,fontSize:BaseComponent.W*15/375}}
                           underlineColorAndroid='transparent' maxLength={20} defaultValue={this.state.userInfo.wxNo}
                          onChangeText={(event)=>this.setState({wxNo:event})}></TextInput>
            </View> 
            <View style={{flex:1,backgroundColor:"#F0F0F0"}}></View> 
      </View>
    )
  }
}