/**
 * @name PersonalInfo.js
 * @auhor 程浩
 * @date 2018.10.8
 * @desc 个人信息,通过我的主页点击设置进来
 */
import React,{Component} from 'react';
import {Text,View,StyleSheet,Image,TouchableWithoutFeedback,StatusBar,TextInput,Modal,ImageBackground,Animated,ActivityIndicator} from 'react-native';
import BaseComponent from '../BaseComponent';
import ImagePicker from 'react-native-image-crop-picker'; 
// 导入工具函数库
import T from "../../lib/Tools";
// 导入公共样式
import St from "../../lib/Style";
// 导入图片资源库
import Imgs from "../../lib/Img";
// 转菊花
import Spinner from 'react-native-loading-spinner-overlay';
import UpdateNickname from './UpdateNickname';
import MinePhoneNo from './MinePhoneNo';
import AccountManagement from './AccountManagement';

// Tools实例
let ti = T.getInstance();

let style = StyleSheet.create({
  wrapper:{
    backgroundColor:"#fff",
    flex:1,
    flexDirection:"column",
    alignItems:"stretch",
    justifyContent:"flex-start",
    position:"relative",
  },
  statusBar:{
    backgroundColor:"#fff",
    height:ti.select({ios:16,android:22}),
    position:"relative",
    zIndex:1,
    opacity:1,
  },
  navBar:{
    backgroundColor:"#fff",
    height:"7%",
    position:"relative",
    zIndex:1,
    opacity:1,
    borderBottomWidth:1,
    borderBottomColor:"rgb(241,241,241)"
  },
});
export default class PersonalInfo extends BaseComponent{
  constructor(props){
    super(props);
    this.state={
      isvisible:false,//修改头像弹出框
      bottom:new Animated.Value(0),
      photos:null,
      sexvisible:false,//设置性别弹出框
      userInfo:"",
      bottom1:new Animated.Value(0),
      visible:false,//转菊花
    }
  }
  
  componentWillMount(){
    var thiz=this;
    var userInfo=thiz.params.userInfo;
    thiz.log("----------------------userInfo---------------------",userInfo);
    thiz.setState({userInfo:userInfo});
    //监听修改信息
    thiz.listen("modify_user_info_success",function(){
      thiz.getData();
    });
  };
  getData=()=>{
    var thiz=this;
    thiz.request("user/getInfo",{
      method:"POST",
      success:function(ret){
        thiz.setState({userInfo:ret.data});
      },
      error:function(err){

      }
    })
  }
  //打开相册
  openAlbum=()=>{
      let thiz = this;

      thiz.setState({
        isvisible:false,
      });
      
      setTimeout(function(){
        ImagePicker.openPicker({  
          width: 300,  
          height: 400,  
          cropping:false,
          compressImageQuality:0.5,
      }).then(image => {  

        // 上传图片
        // 开始转菊花
        thiz.setState({visible:true});

        let url = image.path;
        thiz.log("--------image.path11111111111--------",url); 
        if(url&&BaseComponent.OS=="ios"){
          url = "file:///"+url;
        }
        thiz.upload(url,{
          progress:function(pro){
            
          },
          success:function(ret){

            let userInfo = thiz.state.userInfo;
            if(!userInfo.headImageAttachment){
              userInfo.headImageAttachment = {};
            }

            userInfo.headImageAttachment.resourceFullAddress = ret.data.resourceFullAddress;

            // 调用提交接口
            thiz.request("user/updateHeadImage",{
              method:"POST",
              data:{
                attachmentId:ret.data.id,
              },
              success:function(ret,err){
                setTimeout(function(){
                  thiz.setState({
                    visible:false,
                  });
                },100);
                

                if(ret&&ret.respCode==="00"){
                  thiz.emit("modify_user_info_success","上传头像成功");
                  thiz.setState({
                    userInfo:userInfo,
                  });
                }

              },
              error:function(ret,err){
                thiz.toast(err.msg);
                setTimeout(function(){
                  thiz.setState({
                    visible:false,
                  });
                },100);
              }
            });

          },
          error:function(err){
            setTimeout(function(){
              thiz.setState({
                visible:false,
              });
            },100);
          }
        });
      });
      },500);
  }

   //打开相机
   openCamera=()=>{
      let thiz = this;

      thiz.setState({
        isvisible:false,
      });
      
      setTimeout(function(){
      ImagePicker.openCamera({
        width:300,
        height:400,
        cropping:false,
        compressImageQuality:0.1,
      }).then(image =>{
        
        let url = image.path;
        if(url&&BaseComponent.OS=="ios"){
          url = "file:///"+url;
        }

        thiz.setState({visible:true});

        thiz.upload(url,{
          progress:function(pro){
            
          },
          success:function(ret){

            let userInfo = thiz.state.userInfo;
            if(!userInfo.headImageAttachment){
              userInfo.headImageAttachment = {};
            }
            userInfo.headImageAttachment.resourceFullAddress = ret.data.resourceFullAddress;

            // thiz.setState({userInfo:userInfo});
            
            thiz.log("--------upload_success--------",ret);
            // 调用提交接口
            thiz.request("user/updateHeadImage",{
              method:"POST",
              data:{
                attachmentId:ret.data.id,
              },
              success:function(ret,err){

                setTimeout(function(){
                  thiz.setState({
                    visible:false,
                  });
                },100);

                thiz.log("--------user/updateHeadImage-------",ret);
                if(ret&&ret.respCode==="00"){
                  thiz.emit("modify_user_info_success","上传头像成功");
                  thiz.setState({userInfo:userInfo});
                }
              },
              error:function(ret,err){
                setTimeout(function(){
                  thiz.setState({
                    visible:false,
                  });
                },100);
                thiz.toast(err.msg);
              }
            });

          },
          error:function(err){
            setTimeout(function(){
              thiz.setState({
                visible:false,
              });
            },100);
          }
        });
      });
    },500);
   }

    _translateAni = ()=>{
        this.state.bottom.setValue(0);
        Animated.timing(
            this.state.bottom,
            {
                toValue: BaseComponent.W*87/375,
                duration: 500,
            },
        ).start();
    }
    _translateAni1 = ()=>{
        this.state.bottom1.setValue(0);
        Animated.timing(
            this.state.bottom1,
            {
                toValue: BaseComponent.W*78/375,
                duration: 500,
            },
        ).start();
    }
  render(){
    let thiz=this;
    return (
      <View style={style.wrapper}>
                {/*转菊花*/}
                <Modal
                  visible={this.state.visible}
                  onRequestClose={()=>{this.setState({visible:false})}}
                  transparent={true}
                  animationType={"fade"}>
                  <View style={{backgroundColor:"rgba(14,14,14,0.5)",width:"100%",width:"100%",flex:1,flexDirection:"column",alignItems:"center",justifyContent:"center",position:"relative"}}>
                    <View>
                          <ActivityIndicator size="large" color='white' animating={this.state.visible}/>
                    </View>
                  </View>
                </Modal>
                {/*状态栏占位*/}
                <View style={{backgroundColor:this.state.visible||this.state.isvisible?'rgba(14,14,14,0.5)':"#fff",
                              height:this.isIphoneX(30,BaseComponent.SH),
                              position:"relative",
                              zIndex:1,
                              opacity:1,}}></View>
                {/*顶部导航栏*/}
                <View style={style.navBar}>
                  <View style={{flex:1,flexDirection:"row",justifyContent:"flex-start",alignItems:'center'}}>

                    <TouchableWithoutFeedback onPress={()=>this.goBack()}>
                    <View style={{width:"15%",height:"100%",backgroundColor:"transparent",justifyContent:"center",marginLeft:BaseComponent.W*10/375}}>
                      <Image source={Imgs.back} style={{width:(BaseComponent.W*0.127*0.4),height:(BaseComponent.W*0.127*0.4),marginTop:ti.select({ios:BaseComponent.W*0.02,android:0})}}></Image>
                    </View>
                    </TouchableWithoutFeedback>

                    <View style={{flexGrow:1,backgroundColor:"transparent",borderWidth:0,}}>
                      <Text style={{color:"#0A0A0A",width:"100%",textAlign:"center",marginTop:ti.select({ios:"4%",android:"1.6%"}),fontSize:BaseComponent.W*18/375}}>{this.params.title}</Text>
                    </View>
                    
                    <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center",}}>                        
                    </View>

                  </View>
                </View>
            


            {/*修改头像弹出框选择*/}
            <Modal visible={this.state.isvisible}
                   transparent={true}
                   onRequestClose={() => {this.setState({isvisible:false})}}>
                <TouchableWithoutFeedback onPress={()=>this.setState({isvisible:false})}>   
                <View style={{flex:1,backgroundColor:'rgba(14,14,14,0.5)',justifyContent:'flex-end',alignItems:'center'}}>
                      <Animated.View style={{width:BaseComponent.W*355/375,height:BaseComponent.W*87/375,backgroundColor:'#fff',marginBottom:-BaseComponent.W*77/375,bottom:this.state.bottom,borderRadius:BaseComponent.W*10/375}}>
                          <TouchableWithoutFeedback onPress={()=>this.openAlbum()}>  
                          <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                              <Text style={{fontSize:BaseComponent.W*18/375,color:'#313131'}}>获取相册</Text>
                          </View>
                          </TouchableWithoutFeedback>
                          
                          <View style={{width:BaseComponent.W*345/375,height:0.5,backgroundColor:'#D6D6D6',marginLeft:BaseComponent.W*5/375}}></View>
                          <TouchableWithoutFeedback onPress={()=>this.openCamera()}> 
                           <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                              <Text style={{fontSize:BaseComponent.W*18/375,color:'#313131'}}>拍照</Text>
                          </View>
                          </TouchableWithoutFeedback>
                      </Animated.View>
                </View>
                </TouchableWithoutFeedback>
            </Modal>

            {/*修改性别弹出框选择*/}
            <Modal visible={this.state.sexvisible}
                   transparent={true}
                   onRequestClose={() => {this.setState({sexvisible:false})}}>
                   <TouchableWithoutFeedback onPress={()=>this.setState({sexvisible:false})}>
                   <View style={{flex:1,backgroundColor:'rgba(14,14,14,0.5)'}}>
                      <Animated.View style={{marginBottom:-BaseComponent.W*77/375,bottom:this.state.bottom1,position:'absolute'}}>
                        <View style={{width:BaseComponent.W*355/375,height:BaseComponent.W*87/375,marginLeft:BaseComponent.W*10/375,backgroundColor:'white',position:'absolute',
                                    bottom:0,marginBottom:BaseComponent.W*60/375,borderRadius:BaseComponent.W*10/375}}>
                              <TouchableWithoutFeedback onPress={()=>{
                                    var userInfo=thiz.state.userInfo;
                                    if(!userInfo.personalitySignature)
                                    {
                                      userInfo.personalitySignature="";
                                    } 
                                    thiz.setState({visible:true});
                                    thiz.request("user/updateUserInfo",{
                                        method:"POST",
                                        data:{
                                          sexual:"MALE",
                                          nickname:thiz.state.userInfo.nickname,
                                          personalitySignature:userInfo.personalitySignature,
                                      },
                                        success:function(ret){
                                            thiz.setState({sexvisible:false});
                                            thiz.request("user/getInfo",{
                                              method:"POST",
                                              success:function(ret){
                                                  thiz.setState({visible:false,userInfo:ret.data});
                                              },
                                              error:function(err){
                                                  thiz.setState({visible:false});
                                                  thiz.toast(err.msg);
                                              }
                                            })
                                        },
                                        error:function(err){
                                          thiz.setState({visible:false});
                                          thiz.log("----------------err-----------------",err);
                                           thiz.toast(err.msg);
                                        }
                                    });
                              }}>
                              <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                                  <Text style={{fontSize:BaseComponent.W*17/375,color:'#0A0A0A'}}>男</Text>
                              </View> 
                              </TouchableWithoutFeedback>  
                              <View style={{width:BaseComponent.W*347/375,height:0.5,marginLeft:BaseComponent.W*5/375,backgroundColor:'#D6D6D6'}}></View>
                              
                              <TouchableWithoutFeedback onPress={()=>{
                                 var userInfo=thiz.state.userInfo;
                                    if(!userInfo.personalitySignature)
                                    {
                                      userInfo.personalitySignature="";
                                    } 
                                    thiz.setState({visible:true});
                                    thiz.request("user/updateUserInfo",{
                                        method:"POST",
                                        data:{
                                          sexual:"FEMALE",
                                          nickname:thiz.state.userInfo.nickname,
                                          personalitySignature:userInfo.personalitySignature,
                                      },
                                        success:function(ret){
                                            thiz.setState({sexvisible:false});
                                            thiz.request("user/getInfo",{
                                              method:"POST",
                                              success:function(ret){
                                                    thiz.setState({visible:false,userInfo:ret.data});
                                              },
                                              error:function(err){
                                                    thiz.setState({visible:false});
                                                    thiz.toast(err.msg);  
                                              }
                                            })
                                        },
                                        error:function(err){
                                          thiz.setState({visible:false});
                                           thiz.toast(err.msg);
                                          thiz.log("----------------err-----------------",err);
                                        }
                                    });
                              }}>
                              <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                                  <Text style={{fontSize:BaseComponent.W*17/375,color:'#0A0A0A'}}>女</Text>
                              </View>
                              </TouchableWithoutFeedback>      
                        </View>
                        <View style={{width:BaseComponent.W*355/375,height:BaseComponent.W*44/375,marginLeft:BaseComponent.W*10/375,position:'absolute',bottom:0,
                                      marginBottom:BaseComponent.W*9/375,backgroundColor:'#ffffff',borderRadius:BaseComponent.W*10/375,justifyContent:'center',alignItems:'center'}}>
                               <Text style={{fontSize:BaseComponent.W*17/375,color:'#0A0A0A'}}>不选</Text>         
                        </View>
                      </Animated.View>    
                   </View>
                   </TouchableWithoutFeedback>
            </Modal>
             
            {/*头像选取*/}
            <TouchableWithoutFeedback onPress={()=>{
                  this.setState({isvisible:true})
                  this._translateAni()}}>
            <View style={{width:'100%',height:BaseComponent.W*70/375,flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
                <View style={{width:BaseComponent.W*48/375,height:BaseComponent.W*48/375,borderRadius:BaseComponent.W*24/375,marginLeft:BaseComponent.W*10/375}}>
                <Image style={{width:BaseComponent.W*48/375,height:BaseComponent.W*48/375,borderRadius:BaseComponent.W*24/375}} source={this.state.userInfo.headImageAttachment&&this.state.userInfo.headImageAttachment.resourceFullAddress?{uri:this.state.userInfo.headImageAttachment.resourceFullAddress}:require("../../image/mine/avatar.png")}></Image>
                </View>
                <View style={{height:BaseComponent.W*70/375,flexDirection:'row',alignItems:'center',justifyContent:'flex-end'}}>
                    <Text style={{fontSize:BaseComponent.W*15/375,color:'#999999'}}>修改头像</Text>
                    <Image style={{width:BaseComponent.W*25/375,height:BaseComponent.W*25/375,marginRight:BaseComponent.W*8/375}} source={require('../../image/home/youjiantou.png')}></Image>
                </View>   
            </View>  
            </TouchableWithoutFeedback>
            <View style={{width:'100%',height:0.5,backgroundColor:'#F0F0F0'}}></View>  
            
            {/*昵称*/}
            <TouchableWithoutFeedback onPress={()=>this.navigate('UpdateNickname',{title:'修改昵称',userInfo:thiz.state.userInfo})}>
            <View style={{width:'100%',height:BaseComponent.W*49/375,flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
                <Text style={{fontSize:BaseComponent.W*15/375,color:'#333333',marginLeft:BaseComponent.W*19/375}}>昵称</Text>
                <View style={{height:BaseComponent.W*49/375,flexDirection:'row',alignItems:'center',justifyContent:'flex-end'}}>
                    <Text style={{fontSize:BaseComponent.W*15/375,color:'#999999'}}>{this.state.userInfo.nickname?this.state.userInfo.nickname:""}</Text>
                    <Image style={{width:BaseComponent.W*25/375,height:BaseComponent.W*25/375,marginRight:BaseComponent.W*8/375}} source={require('../../image/home/youjiantou.png')}></Image>
                </View>   
            </View>  
            </TouchableWithoutFeedback>
            <View style={{width:'100%',height:0.5,backgroundColor:'#F0F0F0'}}></View>

            {/*性别*/}
            <TouchableWithoutFeedback onPress={()=>{
              this.setState({sexvisible:true});
              this._translateAni1()
            }}>
            <View style={{width:'100%',height:BaseComponent.W*49/375,flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
                <Text style={{fontSize:BaseComponent.W*15/375,color:'#333333',marginLeft:BaseComponent.W*19/375}}>性别</Text>
                <View style={{height:BaseComponent.W*49/375,flexDirection:'row',alignItems:'center',justifyContent:'flex-end'}}>
                    <Text style={{fontSize:BaseComponent.W*15/375,color:'#999999'}}>{this.state.userInfo.sexual=="UNKNOWN"?"未设置":(this.state.userInfo.sexual=="MALE"?"男":"女")}</Text>
                    <Image style={{width:BaseComponent.W*25/375,height:BaseComponent.W*25/375,marginRight:BaseComponent.W*8/375}} source={require('../../image/home/youjiantou.png')}></Image>
                </View>   
            </View>
            </TouchableWithoutFeedback>  
            <View style={{width:'100%',height:0.5,backgroundColor:'#F0F0F0'}}></View>

            {/*账号管理*/}
            <TouchableWithoutFeedback onPress={()=>this.navigate('AccountManagement',{title:"账号管理"})}>
            <View style={{width:'100%',height:BaseComponent.W*49/375,flexDirection:'row',justifyContent:'space-between',alignItems:'center',display:"none"}}>
                <Text style={{fontSize:BaseComponent.W*15/375,color:'#333333',marginLeft:BaseComponent.W*19/375}}>账号管理</Text>
                <View style={{height:BaseComponent.W*49/375,flexDirection:'row',alignItems:'center',justifyContent:'flex-end'}}>
                    <Text style={{fontSize:BaseComponent.W*15/375,color:'#999999'}}>其他登录方式</Text>
                    <Image style={{width:BaseComponent.W*25/375,height:BaseComponent.W*25/375,marginRight:BaseComponent.W*8/375}} source={require('../../image/home/youjiantou.png')}></Image>
                </View>   
            </View>
            </TouchableWithoutFeedback>  
            <View style={{width:'100%',height:0.5,backgroundColor:'#F0F0F0'}}></View>  

            {/*我的手机号*/}
            <TouchableWithoutFeedback onPress={()=>this.navigate('MinePhoneNo',{title:'我的手机号',userInfo:thiz.state.userInfo})}>
            <View style={{width:'100%',height:BaseComponent.W*49/375,flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
                <Text style={{fontSize:BaseComponent.W*15/375,color:'#333333',marginLeft:BaseComponent.W*19/375}}>我的手机号</Text>
                <View style={{height:BaseComponent.W*49/375,flexDirection:'row',alignItems:'center',justifyContent:'flex-end'}}>
                    <Text style={{fontSize:BaseComponent.W*15/375,color:'#999999'}}>{this.state.userInfo.phoneNo?this.state.userInfo.phoneNo:""}</Text>
                    <Image style={{width:BaseComponent.W*25/375,height:BaseComponent.W*25/375,marginRight:BaseComponent.W*8/375}} source={require('../../image/home/youjiantou.png')}></Image>
                </View>   
            </View>
            </TouchableWithoutFeedback>  
            <View style={{width:'100%',height:BaseComponent.W*10/375,backgroundColor:'#F0F0F0'}}></View>
            
            
            {/*我的收货地址*/}
            <TouchableWithoutFeedback onPress={()=>this.navigate('shouhuoAddress',{title:'收货地址'})}>
            <View style={{width:'100%',height:BaseComponent.W*49/375,flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
                <Text style={{fontSize:BaseComponent.W*15/375,color:'#333333',marginLeft:BaseComponent.W*19/375}}>我的收货地址</Text>
                <View style={{height:BaseComponent.W*49/375,flexDirection:'row',alignItems:'center',justifyContent:'flex-end'}}>
                    <Image style={{width:BaseComponent.W*25/375,height:BaseComponent.W*25/375,marginRight:BaseComponent.W*8/375}} source={require('../../image/home/youjiantou.png')}></Image>
                </View>   
            </View>
            </TouchableWithoutFeedback>  
            <View style={{flex:1,backgroundColor:'#F0F0F0'}}></View>
            
      </View>
    )
  }
}