/**
 * @name SelectShouhouGoods.js
 * @auhor 程浩
 * @date 2018.8.21
 * @desc 售后类型商品选择界面
 */
import React,{Component} from 'react';
import {Text,View,StyleSheet,Image,TouchableWithoutFeedback,ScrollView,FlatList,StatusBar,Modal,ActivityIndicator} from 'react-native';
// 导入工具函数库
import T from "../../lib/Tools";
// 导入公共样式
import St from "../../lib/Style";
// 导入图片资源库
import Imgs from "../../lib/Img";
import BaseComponent from '../BaseComponent';
import ApplyShouhou from './ApplyShouhou';
// 转菊花
import Spinner from 'react-native-loading-spinner-overlay';

// Tools实例
let ti = T.getInstance();

let data=[1,1];
let style = StyleSheet.create({
  wrapper:{
    backgroundColor:"white",
    flex:1,
    flexDirection:"column",
    alignItems:"stretch",
    justifyContent:"flex-start",
    position:"relative",
  },
  statusBar:{
    backgroundColor:"rgb(255,255,255)",
    height:ti.select({ios:16,android:22}),
    position:"relative",
    zIndex:1,
    opacity:1,
  },
  navBar:{
    backgroundColor:"rgb(255,255,255)",
    height:"7%",
    position:"relative",
    zIndex:1,
    opacity:1,
    borderBottomWidth:1,
    borderBottomColor:"rgb(241,241,241)"
  },
});
export default class SelectShouhouGoods extends BaseComponent{
  constructor(props){
    super(props);
    this.state={
      selectedGoods:{},//选中商品的数据
      data:"",//商品数据
      type:"",//售后类型
      next:false,//是否能点击下一步,false不能点击
      visible:false,//转菊花
      netError:false,//true表示网络错误
    }
  };  
  componentDidMount(){
      var thiz=this;
      var type=thiz.params.type;
      thiz.setState({type:type});
      thiz.getData();
  };
  //获取包裹里的商品信息
  getData=()=>{
    var thiz=this;
    var id=thiz.params.id;//拿到的是包裹的ID
    console.log("----------------------id-----------------------",id);
    thiz.setState({visible:true});
    thiz.request("orderPackage/getRefundOrderInfoItems",{
      method:"POST",
      data:{id:id},
      success:function(ret){
          if(ret.respCode=="00")
          {
            var data=ret.data;
            thiz.log("------------------------data---------------------------",data);
            for(var i=0;i<data.length;i++)
            {
              data[i].isSelected=false;
            }
            thiz.setState({
              data:data,
              visible:false
            });
          }
      },
      error:function(err){
        thiz.setState({visible:false});
        if(err.code&&err.code==200){
          console.log("11111111111111111111111111111111");
          thiz.setState({netError:true});
        }  
      }
    })
  }
  _keyExtractor=(item,index)=>index.toString();
  //渲染售后商品数据
  _renderItem=(info)=>{
    var thiz=this;
    return (
      <TouchableWithoutFeedback onPress={()=>{
        thiz.log("-----------info-----------",info);
        if(info.item.supportRefund)
        {

          var index=info.index;
          var data=thiz.state.data;
         
            for(var j=0;j<data.length;j++)
            {
              if(index!=j)
              {
                data[j].isSelected=false;
              }else{
                data[j].isSelected=!data[j].isSelected;
              }
            }
          
        
          thiz.setState({data:data,selectedGoods:info.item});
          thiz.log("--------------------------------------------------last_data----------------------",data);
          for(var i=0;i<data.length;i++)
          {
            if(data[i].isSelected)
            {
                thiz.setState({next:true});
                break;
            }else
            {
                thiz.setState({next:false,});
            }
          }
        }
        else
        {
          thiz.toast(""+info.item.notSupportRefundDesc+"");
        }
      }}>
      <View style={{width:'100%',height:BaseComponent.W*100/375,backgroundColor:'#ffffff',flexDirection:'row',alignItems:'center'}}>
          <View style={{width:BaseComponent.W*20/375,height:BaseComponent.W*20/375,borderRadius:BaseComponent.W*10/375,marginLeft:BaseComponent.W*10/375}}>
                <Image style={{width:BaseComponent.W*20/375,height:BaseComponent.W*20/375}} source={info.item.isSelected?require('../../image/home/xuanzhong.png'):require('../../image/home/weixuanzhong.png')}></Image>
          </View>
          <Image style={{width:BaseComponent.W*60/375,height:BaseComponent.W*60/375,marginLeft:BaseComponent.W*10/375}} source={{uri:info.item.sku.imageAttachment.resourceFullAddress?info.item.sku.imageAttachment.resourceFullAddress:""}}></Image>
          <View style={{width:BaseComponent.W*150/375,height:BaseComponent.W*60/375,marginLeft:BaseComponent.W*10/375,justifyContent:'space-between'}}>
              <Text style={{fontSize:BaseComponent.W*13/375,color:'#0A0A0A',lineHeight:BaseComponent.W*20/375}} numberOfLines={2}>{info.item.skuName}</Text>
              <Text style={{fontSize:BaseComponent.W*12/375,color:'#999999',marginTop:BaseComponent.W*10/375}}>{info.item.sku.goodsAttributeOptions[0].goodsAttributeOption.value}&nbsp;&nbsp;{info.item.sku.goodsAttributeOptions[1]?info.item.sku.goodsAttributeOptions[1].goodsAttributeOption.value:""}</Text>
          </View>
          <View style={{width:BaseComponent.W*105/375,height:BaseComponent.W*60/375,alignItems:'flex-end'}}>
              <Text style={{fontSize:BaseComponent.W*13/375,color:"#3A3A3A"}}>¥{info.item.buyUnitAmount}</Text>
              <Text style={{fontSize:BaseComponent.W*12/375,color:"#999999"}}>×{info.item.num}</Text>
          </View>
      </View>
      </TouchableWithoutFeedback>
    )
  };
  //下一步
  next=()=>{
    var thiz=this;
    var type=thiz.state.type;
    var selectedGoods=thiz.state.selectedGoods;
    if(thiz.state.next)
    {
        console.log("-------------------------type--------------------------",type);
        thiz.log("---------------------selectedGoods-----------------------",selectedGoods);
        if(type=="退货退款")
        {
          thiz.navigate('ApplyShouhou',{title:"申请售后",data:selectedGoods});
        }
        if(type=="仅退款")
        {
          thiz.navigate('ApplyShouhouRefund',{title:"申请售后",data:selectedGoods});
        }
    }
    else
    {
      console.log("------------------------不能点击----------------------------------");
    }  
    
  }
       render() {
        let thiz=this;
          return ( 
            <View style={style.wrapper}>
                {/*状态栏占位*/}
                <View style={{backgroundColor:this.state.visible?"rgba(14,14,14,0.5)":"rgb(255,255,255)",
                              height:this.isIphoneX(30,BaseComponent.SH),
                              position:"relative",
                              zIndex:1,
                              opacity:1,}}></View>
                {/*顶部导航栏*/}
                <View style={style.navBar}>
                  <View style={{flex:1,flexDirection:"row",justifyContent:"flex-start"}}>

                    <TouchableWithoutFeedback onPress={()=>this.goBack()}>
                    <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>
                      <Image source={Imgs.back} style={{width:(BaseComponent.W*0.127*0.4),height:(BaseComponent.W*0.127*0.4),marginRight:BaseComponent.W*0.04}}></Image>
                    </View>
                    </TouchableWithoutFeedback>

                    <View style={{flexGrow:1,backgroundColor:"transparent",
                      marginTop:ti.select({ios:(BaseComponent.H*0.02)/2,android:(BaseComponent.H*0.016)/2}),borderRadius:ti.select({ios:(BaseComponent.H*0.05)/2,android:(BaseComponent.H*0.054)/2}),borderColor:"#0A0A0A",
                      borderWidth:0,}}>
                      <Text style={{color:"#0A0A0A",width:"100%",textAlign:"center",marginTop:ti.select({ios:"4.5%",android:"1.6%"}),fontSize:BaseComponent.W*17/375}}>{this.params.title}</Text>
                      <Text style={{fontSize:BaseComponent.W*15/375,color:'#343434',position:'absolute',right:0,marginTop:ti.select({ios:BaseComponent.W*13.5/375,android:BaseComponent.W*6.5/375})}}>售后政策</Text>
                    </View>
                    <TouchableWithoutFeedback onPress={()=>{
                      // 跳转客服
                      thiz.goPage("Service",{title:"客服"});
                    }}>
                    <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>
                      <Image source={require('../../image/home/kefu.png')} style={{width:(BaseComponent.W*0.127*0.5),height:(BaseComponent.W*0.127*0.5),display:"flex"}}></Image>
                    </View>
                    </TouchableWithoutFeedback>

                  </View>
                </View>

                {/*转菊花*/}
                <Modal
                  visible={this.state.visible}
                  onRequestClose={()=>{this.setState({visible:false})}}
                  transparent={true}
                  animationType={"fade"}>
                  <View style={{backgroundColor:"rgba(14,14,14,0.5)",width:"100%",width:"100%",flex:1,flexDirection:"column",alignItems:"center",justifyContent:"center",position:"relative"}}>
                    <View>
                          <ActivityIndicator size="large" color='white'/>
                    </View>
                  </View>
                </Modal>

              {
                thiz.state.netError?(
                  <View style={{width:BaseComponent.W,height:BaseComponent.H*0.85,justifyContent:'center',alignItems:'center'}}>
                    <Image style={{width:BaseComponent.W*180/375,height:BaseComponent.W*180/375,borderRadius:BaseComponent.W*90/375}} source={require('../../image/mine/neterror.png')}></Image>
                    <TouchableWithoutFeedback onPress={()=>{
                      thiz.setState({netError:false});
                      thiz.getData();
                    }}>
                    <View style={{width:BaseComponent.W*70/375,height:BaseComponent.W*30/375,borderRadius:BaseComponent.W*15/375,borderWidth:0.5,borderColor:'#080808',
                              justifyContent:'center',alignItems:'center',marginTop:BaseComponent.W*15/375}}>
                          <Text style={{fontSize:BaseComponent.W*15/375,color:'#252525'}}>刷新</Text>          
                    </View>
                    </TouchableWithoutFeedback>
                  </View>
                      ):(
                      <View style={{flex:1}}>
                        {/*商品列表*/}
                        <View style={{flex:1,backgroundColor:'#F0F0F0'}}>
                            <FlatList
                                  data={this.state.data}
                                  keyExtractor={this._keyExtractor}
                                  renderItem={this._renderItem}
                                  showsVerticalScrollIndicator={false}
                                  extraData={this.state}  
                            />
                        </View>
                        {/*下一步*/}
                        
                        <TouchableWithoutFeedback onPress={()=>{this.next();}}>
                        <View style={{width:'100%',height:BaseComponent.W*50/375,backgroundColor:this.state.next?'#FED584':'#CCCCCC',justifyContent:'center',alignItems:'center'}}>
                              <Text style={{fontSize:BaseComponent.W*16/375,color:'#2D2D2D'}}>下一步</Text>
                        </View>
                        </TouchableWithoutFeedback> 
                      </View>
                      )
              }

               
            </View>    
          ) 
      }
} 