/**
 * @name Set.js
 * @auhor 程浩
 * @date 2018.10.8
 * @desc 设置界面
 */
import React,{Component} from 'react';
import {Text,View,StyleSheet,Image,TouchableWithoutFeedback,ScrollView,
ImageBackground,TextInput,StatusBar,Modal,FlatList,Picker,ActivityIndicator} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
// 导入工具函数库
import T from "../../lib/Tools";
// 导入公共样式
import St from "../../lib/Style";
// 导入图片资源库
import Imgs from "../../lib/Img";
import BaseComponent from '../BaseComponent';
import PersonalInfo from './PersonalInfo';
import AboutMe from './AboutMe';
import shouhuoAddress from './shouhuoAddress';


// Tools实例
let ti = T.getInstance();

let style = StyleSheet.create({
  wrapper:{
    backgroundColor:"white",
    flex:1,
    flexDirection:"column",
    alignItems:"stretch",
    justifyContent:"flex-start",
    position:"relative",
  },
  statusBar:{
    backgroundColor:"rgb(255,255,255)",
    height:ti.select({ios:16,android:22}),
    position:"relative",
    zIndex:1,
    opacity:1,
  },
  navBar:{
    backgroundColor:"rgb(255,255,255)",
    height:"7%",
    position:"relative",
    zIndex:1,
    opacity:1,
    borderBottomWidth:1,
    borderBottomColor:"rgb(241,241,241)"
  },
});
export default class Msgcenter extends BaseComponent{
constructor(props){
    super(props);
    this.state={
      isvisible:false,//清除缓存弹出框是否可见，false表示不可见
      userInfo:this.params.userInfo,
      isLogin:false,
      appVersion:BaseComponent.V,

      visible:false,
    }
}

  // 退出登录
  quitLogin(){
    let thiz = this;
    // 删除账号
    thiz.T.remove("account");
    thiz.T.remove("cacheUserInfo");
    thiz.emit("quit_login");

    thiz.setState({visible:true});
    thiz.request("user/toLogout",{
      method:"POST",
      success:function(ret){

        thiz.setState({visible:false});

        setTimeout(function(){
          thiz.goBack();
        },100);

      },
      error:function(err){
        
        thiz.setState({visible:false});
        
        setTimeout(function(){
          thiz.goBack();
        },100);
      }
    });

  }

  componentDidMount(){
    let thiz = this;

    // 先判断是否需要登录
    thiz.isLogin(function(ret,err){
          if(ret){
              thiz.setState({
                isLogin:true
              });
          }else{
              thiz.setState({
                isLogin:false
              });
          }
    });
  }

  render(){
    let thiz = this;
    return (
      <View style={style.wrapper}>   
                {/*状态栏占位*/}
                <View style={{ backgroundColor:this.state.isvisible?"rgba(14,14,14,0.5)":"rgba(255,255,255)",
                      height:thiz.isIphoneX(30,BaseComponent.SH),
                      position:"relative",
                      zIndex:1,
                      opacity:1,}}></View>
                {/*顶部导航栏*/}
              <View style={style.navBar}>
                <View style={{flex:1,flexDirection:"row",justifyContent:"flex-start"}}>

                    <TouchableWithoutFeedback onPress={()=>this.goBack()}>
                    <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>
                      <Image source={Imgs.back} style={{width:(BaseComponent.W*0.127*0.4),height:(BaseComponent.W*0.127*0.4),marginRight:BaseComponent.W*0.04}}></Image>
                    </View>
                    </TouchableWithoutFeedback>

                    <View style={{flexGrow:1,backgroundColor:"transparent",
                      marginTop:ti.select({ios:(BaseComponent.H*0.02)/2,android:(BaseComponent.H*0.016)/2}),borderRadius:ti.select({ios:(BaseComponent.H*0.05)/2,android:(BaseComponent.H*0.054)/2}),borderColor:"#0A0A0A",
                      borderWidth:0,}}>
                      <Text style={{color:"#0A0A0A",width:"100%",textAlign:"center",marginTop:ti.select({ios:"4.5%",android:"1.6%"}),fontSize:BaseComponent.W*17/375,marginLeft:'4.3%'}}>{this.params.title}</Text>
                    </View>
                    <View style={{width:"14%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center",marginRight:BaseComponent.W*5/375}}>
                        
                    </View>
                </View>
              </View>

              {/*转菊花*/}
                <Modal
                  visible={this.state.visible}
                  onRequestClose={()=>{this.setState({visible:false})}}
                  transparent={true}
                  animationType={"fade"}>
                  <View style={{backgroundColor:"rgba(0,0,0,0.4)",width:"100%",width:"100%",flex:1,flexDirection:"column",alignItems:"center",justifyContent:"center",position:"relative"}}>
                    <View>
                          <ActivityIndicator size="large" color='white'/>
                    </View>
                  </View>
                </Modal>
            
            {/*清除缓存弹出框*/}
            <Modal visible={this.state.isvisible}
                   transparent={true}
                   onRequestClose={() => {this.setState({isvisible:false})}}>
                   <View style={{flex:1,backgroundColor:'rgba(14,14,14,0.5)'}}>
                        <View style={{width:BaseComponent.W*355/375,height:BaseComponent.W*87/375,backgroundColor:'white',position:'absolute',bottom:0,borderRadius:BaseComponent.W*10/375,
                                      marginLeft:BaseComponent.W*10/375,marginBottom:BaseComponent.W*60/375}}>
                              <View style={{width:'100%',height:BaseComponent.W*43/375,justifyContent:'center',alignItems:'center'}}>
                                  <View style={{width:BaseComponent.W*299/375,height:BaseComponent.W*26/375}}>
                                      <Text style={{fontSize:BaseComponent.W*12/375,color:'#909090',textAlign:'center'}}>清除后，图片视频等多媒体消息需要重新下载查看，确定清除？</Text>
                                  </View>
                              </View>
                              <View style={{width:BaseComponent.W*347/375,height:0.5,backgroundColor:'#D6D6D6',marginLeft:BaseComponent.W*5/375}}></View>
                              <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                                  <Text style={{fontSize:BaseComponent.W*17/375,color:"#E31436"}}>清除</Text>
                              </View>
                        </View>
                        <TouchableWithoutFeedback onPress={()=>{
                          this.setState({isvisible:false})
                        }}>
                        <View style={{width:BaseComponent.W*355/375,height:BaseComponent.W*44/375,backgroundColor:'white',position:'absolute',bottom:0,borderRadius:BaseComponent.W*5/375,marginBottom:BaseComponent.W*9/375,
                                      marginLeft:BaseComponent.W*10/375,justifyContent:'center',alignItems:'center'}}>
                              <Text style={{fontSize:BaseComponent.W*17/375,color:'#3048FB'}}>取消</Text>          
                        </View>
                        </TouchableWithoutFeedback>
                   </View>
            </Modal>

          {/*个人信息*/}
          <TouchableWithoutFeedback onPress={()=>this.navigate("PersonalInfo",{title:'个人信息',userInfo:this.state.userInfo})}>
          <View style={{display:thiz.state.isLogin?"flex":"none",width:'100%',height:BaseComponent.W*49/375,flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
              <Text style={{fontSize:BaseComponent.W*15/375,color:'#333333',marginLeft:BaseComponent.W*10/375}}>个人信息</Text>
              <Image style={{width:BaseComponent.W*25/375,height:BaseComponent.W*25/375,marginRight:BaseComponent.W*8/375}} source={require('../../image/home/youjiantou.png')}></Image>
          </View>
          </TouchableWithoutFeedback>
          <View style={{display:thiz.state.isLogin?"flex":"none",width:'100%',height:0.5,backgroundColor:'#C8C7CC'}}></View> 

          {/*实名认证*/}
          {/*<View style={{width:'100%',height:BaseComponent.W*49/375,flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
              <Text style={{fontSize:BaseComponent.W*17/375,color:'#333333',marginLeft:BaseComponent.W*10/375}}>我的实名认证</Text>
              <Image style={{width:BaseComponent.W*25/375,height:BaseComponent.W*25/375,marginRight:BaseComponent.W*8/375}} source={require('../../image/home/youjiantou.png')}></Image>
          </View>
          <View style={{width:'100%',height:0.5,backgroundColor:'#C8C7CC'}}></View>*/} 

          {/*收货地址*/}
          <TouchableWithoutFeedback onPress={()=>this.navigate('shouhuoAddress',{title:"收货地址"})}>
          <View style={{display:thiz.state.isLogin?"flex":"none",width:'100%',height:BaseComponent.W*49/375,flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
              <Text style={{fontSize:BaseComponent.W*15/375,color:'#333333',marginLeft:BaseComponent.W*10/375}}>我的收货地址</Text>
              <Image style={{width:BaseComponent.W*25/375,height:BaseComponent.W*25/375,marginRight:BaseComponent.W*8/375}} source={require('../../image/home/youjiantou.png')}></Image>
          </View>
          </TouchableWithoutFeedback>
            

          {/*消息通知设置,第一期隐藏*/}
          <View style={{display:"none",width:'100%',height:BaseComponent.W*49/375,flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
              <Text style={{fontSize:BaseComponent.W*15/375,color:'#333333',marginLeft:BaseComponent.W*10/375}}>消息通知设置</Text>
              <Image style={{width:BaseComponent.W*25/375,height:BaseComponent.W*25/375,marginRight:BaseComponent.W*8/375}} source={require('../../image/home/youjiantou.png')}></Image>
          </View>
          <View style={{display:"none",width:'100%',height:0.5,backgroundColor:'#C8C7CC'}}></View>

          {/*清除缓存*/}
          <TouchableWithoutFeedback onPress={()=>this.setState({isvisible:true})}>
          <View style={{display:"none",width:'100%',height:BaseComponent.W*49/375,flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
              <Text style={{fontSize:BaseComponent.W*15/375,color:'#333333',marginLeft:BaseComponent.W*10/375}}>清除缓存</Text>
              <Image style={{width:BaseComponent.W*25/375,height:BaseComponent.W*25/375,marginRight:BaseComponent.W*8/375}} source={require('../../image/home/youjiantou.png')}></Image>
          </View>
          </TouchableWithoutFeedback>
          <View style={{width:'100%',height:BaseComponent.W*15/375,backgroundColor:'#F0F0F0'}}></View>

          {/*关于小洋匠*/}
          <TouchableWithoutFeedback onPress={()=>this.navigate('AboutMe',{title:"关于小洋匠"})}>
          <View style={{width:'100%',height:BaseComponent.W*49/375,flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
              <Text style={{fontSize:BaseComponent.W*15/375,color:'#333333',marginLeft:BaseComponent.W*10/375}}>关于小洋匠</Text>
              <View style={{height:BaseComponent.W*49/375,flexDirection:'row',alignItems:'center',justifyContent:'flex-end'}}>
                  <Text style={{fontSize:BaseComponent.W*16/375,color:'#999999'}}>v{thiz.state.appVersion}</Text>
                  <Image style={{width:BaseComponent.W*25/375,height:BaseComponent.W*25/375,marginRight:BaseComponent.W*8/375}} source={require('../../image/home/youjiantou.png')}></Image>
              </View>
          </View>
          </TouchableWithoutFeedback>
          <View style={{width:'100%',height:BaseComponent.W*15/375,backgroundColor:'#F0F0F0'}}></View>

           {/*退出当前账号*/}
          <TouchableWithoutFeedback onPress={()=>{this.quitLogin()}}>
          <View style={{display:thiz.state.isLogin?"flex":"none",width:'100%',height:BaseComponent.W*49/375,justifyContent:'center',alignItems:'center'}}>
              <Text style={{fontSize:BaseComponent.W*16/375,color:'#E31436'}}>退出当前账号</Text>
          </View>
          </TouchableWithoutFeedback>
          <View style={{flex:1,backgroundColor:'#F0F0F0'}}></View>
      </View>        
    )
  }
}  
