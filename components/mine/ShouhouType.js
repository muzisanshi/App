/**
 * @name ShouhouType.js
 * @auhor 程浩
 * @date 2018.8.21
 * @desc 售后类型选择界面
 */
import React,{Component} from 'react';
import {Text,View,StyleSheet,Image,TouchableWithoutFeedback,ScrollView,FlatList,StatusBar} from 'react-native';
// 导入工具函数库
import T from "../../lib/Tools";
// 导入公共样式
import St from "../../lib/Style";
// 导入图片资源库
import Imgs from "../../lib/Img";
import BaseComponent from '../BaseComponent';
import SelectShouhouGoods from './SelectShouhouGoods';
import ApplyShouhouRefund from './ApplyShouhouRefund';

// Tools实例
let ti = T.getInstance();


let style = StyleSheet.create({
  wrapper:{
    backgroundColor:"white",
    flex:1,
    flexDirection:"column",
    alignItems:"stretch",
    justifyContent:"flex-start",
    position:"relative",
  },
  statusBar:{
    backgroundColor:"rgb(255,255,255)",
    height:ti.select({ios:16,android:22}),
    position:"relative",
    zIndex:1,
    opacity:1,
  },
  navBar:{
    backgroundColor:"rgb(255,255,255)",
    height:"7%",
    position:"relative",
    zIndex:1,
    opacity:1,
    borderBottomWidth:1,
    borderBottomColor:"rgb(241,241,241)"
  },
});
export default class ShouhouType extends BaseComponent{
  constructor(props){
    super(props);
    this.state={
      id:"",//退货的包裹ID
    }
  };
  componentDidMount(){
    var thiz=this;
    var id=thiz.params.id;
    console.log("--------------------------------------id--------------------------------------",id);
    thiz.setState({id:id});
  };

       render() {
        let thiz=this;
          return ( 
            <View style={style.wrapper}>
                {/*状态栏占位*/}
                <View style={{backgroundColor:"rgb(255,255,255)",
                          height:this.isIphoneX(30,BaseComponent.SH),
                          position:"relative",
                          zIndex:1,
                          opacity:1,}}></View>
                {/*顶部导航栏*/}
                <View style={style.navBar}>
                  <View style={{flex:1,flexDirection:"row",justifyContent:"flex-start"}}>

                    <TouchableWithoutFeedback onPress={()=>this.goBack()}>
                    <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>
                      <Image source={Imgs.back} style={{width:(BaseComponent.W*0.127*0.4),height:(BaseComponent.W*0.127*0.4),marginRight:BaseComponent.W*0.04}}></Image>
                    </View>
                    </TouchableWithoutFeedback>

                    <View style={{flexGrow:1,backgroundColor:"transparent",
                      marginTop:ti.select({ios:(BaseComponent.H*0.02)/2,android:(BaseComponent.H*0.016)/2}),borderRadius:ti.select({ios:(BaseComponent.H*0.05)/2,android:(BaseComponent.H*0.054)/2}),borderColor:"#0A0A0A",
                      borderWidth:0,}}>
                      <Text style={{color:"#0A0A0A",width:"100%",textAlign:"center",marginTop:ti.select({ios:"4.5%",android:"1.6%"}),fontSize:BaseComponent.W*17/375}}>{this.params.title}</Text>
                      <Text style={{fontSize:BaseComponent.W*15/375,color:'#343434',position:'absolute',right:0,marginTop:ti.select({ios:BaseComponent.W*13.5/375,android:BaseComponent.W*6.5/375})}}>售后政策</Text>
                    </View>
                    <TouchableWithoutFeedback onPress={()=>{
                      // 跳转客服
                      thiz.goPage("Service",{title:"客服"});
                    }}>
                    <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>
                      <Image source={require('../../image/home/kefu.png')} style={{width:(BaseComponent.W*0.127*0.5),height:(BaseComponent.W*0.127*0.5),display:"flex"}}></Image>
                    </View>
                    </TouchableWithoutFeedback>

                  </View>
                </View>

              {/*退货退款*/}
              <TouchableWithoutFeedback onPress={()=>this.navigate('SelectShouhouGoods',{title:'选择售后商品',id:this.state.id,type:"退货退款"})}>
              <View style={{width:"100%",height:BaseComponent.W*100/375,flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
                    <View style={{width:BaseComponent.W*300/375,height:"100%",justifyContent:'center'}}>
                        <View style={{width:BaseComponent.W*140/375,height:BaseComponent.W*30/375,marginLeft:BaseComponent.W*16/375,flexDirection:'row',alignItems:'center'}}>
                            <Image style={{width:BaseComponent.W*30/375,height:BaseComponent.W*30/375}} source={require('../../image/home/tuihuoshouhou.png')}></Image>
                            <Text style={{fontSize:BaseComponent.W*17/375,color:'#1A1A1A',marginLeft:BaseComponent.W*13/375}}>退货退款</Text>
                        </View>
                        <Text style={{fontSize:BaseComponent.W*14/375,color:'#ABABAB',marginLeft:BaseComponent.W*16/375,marginTop:BaseComponent.W*10/375}}>已收到货，需要退还已收到的货物</Text>
                    </View>
                    <Image style={{width:BaseComponent.W*25/375,height:BaseComponent.W*25/375,marginRight:BaseComponent.W*11/375}} source={require('../../image/home/youjiantou.png')}></Image>
              </View>
              </TouchableWithoutFeedback>
              <View style={{width:'100%',height:0.5,backgroundColor:'#D0D0D0'}}></View>
              {/*仅退款*/}
              <TouchableWithoutFeedback onPress={()=>this.navigate('SelectShouhouGoods',{title:"选择售后商品",id:this.state.id,type:"仅退款"})}>
              <View style={{width:"100%",height:BaseComponent.W*100/375,flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
                    <View style={{width:BaseComponent.W*300/375,height:"100%",justifyContent:'center'}}>
                        <View style={{width:BaseComponent.W*140/375,height:BaseComponent.W*30/375,marginLeft:BaseComponent.W*16/375,flexDirection:'row',alignItems:'center'}}>
                            <Image style={{width:BaseComponent.W*30/375,height:BaseComponent.W*30/375}} source={require('../../image/home/tuikuan.png')}></Image>
                            <Text style={{fontSize:BaseComponent.W*17/375,color:'#1A1A1A',marginLeft:BaseComponent.W*13/375}}>仅退款</Text>
                        </View>
                        <Text style={{fontSize:BaseComponent.W*14/375,color:'#ABABAB',marginLeft:BaseComponent.W*16/375,marginTop:BaseComponent.W*10/375}}>未收到货，或与客服协商同意仅退款</Text>
                    </View>
                    <Image style={{width:BaseComponent.W*25/375,height:BaseComponent.W*25/375,marginRight:BaseComponent.W*11/375}} source={require('../../image/home/youjiantou.png')}></Image>
              </View>
              </TouchableWithoutFeedback>
              <View style={{flex:1,backgroundColor:'#F0F0F0'}}></View>
            </View>      
        )
    }               
}