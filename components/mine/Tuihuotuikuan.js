/**
 * @name Tuihuotuikuan.js
 * @auhor 程浩
 * @date 2018.8.23
 * @desc 订单退款售后
 */
import React,{Component} from 'react';
import {Text,View,StyleSheet,Image,TouchableWithoutFeedback,ScrollView,TextInput,StatusBar,BackHandler,Modal,ActivityIndicator,RefreshControl} from 'react-native';
 // 导入工具函数库
import T from "../../lib/Tools";
// 导入公共样式
import St from "../../lib/Style";
// 导入图片资源库
import Imgs from "../../lib/Img";
import BaseComponent from '../BaseComponent';
// 转菊花
import Spinner from 'react-native-loading-spinner-overlay';

// Tools实例
let ti = T.getInstance();

let style = StyleSheet.create({
  wrapper:{
    backgroundColor:"#F0F0F0",
    flex:1,
    flexDirection:"column",
    alignItems:"stretch",
    justifyContent:"flex-start",
    position:"relative",
  },
  statusBar:{
    backgroundColor:"rgb(255,255,255)",
    height:ti.select({ios:16,android:22}),
    position:"relative",
    zIndex:1,
    opacity:1,
  },
  navBar:{
    backgroundColor:"rgb(255,255,255)",
    height:"7%",
    position:"relative",
    zIndex:1,
    opacity:1,
    borderBottomWidth:1,
    borderBottomColor:"rgb(241,241,241)",
    flexDirection:'row',
    alignItems:'center',
  },
});
let data=[1,1];
export default class Tuihuotuikuan extends BaseComponent{
  constructor(props){
    super(props);
    this.state={
      visible:false,
      pageNumber:1,//当前页数
      pageSize:10,//每页的数据条数
      netError:false,//true表示网络错误
      data:[],//售后的数据
      hasNext:false,//是否有下一页
      isRefreshing:false,
      isvisible:false,
      item:"",
    }
  };
  componentDidMount(){
    var thiz=this;
    thiz.getData();
    //监听取消售后
    thiz.listen("cancel_afterSale",function(){
        thiz.getData();
    });
       //监听取消售后
    thiz.listen("cancel_afterSale_success",function(){
        setTimeout(function(){
          thiz.getData();
        },500);
    });
    
  };
  //获取数据
  getData=()=>{
    var thiz=this;
    thiz.setState({visible:true});
    thiz.request("afterSale/getApplyRefundPage",{
      method:"POST",
      data:{page:{pageSize:thiz.state.pageSize,
                pageNumber:thiz.state.pageNumber}},
      success:function(ret){
        if(ret.respCode=="00")
        {
          if(ret.data.records){
          thiz.setState({
            visible:false,
            data:ret.data.records,
            hasNext:ret.data.hasNext,
            isRefreshing:false,
          });
        }
        else{
          
          thiz.setState({isRefreshing:false,visible:false});
        }
        }
      },
      error:function(err){
        thiz.setState({visible:false,isRefreshing:false});
        if(err.code&&err.code==200){
            thiz.setState({netError:true});
        }  
      },
    });
  };
  //取消售后
  cancel=(item)=>{
    var thiz=this;
    thiz.log("-------------------------------------cancel------------------------------",item);

    thiz.setState({isvisible:true,item:item});

  };
  //下拉刷新
  onRefresh=()=>{
    var thiz=this;
    var state=thiz.state;
    state.data=[];
    state.pageNumber=1;
    state.isRefreshing=true;
    thiz.setState(state);
    thiz.getData();
  }
  render(){
    let thiz=this;
    return (
      <View style={style.wrapper}>

          {/*转菊花*/}
          <Modal
            visible={this.state.visible}
            onRequestClose={()=>{this.setState({visible:false})}}
            transparent={true}
            animationType={"fade"}>
            <View style={{backgroundColor:"rgba(14,14,14,0.5)",width:"100%",width:"100%",flex:1,flexDirection:"column",alignItems:"center",justifyContent:"center",position:"relative"}}>
              <View>
                    <ActivityIndicator size="large" color='white'/>
              </View>
            </View>
          </Modal>

            {/*提醒取消售后*/}
                <Modal visible={this.state.isvisible}
                       onRequestClose={()=>{this.setState({isvisible:false})}}
                       transparent={true}>
                       <View style={{flex:1,backgroundColor:'rgba(14,14,14,0.5)',justifyContent:'center',alignItems:'center'}}>
                          <View style={{width:BaseComponent.W*275/375,height:BaseComponent.W*105/375,backgroundColor:'rgba(255, 255, 255,1)',borderRadius:BaseComponent.W*20/375}}>
                              <View style={{width:'100%',height:BaseComponent.W*56/375,justifyContent:'center',alignItems:'center'}}>
                                <Text style={{fontSize:BaseComponent.W*15/375,color:'rgba(94, 94, 94, 1)'}}>确定要取消售后吗?</Text>
                              </View>
                              <View style={{width:'100%',height:0.5,backgroundColor:'rgba(189, 189, 189, 1)'}}></View>
                              <View style={{width:'100%',height:BaseComponent.W*49/375,flexDirection:'row'}}>
                              <TouchableWithoutFeedback onPress={()=>this.setState({isvisible:false})}> 
                                <View style={{width:'50%',height:BaseComponent.W*49/375,justifyContent:'center',alignItems:'center'}}>
                                  <Text style={{fontSize:BaseComponent.W*15/375,color:'rgba(94, 94, 94, 1)'}}>取消</Text>
                                </View>
                              </TouchableWithoutFeedback> 
                                <View style={{width:0.5,height:'100%',backgroundColor:'rgba(189, 189, 189, 1)'}}></View>
                                <TouchableWithoutFeedback onPress={()=>{

                                     thiz.setState({visible:true,isvisible:false});
                                      thiz.request("afterSale/cancel",{
                                        method:"POST",
                                        data:{id:thiz.state.item.id},
                                        success:function(ret){
                                          thiz.emit("cancel_afterSale");
                                          thiz.setState({visible:false});
                                        },
                                        error:function(err){
                                            thiz.toast(err.msg);
                                        }
                                      });
                                }}> 
                                <View style={{width:'50%',height:BaseComponent.W*49/375,justifyContent:'center',alignItems:'center'}}>
                                  <Text style={{fontSize:BaseComponent.W*15/375,color:'#FED584'}}>确认</Text>
                                </View>
                                </TouchableWithoutFeedback>
                              </View>
                          </View>
                       </View>
                </Modal>
          
          {/*状态栏占位*/}
          <View style={{backgroundColor:this.state.visible?"rgba(14,14,14,0.5)":"#fff",
                        height:BaseComponent.SH,
                        position:"relative",
                        zIndex:1,
                        opacity:1,}}></View>
          {/*顶部导航栏*/}
          <View style={style.navBar}>
            <View style={{flex:1,flexDirection:"row",justifyContent:"flex-start",alignItems:'center'}}>

              <TouchableWithoutFeedback onPress={()=>this.goBack()}>
              <View style={{width:"15%",height:"100%",backgroundColor:"transparent",justifyContent:"center",marginLeft:BaseComponent.W*10/375}}>
                <Image source={Imgs.back} style={{width:(BaseComponent.W*0.127*0.4),height:(BaseComponent.W*0.127*0.4),marginBottom:ti.select({ios:BaseComponent.W*7/375,android:5})}}></Image>
              </View>
              </TouchableWithoutFeedback>

              <View style={{flexGrow:1,backgroundColor:"transparent",borderWidth:0}}>
                <Text style={{color:"#0A0A0A",width:"100%",textAlign:"center",marginTop:ti.select({ios:"4%",android:"1.6%"}),fontSize:BaseComponent.W*18/375}}>{this.params.title}</Text>
              </View>
               
              <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center",}}>
                  <Text style={{fontSize:BaseComponent.W*0.037,color:'black'}}></Text>
              </View>
            </View>
          </View>
          {
            thiz.state.netError?(
                <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                  <Image style={{width:BaseComponent.W*180/375,height:BaseComponent.W*180/375,borderRadius:BaseComponent.W*90/375}} source={require('../../image/mine/neterror.png')}></Image>
                  <TouchableWithoutFeedback onPress={()=>{
                    thiz.setState({netError:false});
                    thiz.getData();
                  }}>
                  <View style={{width:BaseComponent.W*70/375,height:BaseComponent.W*30/375,borderRadius:BaseComponent.W*15/375,borderWidth:0.5,borderColor:'#080808',
                            justifyContent:'center',alignItems:'center',marginTop:BaseComponent.W*15/375}}>
                        <Text style={{fontSize:BaseComponent.W*15/375,color:'#252525'}}>刷新</Text>          
                    </View>
                  </TouchableWithoutFeedback>
                </View>
              ):
              (thiz.state.data.length==0?( 
                  <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                    <Image style={{width:BaseComponent.W*180/375,height:BaseComponent.W*180/375,borderRadius:BaseComponent.W*90/375}} source={require('../../image/mine/Noaftersales.png')}></Image>
                  </View>
                  ):(
              <View style={{flex:1}}>
                   <ScrollView showsVerticalScrollIndicator={false}
                              
                             style={{backgroundColor:'#F0F0F0'}}
                              onMomentumScrollEnd={(e)=>
                              {
                                var offsetY = e.nativeEvent.contentOffset.y; //滑动距离
                                    var contentSizeHeight = e.nativeEvent.contentSize.height; //scrollView contentSize高度
                                    var oriageScrollHeight = e.nativeEvent.layoutMeasurement.height; //scrollView高度
                                     thiz.log("---------------------滑动距离-------------------",offsetY);
                                     thiz.log("---------------------scrollView contentSize高度-------------------",contentSizeHeight);
                                     thiz.log("---------------------scrollView高度-------------------",oriageScrollHeight);
                                    if (offsetY + oriageScrollHeight >= contentSizeHeight)
                                    {
                                      var pageNumber=thiz.state.pageNumber+1;
                                         thiz.log("------------------------------------------Second_PageNumber------------------------------------------------------",pageNumber);
                                          var hasNext=thiz.state.hasNext;
                                         thiz.log("----------------------------------hasNext----------------------------------------",hasNext);
                                          if(hasNext){
                                            thiz.setState({
                                              pageNumber:pageNumber,
                                              visible:true,
                                            });
                                            thiz.request("afterSale/getApplyRefundPage",{
                                            method:"POST",
                                            data:{page:{pageSize:thiz.state.pageSize,
                                                  pageNumber:pageNumber}},
                                            success:function(ret){
                                              thiz.setState({visible:false});
                                              var data=thiz.state.data;
                                              thiz.log("-------------------------------------Tuihuotuikuan_data-----------------------------",data);
                                              thiz.log("---------------------Tuihuotuikuan_ret----------------------",ret);
                                               if(ret.respCode=="00")
                                                  {
                                                    // data.push(ret.data.records);
                                                    if(ret.data.records)
                                                    {
                                                      for(var i=0;i<ret.data.records.length;i++)
                                                      {
                                                        data.push(ret.data.records[i]);
                                                      }
                                                    }
                                                    thiz.setState({visible:false,data:data,hasNext:ret.data.hasNext});
                                                    thiz.log("-------------------------------------Tuihuotuikuan_second_data----------------------",data);
                                                  }
                                            },
                                            error:function(error){
                                               thiz.setState({visible:false});  
                                            }
                                          });
                                        }
                                        else
                                        {
                                        // thiz.toast("已经到底了，没有更多数据了");
                                        }
                                    }
                              }}
                              refreshControl={
                                  <RefreshControl
                                    refreshing={thiz.state.isRefreshing}
                                    onRefresh={()=>{thiz.onRefresh()}}
                                    colors={['#ff0000', '#00ff00','#0000ff','#3ad564']}
                                    progressBackgroundColor="#ffffff"
                                  />
                                }>
                        {
                          thiz.state.data.map((item,index)=>{
                            
                            return (
                              <TouchableWithoutFeedback onPress={()=>{
                                thiz.log("---------------------item--------------------",item);
                                thiz.navigate('AfterSalesDetails',{title:'售后详情',serialNo:item.serialNo});
                              }}>
                                  <View key={index} style={{backgroundColor:"#fff"}}>
                                    <View style={{width:'100%',height:BaseComponent.W*10/375,backgroundColor:'#F0F0F0'}}></View> 
                                     <View style={{width:'100%',height:BaseComponent.W*40/375,flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
                                        <Text style={{fontSize:BaseComponent.W*12/375,color:'#626262',marginLeft:BaseComponent.W*12/375}}>申请单号:&nbsp;&nbsp;{item.serialNo}</Text>
                                        <Text style={{fontSize:BaseComponent.W*14/375,color:'#626262',marginRight:BaseComponent.W*12/375}}>
                                            {(()=>{
                                              if(item.applyRefundProgress=="NOT_AUDIT"){return "审核中"};
                                              if(item.applyRefundProgress=="AUDIT_NOT_PASS"){return "审核未通过"};
                                              if(item.applyRefundProgress=="REFUND_PROCESSING"||item.applyRefundProgress=="AUDIT_PASS"){return "退款中"};
                                              if(item.applyRefundProgress=="CANCELED"){return "已关闭"};
                                              if(item.applyRefundProgress=="REFUND_SUCCESS"){return "已完成"};
                                            })()} 
                                        </Text>
                                     </View>
                                     <View style={{width:BaseComponent.W*365/375,height:0.5,marginLeft:BaseComponent.W*10/375,backgroundColor:'#D0D0D0'}}></View>

                                     <View style={{width:'100%',height:BaseComponent.W*87/375,flexDirection:'row',alignItems:'center'}}>
                                        <Image style={{width:BaseComponent.W*60/375,height:BaseComponent.W*60/375,marginLeft:BaseComponent.W*11/375}} source={{uri:item.applyRefundRefOrderInfoItems[0].orderInfoItem.sku.imageAttachment.resourceFullAddress?item.applyRefundRefOrderInfoItems[0].orderInfoItem.sku.imageAttachment.resourceFullAddress:''}}></Image>
                                        
                                        <View style={{width:BaseComponent.W*200/375,height:BaseComponent.W*60/375,marginLeft:BaseComponent.W*10/375,justifyContent:'space-between'}}>
                                            <Text style={{fontSize:BaseComponent.W*13/375,color:'#818181',lineHeight:BaseComponent.W*20/375}} numberOfLines={2}>{item.applyRefundRefOrderInfoItems[0].orderInfoItem.skuName}</Text>
                                            <Text style={{fontSize:BaseComponent.W*12/375,color:'#999899',marginTop:BaseComponent.W*5/375}}>{item.applyRefundRefOrderInfoItems[0].orderInfoItem.sku.goodsAttributeOptions[0].goodsAttributeOption.value}&nbsp;&nbsp;{item.applyRefundRefOrderInfoItems[0].orderInfoItem.sku.goodsAttributeOptions[1]?item.applyRefundRefOrderInfoItems[0].orderInfoItem.sku.goodsAttributeOptions[1].goodsAttributeOption.value:""}</Text>
                                        </View>

                                        <View style={{width:BaseComponent.W*60/375,height:BaseComponent.W*60/375,flexDirection:'column',alignItems:'flex-end',position:'absolute',right:BaseComponent.W*12/375}}>
                                            <Text style={{fontSize:BaseComponent.W*13/375,color:'#3A3A3A'}}>¥&nbsp;{item.applyRefundRefOrderInfoItems[0].orderInfoItem.buyUnitAmount}</Text>
                                            <Text style={{fontSize:BaseComponent.W*12/375,color:'#666666'}}>x{item.applyRefundRefOrderInfoItems[0].orderInfoItem.num}</Text>
                                        </View>  
                                     </View>
                                     <View style={{width:BaseComponent.W*365/375,height:0.5,marginLeft:BaseComponent.W*10/375,backgroundColor:'#D0D0D0'}}></View>
                                     
                                     <View style={{width:'100%',height:BaseComponent.W*75/375}}>
                                        <View style={{width:'100%',height:BaseComponent.W*40/375,flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
                                            <Text style={{fontSize:BaseComponent.W*13/375,color:'#626262',marginLeft:BaseComponent.W*11/375}}>申请退款价格:{item.applyRefundAmount}</Text>
                                            <Text style={{fontSize:BaseComponent.W*13/375,color:'#626262',marginRight:BaseComponent.W*12/375,display:item.applyRefundProgress=="REFUND_SUCCESS"?'flex':'none'}}>实际退款价格:{item.agreeRefundAmount}</Text>
                                        </View>
                                        <View style={{width:'100%',height:BaseComponent.W*40/375,flexDirection:'row',justifyContent:'space-between'}}>
                                            <Text style={{fontSize:BaseComponent.W*11/375,color:'#626262',marginLeft:BaseComponent.W*11/375,marginTop:BaseComponent.W*7/375}}>申请时间&nbsp;&nbsp;{item.applyRefundDatetime}</Text>
                                            
                                            <TouchableWithoutFeedback onPress onPress={()=>{
                                                  this.cancel(item);
                                            }}>
                                            <View style={{display:item.applyRefundProgress=="NOT_AUDIT"?'flex':'none',width:BaseComponent.W*70/375,height:BaseComponent.W*30/375,borderRadius:BaseComponent.W*15/375,borderWidth:1,borderColor:'#606060',justifyContent:'center',alignItems:'center',marginRight:BaseComponent.W*12/375}}>
                                                <Text style={{fontSize:BaseComponent.W*13/375,color:'#252525'}}>取消售后</Text>
                                            </View>
                                            </TouchableWithoutFeedback>

                                            <Text style={{fontSize:BaseComponent.W*12/375,color:'#626262',marginRight:BaseComponent.W*12/375,marginTop:BaseComponent.W*7/375,display:item.applyRefundProgress=="AUDIT_NOT_PASS"?'flex':'none'}}>未通过原因：{item.rejectReason}</Text>
                                        </View>
                                     </View>

                                     
                                  </View>
                              </TouchableWithoutFeedback>    
                            )
                          })
                        }
                        {
                          thiz.state.hasNext?null:thiz.nomore({bgcolor:'#F0F0F0'})
                        }
                        </ScrollView>
              </View>
              )
              )
          }
         
      </View>          
    )
  }
}
