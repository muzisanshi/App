/**
 * @name UpdateNickname.js
 * @auhor 程浩
 * @date 2018.10.8
 * @desc 修改昵称界面
 */
import React,{Component} from 'react';
import {Text,View,StyleSheet,Image,TouchableWithoutFeedback,ScrollView,
ImageBackground,TextInput,StatusBar,Modal,FlatList,Picker} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
// 导入工具函数库
import T from "../../lib/Tools";
// 导入公共样式
import St from "../../lib/Style";
// 导入图片资源库
import Imgs from "../../lib/Img";
import BaseComponent from '../BaseComponent';
import PersonalInfo from './PersonalInfo';



// Tools实例
let ti = T.getInstance();

let style = StyleSheet.create({
  wrapper:{
    backgroundColor:"white",
    flex:1,
    flexDirection:"column",
    alignItems:"stretch",
    justifyContent:"flex-start",
    position:"relative",
  },
  statusBar:{
    backgroundColor:"rgb(255,255,255)",
    height:ti.select({ios:16,android:22}),
    position:"relative",
    zIndex:1,
    opacity:1,
  },
  navBar:{
    backgroundColor:"rgb(255,255,255)",
    height:"7%",
    position:"relative",
    zIndex:1,
    opacity:1,
    borderBottomWidth:1,
    borderBottomColor:"rgb(241,241,241)"
  },
});

export default class Msgcenter extends BaseComponent{
constructor(props){
    super(props);
    this.state={
      
      userInfo:""
    }
}
componentDidMount(){
  var thiz=this;
  var userInfo=thiz.params.userInfo;
  thiz.log("-----------------userInfo------------------",userInfo);
  thiz.setState({userInfo:userInfo});
};
  render(){
    let thiz=this;
    return (
      <View style={style.wrapper}>   
                {/*状态栏占位*/}
                <View style={{ backgroundColor:"rgba(255,255,255)",
                      height:ti.select({ios:16,android:StatusBar.currentHeight}),
                      position:"relative",
                      zIndex:1,
                      opacity:1,}}></View>
                {/*顶部导航栏*/}
              <View style={style.navBar}>
                <View style={{flex:1,flexDirection:"row",justifyContent:"flex-start"}}>

                    <TouchableWithoutFeedback onPress={()=>this.goBack()}>
                    <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>
                      <Image source={Imgs.back} style={{width:(BaseComponent.W*0.127*0.4),height:(BaseComponent.W*0.127*0.4),marginTop:ti.select({ios:BaseComponent.W*0.02,android:0}),marginRight:BaseComponent.W*0.04}}></Image>
                    </View>
                    </TouchableWithoutFeedback>

                    <View style={{flexGrow:1,backgroundColor:"transparent",
                      marginTop:ti.select({ios:(BaseComponent.H*0.02)/2,android:(BaseComponent.H*0.016)/2}),borderRadius:ti.select({ios:(BaseComponent.H*0.05)/2,android:(BaseComponent.H*0.054)/2}),borderColor:"#0A0A0A",
                      borderWidth:0,}}>
                      <Text style={{color:"#0A0A0A",width:"100%",textAlign:"center",marginTop:ti.select({ios:"4%",android:"1.6%"}),fontSize:BaseComponent.W*17/375,marginLeft:'4.3%'}}>{this.params.title}</Text>
                    </View>
                    <View style={{width:"14%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center",marginRight:BaseComponent.W*5/375}}>
                        
                    </View>
                </View>
              </View>
        
        <TextInput style={{width:'100%',height:BaseComponent.W*49/375,backgroundColor:'white',marginTop:BaseComponent.W*10/375,fontSize:BaseComponent.W*17/375,color:'#0A0A0A',marginLeft:BaseComponent.W*16/375}} 
                    underlineColorAndroid='transparent' maxLength={10} defaultValue={this.state.userInfo.nickname} onSubmitEditing={(nickname)=>{
                        thiz.request("user/updateUserInfo",{
                          method:"POST",
                          data:{
                            nickname:nickname.nativeEvent.text,
                            personalitySignature:thiz.state.userInfo.personalitySignature,
                            sexual:thiz.state.userInfo.sexual,
                          },
                          success:function(ret){
                              thiz.emit("modify_user_info_success");
                              thiz.goBack();
                          },
                          error:function(err){

                          }
                        }) 
                    }}></TextInput>
        <View style={{width:'100%',height:'100%',backgroundColor:'#F0F0F0'}}></View>     
      </View>
    )
  }
}              