/**
 * @name Daishouhuo.js
 * @auhor 程浩
 * @date 2018.8.23
 * @desc 订单待付款
 */
import React,{Component} from 'react';
import {Text,View,StyleSheet,Image,TouchableWithoutFeedback,ScrollView,SectionList,RefreshControl,ActivityIndicator,Modal,StatusBar} from 'react-native';

import BaseComponent from '../BaseComponent';
// import DingdanXiangqingDaizhifu from './DingdanXiangqingDaizhifu';
// 转菊花
import Spinner from 'react-native-loading-spinner-overlay';

import Tools from "../../lib/Tools";
let ti = Tools.getInstance();

export default class Daishouhuo extends BaseComponent{
 constructor(props){
    super(props);
    this.state={
      pageNumber:1,//当前页数
      pageSize:10,//每页的数据条数
      hasNext:true,//是否上拉加载更多时有下一页数据
      visible:false,//转菊花
      pageElements:0,
      totalPages:1,//总页数
      totalElements:0,
      hasPrevious:false,
      dingdanData:[],
      isRefreshing:false,//下拉刷新
      shouhuovisible:false,//提醒确认收货
      id:"",//确认收货包裹的id
      netError:false,//true表示网络错误
    }
  };
  componentDidMount(){
      var thiz=this;
      thiz.getData();
      //监听包裹确认收货
      thiz.listen("orderPackage_confirmReceived",function(){
        thiz.setState({dingdanData:[]});
        thiz.getData();
      });
      //监听订单确认收货
      thiz.listen('confirm_Received',function(){
        thiz.setState({dingdanData:[]});
        thiz.getData();
      })
  }
  //加载数据
  getData=()=>{
    var thiz=this;
    thiz.setState({visible:true});
    // thiz.request("order/getPage",{
    thiz.request("order/getPageWithRepertory",{
      method:"POST",
      data:{orderStatus:"WAIT_FOR_RECEIVED",page:{pageSize:thiz.state.pageSize,
            pageNumber:thiz.state.pageNumber}},
      success:function(ret){
        if(!ret.data.records||ret.data.records.length==0)
        {
          thiz.setState({visible:false});
          return ;
        }
        //获取数据变成我想要的结构

        if(ret.data.records)
        { 
          var dingdanData=thiz.state.dingdanData;

          for(var i=0;i<ret.data.records.length;i++)
          {
            var records={};
            records.id=ret.data.records[i].id;
            records.businessOrderNo=ret.data.records[i].businessOrderNo;
            records.orderStatus=ret.data.records[i].orderStatus;
            records.orderPackages=[];
            
            // 根据仓库遍历
            for(var k = 0;k<ret.data.records[i].repertoryList.length;k++){
              // 遍历仓库中所有的包裹
              for(var ii=0;ii<ret.data.records[i].repertoryList[k].orderPackages.length;ii++)
              {
                // 重构单个包裹
                var orderPackages={};
                orderPackages.id=ret.data.records[i].repertoryList[k].orderPackages[ii].id;
                orderPackages.name=ret.data.records[i].repertoryList[k].orderPackages[ii].name;
                orderPackages.receiverReceivingStatus=ret.data.records[i].repertoryList[k].orderPackages[ii].receiverReceivingStatus;
                orderPackages.deliveryStatus=ret.data.records[i].repertoryList[k].orderPackages[ii].deliveryStatus;

                orderPackages.logisticDetail=ret.data.records[i].repertoryList[k].orderPackages[ii].logisticDetail;
                orderPackages.internalLogisticNo=ret.data.records[i].repertoryList[k].orderPackages[ii].internalLogisticNo;
                orderPackages.logisticState=ret.data.records[i].repertoryList[k].orderPackages[ii].logisticState;
                orderPackages.latestLogisticFlowLine=ret.data.records[i].repertoryList[k].orderPackages[ii].latestLogisticFlowLine;

                // 给单个包裹加上收货人信息
                orderPackages.reciverInfo = ret.data.records[i].orderDetail;
                
                // 给单个包裹加上包裹里面所有商品的信息
                orderPackages.orderInfoItems=[];
                for(var iii=0;iii<ret.data.records[i].repertoryList[k].orderPackages[ii].orderInfoItems.length;iii++)
                {
                  if(!ret.data.records[i].repertoryList[k].orderPackages[ii].orderInfoItems[iii].sku)
                  {
                    ret.data.records[i].repertoryList[k].orderPackages[ii].orderInfoItems[iii].sku={};
                    if(!ret.data.records[i].repertoryList[k].orderPackages[ii].orderInfoItems[iii].sku.imageAttachment)
                    {
                      ret.data.records[i].repertoryList[k].orderPackages[ii].orderInfoItems[iii].sku.imageAttachment={};
                      if(!ret.data.records[i].repertoryList[k].orderPackages[ii].orderInfoItems[iii].sku.imageAttachment.resourceFullAddress){
                        ret.data.records[i].repertoryList[k].orderPackages[ii].orderInfoItems[iii].sku.imageAttachment.resourceFullAddress="dasda";
                      }
                    }
                  }

                  // 重构单个商品信息
                  var orderInfoItems={};
                  orderInfoItems.id=ret.data.records[i].repertoryList[k].orderPackages[ii].orderInfoItems[iii].id;
                  orderInfoItems.num=ret.data.records[i].repertoryList[k].orderPackages[ii].orderInfoItems[iii].num;
                  orderInfoItems.buyUnitAmount=ret.data.records[i].repertoryList[k].orderPackages[ii].orderInfoItems[iii].buyUnitAmount;
                  orderInfoItems.img=ret.data.records[i].repertoryList[k].orderPackages[ii].orderInfoItems[iii].sku.imageAttachment.resourceFullAddress;
                  orderInfoItems.skuName=ret.data.records[i].repertoryList[k].orderPackages[ii].orderInfoItems[iii].skuName;
                  orderInfoItems.goodsAttributeOptions=ret.data.records[i].repertoryList[k].orderPackages[ii].orderInfoItems[iii].sku.goodsAttributeOptions;
                  orderPackages.orderInfoItems.push(orderInfoItems);
                }

                records.orderPackages.push(orderPackages);
              }
            }

            // 加入重构的订单项
            dingdanData.push(records);

          }
          thiz.log("------------dingdanData-----------------",dingdanData);
          thiz.log("--------end_time2--------",(new Date()).getTime());
          thiz.setState({
            dingdanData:dingdanData,
            isRefreshing:false,
            visible:false,
            hasNext:ret.data.hasNext
          });
        }
      },
      error:function(err){
          thiz.setState({visible:false,isRefreshing:false});
          if(err.code&&err.code==200){
          console.log("11111111111111111111111111111111");
          thiz.setState({netError:true});
        }  
      }
    })
  };
 //下拉刷新
  onRefresh=()=>{
    var thiz=this;
    var state=thiz.state;
    state.dingdanData=[];
    state.pageNumber=1;
    state.isRefreshing=true;
    thiz.setState(state);
    thiz.getData();
  };

  render(){
    let thiz=this;
    return (
      <View style={{backgroundColor:'#fff'}}>
      <StatusBar translucent={true} backgroundColor={this.state.visible||this.state.shouhuovisible?"rgba(14,14,14,0.5)":"rgb(255,255,255)"} barStyle={'dark-content'}/>
       
        {/*转菊花*/}
        <Modal
          visible={this.state.visible}
          onRequestClose={()=>{this.setState({visible:false})}}
          transparent={true}
          animationType={"fade"}>
          <View style={{backgroundColor:"rgba(14,14,14,0.5)",width:"100%",width:"100%",flex:1,flexDirection:"column",alignItems:"center",justifyContent:"center",position:"relative"}}>
            <View>
                  <ActivityIndicator size="large" color='white'/>
            </View>
          </View>
        </Modal>

           {/*提醒确认收货订单*/}
                <Modal visible={this.state.shouhuovisible}
                       onRequestClose={()=>{this.setState({shouhuovisible:false})}}
                       transparent={true}>
                       <View style={{flex:1,backgroundColor:'rgba(14,14,14,0.5)',justifyContent:'center',alignItems:'center'}}>
                          <View style={{width:BaseComponent.W*275/375,height:BaseComponent.W*105/375,backgroundColor:'rgba(255, 255, 255,1)',borderRadius:BaseComponent.W*20/375}}>
                              <View style={{width:'100%',height:BaseComponent.W*56/375,justifyContent:'center',alignItems:'center'}}>
                                <Text style={{fontSize:BaseComponent.W*15/375,color:'#3a3a3a'}}>确认收货吗?</Text>
                              </View>
                              <View style={{width:'100%',height:0.5,backgroundColor:'rgba(189, 189, 189, 1)'}}></View>
                              <View style={{width:'100%',height:BaseComponent.W*49/375,flexDirection:'row'}}>
                              <TouchableWithoutFeedback onPress={()=>this.setState({shouhuovisible:false})}> 
                                <View style={{width:'50%',height:BaseComponent.W*49/375,justifyContent:'center',alignItems:'center'}}>
                                  <Text style={{fontSize:BaseComponent.W*15/375,color:'rgba(94, 94, 94, 1)'}}>取消</Text>
                                </View>
                              </TouchableWithoutFeedback> 
                                <View style={{width:0.5,height:'100%',backgroundColor:'rgba(189, 189, 189, 1)'}}></View>
                                <TouchableWithoutFeedback onPress={()=>{
                                      // 包裹确认收货
                                      thiz.request("orderPackage/confirmReceived",{
                                        method:"POST",
                                        data:{id:thiz.state.id},
                                        success:function(ret){
                                          thiz.setState({shouhuovisible:false});
                                          thiz.emit('orderPackage_confirmReceived');
                                              console.log("1111111111111111111111111111");
                                        },
                                        error:function(err){

                                        }
                                      })
                                }}> 
                                <View style={{width:'50%',height:BaseComponent.W*49/375,justifyContent:'center',alignItems:'center'}}>
                                  <Text style={{fontSize:BaseComponent.W*15/375,color:'#FED584'}}>确认收货</Text>
                                </View>
                                </TouchableWithoutFeedback>
                              </View>
                          </View>
                       </View>
                </Modal>

                {
                  thiz.state.netError?(
                   <View style={{width:BaseComponent.W,height:BaseComponent.H*0.85,justifyContent:'center',alignItems:'center'}}>
                      <Image style={{width:BaseComponent.W*180/375,height:BaseComponent.W*180/375,borderRadius:BaseComponent.W*90/375}} source={require('../../image/mine/neterror.png')}></Image>
                      <TouchableWithoutFeedback onPress={()=>{
                        thiz.setState({netError:false});
                        thiz.getData();
                      }}>
                      <View style={{width:BaseComponent.W*70/375,height:BaseComponent.W*30/375,borderRadius:BaseComponent.W*15/375,borderWidth:0.5,borderColor:'#080808',
                                justifyContent:'center',alignItems:'center',marginTop:BaseComponent.W*15/375}}>
                            <Text style={{fontSize:BaseComponent.W*15/375,color:'#252525'}}>刷新</Text>          
                      </View>
                      </TouchableWithoutFeedback>
                    </View>
                    ):(thiz.state.dingdanData.length==0?(
                        <View style={{width:BaseComponent.W,height:BaseComponent.H*0.85,justifyContent:'center',alignItems:'center'}}>
                              <Image style={{width:BaseComponent.W*180/375,height:BaseComponent.W*180/375,borderRadius:BaseComponent.W*90/375}} source={require('../../image/mine/noOrder.png')}></Image>
                        </View>
                    ):(
                        <View>
                          <ScrollView showsVerticalScrollIndicator={false}
                                 style={{backgroundColor:'#F0F0F0'}}
                                
                                onMomentumScrollEnd={(e)=>{
                                 var offsetY = e.nativeEvent.contentOffset.y; //滑动距离
                                  var contentSizeHeight = e.nativeEvent.contentSize.height; //scrollView contentSize高度
                                  var oriageScrollHeight = e.nativeEvent.layoutMeasurement.height; //scrollView高度
                                  console.log("---------------------滑动距离-------------------",offsetY);
                                  console.log("---------------------scrollView contentSize高度-------------------",contentSizeHeight);
                                  console.log("---------------------scrollView高度-------------------",oriageScrollHeight);
                                  if (Math.abs((offsetY + oriageScrollHeight) - contentSizeHeight)<5)
                                  {
                                    var pageNumber=thiz.state.pageNumber+1;
                                        console.log("------------------------------------------Second_PageNumber------------------------------------------------------",pageNumber);
                                        var hasNext=thiz.state.hasNext;
                                        console.log("----------------------------------hasNext----------------------------------------",hasNext);
                                        if(hasNext){
                                          thiz.setState({
                                            pageNumber:pageNumber,
                                            visible:true,
                                          });
                                          // thiz.request("order/getPage",{
                                          thiz.request("order/getPageWithRepertory",{
                                          method:"POST",
                                          data:{orderStatus:"WAIT_FOR_RECEIVED",page:{pageSize:thiz.state.pageSize,
                                                pageNumber:pageNumber}},
                                          success:function(ret){
                                            //获取数据变成我想要的结构
                                            if(ret.data.records)
                                            { 
                                              var dingdanData=thiz.state.dingdanData;

                                              for(var i=0;i<ret.data.records.length;i++)
                                              {
                                                var records={};
                                                records.id=ret.data.records[i].id;
                                                records.businessOrderNo=ret.data.records[i].businessOrderNo;
                                                records.orderStatus=ret.data.records[i].orderStatus;
                                                records.orderPackages=[];
                                                
                                                // 根据仓库遍历
                                                for(var k = 0;k<ret.data.records[i].repertoryList.length;k++){
                                                  // 遍历仓库中所有的包裹
                                                  for(var ii=0;ii<ret.data.records[i].repertoryList[k].orderPackages.length;ii++)
                                                  {
                                                    // 重构单个包裹
                                                    var orderPackages={};
                                                    orderPackages.id=ret.data.records[i].repertoryList[k].orderPackages[ii].id;
                                                    orderPackages.name=ret.data.records[i].repertoryList[k].orderPackages[ii].name;
                                                    orderPackages.receiverReceivingStatus=ret.data.records[i].repertoryList[k].orderPackages[ii].receiverReceivingStatus;
                                                    orderPackages.deliveryStatus=ret.data.records[i].repertoryList[k].orderPackages[ii].deliveryStatus;

                                                    orderPackages.logisticDetail=ret.data.records[i].repertoryList[k].orderPackages[ii].logisticDetail;
                                                    orderPackages.internalLogisticNo=ret.data.records[i].repertoryList[k].orderPackages[ii].internalLogisticNo;
                                                    orderPackages.logisticState=ret.data.records[i].repertoryList[k].orderPackages[ii].logisticState;
                                                    orderPackages.latestLogisticFlowLine=ret.data.records[i].repertoryList[k].orderPackages[ii].latestLogisticFlowLine;

                                                    // 给单个包裹加上收货人信息
                                                    orderPackages.reciverInfo = ret.data.records[i].orderDetail;
                                                    
                                                    // 给单个包裹加上包裹里面所有商品的信息
                                                    orderPackages.orderInfoItems=[];
                                                    for(var iii=0;iii<ret.data.records[i].repertoryList[k].orderPackages[ii].orderInfoItems.length;iii++)
                                                    {
                                                      if(!ret.data.records[i].repertoryList[k].orderPackages[ii].orderInfoItems[iii].sku)
                                                      {
                                                        ret.data.records[i].repertoryList[k].orderPackages[ii].orderInfoItems[iii].sku={};
                                                        if(!ret.data.records[i].repertoryList[k].orderPackages[ii].orderInfoItems[iii].sku.imageAttachment)
                                                        {
                                                          ret.data.records[i].repertoryList[k].orderPackages[ii].orderInfoItems[iii].sku.imageAttachment={};
                                                          if(!ret.data.records[i].repertoryList[k].orderPackages[ii].orderInfoItems[iii].sku.imageAttachment.resourceFullAddress){
                                                            ret.data.records[i].repertoryList[k].orderPackages[ii].orderInfoItems[iii].sku.imageAttachment.resourceFullAddress="dasda";
                                                          }
                                                        }
                                                      }

                                                      // 重构单个商品信息
                                                      var orderInfoItems={};
                                                      orderInfoItems.id=ret.data.records[i].repertoryList[k].orderPackages[ii].orderInfoItems[iii].id;
                                                      orderInfoItems.num=ret.data.records[i].repertoryList[k].orderPackages[ii].orderInfoItems[iii].num;
                                                      orderInfoItems.buyUnitAmount=ret.data.records[i].repertoryList[k].orderPackages[ii].orderInfoItems[iii].buyUnitAmount;
                                                      orderInfoItems.img=ret.data.records[i].repertoryList[k].orderPackages[ii].orderInfoItems[iii].sku.imageAttachment.resourceFullAddress;
                                                      orderInfoItems.skuName=ret.data.records[i].repertoryList[k].orderPackages[ii].orderInfoItems[iii].skuName;
                                                      orderInfoItems.goodsAttributeOptions=ret.data.records[i].repertoryList[k].orderPackages[ii].orderInfoItems[iii].sku.goodsAttributeOptions;
                                                      orderPackages.orderInfoItems.push(orderInfoItems);
                                                    }

                                                    records.orderPackages.push(orderPackages);
                                                  }
                                                }

                                                // 加入重构的订单项
                                                dingdanData.push(records);

                                              }
                                              thiz.log("------------dingdanData-----------------",dingdanData);
                                              thiz.log("--------end_time2--------",(new Date()).getTime());
                                              thiz.setState({
                                                dingdanData:dingdanData,
                                                isRefreshing:false,
                                                visible:false,
                                                hasNext:ret.data.hasNext
                                              });
                                            }
                                          },
                                          error:function(error){
                                               thiz.setState({visible:false});     
                                          }
                                        });
                                      }
                                      else
                                      {

                                      // thiz.toast("已经到底了，没有更多数据了");
                                      }
                                  }
                                }}
                                refreshControl={
                                <RefreshControl
                                  refreshing={this.state.isRefreshing}
                                  onRefresh={()=>{this.onRefresh()}}
                                  colors={['#ff0000', '#00ff00','#0000ff','#3ad564']}
                                  progressBackgroundColor="#ffffff"
                                />
                              }>
                    {
                      this.state.dingdanData.map((item,index1)=>{
                        this.log("-------------------------item-------------------",item);
                        return (
                          <View style={{width:'100%',backgroundColor:'white'}} key={index1}>
                              <View style={{width:'100%',height:BaseComponent.W*10/375,backgroundColor:'#F0F0F0'}}></View>
                              <TouchableWithoutFeedback onPress={()=>{
                                thiz.navigate('DingdanXiangqingDaifahuo',{title:"订单详情",businessOrderNo:item.businessOrderNo});
                              }}>
                              <View style={{width:'100%',height:BaseComponent.W*53/375,flexDirection:'row',alignItems:'center'}}>
                                <Text style={{fontSize:BaseComponent.W*15/375,color:'#040000',marginLeft:BaseComponent.W*12/375}}>订单号:&nbsp;&nbsp;{item.businessOrderNo}</Text>
                                <TouchableWithoutFeedback onPress={()=>{
                                  //订单再次购买
                                      thiz.request("order/buyAgain",{
                                          method:"POST",
                                          data:{businessOrderNo:item.businessOrderNo},
                                          success:function(ret){
                                              thiz.emit("click_buyAgain");
                                              BaseComponent.CUR_TAB = "Cart";
                                              thiz.navigate("Cart");
                                          },
                                          error:function(error){

                                          }
                                      }); 
                                }}>
                                <View style={{width:BaseComponent.W*70/375,height:BaseComponent.W*30/375,borderRadius:BaseComponent.W*15/375,display:'none',
                                                      marginRight:BaseComponent.W*15/375,borderWidth:0.5,borderColor:'#606060',justifyContent:'center',alignItems:'center'}}>
                                      <Text style={{fontSize:BaseComponent.W*13/375,color:'#252525'}}>再次购买</Text>
                                </View>
                                </TouchableWithoutFeedback>  
                              </View>
                              </TouchableWithoutFeedback>
                              <View style={{width:BaseComponent.W*365/375,height:0.5,backgroundColor:'#DDDDDD',marginLeft:BaseComponent.W*10/375}}></View>
                              {
                                item.orderPackages.map((item1,index2)=>{
                                  return (
                                      <View key={index2}>
                                          <TouchableWithoutFeedback onPress={()=>{
                                            thiz.navigate('DingdanXiangqingDaifahuo',{title:"订单详情",businessOrderNo:item.businessOrderNo});
                                          }}>
                                          <View style={{width:'100%',height:BaseComponent.W*92/375,flexDirection:'row',alignItems:'center'}}>
                                              <View style={{flexDirection:'row',alignItems:'center',height:BaseComponent.W*92/375,display:item.orderPackages[index2].orderInfoItems.length>1?'none':'flex'}}> 
                                                <View style={{width:BaseComponent.W*60/375,height:BaseComponent.W*60/375,marginLeft:BaseComponent.W*10/375}}>
                                                  <Image style={{width:BaseComponent.W*60/375,height:BaseComponent.W*60/375}} source={{uri:item.orderPackages[index2].orderInfoItems[0].img?item.orderPackages[index2].orderInfoItems[0].img:""}}></Image>
                                                </View>
                                                <View style={{width:BaseComponent.W*200/375,height:BaseComponent.W*60/375,marginLeft:BaseComponent.W*10/375,justifyContent:'space-between'}}>
                                                  <Text style={{fontSize:BaseComponent.W*13/375,color:'#040000',lineHeight:BaseComponent.W*18/375}} numberOfLines={2}>{item.orderPackages[index2].orderInfoItems[0].skuName}</Text>
                                                  <Text style={{fontSize:BaseComponent.W*12/375,color:'#999899',marginTop:BaseComponent.W*5/375}}>{item.orderPackages[index2].orderInfoItems[0].goodsAttributeOptions[0].goodsAttributeOption.value}&nbsp;&nbsp;{item.orderPackages[index2].orderInfoItems[0].goodsAttributeOptions[1]?item.orderPackages[index2].orderInfoItems[0].goodsAttributeOptions[1].goodsAttributeOption.value:""}</Text>
                                                </View>
                                              </View>
                                        
                                              <View style={{width:BaseComponent.W*235/375,height:BaseComponent.W*92/375,flexDirection:'row',alignItems:'center',display:item.orderPackages[index2].orderInfoItems.length>1?'flex':'none'}}>
                                                  {
                                                    item.orderPackages[index2].orderInfoItems.map((itemImg,index3)=>{
                                                      if(index3<=2){
                                                      return (
                                                        <View style={{width:BaseComponent.W*60/375,height:BaseComponent.W*60/375,marginLeft:BaseComponent.W*10/375}} key={index3}>
                                                          <Image style={{width:BaseComponent.W*60/375,height:BaseComponent.W*60/375}} source={{uri:itemImg.img}}></Image>
                                                        </View>
                                                      )
                                                      }
                                                    })
                                                  }
                                              </View>

                                              <View style={{width:item.orderPackages[index2].orderInfoItems.length>1?BaseComponent.W*125/375:BaseComponent.W*80/375,height:BaseComponent.W*60/375,marginLeft:BaseComponent.W*0/375,alignItems:'flex-end',justifyContent:'space-between'}}>
                                                  <Text style={{fontSize:BaseComponent.W*13/375,color:'#393A3A'}}>{item.orderPackages[index2].name}</Text>
                                                  <Text style={{fontSize:BaseComponent.W*14/375,color:'#7D7D7D'}}>
                                                      {(()=>{
                                                        if(item.orderPackages[index2].receiverReceivingStatus=="CONFIRM_RECEIVED"&&item.orderPackages[index2].deliveryStatus=="DELIVERED"){return "已收货"};
                                                        if(item.orderPackages[index2].deliveryStatus=="DELIVERED"){return "待收货"};
                                                        if(item.orderPackages[index2].deliveryStatus=="NOT_DELIVER"){return "待发货"};
                                                      })()} 
                                                  </Text> 
                                              </View>
                                          </View>
                                          </TouchableWithoutFeedback>



                                          {/*包裹状态为待收货(或已发货)、交易完成、退换/售后，增加的内容*/}
                                          {
                                            (item.orderPackages[index2].deliveryStatus=="DELIVERED"&& item.orderPackages[index2].receiverReceivingStatus=="NOT_RECEIVED")||
                                            (item.orderPackages[index2].deliveryStatus=="DELIVERED"&& item.orderPackages[index2].receiverReceivingStatus=="CONFIRM_RECEIVED")||
                                            (item.orderStatus=="REFUND_SUCCESS"||item.orderStatus=="APPLY_REFUND"||item.orderStatus=="REFUND_PROCESSING")?
                                            (
                                              <View style={{paddingLeft:30,paddingRight:30,paddingTop:10,paddingBottom:10,}}>

                                                <View style={{flexDirection:"row"}}>
                                                  <Text style={{color:"#121212"}}>收货人：</Text>
                                                  <Text style={{color:"#121212"}}>{item.orderPackages[index2].reciverInfo?item.orderPackages[index2].reciverInfo.receiverName:"暂无"}</Text>
                                                  <Text style={{color:"#121212",marginLeft:10,}}>{item.orderPackages[index2].reciverInfo?item.orderPackages[index2].reciverInfo.receiverPhoneNo:""}</Text>
                                                </View>

                                                <View style={{flexDirection:"row",marginTop:10,position:"relative"}}>
                                                  <Text style={{color:"#121212"}}>{item.orderPackages[index2].internalLogisticNo?item.orderPackages[index2].internalLogisticNo:"暂无物流单号"}</Text>

                                                  <TouchableWithoutFeedback onPress={()=>{
                                                    if(item.orderPackages[index2].internalLogisticNo){
                                                      ti.copy(item.orderPackages[index2].internalLogisticNo);
                                                      thiz.toast("复制成功");
                                                    }
                                                  }}>
                                                  <View style={{width:BaseComponent.W*50/375,height:BaseComponent.W*22/375,borderRadius:BaseComponent.W*11/375,
                                                        marginRight:BaseComponent.W*0/375,borderWidth:0.5,borderColor:'#606060',justifyContent:'center',alignItems:'center',
                                                        position:item.orderPackages[index2].internalLogisticNo?"absolute":"relative",right:0,zIndex:100001,
                                                        display:item.orderPackages[index2].internalLogisticNo?"flex":"none"}}>
                                                    <Text style={{fontSize:BaseComponent.W*13/375,color:'#393A3A'}}>复制</Text>
                                                  </View>
                                                  </TouchableWithoutFeedback>

                                                </View>

                                                <View style={{marginTop:10,}}>
                                                  <Text style={{fontSize:BaseComponent.W*13/375,color:'#7C7C7C'}}>{item.orderPackages[index2].latestLogisticFlowLine&&item.orderPackages[index2].latestLogisticFlowLine.context?item.orderPackages[index2].latestLogisticFlowLine.context:"暂无物流信息"}</Text>
                                                </View>

                                              </View>
                                            ):null
                                          }



                                          <View style={{width:BaseComponent.W*365/375,height:0.5,backgroundColor:'#DDDDDD',marginLeft:BaseComponent.W*10/375}}></View>   
                                          
                                          <View style={{width:'100%',height:BaseComponent.W*50/375,flexDirection:'row',alignItems:'center',justifyContent:'flex-end'}}>
                                                <TouchableWithoutFeedback onPress={()=>{
                                                  this.navigate('LogisticsTracking',{title:"物流追踪",id:item1.id});                     
                                                }}>
                                                <View style={{width:BaseComponent.W*70/375,height:BaseComponent.W*30/375,borderRadius:BaseComponent.W*15/375,
                                                      marginRight:BaseComponent.W*15/375,borderWidth:0.5,borderColor:'#606060',justifyContent:'center',alignItems:'center'}}>
                                                  <Text style={{fontSize:BaseComponent.W*13/375,color:'#252525'}}>跟踪物流</Text>
                                                </View>
                                                </TouchableWithoutFeedback>

                                                <TouchableWithoutFeedback onPress={()=>{
                                                  //订单确认收货
                                                  thiz.setState({shouhuovisible:true,id:item1.id});
                                                  thiz.log("-----------------------------item1----------------------",item1);

                                                }}>
                                                <View style={{width:BaseComponent.W*70/375,height:BaseComponent.W*30/375,borderRadius:BaseComponent.W*15/375,display:item.orderPackages[index2].deliveryStatus=="DELIVERED"&&item.orderPackages[index2].receiverReceivingStatus=="NOT_RECEIVED"?'flex':'none',
                                                      marginRight:BaseComponent.W*15/375,borderWidth:0.5,borderColor:'#606060',justifyContent:'center',alignItems:'center'}}>
                                                  <Text style={{fontSize:BaseComponent.W*13/375,color:'#252525'}}>确认收货</Text>
                                                </View>
                                                </TouchableWithoutFeedback>
                                          </View>
                                          <View style={{display:index2<item.orderPackages.length-1?"flex":"none",width:BaseComponent.W*365/375,height:0.5,backgroundColor:'#DDDDDD',marginLeft:BaseComponent.W*10/375}}></View>

                                      </View>
                                  )
                                })
                              }
                          </View>
                        )
                      })
                    }  
                     {
                      thiz.state.hasNext?null:thiz.nomore({bgcolor:'#F0F0F0'})
                    }
                    </ScrollView>
                    </View>)
                    )
                }
        
      </View>  
    )
  }
}