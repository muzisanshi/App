/**
 * @name Mine.js
 * @auhor 程浩
 * @date 2018.8.20
 * @desc 查看全部订单
 */
import React,{Component} from 'react';
import {Text,View,StyleSheet,Image,TouchableWithoutFeedback,StatusBar,SectionList} from 'react-native';
import ScrollableTabView, {DefaultTabBar,ScrollableTabBar} from 'react-native-scrollable-tab-view';
import DingdanXiangqingDaizhifu from './OrderDetailWaitForPay';
// import DingdanXiangqingDaishouhuo from './DingdanXiangqingDaishouhuo';
import DingdanQuanbu from './AllOrders';
import BaseComponent from '../BaseComponent';

import Daifukuan from './WaitForPay';
import Daifahuo from './WaitForDelivery';

import Daishouhuo from './WaitForReceived';
import Yiwancheng from './Completed';
import Tuihuotuikuan from './Tuihuotuikuan';

// 导入工具函数库
import T from "../../lib/Tools";
// 导入公共样式
import St from "../../lib/Style";
// 导入图片资源库
import Imgs from "../../lib/Img";

// Tools实例
let ti = T.getInstance();

let style = StyleSheet.create({
  wrapper:{
    backgroundColor:"#F0F0F0",
    flex:1,
    flexDirection:"column",
    alignItems:"stretch",
    justifyContent:"flex-start",
    position:"relative",
  },
  statusBar:{
    backgroundColor:"rgb(255,255,255)",
    height:ti.select({ios:16,android:22}),
    position:"relative",
    zIndex:1,
    opacity:1,
  },
  navBar:{
    backgroundColor:"rgb(255,255,255)",
    height:"7%",
    position:"relative",
    zIndex:1,
    opacity:1,
    borderBottomWidth:1,
    borderBottomColor:"rgb(241,241,241)"
  },
});


export default class Wodedingdan extends BaseComponent{

  constructor(props){
    super(props);
    this.state={
      page:0,//默认初始化page是全部,也就是index为0
    }
  }
  componentWillMount(){
    var thiz=this;
    var page=thiz.params.page;
    console.log("------------------------------------First_page----------------------",page);
    thiz.setState({page:page});
     console.log("------------------------------------second_page----------------------",thiz.state.page);
  };
    render(){
        const {navigation}=this.props;
        var thiz=this;
        console.log("------------------------------------third_page----------------------",thiz.state.page);
        return(
            <View style={style.wrapper}>
                {/*状态栏占位*/}
                <View style={{
                  backgroundColor:"rgb(255,255,255)",
                  height:this.isIphoneX(30,BaseComponent.SH),
                  position:"relative",
                  zIndex:1,
                  opacity:1,
                }}></View>
                {/*顶部导航栏*/}
                <View style={style.navBar}>
                  <View style={{flex:1,flexDirection:"row",justifyContent:"flex-start"}}>

                    <TouchableWithoutFeedback onPress={()=>{
                      if(BaseComponent.CUR_TAB!="Mine"){
                        thiz.navigate(BaseComponent.CUR_TAB);
                      }else{
                        this.goBack()
                      }
                    }}>
                    <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>
                      <Image source={Imgs.back} style={{width:(BaseComponent.W*0.127*0.4),height:(BaseComponent.W*0.127*0.4),marginRight:BaseComponent.W*0.04}}></Image>
                    </View>
                    </TouchableWithoutFeedback>

                    <View style={{flexGrow:1,backgroundColor:"transparent",
                      marginTop:ti.select({ios:(BaseComponent.H*0.02)/2,android:(BaseComponent.H*0.016)/2}),borderRadius:ti.select({ios:(BaseComponent.H*0.05)/2,android:(BaseComponent.H*0.054)/2}),borderColor:"#0A0A0A",
                      borderWidth:0,}}>
                      <Text style={{color:"#0A0A0A",width:"100%",textAlign:"center",marginTop:ti.select({ios:"4.5%",android:"1.6%"}),fontSize:17}}>{this.params.title}</Text>
                    </View>

                    <TouchableWithoutFeedback onPress={()=>{
                      thiz.navigate("OrderSearch",{title:""});
                    }}>
                    <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>
                      <Image source={require('../../image/search.png')} style={{width:(BaseComponent.W*0.127*0.5),height:(BaseComponent.W*0.127*0.5),display:"flex"}}></Image>
                    </View>
                    </TouchableWithoutFeedback>

                  </View>
                </View>

                <ScrollableTabView tabBarPosition='top' tabBarTextStyle={{fontSize:BaseComponent.W*15/375,marginTop:BaseComponent.W*10/375}}
                 tabBarActiveTextColor='#393A3A' tabBarInactiveTextColor='#737373' locked={false} initialPage={thiz.state.page}
                 tabBarBackgroundColor='#fff' tabBarUnderlineStyle={{backgroundColor:'#FDD17A',width:BaseComponent.W*35/375,height:BaseComponent.W*3/375,borderRadius:BaseComponent.W*1.5/375,marginLeft:BaseComponent.W*19/375}}
                 >
                    {/*全部*/}
                    <View tabLabel='全部'>
                        <DingdanQuanbu navigation={this.props.navigation}/>
                    </View>
                    
                    <View tabLabel='待付款'>
                        <Daifukuan navigation={this.props.navigation}/>
                    </View>

                    <View tabLabel='待发货'>
                        <Daifahuo navigation={this.props.navigation}/>
                    </View>

                    <View tabLabel='待收货'>
                        <Daishouhuo navigation={this.props.navigation}/>
                    </View>

                    <View tabLabel='已完成'>
                        <Yiwancheng navigation={this.props.navigation}/>
                    </View>
                    
                </ScrollableTabView>

            </View>
        )
    }
}
