/**
 * @name shouhuoAddress.js
 * @auhor 程浩
 * @date 2018.8.21
 * @desc 地址管理
 */
import React,{Component} from 'react';
import {Text,View,StyleSheet,Image,TouchableWithoutFeedback,ScrollView,Modal,ActivityIndicator,RefreshControl} from 'react-native';
 // 导入工具函数库
import T from "../../lib/Tools";
// 转菊花
import Spinner from 'react-native-loading-spinner-overlay';
// 导入公共样式
import St from "../../lib/Style";
// 导入图片资源库
import Imgs from "../../lib/Img";
import BaseComponent from '../BaseComponent';
import EditAddress from './EditAddress';

// Tools实例
let ti = T.getInstance();

let style = StyleSheet.create({
  wrapper:{
    backgroundColor:"white",
    flex:1,
    flexDirection:"column",
    alignItems:"stretch",
    justifyContent:"flex-start",
    position:"relative",
  },
  statusBar:{
    backgroundColor:"rgb(255,255,255)",
    height:ti.select({ios:16,android:22}),
    position:"relative",
    zIndex:1,
    opacity:1,
  },
  navBar:{
    backgroundColor:"rgb(255,255,255)",
    height:"7%",
    position:"relative",
    zIndex:1,
    opacity:1,
    borderBottomWidth:1,
    borderBottomColor:"rgb(241,241,241)"
  },
});

export default class shouhuoAddress extends BaseComponent{
  constructor(props){
    super(props)
    this.state={
      receiverAddressData:[],//收货地址
      success:false,//设置默认地址,true表示成功
      isBack:"",//从不同界面进来收货地址界面是否能返回上一个界面，默认是true表示能返回
      visible:false,
      netError:false,//true表示网络错误
      isvisible:false,//提醒删除地址弹出框
      item:"",//点击地址的每一列地址
      from:"",//判断从哪一个界面跳转过来的
      hasNext:false,//是否有下一页
      pageNumber:1,
      pageSize:30,
      isRefreshing:false,
      id:"",
    }
  }
  //加载收货地址
  getData(){
      var thiz=this;
      var pageNumber=thiz.state.pageNumber;
      var pageSize=thiz.state.pageSize;
      thiz.setState({visible:true})
      thiz.request("receiverAddress/getPage",{
      method:'POST',
      data:{
          page:{
            pageNumber:pageNumber,
            pageSize:pageSize,
        }
      },
      success:function(ret){
        thiz.setState({visible:false});
        var receiverAddressData=thiz.state.receiverAddressData;
        if(ret.data.records){
        for(var i=0;i<ret.data.records.length;i++)
        {
          var AddressData={};
          if(!AddressData.name)
          {
            AddressData.name="";
          }
          if(!AddressData.address)
          {
            AddressData.address="";
          }
          if(!AddressData.tel)
          {
            AddressData.tel="";
          }
          if(!AddressData.default)
          {
            AddressData.defaultAddress="";
          }
          if(!AddressData.id)
          {
            AddressData.id="";
          }
          AddressData.name=ret.data.records[i].receiverName;
          AddressData.address=ret.data.records[i].fullAddress;
          AddressData.tel=ret.data.records[i].receiverPhoneNo;
          AddressData.defaultAddress=ret.data.records[i].defaultAddress;
          AddressData.id=ret.data.records[i].id;
          receiverAddressData.push(AddressData);
        } 
        thiz.setState({
          receiverAddressData:receiverAddressData,
          hasNext:ret.data.hasNext,
          isRefreshing:false,
        });
      }
      },
      error:function(err){
        thiz.setState({visible:false,isRefreshing:false});
        if(err.code&&err.code==200){
            thiz.setState({netError:true});
        }  
      }
    })
  };
  //下拉刷新
  onRefresh=()=>{
    var thiz=this;
    var state=thiz.state;
    state.receiverAddressData=[];
    state.pageNumber=1;
    state.isRefreshing=true;
    thiz.setState(state);
    thiz.getData();

  };
  componentWillMount=()=>{
      var thiz=this;
      if(thiz.params.from){
        var from=thiz.params.from;
        console.log("--------------------------from-------------------------",from);
        thiz.setState({from:from})
      }
      if(thiz.params.id){
        var id=thiz.params.id;
        thiz.setState({id:id})
      }   
  }
  componentDidMount(){
      var thiz=this;
      var isBack=thiz.params.isBack;
      thiz.setState({
        isBack:isBack,
      })
      thiz.getData();
      //监听保存收货地址
      thiz.listen("save_receiveraddr_success",function(){
        var state=thiz.state;
        state.receiverAddressData=[];
        state.pageNumber=1;
        thiz.setState(state);
        thiz.getData();
      });
      //监听删除收货地址：
      thiz.listen("delete_address_success",function(){
        var state=thiz.state;
        state.receiverAddressData=[];
        state.pageNumber=1;
        thiz.setState(state);
        thiz.getData();
      })
  }
  //设置默认地址
  setToDefault=(info)=>{
    var thiz=this;
    
    thiz.request("receiverAddress/setToDefault",{
      method:"POST",
      data:{id:info.id},
      success:function(ret){
        var state=thiz.state;
        state.receiverAddressData=[];
        state.pageNumber=1;
        thiz.setState(state);
        thiz.getData();
          thiz.log("---------------------默认地址--------------------",thiz.state.receiverAddressData);
          // thiz.emit("set_default_address",{data:info});
      },
      error:function(err){
        
      }
    })
  }
  //从确定订单点击过来选择地址
  selectedAddress=(info)=>{
      var thiz=this;
      thiz.log("-----------------info------------------",info);

      // 判断是否可以设置默认地址
      if(thiz.params.from=="Cart" || thiz.params.from=="Querendingdan"){
        thiz.emit("set_default_address",{data:info});
      }
      
      if(thiz.state.isBack){
        setTimeout(function(){
          thiz.goBack();
        },100);
      }
  }
  //编辑收货地址
  editAddress=(info)=>{
      var thiz=this;
      thiz.log("------------------------info-----------------------",info);
      thiz.navigate('EditAddress',{title:"编辑地址",editDataID:info.id,from:thiz.params.from});
  }
  //删除地址
  deleteAddress=(item)=>{
      var thiz=this;
      thiz.log("--------------------------item---------------------",item);  
      var DeleteId=item.id;//要删除的ID
      thiz.request("receiverAddress/delete",{
        method:"POST",
        data:{id:DeleteId},
        success:function(ret){

            // 发送删除地址成功消息，通知购物车等刷新
            thiz.emit("delete_address_success");

            thiz.setState({receiverAddressData:[],isvisible:false});
            // thiz.getData();
        },
        error:function(err){

        }
      });
  };

  //显示删除提醒框
  showDelete=(item)=>{
    var thiz=this;
    thiz.setState({isvisible:true,item:item});
  };
	render(){
    let thiz=this;
		return(
			 <View style={style.wrapper}>
                {/*状态栏占位*/}
                <View style={{backgroundColor:this.state.visible?"rgba(14,14,14,0.5)":"rgb(255,255,255)",
                              height:this.isIphoneX(30,BaseComponent.SH),
                              position:"relative",
                              zIndex:1,
                              opacity:1,}}></View>
                {/*顶部导航栏*/}
                <View style={style.navBar}>
                  <View style={{flex:1,flexDirection:"row",justifyContent:"flex-start"}}>

                    <TouchableWithoutFeedback onPress={()=>this.goBack()}>
                    <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>
                      <Image source={Imgs.back} style={{width:(BaseComponent.W*0.127*0.4),height:(BaseComponent.W*0.127*0.4),marginRight:BaseComponent.W*0.04}}></Image>
                    </View>
                    </TouchableWithoutFeedback>

                    <View style={{flexGrow:1,backgroundColor:"transparent",
                      marginTop:ti.select({ios:(BaseComponent.H*0.02)/2,android:(BaseComponent.H*0.016)/2}),borderRadius:ti.select({ios:(BaseComponent.H*0.05)/2,android:(BaseComponent.H*0.054)/2}),borderColor:"#0A0A0A",
                      borderWidth:0,}}>
                      <Text style={{color:"#0A0A0A",width:"80%",textAlign:"right",marginTop:ti.select({ios:"4.5%",android:"1.6%"}),fontSize:17}}>{this.params.title}</Text>
                    </View>

                    <TouchableWithoutFeedback onPress={()=>{
                        this.navigate("SearchAddress",{isBack:false,from:thiz.state.from})
                    }}>
                      <View style={{width:BaseComponent.W*40/375,height:'100%',marginRight:BaseComponent.W*10/375,justifyContent:'center',alignItems:'center'}}>
                          <Image style={{width:BaseComponent.W*0.127*0.5,height:BaseComponent.W*0.127*0.5}} source={require('../../image/home/searchaddress.png')}></Image>
                      </View>
                    </TouchableWithoutFeedback>

                    <TouchableWithoutFeedback onPress={()=>{
                        this.navigate('AddAddress',{title:'添加地址',from:thiz.params.from});
                    }}>
                      <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>
                        <Text style={{color:'#0A0A0A',marginTop:5}}>添加</Text>
                      </View>
                    </TouchableWithoutFeedback>

                  </View>
                </View>
                 {/*转菊花*/}
                <Modal
                  visible={this.state.visible}
                  onRequestClose={()=>{this.setState({visible:false})}}
                  transparent={true}
                  animationType={"fade"}>
                  <View style={{backgroundColor:"rgba(14,14,14,0.5)",width:"100%",width:"100%",flex:1,flexDirection:"column",alignItems:"center",justifyContent:"center",position:"relative"}}>
                    <View>
                          <ActivityIndicator size="large" color='white'/>
                    </View>
                  </View>
                </Modal>
                {/*提醒删除收货地址*/}
                 <Modal visible={this.state.isvisible}
                       onRequestClose={()=>{this.setState({isvisible:false})}}
                       transparent={true}>
                       <View style={{flex:1,backgroundColor:'rgba(14,14,14,0.5)',justifyContent:'center',alignItems:'center'}}>
                          <View style={{width:BaseComponent.W*275/375,height:BaseComponent.W*105/375,backgroundColor:'rgba(255, 255, 255,1)',borderRadius:BaseComponent.W*20/375}}>
                              <View style={{width:'100%',height:BaseComponent.W*56/375,justifyContent:'center',alignItems:'center'}}>
                                <Text style={{fontSize:BaseComponent.W*15/375,color:'rgba(94, 94, 94, 1)'}}>确定要删除该地址吗?</Text>
                              </View>
                              <View style={{width:'100%',height:0.5,backgroundColor:'rgba(189, 189, 189, 1)'}}></View>
                              <View style={{width:'100%',height:BaseComponent.W*49/375,flexDirection:'row'}}>
                              <TouchableWithoutFeedback onPress={()=>this.setState({isvisible:false})}> 
                                <View style={{width:'50%',height:BaseComponent.W*49/375,justifyContent:'center',alignItems:'center'}}>
                                  <Text style={{fontSize:BaseComponent.W*15/375,color:'rgba(94, 94, 94, 1)'}}>取消删除</Text>
                                </View>
                              </TouchableWithoutFeedback> 
                                <View style={{width:0.5,height:'100%',backgroundColor:'rgba(189, 189, 189, 1)'}}></View>
                                <TouchableWithoutFeedback onPress={()=>{
                                     thiz.deleteAddress(thiz.state.item);
                                }}> 
                                <View style={{width:'50%',height:BaseComponent.W*49/375,justifyContent:'center',alignItems:'center'}}>
                                  <Text style={{fontSize:BaseComponent.W*15/375,color:'#FED584'}}>确认删除</Text>
                                </View>
                                </TouchableWithoutFeedback>
                              </View>
                          </View>
                       </View>
                
                </Modal>
                
            {
              thiz.state.netError?(
                <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                  <Image style={{width:BaseComponent.W*180/375,height:BaseComponent.W*180/375,borderRadius:BaseComponent.W*90/375}} source={require('../../image/mine/neterror.png')}></Image>
                  <TouchableWithoutFeedback onPress={()=>{
                    thiz.setState({netError:false});
                    thiz.getData();
                  }}>
                  <View style={{width:BaseComponent.W*70/375,height:BaseComponent.W*30/375,borderRadius:BaseComponent.W*15/375,borderWidth:0.5,borderColor:'#080808',
                            justifyContent:'center',alignItems:'center',marginTop:BaseComponent.W*15/375}}>
                        <Text style={{fontSize:BaseComponent.W*15/375,color:'#252525'}}>刷新</Text>          
                    </View>
                  </TouchableWithoutFeedback>
                </View>
              ):(thiz.state.receiverAddressData.length==0?(
                  <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                    <Image style={{width:BaseComponent.W*180/375,height:BaseComponent.W*180/375,borderRadius:BaseComponent.W*90/375}} source={require('../../image/mine/noaddress.png')}></Image>
                  </View>
                  ):(
                  <View style={{flex:1}}>
                      <ScrollView style={{backgroundColor:"#f0f0f0",flex:1}} showsVerticalScrollIndicator = {false}
                        onMomentumScrollEnd={(e)=>
                        {
                            var offsetY = e.nativeEvent.contentOffset.y; //滑动距离
                            var contentSizeHeight = e.nativeEvent.contentSize.height; //scrollView contentSize高度
                            var oriageScrollHeight = e.nativeEvent.layoutMeasurement.height; //scrollView高度
                            console.log("---------------------滑动距离-------------------",offsetY);
                            console.log("---------------------scrollView contentSize高度-------------------",contentSizeHeight);
                            console.log("---------------------scrollView高度-------------------",oriageScrollHeight);
                            if (Math.abs((offsetY + oriageScrollHeight) - contentSizeHeight)<5){
                                  var hasNext=thiz.state.hasNext;
                                  console.log("---------------------------------------hasNext-----------------------------",hasNext);
                                  if(hasNext){
                                    var pageNumber=thiz.state.pageNumber;
                                    pageNumber++;
                                    var state=thiz.state;
                                    state.pageNumber=pageNumber;
                                    thiz.setState(state);
                                    thiz.getData();
                                  }  
                            }
                         }}
                         refreshControl={
                         <RefreshControl
                            refreshing={thiz.state.isRefreshing}
                            onRefresh={()=>{thiz.onRefresh()}}
                            colors={['#ff0000', '#00ff00','#0000ff','#3ad564']}
                            progressBackgroundColor="#ffffff"
                          />
                        }>          
                      {/*循环列表*/}
                      {
                        this.state.receiverAddressData.map((item,index)=>{
                          
                          return (
                            <View style={{width:'100%',backgroundColor:"white"}} key={index}>
                              
                              <View style={{width:'100%',height:BaseComponent.W*0.027,backgroundColor:'#F0F0F0'}}></View>
                              
                              <View style={{flex:1,alignItems:'flex-end'}}>
                                <View style={{width:'100%',height:1,backgroundColor:'#FDD17A',display:thiz.state.id==item.id?'flex':'none'}}></View>
                               <TouchableWithoutFeedback onPress={()=>this.selectedAddress(item)}> 
                               <View style={{width:BaseComponent.W*0.97,height:BaseComponent.W*0.19,flexDirection:'column'}}>
                                  <View style={{flexDirection:'row',marginTop:BaseComponent.W*0.03}}>
                                      <Text style={{fontSize:BaseComponent.W*0.037,color:'#121212',}}>收货人:  {item.name}</Text>
                                      <View style={{display:thiz.state.id==item.id?'flex':'none',marginLeft:BaseComponent.W*10/375,marginTop:3,width:BaseComponent.W*40/375,height:BaseComponent.W*13/375,borderRadius:BaseComponent.W*6.5,backgroundColor:'#414141',justifyContent:'center',alignItems:'center'}}>
                                            <Text style={{fontSize:BaseComponent.W*9/375,color:'#ffffff'}}>当前地址</Text>
                                      </View>
                                  </View>  
                                  <Text style={{fontSize:BaseComponent.W*0.037,color:'#6B6B6B',marginTop:BaseComponent.W*0.032}} numberOfLines={1}>收货地址:  {item.address}</Text>
                                  <Text style={{position:'absolute',fontSize:BaseComponent.W*0.037,color:'#121212',
                                  right:0,marginTop:BaseComponent.W*0.03,marginRight:BaseComponent.W*0.024}}>{item.tel}</Text>
                               </View>
                               </TouchableWithoutFeedback>
                               <View style={{width:BaseComponent.W*365/375,height:0.5,backgroundColor:'#E4E4E4',marginLeft:BaseComponent.W*10/375}}></View>

                               <View style={{width:'100%',height:BaseComponent.W*0.144,flexDirection:'row',
                                    justifyContent:'space-between',backgroundColor:'white'}}>
                                <TouchableWithoutFeedback onPress={()=>this.setToDefault(item)}>    
                                    <View style={{width:BaseComponent.W*0.23,height:BaseComponent.W*0.144,
                                      flexDirection:'row',justifyContent:'space-between',marginTop:BaseComponent.W*11.5/375,marginLeft:BaseComponent.W*0.035}}>
                                      <Image style={{width:BaseComponent.W*0.053,height:BaseComponent.W*0.053}} source={item.defaultAddress?require('../../image/home/xuanzhong.png'):require('../../image/home/weixuanzhong.png')}/>
                                      <Text style={{fontSize:BaseComponent.W*0.037,color:'#121212'}}>默认地址</Text>
                                    </View>
                                </TouchableWithoutFeedback>

                                <View style={{width:BaseComponent.W*0.39,height:BaseComponent.W*0.144,flexDirection:'row',
                                      marginTop:BaseComponent.W*11.5/375,justifyContent:'space-between',marginRight:BaseComponent.W*0.024}}>
                                  <TouchableWithoutFeedback onPress={()=>this.editAddress(item)}>   
                                      <View style={{width:BaseComponent.W*0.17,height:BaseComponent.W*0.08,borderRadius:2,
                                            borderWidth:0.5,borderColor:'#7F7F7F',justifyContent:'center',alignItems:'center'}}>
                                        <Text style={{fontSize:BaseComponent.W*0.037,color:'#121212'}}>编辑</Text>
                                      </View>
                                  </TouchableWithoutFeedback>

                                  <TouchableWithoutFeedback onPress={()=>{this.showDelete(item)}}>
                                      <View style={{width:BaseComponent.W*0.17,height:BaseComponent.W*0.08,borderRadius:2,
                                            borderWidth:0.5,borderColor:'#7F7F7F',justifyContent:'center',alignItems:'center'}}>
                                        <Text style={{fontSize:BaseComponent.W*0.037,color:'#121212'}}>删除</Text>
                                      </View>
                                  </TouchableWithoutFeedback>

                                </View>
    
                               </View>
                               <View style={{width:'100%',height:1,backgroundColor:'#FDD17A',display:thiz.state.id==item.id?'flex':'none'}}></View>
                              </View>
                              
                            </View>
                          );

                        })
                      }
                      
                      <View style={{flex:1,paddingTop:BaseComponent.W*27/375,paddingBottom:BaseComponent.W*27/375,display:thiz.state.hasNext?'none':'flex'}}>
                          <TouchableWithoutFeedback onPress={()=>this.navigate('AddAddress',{title:'添加地址',from:thiz.params.from})}>
                          <View style={{width:BaseComponent.W*355/375,height:BaseComponent.W*50/375,marginLeft:BaseComponent.W*10/375,justifyContent:'center',
                                      borderRadius:BaseComponent.W*25/375,backgroundColor:'#FDD17A',alignItems:'center'}}>
                                <Text style={{fontSize:BaseComponent.W*16/375,color:'#080808'}}>添加收货地址</Text>        
                          </View>
                          </TouchableWithoutFeedback>
                      </View>

                      </ScrollView>
                  </View>))
            }
            
            </View>
		)
	}
}
