/**
 * @name AfterSalesDetails.js
 * @auhor 程浩
 * @date 2018.8.21
 * @desc 售后详情
 */
import React,{Component} from 'react';
import {Text,View,StyleSheet,Image,TouchableWithoutFeedback,ScrollView,Modal,ActivityIndicator,StatusBar,Clipboard} from 'react-native';
// 导入工具函数库
import T from "../../lib/Tools";
// 导入公共样式
import St from "../../lib/Style";
// 导入图片资源库
import Imgs from "../../lib/Img";
import BaseComponent from '../BaseComponent';
// 转菊花
import Spinner from 'react-native-loading-spinner-overlay';

// Tools实例
let ti = T.getInstance();

let style = StyleSheet.create({
  wrapper:{
    backgroundColor:"#F0F0F0",
    flex:1,
    flexDirection:"column",
    alignItems:"stretch",
    justifyContent:"flex-start",
    position:"relative",
  },
  statusBar:{
    backgroundColor:"rgb(255,255,255)",
    height:ti.select({ios:16,android:22}),
    position:"relative",
    zIndex:1,
    opacity:1,
  },
  navBar:{
    backgroundColor:"rgb(255,255,255)",
    height:"7%",
    position:"relative",
    zIndex:1,
    opacity:1,
    borderBottomWidth:1,
    borderBottomColor:"rgb(241,241,241)"
  },
});


export default class AfterSalesDetails extends BaseComponent{
  constructor(props){
    super(props);
    this.state={
      data:{
        refundReason:{},
        orderInfo:{
          payChannelOrder:{
            payMode:{
              payChannel:{}
            }
          },
          orderDetail:{}
        },
        applyRefundRefOrderInfoItems:[
            {
              orderInfoItem:{
                sku:{
                  imageAttachment:{

                  },
                  goodsAttributeOptions:[
                    {
                      goodsAttributeOption:{

                      }
                    },
                    {
                      goodsAttributeOption:{

                      }
                    }
                  ]
                }
            }
          }
        ],
        refundOrderInfo:{}
      },//数据源
      visible:false,//转菊花
      isvisible:false,
      id:""
    }
  }

  componentDidMount(){
    var thiz=this;
    
    thiz.getData();
    //监听取消售
    thiz.listen("cancel_afterSale_success",function(){
      setTimeout(function(){
          thiz.getData();
      },500);
    })
  }
  //获取订单数据
  getData=()=>{
    var thiz=this;
    var serialNo=thiz.params.serialNo;
    console.log("------------------------------serialNo-------------------------------",serialNo);
    thiz.setState({visible:true});
    thiz.request('afterSale/getApplyRefundDetail',{
      method:"POST",
      data:{serialNo:serialNo},
      success:function(ret){
        if(ret&&ret.respCode=='00'){
          thiz.setState({
            data:ret.data,
            visible:false
          })
        }
      },
      error:function(){
        thiz.setState({visible:false})
      }
    })
  };
  //隐藏部分电话号码
  PlacePhone=(phone)=>{
    var num;
    if(phone.length=11)
    {
      num=phone.toString().replace(/^(\d{3})\d{4}(\d{4})$/, '$1****$2');
    }else
    {
       num=phone; 
    }
    return num;
}




  //复制到剪贴板
  copyClipboard=()=>{   
    Clipboard.setString(this.state.data.orderInfo.businessOrderNo);
    this.toast("复制成功");  
  };
   //取消售后
  cancel=(id)=>{
    var thiz=this;
    

    thiz.setState({isvisible:true,id:id});
    
  };
  

    render() {
      let thiz=this;
      
        return ( 
            <View style={style.wrapper}>
                {/*状态栏占位*/}
                <StatusBar translucent={true} backgroundColor={this.state.isvisible||this.state.visible||this.state.shouhuovisible?'rgba(14,14,14,0.5)':"rgb(255,255,255)"} barStyle={'dark-content'}/>
                <View style={{backgroundColor:this.state.isvisible||this.state.visible||this.state.shouhuovisible?'rgba(14,14,14,0.5)':"rgb(255,255,255)",
                          height:thiz.isIphoneX(30,BaseComponent.SH),
                          position:"relative",
                          zIndex:1,
                          opacity:1,}}></View>
                {/*顶部导航栏*/}
                <View style={style.navBar}>
                  <View style={{flex:1,flexDirection:"row",justifyContent:"flex-start"}}>

                    <TouchableWithoutFeedback onPress={()=>{
                      thiz.goBack();
                    }}>
                        <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>
                          <Image source={Imgs.back} style={{width:(BaseComponent.W*0.127*0.4),height:(BaseComponent.W*0.127*0.4),marginRight:BaseComponent.W*0.04}}></Image>
                        </View>
                    </TouchableWithoutFeedback>    

                    <View style={{flexGrow:1,backgroundColor:"transparent",
                      marginTop:ti.select({ios:(BaseComponent.H*0.02)/2,android:(BaseComponent.H*0.016)/2}),borderRadius:ti.select({ios:(BaseComponent.H*0.05)/2,android:(BaseComponent.H*0.054)/2}),borderColor:"#0A0A0A",
                      borderWidth:0,}}>
                      <Text style={{color:"#0A0A0A",width:"100%",textAlign:"center",marginTop:ti.select({ios:"4.5%",android:"1.6%"}),fontSize:17}}>{this.params.title}</Text>
                    </View>
                    <TouchableWithoutFeedback onPress={()=>{
                      // 跳转客服
                      thiz.log("--------toService--------","toService");
                      thiz.goPage("Service",{title:"客服"});
                    }}>
                    <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>
                      <Image source={require('../../image/home/kefu.png')} style={{width:(BaseComponent.W*0.127*0.5),height:(BaseComponent.W*0.127*0.5),display:"flex"}}></Image>
                    </View>
                    </TouchableWithoutFeedback>

                  </View>
                </View>

                {/*转菊花*/}
                <Modal
                  visible={this.state.visible}
                  onRequestClose={()=>{this.setState({visible:false})}}
                  transparent={true}
                  animationType={"fade"}>
                  <View style={{backgroundColor:"rgba(14,14,14,0.5)",width:"100%",width:"100%",flex:1,flexDirection:"column",alignItems:"center",justifyContent:"center",position:"relative"}}>
                    <View>
                          <ActivityIndicator size="large" color='white'/>
                    </View>
                  </View>
                </Modal>

                {/*提醒取消售后*/}
                <Modal visible={this.state.isvisible}
                       onRequestClose={()=>{this.setState({isvisible:false})}}
                       transparent={true}>
                       <View style={{flex:1,backgroundColor:'rgba(14,14,14,0.5)',justifyContent:'center',alignItems:'center'}}>
                          <View style={{width:BaseComponent.W*275/375,height:BaseComponent.W*105/375,backgroundColor:'rgba(255, 255, 255,1)',borderRadius:BaseComponent.W*20/375}}>
                              <View style={{width:'100%',height:BaseComponent.W*56/375,justifyContent:'center',alignItems:'center'}}>
                                <Text style={{fontSize:BaseComponent.W*15/375,color:'rgba(94, 94, 94, 1)'}}>确定要取消售后吗?</Text>
                              </View>
                              <View style={{width:'100%',height:0.5,backgroundColor:'rgba(189, 189, 189, 1)'}}></View>
                              <View style={{width:'100%',height:BaseComponent.W*49/375,flexDirection:'row'}}>
                              <TouchableWithoutFeedback onPress={()=>this.setState({isvisible:false})}> 
                                <View style={{width:'50%',height:BaseComponent.W*49/375,justifyContent:'center',alignItems:'center'}}>
                                  <Text style={{fontSize:BaseComponent.W*15/375,color:'rgba(94, 94, 94, 1)'}}>取消</Text>
                                </View>
                              </TouchableWithoutFeedback> 
                                <View style={{width:0.5,height:'100%',backgroundColor:'rgba(189, 189, 189, 1)'}}></View>
                                <TouchableWithoutFeedback onPress={()=>{

                                     thiz.setState({isvisible:false});
                                      thiz.request("afterSale/cancel",{
                                        method:"POST",
                                        data:{id:thiz.state.id},
                                        success:function(ret){
                                          thiz.emit("cancel_afterSale_success");
                                          // thiz.setState({visible:false});
                                        },
                                        error:function(err){
                                            thiz.toast(err.msg);
                                        }
                                      });
                                }}> 
                                <View style={{width:'50%',height:BaseComponent.W*49/375,justifyContent:'center',alignItems:'center'}}>
                                  <Text style={{fontSize:BaseComponent.W*15/375,color:'#FED584'}}>确认</Text>
                                </View>
                                </TouchableWithoutFeedback>
                              </View>
                          </View>
                       </View>
                </Modal>
              
              <ScrollView>
              {/*售后状态*/}
              <View style={{width:BaseComponent.W,backgroundColor:'#FFD685',marginTop:BaseComponent.W*10/375}}>
                  <Text style={{fontSize:BaseComponent.W*15/375,color:'#181818',fontWeight:'bold',paddingTop:BaseComponent.W*13/375,marginLeft:BaseComponent.W*10/375}}>{(()=>{
                    if(thiz.state.data.applyRefundProgress=='NOT_AUDIT'){return '售后申请正在处理'};
                    if(thiz.state.data.applyRefundProgress=="AUDIT_NOT_PASS"){return '售后申请未通过'};
                    if(thiz.state.data.applyRefundProgress=="AUDIT_PASS"){return '已通过,等待退款中'};
                    if(thiz.state.data.applyRefundProgress=="REFUND_PROCESSING"){return "退款中"};
                    if(thiz.state.data.applyRefundProgress=="REFUND_SUCCESS"){return "售后已完成"};
                    if(thiz.state.data.applyRefundProgress=="CANCELED"){return '因您取消申请,售后已关闭'};
                  })()}</Text>

                  <Text style={{fontSize:BaseComponent.W*15/375,color:'#181818',marginLeft:BaseComponent.W*10/375,marginTop:BaseComponent.W*13/375,paddingBottom:BaseComponent.W*12/375,display:thiz.state.data.applyRefundProgress=='NOT_AUDIT'?'flex':'none'}}>{(()=>{
                    if(thiz.state.data.applyRefundProgress=='NOT_AUDIT'){return '正在处理中,请耐心等候...'}
                  })()}</Text>

                
                  <Text style={{display:thiz.state.data.applyRefundProgress=="AUDIT_NOT_PASS"||thiz.state.data.applyRefundProgress=="AUDIT_PASS"?'flex':'none',fontSize:BaseComponent.W*14/375,color:'#181818',marginLeft:BaseComponent.W*10/375,marginTop:BaseComponent.W*12/375}}>处理时间:&nbsp;&nbsp;{thiz.state.data.auditDatetime}</Text>
                  <Text style={{display:thiz.state.data.applyRefundProgress=="REFUND_PROCESSING"||thiz.state.data.applyRefundProgress=="REFUND_SUCCESS"?'flex':'none',fontSize:BaseComponent.W*14/375,color:'#181818',marginLeft:BaseComponent.W*10/375,marginTop:BaseComponent.W*12/375}}>处理时间:&nbsp;&nbsp;{thiz.state.data.executeRefundDatetime}</Text>
                  <Text style={{display:thiz.state.data.applyRefundProgress=="CANCELED"?'flex':'none',fontSize:BaseComponent.W*14/375,color:'#181818',marginTop:BaseComponent.W*12/375,paddingBottom:BaseComponent.W*12/375,marginLeft:BaseComponent.W*10/375}}>关闭时间:&nbsp;&nbsp;{thiz.state.data.cancelDatetime}</Text>

                  <Text style={{display:thiz.state.data.applyRefundProgress=="AUDIT_NOT_PASS"?'flex':'none',fontSize:BaseComponent.W*14/375,color:'#181818',marginLeft:BaseComponent.W*10/375,marginTop:BaseComponent.W*12/375,paddingBottom:BaseComponent.W*12/375}}>原因:&nbsp;&nbsp;{thiz.state.data.rejectReason}</Text>
              
                  <Text style={{display:thiz.state.data.applyRefundProgress=="AUDIT_PASS"||thiz.state.data.applyRefundProgress=="REFUND_PROCESSING"||thiz.state.data.applyRefundProgress=="REFUND_SUCCESS"?'flex':'none',fontSize:BaseComponent.W*14/375,color:'#181818',marginTop:BaseComponent.W*12/375,marginLeft:BaseComponent.W*10/375,paddingBottom:thiz.state.data.applyRefundProgress=="REFUND_PROCESSING"?0:BaseComponent.W*12/375}}>实际退款金额:&nbsp;&nbsp;<Text style={{fontSize:BaseComponent.W*16/375,color:'#FF0000'}}>￥{thiz.state.data.agreeRefundAmount}</Text></Text>
                  <Text style={{fontSize:BaseComponent.W*10/375,color:'#181818',display:thiz.state.data.applyRefundProgress=="REFUND_PROCESSING"?'flex':'none',marginLeft:BaseComponent.W*10/375,marginTop:2}}>(款项已原路返回,预计1-3个工作日)</Text>
                  <View style={{display:thiz.state.data.applyRefundProgress=="REFUND_PROCESSING"||thiz.state.data.applyRefundProgress=="REFUND_SUCCESS"?'flex':'none',flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
                    <Text style={{fontSize:BaseComponent.W*14/375,color:'#181818',marginTop:thiz.state.data.applyRefundProgress=="REFUND_PROCESSING"?BaseComponent.W*12/375:0,paddingBottom:BaseComponent.W*12/375,marginLeft:BaseComponent.W*10/375,display:thiz.state.data.applyRefundProgress=="REFUND_PROCESSING"||thiz.state.data.applyRefundProgress=="REFUND_SUCCESS"?'flex':'none'}}>退款编号:&nbsp;&nbsp;{thiz.state.data.refundOrderInfo?thiz.state.data.refundOrderInfo.businessOrderNo:''}</Text>
                    <TouchableWithoutFeedback onPress={()=>{
                          Clipboard.setString(this.state.data.refundOrderInfo?this.state.data.refundOrderInfo.businessOrderNo:'');
                          this.toast("复制成功");
                    }}>
                        <View style={{marginBottom:5,width:BaseComponent.W*50/375,height:BaseComponent.W*20/375,borderWidth:1,borderRadius:BaseComponent.W*10/375,borderColor:'#2D2D2D',marginRight:BaseComponent.W*10/375,
                                justifyContent:'center',alignItems:'center'}}>
                            <Text style={{fontSize:BaseComponent.W*14/375,color:'#080808'}}>复制</Text>      
                        </View>
                    </TouchableWithoutFeedback>    
                  </View> 
              </View>  
              
              {/*售后信息*/}
              <View style={{width:'100%',backgroundColor:'white',paddingBottom:10,marginTop:BaseComponent.W*10/375}}>
                  <Text style={{fontSize:BaseComponent.W*12/375,color:'#181818',marginLeft:BaseComponent.W*10/375,marginTop:BaseComponent.W*12/375}}>售后信息</Text>
                  <View style={{width:'100%',flexDirection:'row',marginTop:BaseComponent.W*10/375}}>
                      <Image style={{width:BaseComponent.W*60/375,height:BaseComponent.W*60/375,marginLeft:BaseComponent.W*10/375}} source={{uri:thiz.state.data.applyRefundRefOrderInfoItems[0].orderInfoItem.sku.imageAttachment.resourceFullAddress}}></Image>
                      <View style={{width:BaseComponent.W*200/375,height:BaseComponent.W*60/375,marginLeft:BaseComponent.W*10/375,flexDirection:'column',justifyContent:'space-between'}}>
                          <View style={{width:'100%'}}>
                              <Text style={{fontSize:BaseComponent.W*13/375,lineHeight:BaseComponent.W*20/375}} numberOfLines={2}>{thiz.state.data.applyRefundRefOrderInfoItems[0].orderInfoItem.skuName}</Text>
                          </View>
                          <Text style={{fontSize:BaseComponent.W*12/375,color:'#999899'}} numberOfLines={1}>{thiz.state.data.applyRefundRefOrderInfoItems[0].orderInfoItem.sku.goodsAttributeOptions[0].goodsAttributeOption.value}&nbsp;&nbsp;{thiz.state.data.applyRefundRefOrderInfoItems[0].orderInfoItem.sku.goodsAttributeOptions[1]?thiz.state.data.applyRefundRefOrderInfoItems[0].orderInfoItem.sku.goodsAttributeOptions[1].goodsAttributeOption.value:""}</Text>
                      </View>

                      <View style={{width:BaseComponent.W*80/375,flexDirection:'column',alignItems:'flex-end',marginLeft:BaseComponent.W*5/375}}>
                          <Text style={{fontSize:BaseComponent.W*13/375,color:'#393A3A'}} numberOfLines={1}>¥{thiz.state.data.applyRefundRefOrderInfoItems[0].orderInfoItem.buyUnitAmount}</Text>
                          <Text style={{fontSize:BaseComponent.W*12/375,color:'#999899',marginTop:2}} numberOfLines={1}>×{thiz.state.data.applyRefundRefOrderInfoItems[0].orderInfoItem.num}</Text>
                      </View>
                  </View>
                  <Text style={{fontSize:BaseComponent.W*12/375,color:'#181818',marginLeft:BaseComponent.W*10/375,marginTop:BaseComponent.W*20/375}}>售后类型:&nbsp;&nbsp;{thiz.state.data.afterSaleRefundType=="REFUND_AMOUNT"?'仅退款':'退货退款'}</Text>
                  <Text style={{fontSize:BaseComponent.W*12/375,color:'#181818',marginLeft:BaseComponent.W*10/375,marginTop:BaseComponent.W*5/375}}>退款原因:&nbsp;&nbsp;{thiz.state.data.refundReason.value}</Text>
                  <Text style={{fontSize:BaseComponent.W*12/375,color:'#181818',marginLeft:BaseComponent.W*10/375,marginTop:BaseComponent.W*5/375}}>申请退款金额:&nbsp;&nbsp;￥{thiz.state.data.applyRefundAmount}</Text>
                  <Text style={{display:thiz.state.data.afterSaleRefundType=="REFUND_AMOUNT"?'none':'flex',fontSize:BaseComponent.W*12/375,color:'#181818',marginLeft:BaseComponent.W*10/375,marginTop:BaseComponent.W*5/375}}>申请件数:&nbsp;&nbsp;{thiz.state.data.applyRefundRefOrderInfoItems[0].num}</Text>
                  <Text style={{fontSize:BaseComponent.W*12/375,color:'#181818',marginLeft:BaseComponent.W*10/375,marginTop:BaseComponent.W*5/375}}>申请时间:&nbsp;&nbsp;{thiz.state.data.applyRefundDatetime}</Text>
                  
              </View>
              
              {/*收货人地址*/}
              <View style={{width:'100%',height:BaseComponent.W*0.2,flexDirection:'column',backgroundColor:'white',marginTop:BaseComponent.W*10/375}}>
                  <Image style={{position:'absolute',width:BaseComponent.W*0.045,height:BaseComponent.W*0.045,left:0,marginTop:BaseComponent.W*0.047,marginLeft:BaseComponent.W*0.027}} source={require('../../image/home/mine_dizhiguanli.png')}></Image>
                  <Text style={{fontSize:BaseComponent.W*0.037,marginLeft:BaseComponent.W*0.093,color:'#121212',marginTop:BaseComponent.W*0.056}}>{thiz.state.data.orderInfo.orderDetail.receiverName?thiz.state.data.orderInfo.orderDetail.receiverName:''}&nbsp;&nbsp;<Text>{this.PlacePhone(thiz.state.data.orderInfo.orderDetail.receiverPhoneNo?thiz.state.data.orderInfo.orderDetail.receiverPhoneNo:'')}</Text></Text>
                  <Text style={{fontSize:BaseComponent.W*0.037,marginLeft:BaseComponent.W*0.093,marginTop:BaseComponent.W*0.015,color:'#6B6B6B'}} numberOfLines={1}>{thiz.state.data.orderInfo.orderDetail.fullAddress?thiz.state.data.orderInfo.orderDetail.fullAddress:''}</Text>
              </View>

              {/*费用明细*/}
              <View style={{width:BaseComponent.W,flexDirection:'column',backgroundColor:'white',marginTop:BaseComponent.W*10/375}}>
                  <View style={{width:'100%',height:BaseComponent.W*0.112,flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
                    <Text style={{fontSize:BaseComponent.W*0.037,color:"#121212",marginLeft:BaseComponent.W*0.027}}>实付总额</Text>
                    <Text style={{fontSize:BaseComponent.W*0.037,color:"#FF407E",marginRight:BaseComponent.W*0.029}}>¥{thiz.state.data.orderInfo.payAmount}</Text>
                  </View>

                  <View style={{width:'100%',height:0.5,backgroundColor:'#DDDDDD',marginLeft:BaseComponent.W*0.027}}></View>
                  <View style={styles.CostBreakdownTongyong}>
                      <Text style={{fontSize:BaseComponent.W*0.032,color:'#6B6B6B',marginLeft:BaseComponent.W*0.027}}>商品总额</Text>
                      <Text style={{fontSize:BaseComponent.W*0.037,color:"#121212",marginRight:BaseComponent.W*0.029}}>¥{thiz.state.data.orderInfo.goodsAmount}</Text>
                  </View>

                  <View style={{width:'100%',height:0.5,backgroundColor:'#DDDDDD',marginLeft:BaseComponent.W*0.027}}></View>
                  <View style={styles.CostBreakdownTongyong}>
                      <Text style={{fontSize:BaseComponent.W*0.032,color:'#6B6B6B',marginLeft:BaseComponent.W*0.027}}>税费</Text>
                      <Text style={{fontSize:BaseComponent.W*0.037,color:"#121212",marginRight:BaseComponent.W*0.029}}>¥{thiz.state.data.orderInfo.taxAmount}</Text>
                  </View>

                  <View style={{width:'100%',height:0.5,backgroundColor:'#DDDDDD',marginLeft:BaseComponent.W*0.027}}></View>
                  <View style={styles.CostBreakdownTongyong}>
                      <Text style={{fontSize:BaseComponent.W*0.032,color:'#6B6B6B',marginLeft:BaseComponent.W*0.027}}>服务</Text>
                      <Text style={{fontSize:BaseComponent.W*0.037,color:"#121212",marginRight:BaseComponent.W*0.029}}>¥{thiz.state.data.orderInfo.specialServiceAmount}</Text>
                  </View>

                  <View style={{width:'100%',height:0.5,backgroundColor:'#DDDDDD',marginLeft:BaseComponent.W*0.027}}></View>
                  <View style={styles.CostBreakdownTongyong}>
                      <Text style={{fontSize:BaseComponent.W*0.032,color:'#6B6B6B',marginLeft:BaseComponent.W*0.027}}>邮费</Text>
                      <Text style={{fontSize:BaseComponent.W*0.037,color:"#121212",marginRight:BaseComponent.W*0.029}}>¥{thiz.state.data.orderInfo.logisticAmount}</Text>
                  </View>     
              </View>

              {/*下单时间和编号*/}
              <View style={{width:BaseComponent.W,height:BaseComponent.W*115/375,flexDirection:'column',backgroundColor:'white',marginTop:BaseComponent.W*10/375}}>
                  <View style={{width:'100%',flexDirection:'row',justifyContent:'space-between',marginLeft:BaseComponent.W*0.0293}}>
                      <Text style={{color:'#121212',fontSize:BaseComponent.W*0.0346,marginTop:BaseComponent.W*15/375}}>订单编号:&nbsp;&nbsp;{thiz.state.data.orderInfo.businessOrderNo}</Text>
                      <TouchableWithoutFeedback onPress={()=>{
                        thiz.copyClipboard();
                      }}>
                      <View style={{width:BaseComponent.W*50/375,height:BaseComponent.W*20/375,marginTop:BaseComponent.W*15/375,marginRight:BaseComponent.W*20/375,
                                  borderRadius:BaseComponent.W*10/375,borderWidth:1,borderColor:'#CECECE',justifyContent:'center',alignItems:'center'}}>
                            <Text style={{fontSize:BaseComponent.W*13/375,color:'#999999'}}>复制</Text>
                      </View>
                      </TouchableWithoutFeedback>
                  </View>
                  <Text style={{color:'#121212',fontSize:BaseComponent.W*0.0346,marginTop:BaseComponent.W*1/375,marginLeft:BaseComponent.W*0.0293}}>支付方式:&nbsp;&nbsp;{thiz.state.data.orderInfo.payChannelOrder.payMode.payChannel.name}</Text>
                  <Text style={{color:'#121212',fontSize:BaseComponent.W*0.0346,marginTop:BaseComponent.W*5/375,marginLeft:BaseComponent.W*0.0293}}>支付交易单号:&nbsp;&nbsp;{thiz.state.data.orderInfo.payChannelOrder.channelOrderNo}</Text>
                  <Text style={{color:'#121212',fontSize:BaseComponent.W*0.0346,marginTop:BaseComponent.W*5/375,marginLeft:BaseComponent.W*0.0293}}>下单时间:&nbsp;&nbsp;{thiz.state.data.orderInfo.orderDatetime}</Text>
              </View>
              </ScrollView>
              
              {/*取消售后*/}
              <TouchableWithoutFeedback onPress={()=>{
                var thiz=this;
                this.cancel(thiz.state.data.id);
              }}>
                  <View style={{display:thiz.state.data.applyRefundProgress=="NOT_AUDIT"?'flex':'none',width:'100%',height:BaseComponent.W*50/375,marginTop:BaseComponent.W*10/375,backgroundColor:'white',flexDirection:'row',justifyContent:'flex-end',alignItems:'center'}}>
                      <View style={{width:BaseComponent.W*70/375,height:BaseComponent.W*30/375,borderRadius:BaseComponent.W*15/375,borderWidth:1,borderColor:'#606060',justifyContent:'center',alignItems:'center',marginRight:BaseComponent.W*12/375}}>
                          <Text style={{fontSize:BaseComponent.W*13/375,color:'#252525'}}>取消售后</Text>
                      </View>
                  </View>
              </TouchableWithoutFeedback>        
          </View>    
        )
    }
}
const styles=StyleSheet.create({
  CostBreakdownTongyong:{
      width:'100%',
      height:BaseComponent.W*0.0773,
      flexDirection:'row',
      justifyContent:'space-between',
      alignItems:'center',
  }
})
