/**
 * @name OrderAmountStatistics.js
 * @auhor 程浩
 * @date 2018.8.23
 * @desc 订单额统计
 */
import React,{Component} from 'react';
import {Text,View,StyleSheet,Image,TouchableWithoutFeedback,ScrollView,TextInput,StatusBar,BackHandler,Modal,ActivityIndicator,RefreshControl} from 'react-native';
 // 导入工具函数库
import T from "../../lib/Tools";
// 导入公共样式
import St from "../../lib/Style";
// 导入图片资源库
import Imgs from "../../lib/Img";
import BaseComponent from '../BaseComponent';
// 转菊花
import Spinner from 'react-native-loading-spinner-overlay';
import SelectAddressOrDate from '../mine/SelectAddressOrDate';


// Tools实例
let ti = T.getInstance();

let style = StyleSheet.create({
  wrapper:{
    backgroundColor:"#F0F0F0",
    flex:1,
    flexDirection:"column",
    alignItems:"stretch",
    justifyContent:"flex-start",
    position:"relative",
  },
  statusBar:{
    backgroundColor:"rgb(255,255,255)",
    height:ti.select({ios:16,android:22}),
    position:"relative",
    zIndex:1,
    opacity:1,
  },
  navBar:{
    backgroundColor:"rgb(255,255,255)",
    height:"7%",
    position:"relative",
    zIndex:1,
    opacity:1,
    borderBottomWidth:1,
    borderBottomColor:"rgb(241,241,241)",
    flexDirection:'row',
    alignItems:'center',
  },
});

export default class OrderAmountStatistics extends BaseComponent{
  constructor(props){
    super(props);
    this.state={
      visible:false,
      date:[],//查看的哪一天的日期
      choose:true,//true表示选择查看有效订单,false表示选择售后退款
      isVisible:false,
      totalAmount:"",//订单总金额
      validTotalAmount:"",//有效订单额
      refundTotalAmount:"",//退款总额
      pageNumber:1,
      pageSize:BaseComponent.PAGE_SIZE,
      hasNext:false,
      records:[],//数据源
      showDefault:false,
      isRefreshing:false,
    }
  };

  componentWillMount(){
    var thiz=this;
    //获取当前的年月
    var date=new Date();
    var year=date.getFullYear();
    var month=date.getMonth()+1;
    var dateArray=[];
    dateArray.push(year);
    dateArray.push(month);
    thiz.setState({date:dateArray});
  }

  componentDidMount(){
    var thiz=this;
    //获取本月的订单额
    thiz.getOrderAmountStatistics();
    thiz.getValidPage();
    //监听选择日期
    thiz.listen("selected_date",function(date){
      console.log("-------------------------date----------------------------",date);
      var state=thiz.state;
      state.date=date;
      state.isVisible=false;
      state.pageNumber=1;
      state.records=[];
      thiz.setState(state);
      if(thiz.state.choose){
        thiz.getOrderAmountStatistics();
        thiz.getValidPage();
      }else{
        thiz.getOrderAmountStatistics();
        thiz.getAfterSalePage();
      }
      
    });
    //监听取消选择日期
    thiz.listen("cancel_selected",function(){
      thiz.setState({isVisible:false});
    });
  };

  //获取本月订单额
  getOrderAmountStatistics=()=>{
    var thiz=this;
    var dateArray=thiz.state.date;
    if(dateArray[1]<10){
      var data=dateArray[0]+"-0"+dateArray[1];
    }else{
      var data=dateArray[0]+"-"+dateArray[1];
    }
   
    
    thiz.request("order/getOrderAmountStatistics",{
      method:"POST",
      data:{selectedMonth:data},
      success:function(ret){
        if(ret&&ret.data){
          thiz.setState({
            totalAmount:ret.data.totalAmount,
            validTotalAmount:ret.data.validTotalAmount,
            refundTotalAmount:ret.data.refundTotalAmount,
          })
        }
      },
      error:function(){

      }
    })
  };

  //下拉刷新
  onRefresh=()=>{
    var thiz=this;
    var state=thiz.state;
    state.records=[];
    state.pageNumber=1;
    state.isRefreshing=true;
    thiz.setState(state);
    if(thiz.state.choose){
      thiz.getOrderAmountStatistics();
      thiz.getValidPage();
    }else{
      thiz.getOrderAmountStatistics();
      thiz.getAfterSalePage();
    }
    
    thiz.setState({isRefreshing:false});
  }

  //获取有效订单
  getValidPage=()=>{
    var thiz=this;
    var dateArray=thiz.state.date;
   
    if(dateArray[1]<10){
      var data=dateArray[0]+"-0"+dateArray[1];
    }else{
      var data=dateArray[0]+"-"+dateArray[1];
    }
    

    var pageNumber=thiz.state.pageNumber;
    var pageSize=thiz.state.pageSize;
    

    thiz.setState({visible:true});
    thiz.request("order/getValidPage",{
      method:"POST",
      data:{
        selectedMonth:data,
        page:{
          pageNumber:pageNumber,
          pageSize:pageSize,
        }
      },
      success:function(ret){
        thiz.setState({visible:false});
        if(ret&&ret.data){
          if(ret.data.records&&ret.data.records.length>0){
            var records=thiz.state.records;
            thiz.log("------------------records-------------------",records);
            for(var i=0;i<ret.data.records.length;i++){
              records.push(ret.data.records[i])
            }
            thiz.setState({
              records:records,
              showDefault:false,
              hasNext:ret.data.hasNext,
            });
          }else{
            thiz.setState({showDefault:true,records:[]});
          }
        }
      },
      error:function(){
        thiz.setState({visible:false});
      }
      })
    }

   callBackDateValue(value){
        this.setState({
            date:value
        })
  };

  //获取售后退款
  getAfterSalePage=()=>{
    var thiz=this;
    var dateArray=thiz.state.date;
    
    if(dateArray[1]<10){
      var data=dateArray[0]+"-0"+dateArray[1];
    }else{
      var data=dateArray[0]+"-"+dateArray[1];
    }
    
    var pageNumber=thiz.state.pageNumber;
    var pageSize=thiz.state.pageSize;
    thiz.setState({visible:true});
    thiz.request("refundOrder/getPage",{
      method:"POST",
      data:{
        selectedMonth:data,
        page:{
          pageNumber:pageNumber,
          pageSize:pageSize,
        }
      },
      success:function(ret){
        thiz.setState({visible:false});
        if(ret&&ret.data){
          if(ret.data.records&&ret.data.records.length>0){
            var records=thiz.state.records;
            thiz.log("------------------records-------------------",records);
            for(var i=0;i<ret.data.records.length;i++){
              records.push(ret.data.records[i]);
            }
            thiz.setState({
              records:records,
              showDefault:false,
              hasNext:ret.data.hasNext,
            });
          }else{
            thiz.setState({
              showDefault:true,
              records:[],
            })
          }
        }
      },
      error:function(err){
        thiz.setState({visible:false});
      }
    })
  }
  open(){
      this.refs.SelectAddressOrDate.showDate(this.state.date)
  };
  render(){
    let thiz=this;
    return (
      <View style={style.wrapper}>

          {/*转菊花*/}
          <Modal
            visible={this.state.visible}
            onRequestClose={()=>{this.setState({visible:false})}}
            transparent={true}
            animationType={"fade"}>
            <View style={{backgroundColor:"rgba(14,14,14,0.5)",width:"100%",width:"100%",flex:1,flexDirection:"column",alignItems:"center",justifyContent:"center",position:"relative"}}>
              <View>
                    <ActivityIndicator size="large" color='white'/>
              </View>
            </View>
          </Modal>  
         
          {/*状态栏占位*/}
          <View style={{backgroundColor:this.state.visible?"rgba(14,14,14,0.5)":(thiz.state.isVisible?'rgba(0,0,0,0.8)':'#fff'),
                        height:BaseComponent.SH,
                        position:"relative",
                        zIndex:1,
                        opacity:1,}}></View>
          {/*顶部导航栏*/}
          <View style={style.navBar}>
            <View style={{flex:1,flexDirection:"row",justifyContent:"flex-start",alignItems:'center'}}>

              <TouchableWithoutFeedback onPress={()=>this.goBack()}>
              <View style={{width:"15%",height:"100%",backgroundColor:"transparent",justifyContent:"center",marginLeft:BaseComponent.W*10/375}}>
                <Image source={Imgs.back} style={{width:(BaseComponent.W*0.127*0.4),height:(BaseComponent.W*0.127*0.4),marginBottom:ti.select({ios:BaseComponent.W*7/375,android:5})}}></Image>
              </View>
              </TouchableWithoutFeedback>

              <View style={{flexGrow:1,backgroundColor:"transparent",borderWidth:0}}>
                <Text style={{color:"#0A0A0A",width:"100%",textAlign:"center",marginTop:ti.select({ios:"4%",android:"1.6%"}),fontSize:BaseComponent.W*18/375}}>{this.params.title}</Text>
              </View>
               
              <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center",}}>
                  <Text style={{fontSize:BaseComponent.W*0.037,color:'black'}}></Text>
              </View>
            </View>
          </View>

          <Modal visible={this.state.isVisible}
                 onRequestClose={()=>{
                  thiz.refs.SelectAddressOrDate.hide();
                  this.setState({isVisible:false});
                }}
                 transparent={true}
                 onShow={()=>{
                  thiz.open();
                 }}>
                 <View style={{flex:1,backgroundColor:'rgba(0,0,0,0.8)'}}></View>
          </Modal>

          
          <SelectAddressOrDate ref={'SelectAddressOrDate'} callBackDateValue={this.callBackDateValue.bind(this)}/>
          
         {/*数据选择加展示*/}
         <View style={{width:'100%',height:BaseComponent.W*160/375,backgroundColor:'#000000'}}>
              <TouchableWithoutFeedback onPress={()=>{
                
                thiz.setState({isVisible:true})
              }}>
                  <View style={{flexDirection:'row',width:BaseComponent.W*359/375,marginLeft:BaseComponent.W*150/375,marginTop:BaseComponent.W*12/375,alignItems:'center',}}>
                        <Text style={{fontSize:BaseComponent.W*20/375,color:'white'}}>{thiz.state.date[1]}<Text style={{fontSize:BaseComponent.W*13/375,color:'white'}}>/{thiz.state.date[0]}</Text></Text>
                        <View style={{width:BaseComponent.W*24/375,height:BaseComponent.W*15/375,marginTop:2}}>
                            <Image style={{width:BaseComponent.W*24/375,height:BaseComponent.W*15/375}} source={require('../../image/home/downjiantou1.png')} resizeMode="cover"></Image>
                        </View>
                  </View>
              </TouchableWithoutFeedback>

              <View style={{width:BaseComponent.W*359/375,marginLeft:BaseComponent.W*16/375,flexDirection:'row',marginTop:BaseComponent.W*10/375}}>
                    <Text style={{fontSize:BaseComponent.W*12/375,color:'white'}}>有效订单额(元):&nbsp;&nbsp;&nbsp;&nbsp;<Text style={{fontSize:BaseComponent.W*26/375,color:'#FDD17A'}} numberOfLines={1}>￥{thiz.state.validTotalAmount}</Text></Text>
              </View>

              <View style={{width:BaseComponent.W*359/375,marginLeft:BaseComponent.W*16/375,flexDirection:'row',marginTop:BaseComponent.W*15/375}}>
                    <Text style={{fontSize:BaseComponent.W*12/375,color:'white'}} numberOfLines={1}>总订单额(元):&nbsp;&nbsp;￥{thiz.state.totalAmount}</Text>   
              </View>

              <View style={{width:BaseComponent.W*359/375,marginLeft:BaseComponent.W*16/375,flexDirection:'row',marginTop:BaseComponent.W*15/375}}>
                    <Text style={{fontSize:BaseComponent.W*12/375,color:'white'}} numberOfLines={1}>退&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;款(元):&nbsp;&nbsp;￥{thiz.state.refundTotalAmount}</Text>   
              </View>
         </View>

          {/*选择有效订单或售后退款*/}  
          <View style={{width:'100%',height:BaseComponent.W*45/375,backgroundColor:'white',marginTop:BaseComponent.W*10/375,flexDirection:'row',alignItems:'center',justifyContent:'center'}}>
              <TouchableWithoutFeedback onPress={()=>{
                thiz.setState({
                  choose:true,
                  records:[],
                  pageNumber:1,
                },function(){
                  thiz.getValidPage();
                });

              }}> 
                  <View style={{width:BaseComponent.W*80/375,height:'100%',alignItems:'center'}}>
                      <Text style={{fontSize:BaseComponent.W*17/375,color:thiz.state.choose?'#0A0A0A':'#898989',marginTop:BaseComponent.W*10/375}}>有效订单</Text>
                      <View style={{width:BaseComponent.W*40/375,height:2,borderRadius:1,backgroundColor:'#1C1C1C',marginTop:5,display:thiz.state.choose?'flex':'none'}}></View>
                  </View>
              </TouchableWithoutFeedback>
              
              <TouchableWithoutFeedback onPress={()=>{
                thiz.setState({
                  choose:false,
                  records:[],
                  pageNumber:1,
                },function(){
                  thiz.getAfterSalePage();
                });
              }}>
                  <View style={{width:BaseComponent.W*80/375,height:'100%',alignItems:'center',marginLeft:BaseComponent.W*80/375}}>
                      <Text style={{fontSize:BaseComponent.W*17/375,color:thiz.state.choose?'#898989':'#0A0A0A',marginTop:BaseComponent.W*10/375}}>售后退款</Text>
                      <View style={{width:BaseComponent.W*40/375,height:2,borderRadius:1,backgroundColor:'#1C1C1C',marginTop:5,display:thiz.state.choose?'none':'flex'}}></View>
                  </View>
              </TouchableWithoutFeedback>    
          </View>

          {/*没有数据的缺省图*/}
          <View style={{flexGrow:1,justifyContent:'center',alignItems:'center',display:thiz.state.showDefault?'flex':'none'}}>
                <Image style={{width:BaseComponent.W*120/375,height:BaseComponent.W*95/375}} source={require('../../image/mine/nodata.png')}></Image>
                <Text style={{fontSize:BaseComponent.W*16/375,color:'black',marginTop:10}}>暂时没有数据</Text>
          </View>

          {/*数据列表*/}
          <ScrollView style={{backgroundColor:'#F0F0F0'}}
                      onMomentumScrollEnd={(e)=>{
                          var offsetY = e.nativeEvent.contentOffset.y; //滑动距离
                          var contentSizeHeight = e.nativeEvent.contentSize.height; //scrollView contentSize高度
                          var oriageScrollHeight = e.nativeEvent.layoutMeasurement.height; //scrollView高度
                          console.log("---------------------滑动距离-------------------",offsetY);
                          console.log("---------------------scrollView contentSize高度-------------------",contentSizeHeight);
                          console.log("---------------------scrollView高度-------------------",oriageScrollHeight);
                          if (Math.abs((offsetY + oriageScrollHeight) - contentSizeHeight)<5){
                            var hasNext=thiz.state.hasNext;
                            console.log("------------------------------------------hasNext-------------------------------------",hasNext);
                            if(thiz.state.choose){
                                if(hasNext){
                                var pageNumber=thiz.state.pageNumber;
                                pageNumber++;
                                thiz.setState({pageNumber:pageNumber},function(){
                                  thiz.getValidPage();
                                })
                              }
                            }else{
                                if(hasNext){
                                var pageNumber=thiz.state.pageNumber;
                                pageNumber++;
                                thiz.setState({pageNumber:pageNumber},function(){
                                 thiz.getAfterSalePage();
                                })
                              }
                            }
                            
                          }
                      }}
                      refreshControl={
                        <RefreshControl
                          refreshing={thiz.state.isRefreshing}
                          onRefresh={()=>{thiz.onRefresh()}}
                          colors={['#ff0000', '#00ff00','#0000ff','#3ad564']}
                          progressBackgroundColor="#ffffff"
                        />
                      }>
              {
                thiz.state.records.length>0?thiz.state.records.map((item,index)=>{
                  return (
                    <TouchableWithoutFeedback onPress={()=>{
                      thiz.log("-----------------------item----------------------------",item);
                      if(thiz.state.choose){
                        thiz.navigate("DingdanXiangqingDaifahuo",{title:'订单详情',businessOrderNo:item.businessOrderNo});
                      }else{
                        thiz.navigate('AfterSalesDetails',{title:'售后详情',serialNo:item.serialNo});
                      }
                    }}>
                        <View style={{width:'100%',height:BaseComponent.W*45/375,marginTop:BaseComponent.W*5/375,flexDirection:'row',alignItems:'center',justifyContent:'flex-end',backgroundColor:'white'}}>
                            <Text style={{fontSize:BaseComponent.W*12/375,color:'#898989',position:'absolute',left:BaseComponent.W*15/375}}>{item.orderDatetime}</Text>
                            <Text style={{fontSize:BaseComponent.W*14/375,color:'#000000',position:'absolute',left:BaseComponent.W*170/375,width:BaseComponent.W*90/375}} numberOfLines={1}>￥{thiz.state.choose?item.payAmount:item.refundAmount}</Text>
                            <Text style={{fontSize:BaseComponent.W*12/375,color:'#FF0000',}}>交易成功</Text>
                            <Image style={{width:BaseComponent.W*30/375,height:BaseComponent.W*30/375,marginRight:BaseComponent.W*15/375}} source={require('../../image/home/youjiantou.png')}></Image>
                        </View>
                    </TouchableWithoutFeedback>
                  )
                }):null
              }
              {
                thiz.state.showDefault?null:thiz.state.hasNext?null:thiz.nomore({bgcolor:'#F0F0F0'})
              }
          </ScrollView>
      </View>          
    )
  }
}
