/**
 * @name DingdanQuanbu.js
 * @auhor 程浩
 * @date 2018.8.20
 * @desc 全部订单
 */
import React,{Component} from 'react';
import {Platform,Dimensions,Text,TextInput,View,StyleSheet,Image,TouchableWithoutFeedback,StatusBar,SectionList,ScrollView,RefreshControl,ActivityIndicator,Modal} from 'react-native';
import BaseComponent from '../BaseComponent';
// import DingdanXiangqingDaizhifu from '../mine/DingdanXiangqingDaizhifu';
// import DingdanXiangqingDaifahuo from '../mine/DingdanXiangqingDaifahuo';
// import DingdanXiangqingDaishouhuo from '../mine/DingdanXiangqingDaishouhuo';
// import DingdanXiangqingFinishDeal from '../mine/DingdanXiangqingFinishDeal';
// import DingdanXiangqingFinishAfterSales from '../mine/DingdanXiangqingFinishAfterSales';
import SelectAddressOrDate  from '../mine/SelectAddressOrDate';
// 转菊花
import Spinner from 'react-native-loading-spinner-overlay';

// 导入工具函数库
import T from "../../lib/Tools";
// 导入公共样式
import St from "../../lib/Style";
// 导入图片资源库
import Imgs from "../../lib/Img";

// Tools实例
let ti = T.getInstance();

export default class DingdanQuanbu extends BaseComponent{
	constructor(props){
		super(props);
		this.state={
			pageNumber:1,//当前页数
			pageSize:10,//每页的数据条数
			hasNext:true,//是否有下一页
			pageElements:0,
			totalPages:1,//总页数
			totalElements:0,
			hasPrevious:false,
			dingdanData:[],
			reason:[],//取消订单原因
			isRefreshing:false,//下拉刷新,
			visible:false,//转菊花
			shouhuovisible:false,//提醒订单确认收货
			SelectAddressOrDate:false,//true表示选择显示取消订单原因弹出框
			netError:false,//true表示网络错误
			id:"",//包裹id
			// 搜索关键字
			searchKeywords:"",
		}
		this.inputKeywords = "";
	};

	//获取数据
	getData=()=>{
		var thiz=this;
		console.log("------------------------First_Get_Pageumber---------",thiz.state.pageNumber);
		thiz.setState({visible:true});
		thiz.request("order/getPageWithRepertory",{
			method:"POST",
			data:{
				page:{
					pageSize:thiz.state.pageSize,
					pageNumber:thiz.state.pageNumber,
				},
				searchKeywords:thiz.state.searchKeywords,
			},
			success:function(ret){
				if(!ret.data.records||ret.data.records.length==0)
		        {
		          thiz.setState({visible:false});
		          return ;
		        }
				//获取数据变成我想要的结构
				if(ret.data.records)
				{	
					var dingdanData=thiz.state.dingdanData;
					for(var i=0;i<ret.data.records.length;i++)
					{
						var records={};
						records.id=ret.data.records[i].id;
						records.businessOrderNo=ret.data.records[i].businessOrderNo;
						records.orderStatus=ret.data.records[i].orderStatus;
						records.orderPackages=[];
						
						
						for(var ii=0;ii<ret.data.records[i].orderPackages.length;ii++)
						{
							var orderPackages={};
							orderPackages.id=ret.data.records[i].orderPackages[ii].id;
							orderPackages.name=ret.data.records[i].orderPackages[ii].name;
							orderPackages.receiverReceivingStatus=ret.data.records[i].orderPackages[ii].receiverReceivingStatus;
							orderPackages.deliveryStatus=ret.data.records[i].orderPackages[ii].deliveryStatus;
						
							orderPackages.orderInfoItems=[];

							for(var iii=0;iii<ret.data.records[i].orderPackages[ii].orderInfoItems.length;iii++)
							{
								if(!ret.data.records[i].orderPackages[ii].orderInfoItems[iii].sku)
								{
									ret.data.records[i].orderPackages[ii].orderInfoItems[iii].sku={};
									if(!ret.data.records[i].orderPackages[ii].orderInfoItems[iii].sku.imageAttachment)
									{
										ret.data.records[i].orderPackages[ii].orderInfoItems[iii].sku.imageAttachment={};
										if(!ret.data.records[i].orderPackages[ii].orderInfoItems[iii].sku.imageAttachment.resourceFullAddress){
											ret.data.records[i].orderPackages[ii].orderInfoItems[iii].sku.imageAttachment.resourceFullAddress="dasda";
										}
									}
								}
								var orderInfoItems={};
								orderInfoItems.id=ret.data.records[i].orderPackages[ii].orderInfoItems[iii].id;
								orderInfoItems.num=ret.data.records[i].orderPackages[ii].orderInfoItems[iii].num;
								orderInfoItems.buyUnitAmount=ret.data.records[i].orderPackages[ii].orderInfoItems[iii].buyUnitAmount;
								orderInfoItems.img=ret.data.records[i].orderPackages[ii].orderInfoItems[iii].sku.imageAttachment.resourceFullAddress;
								orderInfoItems.skuName=ret.data.records[i].orderPackages[ii].orderInfoItems[iii].skuName;
								orderInfoItems.goodsAttributeOptions=ret.data.records[i].orderPackages[ii].orderInfoItems[iii].sku.goodsAttributeOptions;
								orderPackages.orderInfoItems.push(orderInfoItems);
							}
							records.orderPackages.push(orderPackages);
						}
						dingdanData.push(records);
					}
					thiz.log("------------dingdanData-----------------",dingdanData);
					thiz.setState({
						dingdanData:dingdanData,
						isRefreshing:false,
						visible:false,
						hasNext:ret.data.hasNext
					})
				}
			},
			error:function(err){
				thiz.setState({visible:false,isRefreshing:false});
				if(err.code&&err.code==200){
					console.log("111111111111111111111111");
		            thiz.setState({netError:true});
		        } 
			}
		})
	};
	
	componentDidMount(){
		var thiz=this;
		
		thiz.getData();

		//监听消息确认收货
		thiz.listen('confirm_Received',function(){
			thiz.state.dingdanData=[];
			thiz.getData();
		});
		//监听消息删除订单
		thiz.listen('delete_order_success',function(){
			thiz.state.dingdanData=[];
			thiz.getData();
		});
		//监听消息取消订单
		thiz.listen("cancel_order_success",function(){
			thiz.state.dingdanData=[];
			thiz.getData();
		});
		 //监听订单支付成功消息
        thiz.listen("pay_order_success",function(){
          	thiz.state.dingdanData=[];
			thiz.getData();
        });
	};
	callBackAddressValue(value){
	        this.setState({
	            reason:value
	        });
	};
	open(){
	     this.refs.SelectAddressOrDate.showAddress(this.state.reason)
	};
	
	//点击列表去付款
	clickToPay=(item)=>{
		var thiz=this;
		if(thiz.state.SelectAddressOrDate)
		{
			thiz.refs.SelectAddressOrDate.hide();
			thiz.setState({SelectAddressOrDate:false});
		}
		thiz.log("---------------------------item---------------------",item);
		if(item.orderStatus=="WAIT_FOR_PAY")
		{
			thiz.navigate('DingdanXiangqingDaizhifu',{title:'订单详情',businessOrderNo:item.businessOrderNo});
		}

		if(item.orderStatus=="CANCELED")
		{
			thiz.navigate('DingdanXiangqingDaizhifu',{title:'订单详情',businessOrderNo:item.businessOrderNo});
		}

		if(item.orderStatus!="CANCELED"&&item.orderStatus!="WAIT_FOR_PAY")
		{
			thiz.navigate('DingdanXiangqingDaifahuo',{title:"订单详情",businessOrderNo:item.businessOrderNo});
		}
			
	};
	//下拉刷新
	onRefresh=()=>{
		var thiz=this;
		var state=thiz.state;
		state.dingdanData=[];
		state.pageNumber=1;
		state.isRefreshing=true;
		thiz.setState(state);
		thiz.getData();
	}
	_keyExtractor=(item,index)=>item+index;

	render(){
		let thiz=this;
		return (
			<View style={styles.wrapper}>
			 {/*状态栏占位*/}
			 <StatusBar translucent={true} backgroundColor={this.state.visible||this.state.shouhuovisible?"rgba(14,14,14,0.5)":"rgb(255,255,255)"} barStyle={'dark-content'}/>

		        {/*状态栏占位*/}
		        <View style={{backgroundColor:"rgb(255,255,255)",
							    height:thiz.isIphoneX(30,BaseComponent.SH),
							    position:"relative",
							    zIndex:1,
							    opacity:1,}}></View>
		        {/*顶部导航栏*/}
		        <View style={styles.navBar}>
		          <View style={{flex:1,flexDirection:"row",justifyContent:"flex-start"}}>

		            <TouchableWithoutFeedback onPress={()=>this.goBack()}>
		            <View style={{width:"12.7%",height:"100%",backgroundColor:"white",justifyContent:'center'}}>
		               <Image source={Imgs.back} style={{width:(BaseComponent.W*0.127*0.4),height:(BaseComponent.W*0.127*0.4),marginLeft:BaseComponent.W*0.02}}></Image>
		            </View>
		            </TouchableWithoutFeedback>

		           
		            <View style={{height:ti.select({ios:(BaseComponent.H*0.05),android:(BaseComponent.H*0.05)}),flexGrow:1,backgroundColor:"transparent",
		              marginTop:ti.select({ios:(BaseComponent.H*0.02)/2,android:(BaseComponent.H*0.016)/2}),borderRadius:24,borderColor:"#C7C7C7",
		              borderWidth:1,alignItems:"center",justifyContent:"center"}}>

		              <TextInput style={{width:'100%',padding:0,backgroundColor:"transparent",marginLeft:BaseComponent.W*30/375,color:"#121212"}} placeholderTextColor="#C7C7C7"
		               placeholder="请输入姓名，电话，订单号或运单号" underlineColorAndroid='transparent' maxLength={30} onSubmitEditing={(event)=>{

		               		// 保存输入结果
		               		var state=thiz.state;
							state.dingdanData=[];
							state.pageNumber=1;
							state.isRefreshing=true;
							state.searchKeywords=event.nativeEvent.text;
							thiz.setState(state);
		               		// 执行搜索
		               		thiz.getData();
		               		
		               }} onChangeText={(event)=>{
                        	thiz.inputKeywords = event;
                        }}>
		               </TextInput>

		            </View>
		          	
		          	<TouchableWithoutFeedback onPress={()=>{

		          		// 保存输入结果
	               		var state=thiz.state;
						state.dingdanData=[];
						state.pageNumber=1;
						state.isRefreshing=true;
						state.searchKeywords=thiz.inputKeywords;
						thiz.setState(state);
	               		// 执行搜索
		               	thiz.getData();

		          	}}>
		            <View style={{width:"12.7%",height:"100%",backgroundColor:"transparent",justifyContent:"center",alignItems:"center"}}>   
		            	<Text style={{color:'rgba(135, 134, 135, 1)'}}>搜索</Text>
		            </View>
		            </TouchableWithoutFeedback>
		          </View>
		        </View>


                {/*提醒确认收货订单*/}
                <Modal visible={this.state.shouhuovisible}
                       onRequestClose={()=>{this.setState({shouhuovisible:false})}}
                       transparent={true}>
                       <View style={{flex:1,backgroundColor:'rgba(14,14,14,0.5)',justifyContent:'center',alignItems:'center'}}>
                          <View style={{width:BaseComponent.W*275/375,height:BaseComponent.W*105/375,backgroundColor:'rgba(255, 255, 255,1)',borderRadius:BaseComponent.W*20/375}}>
                              <View style={{width:'100%',height:BaseComponent.W*56/375,justifyContent:'center',alignItems:'center'}}>
                                <Text style={{fontSize:BaseComponent.W*15/375,color:'#3a3a3a'}}>确认收货吗?</Text>
                              </View>
                              <View style={{width:'100%',height:0.5,backgroundColor:'rgba(189, 189, 189, 1)'}}></View>
                              <View style={{width:'100%',height:BaseComponent.W*49/375,flexDirection:'row'}}>
                              <TouchableWithoutFeedback onPress={()=>this.setState({shouhuovisible:false})}> 
                                <View style={{width:'50%',height:BaseComponent.W*49/375,justifyContent:'center',alignItems:'center'}}>
                                  <Text style={{fontSize:BaseComponent.W*15/375,color:'#3a3a3a'}}>取消</Text>
                                </View>
                              </TouchableWithoutFeedback> 
                                <View style={{width:0.5,height:'100%',backgroundColor:'rgba(189, 189, 189, 1)'}}></View>
                                <TouchableWithoutFeedback onPress={()=>{
                                      // 订单确认收货
                                      thiz.request("orderPackage/confirmReceived",{
								        method:"POST",
								        data:{id:thiz.state.id},
								        success:function(ret){
							      			thiz.setState({shouhuovisible:false});
											thiz.emit('confirm_Received');
								        },
								        error:function(err){

								        }
								      })
                                }}> 
                                <View style={{width:'50%',height:BaseComponent.W*49/375,justifyContent:'center',alignItems:'center'}}>
                                  <Text style={{fontSize:BaseComponent.W*15/375,color:'#FED584'}}>确认收货</Text>
                                </View>
                                </TouchableWithoutFeedback>
                              </View>
                          </View>
                       </View>
                </Modal>
                             
				{/*转菊花*/}
		        <Modal
		          visible={this.state.visible}
		          onRequestClose={()=>{this.setState({visible:false})}}
		          transparent={true}
		          animationType={"fade"}>
		          <View style={{backgroundColor:"rgba(14,14,14,0.5)",width:"100%",width:"100%",flex:1,flexDirection:"column",alignItems:"center",justifyContent:"center",position:"relative"}}>
		            <View>
		                  <ActivityIndicator size="large" color='white'/>
		            </View>
		          </View>
		        </Modal>

		        {
		        	thiz.state.netError?(
						 <View style={{width:BaseComponent.W,flexGrow:1,justifyContent:'center',alignItems:'center'}}>
		                  
		                  <Image style={{width:BaseComponent.W*180/375,height:BaseComponent.W*180/375,borderRadius:BaseComponent.W*90/375}} source={require('../../image/mine/neterror.png')}></Image>
		                  

		                  <TouchableWithoutFeedback onPress={()=>{
		                    thiz.setState({netError:false});
		                    
		                    thiz.getData();
		                  }}>
		                  <View style={{width:BaseComponent.W*70/375,height:BaseComponent.W*30/375,borderRadius:BaseComponent.W*15/375,borderWidth:0.5,borderColor:'#080808',
		                            justifyContent:'center',alignItems:'center',marginTop:BaseComponent.W*15/375}}>
		                        <Text style={{fontSize:BaseComponent.W*15/375,color:'#252525'}}>刷新</Text>          
		                  </View>
		                  </TouchableWithoutFeedback>
		                </View>
		        	):(thiz.state.dingdanData.length==0?(
			        	<View style={{width:BaseComponent.W,flexGrow:1,justifyContent:'center',alignItems:'center'}}>
		                    <Image style={{width:BaseComponent.W*180/375,height:BaseComponent.W*180/375,borderRadius:BaseComponent.W*90/375}} source={require('../../image/mine/noOrder.png')}></Image>
		                </View>):(
				        	<View style={{display:"flex",height:BaseComponent.H*0.93-thiz.isIphoneX(30,BaseComponent.SH),width:"100%",backgroundColor:"transparent"}}>
								 	<ScrollView showsVerticalScrollIndicator={false}
								 	
								 	style={{backgroundColor:'#F0F0F0',}}
									onMomentumScrollEnd={(e)=>
										{
											var offsetY = e.nativeEvent.contentOffset.y; //滑动距离
		        							var contentSizeHeight = e.nativeEvent.contentSize.height; //scrollView contentSize高度
		        							var oriageScrollHeight = e.nativeEvent.layoutMeasurement.height; //scrollView高度
		        							console.log("---------------------滑动距离-------------------",offsetY);
		        							console.log("---------------------scrollView contentSize高度-------------------",contentSizeHeight);
		        							console.log("---------------------scrollView高度-------------------",oriageScrollHeight);
		        							if (Math.abs((offsetY + oriageScrollHeight) - contentSizeHeight)<5)
		        							{
	           	 								var pageNumber=thiz.state.pageNumber+1;
	           	 								var hasNext=thiz.state.hasNext;
	           	 								if(hasNext){

		           	 								thiz.setState({
		           	 									pageNumber:pageNumber,
		           	 									visible:true,
		           	 								});
		           										thiz.request("order/getPage",{
														method:"POST",
														data:{
															page:{
																pageSize:thiz.state.pageSize,
																pageNumber:pageNumber
															},
															searchKeywords:thiz.state.searchKeywords,
														},
														success:function(ret){
															//获取数据变成我想要的结构
															if(ret.data.records)
															{	
																var dingdanData=thiz.state.dingdanData;
																for(var i=0;i<ret.data.records.length;i++)
																{
																	var records={};
																	records.id=ret.data.records[i].id;
																	records.businessOrderNo=ret.data.records[i].businessOrderNo;
																	records.orderStatus=ret.data.records[i].orderStatus;
																	records.orderPackages=[];
																	
																	
																	for(var ii=0;ii<ret.data.records[i].orderPackages.length;ii++)
																	{
																		var orderPackages={};
																		orderPackages.id=ret.data.records[i].orderPackages[ii].id;
																		orderPackages.name=ret.data.records[i].orderPackages[ii].name;
																		orderPackages.receiverReceivingStatus=ret.data.records[i].orderPackages[ii].receiverReceivingStatus;
																		orderPackages.deliveryStatus=ret.data.records[i].orderPackages[ii].deliveryStatus;
																	
																		orderPackages.orderInfoItems=[];

																		for(var iii=0;iii<ret.data.records[i].orderPackages[ii].orderInfoItems.length;iii++)
																		{
																			if(!ret.data.records[i].orderPackages[ii].orderInfoItems[iii].sku)
																			{
																				ret.data.records[i].orderPackages[ii].orderInfoItems[iii].sku={};
																				if(!ret.data.records[i].orderPackages[ii].orderInfoItems[iii].sku.imageAttachment)
																				{
																					ret.data.records[i].orderPackages[ii].orderInfoItems[iii].sku.imageAttachment={};
																					if(!ret.data.records[i].orderPackages[ii].orderInfoItems[iii].sku.imageAttachment.resourceFullAddress){
																						ret.data.records[i].orderPackages[ii].orderInfoItems[iii].sku.imageAttachment.resourceFullAddress="dasda";
																					}
																				}
																			}
																			var orderInfoItems={};
																			orderInfoItems.id=ret.data.records[i].orderPackages[ii].orderInfoItems[iii].id;
																			orderInfoItems.num=ret.data.records[i].orderPackages[ii].orderInfoItems[iii].num;
																			orderInfoItems.buyUnitAmount=ret.data.records[i].orderPackages[ii].orderInfoItems[iii].buyUnitAmount;
																			orderInfoItems.img=ret.data.records[i].orderPackages[ii].orderInfoItems[iii].sku.imageAttachment.resourceFullAddress;
																			orderInfoItems.skuName=ret.data.records[i].orderPackages[ii].orderInfoItems[iii].skuName;
																			orderInfoItems.goodsAttributeOptions=ret.data.records[i].orderPackages[ii].orderInfoItems[iii].sku.goodsAttributeOptions;
																			orderPackages.orderInfoItems.push(orderInfoItems);
																		}
																		records.orderPackages.push(orderPackages);
																	}
																	dingdanData.push(records);
																}
																thiz.log("------------Second_dingdanData-----------------",dingdanData);
																thiz.setState({
																	dingdanData:dingdanData,
																	visible:false,
																	hasNext:ret.data.hasNext,
																});
															}
														},
														error:function(error){
															thiz.setState({visible:false});
														}
													});

		           	 							}
		        							}
										}
									}
								refreshControl={
					              <RefreshControl
					                refreshing={thiz.state.isRefreshing}
					                onRefresh={()=>{thiz.onRefresh()}}
					                colors={['#ff0000', '#00ff00','#0000ff','#3ad564']}
					                progressBackgroundColor="#ffffff"
					              />
					            }>
						<SelectAddressOrDate ref={'SelectAddressOrDate'} callBackAddressValue={this.callBackAddressValue.bind(this)}/>			
						{
							thiz.state.dingdanData.map((item,index)=>{
								
								return (
									
									<View key={index} style={{backgroundColor:'white'}}>
										<View style={{width:'100%',height:BaseComponent.W*10/375,backgroundColor:'#F0F0F0'}}></View>
										
										<TouchableWithoutFeedback onPress={()=>{
												thiz.clickToPay(item);
										}}>
										<View style={{width:'100%',height:BaseComponent.W*43/375,flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
											<Text style={{fontSize:BaseComponent.W*15/375,color:'#040000',marginLeft:BaseComponent.W*12/375}}>订单号:&nbsp;&nbsp;{item.businessOrderNo}</Text>
											<View style={{width:BaseComponent.W*130/375,height:BaseComponent.W*43/375,flexDirection:'row',alignItems:'center',justifyContent:'flex-end'}}>
												<TouchableWithoutFeedback onPress={()=>{
													//订单再次购买
												      thiz.request("order/buyAgain",{
												          method:"POST",
												          data:{businessOrderNo:item.businessOrderNo},
												          success:function(ret){
												          	  thiz.emit("click_buyAgain");
												          	  BaseComponent.CUR_TAB = "Cart";
												              thiz.navigate("Cart");
												          },
												          error:function(error){

												          }
												      });	
												}}>
												<View style={{width:BaseComponent.W*70/375,height:BaseComponent.W*30/375,borderRadius:BaseComponent.W*15/375,borderWidth:0.5,
															borderColor:'#606060',justifyContent:'center',alignItems:'center',marginRight:BaseComponent.W*15/375,display:item.orderStatus=="WAIT_FOR_PAY"?'none':'none'}}>
													<Text style={{fontSize:BaseComponent.W*13/375,color:'#252525'}}>再次购买</Text>
												</View>
												</TouchableWithoutFeedback>

												<Text style={{fontSize:BaseComponent.W*14/375,color:'#FF407E',display:item.orderStatus=="CANCELED"||item.orderStatus=="WAIT_FOR_PAY"||item.orderStatus=="APPLY_REFUND"||item.orderStatus=="REFUND_PROCESSING"||item.orderStatus=="REFUND_SUCCESS"?'flex':'none',marginRight:BaseComponent.W*15/375}}>
													{(()=>{
														if(item.orderStatus=="WAIT_FOR_PAY"){return "待付款"};
														if(item.orderStatus=="CANCELED"){return "已取消"};
														if(item.orderStatus=="APPLY_REFUND"){return "退款申请中"};
														if(item.orderStatus=="REFUND_PROCESSING"){return "退款中"};
														if(item.orderStatus=="REFUND_SUCCESS"){return "退款完成"}
													})()}	
												</Text>	
											</View>	
										</View>
										</TouchableWithoutFeedback>

										<View style={{width:BaseComponent.W*365/375,height:0.5,backgroundColor:'#DDDDDD',marginLeft:BaseComponent.W*10/375}}></View>
										{
											item.orderPackages.map((item1,index1)=>{
												
												return (
												
												<View key={index1}>
													<TouchableWithoutFeedback onPress={()=>{
															thiz.clickToPay(item);
													}}>
													<View style={{width:'100%',height:BaseComponent.W*92/375,flexDirection:'row',alignItems:'center'}}>
														<View style={{flexDirection:'row',alignItems:'center',height:BaseComponent.W*92/375,display:item.orderPackages[index1].orderInfoItems.length>1?'none':'flex'}}>	
															<View style={{width:BaseComponent.W*60/375,height:BaseComponent.W*60/375,marginLeft:BaseComponent.W*10/375}}>
																<Image style={{width:BaseComponent.W*60/375,height:BaseComponent.W*60/375}} source={{uri:item.orderPackages[index1].orderInfoItems[0].img?item.orderPackages[index1].orderInfoItems[0].img:""}}></Image>
															</View>
															<View style={{width:BaseComponent.W*155/375,height:BaseComponent.W*60/375,marginLeft:BaseComponent.W*10/375,justifyContent:'space-between'}}>
																<Text style={{fontSize:BaseComponent.W*13/375,color:'#040000',lineHeight:BaseComponent.W*18/375}} numberOfLines={2}>{item.orderPackages[index1].orderInfoItems[0].skuName}</Text>
																<Text style={{fontSize:BaseComponent.W*12/375,color:'#999899',marginTop:BaseComponent.W*5/375}}>{item.orderPackages[index1].orderInfoItems[0].goodsAttributeOptions[0].goodsAttributeOption.value}&nbsp;&nbsp;{item.orderPackages[index1].orderInfoItems[0].goodsAttributeOptions[1]?item.orderPackages[index1].orderInfoItems[0].goodsAttributeOptions[1].goodsAttributeOption.value:""}</Text>
															</View>
														</View>
														
														<View style={{width:BaseComponent.W*235/375,height:BaseComponent.W*92/375,flexDirection:'row',alignItems:'center',display:item.orderPackages[index1].orderInfoItems.length>1?'flex':'none'}}>
																{
																	item.orderPackages[index1].orderInfoItems.map((itemImg,index2)=>{
																		if(index2<=2){
																		return (
																			<View style={{width:BaseComponent.W*60/375,height:BaseComponent.W*60/375,marginLeft:BaseComponent.W*10/375}} key={index2}>
																				<Image style={{width:BaseComponent.W*60/375,height:BaseComponent.W*60/375}} source={{uri:itemImg.img}}></Image>
																			</View>
																		)
																		}
																	})
																}
														</View>
											
														<View style={{width:BaseComponent.W*80/375,height:BaseComponent.W*60/375,marginLeft:BaseComponent.W*45/375,alignItems:'flex-end',justifyContent:'space-between'}}>
															<Text style={{fontSize:BaseComponent.W*13/375,color:'#393A3A'}}>{item.orderPackages[index1].name}</Text>	
															<Text style={{fontSize:BaseComponent.W*14/375,color:item.orderPackages[index1].deliveryStatus=="NOT_DELIVER"?'#FF407E':(item.orderPackages[index1].deliveryStatus=="DELIVERED"&&item.orderPackages[index1].receiverReceivingStatus=="NOT_RECEIVED"?'#7D7D7D':'#30B507'),marginTop:BaseComponent.W*20/375,display:item.orderStatus=="APPLY_REFUND"||item.orderStatus=="CANCELED"||item.orderStatus=="WAIT_FOR_PAY"||item.orderStatus=="REFUND_PROCESSING"||item.orderStatus=="REFUND_SUCCESS"?'none':'flex'}}>
																{(()=>{
																	if(item.orderPackages[index1].deliveryStatus=="NOT_DELIVER"&& item.orderPackages[index1].receiverReceivingStatus=="NOT_RECEIVED"){return "待发货"};
																	if(item.orderPackages[index1].deliveryStatus=="DELIVERED"&& item.orderPackages[index1].receiverReceivingStatus=="NOT_RECEIVED"){return "待收货"};
																	if(item.orderPackages[index1].deliveryStatus=="DELIVERED"&& item.orderPackages[index1].receiverReceivingStatus=="CONFIRM_RECEIVED"){return "交易完成"};
																})()}	
															</Text>
														</View>
													</View>
													</TouchableWithoutFeedback>	
													
													<View style={{width:BaseComponent.W*365/375,height:0.5,backgroundColor:'#DDDDDD',marginLeft:BaseComponent.W*10/375}}></View>
													
													<View style={{width:'100%',height:BaseComponent.W*50/375,display:item.orderStatus=="WAIT_FOR_DELIVERY"||item.orderStatus=="WAIT_FOR_RECEIVED"?'flex':'none'}}>
														<View style={{width:'100%',height:BaseComponent.W*50/375,flexDirection:'row',alignItems:'center',justifyContent:'flex-end'}}>
															
															<TouchableWithoutFeedback onPress={()=>{
																thiz.navigate('LogisticsTracking',{title:"物流追踪",id:item1.id});											
															}}>
															<View style={{width:BaseComponent.W*70/375,height:BaseComponent.W*30/375,borderRadius:BaseComponent.W*15/375,
																		marginRight:BaseComponent.W*15/375,borderWidth:0.5,borderColor:'#606060',justifyContent:'center',alignItems:'center'}}>
																<Text style={{fontSize:BaseComponent.W*13/375,color:'#393A3A'}}>追踪物流</Text>
															</View>
															</TouchableWithoutFeedback>
															
															<TouchableWithoutFeedback onPress={()=>{
																  //订单确认收货
															      thiz.setState({shouhuovisible:true,id:item1.id})
															}}>
															<View style={{width:BaseComponent.W*70/375,height:BaseComponent.W*30/375,borderRadius:BaseComponent.W*15/375,display:item.orderPackages[index1].deliveryStatus=="NOT_DELIVER"&&item.orderPackages[index1].receiverReceivingStatus=="NOT_RECEIVED"?'none':'flex',
																		marginRight:BaseComponent.W*15/375,borderWidth:0.5,borderColor:'#606060',justifyContent:'center',alignItems:'center'}}>
																<Text style={{fontSize:BaseComponent.W*13/375,color:'#252525'}}>确认收货</Text>
															</View>
															</TouchableWithoutFeedback>

														</View>
													</View>

													<View style={{width:'100%',height:BaseComponent.W*50/375,display:item.orderStatus!="WAIT_FOR_PAY"?'none':(item.orderPackages.length-1==index1?'flex':'none')}}>
														<View style={{width:'100%',height:BaseComponent.W*50/375,flexDirection:'row',alignItems:'center',justifyContent:'flex-end'}}>
															<TouchableWithoutFeedback onPress={()=>{
																thiz.setState({SelectAddressOrDate:true});
																thiz.open();
		                                      					thiz.emit("cancel_order",{businessOrderNo:item.businessOrderNo});
															}}>
															<View style={{width:BaseComponent.W*70/375,height:BaseComponent.W*30/375,borderRadius:BaseComponent.W*15/375,
																		marginRight:BaseComponent.W*15/375,borderWidth:0.5,borderColor:'#999899',justifyContent:'center',alignItems:'center'}}>
																<Text style={{fontSize:BaseComponent.W*13/375,color:'#B6B6B6'}}>取消订单</Text>
															</View>
															</TouchableWithoutFeedback>

															<TouchableWithoutFeedback onPress={()=>{thiz.clickToPay(item)}}>
															<View style={{width:BaseComponent.W*70/375,height:BaseComponent.W*30/375,borderRadius:BaseComponent.W*15/375,
																		marginRight:BaseComponent.W*15/375,borderWidth:0.5,borderColor:'#FF407E',justifyContent:'center',alignItems:'center'}}>
																<Text style={{fontSize:BaseComponent.W*13/375,color:'#FF407E'}}>立即付款</Text>
															</View>
															</TouchableWithoutFeedback>

														</View>
													</View>

													<View style={{display:index1<item.orderPackages.length-1?"flex":"none",width:BaseComponent.W*365/375,height:0.5,backgroundColor:'#DDDDDD',marginLeft:BaseComponent.W*10/375}}></View>
												</View>

												)	
											})
										}
									</View>
								)
							})
						}	
						{
                          thiz.state.hasNext?null:thiz.nomore({bgcolor:'#F0F0F0'})
                        }
						</ScrollView>
		        			</View>))
		        }
				
			</View>
		)
	}

}

const styles=StyleSheet.create({
	item:{
		width:'100%',
		height:BaseComponent.H*0.138,
		flexDirection:'row',
		backgroundColor:'#fff',
		marginTop:BaseComponent.W*0.02,
		marginBottom:BaseComponent.W*0.02
	},
	sectionHeader:{
		width:'100%',
		height:BaseComponent.H*0.075,
		flexDirection:'column',
		backgroundColor:'#fff',
	},
	itemImg:{
		width:BaseComponent.W*0.213,
		height:BaseComponent.H*0.138,
		justifyContent:'center',
		alignItems:'center',
	},
	itemIns:{
		width:BaseComponent.W*0.413,
		height:BaseComponent.H*0.138,
		flexDirection:'column',
		justifyContent:'space-around'
	},
	itemNumber:{
		flex:1,
		flexDirection:'column',
		alignItems:'flex-end',
		marginRight:BaseComponent.W*0.029,
		marginTop:BaseComponent.W*5/375,
		justifyContent:'space-between'
	},
	operation:{
		width:'100%',
		height:BaseComponent.W*0.22,
		flexDirection:'column',	
	},
	Top:{
		width:'100%',
		height:(Dimensions.get('window').height-StatusBar.currentHeight)/17,
		flexDirection:'row',
		alignItems:'center',
		marginTop:Platform.OS ==="ios"?Dimensions.get('window').height/25:0,	
	},
	Top_image:{
		width:Dimensions.get('window').width/37,
		height:'50%',
		marginLeft:Dimensions.get('window').width/29
	},
	Top_view:{
		height:'75%',
		width:'74%',
		marginLeft:Dimensions.get('window').width/15*2-Dimensions.get('window').width/37-Dimensions.get('window').width/29,
		borderRadius:Dimensions.get('window').width/20,
		borderColor:'#BBBBBB',
		borderWidth:1,
		borderStyle:'solid',
	},
	search:{
		marginTop:Dimensions.get('window').height/23,
		width:'100%',
		flexDirection:'column',
	},
	renderItem:{
		height:Dimensions.get('window').height/25,
		backgroundColor:'red',
		justifyContent:'center',
		alignItems:'center'
	},
	statusBar:{
    backgroundColor:"rgb(255,255,255)",
    height:BaseComponent.SH,
    position:"relative",
    zIndex:1,
    opacity:1,
  },
  navBar:{
    backgroundColor:"rgb(255,255,255)",
    height:BaseComponent.H*0.07,
    position:"relative",
    zIndex:1,
    opacity:1,
    borderBottomWidth:1,
    borderBottomColor:"rgb(241,241,241)"
  },
  wrapper:{
    backgroundColor:"#f0f0f0",
    flex:1,
    flexDirection:"column",
    alignItems:"stretch",
    justifyContent:"flex-start",
    position:"relative",
  },
})