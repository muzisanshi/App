/** @format */

import React from "react";
import {AppRegistry,Image,TouchableWithoutFeedback,AsyncStorage,Platform} from 'react-native';
import App from './App';
import Home from './components/Home';
import Category from './components/category/Category';
import Cart from './components/Cart';
import Mine from './components//mine/Mine';
import HomeSearch from './components/HomeSearch';
import HomeSearchResult from './components/HomeSearchResult';
import AddAddress from './components/mine/AddAddress';
import EditAddress from './components/mine/EditAddress';
import LogisticsTracking from './components/mine/LogisticsTracking';
import IntegralDetails from './components/integral/IntegralDetails';
import MyIntegral from './components/integral/MyIntegral';
import InvitePeople from './components/integral/InvitePeople';
import TaInvitePeople from './components/integral/TaInvitePeople';

import PrivacyPolicy from './components/PrivacyPolicy';
import ServiceClause from './components/ServiceClause';
import Introduction from './components/Introduction';
import SearchAddress from './components/mine/SearchAddress';
import RefundInstructions from './components/RefundInstructions';
import OrderAmountStatistics from './components/order/OrderAmountStatistics';
import AfterSalesDetails from './components/order/AfterSalesDetails';
import PromotionCode from './components/integral/PromotionCode';

import DingdanXiangqingDaizhifu from './components/mine/OrderDetailWaitForPay';
import shouhuoAddress from './components/mine/shouhuoAddress';
import ForgetPwd from './components/login/ForgetPwd';
import Register from './components/login/Register';
import Login from './components/login/Login';
import SubmitPwd from './components/login/SubmitPwd';
import BindPhone from './components/login/BindPhone';
import Personal from './components/mine/Personal';
import ApplyShouhou from './components/mine/ApplyShouhou'; 
import GoodsXiangQing from './components/category/GoodsXiangQing';   
import DeliverGoods from './components/cart/DeliverGoods';
import AddDeliverGoods from './components/cart/AddDeliverGoods';
import Msgcenter from './components/mine/Msgcenter';
import MsgNotice from './components/mine/MsgNotice';
import CartPay from './components/cart/CartPay';
import FinishPay from './components/cart/FinishPay';
import Payfailure from './components/cart/Payfailure';
import PersonalInfo from './components/mine/PersonalInfo';
import UpdateNickname from './components/mine/UpdateNickname';
import MinePhoneNo from './components/mine/MinePhoneNo';
import AccountManagement from './components/mine/AccountManagement';
import DingdanXiangqingDaifahuo from './components/mine/OrderDetailWaitForDelivery';
// import DingdanXiangqingFinishDeal from './components/mine/DingdanXiangqingFinishDeal';
// import DingdanXiangqingFinishAfterSales from './components/mine/DingdanXiangqingFinishAfterSales';
import SelectShouhouGoods from './components/mine/SelectShouhouGoods';
import ShouhouType from './components/mine/ShouhouType';
import ApplyShouhouRefund from './components/mine/ApplyShouhouRefund';
import AllBrand from './components/category/AllBrand';
import Tuihuotuikuan from './components/mine/Tuihuotuikuan';

import Wodedingdan from './components/mine/Wodedingdan';
import Country from './components/home/Country';
import DailyUpdate from './components/home/DailyUpdate';
import Banner2 from './components/car/Banner2';
import ShopCar3 from './components/car/ShopCar3';
import BannerTxtImg from './components/home/BannerTxtImg';
import BannerActivity from './components/home/BannerActivity';
import DingdanQuanbu from './components/mine/AllOrders';
import FenleiXiangqing from './components/category/FenleiXiangqing';
import Querendingdan from './components/cart/Querendingdan';
import WebBrowser from './components/common/WebBrowser';
import Service from './components/common/Service';
import Set from './components/mine/Set';
import AboutMe from './components/mine/AboutMe';

import Launcher from "./components/common/Launcher";
import Guide from "./components/common/Guide";

// 订单搜索
import OrderSearch from "./components/order/OrderSearch";

import Member from "./components/member/Member";

import {name as appName} from './app.json';

// import * as Wxpay from 'react-native-wechat';
import Wxpay from '@yyyyu/react-native-wechat';
Wxpay.registerApp({appId:'wx928c9e134253166e'}).then(function(ret){
    console.log("--------wxpay_registerApp_ret--------  "+JSON.stringify(ret));
}).catch(function(err){
    console.log("--------wxpay_registerApp_err--------  "+JSON.stringify(err));
});

import {createBottomTabNavigator,createStackNavigator} from 'react-navigation';
// 导入工具函数库
import T from "./lib/Tools";
// 导入图片资源库
import Imgs from "./lib/Img";

import BaseComponent from "./components/BaseComponent.js";
import config from "./config.js";
import TabIcon from "./lib/TabIcon"

// Tools实例
let ti = T.getInstance();

const prefix = Platform.OS == 'android' ? 'mychat://mychat/' : 'mychat://';
// 底部导航栏
const Tab = createBottomTabNavigator(
    {
        Home: {
            screen: Home,
            path: 'chat/:user',
            navigationOptions:{
                tabBarLabel:"首页",
                tabBarIcon:function(e){
                    ti.log("--------fuck--------","fuck");
                	return (
                        
                		// <Image style={{width:e.focused?40:30,height:e.focused?40:30}} source={e.focused?Imgs.home[1]:Imgs.home[0]}/>
                        <TabIcon width={30} tabFlag={"home"} selected={e.focused}/>
                        
                	);
                },
                tabBarOnPress:async (obj: any) => {
                    BaseComponent.CUR_TAB = "Home";
                    obj.defaultHandler();
                    
                }
            }
        },

        Category: {
            screen: Category,
            navigationOptions:{
                tabBarLabel:"分类",
                tabBarIcon:function(e){

                	return (
                		// <Image style={{width:e.focused?40:30,height:e.focused?40:30}} source={e.focused?Imgs.category[1]:Imgs.category[0]}/>
                        <TabIcon width={30} tabFlag={"category"} selected={e.focused}/>
                	);
                },
                tabBarOnPress:async (obj: any) => {
                    BaseComponent.CUR_TAB = "Category";
                    obj.defaultHandler();
                    
                }
            }
        },
        
        Cart: {
            screen: Cart,
            navigationOptions:{
                tabBarLabel:"购物车",
                tabBarIcon:function(e){
                	return (
                		// <Image style={{width:e.focused?40:30,height:e.focused?40:30}} source={e.focused?Imgs.cart[1]:Imgs.cart[0]}/>
                        <TabIcon width={30} tabFlag={"cart"} selected={e.focused}/>
                	);
                },
                tabBarOnPress:async (obj: any) => {

                    // 判断是否已登录
                    ti.load("account",function(ret,err){
                        if(ret){
                            obj.defaultHandler();
                            BaseComponent.CUR_TAB = "Cart";
                            ti.emit("click_cart");
                        }else{
                            // 未登录跳转到登录页面（让首页代理跳转到登录注册页面）
                            ti.emit("go_login");
                        }
                    });
                }
            }
        },
        Mine:{
            screen: Mine,
            navigationOptions:{
                tabBarLabel:"我的",
                tabBarIcon:function(e){

                	return (
                		// <Image style={{width:e.focused?40:30,height:e.focused?40:30}} source={e.focused?Imgs.mine[1]:Imgs.mine[0]}/>
                        <TabIcon width={30} tabFlag={"mine"} selected={e.focused}/>
                	);
                },
                tabBarOnPress:async (obj: any) => {
                    BaseComponent.CUR_TAB = "Mine";
                    obj.defaultHandler();
                    ti.emit("click_mine");    
                }
            }
        },
    },
    {
    	tabBarOptions: {
	        showIcon: true,
	        showLabel: true,
	        upperCaseLabel: false,
	        pressOpacity: 0.8,
	        style: {
	            backgroundColor: '#fff',//导航栏背景色
	            paddingBottom: 0,
	            borderTopWidth: 0.5,
	            borderTopColor: 'rgb(237,235,235)',//边界线颜色
	            height:50
	        },
	        labelStyle: {
	            fontSize: 12,
	            margin: 1,
	            color:"#666"
	        },
	        indicatorStyle: {height:0},
	    },
	    swipeEnabled: false,
	    animationEnabled: false,
	    lazy: true,
	    backBehavior: 'none',
    }
);

// 导航配置
let navConfig = {
    Tab:{
        screen: Tab,
        navigationOptions:{
            header:null
        }
    },

    // 启动页
    Launcher:{
        screen: Launcher,
        navigationOptions:{
            header:null
        }
    },

    // 引导页
    Guide:{
        screen: Guide,
        navigationOptions:{
            header:null,
            gesturesEnabled: false,
        }
    },

    HomeSearch:{
        screen:HomeSearch,
        navigationOptions:{
            header:null
        }
    },
    HomeSearchResult:{
        screen:HomeSearchResult,
        navigationOptions:{
            header:null
        }
    },
   
    // DingdanXiangqingDaishouhuo:{
    //     screen:DingdanXiangqingDaishouhuo,
    //     navigationOptions:{
    //         header:null
    //     }
    // },
    DingdanXiangqingDaizhifu:{
        screen:DingdanXiangqingDaizhifu,
        navigationOptions:{
            header:null
        }
    },
    shouhuoAddress:{
        screen:shouhuoAddress,
        navigationOptions:{
            header:null
        }
    },
    AddAddress:{
        screen:AddAddress,
        navigationOptions:{
            header:null
        }
    },
    Wodedingdan:{
        screen:Wodedingdan,
        navigationOptions:{
            header:null,
            gesturesEnabled: false
        }
    },
    OrderSearch:{
        screen:OrderSearch,
        navigationOptions:{
            header:null
        }
    },
    DingdanQuanbu:{
        screen:DingdanQuanbu,
        navigationOptions:{
            header:null
        }
    },
   FenleiXiangqing:{
    screen:FenleiXiangqing,
    navigationOptions:{
        header:null
    }
   },
    // 首页banner图文页面跳转
    BannerTxtImg:{
        screen:BannerTxtImg,
        navigationOptions:{
            header:null
        }
    },
    Querendingdan:{
        screen:Querendingdan,
        navigationOptions:{
            header:null
        }
    },
    BannerActivity:{
        screen:BannerActivity,
        navigationOptions:{
            header:null
        }
    },
    Country:{
        screen:Country
    },
    DailyUpdate:{
        screen:DailyUpdate,
        navigationOptions:{
            header:null
        }
    },
    Banner2:{
        screen:Banner2
    },
    ShopCar3:{
        screen:ShopCar3
    },
    ForgetPwd:{
        screen:ForgetPwd,
        navigationOptions:{
            header:null
        }
    },
    Register:{
        screen:Register,
        navigationOptions:{
            header:null
        }
    },
    Login:{
        screen:Login,
        navigationOptions:{
            header:null
        }
    },
    SubmitPwd:{
        screen:SubmitPwd,
        navigationOptions:{
            header:null
        }
    },
    BindPhone:{
        screen:BindPhone,
        navigationOptions:{
            header:null
        }
    },
    Personal:{
        screen:Personal,
        navigationOptions:{
            header:null
        }
    },
    ApplyShouhou:{
        screen:ApplyShouhou,
        navigationOptions:{
            header:null
        }
    },
    WebBrowser:{
        screen:WebBrowser,
        navigationOptions:{
            header:null
        }
    },
    Service:{
        screen:Service,
        navigationOptions:{
            header:null
        }
    },
    GoodsXiangQing:{
        screen:GoodsXiangQing,
        navigationOptions:{
            header:null
        }
    },
    DeliverGoods:{
        screen:DeliverGoods,
        navigationOptions:{
            header:null
        }
    },
    AddDeliverGoods:{
        screen:AddDeliverGoods,
        navigationOptions:{
            header:null
        }
    },
    Msgcenter:{
        screen:Msgcenter,
        navigationOptions:{
            header:null
        }
    },
    MsgNotice:{
        screen:MsgNotice,
        navigationOptions:{
            header:null
        }
    },
    CartPay:{
        screen:CartPay,
        navigationOptions:{
            header:null
        }
    },
    FinishPay:{
        screen:FinishPay,
        navigationOptions:{
            header:null,
            gesturesEnabled: false,
        }
    },
    Set:{
        screen:Set,
        navigationOptions:{
            header:null
        }
    },
    PersonalInfo:{
        screen:PersonalInfo,
        navigationOptions:{
            header:null
        }
    },
    UpdateNickname:{
        screen:UpdateNickname,
        navigationOptions:{
            header:null
        }
    },
    AboutMe:{
        screen:AboutMe,
        navigationOptions:{
            header:null
        }
    },
    MinePhoneNo:{
        screen:MinePhoneNo,
        navigationOptions:{
            header:null
        }
    },
    AccountManagement:{
        screen:AccountManagement,
        navigationOptions:{
            header:null
        }
    },
    DingdanXiangqingDaifahuo:{
        screen:DingdanXiangqingDaifahuo,
        navigationOptions:{
            header:null
        }
    },
    // DingdanXiangqingFinishDeal:{
    //     screen:DingdanXiangqingFinishDeal,
    //     navigationOptions:{
    //         header:null
    //     }
    // },
    // DingdanXiangqingFinishAfterSales:{
    //     screen:DingdanXiangqingFinishAfterSales,
    //     navigationOptions:{
    //         header:null
    //     }
    // },
    EditAddress:{
        screen:EditAddress,
        navigationOptions:{
            header:null
        }
    },
    LogisticsTracking:{
        screen:LogisticsTracking,
        navigationOptions:{
            header:null
        }
    },
    ShouhouType:{
        screen:ShouhouType,
        navigationOptions:{
            header:null
        }
    },
    SelectShouhouGoods:{
        screen:SelectShouhouGoods,
        navigationOptions:{
            header:null
        }
    },
    ApplyShouhouRefund:{
        screen:ApplyShouhouRefund,
        navigationOptions:{
            header:null
        }
    },
    AllBrand:{
        screen:AllBrand,
        navigationOptions:{
            header:null
        }
    },
    Tuihuotuikuan:{
        screen:Tuihuotuikuan,
        navigationOptions:{
            header:null
        }
    },

    PrivacyPolicy:{
        screen:PrivacyPolicy,
        navigationOptions:{
            header:null
        }
    },
    ServiceClause:{
        screen:ServiceClause,
        navigationOptions:{
            header:null
        }
    },
    Introduction:{
        screen:Introduction,
        navigationOptions:{
            header:null
        }
    },
    RefundInstructions:{
        screen:RefundInstructions,
        navigationOptions:{
            header:null
        }
    },
    SearchAddress:{
        screen:SearchAddress,
        navigationOptions:{
            header:null
        }
    },
    OrderAmountStatistics:{
        screen:OrderAmountStatistics,
        navigationOptions:{
            header:null
        }
    },
    AfterSalesDetails:{
        screen:AfterSalesDetails,
        navigationOptions:{
            header:null
        }
    },
    Payfailure:{
        screen:Payfailure,
        navigationOptions:{
            header:null
        }
    },
    IntegralDetails:{
        screen:IntegralDetails,
        navigationOptions:{
            header:null
        }
    },
    MyIntegral:{
        screen:MyIntegral,
        navigationOptions:{
            header:null
        }
    },
    InvitePeople:{
        screen:InvitePeople,
        navigationOptions:{
            header:null
        }
    },
    TaInvitePeople:{
        screen:TaInvitePeople,
        navigationOptions:{
            header:null
        }
    },
    PromotionCode:{
        screen:PromotionCode,
        navigationOptions:{
            header:null
        }
    },
    //会员首页
    Member:{
        screen:Member,
        navigationOptions:{
            header:null
        }
    },
    initialRouteName:'Tab'
};

export const Cnavigator = createStackNavigator(navConfig);

/**
 * 暂时取消调接口授权
 */
// ti.request(config.authURL,{
//     method:"POST",
//     success:function(ret){
//         ti.log("--------ti.request--------","success");
        
//     },
//     error:function(err){
//         ti.log("--------ti.request--------","err");
//     }
// },true);

const MainApp = () => <Cnavigator uriPrefix={prefix} />;
// 发请求
AppRegistry.registerComponent(appName, ()=>MainApp);



 