/**
 * Copyright (c) 2015-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

#import "AppDelegate.h"
#import "AlipayModule.h"

#import <React/RCTBundleURLProvider.h>
#import <React/RCTLinkingManager.h>
#import <React/RCTRootView.h>
#import <React/RCTBridge.h>
#import <RNBackgroundDownloader.h>
#import <React/RCTLinkingManager.h>

@implementation AppDelegate

// 自动以桥接，解决app启动白屏的问题
static RCTBridge * bridge = nil;
static dispatch_once_t onceToken;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{

  // 启动图片延时: 3秒
  [NSThread sleepForTimeInterval:3];

  NSLog(@"--------AppDelegate_didFinishLaunchingWithOptions--------");  
  NSURL *jsCodeLocation;
  
  // 正式配置
  jsCodeLocation = [[RCTBundleURLProvider sharedSettings] jsBundleURLForBundleRoot:@"index" fallbackResource:nil];
  
  // 开发配置
  // jsCodeLocation = [NSURL URLWithString:@"http://192.168.2.112:8081/index.bundle?platform=ios&dev=true&minify=false"];
    // jsCodeLocation = [NSURL URLWithString:@"http://192.168.8.16:8081/index.bundle?platform=ios&dev=true&minify=false"];
    
  // 解决app启动白屏的问题
  dispatch_once(&onceToken,^{
    bridge = [[RCTBridge alloc] initWithBundleURL:jsCodeLocation
                                   moduleProvider:nil
                                    launchOptions:launchOptions];
  });

  // RCTRootView *rootView = [[RCTRootView alloc] initWithBundleURL:jsCodeLocation
  //                                                     moduleName:@"App"
  //                                              initialProperties:nil
  //                                                  launchOptions:launchOptions];

  NSLog(@"--------bridge_only--------");
  RCTRootView *rootView = [[RCTRootView alloc] initWithBridge:bridge
                                                      moduleName:@"App"
                                               initialProperties:nil];

  rootView.backgroundColor = [[UIColor alloc] initWithRed:1.0f green:1.0f blue:1.0f alpha:1];

  self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
  UIViewController *rootViewController = [UIViewController new];
  rootViewController.view = rootView;
  self.window.rootViewController = rootViewController;
  [self.window makeKeyAndVisible];
  return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
  NSLog(@"--------AppDelegate_openURL--------%@",url);
  NSString *urlStr = [url absoluteString];
  if([urlStr rangeOfString:@"wx928c9e134253166e"].location!=NSNotFound){
    return [RCTLinkingManager application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
  }
  [AlipayModule handleCallback:url];
  return YES;
}

- (void)application:(UIApplication *)application handleEventsForBackgroundURLSession:(NSString *)identifier completionHandler:(void (^)())completionHandler
{
  [RNBackgroundDownloader setCompletionHandlerWithIdentifier:identifier completionHandler:completionHandler];
}

@end
