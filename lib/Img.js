
/**
 * @name Img.js
 * @auhor 李磊
 * @date 2018.8.16
 * @desc 图片资源库
 */
//首页
// import home from "../image/home/home.png";
// import home_s from "../image/home/home_s.png";

// import category from "../image/home/category.png";
// import category_s from "../image/home/category_s.png";

// import cart from "../image/home/cart.png";
// import cart_s from "../image/home/cart_s.png";

// import mine from "../image/home/mine.png";
// import mine_s from "../image/home/mine_s.png";

// 新的tab图标
import home from "../image/tab/home.png";
import home_s from "../image/tab/home_active.png";

import category from "../image/tab/cate.png";
import category_s from "../image/tab/cate_active.png";

import cart from "../image/tab/cart.png";
import cart_s from "../image/tab/cart_active.png";

import mine from "../image/tab/mine.png";
import mine_s from "../image/tab/mine_active.png";




import banner1 from "../image/home/banner1.jpg";
import banner2 from "../image/home/banner2.jpg";
import banner3 from "../image/home/banner3.jpg";
import banner4 from "../image/home/banner4.jpg";

import muyin from "../image/home/muying.png";
import meizhuang from "../image/home/meizhuang.png";
import baojian from "../image/home/baojian.png";
import jujia from "../image/home/jujia.png";
import saohuo from "../image/home/saohuo.png";

import ad1 from "../image/home/ad1.png";
import ad2 from "../image/home/ad2.png";

import sort1 from "../image/home/sort1-1.png";
import sort2 from "../image/home/sort1-2.png";
import sort3 from "../image/home/sort2-1.png";
import sort4 from "../image/home/sort2-2.png";
import sort5 from "../image/home/sort2-3.png";

import recommend from "../image/home/recommend.png";

import dayNew from "../image/home/dayNew.png";
import dayNew1 from "../image/home/dayNew1.png";
import dnSlide1 from "../image/home/dnSlide1.png";
import dnSlide2 from "../image/home/dnSlide2.png";
import dnSlide3 from "../image/home/dnSlide3.png";
import home_more from "../image/home/home_more.png";

import timeDiscount from "../image/home/timeDiscount.png";
import shoppingCart from "../image/home/shoppingCart.png";

import specialEvents from "../image/home/specialEvents.png";
import globalShopping from "../image/home/globalShopping.png";
import gsShop1 from "../image/home/gsShop1.png";
import gsShop2 from "../image/home/gsShop2.png";
import gsShop3 from "../image/home/gsShop3.png";

import luna  from "../image/home/luna.png";
import luna1 from "../image/home/luna1.png";
import luna2 from "../image/home/luna2.png";
import luna3 from "../image/home/luna3.png";

import aptamil from "../image/home/aptamil.png";
import aptamil1 from "../image/home/aptamil1.png";
import aptamil2 from "../image/home/aptamil2.png";
import aptamil3 from "../image/home/aptamil3.png";

import swisse from "../image/home/swisse.png";
import sw1 from "../image/home/sw1.png";
import sw2 from "../image/home/sw2.png";

import back from "../image/home/back.png";

import hotProduct from "../image/home/hotProduct.png";
import hotProduct1 from "../image/home/hotProduct1.png";
import hotProduct2 from "../image/home/hotProduct2.png";
import hotProduct3 from "../image/home/hotProduct3.png";
import hotProduct4 from "../image/home/hotProduct4.png";

//二级页面国家馆
import returm from "../image/home/return.png";
import NorthernEurope from "../image/home/NorthernEurope.png";
import Australia from "../image/home/Australia.png";
import Germany from "../image/home/Germany.png";
import England from "../image/home/England.png";
import France from "../image/home/France.png";
import Korea from "../image/home/Korea.png";
import Japan from "../image/home/Japan.png";
import America from "../image/home/America.png";
export default {
	home:[home,home_s],
	category:[category,category_s],
	cart:[cart,cart_s],
	mine:[mine,mine_s],

	// home:["../image/tab/home.png","../image/tab/home_active.png"],
	// category:["../image/tab/home_active.png","../image/tab/cate_active.png"],
	// cart:["../image/tab/cart.png","../image/tab/cart_active.png"],
	// mine:["../image/tab/mine.png","../image/tab/mine_active.png"],
   
	banner1:banner1,
	banner2:banner2,
	banner3:banner3,
	banner4:banner4,

	muyin:muyin,
	meizhuang:meizhuang,
	baojian:baojian,
	jujia:jujia,
	saohuo:saohuo,

	ad1:ad1,
	ad2:ad2,

	sort1:sort1,
	sort2:sort2,
	sort3:sort3,
	sort4:sort4,
	sort5:sort5,

	recommend:recommend,

	dayNew:dayNew,
	dayNew1:dayNew1,
	dnSlide1:dnSlide1,
	dnSlide2:dnSlide2,
	dnSlide3:dnSlide3,
	home_more:home_more,

	timeDiscount:timeDiscount,
	shoppingCart:shoppingCart,

	specialEvents:specialEvents,
	globalShopping:globalShopping,
	gsShop1:gsShop1,
	gsShop2:gsShop2,
	gsShop3:gsShop3,

	luna:luna ,
	luna1:luna1,
	luna2:luna2,
	luna3:luna3,

	aptamil:aptamil,
	aptamil1:aptamil1,
	aptamil2:aptamil2,
	aptamil3:aptamil3,

	swisse:swisse,
	sw1:sw1,
	sw2:sw2,
	
	back:back,

	hotProduct:hotProduct,
	hotProduct1:hotProduct1,
	hotProduct2:hotProduct2,
	hotProduct3:hotProduct3,
	hotProduct4:hotProduct4,

	//二级页面国家馆
	returm:returm,

	NorthernEurope:NorthernEurope,
	Australia:Australia,
	Germany:Germany,
	England:England,
	France:France,
	Korea:Korea,
	Japan:Japan,
	America:America,
}
