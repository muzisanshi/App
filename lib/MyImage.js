/**
 * @name MyImage.js
 * @auhor 李磊
 * @date 2018.10.24
 * @desc 自己封装的超级图片组件
 */

import React, {Component} from "react";
import {Platform, StyleSheet, View,Text,Button,Image,TouchableWithoutFeedback} from "react-native";
import {ScrollView} from "react-native";
// 导入工具函数库
import T from "./Tools";
import BaseComponent from "../components/BaseComponent";
// 导入公共样式
import St from "./Style";
// 导入图片资源库
import Imgs from "./Img";

// Tools实例
let ti = T.getInstance();

// 属性列表
// url、width、height、style、onPress、onload、base64

export default class MyImage extends BaseComponent{

  constructor(props){
    super(props);
    this.state = {
    	realW:0,// 真实宽度
    	realH:0	// 真实高度
    }
  }

  handleProps(){
    let thiz = this;
    // 设置宽度
    this.width = this.props.width;
    // 设置高度
    this.height = this.props.height;
    // 图片加载回调
    this.onload = this.props.onload;
    this.url = this.props.url;
    // 额外的样式
    this.style = this.props.style?this.props.style:{display:"flex"};
    this.source = typeof(this.url)=="string"&&this.url.indexOf("http")>=0?{uri:this.url}:this.url;

    // android本地图片
    if(BaseComponent.OS=="android"){
      if(typeof(this.url)=="string"&&this.url.indexOf("file://")>=0){
        // thiz.log("--------android_MyImage_本地图片--------",this.url);
        this.source = {uri:this.url};
      }
    }
    // ios的本地图片
    if(BaseComponent.OS=="ios"){
      if(typeof(this.url)=="string"){
        // thiz.log("--------ios_MyImage_本地图片--------");
        this.source = {uri:this.url};
      }
    }

    // 判断是否是base64数据
    if(this.props.base64){
      this.source = {uri:this.props.base64};
    }

    this.onPress = this.props.onPress?this.props.onPress:()=>{console.log("MyImage do nothing")};
  }

  render() {
  	let thiz = this;
    
    thiz.handleProps();

    return (
      <TouchableWithoutFeedback onPress={thiz.onPress}>
      	<Image resizeMode={"contain"} style={Object.assign({width:thiz.state.realW,height:thiz.state.realH},thiz.style)} source={thiz.source}/>
      </TouchableWithoutFeedback>
    );
  }

  componentDidMount(){
  	let thiz = this;

    // 这种只适合于网络图片和file:开头的路径
  	// if(typeof(thiz.url)=="string"&&thiz.url.indexOf("http")>=0){
    if(typeof(thiz.url)=="string"){
      // thiz.log("--------MyImage_本地图片_source222222222--------",thiz.url);
  		// 处理图片大小
	  	Image.getSize(thiz.url,function(width,height){
        // thiz.log("--------MyImage_本地图片2222222222--------"+thiz.url,width+","+height);
	  		let rate = height/width;
	  		let setW = thiz.width!=undefined?thiz.width:0;
        let setH = thiz.height!=undefined?thiz.height:0;
        let rW = setW>0?setW:setH/rate;
        let rH = setH>0?setH:setW*rate;

	  		thiz.setState({
	  			realW:rW,
	  			realH:rH
	  		});

        if(thiz.onload){
          thiz.onload({
            rate:rate,
            rW:rW,
            rH:rH,
          });
        }
	  	});

    // 这种只适合于用id的图片
  	}else{
      let imgInfo = Image.resolveAssetSource(thiz.url);
  		if(imgInfo){
  			let rate = imgInfo.height/imgInfo.width;
	  		let setW = thiz.width!=undefined?thiz.width:0;
        let setH = thiz.height!=undefined?thiz.height:0;
        let rW = setW>0?setW:setH/rate;
        let rH = setH>0?setH:setW*rate;

	  		thiz.setState({
          realW:rW,
          realH:rH
        });

        if(thiz.onload){
          thiz.onload({
            rate:rate,
            rW:rW,
            rH:rH,
          });
        }
        
  		}
  	}
  	
  }

}
