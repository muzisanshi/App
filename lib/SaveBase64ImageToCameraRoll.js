import RNFetchBlob from 'react-native-fetch-blob';
import RNFS from 'react-native-fs';
import { Alert, Platform, CameraRoll } from 'react-native';

import Tools from "./Tools";

let ti = Tools.getInstance();


export default function (base64Img, success, fail) {
  
  const dirs =  Platform.OS === 'ios' ? RNFS.LibraryDirectoryPath : RNFS.ExternalDirectoryPath; // 外部文件，共享目录的绝对路径（仅限android）
  const downloadDest = `${dirs}/${((Math.random() * 10000000) | 0)}.png`;
  const imageDatas = base64Img.split('data:image/png;base64,');
  const imageData = imageDatas[1];

  RNFetchBlob.fs.writeFile(downloadDest, imageData, 'base64').then((rst) => {

    try {
      CameraRoll.saveToCameraRoll(downloadDest).then((e1) => {
        console.log('suc',e1);
        success && success();
      }).catch((e2) => {
        console.log('fai',e2);
        ti.toast('没有读写权限。请在[设置]-[应用权限]-[小洋匠]开启');
        fail && fail();
      })
    } catch (e3) {
      console.log('catch',e3);
      fail && fail();
    }
  });
}