
/**
 * @name Style.js
 * @auhor 李磊
 * @date 2018.8.14
 * @desc 公用样式
 */

import {StyleSheet} from "react-native";
import BaseComponent from "../components/BaseComponent";
let Style = StyleSheet.create({
	hang2:{
	    flex:1 ,
	    alignItems:'center',
	    justifyContent:'center',
	    backgroundColor:'#f0f0f0'   
  	},
  	worldBuy:{
	    flex:1 ,
	    backgroundColor:'white' ,
  	},
  	SpecialEventsImg:{
	    width:BaseComponent.W/1,
	    height:185 ,
 	},
	worldBuyShangp:{
	    flex:1 ,
	    backgroundColor:'white' ,
 	},
  	img14:{
	    width:BaseComponent.W/3.7,
	    height:BaseComponent.W*100/375 ,
	    margin:5 ,
	    marginTop: 10 ,
	    marginBottom: 10 ,
	    resizeMode:'contain'
  	},
  	hang3:{
	    height:10 ,
	    backgroundColor:'#F0F0F0'
  	},
  	lunaShangp:{
	    margin:3,
	    marginLeft:6,
	    width:BaseComponent.W*100/375,
	    height:BaseComponent.W*181/375 ,

  	},
	lunaImage:{
	    width:BaseComponent.W*100/375,
	    height:BaseComponent.W*100/375 ,
	    marginTop:10,
	    alignItems:'center',
	    resizeMode:'contain'
	},
	lunatxt:{
	    fontSize:12,
	    color:'#3A3A3A',
	    marginTop:5
	},
	lunatxt1:{
	    color:'#FF407E',
	    marginTop:10 ,
	    textAlign:'center',
	    fontSize:13,
	},

	// 迷你字体
	fontMini:{
		fontSize:12,
	},
	
	// 小字体
	fontSmall:{
		fontSize:14,
	},

	// 中等字体
	fontMid:{
		fontSize:16,
	},

	// 稍大字体
	fontBig:{
		fontSize:18,
	},

	// 大字体
	fontLarge:{
		fontSize:20,
	}
});

export default Style;