/**
 * @name TabIcon.js
 * @auhor 李磊
 * @date 2018.10.24
 * @desc 自己封装的Icon组件
 */

import React, {Component} from "react";
import {Platform, StyleSheet, View,Text,Button,Image,TouchableWithoutFeedback} from "react-native";
import {ScrollView} from "react-native";
// 导入工具函数库
import T from "./Tools";
import BaseComponent from "../components/BaseComponent";
// 导入公共样式
import St from "./Style";
// 导入图片资源库
import Imgs from "./Img";

// Tools实例
let ti = T.getInstance();

// 属性列表
// url、width、height、style、onPress、onload

export default class TabIcon extends BaseComponent{

  constructor(props){
    super(props);
    let thiz = this;

    this.state = {
    	realW:0,// 真实宽度
    	realH:0,// 真实高度

      // 最终显示的图标的url
      // finalUrl:thiz.props.selected?Imgs[thiz.props.tabFlag][1]:Imgs[thiz.props.tabFlag][0],
      finalUrl:null,

    };

    // 初始化属性和数据
    this.init();

    // 监听消息
    this.listen("change_tab_icon",function(ret,err){

      if(ret){

        let urlTmp = thiz.selected?ret[thiz.tabFlag][1]:ret[thiz.tabFlag][0];
        let url = urlTmp.uri?urlTmp.uri:urlTmp;

        thiz.ajust(url);
      }

    });

    // 只需要在home的TabIcon里面加载所有图标数据就行了
    if(this.tabFlag=="home"&&this.selected){
      thiz.getOnlineIcons();
    }
    
  }

  // 处理属性 
  init(){
    let thiz = this;
    // 设置宽度
    thiz.width = thiz.props.width;
    // 设置高度
    thiz.height = thiz.props.height;

    // 图标基础宽高
    thiz.baseW = 0;
    thiz.baseH = 0;

    // TabIcon的标志
    thiz.tabFlag = thiz.props.tabFlag;

    // 当前图标是用于选中还是未选中
    thiz.selected = thiz.props.selected;
  }

  // 获取在线图标
  getOnlineIcons(){
    let thiz = this;

    // 默认返回的数据
    let imgs = {
      home:Imgs.home,
      category:Imgs.category,
      cart:Imgs.cart,
      mine:Imgs.mine
    };

    thiz.request("barBar/getByCodes",{
      method:"POST",
      success:function(ret){

        if(ret&&ret.respCode==="00"){

          if(ret.data.length>0){// 要更新tab图标

            // 处理数据

            // 判断是否有各个tab的配置
            for(let i=0;i<ret.data.length;i++){
              if(ret.data[i].code=="INDEX_BUTTON"){
                if(ret.data[i].unActiveImageAttachment&&ret.data[i].unActiveImageAttachment.resourceFullAddress){
                  imgs.home[0] = {uri:ret.data[i].unActiveImageAttachment.resourceFullAddress};
                }
                if(ret.data[i].activeImageAttachment&&ret.data[i].activeImageAttachment.resourceFullAddress){
                  imgs.home[1] = {uri:ret.data[i].activeImageAttachment.resourceFullAddress};
                }
                continue;
              }
              if(ret.data[i].code=="GOODS_GROUP_BUTTON"){
                if(ret.data[i].unActiveImageAttachment&&ret.data[i].unActiveImageAttachment.resourceFullAddress){
                  imgs.category[0] = {uri:ret.data[i].unActiveImageAttachment.resourceFullAddress};
                }
                if(ret.data[i].activeImageAttachment&&ret.data[i].activeImageAttachment.resourceFullAddress){
                  imgs.category[1] = {uri:ret.data[i].activeImageAttachment.resourceFullAddress};
                }
                continue;
              }
              if(ret.data[i].code=="SHOPPING_CAR_BUTTON"){
                if(ret.data[i].unActiveImageAttachment&&ret.data[i].unActiveImageAttachment.resourceFullAddress){
                  imgs.cart[0] = {uri:ret.data[i].unActiveImageAttachment.resourceFullAddress};
                }
                if(ret.data[i].activeImageAttachment&&ret.data[i].activeImageAttachment.resourceFullAddress){
                  imgs.cart[1] = {uri:ret.data[i].activeImageAttachment.resourceFullAddress};
                }
                continue;
              }
              if(ret.data[i].code=="MINE_BUTTON"){
                if(ret.data[i].unActiveImageAttachment&&ret.data[i].unActiveImageAttachment.resourceFullAddress){
                  imgs.mine[0] = {uri:ret.data[i].unActiveImageAttachment.resourceFullAddress};
                }
                if(ret.data[i].activeImageAttachment&&ret.data[i].activeImageAttachment.resourceFullAddress){
                  imgs.mine[1] = {uri:ret.data[i].activeImageAttachment.resourceFullAddress};
                }
              }
            }
            
            let urlTmp = thiz.selected?imgs[thiz.tabFlag][1]:imgs[thiz.tabFlag][0];
            let url = urlTmp.uri?urlTmp.uri:urlTmp;
            // 调整样式
            thiz.ajust(url);
            
          }
        }

        // 把数据发出去，让其他TabIcon组件也更新图标
        thiz.emit("change_tab_icon",imgs);
        
      },
      error:function(err){
        thiz.log("--------barBar/getByCodes_err-------",err);
        // 把数据发出去，让其他TabIcon组件也更新图标
        thiz.emit("change_tab_icon",imgs);
      }
    });
  }

  // 调整样式
  ajust(url){
    let thiz = this;
    // 从网络加载图标
    if(typeof(url)=="string"&&url.indexOf("http")>=0){

      // 处理图片大小
      Image.getSize(url,function(width,height){
        let rate = height/width;
        let setW = thiz.width!=undefined?thiz.width:0;
        let setH = thiz.height!=undefined?thiz.height:0;
        let rW = setW>0?setW:setH/rate;
        let rH = setH>0?setH:setW*rate;

        // 获取基础宽高
        thiz.baseW = rW;
        thiz.baseH = rH;

        thiz.setState({
          
          // 点击放大效果
          // realW:thiz.selected?rW*1.1:rW,
          // realH:thiz.selected?rH*1.1:rH,

          realW:rW,
          realH:rH,

          finalUrl:{uri:url},
        });

      });

    }else{// 从本地加载图标
      
      let imgInfo = Image.resolveAssetSource(url);
      if(imgInfo){
        let rate = imgInfo.height/imgInfo.width;
        let setW = thiz.width!=undefined?thiz.width:0;
        let setH = thiz.height!=undefined?thiz.height:0;
        let rW = setW>0?setW:setH/rate;
        let rH = setH>0?setH:setW*rate;
        
        // 获取基础宽高
        thiz.baseW = rW;
        thiz.baseH = rH;
        
        thiz.setState({

          // 点击放大效果
          // realW:thiz.selected?rW*1.1:rW,
          // realH:thiz.selected?rH*1.1:rH,

          realW:rW,
          realH:rH,

          finalUrl:url,
        });
        
      }

    }// end
  }

  render() {
  	let thiz = this;
    return (
      <Image resizeMode={"contain"} style={{width:thiz.state.realW,height:thiz.state.realH}} source={thiz.state.finalUrl}/>
    );
  }

  componentDidMount(){
  	let thiz = this;
    thiz.ajust(thiz.state.finalUrl);
  	
  }

}
